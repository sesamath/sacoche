<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action     = Clean::post('f_action'          , 'texte');
$page_ref   = Clean::post('f_page_ref'        , 'id');
$colonne    = Clean::post('choix_'.$page_ref  , 'id');
$moy_classe = Clean::post('moyenne_'.$page_ref, 'entier'); // pour le 2nd degré
$colonne_id = Clean::post('f_colonne_id'      , 'entier');
$legende    = Clean::post('f_colonne_legende' , 'texte');

$tab_colonne_choix = array('moyenne','pourcentage','position','objectif');
$tab_colonne_id = array(
  'reussite' => array(11,12,13),
  'objectif' => array(21,22,23,24),
  'maitrise' => array(31,32,33,34),
  'position' => array(41,42,43,44),
);

if( ($action=='memoriser_legende') && in_array($colonne_id,$tab_colonne_id['position']) && $legende )
{
  DB_STRUCTURE_LIVRET::DB_modifier_legende( $colonne_id , $legende );
  Json::end( TRUE );
}

if( ($action!='enregistrer_choix') || !$page_ref )
{
  Json::end( FALSE , 'Erreur avec les données transmises (référence de page) !' );
}

$DB_ROW = DB_STRUCTURE_LIVRET::DB_recuperer_page_info($page_ref);

if( !in_array( $DB_ROW['livret_page_rubrique_type'] , array('c3_matiere','c4_matiere') ) )
{
  $moy_classe = 0 ;
}

if( empty($DB_ROW) || !$DB_ROW['livret_page_rubrique_type'] /*brevet*/ )
{
  Json::end( FALSE , 'Erreur avec les données transmises (incohérence base) !' );
}

if( in_array( $DB_ROW['livret_page_colonne'] , $tab_colonne_choix ) )
{
  if( !in_array( $colonne , $tab_colonne_choix ) )
  {
    Json::end( FALSE , 'Erreur avec les données transmises (choix incompatible) !' );
  }
  else if( ($moy_classe !== 0 ) && ( $moy_classe !== 1 ) )
  {
    Json::end( FALSE , 'Erreur avec les données transmises (info moyenne classe) !' );
  }
  // Suite à la circulaire de rentrée du 26/06/2024 concernant l’évolution des conditions d’obtention du DNB (https://www.education.gouv.fr/bo/2024/Hebdo26/MENE2417753C), le 19/07/2024 l’équipe LSU a envoyé un message aux éditeurs disant que désormais il  était "impératif que les positionnements de fin de période contenus dans les bilans périodiques des élèves de 3e soient exclusivement de type NOTES (format numérique) pour permettre l’import dans LSU".
  // En conséquence, dans SACoche, pour les bilans périodiques du livret scolaire en 3e, les positionnements « avec objectifs d’apprentissage » ou « sur une échelle de 1 à 4 » ont été désactivés.
  // Cependant, le 27/09/2024, l’équipe LSU a envoyé un nouveau message aux éditeurs disant que "la réforme du DNB ayant été abandonnée pour la session 2024/2025, les informations transmises dans le message précédent sont à ignorer ; concernant le niveau 3e, les échanges avec Affelnet et Cyclades devraient fonctionner à l’identique de la session 2024, et le paramétrage « en notes » n’est pas modifié".
  // Attendu qu’il devait y avoir très peu d’établissements qui n’ont pas de bilan de fin de période sans positionnement chiffré en 3e, que ce dernier message était partiellement au conditionnel ("devraient"), et que n’était peut être que repoussé d’une année, il n’a pas effectué de nouvelle mise à jour pour revenir en arrière.
  // Et effectivement, c’est bien ce qui devrait se passer car, en date du 12/11/2024, une communication de la ministre de l’E.N. annonce que "en 2026, entrera en vigueur une nouvelle répartition entre le contrôle continu (40 %) et les épreuves terminales (60 %) ; le contrôle continu sera constitué de la moyenne des notes de 3e".
  // Un établissement insistant pour avoir encore cette possibilité de repousser l’échéance d’un an, on a quand même redébloqué ce paramétrage.
  /*
  else if( ($page_ref=='3e') && ( ($colonne == 'position') || ($colonne == 'objectif') ) )
  {
    Json::end( FALSE , 'Erreur avec les données transmises (positionnement 3e) !' );
  }
  */
  else
  {
    $tab_verif_id = ( ($colonne == 'position') || ($colonne == 'objectif') ) ? $tab_colonne_id[$colonne] : NULL ;
  }
}
else
{
  $tab_verif_id = $tab_colonne_id[$DB_ROW['livret_page_colonne']];
}

if($tab_verif_id)
{
  // On récupère les valeurs, on vérifie leur présence, mais on ne revérifie pas toutes les conditions (valeurs distinctes ou croissantes, etc.).
  $tab_seuils = array();
  foreach($tab_verif_id as $colonne_id)
  {
    $clef_debut = 'seuil_'.$page_ref.'_'.$colonne_id;
    $tab_seuils[$colonne_id]['min'] = Clean::post($clef_debut.'_min', 'entier');
    $tab_seuils[$colonne_id]['max'] = Clean::post($clef_debut.'_max', 'entier');
    if( is_null($tab_seuils[$colonne_id]['min']) || is_null($tab_seuils[$colonne_id]['max']) )
    {
      Json::end( FALSE , 'Erreur avec les données transmises (présence valeurs) !' );
    }
  }
  DB_STRUCTURE_LIVRET::DB_modifier_seuils( $page_ref , $tab_seuils );
}
if( in_array( $colonne , $tab_colonne_choix )  && ( ( $colonne != $DB_ROW['livret_page_colonne'] ) || ( $moy_classe != $DB_ROW['livret_page_moyenne_classe'] ) ) )
{
  DB_STRUCTURE_LIVRET::DB_modifier_page_colonne( $page_ref , $colonne , $moy_classe );
}

Json::end( TRUE );


?>
