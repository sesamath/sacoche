/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Modification d’une saisie : alerter besoin d’enregistrer
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('input[type=number]').change
    (
      function()
      {
        var obj_input = $(this);
        var input_id  = obj_input.attr('id');
        var obj_form  = obj_input.parent().parent().parent().parent().parent();
        var groupe_id = obj_form.attr('id').substring(5); // form_*
        $('#ajax_'+groupe_id).attr('class','alerte').html('Penser à enregistrer !');
        window.memo_change[groupe_id][input_id] = true;
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Intercepter la touche entrée
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('input[type=number]').keydown
    (
      function(e)
      {
        if(e.which==13)  // touche entrée
        {
          var obj_input = $(this);
          var obj_form  = obj_input.parent().parent().parent().parent().parent();
          var groupe_id = obj_form.attr('id').substring(5); // form_*
          $('#save_'+groupe_id).click();
        }
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Envoyer les saisies modifiées pour une classe
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('button[id^=save]').click
    (
      function()
      {
        var obj_button = $(this);
        obj_button.prop('disabled',true);
        var groupe_id = obj_button.attr('id').substring(5); // save_*
        // Récupérer les infos
        var tab_infos = [];
        $('#tbody_'+groupe_id+' tr td input').each
        (
          function()
          {
            var obj_input = $(this);
            var input_id = obj_input.attr('id');
            if( typeof(window.memo_change[groupe_id][input_id]) !== 'undefined' )
            {
              var epreuve  = obj_input.data('epreuve');
              var eleve_id = obj_input.parent().parent().data('eleve');
              var note     = obj_input.val().trim();
              var objet    = (note.length) ? 'ajouter' : 'supprimer' ;
              tab_infos.push( objet + '.' + eleve_id + '.' + epreuve + '.' + note );
            }
          }
        );
        $('#ajax_'+groupe_id).attr('class','loader').html('En cours&hellip;');
        // Les envoyer en ajax
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'enregistrer'+'&f_data='+tab_infos.join('_'),
            responseType: 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              obj_button.prop('disabled',false);
              $('#ajax_'+groupe_id).attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              obj_button.prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#ajax_'+groupe_id).attr('class','valide').html('Saisies enregistrées !');
                window.memo_change[groupe_id] = [];
              }
              else
              {
                $('#ajax_'+groupe_id).attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

  }
);
