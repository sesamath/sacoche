<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

$action          = Clean::post('f_action'         , 'texte');
$nom             = Clean::post('f_nom'            , 'nom');
$id_actuel       = Clean::post('f_id_actuel'      , 'entier');
$separation_date = Clean::post('f_separation_date', 'date_fr');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Rechercher un élève
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='chercher') && $nom )
{
  $DB_TAB = DB_STRUCTURE_ADMINISTRATEUR::DB_rechercher_user_for_fusion( $nom , 'eleve' , 1 /*actuel*/ );
  $nb_reponses = count($DB_TAB) ;
  if($nb_reponses==0)
  {
    Json::end( FALSE , 'Aucun élève trouvé !' );
  }
  else if($nb_reponses==1)
  {
    Json::end( TRUE , '<option value="'.$DB_TAB[0]['user_id'].'">'.html($DB_TAB[0]['user_nom'].' '.$DB_TAB[0]['user_prenom'].' ['.$DB_TAB[0]['user_login'].']').'</option>' );
  }
  else
  {
    Json::add_str('<option value="">&nbsp;</option>');
    foreach($DB_TAB as $DB_ROW)
    {
      Json::add_str('<option value="'.$DB_ROW['user_id'].'">'.html($DB_ROW['user_nom'].' '.$DB_ROW['user_prenom'].' ['.$DB_ROW['user_login'].']').'</option>');
    }
    Json::end( TRUE );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Séparer un compte élève en deux
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='separer') && $id_actuel && $separation_date )
{
  $DB_ROW = DB_STRUCTURE_PUBLIC::DB_recuperer_donnees_utilisateur( 'switch' , $id_actuel );
  // Vérifier l’existence / le profil / le statut
  if( empty($DB_ROW) || ($DB_ROW['user_profil_type']!='eleve') || ($DB_ROW['user_sortie_date']<TODAY_SQL) )
  {
    Json::end( FALSE , 'Identifiant du compte activé incompatible !' );
  }
    $separation_date_sql = To::date_french_to_sql($separation_date);
  // Login pour l’ancien compte ; à construire puis à tester (parmi tous les utilisateurs de l’établissement)
  $login = Outil::fabriquer_login( $DB_ROW['user_prenom'].' - Ancien' , $DB_ROW['user_nom'] , 'ELV' /*profil*/ );
  if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login) )
  {
    // Login pris : en chercher un autre en remplaçant la fin par des chiffres si besoin
    $login = DB_STRUCTURE_ADMINISTRATEUR::DB_rechercher_login_disponible($login);
  }
  // On crée l’ancien compte
  $id_ancien = DB_STRUCTURE_COMMUN::DB_ajouter_utilisateur( 0 /*sconet_id*/ , 0 /*sconet_num*/ , '' /*reference*/ , 'ELV' /*profil*/ , $DB_ROW['user_genre'] , $DB_ROW['user_nom'] , $DB_ROW['user_prenom'].' - Ancien' , NULL /*user_naissance_date*/ , '' /*user_email*/ , '' /*user_email_origine*/ , $login , '' /*password*/ );
  // On lui applique une date de sortie
  DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $id_ancien , array(':sortie_date' => $separation_date_sql) );
  // On sépare les données (sauf traitement de sacoche_user + sacoche_jointure_user_groupe + sacoche_jointure_parent_eleve + sacoche_jointure_message_destinataire)
  DB_STRUCTURE_ADMINISTRATEUR::DB_separer_donnees_comptes_eleves( $id_ancien , $id_actuel , $separation_date_sql );
  /*
  */
  // Log de l’action
  SACocheLog::ajouter('Séparation du compte élève '.$DB_ROW['user_nom'].' '.$DB_ROW['user_prenom'].' en deux avec effet au '.$separation_date.'.');
  $notification_contenu = date('d-m-Y H:i:s').' '.$_SESSION['USER_PRENOM'].' '.$_SESSION['USER_NOM'].' a séparé le compte élève '.$DB_ROW['user_nom'].' '.$DB_ROW['user_prenom'].' en deux avec effet au '.$separation_date.'.'."\r\n";
  DB_STRUCTURE_NOTIFICATION::enregistrer_action_admin( $notification_contenu , $_SESSION['USER_ID'] );
  // Afficher le retour
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
