/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    var mode         = false;
    var modification = false;
    var li_one_id    = false;
    var li_two_id    = false;
    var li_one_div   = false;
    var li_two_div   = false;
    var ordre = 0;

    $('#ul_import').hide();

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{3:{sorter:false},4:{sorter:false}} });
    var tableau_tri = function(){ $('#table_action').trigger( 'sorton' , [ [[0,0],[1,0]] ] ); };
    var tableau_maj = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    tableau_tri();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Proposer un nom pour le plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_groupe').change
    (
      function()
      {
        if( $(this).val() )
        {
          var separateur = ' | ';
          var groupe_nom = $('#f_groupe option:selected').text();
          var plan_nom   = $('#f_nom').val();
          if( !plan_nom )
          {
            plan_nom = groupe_nom + separateur + 'salle xxx';
          }
          else
          {
            var tab = plan_nom.split(separateur);
            if( typeof(tab[1]) !== 'undefined' )
            {
              plan_nom = groupe_nom + separateur + tab[1];
            }
            else
            {
              plan_nom = groupe_nom + separateur + tab[0];
            }
          }
          $('#f_nom').val(plan_nom);
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Pour le placement des élèves sur un plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Chargement du plan

    function charger_plan_classe( id , groupe_id , nom , nb_rangees , nb_colonnes )
    {
      var groupe_type = $('#f_groupe option[value='+groupe_id+']').parent().attr('label');
      $('#placer_id'         ).val(id);
      $('#placer_groupe'     ).val(groupe_id);
      $('#placer_groupe_type').val(groupe_type);
      $('#placer_rangees'    ).val(nb_rangees);
      $('#placer_colonnes'   ).val(nb_colonnes);
      // Afficher la zone associée après avoir chargé son contenu
      $.fancybox( '<label class="loader">'+'En cours&hellip;'+'</label>' );
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=charger_eleves'+'&'+$('#zone_placer').serialize(),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==false)
            {
              $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
            }
            else
            {
              modification = false;
              $.fancybox.close();
              var gridTemplateColumns = 'auto '.repeat(nb_colonnes);
              $('#table_action').hide(0);
              $('#gestion_placer_plan_nom').html( escapeHtml(nom) );
              $('#ajax_msg_placer').removeAttr('class').html('');
              $('#ordre_mode').find('option:first').prop('selected',true);
              $('#swap_hv').html(responseJSON['value']).css('grid-template-columns',gridTemplateColumns);
              $('#zone_placer').show(0);
            }
          }
        }
      );
    }

    // Actualisation des messages

    function modif_en_cours()
    {
      if(modification==false)
      {
        $('#fermer_zone_placer').attr('class','annuler').html('Annuler / Retour');
        modification = true;
        $('#ajax_msg_placer').attr('class','alerte').html('Penser à valider les modifications.');
      }
    }

    // Ne pas réagir si on ne fait que modifier le numéro d’ordre

    $('#swap_hv').on
    (
      'click',
      'input',
      function()
      {
        modif_en_cours();
        return false;
      }
    );

    // Événement ajouté pour le cas où l’on n’utilise que la touche tabulation.

    $('#swap_hv').on
    (
      'change',
      'input',
      function()
      {
        modif_en_cours();
      }
    );

    // Changement d’une sélection d’équipe

    $('#swap_hv').on
    (
      'change',
      'select',
      function()
      {
        var val_option = $(this).find('option:selected').val();
        if(val_option)
        {
          $(this).attr('class','e'+val_option);
          $(this).parent().children('input[name=f_role]').attr('class','e'+val_option);
        }
        else
        {
          $(this).removeAttr('class');
          $(this).parent().children('input[name=f_role]').attr('class','hide');
        }
        modif_en_cours();
      }
    );

    // Clic sur une vignette (sélection ou échange)

    $('#swap_hv').on
    (
      // Ajouter un écouteur sur l’événement "dbltap" ne semble pas utile : sous iPad "dblclick" est intercepté et l’ajout de "dbltap" engendre 2 appels.
      'dblclick',
      'li',
      function()
      {
        if( li_one_id === false )
        {
          // sélection d'une première vignette
          li_one_id = $(this).attr('id');
          li_one_div = $(this).children('div');
          $(this).css('background-color','#CCF');
        }
        else
        {
          li_two_id = $(this).attr('id');
          if( li_one_id == li_two_id )
          {
            // clic sur la même : on la 'déselectionne'
            $(this).css('background-color','#DDF');
          }
          else
          {
            // échange avec une seconde vignette
            li_two_div = $(this).children('div');
            // échange des numéros de place pour conserver un ordre de rangement déjà en place
            var input_one = li_one_div.children('input[name=f_ordre]');
            var input_two = li_two_div.children('input[name=f_ordre]');
            if( input_one.length && input_two.length )
            {
              var ordre_one = input_one.val();
              var ordre_two = input_two.val();
              input_one.val(ordre_two);
              input_two.val(ordre_one);
            }
            // échange des équipes et des rôles pour conserver les équipes aux mêmes places
            var select_one = li_one_div.children('select');
            var select_two = li_two_div.children('select');
            var input_one = li_one_div.children('input[name=f_role]');
            var input_two = li_two_div.children('input[name=f_role]');
            if( select_one.length && select_two.length )
            {
              var option_one = select_one.find('option:selected').val();
              var option_two = select_two.find('option:selected').val();
              var role_one = input_one.val();
              var role_two = input_two.val();
              if(option_two)
              {
                select_one.find('option[value='+option_two+']').prop('selected',true);
                select_one.attr('class','e'+option_two);
                input_one.val(role_two).attr('class','e'+option_two);
              }
              else
              {
                select_one.children('option:first').prop('selected',true);
                select_one.removeAttr('class');
                input_one.val(role_two).attr('class','hide');
              }
              if(option_one)
              {
                select_two.find('option[value='+option_one+']').prop('selected',true);
                select_two.attr('class','e'+option_one);
                input_two.val(role_one).attr('class','e'+option_one);
              }
              else
              {
                select_two.children('option:first').prop('selected',true);
                select_two.removeAttr('class');
                input_two.val(role_one).attr('class','hide');
              }
            }
            $('#'+li_one_id).html(li_two_div).css('background-color','#DDF');
            $('#'+li_two_id).html(li_one_div);
            modif_en_cours();
          }
          li_one_id = false;
          li_two_id = false;
        }
      }
    );

    // Fermer la zone

    function fermer_zone_placer()
    {
      $('#swap_hv').html('<li></li>');
      $(zone_placer).hide(0);
      $('#table_action').show(0);
      return false;
    }

    // Renuméroter automatiquement

    function appliquer_numero(rangee,colonne)
    {
      var obj_li  = $('#'+rangee+'x'+colonne);
      var obj_div = obj_li.children('div');
      if( obj_div.length )
      {
        obj_div.children('input[name=f_ordre]').val(ordre);
      }
      // On incrémente même s’il y a personne à cette place : cela évite un décalage en cas d’ajout d’élève.
      ordre++;
    }

    $('#zone_placer select').change
    (
      function()
      {
        var ordre_mode       = $('#ordre_mode'      ).val();
        var ordre_alternance = $('#ordre_alternance').val();
        var ordre_groupe     = $('#ordre_groupe'    ).val();
        var nb_rangees       = $('#placer_rangees'  ).val();
        var nb_colonnes      = $('#placer_colonnes' ).val();
        if(!ordre_mode)
        {
          return false;
        }
        var tab_modes = ordre_mode.split('_');
        var mode1 = tab_modes[0];
        var mode2 = tab_modes[1];
        if( (mode1=='l2r') || (mode2=='l2r') )
        {
          var colonne_start = 0;
          var colonne_end   = nb_colonnes-1;
          var colonne_incr  = 1;
        }
        else
        {
          var colonne_start = nb_colonnes-1;
          var colonne_end   = 0;
          var colonne_incr  = -1;
        }
        if( (mode1=='u2b') || (mode2=='u2b') )
        {
          var rangee_start = 0;
          var rangee_end   = nb_rangees-1;
          var rangee_incr  = 1;
        }
        else
        {
          var rangee_start = nb_rangees-1;
          var rangee_end   = 0;
          var rangee_incr  = -1;
        }
        ordre = 1;
        var rangee  = rangee_start;
        var colonne = colonne_start;
        while(ordre){
          var memo_rangee  = rangee;
          var memo_colonne = colonne;
          for( var i_groupe=1 ; i_groupe<=ordre_groupe ; i_groupe++ )
          {
            appliquer_numero(rangee,colonne);
            if(i_groupe!=ordre_groupe)
            {
              if( (mode1=='l2r') || (mode1=='r2l') )
              {
                if(rangee!=rangee_end)
                {
                  rangee += rangee_incr;
                }
                else
                {
                  break;
                }
              }
              else
              {
                if(colonne!=colonne_end)
                {
                  colonne += colonne_incr;
                }
                else
                {
                  break;
                }
              }
            }
          }
          rangee  = memo_rangee;
          colonne = memo_colonne;
          if( (mode1=='l2r') || (mode1=='r2l') )
          {
            if(colonne!=colonne_end)
            {
              colonne += colonne_incr;
            }
            else
            {
              if( Math.abs(rangee-rangee_end) >= ordre_groupe )
              {
                if(ordre_alternance=='1')
                {
                  if(mode1=='l2r')
                  {
                    mode1 = 'r2l'
                    colonne_start = nb_colonnes-1;
                    colonne_end   = 0;
                    colonne_incr  = -1;
                  }
                  else
                  {
                    mode1 = 'l2r'
                    colonne_start = 0;
                    colonne_end   = nb_colonnes-1;
                    colonne_incr  = 1;
                  }
                }
                rangee += rangee_incr*ordre_groupe;
                colonne = colonne_start;
              }
              else
              {
                ordre = 0;
              }
            }
          }
          else
          {
            if(rangee!=rangee_end)
            {
              rangee += rangee_incr;
            }
            else
            {
              if( Math.abs(colonne-colonne_end) >= ordre_groupe )
              {
                if(ordre_alternance=='1')
                {
                  if(mode1=='u2b')
                  {
                    mode1 = 'b2u'
                    rangee_start = nb_rangees-1;
                    rangee_end   = 0;
                    rangee_incr  = -1;
                  }
                  else
                  {
                    mode1 = 'u2b'
                    rangee_start = 0;
                    rangee_end   = nb_rangees-1;
                    rangee_incr  = 1;
                  }
                }
                rangee = rangee_start;
                colonne += colonne_incr*ordre_groupe;
              }
              else
              {
                ordre = 0;
              }
            }
          }
        }
        modif_en_cours();
      }
    );

    // Enregistrer le placement

    $('#valider_place').click
    (
      function()
      {
        if(modification==false)
        {
          $('#ajax_msg_placer').attr('class','alerte').html('Aucune modification effectuée !');
        }
        else
        {
          // On récupère les données : identifiant, placement, ordre, équipe, rôle
          var tab_eleve  = [];
          var tab_place  = [];
          var tab_ordre  = [];
          var tab_equipe = [];
          var tab_role   = [];
          $('#swap_hv').children('li').each
          (
            function()
            {
              var placement = $(this).attr('id');
              var obj_div   = $(this).children('div');
              if( obj_div.length )
              {
                var eleve  = obj_div.attr('id').substring(2); // id{num}
                var ordre  = obj_div.children('input[name=f_ordre]').val();
                var equipe = obj_div.children('select').find('option:selected').val();
                var role   = obj_div.children('input[name=f_role]').val();
                tab_eleve.push(eleve);
                tab_place.push(placement);
                tab_ordre.push(ordre);
                tab_equipe.push(equipe);
                tab_role.push(encodeURIComponent(role));
              }
            }
          );
          $('#zone_placer button').prop('disabled',true);
          $('#ajax_msg_placer').attr('class','loader').html('En cours&hellip;');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&f_action='+mode+'&'+$('#zone_placer').serialize()+'&tab_eleve='+tab_eleve+'&tab_place='+tab_place+'&tab_ordre='+tab_ordre+'&tab_equipe='+tab_equipe+'&tab_role='+tab_role.join('⁞'),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#zone_placer button').prop('disabled',false);
                $('#ajax_msg_placer').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                return false;
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                $('#zone_placer button').prop('disabled',false);
                if(responseJSON['statut']==false)
                {
                  $('#ajax_msg_placer').attr('class','alerte').html(responseJSON['value']);
                }
                else
                {
                  modification = false;
                  $('#ajax_msg_placer').attr('class','valide').html('Plan enregistré !');
                  $('#fermer_zone_placer').attr('class','retourner').html('Retour');
                }
              }
            }
          );
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Pour imprimer un plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_imprimer_equipe').change
    (
      function()
      {
        $('#span_role').hideshow( $(this).is(':checked') );
      }
    );

    function imprimer( id , groupe_id , nom , nb_rangees , nb_colonnes )
    {
      var groupe_type = $('#f_groupe option[value='+groupe_id+']').parent().attr('label');
      $('#imprimer_id'         ).val(id);
      $('#imprimer_nom'        ).val(nom);
      $('#imprimer_groupe'     ).val(groupe_id);
      $('#imprimer_groupe_type').val(groupe_type);
      $('#imprimer_rangees'    ).val(nb_rangees);
      $('#imprimer_colonnes'   ).val(nb_colonnes);
      // Afficher la zone associée
      $('#titre_imprimer').html( escapeHtml(nom) );
      $('#ajax_msg_imprimer').removeAttr('class').html('');
      $('#zone_imprimer_retour').html('');
      $.fancybox( { href:'#zone_imprimer' , modal:true , minWidth:600 } );
    };

    $('#valider_imprimer').click
    (
      function()
      {
        $('#zone_imprimer button').prop('disabled',true);
        $('#ajax_msg_imprimer').attr('class','loader').html('En cours&hellip;');
        $('#zone_imprimer_retour').html('');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+mode+'&'+$('#zone_imprimer').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#zone_imprimer button').prop('disabled',false);
              $('#ajax_msg_imprimer').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#zone_imprimer button').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_imprimer').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg_imprimer').attr('class','valide').html('Impression PDF générée !');
                $('#zone_imprimer_retour').html('<hr>'+responseJSON['value']+'<br><span class="noprint">Afin de préserver l’environnement, n’imprimer que si nécessaire !</span>');
              }
            }
          }
        );
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonctions utilisées
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_form_gestion( mode , id , groupe_id , nom , nb_rangees , nb_colonnes )
    {
      $('#f_action').val(mode);
      $('#f_id').val(id);
      $('#f_groupe option[value='+groupe_id+']').prop('selected',true);
      $('#f_nom').val(nom);
      $('#f_rangees').val(nb_rangees);
      $('#f_colonnes').val(nb_colonnes);
      // pour finir
      $('#form_gestion').find('h2').html( mode[0].toUpperCase() + mode.substring(1) + ' un plan de classe' );
      $('#gestion_delete_plan_nom').html( escapeHtml(nom) );
      $('#gestion_edit').hideshow( mode != 'supprimer' );
      $('#gestion_delete').hideshow( mode == 'supprimer' );
      $('#ajax_msg_gestion').removeAttr('class').html('');
      $('#form_gestion label[generated=true]').removeAttr('class').html('');
      $.fancybox( { href:'#form_gestion' , modal:true , minWidth:600 } );
    }

    /**
     * Ajouter un plan de classe : mise en place du formulaire
     * @return void
     */
    var ajouter = function()
    {
      mode = $(this).attr('class');
      // Afficher le formulaire
      afficher_form_gestion( mode , 0 /*id*/ , 0 /*groupe_id*/ , '' /*nom*/ , 5 /*nb_rangees*/ , 6 /*nb_colonnes*/ );
    };

    /**
     * Dupliquer | Modifier | Supprimer un plan de classe : mise en place du formulaire
     * @return void
     */
    var dupliquer_modifier_supprimer_placer_imprimer = function()
    {
      mode = $(this).attr('class');
      var objet_tr    = $(this).parent().parent();
      var objet_tds   = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var id          = objet_tr.attr('id').substring(3);
      var groupe_id   = objet_tds.eq(0).data('id');
      var nom         = objet_tds.eq(1).html();
      var nb_rangees  = objet_tds.eq(2).html();
      var nb_colonnes = objet_tds.eq(3).html();
      // Afficher le formulaire
      if(mode=='placer_eleves')
      {
        charger_plan_classe( id , groupe_id , unescapeHtml(nom) , nb_rangees , nb_colonnes );
      }
      else if(mode=='imprimer')
      {
        imprimer( id , groupe_id , unescapeHtml(nom) , nb_rangees , nb_colonnes );
      }
      else
      {
        afficher_form_gestion( mode , id , groupe_id , unescapeHtml(nom) , nb_rangees , nb_colonnes );
      }
   };

    var importer_plan = function()
    {
      // Afficher le formulaire
      $('#ajax_msg_import').removeAttr('class').html('');
      $('#form_importer label[generated=true]').removeAttr('class').html('');
      $.fancybox( { href:'#form_importer' , modal:true , minWidth:1000 } );
    };

    /**
     * Annuler une action
     * @return void
     */
    var annuler = function()
    {
      $.fancybox.close();
      mode = false;
    };

    /**
     * Intercepter la touche entrée ou escape pour valider ou annuler les modifications
     * @return void
     */
    function intercepter(e)
    {
      if(mode)
      {
        if(e.which==13)  // touche entrée
        {
          $('#bouton_valider').click();
        }
        else if(e.which==27)  // touche escape
        {
          $('#bouton_annuler').click();
        }
      }
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Appel des fonctions en fonction des événements
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on( 'click' , 'q.ajouter'       , ajouter );
    $('#table_action').on( 'click' , 'q.importer_plan' , importer_plan );
    $('#table_action').on( 'click' , 'q.modifier'      , dupliquer_modifier_supprimer_placer_imprimer );
    $('#table_action').on( 'click' , 'q.dupliquer'     , dupliquer_modifier_supprimer_placer_imprimer );
    $('#table_action').on( 'click' , 'q.supprimer'     , dupliquer_modifier_supprimer_placer_imprimer );
    $('#table_action').on( 'click' , 'q.placer_eleves' , dupliquer_modifier_supprimer_placer_imprimer );
    $('#table_action').on( 'click' , 'q.imprimer'      , dupliquer_modifier_supprimer_placer_imprimer );

    $('#form_gestion' ).on( 'click'   , '#bouton_annuler'       , annuler );
    $('#form_importer').on( 'click'   , '#bouton_annuler_import', annuler );
    $('#zone_imprimer').on( 'click'   , '#fermer_zone_imprimer' , annuler );
    $('#zone_placer'  ).on( 'click'   , '#fermer_zone_placer'   , fermer_zone_placer );
    $('#form_gestion' ).on( 'click'   , '#bouton_valider' , function(){formulaire.submit();} );
    $('#form_gestion' ).on( 'keydown' , 'input'           , function(e){intercepter(e);} );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_gestion');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_groupe   : { required:true },
          f_nom      : { required:true , maxlength:40 },
          f_rangees  : { required:true, min:2, max:15 },
          f_colonnes : { required:true, min:2, max:15 }
        },
        messages :
        {
          f_groupe   : { required:'regroupement manquant' },
          f_nom      : { required:'nom manquant' , maxlength:'40 caractères maximum' },
          f_rangees  : { required:'nombre manquant', min:'2 minimum', max:'15 maximum' },
          f_colonnes : { required:'nombre manquant', min:'2 minimum', max:'15 maximum' }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element) { element.after(error); }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        $('#f_groupe_type').val( $('#f_groupe option:selected').parent().attr('label') );
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg_gestion').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        $('#form_gestion button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      $('#form_gestion button').prop('disabled',false);
      $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      $('#form_gestion button').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
        var action = $('#f_action').val();
        switch (action)
        {
          case 'ajouter':
          case 'modifier':
          case 'dupliquer':
            var groupe_nom  = $('#f_groupe option:selected').text();
            responseJSON['value'] = responseJSON['value'].replace('{{GROUPE_NOM}}',groupe_nom);
            if(action=='modifier')
            {
              $('#id_'+$('#f_id').val()).addClass('new').html(responseJSON['value']);
            }
            else
            {
              if(action=='ajouter')
              {
                $('#table_action tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
              }
              $('#table_action tbody').append(responseJSON['value']);
            }
            break;
          case 'supprimer':
            $('#id_'+$('#f_id').val()).remove();
            break;
        }
        tableau_maj();
        $.fancybox.close();
        mode = false;
      }
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Pour importer le plan d’un(e) collègue
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_groupe_import').change
    (
      function()
      {
        $('#ul_import').html('<li></li>').hide();
        var groupe_id = $(this).val();
        if( groupe_id )
        {
          var groupe_type = $('#f_groupe_import option[value='+groupe_id+']').parent().attr('label');
          $('#f_groupe_type_import').val(groupe_type);
          // Charger la liste des plans des collègues disponibles pour le regroupement choisi
          $('#ajax_msg_import').attr('class','loader').html('En cours&hellip;');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&f_action=chercher_plans_collegues'+'&'+$('#form_importer').serialize(),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#ajax_msg_import').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                return false;
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                if(responseJSON['statut']==false)
                {
                  $('#ajax_msg_import').attr('class','alerte').html(responseJSON['value']);
                }
                else
                {
                  $('#ajax_msg_import').removeAttr('class').html('');
                  $('#f_import_id').html(responseJSON['value']);
                  if( responseJSON['value'].indexOf(' disabled') !== -1 )
                  {
                    $('#ajax_msg_import').attr('class','alerte').html('Aucun plan de collègue trouvé !');
                  }
                  else if( responseJSON['value'].indexOf(' selected') !== -1 )
                  {
                    $('#ajax_msg_import').attr('class','valide').html('Un plan trouvé.');
                    previsualiser_plan_collegue();
                  }
                  else if( responseJSON['value'].indexOf(' selected') !== -1 )
                  {
                    $('#ajax_msg_import').attr('class','valide').html('Plusieurs plans trouvés.');
                  }
                }
              }
            }
          );
        }
        else
        {
          $('#f_import_id').html('<option value="" disabled>Sélectionner un regroupement.</option>');
          $('#bouton_importer').prop('disabled',true);
          $('#ajax_msg_import').removeAttr('class').html('');
        }
      }
    );

    $('#f_import_id').change
    (
      function()
      {
        if( $(this).val() )
        {
          previsualiser_plan_collegue();
        }
        else
        {
          $('#ul_import').html('<li></li>').hide();
          $('#bouton_importer').prop('disabled',true);
          $('#ajax_msg_import').removeAttr('class').html('');
        }
      }
    );

    function previsualiser_plan_collegue()
    {
      $('#ajax_msg_import').attr('class','loader').html('En cours&hellip;');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=previsualiser_plan_collegue'+'&'+$('#form_importer').serialize(),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_msg_import').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==false)
            {
              $('#ajax_msg_import').attr('class','alerte').html(responseJSON['value']);
            }
            else
            {
              $('#ajax_msg_import').removeAttr('class').html('');
              $('#bouton_importer').prop('disabled',false);
              var gridTemplateColumns = 'auto '.repeat(responseJSON['nb_colonnes']);
              $('#ul_import').html(responseJSON['li']).css('grid-template-columns',gridTemplateColumns).show();
            }
          }
        }
      );
    }

    $('#bouton_importer').click
    (
      function()
      {
        $('#ajax_msg_import').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=importer_plan_collegue'+'&'+$('#form_importer').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg_import').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_import').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg_import').attr('class','valide').html('Demande réalisée !');
                var groupe_nom  = $('#f_groupe_import option:selected').text();
                responseJSON['value'] = responseJSON['value'].replace('{{GROUPE_NOM}}',groupe_nom);
                $('#table_action tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
                $('#table_action tbody').append(responseJSON['value']);
                tableau_maj();
                $.fancybox.close();
              }
            }
          }
        );
      }
    );

  }
);
