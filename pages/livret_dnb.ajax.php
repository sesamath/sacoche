<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if(($_SESSION['SESAMATH_ID']==ID_DEMO)&&($_POST['f_action']!='initialiser')){Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action = Clean::post('f_action', 'texte');
$datas  = Clean::post('f_data'  , 'texte');

$annee_siecle = To::annee_scolaire('siecle');
if($annee_siecle==2019)
{
  $tab_epreuves = array(
    'fr'       => 'Contrôle continu<br>français',
    'maths'    => 'Contrôle continu<br>mathématiques',
    'hgemc'    => 'Contrôle continu<br>histoire-géographie EMC',
    'sciences' => 'Contrôle continu<br>sciences',
  );
}
else
{
  $tab_epreuves = array(
    'oral'     => 'Épreuve orale<br>soutenance d’un projet',
    'fr'       => 'Épreuve écrite<br>français',
    'maths'    => 'Épreuve écrite<br>mathématiques',
    'hgemc'    => 'Épreuve écrite<br>histoire-géographie EMC',
    'sciences' => 'Épreuve écrite<br>sciences',
  );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Enregistrer des saisies
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='enregistrer') && $datas )
{
  // Récupération des données saisies
  $tab_saisies = explode('_',$datas);
  foreach($tab_saisies as $saisie_infos)
  {
    list( $objet , $eleve_id , $epreuve , $note ) = explode('.',$saisie_infos);
    $eleve_id = (int)$eleve_id;
    $max = ( ( $epreuve != 'hgemc' ) && ( $epreuve != 'sciences' ) ) ? 100 : 50 ;
    $note     = ( $objet == 'ajouter' ) ? min( $max , (int)$note ) : NULL ;
    if( ( ( $objet != 'ajouter' ) && ( $objet != 'supprimer' ) ) || !$eleve_id || !isset($tab_epreuves[$epreuve]) )
    {
      Json::end( FALSE , 'Anomalie avec la donnée transmise "'.$saisie_infos.'" !' );
    }
    DB_STRUCTURE_LIVRET::DB_modifier_note_dnb( $eleve_id , $epreuve , $note );
  }
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
