<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Évaluer un élève à la volée'));

$tab_matieres = DB_STRUCTURE_COMMUN::DB_OPT_matieres_professeur($_SESSION['USER_ID']);
$tab_groupes  = ($_SESSION['USER_JOIN_GROUPES']=='config')
              ? DB_STRUCTURE_COMMUN::DB_OPT_groupes_professeur( $_SESSION['USER_ID'] , TRUE /*with_niveau*/ )
              : DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl( TRUE /*with_niveau*/ ) ;

$select_groupe  = HtmlForm::afficher_select($tab_groupes  , 'f_classe'  /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ , 'regroupements' /*optgroup*/ );
$select_matiere = HtmlForm::afficher_select($tab_matieres , 'f_matiere' /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ ,              '' /*optgroup*/ );

// boutons radio
$tab_notes = array_merge( $_SESSION['NOTE_ACTIF'] , array( 'NN' , 'NE' , 'NF' , 'NR' , 'AB' , 'DI' , 'PA' , 'X' ) );
$tab_radio_boutons = array();
foreach($tab_notes as $note)
{
  $tab_radio_boutons[] = '<label for="note_'.$note.'" class="note"><input type="radio" id="note_'.$note.'" name="f_note" value="'.$note.'"><img alt="'.$note.'" src="'.Html::note_src($note).'"></label>';
}
$radio_boutons = implode(' ',$tab_radio_boutons);

// Élément de formulaire "f_plan"
$tab_plans = DB_STRUCTURE_PROFESSEUR_PLAN::DB_OPT_lister_plans_prof_groupe( $_SESSION['USER_ID'] , TRUE /*msg_si_rien*/ );
$select_plans = HtmlForm::afficher_select($tab_plans , 'f_plan' /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ ,  '' /*optgroup*/ );

// Alerte initialisation annuelle non effectuée (test !empty() car un passage par la page d’accueil n’est pas obligatoire)
if(!empty($_SESSION['NB_DEVOIRS_ANTERIEURS']))
{
  echo'<p class="probleme">Année scolaire précédente non archivée&nbsp;!<br>Au changement d’année scolaire un administrateur doit <span class="manuel"><a class="pop_up" href="'.SERVEUR_DOCUMENTAIRE.'?fichier=support_administrateur__gestion_nettoyage#toggle_initialisation_annuelle">lancer l’initialisation annuelle des données</a></span>.<br>Ne poursuivez pas tant que cela n’est pas fait&nbsp;!</p><hr>';
}

// Ajout exceptionnel de css
Layout::add( 'css_inline' , '#bloc_resultat {margin-left:11em}' );

?>

<div><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_professeur__evaluations_ponctuelles">DOC : Évaluer un élève à la volée.</a></span></div>

<hr>

<form action="#" method="post" id="form_select"><fieldset>
  <p>
    <label class="tab">Intitulé :</label><input id="box_autodescription" name="box_autodescription" type="checkbox" value="1" checked> <label for="box_autodescription">automatique</label><span class="hide"><input id="f_description" name="f_description" type="text" value="" size="50" maxlength="60"></span>
  </p>
  <p>
  <label class="tab" for="f_matiere">Matière :</label><?php echo $select_matiere ?><label id="ajax_maj_matiere">&nbsp;</label><br>
  <span id="bloc_niveau" class="hide"><label class="tab" for="f_niveau">Niveau :</label><select id="f_niveau" name="f_niveau"><option>&nbsp;</option></select><label id="ajax_maj_niveau">&nbsp;</label><br></span>
  <span id="bloc_item" class="hide"><label class="tab" for="f_item">Item :</label><select id="f_item" name="f_item"><option>&nbsp;</option></select></span>
  </p>
  <div>
    <label class="tab">Présentation :</label><label for="f_presentation_formulaire"><input type="radio" id="f_presentation_formulaire" name="f_presentation" value="formulaire"> Formulaire</label>&nbsp;&nbsp;&nbsp;<label for="f_presentation_plan"><input type="radio" id="f_presentation_plan" name="f_presentation" value="plan"> Plan de classe</label><br>
  </div>
  <div id="zone_regroupement" class="hide">
    <label class="tab" for="f_classe">Classe / groupe :</label><?php echo $select_groupe ?><label id="ajax_maj_groupe">&nbsp;</label><br>
    <span id="bloc_eleve" class="hide"><label class="tab" for="f_eleve">Élève :</label><select id="f_eleve" name="f_eleve"><option>&nbsp;</option></select></span>
  </div>
  <div id="zone_plans" class="hide">
    <label class="tab" for="f_plan">Plans :</label><?php echo $select_plans ?><label id="ajax_maj_plan">&nbsp;</label><br>
  </div>
  <div id="zone_resultat" class="hide p">
    <div class="toggle">
      <span class="tab"></span><a href="#" id="voir_resultats" class="puce_plus toggle">Voir les résultats des élèves pour cet item.</a>
    </div>
    <div class="toggle hide">
      <span class="tab"></span><a href="#" id="masquer_resultats" class="puce_moins toggle">Masquer les résultats des élèves pour cet item.</a><br>
      <div id="bloc_resultat"></div>
    </div>
  </div>
  <div id="zone_formulaire_validation" class="p hide">
    <label class="tab">Note :</label><?php echo str_replace('note_','note_form_',$radio_boutons); ?>
    <input id="f_devoir" name="f_devoir" type="hidden" value="0">
    <input id="f_groupe" name="f_groupe" type="hidden" value="0">
    <p>
      <span class="tab"></span><button id="bouton_valider_formulaire" type="button" class="valider">Enregistrer.</button><label id="ajax_msg_enregistrement_formulaire">&nbsp;</label>
    </p>
  </div>
  <div id="zone_plan_classe" class="hide">
    <ul id="ul_plan">
      <li></li>
    </ul>
  </div>
</fieldset></form>

<form id="zone_saisie_plan" class="hide">
  <div><label class="tab">Référentiel :</label><span id="report_referentiel">XXX</span></div>
  <div><label class="tab">Item :</label><span id="report_item">XXX</span></div>
  <div><label class="tab">Élève :</label><span id="report_eleve">XXX</span></div>
  <p id="p_saisie_plan"><label class="tab">Note :</label><?php echo str_replace('note_','note_plan_',$radio_boutons); ?></p>
  <p>
    <input id="f_plan_box" name="box_autodescription" type="hidden" value="0">
    <input id="f_plan_description" name="f_description" type="hidden" value="0">
    <input id="f_plan_eleve" name="f_eleve" type="hidden" value="0">
    <input id="f_plan_item" name="f_item" type="hidden" value="0">
    <input id="f_plan_devoir" name="f_devoir" type="hidden" value="0">
    <input id="f_plan_groupe" name="f_groupe" type="hidden" value="0">
    <span class="tab"></span><button id="bouton_valider_saisie_plan" type="button" class="valider">Enregistrer.</button><label id="ajax_msg_enregistrement_saisie_plan">&nbsp;</label>
  </p>
</form>

<div id="div_lien_eval" class="hide">
  <ul class="puce">
    <li>Une fois toutes vos notes saisies, vous pouvez <a id="lien_eval" href="./index.php?page=evaluation&amp;section=gestion_selection&amp;devoir_id=0&amp;groupe_type=E&amp;groupe_id=0">voir l’évaluation correspondante ainsi obtenue</a>.</li>
  </ul>
</div>

<div id="div_resultats"></div>
