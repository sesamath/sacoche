<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
// Pas de test d’établissement de démo ici mais dans les sous-fichiers, sauf dans 2 cas précis
if( ($_SESSION['SESAMATH_ID']==ID_DEMO) && !empty($_POST['f_action']) && in_array($_POST['f_action'],array('signaler_faute','corriger_faute')) ) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action  = Clean::post('f_action' , 'texte');
$section = Clean::post('f_section', 'texte');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Signaler une faute ou la correction d’une faute
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( !$section && ( ($action=='signaler_faute') || ($action=='corriger_faute') ) )
{
  $destinataire_id  = Clean::post('f_destinataire_id' , 'entier');
  $message_contenu  = Clean::post('f_message_contenu' , 'texte');
  $appreciation_new = Clean::post('f_appreciation'    , 'texte');
  $appreciation_old = Clean::post('f_appreciation_old', 'texte');
  if( !$destinataire_id || !$message_contenu || !$appreciation_old || ( ($action=='corriger_faute') && !$appreciation_new ) )
  {
    Json::end( FALSE , 'Erreur avec les données transmises !' );
  }
  if( ($action=='corriger_faute') && ( $appreciation_old == $appreciation_new ) )
  {
    Json::end( FALSE , 'Aucune différence trouvée avec l’appréciation déjà saisie !' );
  }
  // Notification (qui est envoyée de suite)
  $abonnement_ref = 'bilan_officiel_appreciation';
  $DB_TAB = DB_STRUCTURE_NOTIFICATION::DB_lister_destinataires_avec_informations( $abonnement_ref , $destinataire_id );
  $destinataires_nb = count($DB_TAB);
  if(!$destinataires_nb)
  {
    // Normalement impossible, l’abonnement des personnels à ce type de de notification étant obligatoire
    Json::end( FALSE , 'Destinataire non trouvé !' );
  }
  if($action=='corriger_faute')
  {
    $opcodes = FineDiff::getDiffOpcodes($appreciation_old, $appreciation_new, FineDiff::$wordGranularity );
    $message_contenu .= EOML.EOML.'MODIFICATION(S) :'
                      . EOML.html_entity_decode(FineDiff::renderDiffToHTMLFromOpcodes($appreciation_old, $opcodes))
                      . EOML.EOML.'NOUVELLE APPRÉCIATION :'
                      . EOML.$appreciation_new
                      . EOML.EOML.'ANCIENNE APPRÉCIATION :'
                      . EOML.$appreciation_old;
  }
  elseif($action=='signaler_faute')
  {
    $message_contenu .= EOML.EOML.'APPRÉCIATION ACTUELLE :'
                      . EOML.$appreciation_old;
  }
  $notification_debut = ($action=='signaler_faute') ? 'Signalement effectué par ' : 'Correction apportée par ' ;
  $notification_contenu = $notification_debut.To::texte_genre_identite($_SESSION['USER_NOM'],FALSE,$_SESSION['USER_PRENOM'],TRUE,$_SESSION['USER_GENRE']).' :'.EOML.EOML.$message_contenu.EOML;
  foreach($DB_TAB as $DB_ROW)
  {
    // 1 seul passage en fait
    $notification_statut = ( (COURRIEL_NOTIFICATION=='oui') && ($DB_ROW['jointure_mode']=='courriel') && $DB_ROW['user_email'] ) ? 'envoyée' : 'consultable' ;
    DB_STRUCTURE_NOTIFICATION::DB_ajouter_log_visible( $DB_ROW['user_id'] , $abonnement_ref , $notification_statut , $notification_contenu );
    if($notification_statut=='envoyée')
    {
      $destinataire = $DB_ROW['user_prenom'].' '.$DB_ROW['user_nom'].' <'.$DB_ROW['user_email'].'>';
      $notification_contenu .= Sesamail::texte_pied_courriel( array('no_reply','notif_individuelle','unsubscribe','signature') , $DB_ROW['user_email'] , $DB_ROW['user_id'] , $DB_ROW['user_password'] );
      $courriel_bilan = Sesamail::mail( $destinataire , 'Notification - Erreur appréciation livret scolaire' , $notification_contenu , NULL );
    }
  }
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Récupération des valeurs transmises
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$OBJET            = Clean::post('f_objet'        , 'texte');
$ACTION           = Clean::post('f_action'       , 'texte');
$mode             = Clean::post('f_mode'         , 'texte');
$PAGE_REF         = Clean::post('f_page_ref'     , 'texte');
$periode          = Clean::post('f_periode'      , 'texte');
$classe_id        = Clean::post('f_classe'       , 'entier');
$groupe_id        = Clean::post('f_groupe'       , 'entier');
$eleve_id         = Clean::post('f_user'         , 'entier');
$saisie_id        = Clean::post('f_saisie_id'    , 'entier');
$rubrique_type    = Clean::post('f_rubrique_type', 'texte');
$rubrique_id      = Clean::post('f_rubrique_id'  , 'entier');
$saisie_objet     = Clean::post('f_saisie_objet' , 'texte');
$page_colonne     = Clean::post('f_page_colonne' , 'texte');
$prof_id          = Clean::post('f_prof'         , 'entier'); // id du prof dont on corrige l’appréciation
$appreciation     = Clean::post('f_appreciation' , 'appreciation');
$elements         = Clean::post('f_elements'     , 'appreciation');
$position         = Clean::post('f_position'     , 'decimal');
$import_info      = Clean::post('f_import_info'  , 'texte');
$etape            = Clean::post('f_etape'        , 'entier');
$notification     = Clean::post('f_notification' , 'entier');
$date_visible_sql = Clean::post('f_date_visible' , 'date_sql');

$is_sous_groupe = ($groupe_id) ? TRUE : FALSE ;

// Tableaux communs utiles

$tab_periode_livret = array(
  'periode21' => 'Semestre 1/2' ,
  'periode22' => 'Semestre 2/2' ,
  'periode31' => 'Trimestre 1/3',
  'periode32' => 'Trimestre 2/3',
  'periode33' => 'Trimestre 3/3',
  'periode41' => 'Bimestre 1/4' ,
  'periode42' => 'Bimestre 2/4' ,
  'periode43' => 'Bimestre 3/4' ,
  'periode44' => 'Bimestre 4/4' ,
  'periode51' => 'Période 1/5'  ,
  'periode52' => 'Période 2/5'  ,
  'periode53' => 'Période 3/5'  ,
  'periode54' => 'Période 4/5'  ,
  'periode55' => 'Période 5/5'  ,
  'cycle0'    => 'Fin de cycle' ,
);

// Vérification période + extraction des infos sur la période

if( !isset($tab_periode_livret[$periode]) )
{
  Json::end( FALSE , 'Période "'.html($periode).'" inconnue !' );
}
if(substr($periode,0,7)=='periode')
{
  $PAGE_PERIODICITE = 'periode';
  $JOINTURE_PERIODE = substr($periode,7);
  $DB_ROW = DB_STRUCTURE_LIVRET::DB_recuperer_periode_info( $JOINTURE_PERIODE , $classe_id );
  if(empty($DB_ROW))
  {
    Json::end( FALSE , 'Jointure période/classe transmise indéfinie !' );
  }
  $periode_id     = $DB_ROW['periode_id'];
  $date_sql_debut = $DB_ROW['jointure_date_debut'];
  $date_sql_fin   = $DB_ROW['jointure_date_fin'];
  $date_debut = To::date_sql_to_french($date_sql_debut);
  $date_fin   = To::date_sql_to_french($date_sql_fin);
}
else
{
  $PAGE_PERIODICITE = 'cycle';
  $JOINTURE_PERIODE = 0;
  $periode_id       = 0;
  $date_sql_debut = NULL;
  $date_sql_fin   = NULL;
  $date_debut = '';
  $date_fin   = '';
}
$periode_nom = $tab_periode_livret[$periode];

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Saisir    : affichage des données d’un élève | enregistrement/suppression d’une appréciation ou d’une note ou d’éléments ou d’un rattachement | recalculer une note
// Examiner  : recherche des saisies manquantes (notes et appréciations)
// Consulter : affichage des données d’un élève (HTML) | détail d’une rubrique pour un élève
// Imprimer  : affichage de la liste des élèves | étape d’impression PDF
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( in_array( $section , array('livret_saisir','livret_saisir_multiple','livret_examiner','livret_consulter','livret_imprimer','livret_importer') ) )
{
  if( ($section=='livret_consulter') && ($action=='imprimer') )
  {
    // Il s’agit d’un test d’impression d’un bilan non encore clos (on vérifiera quand même par la suite que les conditions sont respectées (état du bilan, droit de l’utilisateur)
    $section = 'livret_imprimer';
    $OBJET = 'imprimer';
    $is_test_impression = TRUE;
  }
  require(CHEMIN_DOSSIER_INCLUDE.'fonction_livret.php');
  require(CHEMIN_DOSSIER_INCLUDE.'code_'.$section.'.php');
  // Normalement, on est stoppé avant.
  Json::end( FALSE , 'Problème de code : point d’arrêt manquant !' );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Générer un archivage des saisies
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$tab_actions = array
(
  'imprimer_donnees_eleves_prof'             => 'Mes appréciations pour chaque élève et le groupe classe',
  'imprimer_donnees_eleves_collegues'        => 'Appréciations des collègues pour chaque élève',
  'imprimer_donnees_classe_collegues'        => 'Appréciations des collègues sur le groupe classe',
  'imprimer_donnees_eleves_syntheses'        => 'Appréciations de synthèse générale pour chaque élève',
  'imprimer_donnees_eleves_positionnements'  => 'Tableau des positionnements pour chaque élève',
  'imprimer_donnees_eleves_recapitulatif'    => 'Récapitulatif annuel des positionnements et appréciations par élève',
  'imprimer_donnees_eleves_mentions'         => 'Récapitulatif annuel des mentions par élève',
  'imprimer_donnees_eleves_affelnet'         => 'Récapitulatif des points calculés pour saisie dans Affelnet si hors LSU',
  'imprimer_donnees_eleves_socle_maitrise'   => 'Tableau des positionnements sur le socle pour chaque élève',
  'imprimer_donnees_eleves_socle_points_dnb' => 'Tableau des points du brevet pour chaque élève',
);

if( ($section=='livret_archiver') && isset($tab_actions[$action]) )
{
  require(CHEMIN_DOSSIER_INCLUDE.'fonction_livret.php');
  require(CHEMIN_DOSSIER_INCLUDE.'code_livret_archiver.php');
  // Normalement, on est stoppé avant.
  Json::end( FALSE , 'Problème de code : point d’arrêt manquant !' );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Il se peut que rien n’ait été récupéré à cause de l’upload d’un fichier trop lourd
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if(empty($_POST))
{
  Json::end( FALSE , 'Aucune donnée reçue ! Fichier trop lourd ? '.InfoServeur::minimum_limitations_upload() );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
