/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{2:{sorter:false},3:{sorter:false}} });
    var tableau_tri = function(){ $('#table_action').trigger( 'sorton' , [ [[0,0],[1,0]] ] ); };
    var tableau_maj = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    tableau_tri();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Alerter au changement d’un élément de formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_select').on
    (
      'change',
      'select, input',
      function()
      {
        $('#ajax_msg').attr('class','alerte').html('Pensez à valider vos modifications !');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger le select f_eleve en ajax
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_eleve(groupe_id,groupe_type)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eleves',
          data : 'f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_statut=1'+'&f_multiple=1'+'&f_filter=1'+'&f_selection=0',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_msg').attr('class','valide').html('Affichage actualisé !');
              $('#ajax_eleves').html(responseJSON['value']);
            }
            else
            {
              $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }
    function changer_groupe()
    {
      $('#ajax_eleves').html('');
      var groupe_val = $('#select_groupe').val();
      if(groupe_val)
      {
        var groupe_type = $('#select_groupe option:selected').parent().attr('label');
        var groupe_id = groupe_val;
        $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
        maj_eleve(groupe_id,groupe_type);
      }
      else
      {
        $('#ajax_msg').removeAttr('class').html('');
      }
    }
    $('#select_groupe').change
    (
      function()
      {
        changer_groupe();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Soumission du premier formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_associer').click
    (
      function()
      {
        var nb_eleves = $('#f_eleve input:checked:enabled').length;
        if(!nb_eleves)
        {
          $('#ajax_msg').attr('class','erreur').html('Sélectionnez au moins un élève !');
          return false;
        }
        $('#form_select button').prop('disabled',true);
        $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
        // Grouper les checkbox dans un champ unique afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
        var tab_eleve = [];
        $('#f_eleve input:checked:enabled').each
        (
          function()
          {
            tab_eleve.push($(this).val());
          }
        );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=associer'+'&f_eleve='+tab_eleve+'&only_groupes_id='+window.only_groupes_id,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#form_select button').prop('disabled',false);
              $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_select button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#ajax_msg').attr('class','valide').html('Demande réalisée !');
                $('#table_action tbody').html(responseJSON['html']);
                tableau_maj();
              }
              else
              {
                $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Intercepter la touche entrée car dans le cas d’un seul champ input de type text la soumission est sinon incontrôlée
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_select').submit
    (
      function()
      {
        $('#bouton_associer').click();
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation : charger le tableau
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    if( typeof(window.only_groupes_id) !== 'undefined' )
    {
      $('#ajax_msg').addClass('loader').html('En cours&hellip;');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=initialiser'+'&only_groupes_id='+window.only_groupes_id,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_msg').removeAttr('class').html('');
              $('#table_action tbody').prepend(responseJSON['html']);
              tableau_maj();
            }
            else
            {
              $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Modifier les périodes pour un élève
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'q.date_periode',
      function()
      {
        var objet_tr   = $(this).parent().parent();
        var objet_tds  = objet_tr.find('td');
        // Récupérer les informations de la ligne concernée
        var f_id          = objet_tr.attr('id').substring(3);
        var user_identite = objet_tds.eq(1).html();
        var periodicite   = objet_tds.eq(2).data('periodicite');
        var periodes      = objet_tds.eq(2).data('periodes');
        $('#periode_report_id').val(f_id);
        $('#memo_periodicite').val(periodicite);
        $('#periode_eleve_information').html(user_identite);
        var p_periodes = '';
        periodicite = entier(periodicite);
        for( var periode_num = 1 ; periode_num <= periodicite ; periode_num++ )
        {
          var periode_indice = periodicite*10 + periode_num;
          var checked = ( periodes.indexOf(periode_indice) !== -1 ) ? ' checked="checked"' : '' ;
          p_periodes += '<label for="periode'+periode_indice+'"><input type="checkbox" name="f_periodes[]" id="periode'+periode_indice+'" value="'+periode_indice+'"'+checked+'> '+window.tab_periodes[periode_indice]+'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
        $('#p_periodes').html(p_periodes);
        $('#ajax_msg_periode').removeAttr('class').html('');
        $.fancybox( { href:'#form_periode' , modal:true , minWidth:600 } );
      }
    );

    $('#form_periode').on
    (
      'click',
      '#bouton_annuler_periode',
      function()
      {
        $.fancybox.close();
      }
    );

    $('#form_periode').on
    (
      'click',
      '#bouton_valider_periode',
      function()
      {
        if( !$('#p_periodes').find('input:checked').length )
        {
          $('#ajax_msg_periode').attr('class','erreur').html('Au moins une période doit être cochée (sinon, supprimer le dispositif).');
          return false;
        }
        $('#bouton_valider_periode').prop('disabled',true);
        $('#ajax_msg_periode').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&'+$('#form_periode').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#bouton_valider_periode').prop('disabled',false);
              $('#ajax_msg_periode').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#bouton_valider_periode').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                var f_id = $('#periode_report_id').val();
                $('#ajax_msg_periode').attr('class','valide').html('Choix enregistrés !');
                $('#id_'+f_id).find('td').eq(2).replaceWith(responseJSON['value']);
                $.fancybox.close();
              }
              else
              {
                $('#ajax_msg_periode').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Suppression d’un dispositif
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'q.supprimer',
      function()
      {
        var objet_tr = $(this).parent().parent();
        var f_id     = objet_tr.attr('id').substring(3);
        objet_tr.hide();
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=retirer'+'&f_id='+f_id,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              objet_tr.show();
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==true)
              {
                objet_tr.remove();
              }
              else
              {
                objet_tr.show();
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
            }
          }
        );
      }
    );

  }
);
