<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Données personnelles'));
?>

<?php if($_SESSION['USER_PROFIL_TYPE']!='administrateur'): /* HORS ADMINISTRATEURS */ ?>

<?php
if(isset($_SESSION['STOP_CNIL']))
{
  $form_activation = '<h2>'.html(Lang::_('Activation de votre compte')).'</h2>';
  $form_activation.= '<p>';
  $form_activation.= '  <span class="tab"></span><input type="checkbox" id="confirmation_cnil" name="confirmation_cnil" value="1"><label for="confirmation_cnil"> '.html(Lang::_('J’ai pris connaissance des informations relatives à mes données personnelles.')).'</label><br>';
  $form_activation.= '  <span class="tab"></span><button id="f_enregistrer" type="button" class="valider" disabled>Valider.</button><label id="ajax_msg_enregistrer">&nbsp;</label>';
  $form_activation.= '</p>';
}
else
{
  $form_activation = '<h2>'.html(Lang::_('Votre compte est activé')).'</h2>';
  $form_activation.= '<p><label class="valide">'.html(Lang::_('J’ai pris connaissance des informations relatives à mes données personnelles.')).'</label></p>';
}
?>

<p class="astuce">
  Veuillez prendre connaissance des <a target="_blank" rel="noopener noreferrer" href="<?php echo SERVEUR_CNIL ?>">informations <em>CNIL / RGPD</em> relatives à l’application <em>SACoche</em></a>.
</p>
<p>
  Sont précisés en particulier :
</p>
<ul class="puce">
  <li>la nature des données enregistrées</li>
  <li>la durée de conservation de ces données</li>
  <li>les mesures de sécurité prises</li>
  <li>l'accès et la confidentialité de ces données</li>
  <li>votre droit d’accès et de rectification aux données qui vous concernent</li>
</ul>
<p class="astuce">
  Des informations peuvent évoluer ; vous pouvez à tout moment les consulter depuis <a target="_blank" rel="noopener noreferrer" href="<?php echo SERVEUR_CNIL ?>">la page informative <em>CNIL / RGPD</em></a>.
</p>

<hr>
<form action="#" method="post" id="form_cnil">
  <?php echo $form_activation ?>
</form>

<?php else: /* ADMINISTRATEURS */ ?>

<p class="astuce">
  Vous êtes invité à consulter les <a target="_blank" rel="noopener noreferrer" href="<?php echo SERVEUR_CNIL ?>">informations <em>CNIL / RGPD</em> relatives à l’application <em>SACoche</em></a>.<br>
  Les utilisateurs, autres que les administrateurs, doivent cocher qu’ils en ont pris connaissance pour utiliser leur compte.
</p>


<?php endif; /* SUITE COMMUNE */ ?>

<hr>
<h2><?php echo html(Lang::_('Historique de vos connexions')) ?></h2>
<p class="astuce">
  Depuis le 18 juin 2018, <em>SACoche</em> mémorise les connexions aux comptes sur la dernière année.<br>
  Cela uniquement pour que vous puissiez vérifier qu’une tierce personne ne se connecte pas à votre compte à votre insu.<br>
  Seul le jour de la dernière connexion peut être visible d’autres personnels (pas l’historique détaillé des connexions).
</p>
<table>
  <thead>
    <tr>
      <th>Date / Heure</th>
      <th>Mode de connexion</th>
      <th>Info de connexion</th>
    </tr>
  </thead>
  <tbody>
    <?php
    // Lister les connexions ; ne peut pas être vide puisque l’utilisateur est connecté !
    $DB_TAB = DB_STRUCTURE_ACCES_HISTORIQUE::DB_lister_for_user( $_SESSION['USER_ID'] );
    foreach($DB_TAB as $DB_ROW)
    {
      $date_affich = To::datetime_sql_to_french( $DB_ROW['acces_date'] , TRUE /*return_time*/ );
      switch($DB_ROW['acces_mode'])
      {
        case 'normal'         : $mode = 'formulaire SACoche'    ; $info = 'login '.$DB_ROW['acces_info'];          break;
        case 'switch'         : $mode = 'bascule entre comptes' ; $info = 'compte '.$DB_ROW['acces_info'];         break;
        case 'cas'            : $mode = 'externe depuis un ENT' ; $info = 'cas:user '.$DB_ROW['acces_info'];       break; // obsolète, à laisser jusqu'à fin 2025
        case 'cas:user'       : $mode = 'externe depuis un ENT' ; $info = 'cas:user '.$DB_ROW['acces_info'];       break;
        case 'cas:attributes' : $mode = 'externe depuis un ENT' ; $info = 'cas:attributes '.$DB_ROW['acces_info']; break;
        case 'cas:sconet'     : $mode = 'externe depuis un ENT' ; $info = 'cas:sconet '.$DB_ROW['acces_info'];     break;
        case 'shibboleth'     : $mode = 'externe depuis un ENT' ; $info = 'uid '.$DB_ROW['acces_info'];            break;
        case 'siecle'         : $mode = 'externe depuis un ENT' ; $info = 'élève '.$DB_ROW['acces_info'];          break;
        case 'vecteur_parent' : $mode = 'externe depuis un ENT' ; $info = 'parent de '.$DB_ROW['acces_info'];      break;
        case 'api'            : $mode = 'externe depuis une API'; $info = 'jeton '.$DB_ROW['acces_info'];          break;
        default               : $mode = 'non référencé'         ; $info = $DB_ROW['acces_info'];
      }
      // Afficher une ligne du tableau
      echo'<tr>';
      echo  '<td>'.$date_affich.'</td>';
      echo  '<td>'.html($mode).'</td>';
      echo  '<td>'.html($info).'</td>';
      echo'</tr>'.NL;
    }
    ?>
  </tbody>
</table>
