<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$captcha = Clean::post('f_captcha', 'lettres');
$code    = Clean::post('f_code'   , 'code');

function captcha_exit($message)
{
  Session::_inc('TMP','CAPTCHA','DELAI');
  Session::_set('TMP','CAPTCHA','TIME', $_SERVER['REQUEST_TIME'] );
  Json::end( FALSE , $message );
}

// Protection contre les robots (pour éviter des envois intempestifs de courriels)
if(!isset($_SESSION['TMP']['CAPTCHA']))
{
  Json::end( FALSE , 'Session perdue ou absence de cookie : merci d’actualiser la page.' );
}
else if( $_SERVER['REQUEST_TIME'] - $_SESSION['TMP']['CAPTCHA']['TIME'] < $_SESSION['TMP']['CAPTCHA']['DELAI'] )
{
  Session::_set('TMP','CAPTCHA','TIME', $_SERVER['REQUEST_TIME'] );
  Json::end( FALSE , 'Sécurité : patienter '.$_SESSION['TMP']['CAPTCHA']['DELAI'].'s avant une nouvelle tentative.' );
}

if( !$captcha || !$code )
{
  captcha_exit('Erreur avec les données transmises !');
}

// On vérifie le captcha.
if( $captcha != $_SESSION['TMP']['CAPTCHA']['SOLUCE'] )
{
  captcha_exit('Ordre incorrect !');
}

// Extraction et vérification des données du code
list( $stop_key , $user_id , $BASE ) = explode( 'g' , $code ) + array_fill(0,3,NULL) ; // Evite des NOTICE en initialisant les valeurs manquantes
$user_id = (int)$user_id;
$BASE    = (int)$BASE;

if( !$stop_key || !$user_id || ( !$BASE && (HEBERGEUR_INSTALLATION=='multi-structures') ) )
{
  captcha_exit('Le code transmis est incohérent (format inattendu) !');
}

if( (HEBERGEUR_INSTALLATION=='multi-structures') && !$BASE )
{
  captcha_exit('Code avec un numéro de base incorrect ou absent !');
}

// Récupérer la dénomination de l’établissement
if(HEBERGEUR_INSTALLATION=='multi-structures')
{
  $structure_denomination = DB_WEBMESTRE_PUBLIC::DB_recuperer_structure_nom_for_Id($BASE);
  if($structure_denomination===NULL)
  {
    captcha_exit('Code pour un établissement non trouvé dans la base d’administration !');
  }
  $is_etablissement_virtuel = IS_HEBERGEMENT_SESAMATH && ( ($BASE==ID_DEMO) || ($BASE>=CONVENTION_ENT_ID_ETABL_MAXI) || (substr($structure_denomination,0,5)=='Voir ') ) ? TRUE : FALSE ;
  if($is_etablissement_virtuel)
  {
    captcha_exit('Fonctionnalité sans objet car il concerne une structure virtuelle !');
  }
}
else
{
  $DB_TAB = DB_STRUCTURE_PARAMETRE::DB_lister_parametres('"webmestre_denomination"');
  if(!empty($DB_TAB))
  {
    $structure_denomination = $DB_TAB[0]['parametre_valeur'];
  }
  else
  {
    captcha_exit('Code pour un établissement dont la base est incomplète ou non encore installée !');
  }
}

// En cas de multi-structures, il faut charger les paramètres de connexion à la base concernée
if(HEBERGEUR_INSTALLATION=='multi-structures')
{
  DBextra::charger_parametres_sql_supplementaires($BASE);
}

// On récupère et on vérifie les données de l’utilisateur
$DB_ROW = DB_STRUCTURE_PUBLIC::DB_recuperer_user_pour_requete_par_mail('user_id',$user_id);

if(empty($DB_ROW))
{
  captcha_exit('Code d’un utilisateur introuvable !');
}

if( Outil::clef_md5($BASE.$DB_ROW['user_id'].$DB_ROW['user_email'].$DB_ROW['user_password']) != $stop_key )
{
  captcha_exit('Le code transmis est invalide ou obsolète !');
}

if( $DB_ROW['user_email_refus'] )
{
  captcha_exit('Code d’un compte utilisateur déjà paramétré pour refuser les envois de courriel !');
}

if( $DB_ROW['user_profil_nom_court_singulier'] == 'administrateur' )
{
  captcha_exit('Code d’un compte administrateur, profil non concerné par cette fonctionnalité !');
}

// Prendre en compte la demande de refus d’envoi de courriel
DB_STRUCTURE_COMMUN::DB_modifier_user_parametre( $DB_ROW['user_id'] , 'user_email_refus' , 1 );

// Retour
$tab_retour = array(
  'structure' => html($structure_denomination) ,
  'courriel'  => html($DB_ROW['user_email']) ,
  'user'      => html($DB_ROW['user_nom'].' '.$DB_ROW['user_prenom'].' ('.$DB_ROW['user_profil_nom_court_singulier'].')') ,
);
Json::end( TRUE , $tab_retour );

?>
