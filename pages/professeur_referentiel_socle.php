<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Lier les items au socle commun'));

if(!Outil::test_user_droit_specifique( $_SESSION['DROIT_GERER_REFERENTIEL'] , NULL /*matiere_coord_or_groupe_pp_connu*/ , 0 /*matiere_id_or_groupe_id_a_tester*/ ))
{
  echo'<p class="danger">'.html(Lang::_('Vous n’êtes pas habilité à accéder à cette fonctionnalité !')).'</p>'.NL;
  echo'<div class="astuce">Profils autorisés (par les administrateurs) :</div>'.NL;
  echo Outil::afficher_profils_droit_specifique($_SESSION['DROIT_GERER_REFERENTIEL'],'li');
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
?>

<div><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=referentiels_socle__liaison_matiere_socle#toggle_lier">DOC : Lier les items au socle commun</a></span></div>

<hr>

<?php
$tab_matiere = array();

// On récupère la liste des référentiels des matières auxquelles le professeur est rattaché, et s’il en est coordonnateur
$DB_TAB = DB_STRUCTURE_PROFESSEUR::DB_lister_matieres_niveaux_referentiels_professeur( $_SESSION['USER_ID'] , TRUE /*without_matiere_experimentale*/ );
if(empty($DB_TAB))
{
  echo'<ul class="puce">'.NL;
  echo  '<li><span class="danger">Aucun référentiel présent parmi les matières qui vous sont rattachées !</span></li>'.NL;
  echo  '<li><span class="astuce">Commencer par <a href="./index.php?page=professeur_referentiel&amp;section=gestion">créer ou importer un référentiel</a>.</span></li>'.NL;
  echo'</ul>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
// On récupère les données
foreach($DB_TAB as $DB_ROW)
{
  if( !isset($tab_matiere[$DB_ROW['matiere_id']]) && Outil::test_user_droit_specifique( $_SESSION['DROIT_GERER_REFERENTIEL'] , $DB_ROW['jointure_coord'] /*matiere_coord_or_groupe_pp_connu*/ ) )
  {
    $tab_matiere[$DB_ROW['matiere_id']] = $DB_ROW['matiere_id'];
  }
}
if(empty($tab_matiere))
{
  echo'<ul class="puce">'.NL;
  echo  '<li><span class="danger">Aucun référentiel présent parmi les matières que vous avez le droit de gérer !</span></li>'.NL;
  echo'</ul>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}

$listing_matiere_id = implode(',',$tab_matiere);
$DB_TAB = DB_STRUCTURE_REFERENTIEL::DB_recuperer_referentiels( $listing_matiere_id , TRUE /*without_matiere_experimentale*/ );
if(empty($DB_TAB))
{
  echo'<p class="danger">Aucun référentiel enregistré !</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}

$tab_sousmenu_matiere = array();
$tab_sousmenu_matiere = array();
$tab_niveau_for_matiere = array();
// Passer en revue les référentiels
$memo_matiere_id = 0;
foreach($DB_TAB as $DB_ROW)
{
  $matiere_id = (int)$DB_ROW['matiere_id'];
  $niveau_id  = (int)$DB_ROW['niveau_id'];
  $tab_sousmenu_matiere[$matiere_id] = '<a href="#@" data-matiere="'.$matiere_id.'">'.html($DB_ROW['matiere_nom']).'</a>';
  $tab_sousmenu_niveau[$niveau_id] = '<a href="#@" id="niveau_'.$niveau_id.'" data-niveau="'.$niveau_id.'" class="disabled">'.html($DB_ROW['niveau_nom']).'</a>';
  $tab_niveau_for_matiere[$matiere_id][$niveau_id] = $niveau_id;
}
ksort($tab_sousmenu_niveau);

// On passe au socle
$tab_cycle_used = array( 2=>0 , 3=>0 , 4=>0 );
$tab_socle_cycle      = array();
$tab_sousmenu_cycle   = array();
$tab_sousmenu_domaine = array();
$DB_TAB = DB_STRUCTURE_COMMUN::DB_OPT_socle2016_cycles( TRUE /*only_used*/ );
if(is_string($DB_TAB))
{
  echo'<p class="danger">Aucun item de référentiel n’étant relié au nouveau socle commun, aucun bilan ne peut être obtenu.</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
foreach($DB_TAB as $DB_ROW)
{
  $cycle_id = (int)$DB_ROW['valeur'];
  $tab_cycle_used[$cycle_id] = 1;
}
$DB_TAB = DB_STRUCTURE_COMMUN::DB_recuperer_socle2016_cycles();
foreach($DB_TAB as $DB_ROW)
{
  $cycle_id = (int)$DB_ROW['socle_cycle_id'];
  $class = ($tab_cycle_used[$cycle_id]) ? ' class="actif"' : '' ;
  $tab_sousmenu_cycle[$cycle_id] = '<a href="#@" data-cycle="'.$cycle_id.'"'.$class.infobulle($DB_ROW['socle_cycle_description']).'>'.html($DB_ROW['socle_cycle_nom']).'</a>';
  $tab_socle_cycle[$cycle_id] = $DB_ROW['socle_cycle_nom'];
}
$DB_TAB = DB_STRUCTURE_COMMUN::DB_recuperer_socle2016_domaines();
foreach($DB_TAB as $DB_ROW)
{
  $domaine_id = (int)$DB_ROW['socle_domaine_id'];
  $tab_sousmenu_domaine[$domaine_id] = '<a href="#@" data-domaine="'.$domaine_id.'" class="actif"'.infobulle($DB_ROW['socle_domaine_nom_simple']).'>Domaine '.$domaine_id.'</a>';
}

// Récupération des données du socle
$tab_cellules_domaine = array();
$tab_cellules_item    = array();
$tab_socle_domaine    = array();
$tab_socle_composante = array();
$DB_TAB = DB_STRUCTURE_COMMUN::DB_recuperer_socle2016_arborescence();
foreach($DB_TAB as $DB_ROW)
{
  $socle_domaine_id    = $DB_ROW['socle_domaine_id'];
  $socle_composante_id = $DB_ROW['socle_composante_id'];
  $tab_socle_domaine[$socle_domaine_id] = $DB_ROW['socle_domaine_nom_simple'];
  $tab_socle_composante[$socle_domaine_id][$socle_composante_id] = $DB_ROW['socle_composante_nom_simple'];
}
foreach($tab_socle_cycle as $cycle_id => $cycle_nom)
{
  $domaine_numero = 0;
  foreach($tab_socle_domaine as $domaine_id => $domaine_nom)
  {
    $domaine_numero++;
    $composante_numero = 0;
    foreach($tab_socle_composante[$domaine_id] as $composante_id => $composante_nom)
    {
      $composante_numero++;
      $tab_cellules_domaine[] = '<td class="show" id="id_'.$cycle_id.'_'.$domaine_id.'_'.$composante_id.'_d{KEY}"'.infobulle($cycle_nom.BRJS.$domaine_nom.BRJS.$composante_nom).'>'.$cycle_id.'.'.$domaine_numero.'.'.$composante_numero.'</td>';
      $tab_cellules_item[] = '<td class="hc br show" id="id_'.$cycle_id.'_'.$domaine_id.'_'.$composante_id.'_{KEY}"><input type="checkbox"></td>';
    }
  }
}

// javascript
Layout::add( 'js_inline_before' , 'window.matiere_nb = '.count($tab_sousmenu_matiere).';' );
Layout::add( 'js_inline_before' , 'window.tab_niveau_for_matiere = '.str_replace('"','',json_encode($tab_niveau_for_matiere)).';' );
Layout::add( 'js_inline_before' , 'window.tab_cycle_used = '.str_replace('"','',json_encode($tab_cycle_used)).';' );
Layout::add( 'js_inline_before' , 'window.tab_domaine_used = {1:1,2:1,3:1,4:1,5:1};' );
Layout::add( 'js_inline_before' , 'window.cellules_domaine = "'.str_replace('"','\"',implode('',$tab_cellules_domaine)).'";' );
Layout::add( 'js_inline_before' , 'window.cellules_item    = "'.str_replace('"','\"',implode('',$tab_cellules_item)).'";' );

?>

<h3>Affichage du référentiel</h3>
<div id="sousmenu_matiere" class="sousmenu">
  <b>Matière :</b>
  <?php echo implode(NL,$tab_sousmenu_matiere) ?>
</div>
<div id="sousmenu_niveau" class="sousmenu">
  <b>Niveau :</b>
  <?php echo implode(NL,$tab_sousmenu_niveau) ?>
</div>

<hr>

<h3>Affichage du socle</h3>
<div id="sousmenu_cycle" class="sousmenu">
  <b>Cycle :</b>
  <?php echo implode(NL,$tab_sousmenu_cycle) ?>
</div>
<div id="sousmenu_domaine" class="sousmenu">
  <b>Domaine :</b>
  <?php echo implode(NL,$tab_sousmenu_domaine) ?>
</div>

<hr>

<table id="table_action" class="vm_nug hide"><tbody>
</tbody></table>

<p id="force_scroll"></p>
