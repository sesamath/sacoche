/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    var mode = false;
    var nb_caracteres_max = 250;

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{1:{sorter:'date_fr'},3:{sorter:false}} });
    var tableau_tri = function(){ $('#table_action').trigger( 'sorton' , [ [[1,1],[0,0]] ] ); };
    var tableau_maj = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    tableau_tri();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Voir ou masquer un commentaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_body').on(
      // Ajouter un écouteur sur l’événement "dbltap" ne semble pas utile : sous iPad "dblclick" est intercepté et l’ajout de "dbltap" engendre 2 appels.
      'dblclick' ,
      'td' ,
      function()
      {
        var objet_tr   = $(this).parent();
        var classe     = objet_tr.attr('class');
        var new_classe = (classe=='new') ? 'vue' : 'new' ;
        objet_tr.attr('class',new_classe);
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonctions utilisées
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_form_gestion( mode , id , courriel , date_fr , users, message )
    {
      $('#f_action').val(mode);
      $('#f_id').val(id);
      $('#f_courriel').val(courriel);
      // pour finir
      $('#form_gestion h2').html(mode[0].toUpperCase() + mode.substring(1) + ' une adresse');
      if(mode!='supprimer')
      {
        $('#f_date').val(date_fr);
        if(mode=='ajouter')
        {
          $('#f_users').val('automatique').prop('readonly',true);
        }
        else
        {
          $('#f_users').val(users).prop('readonly',false);
        }
        $('#f_message').focus().val(message);
        afficher_textarea_reste( $('#f_message') , nb_caracteres_max );
      }
      $('#gestion_delete_courriel').html( escapeHtml(courriel) );
      $('#gestion_edit').hideshow( mode != 'supprimer' );
      $('#gestion_delete').hideshow( mode == 'supprimer' );
      $('#ajax_msg_gestion').removeAttr('class').html('');
      $('#form_gestion label[generated=true]').removeAttr('class').html('');
      $.fancybox( { href:'#form_gestion' , modal:true , minHeight:300 , minWidth:750 } );
    }

    /**
     * Ajouter un courriel : mise en place du formulaire
     * @return void
     */
    var ajouter = function()
    {
      mode = $(this).attr('class');
      // Afficher le formulaire
      afficher_form_gestion( mode , '' /*id*/ , '' /*courriel*/ , '' /*date_fr*/ , '' /*users*/ , '' /*message*/ );
    };

    /**
     * Modifier un courriel : mise en place du formulaire
     * @return void
     */
    var modifier = function()
    {
      mode = $(this).attr('class');
      var objet_tr  = $(this).parent().parent();
      var objet_tds = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var id        = objet_tr.attr('id').substring(3);
      var courriel  = objet_tds.eq(0).html();
      var date_fr   = objet_tds.eq(1).html();
      var users     = objet_tds.eq(2).html();
      var message   = tab_erreur_infos[id];
      // Afficher le formulaire
      afficher_form_gestion( mode , id , courriel , date_fr , unescapeHtml(users) , unescapeHtml(message) );
    };

    /**
     * Supprimer un courriel : mise en place du formulaire
     * @return void
     */
    var supprimer = function()
    {
      mode = $(this).attr('class');
      var objet_tr  = $(this).parent().parent();
      var objet_tds = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var id        = objet_tr.attr('id').substring(3);
      var courriel  = objet_tds.eq(0).html();
      // Afficher le formulaire
      afficher_form_gestion( mode , id , courriel , '' /*date_fr*/ , '' /*users*/ , '' /*message*/ );
    };

    /**
     * Annuler une action
     * @return void
     */
    var annuler = function()
    {
      $.fancybox.close();
      mode = false;
    };

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Appel des fonctions en fonction des événements
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on( 'click' , 'q.ajouter'       , ajouter );
    // $('#table_action').on( 'click' , 'q.modifier'      , modifier );
    // $('#table_action').on( 'click' , 'q.supprimer'     , supprimer );

    $('#form_gestion').on( 'click' , '#bouton_annuler' , annuler );
    $('#form_gestion').on( 'click' , '#bouton_valider' , function(){formulaire.submit();} );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer le nombre de caractères restants autorisés dans le textarea
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // input permet d’intercepter à la fois les saisies au clavier et les copier-coller à la souris (clic droit)
    $('#gestion_edit').on( 'input' , '#f_message' , function() { afficher_textarea_reste( $(this) , nb_caracteres_max ); } );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Détecter une adresse à partir d’un copier-coller d’un log d’erreur dans le champ de commentaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function search_email(contenu_champ)
    {
      var regex =/<([a-z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-z0-9-]+(?:\.[a-z0-9-]+))>/; // simplifiée ; indiquée par la spécification du W3C pour les input de type="email"
      var email_find = contenu_champ.match(regex);
      if( email_find != null )
      {
        $('#f_courriel').val(email_find[1]);
      }
    }

    // input permet d’intercepter à la fois les saisies au clavier et les copier-coller à la souris (clic droit)
    $('#gestion_edit').on( 'input' , '#f_message' , function() { search_email( $(this).val() ); } );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_gestion');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_courriel : { required:true , courriel:true , maxlength:window.COURRIEL_LONGUEUR_MAX },
          f_users    : { required:false , maxlength:200 },
          f_message  : { required:false , maxlength:nb_caracteres_max }
        },
        messages :
        {
          f_courriel : { required:'adresse manquante' , courriel:'courriel invalide', maxlength:window.COURRIEL_LONGUEUR_MAX+' caractères maximum' },
          f_users    : { maxlength:200+' caractères maximum' },
          f_message  : { maxlength:nb_caracteres_max+' caractères maximum' }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element) { element.after(error); }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_gestion',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg_gestion').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        $('#form_gestion button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      $('#form_gestion button').prop('disabled',false);
      $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      $('#form_gestion button').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
        switch (mode)
        {
          case 'ajouter':
            $('#table_action tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
            $('#table_action tbody').prepend(responseJSON['html']);
            // window.js_tab_erreur_infos[responseJSON['erreur_id']] = responseJSON['erreur_infos'];
            break;
          case 'modifier':
            // N’EST PLUS UTILISÉ
            // var erreur_id = $('#f_id').val();
            // $('#id_'+erreur_id).addClass('new').html(responseJSON['html']);
            // window.js_tab_erreur_infos[erreur_id] = responseJSON['erreur_infos'];
            break;
          case 'supprimer':
            $('#id_'+$('#f_id').val()).remove();
            break;
        }
        tableau_maj();
        $.fancybox.close();
        mode = false;
      }
    }

  }
);
