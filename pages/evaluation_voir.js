/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// Variables globales à ne pas définir plus tard sinon la minification les renomme et cela pose ensuite souci.
var tab_diagnostic = [];
var tab_dates      = [];

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var groupe_id         = 0;
    var prof_id           = 0;
    var groupe_type       = '';
    var nb_caracteres_max = 2000;
    var action_prof       = 'ajouter';

    // tri des tableaux (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{0:{sorter:'date_fr'},3:{sorter:false},4:{sorter:false},5:{sorter:false}} });
    $('#table_voir'  ).tablesorter({ sortLocaleCompare : true, headers:{} });
    var tableau_tri_action = function(){ $('#table_action').trigger( 'sorton' , [ [[0,1]] ] ); };
    var tableau_tri_voir   = function(){ $('#table_voir'  ).trigger( 'sorton' ); };
    var tableau_maj_action = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    var tableau_maj_voir   = function(){ $('#table_voir'  ).trigger( 'update' , [ true ] ); };
    tableau_tri_action();
    tableau_tri_voir();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Masquer éventuellement des lignes
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#para_affich').on( 'click' ,  'input' , function(){
        var obj_input = $(this);
        var obj_val = obj_input.val();
        $('#table_action tbody').find('tr[data-rempli='+obj_val+']').hideshow( !obj_input.is(':checked') );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger le select f_eleve en ajax
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_eleve(groupe_id,groupe_type)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eleves',
          data : 'f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_eleves_ordre=nom'+'&f_statut=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj').removeAttr('class').html('');
              $('#f_eleve').html(responseJSON['value']).parent().show();
              if($('#f_eleve option').length==2)
              {
                // Cas d’un seul élève retourné dans le regroupement (en particulier pour un parent de plusieurs enfants)
                $('#f_eleve option').eq(1).prop('selected',true);
                 maj_affichage();
              }
            }
            else
            {
              $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    $('#f_groupe').change
    (
      function()
      {
        groupe_type = $('#f_groupe option:selected').parent().attr('label');
        // Rechercher automatiquement la liste des profs
        if( (typeof(groupe_type)!='undefined') && (groupe_type!='Besoins') )
        {
          if( (window.USER_PROFIL_TYPE!='professeur') || (action_prof=='retirer') )
          {
            charger_profs_groupe();
          }
        }
        else
        {
          afficher_prof_connecte();
        }
        // Pour un directeur, un professeur ou un parent de plusieurs enfants, on met à jour f_eleve
        // Pour un élève ou un parent d’un seul enfant cette fonction n’est pas appelée puisque son groupe (masqué) ne peut être changé
        $('#f_eleve').html('<option value="">&nbsp;</option>').parent().hide();
        $('#choix_professeur').hide();
        $('#ajax_msg').removeAttr('class').html('');
        $('#zone_eval_choix').hide();
        groupe_id = $('#f_groupe option:selected').val();
        if(groupe_id)
        {
          $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
          maj_eleve(groupe_id,groupe_type);
          $('#bloc_profs').show();
        }
        else
        {
          $('#ajax_maj').removeAttr('class').html('');
          $('#bloc_profs').hide();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger tous les profs d’une classe (approximativement) ou n’affiche que le prof connecté
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_prof_connecte()
    {
      $('#f_prof').html('<option value="'+window.USER_ID+'">'+window.USER_TEXTE+'</option>');
      action_prof = 'ajouter';
      $('#retirer_prof').hide(0);
      $('#ajouter_prof').show(0);
    }

    function charger_profs_groupe()
    {
      $('button').prop('disabled',true);
      prof_id     = $('#f_prof   option:selected').val();
      groupe_id   = $('#f_groupe option:selected').val();
      groupe_type = $('#f_groupe option:selected').parent().attr('label');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_profs_groupe',
          data : 'f_prof='+prof_id+'&f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_first=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('button').prop('disabled',false);
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('button').prop('disabled',false);
            if(responseJSON['statut']==true)
            {
              $('#f_prof').html(responseJSON['value']);
              action_prof = 'retirer';
              $('#ajouter_prof').hide(0);
              $('#retirer_prof').show(0);
            }
          }
        }
      );
    }

    $('#ajouter_prof , #retirer_prof').click
    (
      function()
      {
        if(action_prof=='retirer')
        {
          afficher_prof_connecte();
        }
        else if(action_prof=='ajouter')
        {
          charger_profs_groupe();
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_groupe     : { required:true },
          f_eleve      : { required:true },
          f_prof       : { required:false },
          f_date_debut : { required:true , dateITA:true },
          f_date_fin   : { required:true , dateITA:true },
          f_all_dates  : { required:false }
        },
        messages :
        {
          f_groupe     : { required:'groupe manquant' },
          f_eleve      : { required:'élève manquant' },
          f_prof       : { },
          f_date_debut : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_date_fin   : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_all_dates  : { }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element) { $('#ajax_msg').after(error); }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        $('#actualiser').prop('disabled',true);
        $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
        $('#zone_eval_choix').hide();
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      $('#actualiser').prop('disabled',false);
      $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      $('#actualiser').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        $('#ajax_msg').attr('class','valide').html('Demande réalisée !');
        $('#table_action tbody').html( responseJSON['html'] );
        tableau_maj_action();
        // masquer éventuellement des lignes
        $('#para_affich').find('input').each
        (
          function()
          {
            var obj_input = $(this);
            if(obj_input.is(':checked'))
            {
              var obj_val = obj_input.val();
              $('#table_action tbody').find('tr[data-rempli='+obj_val+']').hide(0);
            }
          }
        );
        // nom éventuel de l’élève
        if(window.aff_nom_eleve)
        {
          $('#zone_eval_choix h2').html($('#f_eleve option:selected').text());
        }
        $('#zone_eval_choix').show();
        tab_dates = JSON.parse(responseJSON['tab_dates']);
        tab_diagnostic = JSON.parse(responseJSON['tab_diagnostic']);
        // Afficher des résultats au chargement
        if(window.auto_voir_devoir_id)
        {
          if( $('#devoir_'+window.auto_voir_devoir_id).length )
          {
            log('info','auto '+window.auto_voir_devoir_id+' présent '+window.auto_mode);
            $('#devoir_'+window.auto_voir_devoir_id).find('td:last').children('q.'+window.auto_mode).click();
          }
          window.auto_voir_devoir_id = false;
        }
      }
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation et chargement au changement d’élève ou de professeur
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_affichage()
    {
      if($('#f_eleve option:selected').val())
      {
        formulaire.submit();
      }
      else
      {
        $('#ajax_msg').removeAttr('class').html('');
        $('#zone_eval_choix').hide();
      }
    }

    $('#f_eleve').change
    (
      function()
      {
        maj_affichage();
      }
    );

    $('#f_prof').change
    (
      function()
      {
        maj_affichage();
      }
    );

    $('#f_all_dates').change
    (
      function()
      {
        maj_affichage();
      }
    );

    maj_affichage();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur une image action
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_eval_choix').on
    (
      'click',
      'q',
      function()
      {
        var objet      = $(this).attr('class');
        var objet_tr   = $(this).parent().parent();
        var objet_tds  = objet_tr.find('td');
        // Récupérer les informations de la ligne concernée
        var devoir_id  = objet_tr.attr('id').substring(7); // "devoir_" + id
        var texte_date = objet_tds.eq(0).html();
        var texte_prof = objet_tds.eq(1).html();
        var texte_info = objet_tds.eq(2).html();
        // objet de l’action
        if( (objet=='voir') || (objet=='texte_consulter') || (objet=='audio_ecouter') )
        {
          // Voir les notes saisies à un devoir
          // Lire un commentaire écrit
          // Écouter un commentaire audio
          var action = 'Voir_notes';
        }
        else if(objet=='voir_repart')
        {
          // Voir la répartition quantitive des notes saisies
          var action = 'Voir_repartition';
        }
        else if(objet=='saisir')
        {
          // Saisir les notes d’un devoir (auto-évaluation)
          var action = 'Saisir_notes';
        }
        // Afficher la zone associée après avoir chargé son contenu
        $.fancybox( '<label class="loader">'+'En cours&hellip;'+'</label>' );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+action+'&f_eleve='+$('#f_eleve option:selected').val()+'&f_devoir='+devoir_id,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                if(action=='Voir_notes')
                {
                  $('#th_autoeval').hideshow( responseJSON['memo_autoeval'] );
                  $('#titre_voir').html('Devoir du ' + texte_date + ' par ' + texte_prof + ' [ ' + texte_info + ' ]');
                  $('#table_voir tbody').html(responseJSON['lignes']);
                  $('#report_legende'  ).html(responseJSON['legende']);
                  $('#report_texte'    ).html(responseJSON['texte']);
                  $('#report_audio'    ).html(responseJSON['audio']);
                  $('#info_diagnostic').hideshow( tab_diagnostic[devoir_id] );
                  tableau_maj_voir();
                  $.fancybox( { href:'#zone_eval_voir' } );
                }
                else if(action=='Voir_repartition')
                {
                  $('#titre_repartition').html('Devoir du ' + texte_date + ' par ' + texte_prof + ' [ ' + texte_info + ' ]');
                  $('#table_voir_repart_quantitative').html(responseJSON['value']);
                  $.fancybox( { href:'#zone_eval_repartition' , minWidth:600 } );
                }
                else if(action=='Saisir_notes')
                {
                  $('#titre_saisir').html('Devoir du ' + texte_date + ' par ' + texte_prof + ' [ ' + texte_info + ' ]');
                  $('#report_date').html(tab_dates[devoir_id]);
                  $('#fermer_zone_saisir').attr('class','retourner').html('Retour');
                  $('#msg_saisir').removeAttr('class').html('');
                  $('#f_devoir').val(devoir_id);
                  $('#table_saisir tbody').html(responseJSON['lignes']);
                  $.fancybox( { href:'#zone_eval_saisir' , 'margin':0 , modal:true } );
                  $('#last_autoeval').html(responseJSON['memo_autoeval']);
                  $('#f_msg_autre').val(responseJSON['audio_autre']);
                  $('#f_msg_url'  ).val(responseJSON['texte_url']);
                  $('#f_msg_texte').focus().val(responseJSON['texte_data']);
                  afficher_textarea_reste( $('#f_msg_texte') , nb_caracteres_max );
                }
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Réagir à la modification d’une note ou d’un commentaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var modification = false;

    $('#table_saisir').on
    (
      'click',
      'input[type=radio]',
      function()
      {
        modification = true;
        $('#fermer_zone_saisir').attr('class','annuler').html('Annuler / Retour');
        $('#msg_saisir').removeAttr('class').html('');
      }
    );

    $('#f_msg_texte').change
    (
      function()
      {
        modification = true;
        $('#fermer_zone_saisir').attr('class','annuler').html('Annuler / Retour');
        $('#msg_saisir').removeAttr('class').html('');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer le nombre de caractères restants autorisés dans le textarea
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // input permet d’intercepter à la fois les saisies au clavier et les copier-coller à la souris (clic droit)
    $('#zone_eval_saisir').on( 'input' , '#f_msg_texte' , function() { afficher_textarea_reste( $(this) , nb_caracteres_max ); } );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour fermer le formulaire servant à saisir les acquisitions à une évaluation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#fermer_zone_saisir').click
    (
      function()
      {
        modification = false;
        $.fancybox.close();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le lien pour mettre à jour les acquisitions à une évaluation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#valider_saisir').click
    (
      function()
      {
        if(modification==false)
        {
          $('#msg_saisir').attr('class','alerte').html('Aucune modification effectuée !');
        }
        else
        {
          $('#zone_eval_saisir button').prop('disabled',true);
          $('#msg_saisir').attr('class','loader').html('En cours&hellip;');
          // On ne risque pas de problème dû à une limitation du module "suhosin" ou à "max input vars" pour un seul élève (nb champs envoyés = nb items + 1).
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&f_action=Enregistrer_saisies'+'&'+$('#zone_eval_saisir').serialize(),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#zone_eval_saisir button').prop('disabled',false);
                $('#msg_saisir').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                return false;
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                $('#zone_eval_saisir button').prop('disabled',false);
                if(responseJSON['statut']==false)
                {
                  $('#msg_saisir').attr('class','alerte').html(responseJSON['value']);
                }
                else
                {
                  modification = false;
                  $('#msg_saisir').attr('class','valide').html('Saisies enregistrées !');
                  $('#fermer_zone_saisir').attr('class','retourner').html('Retour');
                }
              }
            }
          );
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire #form_copie
    // Upload d’un fichier (avec jquery.form.js)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire_copie = $('#form_copie');

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions_copie =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      error : retour_form_erreur_copie,
      success : retour_form_valide_copie
    };

    // Vérifications précédant l’envoi du formulaire, déclenchées au choix d’un fichier
    $('#f_copie').change
    (
      function()
      {
        var file = this.files[0];
        if( typeof(file) == 'undefined' )
        {
          $.fancybox( '<label class="alerte">Absence de fichier sélectionné.</label>' );
          return false;
        }
        else
        {
          var fichier_nom = file.name;
          var fichier_ext = fichier_nom.split('.').pop().toLowerCase();
          if( '.bat.com.exe.php.'.indexOf('.'+fichier_ext+'.') !== -1 )
          {
            $.fancybox( '<label class="erreur">Extension non autorisée.</label>' );
            return false;
          }
          else
          {
            $('#zone_eval_choix').find('q').hide(0);
            $.fancybox( '<label class="loader">En cours&hellip;</label>' );
            formulaire_copie.submit();
          }
        }
      }
    );

    // Envoi du formulaire (avec jquery.form.js)
    formulaire_copie.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions_copie);
        return false;
      }
    );

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur_copie(jqXHR, textStatus, errorThrown)
    {
      $('#f_copie').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#zone_eval_choix').find('q').show(0);
      $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide_copie(responseJSON)
    {
      $('#f_copie').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#zone_eval_choix').find('q').show(0);
      if(responseJSON['statut']==false)
      {
        $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
      }
      else
      {
        initialiser_compteur();
        $.fancybox.close();
        var devoir_id = responseJSON['devoir_id'];
        var url       = responseJSON['url'];
        $('#devoir_'+devoir_id).find('td').eq(3).html('<a href="'+url+'" target="_blank" rel="noopener noreferrer" class="no_puce"><img alt="copie élève" src="./_img/document/copie_oui.png"'+infobulle('Copie élève disponible.')+'></a>'+'<q class="supprimer"'+infobulle('Retirer sa copie (aucune confirmation ne sera demandée).')+'></q>');
        
      }
    }

    $('#table_action').on
    (
      'click',
      'q.uploader_doc',
      function()
      {
        var objet_tr   = $(this).parent().parent();
        var devoir_id  = objet_tr.attr('id').substring(7); // "devoir_" + id
        $('#f_devoir_id').val(devoir_id);
        $('#f_copie').click();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Appel en ajax pour supprimer une copie
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'q.supprimer',
      function()
      {
        var objet_tr   = $(this).parent().parent();
        var objet_td   = objet_tr.find('td').eq(3);
        var devoir_id  = objet_tr.attr('id').substring(7); // "devoir_" + id
        var doc_url    = objet_td.children('a').attr('href');
        $('#zone_eval_choix').find('q').hide(0);
        $.fancybox( '<label class="loader">En cours&hellip;' );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=retirer_copie'+'&f_devoir='+devoir_id+'&f_doc_url='+encodeURIComponent(doc_url),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
              return false;
            },
            success : function(responseJSON)
            {
              $('#zone_eval_choix').find('q').show(0);
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                $.fancybox.close();
                objet_td.html('<img alt="copie élève" src="./_img/document/copie_non.png">'+'<q class="uploader_doc"'+infobulle('Envoyer sa copie.')+'></q>');
              }
            }
          }
        );
      }
    );

  }
);

