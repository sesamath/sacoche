<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Mode d’identification / Connecteur ENT'));

require(CHEMIN_DOSSIER_INCLUDE.'tableau_sso.php');

if(IS_HEBERGEMENT_SESAMATH)
{

  if (!activeSesamathLoader(false, true) || !class_exists('EntConventions')) {
    echo '<p class="danger">Le fichier &laquo;&nbsp;<b>EntConventions.php</b>&nbsp;&raquo; (uniquement présent sur le serveur Sésamath) n’a pas été détecté !</p>'.NL;
    return; // Ne pas exécuter la suite de ce fichier inclus.
  }
  $tab_connecteurs_convention = EntConventions::$tab_connecteurs_convention;
  $tab_connecteurs_hebergement = EntConventions::$tab_connecteurs_hebergement;
  $tab_ent_convention_infos = EntConventions::$tab_ent_convention_infos;
  $info_moratoire = '<p class="astuce">Le C.A. de l’association a décidé de leur gratuité pour les années scolaires '.implode(' &amp; ',EntConventions::get_annees_scolaires_moratoire()).' (moratoire au vu du bilan financier).</p>';
}
else
{
  $tab_connecteurs_hebergement = $tab_connecteurs_convention = $tab_ent_convention_infos = array();
  $info_moratoire = '';
}

// Séparer le sous-domaine et le domaine du HOST (besoin pour les ENT avec un serveur par établissement, comme ceux du projet ENVOLE sur serveur SCRIBE).
$tab_host_part = explode('.',$_SESSION['CAS_SERVEUR']['HOST']);
$tld    = array_pop($tab_host_part);
$domain = array_pop($tab_host_part);
$SESSION_HOST_DOMAIN    = ( $domain && $tld )   ? $domain.'.'.$tld             : '' ;
$SESSION_HOST_SUBDOMAIN = count($tab_host_part) ?  implode('.',$tab_host_part) : '' ;

// Liste des possibilités
// Retenir en variable javascript les paramètres des serveurs CAS et de Gepi, ainsi que l’état des connecteurs CAS (opérationnels ou pas, avec convention ou pas, avec sous-domaine personnalisable ou pas)
$select_connexions = '';
$js_tab_param = array();
foreach($tab_connexion_mode as $connexion_mode => $mode_texte)
{
  $select_connexions .= '<optgroup label="'.html($mode_texte).'">';
  foreach($tab_connexion_info[$connexion_mode] as $connexion_ref => $tab_info)
  {
    $selected = ( ($connexion_mode==$_SESSION['CONNEXION_MODE']) && ($connexion_ref==$_SESSION['CONNEXION_DEPARTEMENT'].'|'.$_SESSION['CONNEXION_NOM']) ) ? ' selected' : '' ;
    $disabled     = ( $tab_info['obsolete'] ) ? ' disabled'   : '' ;
    $txt_obsolete = ( $tab_info['obsolete'] ) ? ' [obsolète]' : '' ;
    list($departement,$connexion_nom) = explode('|',$connexion_ref);
    $departement = $departement ? $departement.' | ' : '' ;
    $select_connexions .= '<option value="'.$connexion_mode.'~'.$connexion_ref.'"'.$selected.$disabled.'>'.$departement.$tab_info['txt'].$txt_obsolete.'</option>';
    switch($connexion_mode)
    {
      case 'cas' :
        if( isset($tab_connecteurs_hebergement[$connexion_ref]) )
        {
          $convention = 'heberg_acad';
        }
        // on ne peut pas utiliser EntConventions::has_ent_convention_active($connexion_ref) car on n’est pas forcément dans le cas IS_HEBERGEMENT_SESAMATH
        else if( isset($tab_connecteurs_convention[$connexion_ref]) && $tab_ent_convention_infos[$tab_connecteurs_convention[$connexion_ref]]['actif'] )
        {
          $convention = 'conv_acad';
        }
        else
        {
          $convention = 'conv_etabl';
        }
        $domaine_edit = ($tab_info['serveur_host_subdomain']=='*') ? 'oui' : 'non' ;
        $port_edit    = ($tab_info['serveur_port']=='*')           ? 'oui' : 'non' ;
        if( ($connexion_nom=='perso') && $selected )
        {
          // Surcharger les paramètres CAS perso (vides par défaut) avec ceux en session (éventuellement personnalisés).
          $tab_info['serveur_host_subdomain'] = '';
          $tab_info['serveur_host_domain']    = $_SESSION['CAS_SERVEUR']['HOST'];
          $tab_info['serveur_port']           = $_SESSION['CAS_SERVEUR']['PORT'];
          $tab_info['serveur_root']           = $_SESSION['CAS_SERVEUR']['ROOT'];
          $tab_info['serveur_url_login']      = $_SESSION['CAS_SERVEUR']['URL_LOGIN'];
          $tab_info['serveur_url_logout']     = $_SESSION['CAS_SERVEUR']['URL_LOGOUT'];
          $tab_info['serveur_url_validate']   = $_SESSION['CAS_SERVEUR']['URL_VALIDATE'];
        }
        else
        {
          if($tab_info['serveur_host_subdomain']=='*')
          {
            // Sous-domaine reporté si en session, vide sinon
            $tab_info['serveur_host_subdomain'] = ($tab_info['serveur_host_domain']==$SESSION_HOST_DOMAIN) ? $SESSION_HOST_SUBDOMAIN : '' ;
          }
          if($tab_info['serveur_port']=='*')
          {
            // Port reporté si en session, vide sinon
            $tab_info['serveur_port'] = ($tab_info['serveur_port']=='*') ? ( ($_SESSION['CAS_SERVEUR']['PORT']) ? $_SESSION['CAS_SERVEUR']['PORT'] : 8443 ) : $_SESSION['CAS_SERVEUR']['PORT'] ;
          }
        }
        $js_tab_param[$connexion_mode][$connexion_ref] = array( $convention , $domaine_edit , $port_edit , $tab_info['integration_ent'] , $tab_info['serveur_host_subdomain'] , $tab_info['serveur_host_domain'] , $tab_info['serveur_port'] , $tab_info['serveur_root'] , $tab_info['serveur_url_login'] , $tab_info['serveur_url_logout'] , $tab_info['serveur_url_validate'] );
        break;
      case 'shibboleth' :
        $js_tab_param[$connexion_mode][$connexion_ref] = array( $tab_info['integration_ent'] );
        break;
    }
  }
  $select_connexions .= '</optgroup>';
}

// Javascript
Layout::add( 'js_inline_before' , 'window.IS_HEBERGEMENT_SESAMATH = '.(int)IS_HEBERGEMENT_SESAMATH.';' );
Layout::add( 'js_inline_before' , 'window.CONVENTION_ENT_REQUISE  = '.(int)CONVENTION_ENT_REQUISE.';' );
Layout::add( 'js_inline_before' , 'window.tab_param = '.json_encode($js_tab_param).';' );

// Formulaire SELECT pour la vérification du certificat SSL
$tab_cas_verif = array(
  1 => 'Oui = Sécurité optimale (le certificat SSL du serveur d’authentification est valide)',
  0 => 'Non = Sécurité diminuée (la connexion n’est plus sécurisée et peut être interceptée)',
);
$options_cas_verif = '';
foreach($tab_cas_verif as $option_value => $option_texte)
{
  $selected = ($option_value==$_SESSION['CAS_SERVEUR']['VERIF_CERTIF_SSL']) ? ' selected' : '' ;
  $options_cas_verif .= '<option value="'.$option_value.'"'.$selected.'>'.$option_texte.'</option>';
}

// Modèle d’url SSO
$get_base = ($_SESSION['BASE']) ? '='.$_SESSION['BASE'] : '' ;
$url_sso = URL_DIR_SACOCHE.'?sso'.$get_base;

// Coordonnées de l'établissement
$tab_etabl_coords = array( 0 => $_SESSION['ETABLISSEMENT']['DENOMINATION'] );
if($_SESSION['ETABLISSEMENT']['ADRESSE1'])  { $tab_etabl_coords[] = $_SESSION['ETABLISSEMENT']['ADRESSE1']; }
if($_SESSION['ETABLISSEMENT']['ADRESSE2'])  { $tab_etabl_coords[] = $_SESSION['ETABLISSEMENT']['ADRESSE2']; }
if($_SESSION['ETABLISSEMENT']['ADRESSE3'])  { $tab_etabl_coords[] = $_SESSION['ETABLISSEMENT']['ADRESSE3']; }

// Données Chorus
$check_chorus_envoi_automatique = ($_SESSION['CHORUS_ENVOI_AUTOMATIQUE']) ? ' checked="checked"' : '' ;
$value_chorus_numero_engagement = html($_SESSION['CHORUS_NUMERO_ENGAGEMENT']);
if( IS_HEBERGEMENT_SESAMATH && $_SESSION['WEBMESTRE_UAI'] )
{
  // Chorus : appel afin de lister les "services" de l’établissement
  // Déclenche un appel de Chorus pour vérifier qu’il connaît l’établissement et permet de choisir le service de l’établissement vers lequel envoyer la facture.
  DBextra::charger_parametres_sql_supplementaires( 0 /*BASE*/ );
  activeSesamathLoader();
  try
  {
    $chorus = new Chorus();
    $options_chorus_code_service = $chorus->fetchServicesOptions($_SESSION['WEBMESTRE_UAI'], $_SESSION['CHORUS_CODE_SERVICE']);
  }
  catch (Exception $e)
  {
    if (!is_a($e, 'Sesamath\Common\UserException')) {
      trigger_error($e, E_USER_WARNING);
    }
    $options_chorus_code_service = $e->getMessage();
  }
}
else
{
  $options_chorus_code_service = '<option value="">Pas d’envoi possible vers Chorus en l’absence de numéro UAI !</option>';
}
?>

<div><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_administrateur__gestion_mode_identification">DOC : Mode d’identification &amp; intégration aux ENT</a></span></div>

<hr>

<form id="form_mode" action="#" method="post"><fieldset>
  <p><label class="tab">Choix :</label><select id="connexion_mode_nom" name="connexion_mode_nom"><?php echo $select_connexions ?></select></p>
  <div id="cas_options" class="hide">
    <label class="tab" for="cas_serveur_host">Domaine <?php echo infobulle('Souvent de la forme ’cas.domaine.fr’.',TRUE) ?> :</label><input id="cas_serveur_host" name="cas_serveur_host" size="40" type="text" value=""><br>
    <label class="tab" for="cas_serveur_port">Port <?php echo infobulle('En général 443.'.BRJS.'Parfois 8443.',TRUE) ?> :</label><input id="cas_serveur_port" name="cas_serveur_port" size="5" type="text" value=""><br>
    <label class="tab" for="cas_serveur_root">Chemin <?php echo infobulle('En général vide.'.BRJS.'Parfois ’cas’.',TRUE) ?> :</label><input id="cas_serveur_root" name="cas_serveur_root" size="20" type="text" value=""><br>
    <label class="tab" for="cas_serveur_url_login">URL Login <?php echo infobulle('Par défaut, laisser le champ vide.'.BRJS.'Dans ce cas, construit sur le modèle ’https://[domaine]:[port]/[chemin]/login’.'.BRJS.'Indiquer une autre URL pour surcharger ce chemin automatique.',TRUE) ?> :</label><input id="cas_serveur_url_login" name="cas_serveur_url_login" size="60" type="text" value=""><br>
    <label class="tab" for="cas_serveur_url_logout">URL Logout <?php echo infobulle('Par défaut, laisser le champ vide.'.BRJS.'Dans ce cas, construit sur le modèle ’https://[domaine]:[port]/[chemin]/logout’.'.BRJS.'Indiquer une autre URL pour surcharger ce chemin automatique.',TRUE) ?> :</label><input id="cas_serveur_url_logout" name="cas_serveur_url_logout" size="60" type="text" value=""><br>
    <label class="tab" for="cas_serveur_url_validate">URL Validate <?php echo infobulle('Par défaut, laisser le champ vide.'.BRJS.'Dans ce cas, construit sur le modèle ’https://[domaine]:[port]/[chemin]/serviceValidate’.'.BRJS.'Indiquer une autre URL pour surcharger ce chemin automatique.',TRUE) ?> :</label><input id="cas_serveur_url_validate" name="cas_serveur_url_validate" size="60" type="text" value=""><br>
  </div>
  <div id="cas_domaine" class="hide">
    <label class="tab" for="serveur_host_subdomain">Domaine <?php echo infobulle('Indiquer le sous-domaine, par exemple'.BRJS.'clg-truc (pour CEL ou ENOE)'.BRJS.'icart.clg16-truc (pour i-Cart!)'.BRJS.'ent.clg-truc (pour Ard’ENT)',TRUE) ?> :</label><input id="serveur_host_subdomain" name="serveur_host_subdomain" size="30" type="text" value=""> . <input id="serveur_host_domain" name="serveur_host_domain" size="20" type="text" value="" readonly>
  </div>
  <div id="cas_port" class="hide">
    <label class="tab" for="serveur_port">Port <?php echo infobulle('Indiquer le port,'.BRJS.'en général 8443 (pour i-Cart!),'.BRJS.'mais déjà vu à 4443 dans un cas particulier.',TRUE) ?> :</label><input id="serveur_port" name="serveur_port" size="5" type="text" value="">
  </div>
  <div id="cas_verif" class="hide">
    <label class="tab" for="cas_serveur_verif_certif_ssl">Vérif. Certificat SSL :</label><select id="cas_serveur_verif_certif_ssl" name="cas_serveur_verif_certif_ssl"><?php echo $options_cas_verif; ?></select>
  </div>
  <p><span class="tab"></span><button id="bouton_valider_mode" type="button" class="parametre">Valider ce mode d’identification.</button><label id="ajax_msg_mode">&nbsp;</label></p>
</fieldset></form>

<div id="lien_direct" class="hide">
  <p class="astuce">Pour importer les identifiants de l’ENT, utiliser ensuite la page "<a href="./index.php?page=administrateur_fichier_identifiant">importer / imposer des identifiants</a>".</p>
  <p class="astuce">Une fois <em>SACoche</em> convenablement configuré, pour une connexion automatique avec l’authentification externe, utiliser cette adresse&nbsp;:</p>
  <ul class="puce"><li class="b"><?php echo $url_sso ?></li></ul>
</div>

<div id="info_inacheve" class="hide">
  <p class="danger"><em>SACoche</em> sait interroger le serveur d’authentification de cet ENT, mais ses responsables ne l’ont pas intégré.</p>
  <p class="astuce">Si vous êtes concerné, alors faites remonter votre intérêt pour un tel connecteur auprès des responsables de cet ENT&hellip;</p>
</div>

<hr>

<h2>Convention d’accès au service</h2>

<div id="info_hors_sesamath" class="hide">
  <p class="astuce">Sans objet pour cet hébergement.</p>
</div>

<div id="info_hors_actualite" class="hide">
  <p class="astuce">Sans objet car non requis actuellement.</p>
</div>

<div id="info_hors_ent" class="hide">
  <p class="astuce">Sans objet pour ce mode d’authentification.</p>
</div>

<div id="info_heberg_acad" class="hide">
  <p class="astuce">Sans objet car hébergement académique ou départemental.</p>
</div>

<div id="info_conv_acad" class="hide">
  <p><label class="valide">Signée et réglée par le service académique ou départemental (<a href="./index.php?page=compte_accueil">voir en page d’accueil</a>).</label></p>
</div>

<div id="info_conv_etabl" class="hide">
  <p class="astuce">
    La signature d’une convention et son règlement est requis à compter du <?php echo CONVENTION_ENT_START_DATE_FR ?> pour bénéficier de ce service sur le serveur <em>Sésamath</em>.<br>
    Veuillez consulter <a href="<?php echo SERVEUR_GUIDE_ENT ?>#toggle_partenariats" target="_blank" rel="noopener noreferrer">la documentation</a> pour davantage d’explications.
  </p>
  <?php echo $info_moratoire ?>
  <p><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_administrateur__gestion_mode_identification#toggle_gestion_convention">DOC : Gestion d’une convention ENT-SACoche par un établissement</a></span></p>
  <p><form action="#" method="post"><fieldset><span class="tab"></span><button id="bouton_ajouter" type="button" class="ajouter">Ajouter une convention</button></fieldset></form></p>
  <table id="table_action" class="form hsort">
    <thead>
      <tr>
        <th>Nom du service</th>
        <th>Période</th>
        <th>Date d’ajout</th>
        <th>Convention reçue</th>
        <th>Règlement perçu</th>
        <th>Service activé</th>
      </tr>
    </thead>
    <tbody>
      <?php
      // Récupérer les coordonnées du contact référent
      $contact_nom = $contact_prenom = $contact_courriel = '' ;
      if( (IS_HEBERGEMENT_SESAMATH) && (HEBERGEUR_INSTALLATION=='multi-structures') )
      {
        DBextra::charger_parametres_sql_supplementaires( 0 /*BASE*/ );
        $DB_ROW_contact = DB_WEBMESTRE_ADMINISTRATEUR::DB_recuperer_contact_infos($_SESSION['BASE']);
        $contact_nom      = $DB_ROW_contact['structure_contact_nom'];
        $contact_prenom   = $DB_ROW_contact['structure_contact_prenom'];
        $contact_courriel = $DB_ROW_contact['structure_contact_courriel'];
        $DB_TAB = DB_WEBMESTRE_ADMINISTRATEUR::DB_lister_conventions_structure($_SESSION['BASE']);
      }
      // Lister les conventions de cet établissement
      if(!empty($DB_TAB))
      {
        foreach($DB_TAB as $num_row => $DB_ROW)
        {
          // Formater certains éléments
          $texte_signature  = is_null($DB_ROW['convention_signature']) ? 'Non réceptionnée' : 'Oui, le '.To::date_sql_to_french($DB_ROW['convention_signature']) ;
          $is_not_paid = is_null($DB_ROW['convention_paiement']);
          $texte_paiement   = $is_not_paid  ? 'Non réceptionné'  : 'Oui, le '.To::date_sql_to_french($DB_ROW['convention_paiement']) ;
          $texte_activation = (!$DB_ROW['convention_activation']) ? 'Non' : ( ( ($DB_ROW['convention_date_debut']>TODAY_SQL) || ($DB_ROW['convention_date_fin']<TODAY_SQL) ) ? 'Non (hors période)' : 'Oui' ) ;
          // Ajout infos Chorus
          if(!is_null($DB_ROW['convention_chorus_date']))
          {
            $texte_paiement .= '<br>Facture envoyée à Chorus le '.To::date_sql_to_french($DB_ROW['convention_chorus_date']).' (id Chorus '.$DB_ROW['convention_chorus_facture_id'].').';
          }
          elseif( $is_not_paid && EntConventions::is_gestion_v2($DB_ROW['convention_id']) )
          {
            $texte_paiement .= '<br><button type="button" class="eclair">Envoyer vers Chorus</button>';
          }
          $class_signature  = (substr($texte_signature ,0,3)=='Non') ? 'br' : 'bv' ;
          $class_paiement   = (substr($texte_paiement  ,0,3)=='Non') ? 'br' : 'bv' ;
          $class_activation = (substr($texte_activation,0,3)=='Non') ? 'br' : 'bv' ;
          $texte_signature  .= '<br><a href="#convention"><span class="file file_pdf">Convention</span></a>';
          $texte_paiement   .= '<br><a href="#facture"><span class="file file_pdf">Facture</span></a>';
          if( EntConventions::is_gestion_v2($DB_ROW['convention_id']) && is_null($DB_ROW['convention_signature']) )
          {
            $texte_paiement = 'Sans objet pour l’instant';
            $class_paiement = 'bj';
          }
          if(EntConventions::is_moratoire($DB_ROW['convention_date_debut']))
          {
            $texte_paiement = 'Sans objet (moratoire)';
            $class_paiement = 'bj';
          }
          if(EntConventions::is_gestion_v2($DB_ROW['convention_id']))
          {
            if( !$DB_ROW['convention_activation'] )
            {
              $texte_activation = 'Non';
              $class_activation = 'br';
            }
            elseif( ($DB_ROW['convention_date_debut']>TODAY_SQL) || ($DB_ROW['convention_date_fin']<TODAY_SQL) )
            {
              $texte_activation = 'Non (hors période)';
              $class_activation = 'br';
            }
            elseif( is_null($DB_ROW['convention_signature']) || is_null($DB_ROW['convention_paiement']) )
            {
              $texte_activation = 'Provisoirement';
              $class_activation = 'bj';
            }
            else
            {
              $texte_activation = 'Oui';
              $class_activation = 'bv';
            }
          }
          // Afficher une ligne du tableau (masquer par défaut les plus anciennes)
          $class = ( ($num_row<2) || ($DB_ROW['convention_date_fin']>TODAY_SQL) ) ? '' : ' class="hide"' ;
          echo'<tr data-id="'.$DB_ROW['convention_id'].'"'.$class.'>';
          echo  '<td>'.html($DB_ROW['connexion_nom']).'</td>';
          echo  '<td>du '.To::date_sql_to_french($DB_ROW['convention_date_debut']).'<br>au '.To::date_sql_to_french($DB_ROW['convention_date_fin']).'</td>';
          echo  '<td>le '.To::date_sql_to_french($DB_ROW['convention_creation']).'</td>';
          echo  '<td class="'.$class_signature.'">'.$texte_signature.'</td>';
          echo  '<td class="'.$class_paiement.'">'.$texte_paiement.'</td>';
          echo  '<td class="'.$class_activation.'">'.$texte_activation.'</td>';
          echo'</tr>'.NL;
        }
        if($class)
        {
          echo'<tr class="vide"><td class="nu" colspan="6"><button id="bouton_voir_tout" type="button" class="rechercher">Afficher toutes les conventions précédentes.</button></td></tr>';
        }
      }
      else
      {
        echo'<tr class="vide"><td class="nu probleme" colspan="6">Cliquer sur le bouton ci-dessus pour ajouter une convention.</td></tr>'.NL;
      }
      ?>
    </tbody>
  </table>
  <hr>
  <h2>Coordonnées / Contact</h2>
  <p>
    Les coordonnées de votre établissement sont : <b><?php echo html(implode(' / ',$tab_etabl_coords)); ?></b>.
  </p>
  <p>
    Les documents seront établis au nom de <b><?php echo html($contact_nom.' '.$contact_prenom); ?></b>, contact référent de l’établissement pour <em>SACoche</em>, qui recevra des informations sur l’avancement du dossier à son adresse <b><?php echo html($contact_courriel) ?></b>.
  </p>
  <p class="astuce">
    Pour modifier les références de votre établissement ou du contact référent, voyez le menu <a href="./index.php?page=administrateur_etabl_identite">[Identité de l’établissement]</a>.
  </p>
  <hr>
  <h2>Usage de <em>Chorus Pro</em></h2>
  <p>
    Pour le règlement d’une facture, il est possible d’utiliser le service <em>Chorus Pro</em>.<br>
    Nous incitons fortement à utiliser ce service si l’établissement y est éligible.<br>
    Plusieurs gestionnaires en font d’ailleurs une obligation.<br>
    Ainsi, depuis 2024, l’envoi des factures y est activé par défaut.
  </p>
  <form action="#" method="post" id="form_chorus">
    <fieldset>
      <label class="tab">Usage :</label> <label for="f_chorus_envoi_automatique"><input type="checkbox" id="f_chorus_envoi_automatique" name="f_chorus_envoi_automatique" value="1"<?php echo $check_chorus_envoi_automatique ?>> envoi automatique de la facture à <em>Chorus Pro</em></label><br>
      <label class="tab" for="f_chorus_code_service">Service <?php echo infobulle('Certaines structures Chorus exigent de préciser un service destinataire.'.BRJS.'Mais si vous ne savez pas, ou si la liste ne vous évoque rien, ou si la liste ne contient rien, alors laisser ce choix vide.',TRUE) ?> :</label><select id="f_chorus_code_service" name="f_chorus_code_service"><?php echo $options_chorus_code_service ?></select><br>
      <label class="tab" for="f_chorus_numero_engagement">N° engagement <?php echo infobulle('Si votre structure Chorus l’exige, vous pouvez préciser ici un numéro d’engagement.'.BRJS.'Sinon, ou si cela ne vous évoque rien, alors laisser ce champ vide.',TRUE) ?> :</label><input id="f_chorus_numero_engagement" name="f_chorus_numero_engagement" size="12" type="text" value="<?php echo $value_chorus_numero_engagement ?>"><br>
      <span class="tab"></span><button id="bouton_valider_chorus" type="button" class="parametre">Enregistrer cette configuration.</button><label id="ajax_msg_chorus">&nbsp;</label>
    </fieldset>
  </form>
  <p>&nbsp;</p>
</div>

<form action="#" method="post" id="form_ajout" class="hide">
  <h2>Ajouter une convention</h2>
  <p>
    <label class="tab" for="f_etablissement_denomination">Établissement :</label><input id="f_etablissement_denomination" name="f_etablissement_denomination" type="text" value="<?php echo html($_SESSION['WEBMESTRE_DENOMINATION'].' ['.$_SESSION['WEBMESTRE_UAI'].']'); ?>" size="60" readonly>
  </p>
  <p>
    <label class="tab" for="f_connexion_texte">Service :</label><input id="f_connexion_texte" name="f_connexion_texte" type="text" value="" size="60" readonly><br>
    <span class="tab"></span><span class="astuce">Le service est celui qui a été sélectionné sur cette même page.</span>
  </p>
  <p>
    <label class="tab" for="f_annee">Période :</label><select id="f_annee" name="f_annee">
      <option value="-1">&nbsp;</option>
      <option value="0">Année scolaire actuelle : du <?php echo To::jour_debut_annee_scolaire('fr',0).' au '.To::jour_fin_annee_scolaire('fr',0) ?></option>
      <option value="1"<?php echo (To::mois_restants_annee_scolaire()>4) ? ' disabled' : '' ; ?>>Année scolaire suivante : du <?php echo To::jour_debut_annee_scolaire('fr',1).' au '.To::jour_fin_annee_scolaire('fr',1) ?></option>
    </select><br>
    <span class="tab"></span><span class="astuce">Les dates dépendent de l’année scolaire définie au menu <a href="./index.php?page=administrateur_etabl_identite">[Identité de l’établissement]</a>.</span>
  </p>
  <p>
    <span class="tab"></span><button id="bouton_valider_ajout" type="button" class="valider">Valider.</button> <button id="bouton_annuler_ajout" type="button" class="annuler">Annuler.</button><br>
    <span class="tab"></span><label id="ajax_msg_ajout">&nbsp;</label>
  </p>
</form>

<form action="#" method="post" id="form_impression" class="hide">
  <h2>Récupérer / Imprimer les documents associés</h2>
  <p class="astuce">Les coordonnées de votre établissement et du contact référent sont définies dans le menu <a href="./index.php?page=administrateur_etabl_identite">[Identité de l’établissement]</a>.</p>
  <ul class="puce">
    <li><a id="fichier_convention" target="_blank" rel="noopener noreferrer" href=""><span class="file file_pdf">Récupérer / Imprimer votre convention (format <em>pdf</em>).</span></a></li>
    <li><a id="fichier_facture" target="_blank" rel="noopener noreferrer" href=""><span class="file file_pdf">Récupérer / Imprimer votre facture (format <em>pdf</em>).</span></a></li>
  </ul>
</form>
