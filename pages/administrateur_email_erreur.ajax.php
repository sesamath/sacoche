<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action   = Clean::post('f_action'  , 'texte');
$id       = Clean::post('f_id'      , 'entier');
$courriel = Clean::post('f_courriel', 'courriel');
$date_fr  = Clean::post('f_date'    , 'date_fr');
$users    = Clean::post('f_users'   , 'texte');
$message  = Clean::post('f_message' , 'texte');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter un nouveau courriel
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='ajouter') && $courriel )
{
  // Pas de vérification du domaine du serveur mail puisque justement il peut s’agir d’une adresse invalide à cause du domaine.
  // Vérifier que le courriel n’est pas déjà référencé
  if( DB_STRUCTURE_COURRIEL_ERREUR::DB_tester_courriel($courriel) )
  {
    Json::end( FALSE , 'Courriel déjà répertorié !' );
  }
  // Chercher les users avec cet ancien mail
  $DB_TAB = DB_STRUCTURE_ADMINISTRATEUR::DB_rechercher_users( 'email' , $courriel );
  if(empty($DB_TAB))
  {
    Json::end( FALSE , 'Aucun utilisateur trouvé avec cette adresse de courriel.' );
  }
  // Effectuer le changement (retrait)
  $tab_donnees = array( ':courriel'=>'' , ':email_origine'=>'' );
  $tab_identite = array();
  foreach($DB_TAB as $DB_ROW)
  {
    DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $DB_ROW['user_id'] , $tab_donnees );
    $tab_identite[] = $DB_ROW['user_nom'].' '.$DB_ROW['user_prenom'].' ('.$DB_ROW['user_profil_sigle'].')';
  }
  $identites = implode(' ; ',$tab_identite);
  // Ajout à la liste des courriels problématiques
  $erreur_id = DB_STRUCTURE_COURRIEL_ERREUR::DB_ajouter_courriel( $courriel , $identites , $message );
  // Afficher le retour
  Json::add_row( 'html' , '<tr id="id_'.$erreur_id.'" class="new">' );
  Json::add_row( 'html' ,   '<td>'.html($courriel).'</td>' );
  Json::add_row( 'html' ,   '<td>'.TODAY_FR.'</td>' );
  Json::add_row( 'html' ,   '<td>'.html($identites).'</td>' );
  Json::add_row( 'html' ,   '<td class="detail">'.html($message).'</td>' ); // Outil::afficher_texte_tronque($message,60)
  Json::add_row( 'html' ,   '<td class="nu">' );
  // Json::add_row( 'html' ,     '<q class="modifier"'.infobulle('Modifier cette adresse ou son commentaire.').'></q>' );
  // Json::add_row( 'html' ,     '<q class="supprimer"'.infobulle('Débloquer cette adresse.').'></q>' );
  Json::add_row( 'html' ,   '</td>' );
  Json::add_row( 'html' , '</tr>' );
  // Json::add_row( 'erreur_id' , $erreur_id );
  // Json::add_row( 'erreur_infos' , html($message) );
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier un courriel existant <<<<<<<<<< N’EST PLUS UTILISÉ >>>>>>>>>>
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier') && $id && $courriel && $date_fr )
{
  // Pas de vérification du domaine du serveur mail puisque justement il peut s’agir d’une adresse invalide à cause du domaine.
  // Vérifier que le courriel n’est pas déjà référencé
  if( DB_STRUCTURE_COURRIEL_ERREUR::DB_tester_courriel($courriel,$id) )
  {
    Json::end( FALSE , 'Courriel déjà répertorié !' );
  }
  // Modification
  DB_STRUCTURE_COURRIEL_ERREUR::DB_modifier_courriel( $id , $courriel , $users , $message );
  // Retrait pour les utilisateurs concernés
  $DB_TAB = DB_STRUCTURE_ADMINISTRATEUR::DB_rechercher_users( 'email' , $courriel );
  if(!empty($DB_TAB))
  {
    $tab_donnees = array( ':courriel'=>'' , ':email_origine'=>'' );
    foreach($DB_TAB as $DB_ROW)
    {
      DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $DB_ROW['user_id'] , $tab_donnees );
    }
  }
  // Afficher le retour
  Json::add_row( 'html' , '<td>'.html($courriel).'</td>' );
  Json::add_row( 'html' , '<td>'.html($date_fr).'</td>' );
  Json::add_row( 'html' , '<td>'.html($users).'</td>' );
  Json::add_row( 'html' , '<td>'.html(Outil::afficher_texte_tronque($message,60)).'</td>' );
  Json::add_row( 'html' , '<td class="nu">' );
  // Json::add_row( 'html' ,   '<q class="modifier"'.infobulle('Modifier cette adresse ou son commentaire.').'></q>' );
  // Json::add_row( 'html' ,   '<q class="supprimer"'.infobulle('Débloquer cette adresse.').'></q>' );
  Json::add_row( 'html' , '</td>' );
  Json::add_row( 'erreur_infos' , html($message) );
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Supprimer un courriel existant <<<<<<<<<< N’EST PLUS UTILISÉ >>>>>>>>>>
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='supprimer') && $id )
{
  // suppression
  DB_STRUCTURE_COURRIEL_ERREUR::DB_supprimer_courriel($id);
  // Afficher le retour
  Json::end( TRUE );
}

?>
