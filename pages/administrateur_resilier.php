<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Résilier l’inscription de l’établissement'));
?>

<p class="astuce">Pour transmettre un rôle d’administrateur, il suffit de l’inscrire comme administrateur ou de lui donner vos identifiants.</p>

<?php
if( !$_SESSION['USER_EMAIL'] )
{
  echo'<p>Pour supprimer l’inscription de cet établissement, votre adresse de courriel est nécessaire, or elle n’est pas renseignée&nbsp;: <a href="./index.php?page=compte_email">veuillez y remédier</a> avant de revenir sur cette page.</p>';
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
?>

<p>Pour supprimer complètement l’inscription de cet établissement, <button id="bouton_resiliation" class="mail_ecrire">envoyez la demande au webmestre</button> responsable de <em>SACoche</em> sur ce serveur.</p>

<p class="danger">Si vous confirmez ce choix, alors toutes les données des élèves, professeurs, compétences, classes, etc. seront complètement effacées !</p>

<p class="danger">Suite à une mutation, si vous comptez utiliser <em>SACoche</em> dans un autre établissement, pensez auparavant à mettre en partage les référentiels utilisés (depuis un compte enseignant) afin de pouvoir les importer dans l’instance <em>SACoche</em> de votre autre établissement.</p>
