/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    var presentation = false;
    var resultats_affiches = false;

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation : s’il n’y a qu’une matière, on la sélectionne, et on charge donc les niveaux correspondants
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    if( $('#f_matiere').children('option').length == 2 )
    {
      $('#f_matiere').find('option:last').prop('selected',true);
      changement_matiere();
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enlever le message ajax et le résultat précédent au changement d’un élément de formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_select').on
    (
      'change',
      'select, input',
      function()
      {
        $('#ajax_msg_enregistrement_formulaire').removeAttr('class').html('');
        $('#div_lien_eval').hide();
      }
    );

    $('#zone_saisie_plan').on
    (
      'change',
      'select, input',
      function()
      {
        $('#ajax_msg_enregistrement_saisie_plan').removeAttr('class').html('');
        $('#div_lien_eval').hide();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Choix du mode de présentation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_select').on
    (
      'change',
      'input[name=f_presentation]',
      function()
      {
        presentation = $(this).val();
        if(presentation=='formulaire')
        {
          $('#zone_regroupement').show();
          $('#zone_plans').hide();
          $('#zone_plan_classe').hide();
          afficher_formulaire_saisie_note();
        }
        else if(presentation=='plan') // forcé
        {
          $('#zone_regroupement').hide();
          $('#zone_plans').show();
          $('#zone_formulaire_validation').hide();
          afficher_plan_saisie_note();
        }
        afficher_masquer_lien_resultats(true);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Sélectionner automatiquement un niveau au changement de classe ou de matière
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function selection_niveau()
    {
      var niveau_val = $('#f_niveau').val();
      var niveau_id = $('#f_classe option:selected').data('data');
      var item_id = $('#f_item').val();
      if( niveau_id && ( niveau_id != niveau_val ) && !item_id && $('#f_niveau option[value='+niveau_id+']').length )
      {
        $('#f_niveau option[value='+niveau_id+']').prop('selected',true);
        changement_niveau();
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger le select f_niveau en ajax (au changement de f_matiere)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_niveau(matiere_val)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_niveaux',
          data : 'f_matiere='+matiere_val+'&f_first=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj_matiere').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj_matiere').removeAttr('class').html('');
              $('#f_niveau').html(responseJSON['value']);
              $('#bloc_niveau').show();
              // S’il n’y a qu’un niveau, on le sélectionne, et on charge donc les items correspondants
              if( $('#f_niveau').children('option').length == 2 )
              {
                $('#f_niveau').find('option:last').prop('selected',true);
                changement_niveau();
              }
              else
              {
                selection_niveau();
              }
            }
            else
            {
              $('#ajax_maj_matiere').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    function changement_matiere()
    {
      afficher_masquer_lien_resultats(false);
      $('#bloc_niveau , #bloc_item , #zone_formulaire_validation , #zone_plan_classe').hide();
      $('#f_niveau').html('<option value="">&nbsp;</option>');
      var matiere_val = $('#f_matiere').val();
      if(matiere_val)
      {
        $('#ajax_maj_matiere').attr('class','loader').html('En cours&hellip;');
        maj_niveau(matiere_val);
      }
      else
      {
        $('#ajax_maj_matiere').removeAttr('class').html('');
        return false;
      }
    }

    $('#f_matiere').change
    (
      function()
      {
        changement_matiere();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger le select f_item en ajax (au changement de f_niveau)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_item(matiere_val,niveau_val)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_items',
          data : 'f_matiere='+matiere_val+'&f_niveau='+niveau_val,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj_niveau').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj_niveau').removeAttr('class').html('');
              $('#f_item').html(responseJSON['value']);
              $('#bloc_item').show();
            }
            else
            {
              $('#ajax_maj_niveau').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    function changement_niveau()
    {
      afficher_masquer_lien_resultats(false);
      $('#bloc_item , #zone_formulaire_validation , #zone_plan_classe').hide();
      $('#f_item').html('<option value="">&nbsp;</option>');
      var matiere_val = $('#f_matiere').val();
      var niveau_val = $('#f_niveau').val();
      if(matiere_val && niveau_val)
      {
        $('#ajax_maj_niveau').attr('class','loader').html('En cours&hellip;');
        maj_item(matiere_val,niveau_val);
      }
      else
      {
        $('#ajax_maj_niveau').removeAttr('class').html('');
        return false;
      }
    }

    $('#f_niveau').change
    (
      function()
      {
        changement_niveau();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger le select f_eleve en ajax (au changement de f_classe)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_eleve(groupe_id,groupe_type)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eleves',
          data : 'f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_eleves_ordre=nom'+'&f_statut=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj_groupe').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj_groupe').removeAttr('class').html('');
              $('#f_eleve').html(responseJSON['value']);
              $('#bloc_eleve').show();
              afficher_masquer_lien_resultats(true);
              }
            else
            {
              afficher_masquer_lien_resultats(false);
              $('#ajax_maj_groupe').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    $('#f_classe').change
    (
      function()
      {
        $('#bloc_eleve , #zone_formulaire_validation').hide();
        $('#f_eleve').html('<option value="">&nbsp;</option>');
        var groupe_id = $('#f_classe').val();
        if(groupe_id)
        {
          var groupe_type = $('#f_classe option:selected').parent().attr('label');
          $('#ajax_maj_groupe').attr('class','loader').html('En cours&hellip;');
          maj_eleve(groupe_id,groupe_type);
          selection_niveau();
        }
        else
        {
          afficher_masquer_lien_resultats(false);
          $('#ajax_maj_groupe').removeAttr('class').html('');
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger un plan de classe
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_plan').change
    (
      function()
      {
        $('#zone_plan_classe').hide();
        $('#ul_plan').html('<li></li>');
        var plan_id = $(this).val();
        var groupe_id = $('#f_plan option:selected').data('data');
        var groupe_type = $('#f_classe option[value='+groupe_id+']').parent().attr('label');
        if( plan_id )
        {
          // Charger le plan demandé
          $('#ajax_maj_plan').attr('class','loader').html('En cours&hellip;');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&f_action=charger_plan_classe'+'&f_plan='+plan_id+'&f_groupe='+groupe_id+'&f_groupe_type='+groupe_type,
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#ajax_maj_plan').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                return false;
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                if(responseJSON['statut']==false)
                {
                  $('#ajax_maj_plan').attr('class','alerte').html(responseJSON['value']);
                  afficher_masquer_lien_resultats(false);
                }
                else
                {
                  $('#ajax_maj_plan').removeAttr('class').html('');
                  $('#ul_plan').html(responseJSON['li']).css( 'grid-template-columns' , 'auto '.repeat(responseJSON['nb_colonnes']) );
                  afficher_plan_saisie_note();
                  afficher_masquer_lien_resultats(true);
                }
              }
            }
          );
        }
        else
        {
          $('#ajax_maj_plan').removeAttr('class').html('');
          afficher_masquer_lien_resultats(false);
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Afficher ou non le lien pour voir les résultats des élèves
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_item').change
    (
      function()
      {
        afficher_masquer_lien_resultats(true);
      }
    );

    function afficher_masquer_lien_resultats(test_show)
    {
      if(resultats_affiches)
      {
        resultats_affiches = false;
        $('#masquer_resultats').click();
      }
      if( !test_show )
      {
        $('#zone_resultat').hide(0);
      }
      else
      {
        var item_id   = $('#f_item').val();
        var groupe_id = $('#f_classe').val();
        var plan_id   = $('#f_plan').val();
        $('#zone_resultat').hideshow( item_id && ( ( (presentation=='formulaire') && groupe_id ) || ( (presentation=='plan') && plan_id ) ) );
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le checkbox pour choisir ou non une description du devoir
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#box_autodescription').click
    (
      function()
      {
        if($(this).is(':checked'))
        {
          $(this).next().show(0).next().hide(0);
        }
        else
        {
          $(this).next().hide(0).next().show(0).children('input').focus();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Afficher la dernière partie du formulaire (au changement de f_item ou f_eleve ou f_presentation)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_formulaire_saisie_note()
    {
      var item_id  = entier( $('#f_item').val()  );
      var eleve_id = entier( $('#f_eleve').val() );
      if( item_id && eleve_id )
      {
        $('#zone_formulaire_validation').find('label').removeClass('check');
        $('#zone_formulaire_validation').show();
      }
      else
      {
        $('#zone_formulaire_validation').hide();
      }
    }

    function afficher_plan_saisie_note()
    {
      var item_id = entier( $('#f_item').val() );
      var plan_id = entier( $('#f_plan').val() );
      $('#zone_plan_classe').hideshow( item_id && plan_id );
    }

    $('#f_item').change
    (
      function()
      {
        if(presentation=='formulaire')
        {
          afficher_formulaire_saisie_note();
        }
        else if(presentation=='plan') // test obligatoire car on peut ne pas avoir encore choisi le mode de présentation
        {
          $('#ul_plan input').each(function(){$(this).attr('class','X');});
          afficher_plan_saisie_note();
        }
      }
    );

    $('#f_eleve').change
    (
      function()
      {
        afficher_formulaire_saisie_note();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Afficher la saisie d’une note depuis le plan de classe
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#ul_plan').on
    (
      'click',
      'input',
      function()
      {
        var obj_input  = $(this);
        var obj_parent = obj_input.parent();
        var box_autodescription = $('#box_autodescription').is(':checked') ? 1 : 0 ;
        var description         = $('#f_description').val();
        var matiere_nom         = $('#f_matiere option:selected').text();
        var niveau_nom          = $('#f_niveau option:selected').text();
        var item_id             = $('#f_item option:selected').val();
        var item_nom            = $('#f_item option:selected').text();
        var eleve_id            = obj_parent.attr('id').substring(2); // id{num}
        var eleve_nom           = obj_parent.find('span[data=nom]').text();
        var eleve_prenom        = obj_parent.find('span[data=prenom]').text();
        var style               = obj_input.attr('class');
        var note                = isNaN(style.substring(1,2)) ? style : style.substring(1) ;
        if( !box_autodescription && !description )
        {
          window.scrollTo(0,150);
          $('#f_description').focus();
          return false;
        }
        $('#report_referentiel').html(matiere_nom+' | '+niveau_nom);
        $('#report_item'       ).html(item_nom);
        $('#report_eleve'      ).html(eleve_nom+' '+eleve_prenom);
        $('#f_plan_box'        ).val(box_autodescription);
        $('#f_plan_description').val(description);
        $('#f_plan_eleve'      ).val(eleve_id);
        $('#f_plan_item'       ).val(item_id);
        $('#p_saisie_plan').find('label').removeClass('check');
        $('#note_plan_'+note).prop('checked',true).parent().addClass('check');
        $('#ajax_msg_enregistrement_saisie_plan').removeAttr('class').html('');
        $.fancybox( { href:'#zone_saisie_plan' , minWidth:800 } );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer visuellement la note cochée
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_formulaire_validation , #p_saisie_plan').on
    (
      'click',
      'input[name=f_note]',
      function()
      {
        $(this).parent().parent().find('label').removeClass('check');
        $(this).parent().addClass('check');
        if(presentation=='plan')
        {
          $('#ajax_msg_enregistrement_saisie_plan').removeAttr('class').html('');
          // Pour un enregistrement automatique...
          // $('#bouton_valider_saisie_plan').click();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enregistrement d’une note via le formulaire de saisie
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider_formulaire').click
    (
      function()
      {
        var note = $('#zone_formulaire_validation input[name=f_note]:checked').val();
        if(typeof(note)=='undefined') // normalement impossible, sauf si par exemple on triche avec la barre d’outils Web Developer...
        {
          $('#ajax_msg_enregistrement_formulaire').attr('class','erreur').html('Choisir une note !');
          return false;
        }
        if( !$('#box_autodescription').is(':checked') && !$('#f_description').val() )
        {
          $('#ajax_msg_enregistrement_formulaire').attr('class','erreur').html('Choisir un intitulé ou cocher la case !');
          $('#f_description').focus();
          return false;
        }
        $('#form_select button').prop('disabled',true);
        $('#ajax_msg_enregistrement_formulaire').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=enregistrer_note'+'&'+$('#form_select').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#form_select button').prop('disabled',false);
              $('#ajax_msg_enregistrement_formulaire').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_select button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#ajax_msg_enregistrement_formulaire').attr('class','valide').html('Note enregistrée !');
                $('#f_devoir').val(responseJSON['devoir_id']);
                $('#f_groupe').val(responseJSON['groupe_id']);
                $('#lien_eval').attr('href','./index.php?page=evaluation&section=gestion_selection&devoir_id='+responseJSON['devoir_id']+'&groupe_type='+'E'+'&groupe_id='+responseJSON['groupe_id']);
                $('#div_lien_eval').show();
              }
              else
              {
                $('#ajax_msg_enregistrement_formulaire').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enregistrement d’une note via le plan de classe
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider_saisie_plan').click
    (
      function()
      {
        var note = $('#zone_saisie_plan input[name=f_note]:checked').val();
        if(typeof(note)=='undefined') // normalement impossible, sauf si par exemple on triche avec la barre d’outils Web Developer...
        {
          $('#ajax_msg_enregistrement_saisie_plan').attr('class','erreur').html('Choisir une note !');
          return false;
        }
        $('#zone_saisie_plan button').prop('disabled',true);
        $('#ajax_msg_enregistrement_saisie_plan').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=enregistrer_note'+'&'+$('#zone_saisie_plan').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#zone_saisie_plan button').prop('disabled',false);
              $('#ajax_msg_enregistrement_saisie_plan').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#zone_saisie_plan button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#ajax_msg_enregistrement_saisie_plan').attr('class','valide').html('Note enregistrée !');
                $('#f_devoir').val(responseJSON['devoir_id']);
                $('#f_groupe').val(responseJSON['groupe_id']);
                $('#lien_eval').attr('href','./index.php?page=evaluation&section=gestion_selection&devoir_id='+responseJSON['devoir_id']+'&groupe_type='+'E'+'&groupe_id='+responseJSON['groupe_id']);
                $('#div_lien_eval').show();
                var eleve_id = $('#f_plan_eleve').val();
                var style = isNaN(note) ? note : 'N'+note ;
                $('#id'+eleve_id).find('input').attr('class',style);
                $.fancybox.close();
              }
              else
              {
                $('#ajax_msg_enregistrement_saisie_plan').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Voir les notes déjà enregistrées
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#voir_resultats').click
    (
      function()
      {
        var tab_eleve = [];
        if(presentation=='formulaire')
        {
          var groupe_id   = $('#f_classe').val();
          var groupe_nom  = $('#f_classe option:selected').text();
          var groupe_type = $('#f_classe option:selected').parent().attr('label');
          $('#f_eleve option').each(function(){tab_eleve.push($(this).val());}); // dont le premier <option> vide, mais la valeur sera retirée par le fichier ajax
        }
        else if(presentation=='plan') // forcément
        {
          var groupe_id   = $('#f_plan option:selected').data('data');
          var groupe_nom  = $('#f_classe option[value='+groupe_id+']').text();
          var groupe_type = $('#f_classe option[value='+groupe_id+']').parent().attr('label');
          $('#ul_plan li div').each(function(){tab_eleve.push($(this).attr('id').substring(2));}); // id{num}
        }
        var item_id = $('#f_item').val();
        resultats_affiches = true;
        $('#bloc_resultat').html( '<label class="loader">'+'En cours&hellip;'+'</label>' );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=releve_items',
            data : 'csrf='+window.CSRF+'&f_origine='+window.PAGE+'&f_compet_liste='+item_id+'&f_groupe='+groupe_id+'&f_eleve='+tab_eleve+'&f_groupe_type='+encodeURIComponent(groupe_type)+'&f_groupe_nom='+encodeURIComponent(groupe_nom),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#bloc_resultat').html( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $('#bloc_resultat').html( '<label class="alerte">'+responseJSON['value']+'</label>' );
                return false;
              }
              else
              {
                $('#bloc_resultat').html( responseJSON['bilan'].substring(responseJSON['bilan'].indexOf('<table')) );
              }
            }
          }
        );
      }
    );

  }
);
