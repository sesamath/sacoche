/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    /**
     * Ce fichier est associé à releve_html.php (affichage d’un relevé HTML enregistré temporairement).
     * Ces relevés peuvent en effet comporter des éléments dynamiques : cases à cocher et formulaire à soumettre 
     * afin de créer une évaluation ou constituer un groupe de besoin ou évaluer à la volée.
     */

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Tableaux à trier
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    if(typeof tri === 'function')
    {
      tri();
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// 
// Pour "Synthèse de maîtrise du socle" il y a un formulaire simplifié
//  (et pour "Recherche ciblée" c’est affiché directement sans passer par releve_html.php)
// Une bonne partie de ce code ne concerne donc que "Grille d’items d’un référentiel" et "Relevé d’items [...]"
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    if($('#span_submit').length)
    {
      $('#form_synthese input[type=checkbox]').css('display','none'); // bcp plus rapide que hide() qd il y a bcp d’éléments
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Voir / masquer tous les détails ; code aussi placé dans la page principale pour les élèves et les parents
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('div.astuce').on
    (
      'click',
      '#montrer_details',
      function()
      {
        $('body').find('a.toggle_plus').click();
        $(this).replaceWith('<a href="#" id="masquer_details">tout masquer</a>');
        return false;
      }
    );

    $('div.astuce').on
    (
      'click',
      '#masquer_details',
      function()
      {
        $('body').find('a.toggle_moins').click();
        $(this).replaceWith('<a href="#" id="montrer_details">tout montrer</a>');
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Afficher / masquer checkbox au changement d’un choix d’action
// 
// .css('display','...') utilisé en remplacement de hide() et show() car plus rapide quand il y a bcp d’éléments.
// Ici, utiliser les fonctions de jQuery ralentissaient Firefox jusqu’à obtenir un boîte d’avertissement.
// 
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_action').change
    (
      function()
      {
        $('#form_synthese input[type=checkbox]').css('display','none');
        $('#check_msg').removeAttr('class').html('');
        var action = $('#f_action option:selected').val();
        if(action=='evaluer_items_perso')
        {
          $('#form_synthese input[name=id_req\\[\\]]').css('display',window.DISPLAY_MODE);
          $('#span_submit').show(0);
        }
        else if(action=='evaluer_items_commun')
        {
          $('#form_synthese input[name=id_user\\[\\]]').css('display',window.DISPLAY_MODE);
          $('#form_synthese input[name=id_item\\[\\]]').css('display',window.DISPLAY_MODE);
          $('#span_submit').show(0);
          
        }
        else if(action=='constituer_groupe_besoin')
        {
          $('#form_synthese input[name=id_user\\[\\]]').css('display',window.DISPLAY_MODE);
          $('#span_submit').show(0);
        }
        else
        {
          $('#span_submit').hide(0);
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Valider "Grille d’items d’un référentiel" et "Relevé d’items [...]"
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_submit').click
    (
      function()
      {
        var objet = $('#f_action option:selected').val();
        var action = false;
        // evaluer_items_perso
        if(objet=='evaluer_items_perso')
        {
          if( !$('#form_synthese input[name=id_req\\[\\]]:checked').length )
          {
            $('#check_msg').attr('class','alerte').html('Aucune case cochée !');
            return false;
          }
          else
          {
            $('#form_synthese input[name=id_user\\[\\]]').remove();
            $('#form_synthese input[name=id_item\\[\\]]').remove();
            action = 'evaluation_gestion';
          }
        }
        // evaluer_items_commun
        else if(objet=='evaluer_items_commun')
        {
          if( !$('#form_synthese input[name=id_user\\[\\]]:checked').length )
          {
            $('#check_msg').attr('class','alerte').html('Aucun élève coché !');
            return false;
          }
          else if( !$('#form_synthese input[name=id_item\\[\\]]:checked').length )
          {
            $('#check_msg').attr('class','alerte').html('Aucun item coché !');
            return false;
          }
          else
          {
            $('#form_synthese input[name=id_req\\[\\]]').remove();
            action = 'evaluation_gestion';
          }
        }
        // constituer_groupe_besoin
        else if(objet=='constituer_groupe_besoin')
        {
          if( !$('#form_synthese input[name=id_user\\[\\]]:checked').length )
          {
            $('#check_msg').attr('class','alerte').html('Aucun élève coché !');
            return false;
          }
          else
          {
            $('#form_synthese input[name=id_req\\[\\]]').remove();
            $('#form_synthese input[name=id_item\\[\\]]').remove();
            action = 'professeur_groupe_besoin';
          }
        }
        // si ok
        if(action)
        {
          $('#check_msg').removeAttr('class').html('');
          $('#form_synthese').attr( 'action' , './index.php?page='+action );
          $('#form_synthese').submit();
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Valider "Synthèse de maîtrise du socle" (formulaire simplifié)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_synthese button.ajouter').click
    (
      function()
      {
        if( $('#form_synthese input[name=id_user\\[\\]]:checked').length )
        {
          $('#check_msg').removeAttr('class').html('');
          $('#form_synthese').attr( 'action' , './index.php?page='+$(this).attr('name') );
          $('#form_synthese').submit();
        }
        else
        {
          $('#check_msg').attr('class','alerte').html('Aucun élève coché !');
          return false;
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur un panier de demande d’évaluation, mais ici ce ne peut être que pour un bilan généré par un personnel (pour les élèves et les parents c’est dans script.js)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $(document).on
    (
      'click',
      'q.demander_add',
      function()
      {
        $.fancybox( '<div class="astuce">Panier uniquement disposé pour information.<br>Les demandes d’évaluations s’effectuent depuis un compte élève.</div>' );
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajout à la volée d’une évaluation par un prof depuis un bilan
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    var memo_box_autodescription = true;
    var memo_input_description   = '';

    $(document).on
    (
      'click',
      'q.ajouter_note',
      function()
      {
        // Récupérer les infos associées
        var obj_parent = $(this).parent(); // peut être un <td> (relevés) ou un <div> (synthèses)
        var matiere_id = obj_parent.data('matiere');
        var item_id    = obj_parent.data('item');
        var eleve_id   = obj_parent.data('eleve');
        // S’assurer que le prof est toujours connecté, vérifier les infos, récupérer les noms (élève / item), récupérer la liste des codes de notation
        $.fancybox( '<label class="loader">'+'En cours&hellip;'+'</label>' );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=evaluation_ponctuelle_prof_ajout',
            data : 'f_action=charger_formulaire'+'&'+'f_matiere_id='+matiere_id+'&'+'f_item_id='+item_id+'&'+'f_eleve_id='+eleve_id,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            },
            success : function(responseJSON)
            {
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                var box_checked = (memo_box_autodescription) ? ' checked' : '' ;
                var class_label = (memo_box_autodescription) ? '' : ' class="hide"' ;
                var class_span  = (memo_box_autodescription) ? ' class="hide"' : '' ;
                var contenu = '<h2>Ajouter une évaluation à la volée</h2>'
                            + '<form action="#" method="post" id="form_evaluation_ajout">'
                            + '<p><label class="tab">Élève :</label>'+responseJSON['eleve_nom']+' '+responseJSON['eleve_prenom']+'</p>'
                            + '<p><label class="tab">Item :</label>'+responseJSON['item_nom']+'</p>'
                            + '<p><label class="tab">Note :</label>'+responseJSON['label_radio']+'</p>'
                            + '<p><label class="tab">Intitulé :</label><input id="box_autodescription" name="box_autodescription" type="checkbox" value="1"'+box_checked+'> <label for="box_autodescription"="'+class_label+'">automatique</label><span'+class_span+'><input id="f_description" name="f_description" type="text" value="'+unescapeHtml(memo_input_description)+'" size="50" maxlength="60"></span></p>'
                            + '<p><span class="tab"></span><input name="f_eleve" type="hidden" value="'+responseJSON['eleve_id']+'"><input name="f_item" type="hidden" value="'+responseJSON['item_id']+'">'
                            + '<button id="confirmer_evaluation_ajout" type="button" class="valider">Enregistrer.</button> <button id="fermer_evaluation_ajout" type="button" class="annuler">Annuler.</button><label id="ajax_msg_evaluation_ajout"></label></p>'
                            + '</form>';
                $.fancybox( contenu , { modal:true , minWidth:750 } );
              }
            }
          }
        );
      }
    );

    // Clic sur le checkbox pour choisir ou non une description du devoir
    $(document).on
    (
      'click',
      '#box_autodescription',
      function()
      {
        if($(this).is(':checked'))
        {
          $(this).next().show(0).next().hide(0);
        }
        else
        {
          $(this).next().hide(0).next().show(0).children('input').focus();
        }
      }
    );

    $(document).on
    (
      'click',
      '#fermer_evaluation_ajout',
      function()
      {
        $.fancybox.close();
      }
    );

    $(document).on
    (
      'click',
      '#confirmer_evaluation_ajout',
      function()
      {
        var valeur = $('#form_evaluation_ajout input[name=f_note]:checked').val();
        if(typeof(valeur)=='undefined')
        {
          $('#ajax_msg_evaluation_ajout').attr('class','erreur').html('Choisir une note !');
          return false;
        }
        else
        {
          memo_box_autodescription = $('#box_autodescription').is(':checked');
          memo_input_description   = $('#f_description').val();
          $('#form_evaluation_ajout button').prop('disabled',true);
          $('#ajax_msg_evaluation_ajout').attr('class','loader').html('En cours&hellip;');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page=evaluation_ponctuelle',
              data : 'csrf='+window.CSRF+'&f_action=enregistrer_note'+'&f_devoir=0'+'&f_groupe=0'+'&'+$('#form_evaluation_ajout').serialize(),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#ajax_msg_evaluation_ajout').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                $('#form_evaluation_ajout button').prop('disabled',false);
              },
              success : function(responseJSON)
              {
                if(responseJSON['statut']==false)
                {
                  $('#ajax_msg_evaluation_ajout').attr('class','alerte').html(responseJSON['value']);
                }
                else
                {
                  var contenu = '<p><label class="valide">Note supplémentaire enregistrée.</label></p>'
                              + '<p>Pour voir la prise en compte de cette nouvelle note dans le relevé / bilan, re-générer celui-ci.</p>'
                              + '<p>Une fois toutes vos notes saisies, vous pouvez <a id="bilan_lien" href="./index.php?page=evaluation&amp;section=gestion_selection&amp;devoir_id='+responseJSON['devoir_id']+'&amp;groupe_type=E&amp;groupe_id='+responseJSON['groupe_id']+'">voir l’évaluation correspondante ainsi obtenue</a>.</p>'
                              + '<p><span class="tab"></span><button id="fermer_evaluation_ajout" type="button" class="retourner">Fermer.</button></p>';
                  $('#form_evaluation_ajout').html( contenu );
                }
                $('#form_evaluation_ajout button').prop('disabled',false);
              }
            }
          );
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modification à la volée d’une évaluation par un prof depuis un bilan
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $(document).on
    (
      'click',
      'q.modifier_note',
      function()
      {
        // Récupérer les infos associées
        var obj_parent = $(this).parent(); // peut être un <td> (relevés) ou un <div> (synthèses)
        var matiere_id = obj_parent.data('matiere');
        var item_id    = obj_parent.data('item');
        var eleve_id   = obj_parent.data('eleve');
        // S’assurer que le prof est toujours connecté, vérifier les infos, récupérer les noms (élève / item), récupérer la liste des codes de notation et des évaluations
        $.fancybox( '<label class="loader">'+'En cours&hellip;'+'</label>' );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=evaluation_ponctuelle_prof_modification',
            data : 'f_action=charger_formulaire'+'&'+'f_matiere_id='+matiere_id+'&'+'f_item_id='+item_id+'&'+'f_eleve_id='+eleve_id,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            },
            success : function(responseJSON)
            {
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                var box_checked = (memo_box_autodescription) ? ' checked' : '' ;
                var class_label = (memo_box_autodescription) ? '' : ' class="hide"' ;
                var class_span  = (memo_box_autodescription) ? ' class="hide"' : '' ;
                var contenu = '<h2>Modifier à la volée une saisie d’évaluation</h2>'
                            + '<form action="#" method="post" id="form_evaluation_modification">'
                            + '<p><label class="tab">Élève :</label><input id="f_eleve_report" type="hidden" value="'+responseJSON['eleve_id']+'">'+responseJSON['eleve_nom']+' '+responseJSON['eleve_prenom']+'</p>'
                            + '<p><label class="tab">Item :</label><input id="f_item_report" type="hidden" value="'+responseJSON['item_id']+'">'+responseJSON['item_nom']+'</p>'
                            + '<table id="table_radio" class="form">'
                            + '  <thead>'
                            + '    <tr>'
                            + '      <th class="nu"></th>'
                            + '      <th>Note</th>'
                            + '      <th>Date</th>'
                            + '      <th>Évaluation</th>'
                            + '    </tr>'
                            + '  </thead>'
                            + '  <tbody id="tbody_report_notes">'
                            + responseJSON['lignes_eval']
                            + '  </tbody>'
                            + '</table>'
                            + '<fieldset id="champ_modif" class="p hide">'
                            + '  <p id="p_note_modif"><label class="tab">Note :</label>'+responseJSON['label_radio']+'</p>'
                            + '  <p><span class="tab"></span><button id="confirmer_evaluation_modification" type="button" class="valider">Enregistrer.</button> <button id="fermer_evaluation_modification" type="button" class="annuler">Annuler.</button><label id="ajax_msg_evaluation_modification">&nbsp;</label></p>'
                            + '</fieldset>'
                            + '<p id="confirmation_modif"></p>'
                            + '</form>';
                $.fancybox( contenu , { modal:true , minWidth:750 } );
              }
            }
          }
        );
      }
    );

    // Non traité par script.js car page considérée comme "public" & tableau pas dans le DOM au début
    $(document).on
    (
      'click',
      '#table_radio td.label',
      function()
      { 
        $('#ajax_msg_evaluation_modification').removeAttr('class').html('');
        $('#confirmation_modif').html('');
        $(this).parent().find('input[type=radio]:enabled').click();
      }
    );

    // Indiquer la liste de notes à cocher
    $(document).on
    (
      'click',
      '#tbody_report_notes input',
      function()
      {
        $('#ajax_msg_evaluation_modification').removeAttr('class').html('');
        $('#confirmation_modif').html('');
        $('#p_note_modif').children('label').removeClass('check');
        $('input[name=f_note_modif]').prop('checked',false);
        $('#champ_modif').show(0);
      }
    );

    // Indiquer visuellement la note cochée
    $(document).on
    (
      'click',
      '#p_note_modif input',
      function()
      {
        $('#ajax_msg_evaluation_modification').removeAttr('class').html('');
        $('#confirmation_modif').html('');
        $(this).parent().parent().find('label').removeClass('check');
        $(this).parent().addClass('check');
      }
    );

    $(document).on
    (
      'click',
      '#fermer_evaluation_modification',
      function()
      {
        $.fancybox.close();
      }
    );

    $(document).on
    (
      'click',
      '#confirmer_evaluation_modification',
      function()
      {
        var obj_input_devoir = $('#tbody_report_notes input[name=devoir]:checked');
        var obj_input_note   = $('#p_note_modif input[name=f_note_modif]:checked');
        var decription = obj_input_devoir.parent().next().next().next().text();
        var devoir_id  = obj_input_devoir.val();
        var valeur     = obj_input_note.val();
        var note_image = obj_input_note.next().clone();
        var eleve_id = $('#f_eleve_report').val();
        var item_id  = $('#f_item_report').val();
        if(typeof(devoir_id)=='undefined')	// normalement impossible, sauf si par exemple on triche avec la barre d’outils Web Developer...
        {
          $('#ajax_msg_evaluation_modification').attr('class','erreur').html('Cocher l’évaluation concernée !');
          return false;
        }
        else if(typeof(valeur)=='undefined')
        {
          $('#ajax_msg_evaluation_modification').attr('class','erreur').html('Choisir une nouvelle note en remplacement !');
          return false;
        }
        else
        {
          $('#form_evaluation_modification button').prop('disabled',true);
          $('#ajax_msg_evaluation_modification').attr('class','loader').html('En cours&hellip;');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page=evaluation_ponctuelle_prof_modification',
              data : 'csrf='+window.CSRF+'&f_action=modifier_saisie'+'&f_devoir_id='+devoir_id+'&f_note='+valeur+'&f_item_id='+item_id+'&f_eleve_id='+eleve_id+'&f_description='+encodeURIComponent(decription),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#form_evaluation_modification button').prop('disabled',false);
                $('#ajax_msg_evaluation_modification').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              },
              success : function(responseJSON)
              {
                $('#form_evaluation_modification button').prop('disabled',false);
                if(responseJSON['statut']==false)
                {
                  $('#ajax_msg_evaluation_modification').attr('class','alerte').html(responseJSON['value']);
                }
                else
                {
                  $('#devoir_'+devoir_id).parent().next().html(note_image);
                  $('#ajax_msg_evaluation_modification').attr('class','valide').html('Note remplacée !');
                  $('#confirmation_modif').html('<span class="astuce">Pour voir la prise en compte de cette modification dans le relevé / bilan, re-générer celui-ci.</span>');
                }
              }
            }
          );
        }
      }
    );

  }
);

