/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    var mode = false;

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{1:{sorter:false},2:{sorter:false},3:{sorter:false}} });
    var tableau_tri = function(){ $('#table_action').trigger( 'sorton' , [ [[0,0]] ] ); };
    var tableau_maj = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    tableau_tri();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonctions utilisées
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_form_gestion( mode , selection_id , nom , compet_nombre , compet_liste , prof_nombre , prof_liste , proprio_id )
    {
      // Éviter, en cas de duplication d’un regroupement dont on n’est pas le propriétaire, de se retrouver avec des complications
      // (droit du propriétaire d’origine ? regroupements en exemplaires multiples pour les autres ?)
      if( (mode=='dupliquer') && (window.USER_ID!=proprio_id) )
      {
        prof_nombre = 'non';
        prof_liste = '';
      }
      // Choix des collègues à masquer en cas de modification d’un regroupement dont on n’est pas le propriétaire
      // (ingérable sinon : on apparait comme propriétaire, le vrai propriétaire n’apparait pas comme tel...)
      var is_modif_autrui = ( (mode=='modifier') && (window.USER_ID!=proprio_id) ) ? true : false ;
      $('#choisir_prof').hideshow( !is_modif_autrui );
      $('#choisir_prof_non').hideshow( is_modif_autrui );
      $('#f_action').val(mode);
      $('#f_id').val(selection_id);
      $('#f_nom').val(nom);
      $('#f_compet_nombre').val(compet_nombre);
      $('#f_compet_liste').val(compet_liste);
      $('#f_prof_nombre').val(prof_nombre);
      $('#f_prof_liste').val(prof_liste);
      // pour finir
      $('#form_gestion h2').html(mode[0].toUpperCase() + mode.substring(1) + ' un regroupement d’items');
      $('#gestion_delete_identite').html( escapeHtml(nom) );
      $('#gestion_edit').hideshow( mode != 'supprimer' );
      $('#gestion_delete').hideshow( mode == 'supprimer' );
      $('#ajax_msg_gestion').removeAttr('class').html('');
      $('#form_gestion label[generated=true]').removeAttr('class').html('');
      $.fancybox( { href:'#form_gestion' , modal:true , minWidth:700 } );
      if( (mode=='ajouter') || (mode=='dupliquer') ) { $('#f_nom').focus(); }
    }

    /**
     * Ajouter une sélection d’items : mise en place du formulaire
     * @return void
     */
    var ajouter = function()
    {
      mode = $(this).attr('class');
      // Afficher le formulaire
      afficher_form_gestion( mode , '' /*selection_id*/ , '' /*nom*/ , 'aucun' /*compet_nombre*/ , '' /*compet_liste*/ , 'non' /*prof_nombre*/ , '' /*prof_liste*/ , window.USER_ID /*proprio_id*/ );
    };

    /**
     * Modifier | Dupliquer une sélection d’items : mise en place du formulaire
     * @return void
     */
    var modifier_dupliquer = function()
    {
      mode = $(this).attr('class');
      var objet_tr      = $(this).parent().parent();
      var objet_tds     = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var selection_id  = objet_tr.attr('id').substring(3); // "id_" + ref
      var nom           = objet_tds.eq(0).html();
      var compet_nombre = objet_tds.eq(1).text().trim();
      var prof_nombre   = objet_tds.eq(2).text().trim();
      var proprio_id    = objet_tds.eq(2).data('proprio');
      // liste des profs et des items
      var prof_liste    = window.tab_profs[selection_id];
      var compet_liste  = window.tab_items[selection_id];
      // Afficher le formulaire
      afficher_form_gestion( mode , selection_id , unescapeHtml(nom) , compet_nombre , compet_liste , prof_nombre , prof_liste , proprio_id );
    };

    /**
     * Supprimer une sélection d’items : mise en place du formulaire
     * @return void
     */
    var supprimer = function()
    {
      mode = $(this).attr('class');
      var objet_tr      = $(this).parent().parent();
      var objet_tds     = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var selection_id  = objet_tr.attr('id').substring(3);
      var nom           = objet_tds.eq(0).html();
      // Afficher le formulaire
      afficher_form_gestion( mode , selection_id , unescapeHtml(nom) , '' /*compet_nombre*/ , '' /*compet_liste*/ , '' /*prof_nombre*/ , '' /*prof_liste*/ , window.USER_ID /*proprio_id*/ );
    };

    /**
     * Annuler une action
     * @return void
     */
    var annuler = function()
    {
      $.fancybox.close();
      mode = false;
    };

    /**
     * Intercepter la touche entrée ou escape pour valider ou annuler les modifications
     * @return void
     */
    function intercepter(e)
    {
      if(mode)
      {
        if(e.which==13)  // touche entrée
        {
          $('#bouton_valider').click();
        }
        else if(e.which==27)  // touche escape
        {
          $('#bouton_annuler').click();
        }
      }
    }

    /**
     * Choisir les items associés à une sélection : mise en place du formulaire
     * @return void
     */
    var choisir_compet = function()
    {
      // Ne pas changer ici la valeur de "mode" (qui est à "ajouter" ou "modifier" ou "dupliquer").
      cocher_matieres_items( $('#f_compet_liste').val() );
      // Afficher la zone
      $.fancybox( { href:'#zone_matieres_items' , modal:true } );
      if( !IsTouch )
      {
        $(document).tooltip('destroy');display_infobulle(); // Sinon, bug avec l’infobulle contenu dans le fancybox qui ne disparait pas au clic...
      }
    };

    /**
     * Choisir les professeurs associés à une sélection : mise en place du formulaire
     * @return void
     */
    var choisir_prof = function()
    {
      selectionner_profs_option( $('#f_prof_liste').val() );
      // Afficher la zone
      $.fancybox( { href:'#zone_profs' , modal:true , minWidth:700 } );
      if( !IsTouch )
      {
        $(document).tooltip('destroy');display_infobulle(); // Sinon, bug avec l’infobulle contenu dans le fancybox qui ne disparait pas au clic...
      }
    };

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Appel des fonctions en fonction des événements
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on( 'click' , 'q.ajouter'        , ajouter );
    $('#table_action').on( 'click' , 'q.modifier'       , modifier_dupliquer );
    $('#table_action').on( 'click' , 'q.dupliquer'      , modifier_dupliquer );
    $('#table_action').on( 'click' , 'q.supprimer'      , supprimer );

    $('#form_gestion').on( 'click'   , '#bouton_annuler'  , annuler );
    $('#form_gestion').on( 'click'   , '#bouton_valider'  , function(){formulaire.submit();} );
    $('#form_gestion').on( 'keydown' , 'input,select'     , function(e){intercepter(e);} );
    $('#form_gestion').on( 'click'   , 'q.choisir_compet' , choisir_compet );
    $('#form_gestion').on( 'click'   , 'q.choisir_prof'   , choisir_prof );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer au survol une liste de profs associés à une sélection d’items
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'mouseover',
      'td.bulle_profs',
      function()
      {
        var obj_td       = $(this);
        var selection_id = obj_td.parent().attr('id').substring(3); // "id_" + id
        var proprio_id   = obj_td.data('proprio');
        var prof_liste   = window.tab_profs[selection_id];
        var tab_texte    = [];
        if(prof_liste.length)
        {
          prof_liste += '_z'+proprio_id;
          var tab_id = prof_liste.split('_');
          var prof_nombre  = tab_id.length;
          for(var i in tab_id)
          {
            if( (prof_nombre>=12) && (i>=10) )
            {
              var nombre_reste = prof_nombre - i;
              tab_texte[i] = '... et '+nombre_reste+' collègues supplémentaires';
              break;
            }
            var val_option = tab_id[i].substring(0,1);
            var id_prof    = tab_id[i].substring(1);
            var id_select  = 'p'+'_'+id_prof;
            if($('#'+id_select).length)
            {
              tab_texte[i] = $('#'+id_select).next().next().text();
            }
            else
            {
              tab_texte[i] = 'collègue n°'+id_prof+'... ?';
            }
          }
          tab_texte.sort();
        }
        obj_td.attr( 'title' , tab_texte.join('<br>') );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer au survol une liste d’items associés à une sélection d’items
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'mouseover',
      'td.bulle_items',
      function()
      {
        var obj_td       = $(this);
        var selection_id = obj_td.parent().attr('id').substring(3); // "id_" + id
        var item_liste   = window.tab_items[selection_id];
        var tab_id       = item_liste.split('_');
        var item_nombre  = tab_id.length;
        var tab_texte    = [];
        for(var i in tab_id)
        {
          if( (item_nombre>=12) && (i>=10) )
          {
            var nombre_reste = item_nombre - i;
            tab_texte[i] = '... et '+nombre_reste+' items supplémentaires';
            break;
          }
          var input_id = 'item_'+tab_id[i];
          if($('#'+input_id).length)
          {
            tab_texte[i] = $('#'+input_id).parent().text().substring(4); // "[S] " ou "[-] "
          }
          else
          {
            tab_texte[i] = 'item n°'+tab_id[i]+' (ne vous est pas rattaché)';
          }
        }
        obj_td.attr( 'title' , tab_texte.join('<br>') );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Récupération du listing des profs associés à un regroupement
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_groupe_profs').change
    (
      function()
      {
        var obj_option = $(this).find('option:selected');
        var prof_groupe_id = obj_option.val();
        var listing_profs_concernes = obj_option.data('listing');
        if( prof_groupe_id && (typeof(listing_profs_concernes)=='undefined') )
        {
          $('#appliquer_droit_prof_groupe').prop('disabled',true);
          $('#load_profs_groupe').attr('class','loader').html('Recherche&hellip;');
          var prof_groupe_type = obj_option.parent().attr('label');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page=_maj_listing_professeurs',
              data : 'f_groupe_id='+prof_groupe_id+'&f_groupe_type='+prof_groupe_type,
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#appliquer_droit_prof_groupe').prop('disabled',false);
                $('#load_profs_groupe').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                if(responseJSON['statut']==true)
                {
                  obj_option.data('listing',responseJSON['value']);
                  $('#load_profs_groupe').removeAttr('class').html('');
                }
                else
                {
                  $('#load_profs_groupe').attr('class','alerte').html(responseJSON['value']);
                }
                $('#appliquer_droit_prof_groupe').prop('disabled',false);
              }
            }
          );
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Modification du choix du droit pour un lot de profs
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#appliquer_droit_prof_groupe').click
    (
      function()
      {
        var valeur = $('input[name=prof_check_all]:checked').val();
        if(typeof(valeur)=='undefined')
        {
          $('#load_profs_groupe').attr('class','erreur').html('Cocher un niveau de droit !');
          return false;
        }
        var listing_profs_concernes = $('#f_groupe_profs').find('option:selected').data('listing');
        if(typeof(listing_profs_concernes)=='undefined')
        {
          $('#load_profs_groupe').attr('class','erreur').html('Liste des collègues non récupérée !');
          return false;
        }
        $('#load_profs_groupe').removeAttr('class').html('');
        var tab_profs_concernes = listing_profs_concernes.split(',');
        $('.prof_liste').find('select:enabled').each
        (
          function()
          {
            var prof_id = $(this).attr('id').substring(2); // "p_" + id
            var prof_valeur = ( tab_profs_concernes.indexOf(prof_id) == -1 ) ? 'x' : valeur ;
            $(this).find('option[value='+prof_valeur+']').prop('selected',true);
            $(this).next('span').attr('class','select_img droit_'+prof_valeur);
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Modification du choix du droit pour un prof
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_profs').on
    (
      'change',
      'select',
      function()
      {
        var val_option = $(this).find('option:selected').val();
        $(this).next('span').attr('class','select_img droit_'+val_option);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour fermer le cadre des items associés à une sélection (annuler / retour)
    // Clic sur le bouton pour fermer le cadre des professeurs associés à une sélection (annuler / retour)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#annuler_compet , #annuler_profs').click
    (
      function()
      {
        $.fancybox( { href:'#form_gestion' , modal:true , minWidth:700 } );
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour valider le choix des items associés à une sélection
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#valider_compet').click
    (
      function()
      {
        var liste = '';
        var nombre = 0;
        $('#zone_matieres_items input[type=checkbox]:checked').each
        (
          function()
          {
            liste += $(this).val()+'_';
            nombre++;
          }
        );
        var compet_liste  = liste.substring(0,liste.length-1);
        var compet_nombre = (nombre==0) ? 'aucun' : ( (nombre>1) ? nombre+' items' : nombre+' item' ) ;
        $('#f_compet_liste').val(compet_liste);
        $('#f_compet_nombre').val(compet_nombre);
        $('#annuler_compet').click();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour valider le choix des profs associés à une sélection
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#valider_profs').click
    (
      function()
      {
        var liste = '';
        var nombre = 0;
        $('.prof_liste').find('select').each
        (
          function()
          {
            var val_option = $(this).find('option:selected').val();
            if( (val_option!='x') && (val_option!='z') )
            {
              var tab_val = $(this).attr('id').split('_');
              var id_prof = tab_val[1];
              liste += val_option+id_prof+'_';
              nombre++;
            }
          }
        );
        liste  = (!nombre) ? '' : liste.substring(0,liste.length-1) ;
        nombre = (!nombre) ? 'non' : (nombre+1)+' collègues' ;
        $('#f_prof_liste').val(liste);
        $('#f_prof_nombre').val(nombre);
        $('#annuler_profs').click();
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_gestion');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_nom          : { required:true , maxlength:60 },
          f_compet_liste : { required:true },
          f_prof_liste   : { required:false }
        },
        messages :
        {
          f_nom          : { required:'nom manquant' , maxlength:'60 caractères maximum' },
          f_compet_liste : { required:'item(s) manquant(s)' },
          f_prof_liste   : { }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          element.after(error);
        }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_gestion',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        if (!please_wait)
        {
          $(this).ajaxSubmit(ajaxOptions);
          return false;
        }
        else
        {
          return false;
        }
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg_gestion').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        please_wait = true;
        $('#form_gestion button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      please_wait = false;
      $('#form_gestion button').prop('disabled',false);
      $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      please_wait = false;
      $('#form_gestion button').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
        var action = $('#f_action').val();
        switch (mode)
        {
          case 'ajouter':
            $('#table_action tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
          case 'dupliquer':
            $('#table_action tbody').prepend(responseJSON['html']);
            window.tab_items[responseJSON['selection_id']] = responseJSON['items'];
            window.tab_profs[responseJSON['selection_id']] = responseJSON['profs'];
            break;
          case 'modifier':
            var selection_id = $('#f_id').val();
            $('#id_'+selection_id).addClass('new').html(responseJSON['html']);
            window.tab_items[selection_id] = responseJSON['items'];
            window.tab_profs[selection_id] = responseJSON['profs'];
            break;
          case 'supprimer':
            $('#id_'+$('#f_id').val()).remove();
            break;
        }
        tableau_maj();
        $.fancybox.close();
        mode = false;
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Récupéré après le chargement de la page car potentiellement lourd pour les directeurs et les PP (bloque l’affichage plusieurs secondes)
    if( (window.PROFIL_TYPE=='professeur') || (window.PROFIL_TYPE=='directeur') )
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_load_arborescence',
          data : 'f_objet=referentiels'+'&f_item_comm=0'+'&f_all_if_pp=0',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#arborescence label').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            return false;
          },
          success : function(responseJSON)
          {
            $('#arborescence').replaceWith(responseJSON['value']);
          }
        }
      );
    }

  }
);
