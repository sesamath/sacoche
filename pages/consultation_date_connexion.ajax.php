<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {}

$profil      = Clean::post('f_profil'     , 'lettres'); // professeur personnel directeur eleve parent
$groupe_type = Clean::post('f_groupe_type', 'lettres'); // d n c g b
$groupe_id   = Clean::post('f_groupe_id'  , 'entier');
$tab_types   = array('d'=>'all' , 'n'=>'niveau' , 'c'=>'classe' , 'g'=>'groupe' , 'b'=>'besoin');

if( !$profil || !$groupe_id || !isset($tab_types[$groupe_type]) )
{
  Json::end( FALSE , 'Erreur avec les données transmises !' );
}

$champs = ($profil!='parent') ? 'CONCAT(user_nom," ",user_prenom) AS user_identite , user_connexion_date AS connexion_date' : 'CONCAT(parent.user_nom," ",parent.user_prenom," (",enfant.user_nom," ",enfant.user_prenom,")") AS user_identite , parent.user_connexion_date AS connexion_date' ;
$DB_TAB = DB_STRUCTURE_COMMUN::DB_lister_users_regroupement( $profil /*profil_type*/ , 1 /*statut*/ , $tab_types[$groupe_type] , $groupe_id , 'nom' /*eleves_ordre*/ , $champs );

// Les sorties
$html = '';
$csv = new CSV();
$csv->add( array('UTILISATEUR','DATE') , 2 );

foreach($DB_TAB as $DB_ROW)
{
  // Formater la date (dont on ne garde que le jour)
  $date_sql    = ($DB_ROW['connexion_date']===NULL) ? '0' : substr($DB_ROW['connexion_date'],0,10) ;
  $date_affich = ($DB_ROW['connexion_date']===NULL) ? '' : To::date_sql_to_french($date_sql) ;
  // On complète les sorties
  $html .= '<tr>'
        .    '<td>'.html($DB_ROW['user_identite']).'</td>'
        .    '<td>'.$date_affich.'</td>'
        .  '</tr>';
  $csv->add( array( $DB_ROW['user_identite'], $date_affich ) , 1 );
}

// enregistrement CSV
$fnom = 'export_date_connexion_'.$profil.'_'.$tab_types[$groupe_type].'_'.FileSystem::generer_fin_nom_fichier__date_et_alea();
FileSystem::ecrire_objet_csv( CHEMIN_DOSSIER_EXPORT.$fnom.'.csv' , $csv );

// retour
Json::end( TRUE , array( 'html_tbody'=>$html , 'csv_href'=>'./force_download.php?fichier='.$fnom.'.csv' ) );

?>
