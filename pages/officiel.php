<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Synthèses / Bilans'));

// Sous-Menu d’en-tête
if( ($_SESSION['USER_PROFIL_TYPE']=='administrateur') || (($_SESSION['USER_PROFIL_TYPE']=='directeur')&&((substr($SECTION,0,8)=='reglages')||($SECTION=='assiduite'))) )
{
  $SOUS_MENU = '';
  $tab_sous_menu = array(
    'reglages_configuration'     => Lang::_('Configuration des bilans officiels'),
    'reglages_mise_en_page'      => Lang::_('Mise en page des bilans officiels'),
    'reglages_decisions_conseil' => Lang::_('Décisions du conseil de classe'),
    'assiduite'                  => Lang::_('Absences / Retards'),
  );
  foreach($tab_sous_menu as $sous_menu_section => $sous_menu_titre)
  {
    $class = ($sous_menu_section==$SECTION) ? ' class="actif"' : '' ;
    $SOUS_MENU .= '<a'.$class.' href="./index.php?page='.$PAGE.'&amp;section='.$sous_menu_section.'">'.html($sous_menu_titre).'</a>'.NL;
  }
}

if($SECTION=='reglages')
{
  echo'<p>Choisir une rubrique ci-dessus&hellip;</p>'.NL;
  echo'<p class="astuce">Un administrateur doit préalablement choisir l’ordre d’affichage des matières (<span class="manuel"><a class="pop_up" href="'.SERVEUR_DOCUMENTAIRE.'?fichier=support_administrateur__gestion_matieres#toggle_ordre">DOC</a></span>).</p>'.NL;
  echo'<p class="astuce">Un administrateur / professeur doit indiquer le type de synthèse adapté à chaque référentiel (<span class="manuel"><a class="pop_up" href="'.SERVEUR_DOCUMENTAIRE.'?fichier=support_administrateur__gestion_matieres#toggle_type_synthese">DOC</a></span>).</p>'.NL;
  // Avertissement mode de synthèse non configuré ou configuré sans synthèse
  $tab_mode = array(
    'inconnu' => 'dont le format de synthèse est inconnu',
    'sans'    => 'volontairement sans format de synthèse',
  );
  $is_alerte = FALSE;
  $lien_admin = ($_SESSION['USER_PROFIL_TYPE']=='administrateur') ? ' <a href="./index.php?page=administrateur_etabl_matiere&amp;section=synthese">&rarr; Configurer les formats de synthèse.</a>' : '' ;
  foreach($tab_mode as $mode => $explication)
  {
    $nb = DB_STRUCTURE_BILAN::DB_compter_modes_synthese($mode);
    if($nb)
    {
      $is_alerte = TRUE;
      $s = ($nb>1) ? 's' : '' ;
      echo'<p><label class="alerte">Il y a '.$nb.' référentiel'.$s.' '.infobulle(DB_STRUCTURE_BILAN::DB_recuperer_modes_synthese($mode),TRUE).' '.$explication.' (donc non pris en compte).</label>'.$lien_admin.'</p>'.NL;
    }
  }
  if(!$is_alerte)
  {
    echo'<p><label class="valide">Tous les référentiels ont un format de synthèse prédéfini.</label></p>'.NL;
  }
}
elseif($SECTION=='assiduite')
{
  $fichier_section = CHEMIN_DOSSIER_PAGES.$PAGE.'_'.$SECTION.'.php';
  $PAGE = $PAGE.'_'.$SECTION ;
  require($fichier_section);
}
else
{
  if(substr($SECTION,0,8)=='accueil_')
  {
    $BILAN_TYPE = substr($SECTION,8);
    $SECTION = 'accueil';
  }
  // Afficher la bonne page et appeler le bon js / ajax par la suite
  $fichier_section = CHEMIN_DOSSIER_PAGES.$PAGE.'_'.$SECTION.'.php';
  if(!is_file($fichier_section))
  {
    echo'<p class="danger">Page introuvable (paramètre manquant ou incorrect) !</p>'.NL;
    return; // Ne pas exécuter la suite de ce fichier inclus.
  }
  if( !isset($BILAN_TYPE) || in_array($BILAN_TYPE,array('releve','bulletin')) )
  {
    $PAGE = $PAGE.'_'.$SECTION ;
    require($fichier_section);
  }
  else
  {
    echo'<p class="danger">Page introuvable (paramètre manquant ou incorrect) !</p>'.NL;
    return; // Ne pas exécuter la suite de ce fichier inclus.
  }
}
?>
