/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // initialisation (variables globales)
    var modification = false;
    var images = [];
    images['categorie']  = '<q class="modifier"' +infobulle('Modifier cette catégorie')+'></q>'
                         + '<q class="dupliquer"'+infobulle('Dupliquer cette catégorie')+'></q>'
                         + '<q class="supprimer"'+infobulle('Supprimer cette catégorie')+'></q>';
    images['appreciation'] = '<q class="modifier"' +infobulle('Modifier cette appréciation')+'></q>'
                           + '<q class="dupliquer"'+infobulle('Dupliquer cette appréciation')+'></q>'
                           + '<q class="supprimer"'+infobulle('Supprimer cette appréciation')+'></q>';

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Réagir à un réagencement
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function modif_ordre_ou_contenu()
    {
      if(modification==false)
      {
        modification = true;
        $('#ajax_msg').attr('class','alerte').html('Penser à valider !');
      }
    }

    $('#sortable_v').sortable( { cursor:'ns-resize' , update:function(event,ui){modif_ordre_ou_contenu();} } );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour ajouter un titre de catégorie
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#categorie_ajouter').click
    (
      function()
      {
        var categorie_val = escapeHtml( $('#categorie_val').val() );
        if(categorie_val == '')
        {
          $('label[for=categorie_val]').addClass('erreur').html('Saisie manquante !');
          $('#categorie_val').focus();
          return false;
        }
        else
        {
          initialiser_compteur();
          $('label[for=categorie_val]').removeAttr('class').html('');
          $('#sortable_v').append('<li><span class="b u">'+categorie_val+'</span>'+images['categorie']+'</li>');
          $('#sortable_v li.i').remove();
          $('#categorie_val').val('');
          modif_ordre_ou_contenu();
          initialiser_compteur();
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour ajouter une appréciation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#appreciation_ajouter').click
    (
      function()
      {
        var appreciation_val = escapeHtml( $('#appreciation_val').val() );
        if(appreciation_val == '')
        {
          $('label[for=appreciation_val]').addClass('erreur').html('Saisie manquante !');
          $('#appreciation_val').focus();
          return false;
        }
        else
        {
          initialiser_compteur();
          $('label[for=appreciation_val]').removeAttr('class').html('');
          $('#sortable_v').append('<li><span>'+appreciation_val+'</span>'+images['appreciation']+'</li>');
          $('#sortable_v li.i').remove();
          $('#appreciation_val').val('');
          modif_ordre_ou_contenu();
          initialiser_compteur();
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour supprimer un élément
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sortable_v').on
    (
      'click',
      'q.supprimer',
      function()
      {
        var nb_li = $(this).parent().parent().children().length;
        $(this).parent().remove();
        if(nb_li==1)
        {
          $('#sortable_v').append('<li class="i">Encore aucun élément actuellement ! Utilisez les outils ci-dessous pour en ajouter&hellip;</li>');
        }
        else
        {
          modif_ordre_ou_contenu();
          initialiser_compteur();
        }
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur un bouton pour modifier un élément
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sortable_v').on
    (
      'click',
      'q.modifier',
      function()
      {
        var container = $(this).parent();
        var element = $(this).prev();
        var input_val = element.html();
        // soit c’est un titre de catégorie
        if(element.hasClass('b'))
        {
          container.html('<label class="tab" data-type="categorie">Catégorie :</label><input name="input_val" value="'+escapeQuote(input_val)+'" type="text" size="40" maxlength="'+window.CATEGORIE_LENGTH+'"><input name="input_val_old" value="'+escapeQuote(input_val)+'" type="hidden"><q class="valider"'+infobulle('Valider la modification')+'></q><q class="annuler"'+infobulle('Annuler la modification')+'></q><label></label><br><span class="tab"></span><label class="alerte">Pensez à enregistrer les modifications effectuées (à l’aide du bouton ci-dessous).</label>');
        }
        // soit c’est une appréciation
        else
        {
          container.html('<label class="tab" data-type="appreciation">Appréciation :</label><input name="input_val" value="'+escapeQuote(input_val)+'" type="text" size="115" maxlength="'+window.APPRECIATION_LENGTH+'"><input name="input_val_old" value="'+escapeQuote(input_val)+'" type="hidden"><q class="valider"'+infobulle('Valider la modification')+'></q><q class="annuler"'+infobulle('Annuler la modification')+'></q><label></label><br><span class="tab"></span><label class="alerte">Pensez à enregistrer les modifications effectuées (à l’aide du bouton ci-dessous).</label>');
        }
        container.find('input[name=input_val]').focus();
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur un bouton pour dupliquer un élément
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sortable_v').on
    (
      'click',
      'q.dupliquer',
      function()
      {
        var element = $(this).prev().prev();
        var input_val = element.html();
        // soit c’est un titre de catégorie
        if(element.hasClass('b'))
        {
          $('#categorie_val').val(input_val).focus();
        }
        // soit c’est une appréciation
        else
        {
          $('#appreciation_val').val(input_val).focus();
        }
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur un bouton pour annuler la modification d’un élément
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sortable_v').on
    (
      'click',
      'q.annuler',
      function()
      {
        var container = $(this).parent();
        var objet = container.children('label').data('type');
        var input_val = escapeHtml( container.children('input[name=input_val_old]').val() );
        // soit c’est un titre de catégorie
        if(objet=='categorie')
        {
          $(this).parent().html('<span class="b u">'+input_val+'</span>'+images['categorie']);
        }
        // soit c’est une appréciation
        else
        {
          $(this).parent().html('<span>'+input_val+'</span>'+images['appreciation']);
        }
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur un bouton pour valider la modification d’un élément
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sortable_v').on
    (
      'click',
      'q.valider',
      function()
      {
        var container = $(this).parent();
        var objet = container.children('label').data('type');
        var input_val = escapeHtml( container.children('input[name=input_val]').val() );
        if(!input_val)
        {
          $(this).next().next('label').addClass('erreur').html('Saisie manquante !');
          $(this).parent().children('input[name=input_val]').focus();
          return false;
        }
        // soit c’est un titre de catégorie
        if(objet=='categorie')
        {
          $(this).parent().html('<span class="b u">'+input_val+'</span>'+images['categorie']);
        }
        // soit c’est une appréciation
        else
        {
          $(this).parent().html('<span>'+input_val+'</span>'+images['appreciation']);
        }
        modif_ordre_ou_contenu();
        initialiser_compteur();
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Intercepter la touche entrée ou escape pour valider ou annuler les modifications
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $(document).on
    (
      'keydown',
      'input',
      function(e)
      {
        if(e.which==13)  // touche entrée
        {
          $(this).nextAll('q.valider , q.ajouter').click();
          return false;
        }
        else if(e.which==27)  // touche escape
        {
          $(this).nextAll('q.annuler').click();
          return false;
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour valider et enregistrer le contenu des appréciations
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider').click
    (
      function()
      {
        // Récupérer les éléments
        var tab_catalogue_id  = [];
        var tab_catalogue_val = [];
        var modif_en_cours = false;
        $('#sortable_v li').each
        (
          function()
          {
            if($(this).hasClass('i'))
            {
              // C’est une liste vide
            }
            // soit c’est un titre de catégorie
            else if($(this).children('span.b').length)
            {
              var categorie_id  = ( typeof($(this).attr('id')) !== 'undefined' ) ? $(this).attr('id') : 'cat_0' ;
              var categorie_val = unescapeHtml( $(this).children('span').html() );
              tab_catalogue_id.push(categorie_id);
              tab_catalogue_val.push(encodeURIComponent(categorie_val));
            }
            // soit c’est une appréciation
            else if($(this).children('span').length)
            {
              var appreciation_id  = ( typeof($(this).attr('id')) !== 'undefined' ) ? $(this).attr('id') : 'app_0' ;
              var appreciation_val = unescapeHtml( $(this).children('span').html() );
              tab_catalogue_id.push(appreciation_id);
              tab_catalogue_val.push(encodeURIComponent(appreciation_val));
            }
            // soit une modification d’un élément est en cours
            else
            {
              modif_en_cours = true;
              return false;
            }
          }
        );
        if(modif_en_cours)
        {
          $('#ajax_msg').attr('class','erreur').html('Valider ou annuler d’abord toute modification en cours !');
          return false;
        }
        // appel ajax
        $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            // malgré encodeURIComponent() quand PHP reçoit la chaîne il traite %2C comme les autres virgules séparant les données, d’où l’usage d’un autre séparateur
            data : 'csrf='+window.CSRF+'&f_action=enregistrer'+'&f_catalogue_id='+tab_catalogue_id+'&f_catalogue_val='+tab_catalogue_val.join('⁞'),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
                return false;
              }
              else
              {
                if(responseJSON['value'])
                {
                  responseJSON['value'] = responseJSON['value'].replaceAll( '{{QC}}' , '</span>'+images['categorie']    );
                  responseJSON['value'] = responseJSON['value'].replaceAll( '{{QA}}' , '</span>'+images['appreciation'] );
                  $('#sortable_v').html(responseJSON['value']);
                }
                $('label[for=categorie_val]').removeAttr('class').html('');
                $('label[for=appreciation_val]').removeAttr('class').html('');
                $('#ajax_msg').removeAttr('class').html('');
                initialiser_compteur();
                modification=false;
              }
            }
          }
        );
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Import / Export de son catalogue : choix principal de l’action
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('input[name=f_mode]').click
    (
      function()
      {
        $('#form_export').hideshow( $('#f_mode_export').is(':checked') );
        $('#form_import').hideshow( $('#f_mode_import').is(':checked') );
        $('#ajax_msg_export').removeAttr('class').html('');
        $('#ajax_msg_import').removeAttr('class').html('');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Exporter un fichier de validations
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_export').click
    (
      function()
      {
        $('#bouton_export').prop('disabled',true);
        $('#ajax_msg_export').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=export',
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#bouton_export').prop('disabled',false);
              $('#ajax_msg_export').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              $('#bouton_export').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_export').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                initialiser_compteur();
                $('#ajax_msg_export').removeAttr('class').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire #form_import
    // Upload d’un fichier (avec jquery.form.js)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire_import = $('#form_import');

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions_import =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_import',
      error : retour_form_erreur_import,
      success : retour_form_valide_import
    };

    // Vérifications précédant l’envoi du formulaire, déclenchées au choix d’un fichier
    $('#f_import').change
    (
      function()
      {
        var file = this.files[0];
        if( typeof(file) == 'undefined' )
        {
          $('#ajax_msg_import').removeAttr('class').html('');
          return false;
        }
        else
        {
          var fichier_nom = file.name;
          var fichier_ext = fichier_nom.split('.').pop().toLowerCase();
          if( '.xml.zip.'.indexOf('.'+fichier_ext+'.') == -1 )
          {
            $('#ajax_msg_import').attr('class','erreur').html('Le fichier "'+escapeHtml(fichier_nom)+'" n’a pas une extension "xml" ou "zip".');
            return false;
          }
          else
          {
            $('#bouton_import').prop('disabled',true);
            $('#ajax_msg_import').attr('class','loader').html('Récupération du fichier&hellip;');
            formulaire_import.submit();
          }
        }
      }
    );

    // Envoi du formulaire (avec jquery.form.js)
    formulaire_import.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions_import);
        return false;
      }
    );

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur_import(jqXHR, textStatus, errorThrown)
    {
      $('#f_import').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#bouton_import').prop('disabled',false);
      $('#ajax_msg_import').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide_import(responseJSON)
    {
      $('#f_import').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#bouton_import').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_import').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        initialiser_compteur();
        $('#ajax_msg_import').attr('class','valide').html(responseJSON['value']);
        setTimeout( function() { document.location.reload(); }, 5000 );
      }
    }

  }
);
