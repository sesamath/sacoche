/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_notifications').tablesorter({ sortLocaleCompare : true, headers:{0:{sorter:'date_fr'},3:{sorter:false},4:{sorter:false}} });
    var tableau_tri = function(){ $('#table_notifications').trigger( 'sorton' , [ [[0,1]] ] ); };
    var tableau_maj = function(){ $('#table_notifications').trigger( 'update' , [ true ] ); };
    tableau_tri();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Passer une notification comme vue ou consultable
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_body').on(
      // Ajouter un écouteur sur l’événement "dbltap" ne semble pas utile : sous iPad "dblclick" est intercepté et l’ajout de "dbltap" engendre 2 appels.
      'dblclick' ,
      'td' ,
      function()
      {
        var objet_tr   = $(this).parent();
        var objet_tds  = objet_tr.find('td');
        var id         = objet_tr.attr('id').substring(3);
        var classe     = objet_tr.attr('class');
        var new_classe = (classe=='new') ? 'vue' : 'new' ;
        // Une requête ajax discrète pour mémoriser l’action
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=memoriser_consultation'+'&f_id='+id+'&f_etat='+new_classe,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==true)
              {
                var new_texte = (new_classe=='vue') ? 'consultée' : 'consultable' ;
                objet_tr.attr('class',new_classe);
                objet_tds.eq(2).html(new_texte);
                tableau_maj();
              }
              else
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
            }
          }
        );
      }
    );

  }
);
