<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// Indication des profils autorisés
$puce_profil_consultation = in_array($_SESSION['USER_PROFIL_TYPE'],array('directeur','administrateur')) ? '<li><span class="danger b">Les directeurs n’ont qu’un accès en consultation car leur profil n’est pas utilisé pour évaluer.</span></li>' : '' ;

// DÉBUT NON PRIS EN COMPE SI INCLUSION DEPUIS livret_crcn.php
if($PAGE=='evaluation_crcn'):

$TITRE = in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) ? html(Lang::_('Compétences numériques')) : html(Lang::_('Évaluer les compétences numériques')) ;

if( ($_SESSION['USER_PROFIL_TYPE']=='parent') && (!$_SESSION['NB_ENFANTS']) )
{
  echo'<p class="danger">'.$_SESSION['OPT_PARENT_ENFANTS'].'</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}

Layout::add( 'js_inline_before' , 'window.USER_PROFIL_TYPE = "'.$_SESSION['USER_PROFIL_TYPE'].'";' );
?>

<ul class="puce">
  <li><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_professeur__evaluations_crcn">DOC : Évaluation des compétences numériques (CRCN)</a></span></li>
  <li><span class="astuce">Ces positionnements sont utilisés pour le <b>dernier bilan périodique de CM2 et de 6e du Livret Scolaire</b>.</span></li>
  <?php echo $puce_profil_consultation ?>
</ul>

<hr>

<?php
endif;
// FIN NON PRISE EN COMPE SI INCLUSION DEPUIS livret_crcn.php

if( !DB_STRUCTURE_LIVRET::DB_tester_classes_crcn() )
{
  $consigne = ($_SESSION['USER_PROFIL_TYPE']=='professeur') ? 'un administrateur ou directeur doit commencer' : 'commencez' ;
  echo'<p class="danger">Aucune classe n’est associée à une page du livret concernée par ce dispositif !<br>Si besoin, '.$consigne.' par <a href="./index.php?page=livret&amp;section=classes">associer les classes au livret scolaire</a>.</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}

//Pour un lien depuis la page d’accueil
$eleve_id    = Clean::get('eleve_id' , 'entier');
$groupe_id   = Clean::get('classe_id', 'entier');
$groupe_type = ($groupe_id)? 'c' : '';
// Récupération des données transmises
$eleve_id     = Clean::post('f_eleve_id'    , 'entier' , $eleve_id);
$groupe_id    = Clean::post('f_groupe_id'   , 'entier' , $groupe_id);
$groupe_type  = Clean::post('f_groupe_type' , 'lettres', $groupe_type);
$eleves_ordre = Clean::post('f_eleves_ordre', 'eleves_ordre', 'nom'); // Non transmis par Safari si dans le <span> avec la classe "hide".
$numero       = Clean::post('f_numero'      , 'entier' , 1);

$tab_types = array( 'c'=>'classe' , 'g'=>'groupe' , 'b'=>'besoin' ) + array( 'Classes'=>'classe' , 'Groupes'=>'groupe' , 'Besoins'=>'besoin' );

if( $groupe_id && !isset($tab_types[$groupe_type]) )
{
  echo'<p class="danger">Type de groupe inattendu ("'.html($groupe_type).'").</p>';
  $groupe_id = 0;
}

// Fabrication des éléments select du formulaire
if( in_array($_SESSION['USER_PROFIL_TYPE'],array('directeur','administrateur')) )
{
  $tab_groupes = DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl();
  $tab_eleves  = array(); // inutilisé
  $of_groupe  = '';
  $of_eleve   = FALSE;
  $sel_groupe = $groupe_id;
  $sel_eleve  = FALSE;
  $class_form_eleve = 'show';
  $class_bloc_eleve = 'hide';
}
if($_SESSION['USER_PROFIL_TYPE']=='professeur')
{
  $tab_groupes = ($_SESSION['USER_JOIN_GROUPES']=='config') ? DB_STRUCTURE_COMMUN::DB_OPT_groupes_professeur($_SESSION['USER_ID']) : DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl() ;
  $tab_eleves  = array(); // inutilisé
  $of_groupe  = '';
  $of_eleve   = FALSE;
  $sel_groupe = $groupe_id;
  $sel_eleve  = FALSE;
  $class_form_eleve = 'show';
  $class_bloc_eleve = 'hide';
}
if( ($_SESSION['USER_PROFIL_TYPE']=='parent') && ($_SESSION['NB_ENFANTS']>1) )
{
  $tab_groupes = $_SESSION['OPT_PARENT_CLASSES'];
  $tab_eleves  = ($eleve_id) ? DB_STRUCTURE_COMMUN::DB_OPT_eleves_regroupement( 'classe' , $groupe_id , 1 /*statut*/ , 'nom' /*ordre*/ ) : array() ; // maj en ajax suivant le choix du groupe
  $of_groupe  = '';
  $of_eleve   = '';
  $sel_groupe = $groupe_id;
  $sel_eleve  = $eleve_id;
  $class_form_eleve = 'show';
  $class_bloc_eleve = ($eleve_id) ? 'show' : 'hide' ;
}
if( ($_SESSION['USER_PROFIL_TYPE']=='parent') && ($_SESSION['NB_ENFANTS']==1) )
{
  $tab_groupes = array( 0 => array( 'valeur'=>$_SESSION['ELEVE_CLASSE_ID'] , 'texte'=>$_SESSION['ELEVE_CLASSE_NOM'] , 'optgroup'=>'classe' ) );
  $tab_eleves  = array( 0 => array( 'valeur'=>$_SESSION['OPT_PARENT_ENFANTS'][0]['valeur'] , 'texte'=>$_SESSION['OPT_PARENT_ENFANTS'][0]['texte'] ) );
  $of_groupe  = FALSE;
  $of_eleve   = FALSE;
  $sel_groupe = $_SESSION['ELEVE_CLASSE_ID'];
  $sel_eleve  = $_SESSION['OPT_PARENT_ENFANTS'][0]['valeur'];
  $class_form_eleve = 'hide';
  $class_bloc_eleve = 'hide';
}
if($_SESSION['USER_PROFIL_TYPE']=='eleve')
{
  $tab_groupes = array( 0 => array( 'valeur'=>$_SESSION['ELEVE_CLASSE_ID'] , 'texte'=>$_SESSION['ELEVE_CLASSE_NOM'] , 'optgroup'=>'classe' ) );
  $tab_eleves  = array( 0 => array( 'valeur'=>$_SESSION['USER_ID'] , 'texte'=>$_SESSION['USER_NOM'].' '.$_SESSION['USER_PRENOM'] ) );
  $tab_profs   = DB_STRUCTURE_COMMUN::DB_OPT_profs_groupe( 'classe' , $_SESSION['ELEVE_CLASSE_ID'] );
  $of_groupe  = FALSE;
  $of_eleve   = FALSE;
  $sel_groupe = $_SESSION['ELEVE_CLASSE_ID'];
  $sel_eleve  = $_SESSION['USER_ID'];
  $class_form_eleve = 'hide';
  $class_bloc_eleve = 'hide';
}

$tab_eleves_ordre = ($_SESSION['USER_PROFIL_TYPE']!='professeur') ? Form::$tab_select_eleves_ordre : array_merge( Form::$tab_select_eleves_ordre , DB_STRUCTURE_PROFESSEUR_PLAN::DB_OPT_lister_plans_prof_groupe( $_SESSION['USER_ID'] ) ) ;

$select_groupe       = HtmlForm::afficher_select($tab_groupes      , 'f_groupe_id'    /*select_nom*/ , $of_groupe /*option_first*/ , $sel_groupe   /*selection*/ , 'regroupements' /*optgroup*/ );
$select_eleves_ordre = HtmlForm::afficher_select($tab_eleves_ordre , 'f_eleves_ordre' /*select_nom*/ ,      FALSE /*option_first*/ , $eleves_ordre /*selection*/ ,  'eleves_ordre' /*optgroup*/ );
$select_eleve        = HtmlForm::afficher_select($tab_eleves       , 'f_eleve_id'     /*select_nom*/ , $of_eleve  /*option_first*/ , $sel_eleve    /*selection*/ ,              '' /*optgroup*/ );
?>

<form action="#" method="post" id="form_prechoix">
  <p class="<?php echo $class_form_eleve ?>">
    <label class="tab" for="f_groupe_id">Classe / groupe :</label><?php echo $select_groupe ?><input id="f_groupe_type" name="f_groupe_type" type="hidden" value=""> <span id="bloc_ordre" class="hide"><?php echo $select_eleves_ordre ?></span><label id="ajax_maj">&nbsp;</label><br>
    <span id="bloc_eleve" class="<?php echo $class_bloc_eleve ?>"><label class="tab" for="f_eleve_id">Élève :</label><?php echo $select_eleve ?></span>
  </p>

<?php
if( !$sel_groupe || ( !$sel_eleve && in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) ) )
{
  echo'</form>';
  return; // Ne pas exécuter la suite de ce fichier inclus.
}

// Lister les élèves
if( !in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) )
{
  $tab_thead = array();
  $DB_TAB = DB_STRUCTURE_COMMUN::DB_lister_users_regroupement( 'eleve' /*profil_type*/ , 1 /*statut*/ , $tab_types[$groupe_type] , $sel_groupe , $eleves_ordre , 'user_id,user_nom,user_prenom' /*champs*/ );
  if(empty($DB_TAB))
  {
    echo'</form>';
    echo'<p class="danger">Aucun élève trouvé dans ce regroupement !</p>';
    return; // Ne pas exécuter la suite de ce fichier inclus.
  }
  foreach($DB_TAB as $key => $DB_ROW)
  {
    $numero_colonne = $key + 1;
    $tab_thead[$DB_ROW['user_id']] = '<th id="C'.$numero_colonne.'"><dfn>'.html($DB_ROW['user_nom']).' '.html($DB_ROW['user_prenom']).'</dfn></th>';
  }
  $tab_eleve_id = array_keys($tab_thead);
  $listing_eleve_id = implode(',',$tab_eleve_id);
  $colspan = count($tab_eleve_id);
}
else
{
  $listing_eleve_id = $sel_eleve;
}

// Lister les niveaux de maîtrise
$tab_texte_niveaux = array();
$tab_niveau_public = array(
  0 => '',
  3 => 'CYCLE 2 / CYCLE 3',
  5 => 'CYCLE 4 / LYCÉE',
  6 => 'SUPÉRIEUR / ADULTE',
);
$DB_TAB_NIVEAUX = DB_STRUCTURE_LIVRET::DB_lister_crcn_niveaux();
foreach($DB_TAB_NIVEAUX as $DB_ROW)
{
  $infobulle = ($DB_ROW['crcn_niveau_numero']) ? infobulle($tab_niveau_public[$DB_ROW['crcn_niveau_cycle']].BRJS.$DB_ROW['crcn_niveau_description']) : '' ;
  $texte     = ($DB_ROW['crcn_niveau_numero']) ? '<b>'.$DB_ROW['crcn_niveau_numero'].'</b>&nbsp;'.$DB_ROW['crcn_niveau_categorie'] : 'Non renseigné' ;
  $class     = ($DB_ROW['crcn_niveau_cycle']>5) ? ' notnow' : '' ;
  $tab_texte_niveaux[] = '<span class="crcn_niveau_'.$DB_ROW['crcn_niveau_numero'].$class.'"'.$infobulle.'>&nbsp;'.$texte.'&nbsp;</span>';
}
if( !in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) )
{
  $texte_niveaux = implode('<br>',$tab_texte_niveaux);
}
else
{
  unset($tab_texte_niveaux[0]);
  $texte_niveaux = implode(' ',$tab_texte_niveaux);
}
// Lister les domaines et leurs compétences
$tab_tbody = array();
$numero_ligne = 1;
$memo_domaine = 0;
$DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_crcn_domaines_competences( TRUE /*with_detail*/);
foreach($DB_TAB as $DB_ROW)
{
  if($DB_ROW['crcn_domaine_id']!=$memo_domaine)
  {
    $tab_tbody[$DB_ROW['crcn_domaine_id']][0] = '<b>'.html($DB_ROW['crcn_domaine_libelle']).'</b>';
    $memo_domaine = $DB_ROW['crcn_domaine_id'];
  }
  $description = 'DESCRIPTION'.BRJS.$DB_ROW['crcn_competence_description']
               . BRJS.BRJS.'NIVEAU 1'.BRJS.$DB_ROW['crcn_competence_niveau_1']
               . BRJS.BRJS.'NIVEAU 2'.BRJS.$DB_ROW['crcn_competence_niveau_2']
               . BRJS.BRJS.'NIVEAU 3'.BRJS.$DB_ROW['crcn_competence_niveau_3']
               . BRJS.BRJS.'NIVEAU 4'.BRJS.$DB_ROW['crcn_competence_niveau_4']
               . BRJS.BRJS.'NIVEAU 5'.BRJS.$DB_ROW['crcn_competence_niveau_5'];
  $tab_tbody[$DB_ROW['crcn_domaine_id']][$DB_ROW['crcn_competence_id']] = '<th id="L'.$numero_ligne.'">'.infobulle($description,TRUE).' '.html($DB_ROW['crcn_competence_libelle']).'</th>';
  $numero_ligne++;
}

// Lister niveaux déjà saisis
$tab_saisies = array();
$DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_crcn_saisies( $listing_eleve_id , FALSE /*only_positif*/ );
foreach($DB_TAB as $DB_ROW)
{
  $verbe = ($DB_ROW['crcn_niveau_numero']) ? 'renseigné' : 'retiré' ;
  $identite = ($DB_ROW['prof_nom']) ? To::texte_genre_identite( $DB_ROW['prof_nom'] , FALSE , $DB_ROW['prof_prenom'] , TRUE , $DB_ROW['prof_genre'] ) : ( ($DB_ROW['prof_id']) ? 'l’ancien compte n°'.$DB_ROW['prof_id'] : 'un établissement antérieur' ) ;
  $tab_saisies[$DB_ROW['eleve_id']][$DB_ROW['crcn_competence_id']] = array(
    'val'   => $DB_ROW['crcn_niveau_numero'],
    'title' => $verbe.' le '.To::date_sql_to_french($DB_ROW['crcn_saisie_date']).' par '.$identite,
  );
}

if( in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) )
{
  echo'<p id="label_crcn"><label class="tab">Niveaux de maîtrise :</label>'.$texte_niveaux.'</p>';
}
?>
<table id="table_crcn">
  <?php if( !in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) ): ?>
  <thead>
    <tr>
      <?php
      if($_SESSION['USER_PROFIL_TYPE']=='professeur')
      {
        echo'<td>';
        echo  '<p class="hc"><label for="radio_clavier"><input type="radio" id="radio_clavier" name="mode_saisie" value="clavier"> <span class="eval pilot_keyboard">Piloter au clavier</span></label> '.infobulle('Sélectionner une case'.BRJS.'au clavier (flèches) ou à la souris'.BRJS.'puis utiliser les touches suivantes :'.BRJS.'suppr ; 1 ; 2 ; 3 ; 4 ; 5.',TRUE).'<br>';
        echo  '<span id="arrow_continue"><label for="arrow_continue_down"><input type="radio" id="arrow_continue_down" name="arrow_continue" value="down"> <span class="eval arrow_continue_down">par élève</span></label>&nbsp;&nbsp;&nbsp;<label for="arrow_continue_right"><input type="radio" id="arrow_continue_right" name="arrow_continue" value="right"> <span class="eval arrow_continue_right">par item</span></label></span>&nbsp;</p>';
        echo  '<p class="hc"><label for="radio_souris"><input type="radio" id="radio_souris" name="mode_saisie" value="souris"> <span class="eval pilot_mouse">Piloter à la souris</span></label> '.infobulle('Survoler une case du tableau avec la souris'.BRJS.'puis cliquer sur une des valeurs proposées.',TRUE).'</p>';
        echo  '<p class="hc"><button id="bouton_valider" type="button" class="valider">Enregistrer.</button> <button id="bouton_annuler" type="button" class="annuler">Annuler.</button></p>';
        echo'</td>';
        echo implode('',$tab_thead);
        echo'<td>';
        echo  '<p class="b">Niveaux de<br>maîtrise :</p><div class="niveaux">'.$texte_niveaux.'</div>';
        echo'</td>';
      }
      else
      {
        echo'<td>';
        echo  '<p class="b">Niveaux de maîtrise :</p><p class="niveaux">'.$texte_niveaux.'</p>';
        echo'</td>';
        echo implode('',$tab_thead);
        echo'<td></td>';
      }
      ?>
    </tr>
  </thead>
  <?php endif; ?>
  <?php
    $numero_colonne = 1;
    $numero_ligne = 1;
    foreach($tab_tbody as $domaine_id => $tab_tbody_domaine)
    {
      echo'<tbody class="crcn_domaine_'.$domaine_id.'">';
      if( !in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) )
      {
        $rowspan = count($tab_tbody_domaine) - 1 ;
        $th = '<th rowspan="'.$rowspan.'">'.str_replace(' ','<br>',$tab_tbody_domaine[0]).'</th>';
        unset( $tab_tbody_domaine[0] );
        foreach($tab_tbody_domaine as $competence_id => $th_competence)
        {
          echo'<tr>';
          echo $th_competence;
          foreach($tab_eleve_id as $eq_colonne => $eleve_id)
          {
            $numero_colonne = $eq_colonne + 1;
            $value = !empty($tab_saisies[$eleve_id][$competence_id]['val']) ? $tab_saisies[$eleve_id][$competence_id]['val'] : '&nbsp;' ; // pas de saisie ou saisie NULL (supprimée)
            $title = isset($tab_saisies[$eleve_id][$competence_id]) ? infobulle($tab_saisies[$eleve_id][$competence_id]['title']) : '' ;
            echo ($puce_profil_consultation) ? '<td'.$title.'>'.$value.'</td>' : '<td class="td_clavier"'.$title.'><input data-cible="eleve_competence" data-eleve="'.$eleve_id.'" data-competence="'.$competence_id.'" data-ligne="'.$numero_ligne.'" data-colonne="'.$numero_colonne.'" id="L'.$numero_ligne.'C'.$numero_colonne.'" class="crcn_domaine_'.$domaine_id.'" type="text" value="'.$value.'" readonly></td>' ;
          }
          echo $th.'</tr>';
          $th = '';
          $numero_ligne++;
        }
      }
      else
      {
        echo'<tr><th colspan="2">'.$tab_tbody_domaine[0].'</th></tr>';
        unset( $tab_tbody_domaine[0] );
        foreach($tab_tbody_domaine as $competence_id => $th_competence)
        {
          echo'<tr>'.$th_competence;
          $value = !empty($tab_saisies[$sel_eleve][$competence_id]['val']) ? $tab_saisies[$sel_eleve][$competence_id]['val'] : '' ; // pas de saisie ou saisie NULL (supprimée)
          $title = isset($tab_saisies[$sel_eleve][$competence_id]) ? infobulle($tab_saisies[$sel_eleve][$competence_id]['title'],TRUE) : '' ;
          $texte = ($value) ? '<b>Niveau '.$value.'</b>' : '&nbsp;Non&nbsp;renseigné&nbsp;' ;
          echo'<td>'.$texte.' '.$title.'</td>';
          echo'</tr>';
        }
      }
      echo'</tbody>';
    }
    // Javascript
    Layout::add( 'js_inline_before' , 'window.nb_competences = '.($numero_ligne-1).';' );
    Layout::add( 'js_inline_before' , 'window.nb_eleves = '.$numero_colonne.';' );
  ?>
</table>

</form>

<p>&nbsp;</p>

<?php /*  Pour la saisie des notes à la souris */ ?>
<div id="td_souris_container"><div class="td_souris">
  <span data-val="">&nbsp;</span><span data-val="1">1</span><span data-val="2">2</span><br>
  <span data-val="3">3</span><span data-val="4">4</span><span data-val="5">5</span>
</div></div>

<?php /*  Clavier virtuel pour les dispositifs tactiles */ ?>
<div id="cadre_tactile">
<?php
// Images pour une saisie tactile : 4 flèches + 6 notes + ok / del
/*
f1 f2 f3 f4
 x n1 n2 n3
n4 n5 del ok
*/
$tab_ascii = array(
  'fleche' => array(
    'g' => array(  37 , 'Gauche' ),
    'd' => array(  39 , 'Droite' ),
    'h' => array(  38 , 'Haut' ),
    'b' => array(  40 , 'Bas' ),
  ),
  'note_speciale' => array(
    'X'  => array( 46 , 8 ), // suppr backspace
  ),
  'note_usuelle' => array(
    1 => array(  97 , 49 ), // 1 &
    2 => array(  98 , 50 ), // 2 é
    3 => array(  99 , 51 ), // 3 "
    4 => array( 100 , 52 ), // 4 '
    5 => array( 101 , 53 ), // 5 (
  ),
  'action' => array(
    'valider'   => 13,
    'retourner' => 27,
  ),
);
$nb_lignes = 3;
$tab_tactile_ligne = array_fill( 0 , $nb_lignes , '' );
foreach($tab_ascii['fleche'] as $key => $tab_infos)
{
  list( $keycode , $alt ) = $tab_infos;
  $tab_tactile_ligne[0] .= '<span id="kbd_'.$keycode.'"><img alt="'.$alt.'" src="./_img/fleche/fleche_'.$key.'1.gif"></span>';
}
foreach($tab_ascii['note_speciale'] as $alt => $tab_infos)
{
  $ligne = 1;
  $keycode = $tab_infos[0];
  $tab_tactile_ligne[$ligne] .= '<span id="kbd_'.$keycode.'"><img alt="'.$alt.'" src="'.Html::note_src($alt).'"></span>';
}
foreach( $tab_ascii['note_usuelle'] as $val => $tab_infos)
{
  $ligne = 1 + floor( $val / 4 );
  $keycode = $tab_infos[0];
  $tab_tactile_ligne[$ligne] .= '<span id="kbd_'.$keycode.'">'.$val.'</span>';
}
$bloc_retourner = '<span id="kbd_27" class="img retourner"></span>';
$bloc_valider   = '<span id="kbd_13" class="img valider"></span>';
$tab_tactile_ligne[2] .= $bloc_retourner.$bloc_valider;
$tactile_ligne = '<div>'.implode('<div>'."\r\n".'</div>',$tab_tactile_ligne).'</div>';
echo $tactile_ligne;

// Ajout exceptionnel de css
Layout::add( 'css_inline' , '#cadre_tactile {padding-bottom:18px}' );
?>
</div>
