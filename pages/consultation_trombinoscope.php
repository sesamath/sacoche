<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Trombinoscope'));
?>

<?php
$bouton_modifier_groupes = ( ($_SESSION['USER_PROFIL_TYPE']=='professeur') && ($_SESSION['USER_JOIN_GROUPES']=='config') ) ? '<button id="ajouter_groupe" type="button" class="form_ajouter"'.infobulle('Lister tous les regroupements').'>&plusmn;</button><button id="retirer_groupe" type="button" class="form_retirer hide"'.infobulle('Lister mes seuls regroupements').'>&plusmn;</button>' : '' ;

// Fabrication des éléments select du formulaire
$tab_groupes = ($_SESSION['USER_JOIN_GROUPES']=='config') ? DB_STRUCTURE_COMMUN::DB_OPT_groupes_professeur($_SESSION['USER_ID']) : DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl() ;
$select_groupe          = HtmlForm::afficher_select($tab_groupes , 'f_groupe'          /*select_nom*/ , ''    /*option_first*/ , FALSE /*selection*/ , 'regroupements' /*optgroup*/ );
$select_groupe_multiple = HtmlForm::afficher_select($tab_groupes , 'f_groupe_multiple' /*select_nom*/ , FALSE /*option_first*/ , FALSE /*selection*/ , 'regroupements' /*optgroup*/ , TRUE /*multiple*/ , TRUE /*filter*/ );
?>

<ul class="puce">
  <li><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_administrateur__photos_eleves">DOC : Photos des élèves</a></span></li>
  <li><span class="danger">Respectez les conditions légales d’utilisation (droit à l’image précisé dans la documentation ci-dessus).</span></li>
</ul>

<hr>

<form action="#" method="post" id="form_choix"><fieldset>
  <label class="tab" for="f_choix">Fonctionnalité :</label>
  <select id="f_choix">
    <option value="">&nbsp;</option>
    <option value="voir_regroupement">Afficher / Imprimer pour une classe ou un groupe</option>
    <option value="deviner_regroupement">Jouer à deviner les prénoms pour une classe ou un groupe</option>
    <option value="generer_pdf_multiple">Générer un PDF avec plusieurs classes ou groupes</option>
  </select>
</fieldset></form>

<hr>

<div id="affichage_formulaire" class="hide">

  <h2 id="h2_choix"></h2>

  <form action="#" method="post" id="form_select_groupe"><fieldset>
    <label class="tab" for="f_groupe">Regroupement :</label><?php echo $bouton_modifier_groupes ?><?php echo $select_groupe ?> <label id="ajax_msg_groupe">&nbsp;</label>
  </fieldset></form>

  <form action="#" method="post" id="form_select_multiple"><fieldset>
    <label class="tab" for="f_groupe_multiple">Regroupements :</label><?php echo $select_groupe_multiple; ?>
    <p><span class="tab"></span><button id="bouton_valider_multiple" type="button" class="valider">Valider.</button><label id="ajax_msg_multiple">&nbsp;</label></p>
  </fieldset></form>

</div>

<p id="lien_telechargement_groupe"></p>
<p id="lien_telechargement_multiple"></p>
<div id="zone_affichage"></div>
<div id="zone_jeu"></div>

