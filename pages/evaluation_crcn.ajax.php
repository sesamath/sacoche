<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO){Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action = Clean::post('f_action', 'texte');

$tab_eleves      = Clean::post('f_eleve_liste'     , array('array',','));
$tab_competences = Clean::post('f_competence_liste', array('array',','));
$tab_niveaux     = Clean::post('f_niveau_liste'    , array('array',','));

$tab_eleves      = array_filter( Clean::map('entier',$tab_eleves)      , 'positif' );
$tab_competences = array_filter( Clean::map('entier',$tab_competences) , 'positif' );
$tab_niveaux     = array_filter( Clean::map('entier',$tab_niveaux)     , 'positif_ou_nul' );

$nb_eleves      = count($tab_eleves);
$nb_competences = count($tab_competences);
$nb_niveaux     = count($tab_niveaux);

// Vérification des données transmises
if( ($action!='enregistrer') || !$nb_eleves || !$nb_competences || !$nb_niveaux || ($nb_eleves!=$nb_competences) || ($nb_eleves!=$nb_niveaux) || (max($tab_niveaux)>8) )
{
  Json::end( FALSE , 'Erreur avec les données transmises !'.$nb_eleves.'/'.$nb_competences.'/'.$nb_niveaux );
}

$tab_eleves_sans_doublon = array_unique($tab_eleves);
$listing_eleve_id = implode(',',$tab_eleves_sans_doublon);

// On vérifie que ce sont bien les élèves du professeur
if($_SESSION['USER_JOIN_GROUPES']=='config')
{
  
  Outil::verif_eleves_prof( $tab_eleves_sans_doublon );
}

// Fermeture de session (mais pas destruction, juste écriture et libération des données pour éviter un verrouillage en écriture)
Session::write_close();

// Récupérer les données en base afin de les comparer à ce qui est transmis
$tab_saisies_bdd = array();
$DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_crcn_saisies( $listing_eleve_id , FALSE /*only_positif*/ );
foreach($DB_TAB as $DB_ROW)
{
  // On remplace NULL par 0 sinon en PHP il n’est pas possible de déterminer si une variable vaut NULL sans savoir si elle est définie
  // ( isset() comme empty() renvoient FALSE sur NULL et is_null() renvoie une erreur si la variable n’est pas définie )
  $tab_saisies_bdd[$DB_ROW['eleve_id']][$DB_ROW['crcn_competence_id']] = intval($DB_ROW['crcn_niveau_numero']);
}

// On passe à la comparaison
// On ne fait rien si le niveau reste le même (dont le cas où on ne transmet pas de niveau et qu'il n'y en avait déjà pas)
foreach($tab_eleves as $key => $eleve_id)
{
  $competence_id = $tab_competences[$key];
  $niveau_numero = ($tab_niveaux[$key]) ? $tab_niveaux[$key] : NULL ;
  if( !isset($tab_saisies_bdd[$eleve_id][$competence_id]) )
  {
    if( $niveau_numero )
    {
      DB_STRUCTURE_LIVRET::DB_ajouter_crcn_saisies( $eleve_id , $competence_id , $niveau_numero , $_SESSION['USER_ID'] );
    }
  }
  else
  {
    if( $tab_saisies_bdd[$eleve_id][$competence_id] != $niveau_numero )
    {
      DB_STRUCTURE_LIVRET::DB_modifier_crcn_saisies( $eleve_id , $competence_id , $niveau_numero , $_SESSION['USER_ID'] );
    }
  }
}

Json::end( TRUE );
?>
