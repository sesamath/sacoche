<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action          = Clean::post('f_action'       , 'texte');
$check           = Clean::post('f_check'        , 'entier');
$id              = Clean::post('f_id'           , 'entier');
$id_ent          = Clean::post('f_id_ent'       , 'id_ent');
$id_gepi         = Clean::post('f_id_gepi'      , 'id_ent');
$sconet_id       = Clean::post('f_sconet_id'    , 'entier');
$sconet_num      = Clean::post('f_sconet_num'   , 'entier');
$reference       = Clean::post('f_reference'    , 'ref');
$profil          = 'ELV';
$genre           = Clean::post('f_genre'        , 'lettres');
$nom             = Clean::post('f_nom'          , 'nom');
$prenom          = Clean::post('f_prenom'       , 'prenom');
$birth_date      = Clean::post('f_birth_date'   , 'date_fr');
$login           = Clean::post('f_login'        , 'login');
$password        = Clean::post('f_password'     , 'password');
$entree_date     = Clean::post('f_entree_date'  , 'date_fr');
$sortie_date     = Clean::post('f_sortie_date'  , 'date_fr');
$box_birth_date  = Clean::post('box_birth_date' , 'bool');
$box_login       = Clean::post('box_login'      , 'bool');
$box_password    = Clean::post('box_password'   , 'bool');
$box_entree_date = Clean::post('box_entree_date', 'bool');
$box_sortie_date = Clean::post('box_sortie_date', 'bool');
$courriel        = Clean::post('f_courriel'     , 'courriel');
$email_refus     = Clean::post('f_email_refus'  , 'entier');
$uai_origine     = Clean::post('f_uai_origine'  , 'uai');
$groupe          = Clean::post('f_groupe'       , 'texte', 'd2');

$groupe_type     = Clean::lettres( substr($groupe,0,1) );
$groupe_id       = Clean::entier( substr($groupe,1) );

$verif_post = isset(Html::$tab_genre['enfant'][$genre]) && $nom && $prenom && ($box_login || $login) && ($box_password || strlen($password)) && ($box_birth_date || $birth_date) && ($box_entree_date || $entree_date) && ($box_sortie_date || $sortie_date) && !is_null($id_ent) && !is_null($id_gepi) && !is_null($sconet_id) && !is_null($sconet_num) && !is_null($reference) && !is_null($courriel) && !is_null($uai_origine);

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter un nouvel élève
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='ajouter') && $verif_post )
{
  // Vérifier que l’identifiant ENT est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_ent)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_ent',$id_ent) )
    {
      Json::end( FALSE , 'Identifiant ENT déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant GEPI est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_gepi)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_gepi',$id_gepi) )
    {
      Json::end( FALSE , 'Identifiant Gepi déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant sconet est disponible (parmi les utilisateurs de même type de profil)
  if($sconet_id)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('sconet_id',$sconet_id,NULL,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Identifiant Sconet déjà utilisé !' );
    }
  }
  // Vérifier que le n° sconet est disponible (parmi les utilisateurs de même type de profil)
  if($sconet_num)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('sconet_elenoet',$sconet_num,NULL,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Numéro Sconet déjà utilisé !' );
    }
  }
  // Vérifier que la référence est disponible (parmi les utilisateurs de même type de profil)
  if($reference)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('reference',$reference,NULL,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Référence déjà utilisée !' );
    }
  }
  if($box_login)
  {
    // Construire puis tester le login (parmi tous les utilisateurs de l’établissement)
    $login = Outil::fabriquer_login($prenom,$nom,$profil);
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login) )
    {
      // Login pris : en chercher un autre en remplaçant la fin par des chiffres si besoin
      $login = DB_STRUCTURE_ADMINISTRATEUR::DB_rechercher_login_disponible($login);
    }
  }
  else
  {
    // Vérifier que le login transmis est disponible (parmi tous les utilisateurs de l’établissement)
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login) )
    {
      Json::end( FALSE , 'Login déjà utilisé !' );
    }
  }
  if($box_password)
  {
    // Générer un mdp aléatoire
    $password = Outil::fabriquer_mdp($profil);
  }
  else
  {
    // Vérifier que le mdp transmis est d’une longueur compatible
    if(mb_strlen($password)<$_SESSION['TAB_PROFILS_ADMIN']['MDP_LONGUEUR_MINI'][$profil])
    {
      Json::end( FALSE , 'Mot de passe trop court pour ce profil !' );
    }
  }
  // Vérifier le domaine du serveur mail seulement en mode multi-structures car ce peut être sinon une installation sur un serveur local non ouvert sur l’extérieur.
  if($courriel)
  {
    if(HEBERGEUR_INSTALLATION=='multi-structures')
    {
      list($mail_domaine,$is_domaine_valide) = Outil::tester_domaine_courriel_valide($courriel);
      if(!$is_domaine_valide)
      {
        Json::end( FALSE , 'Erreur avec le domaine "'.$mail_domaine.'" !' );
      }
    }
  }
  // Cas de la date de naissance
  if($box_birth_date)
  {
    $birth_date = '-' ;
    $birth_date_sql = NULL;
  }
  else
  {
    $birth_date_sql = To::date_french_to_sql($birth_date);
  }
  // Cas de la date d’entrée
  if($box_entree_date)
  {
    $entree_date = '-' ;
    $entree_date_sql = ENTREE_DEFAUT_SQL;
  }
  else
  {
    $entree_date_sql = To::date_french_to_sql($entree_date);
  }
  // Cas de la date de sortie
  if($box_sortie_date)
  {
    $sortie_date = '-' ;
    $sortie_date_sql = SORTIE_DEFAUT_SQL;
  }
  else
  {
    $sortie_date_sql = To::date_french_to_sql($sortie_date);
  }
  if($sortie_date_sql<$entree_date_sql)
  {
    Json::end( FALSE , 'Date de sortie antérieure à la date d’entrée !' );
  }
  $user_email_origine = ($courriel) ? 'admin' : '' ;
  // Insérer l’enregistrement
  $user_id = DB_STRUCTURE_COMMUN::DB_ajouter_utilisateur( $sconet_id , $sconet_num , $reference , $profil , $genre , $nom , $prenom , $birth_date_sql , $courriel , $user_email_origine , $login , Outil::crypter_mdp($password) , $id_ent , $id_gepi , $entree_date_sql , 0 /*eleve_classe_id*/ , $uai_origine );
  // Il peut (déjà !) falloir lui affecter une date de sortie...
  if(!$box_sortie_date)
  {
    DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $user_id , array(':sortie_date'=>$sortie_date_sql) );
  }
  // Si on inscrit un élève après avoir fait afficher une classe ou un groupe, alors on l’affecte dans la classe ou le groupe en question
  $tab_groupe_type = array( 'c'=>'classe' , 'g'=>'groupe' );
  if( isset($tab_groupe_type[$groupe_type]) && $groupe_id )
  {
    DB_STRUCTURE_REGROUPEMENT::DB_modifier_liaison_user_groupe_par_admin( $user_id , 'eleve' , $groupe_id , $tab_groupe_type[$groupe_type] , TRUE );
  }
  // Afficher le retour
  Json::add_str('<tr id="id_'.$user_id.'" class="new">');
  Json::add_str(  '<td class="nu"><input type="checkbox" name="f_ids" value="'.$user_id.'"></td>');
  Json::add_str(  '<td class="label">'.html($id_ent).'</td>');
  Json::add_str(  '<td class="label">'.html($id_gepi).'</td>');
  Json::add_str(  '<td class="label">'.html($sconet_id).'</td>');
  Json::add_str(  '<td class="label">'.html($sconet_num).'</td>');
  Json::add_str(  '<td class="label">'.html($reference).'</td>');
  Json::add_str(  '<td class="label">'.Html::$tab_genre['enfant'][$genre].'</td>');
  Json::add_str(  '<td class="label">'.html($nom).'</td>');
  Json::add_str(  '<td class="label">'.html($prenom).'</td>');
  Json::add_str(  '<td class="label">'.$birth_date.'</td>');
  Json::add_str(  '<td class="label new">'.html($login).' '.infobulle('Pensez à relever le login généré !',TRUE).'</td>');
  Json::add_str(  '<td class="label new">'.html($password).' '.infobulle('Pensez à noter le mot de passe !',TRUE).'</td>');
  Json::add_str(  '<td class="label">'.html($courriel).'</td>');
  Json::add_str(  '<td class="label">'.html($uai_origine).'</td>');
  Json::add_str(  '<td class="label">'.$entree_date.'</td>');
  Json::add_str(  '<td class="label">'.$sortie_date.'</td>');
  Json::add_str(  '<td class="nu">');
  Json::add_str(    '<q class="modifier"'.infobulle('Modifier cet élève.').'></q>');
  Json::add_str(  '</td>');
  Json::add_str('</tr>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier un élève existant
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier') && $id && $verif_post )
{
  $tab_donnees = array();
  // Vérifier que l’identifiant ENT est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_ent)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_ent',$id_ent,$id) )
    {
      Json::end( FALSE , 'Identifiant ENT déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant GEPI est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_gepi)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_gepi',$id_gepi,$id) )
    {
      Json::end( FALSE , 'Identifiant Gepi déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant sconet est disponible (parmi les utilisateurs de même type de profil)
  if($sconet_id)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('sconet_id',$sconet_id,$id,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Identifiant Sconet déjà utilisé !' );
    }
  }
  // Vérifier que le n° sconet est disponible (parmi les utilisateurs de même type de profil)
  if($sconet_num)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('sconet_elenoet',$sconet_num,$id,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Numéro Sconet déjà utilisé !' );
    }
  }
  // Vérifier que la référence est disponible (parmi les utilisateurs de même type de profil)
  if($reference)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('reference',$reference,$id,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Référence déjà utilisée !' );
    }
  }
  // Vérifier que le login transmis est disponible (parmi tous les utilisateurs de l’établissement)
  if(!$box_login)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login,$id) )
    {
      Json::end( FALSE , 'Login déjà utilisé !' );
    }
    $tab_donnees[':login'] = $login;
  }
  // Vérifier le domaine du serveur mail seulement en mode multi-structures car ce peut être sinon une installation sur un serveur local non ouvert sur l’extérieur.
  if($courriel)
  {
    if(HEBERGEUR_INSTALLATION=='multi-structures')
    {
      list($mail_domaine,$is_domaine_valide) = Outil::tester_domaine_courriel_valide($courriel);
      if(!$is_domaine_valide)
      {
        Json::end( FALSE , 'Erreur avec le domaine "'.$mail_domaine.'" !' );
      }
    }
    // Vérifier aussi que l’adresse n’est pas répertoriée comme étant en erreur.
    if(DB_STRUCTURE_COURRIEL_ERREUR::DB_tester_courriel($courriel))
    {
      Json::end( FALSE , 'Courriel répertorié comme étant en erreur !' );
    }
    $tab_donnees[':email_origine'] = 'admin';
  }
  else
  {
    $tab_donnees[':email_origine'] = '';
  }
  // Cas du mot de passe
  if(!$box_password)
  {
    $tab_donnees[':password'] = Outil::crypter_mdp($password);
  }
  // Cas de la date de naissance
  if($box_birth_date)
  {
    $birth_date = '-' ;
    $birth_date_sql = NULL;
  }
  else
  {
    $birth_date_sql = To::date_french_to_sql($birth_date);
  }
  // Cas de la date d’entrée
  if($box_entree_date)
  {
    $entree_date = '-' ;
    $entree_date_sql = ENTREE_DEFAUT_SQL;
  }
  else
  {
    $entree_date_sql = To::date_french_to_sql($entree_date);
  }
  // Cas de la date de sortie
  if($box_sortie_date)
  {
    $sortie_date = '-' ;
    $sortie_date_sql = SORTIE_DEFAUT_SQL;
  }
  else
  {
    $sortie_date_sql = To::date_french_to_sql($sortie_date);
  }
  if($sortie_date_sql<$entree_date_sql)
  {
    Json::end( FALSE , 'Date de sortie antérieure à la date d’entrée !' );
  }
  // Mettre à jour l’enregistrement
  $tab_donnees += array(
    ':sconet_id'   => $sconet_id,
    ':sconet_num'  => $sconet_num,
    ':reference'   => $reference,
    ':genre'       => $genre,
    ':nom'         => $nom,
    ':prenom'      => $prenom,
    ':birth_date'  => $birth_date_sql,
    ':courriel'    => $courriel,
    ':id_ent'      => $id_ent,
    ':id_gepi'     => $id_gepi,
    ':uai_origine' => $uai_origine,
    ':entree_date' => $entree_date_sql,
    ':sortie_date' => $sortie_date_sql,
  );
  DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $id , $tab_donnees );
  // En cas de sortie d’un élève, retirer les notes AB etc ultérieures à cette date de sortie, afin d’éviter des bulletins totalement vides
  if(!$box_sortie_date)
  {
    DB_STRUCTURE_ADMINISTRATEUR::DB_supprimer_user_saisies_absences_apres_sortie( $id , $sortie_date_sql );
  }
  // Afficher le retour
  $mail_bulle = ($email_refus) ? 'Envoi de courriel refusé par le destinataire.' : '' ;
  $mail_class = ($email_refus) ? ' notnow' : '' ;
  $checked = ($check) ? ' checked' : '' ;
  Json::add_str('<td class="nu"><input type="checkbox" name="f_ids" value="'.$id.'"'.$checked.'></td>');
  Json::add_str('<td class="label">'.html($id_ent).'</td>');
  Json::add_str('<td class="label">'.html($id_gepi).'</td>');
  Json::add_str('<td class="label">'.html($sconet_id).'</td>');
  Json::add_str('<td class="label">'.html($sconet_num).'</td>');
  Json::add_str('<td class="label">'.html($reference).'</td>');
  Json::add_str('<td class="label">'.Html::$tab_genre['enfant'][$genre].'</td>');
  Json::add_str('<td class="label">'.html($nom).'</td>');
  Json::add_str('<td class="label">'.html($prenom).'</td>');
  Json::add_str('<td class="label">'.$birth_date.'</td>');
  Json::add_str('<td class="label">'.html($login).'</td>');
  Json::add_str( ($box_password) ? '<td class="label i">champ crypté</td>' : '<td class="label new">'.$password.' '.infobulle('Pensez à noter le mot de passe !',TRUE).'</td>');
  Json::add_str('<td class="label'.$mail_class.'"'.infobulle($mail_bulle).'>'.html($courriel).'</td>');
  Json::add_str('<td class="label">'.html($uai_origine).'</td>');
  Json::add_str('<td class="label">'.$entree_date.'</td>');
  Json::add_str('<td class="label">'.$sortie_date.'</td>');
  Json::add_str('<td class="nu">');
  Json::add_str(  '<q class="modifier"'.infobulle('Modifier cet élève.').'></q>');
  Json::add_str('</td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Retirer des comptes
// Réintégrer des comptes
// Supprimer des comptes
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( in_array( $action , array('retirer','reintegrer','supprimer') ) )
{
  require(CHEMIN_DOSSIER_INCLUDE.'code_administrateur_comptes.php');
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
