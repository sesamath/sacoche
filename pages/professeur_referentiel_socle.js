/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// Variables globales à ne pas définir plus tard sinon la minification les renomme et cela pose ensuite souci.
var tab_retour = [];

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var matiere_id = 0;
    var niveau_id  = 0;

    if(window.matiere_nb==1)
    {
      $('#sousmenu_matiere').find('a').click();
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Obliger l’affichage de l’ascenseur vertical car son apparition / disparition en fonction des affichages peut modifier la position des blocs des sous-menus
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#force_scroll').css('height',screen.height);

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Afficher / masquer des colonnes
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Agir sur toutes les cellules du DOM est trop long (plusieurs secondes d’attente, même avec une commande optimale ne se basant que sur les ids).
    // La solution passe par la récupération du DOM dans une chaine, la modification de cette chaine, puis sa réinjection dans la page (effet quasi instantané !)
    function visualiser_colonnes( objet_modif , objet_id )
    {
      var table_html = $('#table_action').html();
      for ( var cycle_key in window.tab_cycle_used )
      {
        for ( var domaine_key in window.tab_domaine_used )
        {
          if( (objet_modif=='init') || ( (objet_modif=='cycle') && (cycle_key==objet_id) ) || ( (objet_modif=='domaine') && (domaine_key==objet_id) ) )
          {
            if( window.tab_cycle_used[cycle_key] && window.tab_domaine_used[domaine_key] )
            {
              table_html = table_html.replaceAll( 'hide" id="id_'+cycle_key+'_'+domaine_key+'_' , 'show" id="id_'+cycle_key+'_'+domaine_key+'_' );
            }
            else
            {
              table_html = table_html.replaceAll( 'show" id="id_'+cycle_key+'_'+domaine_key+'_' , 'hide" id="id_'+cycle_key+'_'+domaine_key+'_' );
            }
          }
        }
      }
      $('#table_action').html(table_html);
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gestion du clic sur une matière
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sousmenu_matiere a').click
    (
      function()
      {
        $('#table_action').hide(0);
        // sous-menu matière
        $('#sousmenu_matiere a').removeAttr('class');
        $(this).addClass('actif');
        matiere_id = $(this).data('matiere');
        // sous-menu niveau
        var nb_niveau = 0;
        $('#sousmenu_niveau a').addClass('disabled');
        for ( var niveau_key in window.tab_niveau_for_matiere[matiere_id] )
        {
          $('#niveau_'+niveau_key).removeAttr('class');
          nb_niveau++;
        }
        if(nb_niveau==1)
        {
          $('#niveau_'+niveau_key).click();
        }
        else if( typeof(window.tab_niveau_for_matiere[matiere_id][niveau_id]) !== undefined )
        {
          $('#niveau_'+niveau_id).click();
        }
        else
        {
          niveau_id = 0;
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gestion du clic sur un niveau
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sousmenu_niveau a').click
    (
      function()
      {
        if(!$(this).hasClass('disabled'))
        {
          $('#table_action').hide(0);
          niveau_id = $(this).data('niveau');
          for ( var niveau_key in window.tab_niveau_for_matiere[matiere_id] )
          {
            if(niveau_key==niveau_id)
            {
              $('#niveau_'+niveau_key).addClass('actif');
            }
            else
            {
              $('#niveau_'+niveau_key).removeAttr('class');
            }
          }
          charger_referentiel(matiere_id,niveau_id);
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gestion du clic sur un cycle
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sousmenu_cycle a').click
    (
      function()
      {
        var cycle_id = $(this).data('cycle');
        var cycle_used = window.tab_cycle_used[cycle_id];
        if(cycle_used)
        {
          $(this).removeAttr('class');
        }
        else
        {
          $(this).addClass('actif');
        }
        window.tab_cycle_used[cycle_id] = 1 - cycle_used;
        visualiser_colonnes( 'cycle' , cycle_id );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gestion du clic sur un domaine
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#sousmenu_domaine a').click
    (
      function()
      {
        var domaine_id = $(this).data('domaine');
        var domaine_used = window.tab_domaine_used[domaine_id];
        if(domaine_used)
        {
          $(this).removeAttr('class');
        }
        else
        {
          $(this).addClass('actif');
        }
        window.tab_domaine_used[domaine_id] = 1 - domaine_used;
        visualiser_colonnes( 'domaine' , domaine_id );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger un référentiel
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function charger_referentiel(matiere_id,niveau_id)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action='+'charger_referentiel'+'&f_matiere='+matiere_id+'&f_niveau='+niveau_id,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              tab_retour = JSON.parse(responseJSON['tab_retour']);
              // Fabriquer le contenu des lignes à partir des infos retournées 
              for ( var key in tab_retour )
              {
                if(!tab_retour[key]['id'])
                {
                  var cellules_affichage = window.cellules_domaine.replaceAll('{KEY}',key);
                  tab_retour[key] = '<tr><th class="wpn">'+tab_retour[key]['nom']+'</th>'+cellules_affichage+'</tr>';
                }
                else
                {
                  var id = tab_retour[key]['id'];
                  var cellules_affichage = window.cellules_item.replaceAll('{KEY}',id);
                  // Cocher les cases
                  if(tab_retour[key]['socle'] !== null)
                  {
                    for ( var i in tab_retour[key]['socle'] )
                    {
                      var cycle      = tab_retour[key]['socle'][i]['cycle'];
                      var domaine    = tab_retour[key]['socle'][i]['domaine'];
                      var composante = tab_retour[key]['socle'][i]['composante'];
                      cellules_affichage = cellules_affichage.replace( 'class="hc br show" id="id_'+cycle+'_'+domaine+'_'+composante+'_'+id+'"><input' , 'class="hc bv show" id="id_'+cycle+'_'+domaine+'_'+composante+'_'+id+'"><input checked' );
                    }
                  }
                  tab_retour[key] = '<tr id="item_'+tab_retour[key]['id']+'"><td class="wpn">'+tab_retour[key]['nom']+'</td>'+cellules_affichage+'</tr>';
                }
              }
              $('#table_action').html('<tbody>'+tab_retour.join()+'</tbody>');
              visualiser_colonnes( 'init' , 0 );
              $('#table_action').show(0);
            }
            else
            {
              $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
            }
          }
        }
      );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Modification d’une liaison au socle
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'input',
      function()
      {
        var objet_input = $(this);
        var objet_td    = objet_input.parent();
        var ids         = objet_td.attr('id');
        var etat        = (objet_input.is(':checked')) ? 1 : 0 ;
        var classe      = (etat) ? 'hc bv show' : 'hc br show' ;
        var tab_infos   = ids.split('_');
        var cycle       = tab_infos[1];
        var domaine     = tab_infos[2];
        var composante  = tab_infos[3];
        var item        = tab_infos[4];
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'modifier_etat'+'&f_cycle='+cycle+'&f_composante='+composante+'&f_item='+item+'&f_etat='+etat,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==true)
              {
                objet_td.attr('class',classe);
              }
              else
              {
                var etat_avant = (etat) ? false : true ;
                objet_input.prop('checked',etat_avant);
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
            }
          }
        );
      }
    );

  }
);
