/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Vérifier si le masque saisi est correct
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function test_masque()
    {
      var masque = $('#f_masque').val();
      // Curieusement, besoin d’échapper l’échappement... (en PHP un échappement simple suffit)
      var reg_filename  = new RegExp('\\[(sconet_id|sconet_num|reference|nom|prenom|login|ent_id)\\]','g');
      var reg_extension = new RegExp('\\.(gif|jpg|jpeg|png)$','g');
      return( reg_filename.test(masque) && reg_extension.test(masque) );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Actualiser l’affichage des vignettes élèves au changement du select
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_affichage(scroll_after)
    {
      $('#liste_eleves').html('');
      // On récupère le regroupement
      var groupe_val = $('#f_groupe option:selected').val();
      if(!groupe_val)
      {
        $('#ajax_msg').removeAttr('class').html('');
        return false
      }
      // Pour un directeur ou un administrateur, groupe_val est de la forme d3 / n2 / c51 / g44
      if(isNaN(entier(groupe_val)))
      {
        var groupe_type = groupe_val.substring(0,1);
        var groupe_id   = groupe_val.substring(1);
      }
      // Pour un professeur, groupe_val est un entier, et il faut récupérer la 1ère lettre du label parent
      else
      {
        var groupe_type = $('#f_groupe option:selected').parent().attr('label').substring(0,1).toLowerCase();
        var groupe_id   = groupe_val;
      }
      $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=afficher'+'&f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==false)
            {
              $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
            }
            else
            {
              $('#ajax_msg').attr('class','valide').html('Demande réalisée !');
              $('#liste_eleves').html(responseJSON['value']);
              if(scroll_after)
              {
                window.scrollTo(0,10000);
              }
            }
          }
        }
      );
    }

    $('#f_groupe').change
    (
      function()
      {
        maj_affichage( true /*scroll_after*/ );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire form_photos
    // Upload d’un fichier (avec jquery.form.js)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire_photos = $('#form_photos');

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions_photos =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_photos',
      error : retour_form_erreur_photos,
      success : retour_form_valide_photos
    };

    // Vérifications précédant l’envoi du formulaire, déclenchées au choix d’un fichier
    $('#f_photos').change
    (
      function()
      {
        var file = this.files[0];
        if( typeof(file) == 'undefined' )
        {
          $('#ajax_msg_photos').removeAttr('class').html('');
          return false;
        }
        else
        {
          if( !test_masque() )
          {
            $('#f_photos').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
            $('#ajax_msg_photos').attr('class','erreur').html('Indiquer correctement la forme des noms des fichiers contenus dans l’archive.');
            $('#f_masque').focus();
            return false;
          }
          else
          {
            var fichier_nom = file.name;
            var fichier_ext = fichier_nom.split('.').pop().toLowerCase();
            if( fichier_ext != 'zip' )
            {
              $('#ajax_msg_photos').attr('class','erreur').html('Le fichier "'+escapeHtml(fichier_nom)+'" n’a pas l’extension zip.');
              return false;
            }
            else
            {
              $('button').prop('disabled',true);
              $('#ajax_msg_photos').attr('class','loader').html('En cours&hellip;');
              formulaire_photos.submit();
            }
          }
        }
      }
    );

    // Envoi du formulaire (avec jquery.form.js)
    formulaire_photos.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions_photos);
        return false;
      }
    );

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur_photos(jqXHR, textStatus, errorThrown)
    {
      $('#f_photos').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('button').prop('disabled',false);
      $('#ajax_msg_photos').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide_photos(responseJSON)
    {
      $('#f_photos').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('button').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_photos').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        initialiser_compteur();
        $('#ajax_msg_photos').attr('class','valide').html('Demande traitée !');
        $.fancybox( { href:responseJSON['value'] , type:'iframe' , width:'80%' , height:'80%' } );
        maj_affichage( false /*scroll_after*/ );
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Upload de fichiers images par glisser-deposer (avec jQuery Ajax File Uploader Widget)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var count_success = -1;
    
    $('#zone_drop').dmUploader({
      url : 'ajax.php?page='+window.PAGE,
      dataType : 'json',
      extraData : { 'csrf' : window.CSRF , 'f_action' : 'envoyer_image' },
      fieldName : 'userfile',
      maxFileSize : 500000, // 500 ko maxi par photo
      allowedTypes: 'image/*',
      extFilter: ['jpg', 'jpeg', 'png', 'gif'],
      onDragEnter: function(){this.addClass('hover');},
      onDragLeave: function(){this.removeClass('hover');},
      onNewFile: function(id, file){
        if(count_success<0)
        {
          if( !test_masque() )
          {
            $('#ajax_msg_photos').attr('class','erreur').html('Indiquer correctement la forme des noms des fichiers.');
            $('#f_masque').focus();
            return false;
          }
          else
          {
            $('#ajax_msg_photos').removeAttr('class').html('');
            count_success++; // le passer à zéro
            $.fancybox( '<ul id="multi_upload" class="puce"></ul>' , { minWidth:600 } );
          }
        }
        $('#multi_upload').prepend('<li>'+file.name+' <label id="label_'+id+'" class="loader">En attente...</label></li>');
      },
      onBeforeUpload: function(id){$('#label_'+id).html('Démarrage...');},
      onUploadProgress: function(id, percent){$('#label_'+id).html('En cours '+percent+'% ...');},
      onUploadSuccess: function(id, responseJSON){
        if(responseJSON['statut']==false)
        {
          $('#label_'+id).attr('class','erreur').html(responseJSON['value']);
        }
        else
        {
          initialiser_compteur();
          $('#label_'+id).attr('class','valide').html('Upload terminé.');
          count_success++;
        }
      },
      onUploadError: function(id, xhr, status, jqXHR, textStatus, errorThrown){
        $('#label_'+id).attr('style','color:red').html(afficher_json_message_erreur(jqXHR,textStatus));
      },
      onFallbackMode: function(){
        $.fancybox( '<label class="alerte">Navigateur incompatible (trop ancien ?) avec cette fonctionnalité.</label>' );
      },
      onFileSizeError: function(file){
        if(count_success<0)
        {
          count_success++; // le passer à zéro
          $.fancybox( '<ul id="multi_upload" class="puce"></ul>' , { minWidth:600 } );
        }
        $('#multi_upload').prepend('<li>'+file.name+' <label class="alerte">Taille &gt; 500 Ko !</label></li>');
      },
      onFileTypeError: function(file){
        if(count_success<0)
        {
          count_success++; // le passer à zéro
          $.fancybox( '<ul id="multi_upload" class="puce"></ul>' , { minWidth:600 } );
        }
        $('#multi_upload').prepend('<li>'+file.name+' <label class="alerte">Type de fichier non autorisé !</label></li>');
      },
      onFileExtError: function(file){
        if(count_success<0)
        {
          count_success++; // le passer à zéro
          $.fancybox( '<ul id="multi_upload" class="puce"></ul>' , { minWidth:600 } );
        }
        $('#multi_upload').prepend('<li>'+file.name+' <label class="alerte">Extension de fichier non autorisée !</label></li>');
      },
      onComplete: function(){
        if(count_success)
        {
          $('#multi_upload').prepend('<li>Traitement des fichiers reçus <label id="label_ajax" class="loader">En cours&hellip;</label></li>');
          var masque = $('#f_masque').val();
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&f_action=traiter_images'+'&f_masque='+encodeURIComponent(masque),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#label_ajax').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                if(responseJSON['statut']==false)
                {
                  $('#label_ajax').attr('class','alerte').html(responseJSON['value']);
                }
                else
                {
                  $('#label_ajax').parent().remove();
                  $('<ul class="puce">'+responseJSON['value']+'</ul><hr>').insertBefore('#multi_upload');
                  maj_affichage( false /*scroll_after*/ );
                }
              }
            }
          );
        }
        count_success = -1;
        // $('#zone_drop').dmUploader('reset'); // retiré car sinon redéclenche onComplete et avec count_success > 0 ! mystère...
      }
    });

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire #form_photo
    // Upload d’un fichier (avec jquery.form.js)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire_photo = $('#form_photo');

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions_photo =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      error : retour_form_erreur_photo,
      success : retour_form_valide_photo
    };

    // Vérifications précédant l’envoi du formulaire, déclenchées au choix d’un fichier
    $('#f_photo').change
    (
      function()
      {
        var file = this.files[0];
        if( typeof(file) == 'undefined' )
        {
          $('#ajax_msg').removeAttr('class').html('');
          return false;
        }
        else
        {
          var fichier_nom = file.name;
          var fichier_ext = fichier_nom.split('.').pop().toLowerCase();
          if( '.gif.jpg.jpeg.png.'.indexOf('.'+fichier_ext+'.') == -1 )
          {
            $('#ajax_msg').attr('class','erreur').html('Le fichier "'+escapeHtml(fichier_nom)+'" n’a pas une extension autorisée (gif jpg jpeg png).');
            return false;
          }
          else
          {
            $('#form_select').find('q').hide(0);
            $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
            formulaire_photo.submit();
          }
        }
      }
    );

    // Envoi du formulaire (avec jquery.form.js)
    formulaire_photo.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions_photo);
        return false;
      }
    );

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur_photo(jqXHR, textStatus, errorThrown)
    {
      $('#f_photo').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#form_select').find('q').show(0);
      $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide_photo(responseJSON)
    {
      $('#f_photo').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#form_select').find('q').show(0);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        initialiser_compteur();
        var user_id    = responseJSON['user_id'];
        var img_width  = responseJSON['img_width'];
        var img_height = responseJSON['img_height'];
        var img_src    = responseJSON['img_src'];
        $('#ajax_msg').removeAttr('class').html('');
        $('#q_'+user_id).parent().html('<img width="'+img_width+'" height="'+img_height+'" src="'+img_src+'" alt=""><q class="supprimer"'+infobulle('Supprimer cette photo (aucune confirmation ne sera demandée).')+'></q>');
      }
    }

    $('#liste_eleves').on
    (
      'click',
      'q.ajouter',
      function()
      {
        var q_id = $(this).attr('id');
        var user_id = q_id.substring(2); // "q_" + id
        $('#f_user_id').val(user_id);
        $('#f_photo').click();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Appel en ajax pour supprimer une photo
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#liste_eleves').on
    (
      'click',
      'q.supprimer',
      function()
      {
        var memo_div = $(this).parent();
        var user_id = memo_div.parent().attr('id').substring(4); // "div_" + id
        $('#form_select').find('q').hide(0);
        $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=supprimer_photo'+'&f_user_id='+user_id,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#form_select').find('q').show(0);
              $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              $('#form_select').find('q').show(0);
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg').removeAttr('class').html('');
                memo_div.html('<q id="q_'+user_id+'" class="ajouter"'+infobulle('Ajouter une photo.')+'></q><img width="1" height="1" src="./_img/auto.gif">');
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Supprimer les photos de plusieurs regroupements d’un coup
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_supprimer_multiple').click
    (
      function()
      {
        var nb_coches = $('#f_groupe_multiple input:checked:enabled').length;
        if( !nb_coches )
        {
          $('#ajax_msg_multiple').attr('class','erreur').html('Sélectionnez au moins un regroupement !');
          return false;
        }
        var precision_nombre = (nb_coches>1) ? 'des '+nb_coches+' regroupements cochés' : 'du regroupement coché' ;
        $.prompt(
          'Souhaitez-vous vraiment SUPPRIMER les photos des élèves '+precision_nombre+' ?',
          {
            title   : 'Demande de confirmation',
            buttons :
            {
              'Non, c’est une erreur !' : false ,
              'Oui, je veux toutes les supprimer !' : true
            },
            submit  : function(event, value, message, formVals)
            {
              if(value)
              {
                $('#form_select_multiple button').prop('disabled',true);
                $('#ajax_msg_multiple').attr('class','loader').html('En cours&hellip;');
                // Grouper les checkbox dans un champ unique afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
                var tab_groupe_multiple = [];
                $('#f_groupe_multiple input:checked:enabled').each
                (
                  function()
                  {
                    tab_groupe_multiple.push($(this).val());
                  }
                );
                $.ajax
                (
                  {
                    type : 'POST',
                    url : 'ajax.php?page='+window.PAGE,
                    data : 'csrf='+window.CSRF+'&f_action=supprimer_multiple'+'&f_groupe_multiple='+tab_groupe_multiple,
                    dataType : 'json',
                    error : function(jqXHR, textStatus, errorThrown)
                    {
                      $('#form_select_multiple button').prop('disabled',false);
                      $('#ajax_msg_multiple').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                      return false;
                    },
                    success : function(responseJSON)
                    {
                      initialiser_compteur();
                      $('#form_select_multiple button').prop('disabled',false);
                      if(responseJSON['statut']==true)
                      {
                        $('#ajax_msg_multiple').attr('class','valide').html(responseJSON['value']);
                        if( $('#f_groupe option:selected').val() )
                        {
                          maj_affichage( false /*scroll_after*/ );
                        }
                      }
                      else
                      {
                        $('#ajax_msg_multiple').attr('class','alerte').html(responseJSON['value']);
                      }
                    }
                  }
                );
              }
            }
          }
        );
      }
    );

    $('#form_select_multiple input').change
    (
      function()
      {
        $('#ajax_msg_multiple').removeAttr('class').html('');
        $('#bilan_multiple').html('');
      }
    );

  }
);
