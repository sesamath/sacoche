/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{2:{sorter:false}} });
    var tableau_tri = function(){ $('#table_action').trigger( 'sorton' , [ [[0,0],[1,0]] ] ); };
    var tableau_maj = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    tableau_tri();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonctions utilisées
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Ajouter un remplaçant : mise en place du formulaire
     * @return void
     */
    var ajouter = function()
    {
      $('#f_absent_id     option:first').prop('selected',true);
      $('#f_remplacant_id option:first').prop('selected',true);
      $('#ajax_msg_gestion').removeAttr('class').html('');
      $.fancybox( { href:'#form_gestion' , modal:true , minWidth:600 } );
    };

    /**
     * Annuler une action
     * @return void
     */
    var annuler = function()
    {
      $.fancybox.close();
      mode = false;
    };

    /**
     * Intercepter la touche entrée ou escape pour valider ou annuler les modifications
     * @return void
     */
    function intercepter(e)
    {
      if(mode)
      {
        if(e.which==13)  // touche entrée
        {
          $('#bouton_valider').click();
        }
        else if(e.which==27)  // touche escape
        {
          $('#bouton_annuler').click();
        }
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Appel en ajax pour supprimer un remplacement
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var supprimer = function()
    {
      var objet_tr   = $(this).parent().parent();
      var objet_tds  = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var absent_id      = objet_tds.eq(0).data('id');
      var remplacant_id  = objet_tds.eq(1).data('id');
      objet_tr.hide();
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=supprimer'+'&f_absent_id='+absent_id+'&f_remplacant_id='+remplacant_id,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            objet_tr.show();
            $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('#form_select').find('q').show(0);
              if(responseJSON['statut']==true)
              {
                objet_tr.remove();
              }
              else
              {
                objet_tr.show();
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
            }
          }
        }
      );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Appel en ajax pour ajouter un remplacement
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var valider_ajouter = function()
    {
      // Récupérer les informations
      var absent_id     = $('#f_absent_id     option:selected').val();
      var remplacant_id = $('#f_remplacant_id option:selected').val();
      if(!absent_id)
      {
        $('#ajax_msg_gestion').attr('class','erreur').html('Sélectionner le professeur absent !');
      }
      else if(!remplacant_id)
      {
        $('#ajax_msg_gestion').attr('class','erreur').html('Sélectionner le professeur remplaçant !');
      }
      else if( absent_id == remplacant_id )
      {
        $('#ajax_msg_gestion').attr('class','erreur').html('Sélectionner des professeurs différents !');
      }
      else
      {
        var absent_nom     = $('#f_absent_id     option:selected').text();
        var remplacant_nom = $('#f_remplacant_id option:selected').text();
        $('#form_gestion button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=ajouter'+'&f_absent_id='+absent_id+'&f_remplacant_id='+remplacant_id+'&f_absent_nom='+encodeURIComponent(absent_nom)+'&f_remplacant_nom='+encodeURIComponent(remplacant_nom),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#form_gestion button').prop('disabled',false);
              $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_gestion button').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
                $('#table_action tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
                $('#table_action tbody').prepend(responseJSON['value']);
                tableau_maj();
                $.fancybox.close();
              }
            }
          }
        );
      }
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Appel des fonctions en fonction des événements
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on( 'click' , 'q.ajouter'   , ajouter );
    $('#table_action').on( 'click' , 'q.supprimer' , supprimer );

    $('#form_gestion').on( 'click'   , '#bouton_annuler' , annuler );
    $('#form_gestion').on( 'click'   , '#bouton_valider' , valider_ajouter );
    $('#form_gestion').on( 'keydown' , 'input,select'    , function(e){intercepter(e);} );

  }
);
