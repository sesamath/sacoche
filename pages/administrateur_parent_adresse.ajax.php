<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action      = Clean::post('f_action'     , 'texte');
$user_id     = Clean::post('f_id'         , 'entier');
$ligne1      = Clean::post('f_ligne1'     , 'adresse');
$ligne2      = Clean::post('f_ligne2'     , 'adresse');
$ligne3      = Clean::post('f_ligne3'     , 'adresse');
$ligne4      = Clean::post('f_ligne4'     , 'adresse');
$code_postal = Clean::post('f_code_postal', 'codepostal');
$commune     = Clean::post('f_commune'    , 'commune');
$pays        = Clean::post('f_pays'       , 'pays');

$verif_post = $user_id && !is_null($ligne1) && !is_null($ligne2) && !is_null($ligne3) && !is_null($ligne4) && !is_null($code_postal) && !is_null($commune) && !is_null($pays);

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter une nouvelle adresse
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='ajouter') && $verif_post )
{
  // Insérer l’enregistrement
  DB_STRUCTURE_ADMINISTRATEUR::DB_ajouter_adresse_parent( $user_id , array($ligne1,$ligne2,$ligne3,$ligne4,$code_postal,$commune,$pays) );
  // Afficher le retour
  Json::add_str('<td><span>'.html($ligne1).'</span> ; <span>'.html($ligne2).'</span> ; <span>'.html($ligne3).'</span> ; <span>'.html($ligne4).'</span></td>');
  Json::add_str('<td>'.html($code_postal).'</td>');
  Json::add_str('<td>'.html($commune).'</td>');
  Json::add_str('<td>'.html($pays).'</td>');
  Json::add_str('<td class="nu">');
  Json::add_str(  '<q class="modifier"'.infobulle('Modifier ce parent.').'></q>');
  Json::add_str('</td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier une adresse existante
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier') && $verif_post )
{
  // Insérer l’enregistrement
  $user_id = DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_adresse_parent( $user_id , array($ligne1,$ligne2,$ligne3,$ligne4,$code_postal,$commune,$pays) );
  // Afficher le retour
  Json::add_str('<td><span>'.html($ligne1).'</span> ; <span>'.html($ligne2).'</span> ; <span>'.html($ligne3).'</span> ; <span>'.html($ligne4).'</span></td>');
  Json::add_str('<td>'.html($code_postal).'</td>');
  Json::add_str('<td>'.html($commune).'</td>');
  Json::add_str('<td>'.html($pays).'</td>');
  Json::add_str('<td class="nu">');
  Json::add_str(  '<q class="modifier"'.infobulle('Modifier ce parent.').'></q>');
  Json::add_str('</td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
