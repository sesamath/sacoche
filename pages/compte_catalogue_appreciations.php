<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Catalogue d’appréciations'));

define('CATEGORIE_LENGTH',50);
define('APPRECIATION_LENGTH',500);

// Javascript
Layout::add( 'js_inline_before' , 'window.CATEGORIE_LENGTH    = '.CATEGORIE_LENGTH.';' );
Layout::add( 'js_inline_before' , 'window.APPRECIATION_LENGTH = '.APPRECIATION_LENGTH.';' );
?>

<ul class="puce">
  <li><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_professeur__catalogue_appreciations">DOC : Catalogue d’appréciations.</a></span></li>
</ul>

<hr>

<form action="#" method="post" id="form_appreciations">
  <ul id="sortable_v">
    <?php
    // Lister les données de l’utilisateur
    $DB_TAB = DB_STRUCTURE_CATALOGUE::DB_lister_appreciations_avec_categories($_SESSION['USER_ID']);
    if(!empty($DB_TAB))
    {
      $categorie_id = 0;
      foreach($DB_TAB as $DB_ROW)
      {
        if($DB_ROW['categorie_id']!=$categorie_id)
        {
          echo'<li id="cat_'.$DB_ROW['categorie_id'].'"><span class="b u">'.html($DB_ROW['categorie_titre']).'</span><q class="modifier"'.infobulle('Modifier cette catégorie').'></q><q class="dupliquer"'.infobulle('Dupliquer cette catégorie').'></q><q class="supprimer"'.infobulle('Supprimer cette catégorie').'></q></li>';
          $categorie_id = $DB_ROW['categorie_id'];
        }
        if(!empty($DB_ROW['appreciation_id']))
        {
          echo'<li id="app_'.$DB_ROW['appreciation_id'].'"><span>'.html($DB_ROW['appreciation_contenu']).'</span><q class="modifier"'.infobulle('Modifier cette appréciation').'></q><q class="dupliquer"'.infobulle('Dupliquer cette appréciation').'></q><q class="supprimer"'.infobulle('Supprimer cette appréciation').'></q></li>';
        }
      }
    }
    else
    {
      echo'<li class="i">Encore aucun élément actuellement ! Utilisez les outils ci-dessous pour en ajouter&hellip;</li>'.NL;
    }
    ?>
  </ul>
  <div><span class="tab"></span><button class="valider" type="button" id="bouton_valider">Enregistrer.</button> <label id="ajax_msg">&nbsp;</label></div>
  <hr>
  <h2>Ajouter une catégorie</h2>
  <div class="sortable"><input id="categorie_val" value="" type="text" size="40" maxlength="<?php echo CATEGORIE_LENGTH ?>"><q id="categorie_ajouter" class="ajouter"<?php echo infobulle('Ajouter cette categorie') ?>></q><label for="categorie_val"></label></div>
  <h2>Ajouter une appréciation</h2>
  <div class="sortable"><input id="appreciation_val" value="" type="text" size="125" maxlength="<?php echo APPRECIATION_LENGTH ?>"><q id="appreciation_ajouter" class="ajouter"<?php echo infobulle('Ajouter cette appreciation') ?>></q><label for="appreciation_val"></label></div>
</form>

<hr>

<form action="#" method="post">
  <h2>Import / Export de son catalogue</h2>
  <p>
    <label class="tab">Procédure :</label>&nbsp;&nbsp;&nbsp;<label for="f_mode_export"><input type="radio" id="f_mode_export" name="f_mode" value="export"> Export</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label for="f_mode_import"><input type="radio" id="f_mode_import" name="f_mode" value="import"> Import</label>
  </p>
</form>

<form action="#" method="post" id="form_export" class="hide">
  <p class="astuce">Utile pour un professeur qui travaillerait sur plusieurs établissements ou serait amené à changer d’établissement.</p>
  <p>
    <span class="tab"></span><button type="button" id="bouton_export" class="fichier_export">Générer le fichier.</button>
    <label id="ajax_msg_export"></label>
  </p>
</form>

<form action="#" method="post" id="form_import" class="hide">
  <p class="danger">Utiliser uniquement un fichier généré par <em>SACoche</em> ; l’import ajoutera son contenu à ce qui existe déjà éventuellement, puis rechargera cette page.</p>
  <p>
    <label class="tab" for="bouton_import">Fichier à importer :</label>
    <input type="hidden" id="f_action" name="f_action" value="import">
    <input id="f_import" type="file" name="userfile">
    <button type="button" id="bouton_import" class="fichier_import">Parcourir...</button>
    <label id="ajax_msg_import"></label>
  </p>
</form>

<hr>

