<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action       = Clean::post('f_action'    , 'texte');
$id           = Clean::post('f_id'        , 'entier');
$id_ent       = Clean::post('f_id_ent'    , 'id_ent');
$id_gepi      = Clean::post('f_id_gepi'   , 'id_ent');
$profil       = 'ADM';
$genre        = Clean::post('f_genre'     , 'lettres');
$nom          = Clean::post('f_nom'       , 'nom');
$prenom       = Clean::post('f_prenom'    , 'prenom');
$login        = Clean::post('f_login'     , 'login');
$password     = Clean::post('f_password'  , 'password');
$box_login    = Clean::post('box_login'   , 'bool');
$box_password = Clean::post('box_password', 'bool');
$courriel     = Clean::post('f_courriel'  , 'courriel');

$verif_post = isset(Html::$tab_genre['adulte'][$genre]) && $nom && $prenom && ($box_login || $login) && ($box_password || strlen($password)) && !is_null($id_ent) && !is_null($id_gepi) && !is_null($courriel);

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter un nouvel administrateur
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='ajouter') && $verif_post )
{
  // Vérifier que l’identifiant ENT est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_ent)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_ent',$id_ent) )
    {
      Json::end( FALSE , 'Identifiant ENT déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant GEPI est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_gepi)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_gepi',$id_gepi) )
    {
      Json::end( FALSE , 'Identifiant Gepi déjà utilisé !' );
    }
  }
  if($box_login)
  {
    // Construire puis tester le login (parmi tous les utilisateurs de l’établissement)
    $login = Outil::fabriquer_login($prenom,$nom,$profil);
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login) )
    {
      // Login pris : en chercher un autre en remplaçant la fin par des chiffres si besoin
      $login = DB_STRUCTURE_ADMINISTRATEUR::DB_rechercher_login_disponible($login);
    }
  }
  else
  {
    // Vérifier que le login transmis est disponible (parmi tous les utilisateurs de l’établissement)
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login) )
    {
      Json::end( FALSE , 'Login déjà utilisé !' );
    }
  }
  if($box_password)
  {
    // Générer un mdp aléatoire
    $password = Outil::fabriquer_mdp($profil);
  }
  else
  {
    // Vérifier que le mdp transmis est d’une longueur compatible
    if(mb_strlen($password)<$_SESSION['TAB_PROFILS_ADMIN']['MDP_LONGUEUR_MINI'][$profil])
    {
      Json::end( FALSE , 'Mot de passe trop court pour ce profil !' );
    }
  }
  // Vérifier le domaine du serveur mail seulement en mode multi-structures car ce peut être sinon une installation sur un serveur local non ouvert sur l’extérieur.
  if($courriel)
  {
    if(HEBERGEUR_INSTALLATION=='multi-structures')
    {
      list($mail_domaine,$is_domaine_valide) = Outil::tester_domaine_courriel_valide($courriel);
      if(!$is_domaine_valide)
      {
        Json::end( FALSE , 'Erreur avec le domaine "'.$mail_domaine.'" !' );
      }
    }
  }
  $user_email_origine = ($courriel) ? 'admin' : '' ;
  // Insérer l’enregistrement
  $user_id = DB_STRUCTURE_COMMUN::DB_ajouter_utilisateur( 0 /*user_sconet_id*/ , 0 /*sconet_num*/ , '' /*reference*/ , $profil , $genre , $nom , $prenom , NULL /*user_naissance_date*/ , $courriel , $user_email_origine , $login , Outil::crypter_mdp($password) , $id_ent , $id_gepi );
  // Pour les admins, abonnement obligatoire aux contacts effectués depuis la page d’authentification
  DB_STRUCTURE_NOTIFICATION::DB_ajouter_abonnement( $user_id , 'contact_externe' , 'accueil' );
  // Afficher le retour
  Json::add_str('<tr id="id_'.$user_id.'" class="new">');
  Json::add_str(  '<td>'.html($id_ent).'</td>');
  Json::add_str(  '<td>'.html($id_gepi).'</td>');
  Json::add_str(  '<td>'.Html::$tab_genre['adulte'][$genre].'</td>');
  Json::add_str(  '<td>'.html($nom).'</td>');
  Json::add_str(  '<td>'.html($prenom).'</td>');
  Json::add_str(  '<td class="new">'.html($login).' '.infobulle('Pensez à relever le login généré !',TRUE).'</td>');
  Json::add_str(  '<td class="new">'.html($password).' '.infobulle('Pensez à noter le mot de passe !',TRUE).'</td>');
  Json::add_str(  '<td>'.html($courriel).'</td>');
  Json::add_str(  '<td class="nu">');
  Json::add_str(    '<q class="modifier"'.infobulle('Modifier cet administrateur.').'></q>');
  Json::add_str(    '<q class="supprimer"'.infobulle('Retirer cet administrateur.').'></q>');
  Json::add_str(  '</td>');
  Json::add_str('</tr>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier un administrateur existant
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier') && $id && $verif_post )
{
  $tab_donnees = array();
  // Vérifier que l’identifiant ENT est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_ent)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_ent',$id_ent,$id) )
    {
      Json::end( FALSE , 'Identifiant ENT déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant GEPI est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_gepi)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_gepi',$id_gepi,$id) )
    {
      Json::end( FALSE , 'Identifiant Gepi déjà utilisé !' );
    }
  }
  // Vérifier que le login transmis est disponible (parmi tous les utilisateurs de l’établissement)
  if(!$box_login)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login,$id) )
    {
      Json::end( FALSE , 'Login déjà utilisé !' );
    }
    $tab_donnees[':login'] = $login;
  }
  // Vérifier le domaine du serveur mail seulement en mode multi-structures car ce peut être sinon une installation sur un serveur local non ouvert sur l’extérieur.
  if($courriel)
  {
    if(HEBERGEUR_INSTALLATION=='multi-structures')
    {
      list($mail_domaine,$is_domaine_valide) = Outil::tester_domaine_courriel_valide($courriel);
      if(!$is_domaine_valide)
      {
        Json::end( FALSE , 'Erreur avec le domaine "'.$mail_domaine.'" !' );
      }
    }
    // Vérifier aussi que l’adresse n’est pas répertoriée comme étant en erreur.
    if(DB_STRUCTURE_COURRIEL_ERREUR::DB_tester_courriel($courriel))
    {
      Json::end( FALSE , 'Courriel répertorié comme étant en erreur !' );
    }
    $tab_donnees[':email_origine'] = 'admin';
  }
  else
  {
    $tab_donnees[':email_origine'] = '';
  }
  // Cas du mot de passe
  if(!$box_password)
  {
    $tab_donnees[':password'] = Outil::crypter_mdp($password);
  }
  // Mettre à jour l’enregistrement
  $tab_donnees += array(
    ':genre'    => $genre,
    ':nom'      => $nom,
    ':prenom'   => $prenom,
    ':courriel' => $courriel,
    ':id_ent'   => $id_ent,
    ':id_gepi'  => $id_gepi,
  );
  DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $id , $tab_donnees );
  // Mettre à jour aussi éventuellement la session
  if($id==$_SESSION['USER_ID'])
  {
    $mail_origine = isset($tab_donnees[':email_origine']) ? $tab_donnees[':email_origine'] : $_SESSION['USER_EMAIL_ORIGINE'] ; // si le mail n’a pas été changé alors il ne faut pas non plus modifier cette valeur
    Session::_set('USER_GENRE'         , $genre);
    Session::_set('USER_NOM'           , $nom);
    Session::_set('USER_PRENOM'        , $prenom);
    Session::_set('USER_EMAIL'         , $courriel);
    Session::_set('USER_EMAIL_ORIGINE' , $mail_origine);
    Session::_set('USER_LOGIN'         , $login);
    Session::_set('USER_ID_ENT'        , $id_ent);
    Session::_set('USER_ID_GEPI'       , $id_gepi);
  }
  // Afficher le retour
  $q_supprimer = ($id!=$_SESSION['USER_ID'])
               ? '<q class="supprimer"'.infobulle('Retirer cet administrateur.').'></q>'
               : '<q class="supprimer_non"'.infobulle('Un administrateur ne peut pas supprimer son propre compte.').'></q>' ;
  Json::add_str('<td>'.html($id_ent).'</td>');
  Json::add_str('<td>'.html($id_gepi).'</td>');
  Json::add_str('<td>'.Html::$tab_genre['adulte'][$genre].'</td>');
  Json::add_str('<td>'.html($nom).'</td>');
  Json::add_str('<td>'.html($prenom).'</td>');
  Json::add_str('<td>'.html($login).'</td>');
  Json::add_str( ($box_password) ? '<td class="i">champ crypté</td>' : '<td class="new">'.$password.' '.infobulle('Pensez à noter le mot de passe !',TRUE).'</td>');
  Json::add_str('<td>'.html($courriel).'</td>');
  Json::add_str('<td class="nu">');
  Json::add_str(  '<q class="modifier"'.infobulle('Modifier cet administrateur.').'></q>');
  Json::add_str(  $q_supprimer);
  Json::add_str('</td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Retirer un administrateur existant
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='supprimer') && $id && $nom && $prenom )
{
  if($id==$_SESSION['USER_ID'])
  {
    Json::end( FALSE , 'Un administrateur ne peut pas supprimer son propre compte !' );
  }
  // Supprimer l’enregistrement
  DB_STRUCTURE_ADMINISTRATEUR::DB_supprimer_utilisateur( $id , $profil );
  // Log de l’action
  SACocheLog::ajouter('Suppression de l’utilisateur '.$nom.' '.$prenom.' ('.$profil.' '.$id.').');
  // Notifications (rendues visibles ultérieurement)
  $notification_contenu = date('d-m-Y H:i:s').' '.$_SESSION['USER_PRENOM'].' '.$_SESSION['USER_NOM'].' a supprimé l’utilisateur '.$nom.' '.$prenom.' ('.$profil.' '.$id.').'."\r\n";
  DB_STRUCTURE_NOTIFICATION::enregistrer_action_admin( $notification_contenu , $_SESSION['USER_ID'] );
  // Afficher le retour
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
