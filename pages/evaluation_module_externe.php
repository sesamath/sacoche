<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Interfaçage avec un module externe'));

$url_generer_enonce      = !empty($_SESSION['MODULE']['GENERER_ENONCE']     ) ? $_SESSION['MODULE']['GENERER_ENONCE']      : '' ;
$url_importer_evaluation = !empty($_SESSION['MODULE']['IMPORTER_EVALUATION']) ? $_SESSION['MODULE']['IMPORTER_EVALUATION'] : '' ;
?>

<div><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_professeur__evaluations_module_externe">DOC : Interfaçage des évaluations avec un module externe.</a></span></div>

<p class="travaux">Fonctionnalité nécessitant l’usage d’un module externe (existant ou mis en place et codé par vos soins).</p>

<form action="#" method="post" id="form_module">

<hr>

<h2>Générer des énoncés d’évaluation (personnalisés)</h2>

<p>
  <label class="tab" for="f_generer_enonce">Adresse de la page :</label><input id="f_generer_enonce" name="f_generer_enonce" size="70" maxlength="255" type="text" value="<?php echo html($url_generer_enonce) ?>"><br>
  <span class="tab"></span><button data-objet="generer_enonce" type="button" class="parametre">Valider.</button><label id="ajax_msg_generer_enonce">&nbsp;</label>
</p>

<hr>

<h2>Importer une évaluation (avec des notes)</h2>

<p>
  <label class="tab" for="f_importer_evaluation">Adresse du dossier :</label><input id="f_importer_evaluation" name="f_importer_evaluation" size="70" maxlength="255" type="text" value="<?php echo html($url_importer_evaluation) ?>"><br>
  <span class="tab"></span><button data-objet="importer_evaluation" type="button" class="parametre">Valider.</button><label id="ajax_msg_importer_evaluation">&nbsp;</label>
</p>

<hr>

</form>




