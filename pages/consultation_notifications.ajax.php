<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action          = Clean::post('f_action', 'texte');
$notification_id = Clean::post('f_id'    , 'entier');
$new_etat        = Clean::post('f_etat'  , 'texte');

$tab_statut = array(
  'vue' => 'consultée',
  'new' => 'consultable',
);

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Changer le statut d’une notification
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='memoriser_consultation') && $notification_id && isset($tab_statut[$new_etat]) )
{
  $is_modif = DB_STRUCTURE_NOTIFICATION::DB_modifier_statut( $notification_id , $_SESSION['USER_ID'] , $tab_statut[$new_etat] );
  // Afficher le retour
  if($is_modif)
  {
    Json::end( TRUE );
  }
  else
  {
    Json::end( FALSE , 'Notification non trouvée ou pas associée à ce compte !' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
