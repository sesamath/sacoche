<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Livret Scolaire')).' &rarr; '.html(Lang::_('Langues vivantes'));

if( ($_SESSION['USER_PROFIL_TYPE']!='directeur') && ($_SESSION['USER_PROFIL_TYPE']!='administrateur') )
{
  echo'<p class="danger">'.html(Lang::_('Vous n’êtes pas habilité à accéder à cette fonctionnalité !')).'</p>'.NL;
  echo'<div class="astuce">Seuils les administrateurs et les personnels de direction peuvent consulter cette page.</div>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
?>

<ul class="puce">
  <li><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=officiel__livret_scolaire_administration#toggle_langues">DOC : Administration du Livret Scolaire &rarr; Langues vivantes</a></span></li>
  <li><span class="astuce">Ce menu ne sert que pour les <b>bilans périodiques</b> des <b>élèves du Collège</b> (sans objet pour les <b>bilans de fin de cycle</b>).</span></li>
</ul>

<hr>

<?php
require(CHEMIN_DOSSIER_INCLUDE.'tableau_langues_vivantes.php');
// Fonction adaptant le tableau pour un affichage dans un formulaire de type select avec des groupes d’options suivant que ces langues soient ou non enseignées dans l’établissement
function OPT_langues($tab_langues)
{
  $tab_optgroup = $tab_matiere_nom = array(); // pour array_multisort()
  $tab_matieres_enseignées = explode(',',DB_STRUCTURE_COMMUN::DB_recuperer_matieres_etabl());
  foreach($tab_langues as $id =>$tab)
  {
    $tab_langues[$id]['optgroup'] = ($id==100) ? 0 : ( count(array_intersect($tab_langues[$id]['tab_matiere_id'],$tab_matieres_enseignées)) ? 1 : 2 ) ;
    $tab_optgroup[$id]    = $tab_langues[$id]['optgroup'];
    $tab_matiere_nom[$id] = $tab_langues[$id]['texte'];
  }
  array_multisort(
    $tab_optgroup   , SORT_ASC,
    $tab_matiere_nom, SORT_ASC,
    $tab_langues
  );
  return $tab_langues;
}

// Fabrication des éléments select du formulaire
$tab_groupes = DB_STRUCTURE_COMMUN::DB_OPT_regroupements_etabl();
$select_eleve  = HtmlForm::afficher_select($tab_groupes              , 'select_groupe' /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ , 'regroupements' /*optgroup*/ );
$select_langue = HtmlForm::afficher_select(OPT_langues($tab_langues) , 'f_langue'      /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ ,       'langues' /*optgroup*/ );
?>

<form action="#" method="post" id="form_select">
  <table><tr>
    <td class="nu" style="width:25em;vertical-align:top">
      <b>Élèves :</b><br>
      <?php echo $select_eleve ?><br>
      <span id="ajax_eleves"></span>
    </td>
    <td class="nu" style="width:25em;vertical-align:top">
      <p><b>Objet :</b> <select id="f_objet" name="f_objet">
        <option value="">&nbsp;</option>
        <option value="lv1">LV1</option>
        <option value="lv2">LV2</option>
      </select><p>
      <p><b>Langue :</b> <?php echo $select_langue; ?></p>
      <p><button id="bouton_associer" type="button" class="parametre">Effectuer ces associations.</button></p>
      <p><label id="ajax_msg">&nbsp;</label></p>
    </td>
  </tr></table>
</form>

<hr>

<div id="bilan">
</div>
