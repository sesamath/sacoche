<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {}

$action         = Clean::post('f_action'        , 'texte');
$matiere_id     = Clean::post('f_matiere_id'    , 'entier');
$niveau_id      = Clean::post('f_niveau_id'     , 'entier');
$structure_id   = Clean::post('f_structure_id'  , 'entier');
$referentiel_id = Clean::post('f_referentiel_id', 'entier');
$maj_date_fr    = Clean::post('f_maj_date'      , 'date_fr');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Afficher le formulaire des structures ayant partagées au moins un référentiel
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($action=='Afficher_structures') // La vérification concernant le nombre de contraintes s’effectue après
{
  Json::end( TRUE , ServeurCommunautaire::afficher_formulaire_structures_communautaires( $_SESSION['SESAMATH_ID'] , $_SESSION['SESAMATH_KEY'] ) );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Lister les référentiels partagés trouvés selon les critères retenus (matière / niveau / structure)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='Lister_referentiels') && !is_null($matiere_id) && !is_null($niveau_id) && !is_null($structure_id) && !is_null($maj_date_fr) )
{
  $maj_date_sql = To::date_french_to_sql($maj_date_fr);
  Json::end( TRUE , ServeurCommunautaire::afficher_liste_referentiels( $_SESSION['SESAMATH_ID'] , $_SESSION['SESAMATH_KEY'] , $matiere_id , $niveau_id , $structure_id , $maj_date_sql ) );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Voir le contenu d’un référentiel partagé
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='Voir_referentiel') && $referentiel_id )
{
 Json::end( TRUE , ServeurCommunautaire::afficher_contenu_referentiel( $_SESSION['SESAMATH_ID'] , $_SESSION['SESAMATH_KEY'] , $referentiel_id ) );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On en devrait pas en arriver là
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
