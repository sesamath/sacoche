<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action = Clean::post('f_action', 'texte');

if( !$action || !$_SESSION['USER_EMAIL'] )
{
  Json::end( FALSE , 'Erreur avec les données transmises !' );
}

// Vérifier le domaine du serveur mail même en mode mono-structure parce que de toutes façons il faudra ici envoyer un mail, donc l’installation doit être ouverte sur l’extérieur.
list($mail_domaine,$is_domaine_valide) = Outil::tester_domaine_courriel_valide(WEBMESTRE_COURRIEL);
if(!$is_domaine_valide)
{
  Json::end( FALSE , 'Erreur avec le domaine "'.$mail_domaine.'" (courriel du webmestre) !' );
}

// Les destinataires : le webmestre et les autres admins en copie
$tab_destinataires = array( WEBMESTRE_COURRIEL );
$abonnement_ref = 'contact_externe';
$DB_TAB = DB_STRUCTURE_NOTIFICATION::DB_lister_destinataires_avec_informations( $abonnement_ref );
$destinataires_nb = count($DB_TAB);
if(!$destinataires_nb)
{
  // Normalement impossible, l’abonnement des admins à ce type de de notification étant obligatoire
  Json::end( FALSE , 'Aucun administrateur avec une adresse de courriel trouvé.' );
}
foreach($DB_TAB as $DB_ROW)
{
  $tab_destinataires[] = $DB_ROW['user_prenom'].' '.$DB_ROW['user_nom'].' <'.$DB_ROW['user_email'].'>';
}

// Le courriel
$mail_objet = 'Résilier inscription SACoche '.$_SESSION['BASE'].' ['.$_SESSION['WEBMESTRE_UAI'].']';
$mail_contenu = 'Bonjour,'.EOML;
$mail_contenu.= EOML;
$mail_contenu.= 'Merci de supprimer l’inscription à SACoche de mon établissement :'.EOML;
$mail_contenu.= $_SESSION['WEBMESTRE_UAI'].' - '.$_SESSION['WEBMESTRE_DENOMINATION'].EOML;
$mail_contenu.= $_SESSION['ETABLISSEMENT']['DENOMINATION'].' '.$_SESSION['ETABLISSEMENT']['ADRESSE1'].' '.$_SESSION['ETABLISSEMENT']['ADRESSE2'].' '.$_SESSION['ETABLISSEMENT']['ADRESSE3'].' '.EOML;
$mail_contenu.= 'Serveur '.URL_INSTALL_SACOCHE.EOML;
$mail_contenu.= EOML;
$mail_contenu.= 'J’ai bien compris et validé que toutes les données seront définitivement effacées.'.EOML;
$mail_contenu.= $_SESSION['USER_PRENOM'].' '.$_SESSION['USER_NOM'].' [id '.$_SESSION['USER_ID'].']'.EOML;
$mail_contenu.= $_SESSION['USER_EMAIL'].EOML;

$courriel_bilan = Sesamail::mail( $tab_destinataires , $mail_objet , $mail_contenu );

if(!$courriel_bilan)
{
  Json::end( FALSE , 'Erreur lors de l’envoi de la demande !<br>Contactez '.WEBMESTRE_COURRIEL.' par courriel.' );
}

// Retour ok
Json::end( TRUE );

?>