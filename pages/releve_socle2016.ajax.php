<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {}

$cycle_id                  = Clean::post('f_cycle'                   , 'entier');
$cycle_nom                 = Clean::post('f_cycle_nom'               , 'texte');
$socle_detail              = Clean::post('f_socle_detail'            , 'texte');
$socle_individuel_format   = Clean::post('f_socle_individuel_format' , 'texte');
$socle_synthese_format     = Clean::post('f_socle_synthese_format'   , 'texte');
$socle_synthese_affichage  = Clean::post('f_socle_synthese_affichage', 'texte');
$tableau_tri_maitrise_mode = Clean::post('f_tri_maitrise_mode'       , 'texte');
$groupe_id                 = Clean::post('f_groupe'                  , 'entier');
$groupe_nom                = Clean::post('f_groupe_nom'              , 'texte');
$groupe_type               = Clean::post('f_groupe_type'             , 'lettres');
$eleves_ordre              = Clean::post('f_eleves_ordre'            , 'eleves_ordre', 'nom'); // Non transmis par Safari si dans le <span> avec la classe "hide".
$mode                      = Clean::post('f_mode'                    , 'texte');
$matiere_nom               = Clean::post('f_matiere_nom'             , 'texte');
$aff_socle_items_acquis    = Clean::post('f_socle_items_acquis'      , 'bool');
$aff_socle_position        = Clean::post('f_socle_position'          , 'bool');
$aff_socle_points_DNB      = Clean::post('f_socle_points_dnb'        , 'bool');
$only_presence             = Clean::post('f_only_presence'           , 'bool');
$only_annee                = Clean::post('f_only_annee'              , 'bool');
$aff_lien                  = Clean::post('f_lien'                    , 'bool');
$aff_panier                = Clean::post('f_panier'                  , 'bool');
$aff_start                 = Clean::post('f_start'                   , 'bool');
$couleur                   = Clean::post('f_couleur'                 , 'texte');
$fond                      = Clean::post('f_fond'                    , 'texte');
$legende                   = Clean::post('f_legende'                 , 'texte');
$marge_min                 = Clean::post('f_marge_min'               , 'entier');
$pages_nb                  = Clean::post('f_pages_nb'                , 'texte');

// tableaux
$tab_eleve   = Clean::post('f_eleve'  , array('array',','));
$tab_matiere = Clean::post('f_matiere', array('array',','));
$tab_type    = Clean::post('f_type'   , array('array',','));
$tab_eleve   = array_filter( Clean::map('entier',$tab_eleve)   , 'positif' );
$tab_matiere = array_filter( Clean::map('entier',$tab_matiere) , 'positif' );
$tab_type    = Clean::map('texte',$tab_type);

// En cas de manipulation du formulaire (avec les outils de développements intégrés au navigateur ou un module complémentaire)...

if(in_array($_SESSION['USER_PROFIL_TYPE'],array('parent','eleve')))
{
  $aff_socle_position   = Outil::test_user_droit_specifique($_SESSION['DROIT_SOCLE_PROPOSITION_POSITIONNEMENT']) ? $aff_socle_position   : 0 ;
  $aff_socle_points_DNB = Outil::test_user_droit_specifique($_SESSION['DROIT_SOCLE_PREVISION_POINTS_BREVET'])    ? $aff_socle_points_DNB : 0 ;
  $tab_type   = array('individuel');
  $aff_panier = 1;
}

// Pour un élève on surcharge avec les données de session
if($_SESSION['USER_PROFIL_TYPE']=='eleve')
{
  $tab_eleve = array($_SESSION['USER_ID']);
  $groupe_id = $_SESSION['ELEVE_CLASSE_ID'];
}

// Pour un parent on vérifie que c’est bien un de ses enfants
if( isset($tab_eleve[0]) && ($_SESSION['USER_PROFIL_TYPE']=='parent') )
{
  Outil::verif_enfant_parent( $tab_eleve[0] );
}

// Pour un professeur on vérifie que ce sont bien ses élèves
if( ($_SESSION['USER_PROFIL_TYPE']=='professeur') && ($_SESSION['USER_JOIN_GROUPES']=='config') )
{
  Outil::verif_eleves_prof( $tab_eleve );
}

$type_individuel  = (in_array('individuel',$tab_type))  ? 1 : 0 ;
$type_synthese    = (in_array('synthese',$tab_type))    ? 1 : 0 ;
$type_repartition = (in_array('repartition',$tab_type)) ? 1 : 0 ;

if(
    !$cycle_id || !$cycle_nom
    || !$groupe_id || !$groupe_nom || !$groupe_type || !count($tab_eleve)
    || !count($tab_type) || !$tableau_tri_maitrise_mode || !in_array($mode,array('auto','manuel'))
    || !$couleur || !$fond || !$legende || !$marge_min || !$pages_nb || !$eleves_ordre
    || ( $type_synthese && !in_array($socle_synthese_affichage,array('pourcentage','position','points')) )
  )
{
  Json::end( FALSE , 'Erreur avec les données transmises !' );
}

Form::save_choix('releve_socle2016');

// Pour les évaluations à la volée.
if($_SESSION['USER_PROFIL_TYPE']=='professeur')
{
  Session::generer_jeton_anti_CSRF('evaluation_ponctuelle');
  $CSRF_eval_eclair = Session::$_CSRF_value;
}

// On ne ferme pas la session tout de suite car il y a un appel ultérieur à Outil::recuperer_seuils_livret()

// Bricoles restantes

$marge_gauche = $marge_droite = $marge_haut = $marge_bas = $marge_min ;

$aff_socle_points_DNB = ( ($cycle_id==4) && ($socle_detail=='livret') ) ? $aff_socle_points_DNB : 0 ;

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// INCLUSION DU CODE COMMUN À PLUSIEURS PAGES
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$make_officiel = FALSE;
$make_livret   = FALSE;
$make_action   = '';
$make_html     = TRUE;
$make_pdf      = TRUE;
$make_csv      = ( $aff_socle_points_DNB && ($socle_synthese_format=='eleve') ) ? TRUE : FALSE ;

require(CHEMIN_DOSSIER_INCLUDE.'noyau_socle2016.php');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Affichage du résultat
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$retour = '';

if($affichage_direct)
{
  $retour .=
    '<hr>'.NL
  . '<ul class="puce">'.NL
  .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
  . '</ul>'.NL
  . $html;
}
else
{
  if($type_individuel)
  {
    $retour .=
      '<h2>Relevé individuel</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','individuel',$fichier_nom).'"><span class="file file_htm">Explorer / Manipuler (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    . '</ul>'.NL;
  }
  if($type_synthese)
  {
    $li_synthese_csv = ($make_csv) ? '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','synthese',$fichier_nom).'.csv"><span class="file file_txt">Exploiter avec un tableur (format <em>csv</em>).</span></a></li>'.NL : '' ;
    $retour .=
      '<h2>Synthèse collective</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','synthese',$fichier_nom).'"><span class="file file_htm">Explorer / Manipuler (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','synthese',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    .   $li_synthese_csv
    . '</ul>'.NL;
  }
  if($type_repartition)
  {
    $retour .=
      '<h2>Répartition statistique</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','repartition',$fichier_nom).'"><span class="file file_htm">Consulter (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','repartition',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    . '</ul>'.NL;
  }
}

Json::add_tab( array(
  'direct' => $affichage_direct ,
  'bilan'  => $retour ,
) );
Json::end( TRUE );

?>
