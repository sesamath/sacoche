<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if(!isset($STEP))       {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Étape 64 - Traitement des modifications d’affectations élèves/groupes (edt_eleves_groupes)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

// Récupérer les éléments postés
$tab_check = Clean::post('f_check' , array('array',','));
$tab_post = array( 'groupe'=>array() );
foreach($tab_check as $check_infos)
{
  if(substr($check_infos,0,7)=='groupe_')
  {
    list($obj,$id1,$id2,$etat) = explode('_',$check_infos);
    $tab_post[$obj][$id1][$id2] = (bool)$etat;
  }
}
// Ajouter / retirer des associations users/groupes
$nb_asso_groupes = count($tab_post['groupe']);
if($nb_asso_groupes)
{
  foreach($tab_post['groupe'] as $user_id => $tab_id2)
  {
    foreach($tab_id2 as $groupe_id => $etat)
    {
      DB_STRUCTURE_REGROUPEMENT::DB_modifier_liaison_user_groupe_par_admin($user_id,$import_profil,$groupe_id,'groupe',$etat);
    }
  }
}
// Afficher le résultat
Json::add_str('<p><label class="valide">Modifications associations utilisateurs / groupes effectuées : '.$nb_asso_groupes.'</label></p>'.NL);
Json::add_str('<ul class="puce p"><li><a href="#step90" id="passer_etape_suivante">Passer à l’étape 4.</a><label id="ajax_msg">&nbsp;</label></li></ul>'.NL);

?>
