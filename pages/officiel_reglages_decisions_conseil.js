/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    var mode = false;
    var categorie = false;

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_mention , #table_engagement , #table_orientation').tablesorter({ sortLocaleCompare : true, headers:{3:{sorter:false}} });
    function tableau_tri(objet){ $('#table_'+objet).trigger( 'sorton' , [ [[0,0],[1,0]] ] ); };
    function tableau_maj(objet){ $('#table_'+objet).trigger( 'update' , [ true ] ); };
    tableau_tri('mention');
    tableau_tri('engagement');
    tableau_tri('orientation');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonctions utilisées
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_form_gestion( mode , categorie , id , usage , ordre , synthese , contenu )
    {
      $('#f_action').val(mode);
      $('#f_categorie_view').val( categorie[0].toUpperCase() + categorie.substring(1) );
      $('#f_categorie').val(categorie);
      $('#f_id').val(id);
      $('#f_usage').val(usage);
      $('#f_ordre').val(ordre);
      $('#f_synthese').val(synthese);
      $('#f_contenu').val(contenu);
      $('#alerte_used').hideshow( (mode!='ajouter') && usage );
      // pour finir
      $('#gestion_titre_action').html( mode[0].toUpperCase() + mode.substring(1) );
      $('#gestion_edit').hideshow( mode != 'supprimer' );
      $('#gestion_delete').hideshow( mode == 'supprimer' );
      $('#ajax_msg_gestion').removeAttr('class').html('');
      $('#form_gestion label[generated=true]').removeAttr('class').html('');
      $.fancybox( { href:'#form_gestion' , modal:true , minWidth:700 } );
    }

    /**
     * Ajouter un contenu : mise en place du formulaire
     * @return void
     */
    var ajouter = function()
    {
      mode = $(this).attr('class');
      categorie = $(this).parent().parent().parent().parent().parent().attr('id').substring(6); // table_*
      // Afficher le formulaire
      afficher_form_gestion( mode , categorie , 0 /*id*/ , 0 /*usage*/ , '' /*ordre*/ , '' /*synthese*/ , '' /*contenu*/ );
    };

    /**
     * Modifier / Dupliquer / Supprimer un contenu : mise en place du formulaire
     * @return void
     */
    var modifier_dupliquer_supprimer = function()
    {
      mode      = $(this).attr('class');
      categorie = $(this).parent().parent().parent().parent().attr('id').substring(6); // table_*
      var objet_tr  = $(this).parent().parent();
      var objet_tds = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var id       = (mode!='dupliquer') ? objet_tr.attr('id').substring(3) : '' ;
      var usage    = objet_tr.data('used');
      var ordre    = objet_tds.eq(0).html();
      var synthese = objet_tds.eq(1).html();
      var contenu  = objet_tds.eq(2).html();
      // Afficher le formulaire
      afficher_form_gestion( mode , categorie , id , usage , ordre , unescapeHtml(synthese) , unescapeHtml(contenu) );
    };

    /**
     * Annuler une action
     * @return void
     */
    var annuler = function()
    {
      $.fancybox.close();
      mode = false;
      categorie = false;
    };

    /**
     * Intercepter la touche entrée ou escape pour valider ou annuler les modifications
     * @return void
     */
    function intercepter(e)
    {
      if(mode)
      {
        if(e.which==13)  // touche entrée
        {
          $('#bouton_valider').click();
        }
        else if(e.which==27)  // touche escape
        {
          $('#bouton_annuler').click();
        }
      }
    }

    var prompt_etapes = {
      etape_1: {
        title   : 'Demande de confirmation',
        html    : 'Attention : les saisies associées à cette décision seront impactées !<br>Souhaitez-vous vraiment modifier ou supprimer ce contenu ?',
        buttons : {
          'Non, c’est une erreur !' : false ,
          'Oui, je confirme !' : true
        },
        submit  : function(event, value, message, formVals) {
          if(value) {
            formulaire.submit();
          }
          else {
            $('#bouton_annuler').click();
          }
        }
      }
    };

    var soumettre_formulaire = function()
    {
      // On demande confirmation pour la suppression d’un dispositif utilisé
      if( ($('#f_action').val()!='ajouter') && ($('#f_usage').val()>0) )
      {
        $.prompt(prompt_etapes);
      }
      else
      {
        formulaire.submit();
      }
      return false;
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Appel des fonctions en fonction des événements
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_mention, #table_engagement, #table_orientation').on( 'click' , 'q.ajouter'   , ajouter );
    $('#table_mention, #table_engagement, #table_orientation').on( 'click' , 'q.modifier'  , modifier_dupliquer_supprimer );
    $('#table_mention, #table_engagement, #table_orientation').on( 'click' , 'q.dupliquer' , modifier_dupliquer_supprimer );
    $('#table_mention, #table_engagement, #table_orientation').on( 'click' , 'q.supprimer' , modifier_dupliquer_supprimer );

    $('#form_gestion').on( 'click'   , '#bouton_annuler' , annuler );
    $('#form_gestion').on( 'click'   , '#bouton_valider' , soumettre_formulaire );
    $('#form_gestion').on( 'keydown' , 'input'           , function(e){intercepter(e);} );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_gestion');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_ordre    : { required:true , digits:true , range:[1,99] },
          f_synthese : { required:true , maxlength:20 },
          f_contenu  : { required:true , maxlength:80 }
        },
        messages :
        {
          f_ordre    : { required:'ordre manquant' , digits:'nombre entier requis' , range:'nombre entre 1 et 99' },
          f_synthese : { required:'synthèse manquante' , maxlength:'20 caractères maximum' },
          f_contenu  : { required:'contenu manquant' , maxlength:'80 caractères maximum' }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element) { element.after(error); }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        if (!please_wait)
        {
          $(this).ajaxSubmit(ajaxOptions);
          return false;
        }
        else
        {
          return false;
        }
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg_gestion').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        please_wait = true;
        $('#form_gestion button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      please_wait = false;
      $('#form_gestion button').prop('disabled',false);
      $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      please_wait = false;
      $('#form_gestion button').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
        switch (mode)
        {
          case 'ajouter':
          case 'modifier':
          case 'dupliquer':
            if(mode=='modifier')
            {
              $('#id_'+$('#f_id').val()).addClass('new').html(responseJSON['value']);
            }
            else
            {
              if(mode=='ajouter')
              {
                $('#table_'+categorie+' tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
              }
              $('#table_'+categorie+' tbody').append(responseJSON['value']);
            }
            break;
          case 'supprimer':
            $('#id_'+$('#f_id').val()).remove();
            break;
        }
        tableau_maj(categorie);
        $.fancybox.close();
        mode = false;
        categorie = false;
      }
    }

  }
);
