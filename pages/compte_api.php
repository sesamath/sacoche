<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('API'));

$api_jeton = !empty($_SESSION['MODULE']['API']) ? $_SESSION['MODULE']['API'] : '' ;

?>

<p><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_administrateur__gestion_administrateurs">DOC : API (interfaçage logiciel)</a></span></p>

<div class="astuce">
  Une <em>API</em> est une interface permettant à des applications informatiques d’échanger des informations.<br>
  On peut ici générer ou regénérer un jeton utilisé par l’application tierce pour authentifier vos requêtes sur <em>SACoche</em>.
</div>

<p class="travaux">Fonctionnalité expérimentale encore en développement.</p>

<form action="#" method="post" id="form_api">
<p>
  <label class="tab" for="f_url_sacoche">Adresse à utiliser :</label><input id="f_url_sacoche" name="f_url_sacoche" size="75" maxlength="75" type="text" value="<?php echo URL_DIR_SACOCHE.'api.php' ?>" readonly><br>
  <label class="tab" for="f_generer_jeton">Jeton de connexion :</label><input id="f_generer_jeton" name="f_generer_jeton" size="75" maxlength="75" type="text" value="<?php echo $api_jeton ?>" readonly><br>
  <span class="tab"></span><button data-objet="generer_jeton" type="button" class="parametre">Générer.</button><label id="ajax_msg_generer_jeton">&nbsp;</label>
</p>
</form>




