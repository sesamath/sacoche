<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

/*
 *   /!\ Cette page est aussi appelée par le script [evaluation_demande_professeur.js]
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action         = Clean::post('f_action'           , 'texte');
$plan_id        = Clean::post('f_plan'             , 'entier');
$item_id        = Clean::post('f_item'             , 'entier');
$eleve_id       = Clean::post('f_eleve'            , 'entier');
$note_val       = Clean::post('f_note'             , 'texte');
$devoir_id      = Clean::post('f_devoir'           , 'entier');
$groupe_id      = Clean::post('f_groupe'           , 'entier');
$groupe_type    = Clean::post('f_groupe_type'      , 'lettres');
$box_auto_descr = Clean::post('box_autodescription', 'bool');
$description    = Clean::post('f_description'      , 'texte', 60);

$tab_notes = array_merge( $_SESSION['NOTE_ACTIF'] , array( 'NN' , 'NE' , 'NF' , 'NR' , 'AB' , 'DI' , 'PA' , 'X' ) );

// Groupe
$tab_types = array(
  'Classes' => 'classe' ,
  'Groupes' => 'groupe' ,
  'Besoins' => 'besoin' ,
);

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Enregistrer une note
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='enregistrer_note') && $item_id && $eleve_id && in_array($note_val,$tab_notes) && ($devoir_id!==NULL) && ($groupe_id!==NULL) && ( $box_auto_descr || $description ) )
{
  // Nom du devoir
  $tab_jour = array(
    0 => 'dimanche',
    1 => 'lundi',
    2 => 'mardi',
    3 => 'mercredi',
    4 => 'jeudi',
    5 => 'vendredi',
    6 => 'samedi',
  );
  $tab_mois = array(
     1 => 'janvier',
     2 => 'février',
     3 => 'mars',
     4 => 'avril',
     5 => 'mai',
     6 => 'juin',
     7 => 'juillet',
     8 => 'août',
     9 => 'septembre',
    10 => 'octobre',
    11 => 'novembre',
    12 => 'décembre',
  );
  $description = ($box_auto_descr) ? 'Évaluation ponctuelle du '.$tab_jour[date("w")].' '.date("j").' '.$tab_mois[date("n")].' '.date("Y").'.' : $description ;
  // On cherche le devoir correspondant.
  $presence_devoir = FALSE;
  if( ($devoir_id) && ($groupe_id) && DB_STRUCTURE_PROFESSEUR::DB_tester_devoir_ponctuel_prof_by_ids( $devoir_id , $_SESSION['USER_ID'] , $groupe_id , $description ) )
  {
    $presence_devoir = TRUE;
  }
  else
  {
    //  Si absence d’identifiants transmis, alors soit le devoir n’existe pas, soit il existe et c’est la 1ère saisie d’une série
    $DB_ROW = DB_STRUCTURE_PROFESSEUR::DB_recuperer_devoir_ponctuel_prof_by_date( $_SESSION['USER_ID'] ,  TODAY_SQL , $description );
    if(!empty($DB_ROW))
    {
      $presence_devoir = TRUE;
      $devoir_id = $DB_ROW['devoir_id'];
      $groupe_id = $DB_ROW['groupe_id'];
    }
  }
  // Si pas de devoir, il faut l’ajouter
  if(!$presence_devoir)
  {
    // Commencer par créer un nouveau groupe de type "eval", utilisé uniquement pour cette évaluation (c’est transparent pour le professeur) ; y associe automatiquement le prof, en responsable du groupe
    $groupe_id = DB_STRUCTURE_REGROUPEMENT::DB_ajouter_groupe_par_prof( $_SESSION['USER_ID'] , 'eval' /*groupe_type*/ , '' /*groupe_nom*/ , 0 /*niveau_id*/ );
    // Insèrer l’enregistrement de l’évaluation
    $devoir_id = DB_STRUCTURE_PROFESSEUR::DB_ajouter_devoir( $_SESSION['USER_ID'] , $groupe_id , TODAY_SQL , $description , NULL /*date_devoir_visible_sql*/ , NULL /*date_saisie_visible_sql*/ ,NULL /*date_autoeval_sql*/ , '' /*doc_sujet*/ , '' /*doc_corrige*/ , 0 /*voir_repartition*/ , 0 /*diagnostic*/ , 0 /*pluriannuel*/ , 'nom' /*eleves_ordre*/ , 0 /*$equipe*/ );
  }
  // Maintenant on recupère le contenu de la base déjà enregistré pour le comparer avec la saisie envoyée.
  $presence_item   = FALSE;
  $presence_eleve  = FALSE;
  $presence_saisie = FALSE;
  $DB_TAB = ($presence_devoir) ? DB_STRUCTURE_PROFESSEUR::DB_lister_devoir_saisies( $devoir_id , TRUE /*with_marqueurs*/ ) : array() ;
  foreach($DB_TAB as $DB_ROW)
  {
    if($DB_ROW['item_id']==$item_id)
    {
      $presence_item = TRUE ;
    }
    if($DB_ROW['eleve_id']==$eleve_id)
    {
      $presence_eleve = TRUE ;
    }
    if( ($DB_ROW['item_id']==$item_id) && ($DB_ROW['eleve_id']==$eleve_id) )
    {
      $presence_saisie = $DB_ROW['saisie_note'] ;
      break; // Pas besoin de tester davantage, on sort du foreach()
    }
  }
  // On enregistre les modifications.
  $info = $description.' ('.To::texte_genre_identite($_SESSION['USER_NOM'],FALSE,$_SESSION['USER_PRENOM'],TRUE,$_SESSION['USER_GENRE']).')';
  if(!$presence_item)
  {
    // 'ajouter' plutôt que 'creer' car en cas d’ajout puis de suppression d’une note à un élève, un item peut se retrouver déjà affecté à un devoir sans qu’il n’y ait de note trouvée
    DB_STRUCTURE_PROFESSEUR::DB_modifier_liaison_devoir_item( $devoir_id , array($item_id) , 'ajouter' );
  }
  if(!$presence_eleve)
  {
    // 'ajouter' plutôt que 'creer' car en cas d’ajout puis de suppression d’une note à un élève, un élève peut se retrouver déjà affecté à un devoir sans qu’il n’y ait de note trouvée
    DB_STRUCTURE_PROFESSEUR::DB_modifier_liaison_devoir_eleve( $_SESSION['USER_ID'] , $devoir_id , $groupe_id , array($eleve_id) , 'ajouter' );
  }
  if( !$presence_devoir && count($_SESSION['PROF_TAB_REMPLACEMENT']) )
  {
    // Affecter tous les profs associés à un remplacement ; pas de notification envoyée.
    $tab_retour = DB_STRUCTURE_PROFESSEUR::DB_modifier_liaison_devoir_prof( $devoir_id , $_SESSION['PROF_TAB_REMPLACEMENT'] , 'creer' );
  }
  $notif_eleve = FALSE;
  if($presence_saisie==FALSE)
  {
    if($note_val!='X')
    {
      DB_STRUCTURE_PROFESSEUR::DB_ajouter_saisie( $_SESSION['USER_ID'] , $eleve_id , $devoir_id , $item_id , TODAY_SQL , $note_val , $info , NULL /*item_date_visible_sql*/ );
      $notif_eleve = TRUE;
    }
  }
  else
  {
    if($note_val=='X')
    {
      DB_STRUCTURE_PROFESSEUR::DB_supprimer_saisie( $eleve_id , $devoir_id , $item_id );
      $notif_eleve = TRUE;
    }
    elseif($presence_saisie!=$note_val)
    {
      DB_STRUCTURE_PROFESSEUR::DB_modifier_saisie( $_SESSION['USER_ID'] , $eleve_id , $devoir_id , $item_id , $note_val , $info );
      $notif_eleve = TRUE;
    }
  }
  // Notifications (rendues visibles ultérieurement) ; le mode discret ne d’applique volontairement pas ici car les modifications sont chirurgicales
  if($notif_eleve)
  {
    $abonnement_ref = 'devoir_saisie';
    $listing_eleves = (string)$eleve_id;
    $listing_parents = DB_STRUCTURE_NOTIFICATION::DB_lister_parents_listing_id($listing_eleves);
    $listing_users = ($listing_parents) ? $listing_eleves.','.$listing_parents : $listing_eleves ;
    $listing_abonnes = DB_STRUCTURE_NOTIFICATION::DB_lister_destinataires_listing_id( $abonnement_ref , $listing_users );
    if($listing_abonnes)
    {
      $adresse_lien_profond = Sesamail::adresse_lien_profond('page=evaluation&section=voir&devoir_id='.$devoir_id.'&eleve_id=');
      $notification_contenu = 'Saisie "à la volée" enregistrée par '.To::texte_genre_identite($_SESSION['USER_NOM'],FALSE,$_SESSION['USER_PRENOM'],TRUE,$_SESSION['USER_GENRE']).'.'."\r\n\r\n";
      $tab_abonnes = DB_STRUCTURE_NOTIFICATION::DB_lister_detail_abonnes_envois( $listing_abonnes , $listing_eleves , $listing_parents );
      foreach($tab_abonnes as $abonne_id => $tab_abonne)
      {
        foreach($tab_abonne as $eleve_id => $notification_intro_eleve)
        {
          $notification_lien = 'Voir le détail :'."\r\n".$adresse_lien_profond.$eleve_id;
          DB_STRUCTURE_NOTIFICATION::DB_modifier_log_attente( $abonne_id , $abonnement_ref , $devoir_id , NULL , $notification_intro_eleve.$notification_contenu.$notification_lien , 'remplacer' );
        }
      }
    }
  }
  // Afficher le retour
  Json::end( TRUE ,  array( 'devoir_id'=>$devoir_id , 'groupe_id'=>$groupe_id ) );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Charger un plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='charger_plan_classe') && $plan_id && $groupe_id && isset($tab_types[$groupe_type]) )
{
  $groupe_type = $tab_types[$groupe_type];
  // Élèves
  $DB_TAB = DB_STRUCTURE_COMMUN::DB_lister_users_regroupement( 'eleve' /*profil_type*/ , 1 /*statut*/ , $groupe_type , $groupe_id , 'nom' /*eleves_ordre*/ );
  $nb_eleves = count($DB_TAB);
  if(!$nb_eleves)
  {
    Json::end( FALSE , 'Aucun élève trouvé dans ce regroupement !' );
  }
  if($nb_eleves>99)
  {
    Json::end( FALSE , 'Trop d’élèves présents dans ce regroupement !' );
  }
  $tab_eleve = array();
  foreach($DB_TAB as $DB_ROW)
  {
    $tab_eleve[$DB_ROW['user_id']] = array(
      'nom'    => $DB_ROW['user_nom'],
      'prenom' => $DB_ROW['user_prenom'],
    );
  }
  // On vérifie que ce sont bien les élèves du professeur
  if($_SESSION['USER_JOIN_GROUPES']=='config')
  {
    Outil::verif_eleves_prof( array_keys($tab_eleve) );
  }
  // On récupère les photos si elles existent
  $coef_reduction = 0.5;
  $img_height = PHOTO_DIMENSION_MAXI * $coef_reduction;
  $img_width  = PHOTO_DIMENSION_MAXI*2/3 * $coef_reduction;
  foreach($tab_eleve as $eleve_id => $tab)
  {
    $tab_eleve[$eleve_id] += array(
      'img_width'  => $img_width,
      'img_height' => $img_height,
      'img_src'    => '',
      'img_title'  => TRUE,
    );
  }
  $listing_user_id = implode(',',array_keys($tab_eleve));
  $DB_TAB = DB_STRUCTURE_IMAGE::DB_lister_images( $listing_user_id , 'photo' );
  if(!empty($DB_TAB))
  {
    foreach($DB_TAB as $DB_ROW)
    {
      $tab_eleve[$DB_ROW['user_id']]['img_width']  = $DB_ROW['image_largeur'] * $coef_reduction;
      $tab_eleve[$DB_ROW['user_id']]['img_height'] = $DB_ROW['image_hauteur'] * $coef_reduction;
      $tab_eleve[$DB_ROW['user_id']]['img_src']    = $DB_ROW['image_contenu'];
      $tab_eleve[$DB_ROW['user_id']]['img_title']  = FALSE;
    }
  }
  // Plan de classe
  $DB_ROW = DB_STRUCTURE_PROFESSEUR_PLAN::DB_recuperer_plan_prof( $plan_id );
  if(empty($DB_ROW))
  {
    Json::end( FALSE , 'Plan de classe introuvable !' );
  }
  if( $DB_ROW['groupe_id'] != $groupe_id )
  {
    Json::end( FALSE , 'Plan de classe d’un autre regroupement !' );
  }
  if( $DB_ROW['prof_id'] != $_SESSION['USER_ID'] )
  {
    Json::end( FALSE , 'Plan de classe d’un autre collègue !' );
  }
  // Vérifier que le plan de classe est assez grand
  $nb_places = $DB_ROW['plan_nb_rangees'] * $DB_ROW['plan_nb_colonnes'];
  $nb_eleves = count($tab_eleve);
  if( $nb_eleves > $nb_places )
  {
    Json::end( FALSE , 'Plan trop petit ('.$nb_places.' places pour '.$nb_eleves.' élèves) !' );
  }
  // Récupèrer le placement des élèves, positionner au passage les élèves s’ils ne l’ont pas été, corriger d’éventuelles anomalies
  $tab_places_occupees = Outil::recuperer_ajuster_places_eleves( $plan_id , $DB_ROW['plan_nb_rangees'] , $DB_ROW['plan_nb_colonnes'] , $tab_eleve );
  // Retour
  $li = '';
  Json::add_row( 'nb_rangees'  , $DB_ROW['plan_nb_rangees']  );
  Json::add_row( 'nb_colonnes' , $DB_ROW['plan_nb_colonnes'] );
  foreach($tab_places_occupees as $jointure_rangee => $tab_colonnes)
  {
    foreach($tab_colonnes as $jointure_colonne => $tab)
    {
      if(is_null($tab))
      {
        $div = '';
      }
      else
      {
        $img_src   = ($tab['img_src'])   ? ' src="data:'.image_type_to_mime_type(IMAGETYPE_JPEG).';base64,'.$tab['img_src'].'"' : ' src="./_img/trombinoscope_vide.png"' ;
        $img_title = ($tab['img_title']) ? infobulle('absence de photo') : '' ;
        $img_html  = '<img width="'.$tab['img_width'].'" height="'.$tab['img_height'].'" alt=""'.$img_src.$img_title.'>';
        $div = '<div id="id'.$tab['id'].'">'.$img_html.'<span data="nom">'.html($tab['nom']).'</span><br><span data="prenom">'.html($tab['prenom']).'</span><br><input type="text" class="X" value="" readonly></div>' ;
      }
      $li .= '<li>'.$div.'</li>';
    }
  }
  Json::add_row( 'li' , $li );
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );
?>
