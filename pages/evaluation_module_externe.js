/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // Alerter sur la nécessité de valider
    $('#form_module').on( 'change' , 'input' ,
      function()
      {
        var f_objet = $(this).attr('id').substring(2);
        $('#ajax_msg_'+f_objet).attr('class','alerte').html('Enregistrer pour confirmer.');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire form_module
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_module').on( 'click' , 'button' ,
      function()
      {
        var bouton  = $(this);
        var f_objet = bouton.data('objet');
        var f_url   = $('#f_'+f_objet).val();
        if( (f_url!='') && !testURL(f_url) )
        {
          $('#ajax_msg_'+f_objet).attr('class','erreur').html('Adresse incorrecte !');
          $('#f_'+f_objet).focus();
          return false;
        }
        else
        {
          bouton.prop('disabled',true);
          $('#ajax_msg_'+f_objet).attr('class','loader').html('En cours&hellip;');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&f_objet='+f_objet+'&f_url='+encodeURIComponent(f_url),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                bouton.prop('disabled',false);
                $('#ajax_msg_'+f_objet).attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                return false;
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                bouton.prop('disabled',false);
                if(responseJSON['statut']==true)
                {
                  $('#ajax_msg_'+f_objet).attr('class','valide').html('Valeur enregistrée !');
                }
                else
                {
                  $('#ajax_msg_'+f_objet).attr('class','alerte').html(responseJSON['value']);
                }
              }
            }
          );
        }
      }
    );

  }
);
