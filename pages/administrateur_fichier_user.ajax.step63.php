<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if(!isset($STEP))       {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Étape 63 - Analyse de modifications d’affectations élèves/groupes (edt_eleves_groupes)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$import_profil = 'eleve';

// Pour préparer l’affichage
$lignes_pb_user   = '';
$lignes_pb_groupe = '';
$lignes_ajouter   = '';
$lignes_retirer   = '';
$lignes_conserver = '';

// On récupère le fichier avec des infos sur les correspondances : $tab_liens_id_base['classes'] -> $tab_i_classe_TO_id_base ; $tab_liens_id_base['groupes'] -> $tab_i_groupe_TO_id_base ; $tab_liens_id_base['users'] -> $tab_i_fichier_TO_id_base
$tab_liens_id_base = FileSystem::recuperer_fichier_infos_serializees( CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'liens_id_base.txt' );
$tab_i_classe_TO_id_base  = $tab_liens_id_base['classes'];
$tab_i_groupe_TO_id_base  = $tab_liens_id_base['groupes'];
$tab_i_fichier_TO_id_base = $tab_liens_id_base['users'];

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On vérifie que les utilisateurs sont bien dans la base.
// ////////////////////////////////////////////////////////////////////////////////////////////////////

// On récupère le fichier avec les utilisateurs : $tab_users_fichier['champ'] : i -> valeur, avec comme champs : sconet_id / profil_sigle / nom / prenom / birth_date / groupes
$tab_users_fichier = FileSystem::recuperer_fichier_infos_serializees( CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'users.txt' );
// On récupère le contenu de la base pour comparer : $tab_users_base['champ'] : id -> valeur, avec comme champs : sconet_id / profil / nom / prenom / birth_date
$tab_users_base                 = array();
$tab_users_base['sconet_id'   ] = array();
$tab_users_base['profil_sigle'] = array();
$tab_users_base['nom'         ] = array();
$tab_users_base['prenom'      ] = array();
$tab_users_base['birth_date'  ] = array();
$liste_champs = 'user_id,user_sconet_id,user_profil_sigle,user_nom,user_prenom,user_naissance_date';
$DB_TAB = DB_STRUCTURE_ADMINISTRATEUR::DB_lister_users( $import_profil , 1 /*only_actuels*/ , $liste_champs , FALSE /*with_classe*/ , FALSE /*tri_statut*/ );
foreach($DB_TAB as $DB_ROW)
{
  $tab_users_base['sconet_id'   ][$DB_ROW['user_id']] = $DB_ROW['user_sconet_id'];
  $tab_users_base['profil_sigle'][$DB_ROW['user_id']] = $DB_ROW['user_profil_sigle'];
  $tab_users_base['nom'         ][$DB_ROW['user_id']] = $DB_ROW['user_nom'];
  $tab_users_base['prenom'      ][$DB_ROW['user_id']] = $DB_ROW['user_prenom'];
  $tab_users_base['birth_date'  ][$DB_ROW['user_id']] = To::date_sql_to_french($DB_ROW['user_naissance_date']);
}
// Comparer fichier et base : c’est parti !
$tab_indices_fichier = array_keys($tab_users_fichier['sconet_id']);
// Parcourir chaque entrée du fichier
foreach($tab_indices_fichier as $i_fichier)
{
  $id_base = FALSE;
  // Recherche sur sconet_id
  if($tab_users_fichier['sconet_id'][$i_fichier])
  {
    $id_base = array_search($tab_users_fichier['sconet_id'][$i_fichier],$tab_users_base['sconet_id']);
  }
  // Recherche sur nom prénom
  else
  {
    $tab_id_nom    = array_keys($tab_users_base['nom'],$tab_users_fichier['nom'][$i_fichier]);
    $tab_id_prenom = array_keys($tab_users_base['prenom'],$tab_users_fichier['prenom'][$i_fichier]);
    $tab_id_commun = array_intersect($tab_id_nom,$tab_id_prenom);
    $nb_homonymes  = count($tab_id_commun);
    if($nb_homonymes==1)
    {
      $id_base = current($tab_id_commun);
    }
  }
  // Si absent de la base alors contenu à ignorer
  if( !$id_base )
  {
    $lignes_pb_user .= '<tr><th>Ignorer</th><td colspan="2">'.html($tab_users_fichier['sconet_id'][$i_fichier].' / '.$tab_users_fichier['nom'][$i_fichier].' '.$tab_users_fichier['prenom'][$i_fichier]).'</td></tr>'.NL;
    unset(
      $tab_users_fichier['sconet_id'   ][$i_fichier] ,
      $tab_users_fichier['profil_sigle'][$i_fichier] ,
      $tab_users_fichier['nom'         ][$i_fichier] ,
      $tab_users_fichier['prenom'      ][$i_fichier] ,
      $tab_users_fichier['birth_date'  ][$i_fichier] ,
      $tab_users_fichier['groupe'      ][$i_fichier]
    );
  }
  // Si présent dans la base alors retenir la correspondance d’indice fichier -> base
  else
  {
    $tab_i_fichier_TO_id_base[$i_fichier] = $id_base;
    unset(
      $tab_users_base['sconet_id'   ][$id_base] ,
      $tab_users_base['profil_sigle'][$id_base] ,
      $tab_users_base['nom'         ][$id_base] ,
      $tab_users_base['prenom'      ][$id_base] ,
      $tab_users_base['birth_date'  ][$id_base]
    );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On vérifie que les groupes sont bien dans la base.
// ////////////////////////////////////////////////////////////////////////////////////////////////////

// On récupère le fichier avec les groupes : $tab_groupes_fichier['ref'] : i -> ref
$tab_groupes_fichier = FileSystem::recuperer_fichier_infos_serializees( CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'groupes.txt' );
// On récupère le contenu de la base pour comparer : $tab_groupes_base['ref'] : id -> ref ; $tab_groupes_base['nom'] : id -> nom
$tab_groupes_base        = array();
$tab_groupes_base['ref'] = array();
$tab_groupes_base['nom'] = array();
$DB_TAB = DB_STRUCTURE_REGROUPEMENT::DB_lister_groupes();
foreach($DB_TAB as $DB_ROW)
{
  $tab_groupes_base['ref'][$DB_ROW['groupe_id']] = $DB_ROW['groupe_ref'];
  $tab_groupes_base['nom'][$DB_ROW['groupe_id']] = $DB_ROW['groupe_nom'];
}
// Comparer fichier et base : c’est parti !
foreach($tab_groupes_fichier['ref'] as $i_groupe => $ref)
{
  $id_base = array_search($ref,$tab_groupes_base['ref']);
  if($id_base)
  {
    $tab_i_groupe_TO_id_base[$i_groupe] = $id_base;
    unset($tab_groupes_fichier['ref'][$i_groupe] , $tab_groupes_base['ref'][$id_base] , $tab_groupes_base['nom'][$id_base]);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// associations élèves/groupes
// ////////////////////////////////////////////////////////////////////////////////////////////////////

// On récupère le contenu de la base pour comparer : $tab_base_affectation[user_id_groupe_id]=TRUE et $tab_base_groupe[groupe_id]=groupe_nom
// En deux requêtes sinon on ne récupère pas les groupes sans utilisateurs affectés.
$tab_base_groupe = array();
$DB_TAB = DB_STRUCTURE_REGROUPEMENT::DB_lister_groupes();
foreach($DB_TAB as $DB_ROW)
{
  $tab_base_groupe[$DB_ROW['groupe_id']] = $DB_ROW['groupe_nom'];
}
$tab_base_affectation = array();
$DB_TAB = DB_STRUCTURE_ADMINISTRATEUR::DB_lister_users_avec_groupe( $import_profil , TRUE /*only_actuels*/ );
foreach($DB_TAB as $DB_ROW)
{
  $tab_base_affectation[$DB_ROW['user_id'].'_'.$DB_ROW['groupe_id']] = TRUE;
  $tab_base_user_identite[$DB_ROW['user_id']] = $DB_ROW['user_nom'].' '.$DB_ROW['user_prenom'];
}
// Parcourir chaque entrée du fichier à la recherche d’affectations utilisateurs/groupes
foreach( $tab_users_fichier['groupe'] as $i_fichier => $tab_groupes )
{
  if(count($tab_groupes))
  {
    foreach( $tab_groupes as $i_groupe => $groupe_ref )
    {
      // Le groupe n’a pas de correspondance en base
      if( !isset($tab_i_groupe_TO_id_base[$i_groupe]) )
      {
        $lignes_pb_groupe .= '<tr><th>Ignorer</th><td>'.html($tab_users_fichier['sconet_id'][$i_fichier].' / '.$tab_users_fichier['nom'][$i_fichier].' '.$tab_users_fichier['prenom'][$i_fichier]).'</td><td>'.html($tab_groupes_fichier['ref'][$i_groupe]).'</td></tr>'.NL;
        unset($tab_users_fichier['groupe'][$i_fichier][$i_groupe]);
      }
      else
      {
        $user_id   = $tab_i_fichier_TO_id_base[$i_fichier];
        $groupe_id = $tab_i_groupe_TO_id_base[$i_groupe];
        // L’association existe déjà : on la conserve
        if(isset($tab_base_affectation[$user_id.'_'.$groupe_id]))
        {
          $lignes_conserver .= '<tr><th>Conserver</th><td>'.html($tab_users_fichier['nom'][$i_fichier].' '.$tab_users_fichier['prenom'][$i_fichier]).'</td><td>'.html($tab_base_groupe[$groupe_id]).'</td></tr>'.NL;
          unset($tab_base_affectation[$user_id.'_'.$groupe_id]);
        }
        // L’association n’existe pas : il faut l’ajouter
        else
        {
          $lignes_ajouter .= '<tr><th><label for="groupe_'.$user_id.'_'.$groupe_id.'_1">Ajouter <input id="groupe_'.$user_id.'_'.$groupe_id.'_1" name="groupe_'.$user_id.'_'.$groupe_id.'_1" type="checkbox" checked></label></th><td>'.html($tab_users_fichier['nom'][$i_fichier].' '.$tab_users_fichier['prenom'][$i_fichier]).'</td><td>'.html($tab_base_groupe[$groupe_id]).'</td></tr>'.NL;
        }
      }
    }
  }
}
// Associations à retirer
if(count($tab_base_affectation))
{
  foreach($tab_base_affectation as $key => $bool)
  {
    list($user_id,$groupe_id) = explode('_',$key);
    $lignes_retirer .= '<tr><th><label for="groupe_'.$user_id.'_'.$groupe_id.'_0">Supprimer <input id="groupe_'.$user_id.'_'.$groupe_id.'_0" name="groupe_'.$user_id.'_'.$groupe_id.'_0" type="checkbox" checked></label></th><td>'.html($tab_base_user_identite[$user_id]).'</td><td>'.html($tab_base_groupe[$groupe_id]).'</td></tr>'.NL;
  }
}
// On enregistre (tableau mis à jour)
$tab_liens_id_base = array(
  'classes' => $tab_i_classe_TO_id_base,
  'groupes' => $tab_i_groupe_TO_id_base,
  'users'   => $tab_i_fichier_TO_id_base,
);
FileSystem::enregistrer_fichier_infos_serializees( CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'liens_id_base.txt', $tab_liens_id_base );
// On affiche
Json::add_str('<p><label class="valide">Veuillez vérifier le résultat de l’analyse des affectations éventuelles.</label></p>'.NL);
if( $lignes_retirer )
{
  Json::add_str('<p class="danger">Des associations n’ont pas été trouvées.<br>Elles peuvent être dues à des ajouts manuels antérieurs dans SACoche.<br>Décochez leur suppression si besoin !</p>'.NL);
}
$ligne_vide = '<tr><td colspan="3">Aucune</td></tr>'.NL;
if(empty($lignes_ajouter  )) { $lignes_ajouter   = $ligne_vide; }
if(empty($lignes_retirer  )) { $lignes_retirer   = $ligne_vide; }
if(empty($lignes_conserver)) { $lignes_conserver = $ligne_vide; }
Json::add_str('<table>'.NL);
Json::add_str(  '<tbody>'.NL);
Json::add_str(    '<tr><th colspan="3">Associations utilisateurs / groupes à ajouter.<q class="cocher_tout"'.infobulle('Tout cocher.').'></q><q class="cocher_rien"'.infobulle('Tout décocher.').'></q></th></tr>'.NL);
Json::add_str(    $lignes_ajouter);
Json::add_str(  '</tbody>'.NL);
Json::add_str(  '<tbody>'.NL);
Json::add_str(    '<tr><th colspan="3">Associations utilisateurs / groupes à supprimer.<q class="cocher_tout"'.infobulle('Tout cocher.').'></q><q class="cocher_rien"'.infobulle('Tout décocher.').'></q></th></tr>'.NL);
Json::add_str(    $lignes_retirer);
Json::add_str(  '</tbody>'.NL);
Json::add_str(  '<tbody>'.NL);
Json::add_str(    '<tr><th colspan="3">Associations utilisateurs / groupes conservées.</th></tr>'.NL);
Json::add_str(    $lignes_conserver);
Json::add_str(  '</tbody>'.NL);
if(!empty($lignes_pb_user))
{
  Json::add_str(  '<tbody>');
  Json::add_str(    '<tr><th colspan="3">Utilisateur indiqué dans le fichier mais sans correspondance en base.</th></tr>'.NL);
  Json::add_str(    $lignes_pb_user);
  Json::add_str(  '</tbody>'.NL);
}
if(!empty($lignes_pb_groupe))
{
  Json::add_str(  '<tbody>');
  Json::add_str(    '<tr><th colspan="3">Groupe indiqué dans le fichier mais sans correspondance en base.</th></tr>'.NL);
  Json::add_str(    $lignes_pb_groupe);
  Json::add_str(  '</tbody>'.NL);
}
Json::add_str('</table>'.NL);
Json::add_str('<ul class="puce p"><li><a href="#step64" id="envoyer_infos_utilisateurs">Valider et afficher le bilan obtenu.</a><label id="ajax_msg">&nbsp;</label></li></ul>'.NL);

?>
