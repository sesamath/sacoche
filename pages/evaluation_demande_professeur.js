/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var nb_caracteres_max = 2000;

    // tri des tableaux (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{0:{sorter:false},7:{sorter:'date_fr'},10:{sorter:false},11:{sorter:false},12:{sorter:false}} });
    var tableau_gestion_tri = function(){ $('#table_action').trigger( 'sorton' , [ [[9,0],[1,0],[3,1],[2,0]] ] ); };
    var tableau_gestion_maj = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    tableau_gestion_tri();
    $('#table_stats').tablesorter({sortLocaleCompare : true});
    var tableau_stats_tri = function(){ $('#table_stats').trigger( 'sorton' , [ [[0,0],[1,0],[2,0]] ] ); };
    var tableau_stats_maj = function(){ $('#table_stats').trigger( 'update' , [ true ] ); };
    tableau_stats_tri();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le checkbox pour choisir ou non une date visible différente de la date du devoir
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_devoir_visible()
    {
      // Emploi de css() au lieu de show() hide() car sinon conflits constatés avec $('#step_creer').show() et $('#step_creer').hide() vers ligne 360.
      if( $('#f_choix_devoir_visible option:selected').val() == 'perso' )
      {
        $('#span_devoir_visible_perso').show(0).children('input').focus();
      }
      else
      {
        $('#span_devoir_visible_perso').hide(0);
      }
    }

    function maj_saisie_visible()
    {
      // Emploi de css() au lieu de show() hide() car sinon conflits constatés avec $('#step_creer').show() et $('#step_creer').hide() vers ligne 360.
      if( $('#f_choix_saisie_visible option:selected').val() == 'perso' )
      {
        $('#span_saisie_visible_perso').show(0).children('input').focus();
      }
      else
      {
        $('#span_saisie_visible_perso').hide(0);
      }
    }

    function maj_autoeval()
    {
      // Emploi de css() au lieu de show() hide() car sinon conflits constatés avec $('#step_creer').show() et $('#step_creer').hide() vers ligne 360.
      if($('#box_autoeval').is(':checked'))
      {
        $('#f_date_autoeval').val('00/00/0000');
        $('#box_autoeval').next().css('display','inline-block').next().css('display','none');
      }
      else
      {
        $('#box_autoeval').next().css('display','none').next().css('display','inline-block');
        $('#f_date_autoeval').val(window.input_autoeval);
      }
    }

    function maj_dates()
    {
      if( $('#f_quoi option:selected').val() == 'completer')
      {
        var tab_infos = $('#f_devoir option:selected').text().split(' || ');
        var devoir_visible = $('#f_devoir option:selected').data('devoir_visible');
        var saisie_visible = $('#f_devoir option:selected').data('saisie_visible');
        var date_devoir = tab_infos[0];
        var description = tab_infos[1];
        var choix_devoir_visible = !test_dateITA(devoir_visible) ? devoir_visible : 'perso' ;
        var choix_saisie_visible = !test_dateITA(saisie_visible) ? saisie_visible : 'perso' ;
        var date_devoir_visible  = !test_dateITA(devoir_visible) ? '' : devoir_visible ;
        var date_saisie_visible  = !test_dateITA(saisie_visible) ? '' : saisie_visible ;
      }
      else
      {
        var date_devoir = '';
        var choix_devoir_visible = 'toujours';
        var choix_saisie_visible = 'toujours';
        var date_devoir_visible  = '';
        var date_saisie_visible  = '';
      }
      $('#f_date_devoir').val(date_devoir);
      $('#f_date_devoir_visible').val(date_devoir_visible);
      $('#f_date_saisie_visible').val(date_saisie_visible);
      $('#f_choix_devoir_visible option[value='+choix_devoir_visible+']').prop('selected',true);
      $('#f_choix_saisie_visible option[value='+choix_saisie_visible+']').prop('selected',true);
      if( $('#f_quoi option:selected').val() == 'completer')
      {
        $('#f_description').val(description);
      }
      maj_devoir_visible();
      maj_saisie_visible();
    }

    $('#f_choix_devoir_visible').change
    (
      function()
      {
        maj_devoir_visible();
      }
    );

    $('#f_choix_saisie_visible').change
    (
      function()
      {
        maj_saisie_visible();
      }
    );

    $('#box_autoeval').click
    (
      function()
      {
        maj_autoeval();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Afficher / masquer des options du premier formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_encours').change
    (
      function()
      {
        var affich_periode = ($('#f_encours').val()==0) ? true : false ;
        $('#zone_periodes').hideshow(affich_periode);
      }
    );

    var autoperiode = true; // Tant qu’on ne modifie pas manuellement le choix des périodes, modification automatique du formulaire

    function view_dates_perso()
    {
      var periode_val = $('#f_periode').val();
      if(periode_val!=0)
      {
        $('#dates_perso').attr('class','hide');
      }
      else
      {
        $('#dates_perso').attr('class','show');
      }
    }

    $('#f_periode').change
    (
      function()
      {
        $('#ajax_msg_prechoix').removeAttr('class').html('');
        view_dates_perso();
        autoperiode = false;
      }
    );

    // Changement de groupe (uniquement pour un groupe)
    // -> desactiver les périodes prédéfinies en cas de groupe de besoin
    // -> choisir automatiquement la meilleure période et chercher les évaluations si un changement manuel de période n’a jamais été effectué

    function modifier_periodes()
    {
      var groupe_type = $('#f_groupe option:selected').parent().attr('label');
      $('#f_periode option').each
      (
        function()
        {
          var periode_id = $(this).val();
          // La période personnalisée est tout le temps accessible
          if(periode_id!=0)
          {
            // groupe de besoin -> desactiver les périodes prédéfinies
            if( (typeof(groupe_type)=='undefined') || (groupe_type=='Besoins') )
            {
              $(this).prop('disabled',true);
            }
            // classe ou groupe classique -> toutes périodes accessibles
            else
            {
              $(this).prop('disabled',false);
            }
          }
        }
      );
      // Sélectionner si besoin la période personnalisée
      if( (typeof(groupe_type)=='undefined') || (groupe_type=='Besoins') )
      {
        $('#f_periode option[value=0]').prop('selected',true);
        $('#dates_perso').attr('class','show');
      }
      // Modification automatique du formulaire
      if( (groupe_type=='Classes') || (groupe_type=='Groupes') )
      {
        if(autoperiode)
        {
          // Rechercher automatiquement la meilleure période
          var id_groupe = $('#f_groupe option:selected').val();
          if(typeof(window.tab_groupe_periode[id_groupe])!='undefined')
          {
            for(var id_periode in window.tab_groupe_periode[id_groupe]) // Parcourir un tableau associatif...
            {
              var tab_split = window.tab_groupe_periode[id_groupe][id_periode].split('_');
              if( (window.TODAY_SQL>=tab_split[0]) && (window.TODAY_SQL<=tab_split[1]) )
              {
                $('#f_periode option[value='+id_periode+']').prop('selected',true);
                view_dates_perso();
                break;
              }
            }
          }
        }
      }
      // Soumettre le formulaire
      formulaire0.submit();
    }

    $('#f_groupe').change
    (
      function()
      {
        modifier_periodes();
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du premier formulaire pour afficher le tableau avec la liste des demandes
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire0 = $('#form_prechoix');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation0 = formulaire0.validate
    (
      {
        rules :
        {
          f_matiere : { required:false },
          f_groupe  : { required:false },
          f_prof    : { required:false },
          f_encours : { required:false },
          f_periode : { required:false },
          f_date_debut : { required:function(){return $('#f_encours').val()==0 && $('#f_periode').val()==0;} , dateITA:true },
          f_date_fin   : { required:function(){return $('#f_encours').val()==0 && $('#f_periode').val()==0;} , dateITA:true }
        },
        messages :
        {
          f_matiere    : { },
          f_groupe     : { },
          f_prof       : { },
          f_encours    : { },
          f_periode    : { },
          f_date_debut : { required:'date manquante' , dateITA:'date JJ/MM/AAAA incorrecte' },
          f_date_fin   : { required:'date manquante' , dateITA:'date JJ/MM/AAAA incorrecte' }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          if(element.is('select')) {element.after(error);}
          else if(element.attr('type')=='text') {element.next().after(error);}
        }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions0 =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_prechoix',
      beforeSubmit : test_form_avant_envoi0,
      error : retour_form_erreur0,
      success : retour_form_valide0
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire0.submit
    (
      function()
      {
        $('#table_action tbody').html('');
        $('#tr_sans').html('<td class="nu"></td>');
        $('#zone_actions').hide(0);
        $('#ajax_msg_gestion').removeAttr('class').html('');
        // Mémoriser le nom de la matière + le type de groupe + le nom du groupe
        $('#f_matiere_nom').val( $('#f_matiere option:selected').text() );
        $('#f_groupe_id').val(   $('#f_groupe option:selected').val() );
        $('#f_groupe_nom').val(  $('#f_groupe option:selected').text() );
        $('#f_groupe_type').val( $('#f_groupe option:selected').parent().attr('label') );
        $('#f2_groupe_id').val( $('#f_groupe_id').val() );
        $('#f2_groupe_type').val( $('#f_groupe_type').val() );
        $(this).ajaxSubmit(ajaxOptions0);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi0(formData, jqForm, options)
    {
      $('#ajax_msg_prechoix').removeAttr('class').html('');
      var readytogo = validation0.form();
      if(readytogo)
      {
        $('#ajax_msg_prechoix').attr('class','loader').html('En cours&hellip;');
        $('#form_gestion , #zone_stats , #table_autres #bilan').hide();
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur0(jqXHR, textStatus, errorThrown)
    {
      $('#ajax_msg_prechoix').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide0(responseJSON)
    {
      initialiser_compteur();
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_prechoix').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        $('#ajax_msg_prechoix').attr('class','valide').html('Données affichées ci-dessous.');
        // CAS DES DEMANDES EN COURS
        if( $('#f_encours').val()==1 )
        {
          $('#export_fichier').attr('href',responseJSON['file']);
          $('#zone_messages').html(responseJSON['msg']);
          $('#table_action tbody').html(responseJSON['tr']);
          $('#tr_sans').html(responseJSON['td']);
          tableau_gestion_maj();
          var etat_disabled = ($('#f_groupe_id').val()>0) ? false : true ;
          var affich_actions = ($('#f_prof option:selected').val()!=0) ? true : false ;
          $('#form_gestion , #table_autres').show();
          $('#f_qui option[value=groupe]').text($('#f_groupe_nom').val()).prop('disabled',etat_disabled);
          if(etat_disabled) { $('#f_qui option[value=select]').prop('selected',true); }
          maj_evaluation();
          $('#span_cocher').hideshow(affich_actions);
          $('#zone_actions').hideshow(affich_actions);
        }
        // CAS DES DEMANDES ARCHIVÉES
        else
        {
          $('#table_stats tbody').html(responseJSON['tr']);
          $('#tr_sans').html(responseJSON['td']);
          tableau_stats_maj();
          $('#zone_stats , #table_autres').show();
        }
      }
    }

    // Soumettre au chargement pour initialiser l’affichage, et au changement d’un select initial

    formulaire0.submit();

    $('#f_matiere , #f_groupe , #f_prof , #f_encours').change
    (
      function()
      {
        formulaire0.submit();
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic pour demander le recalcul d’un score
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'q.actualiser',
      function()
      {
        var obj_q      = $(this);
        var obj_td     = obj_q.parent();
        var obj_tr     = obj_td.parent();
        var score      = obj_td.text();
        var ids        = obj_tr.children('td:first').children('input').val();
        var debut_date = obj_tr.data('debut_date');
        score = (typeof(score)!=='undefined') ? entier(score) : -1 ;
        obj_q.removeAttr('class');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'actualiser_score'+'&ids='+ids+'&f_score='+score+'&f_debut_date='+debut_date,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+' Veuillez recommencer.'+'</label>' );
              obj_q.addClass('actualiser');
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==true)
              {
                obj_td.replaceWith(responseJSON['value']);
              }
              else
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
                obj_q.addClass('actualiser');
              }
            }
          }
        );
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic pour voir les évaluations d’un item et éventuellement les modifier
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'q.saisir',
      function()
      {
        var obj_q      = $(this);
        var obj_tr     = obj_q.parent().parent();
        var ids        = obj_tr.children('td:first').children('input').val();
        var debut_date = obj_tr.data('debut_date');
        var item_nom   = obj_tr.children('td').eq(2).children('img').attr('title');
        var eleve_nom  = obj_tr.children('td').eq(5).text();
        obj_q.removeAttr('class');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'voir_evaluations'+'&ids='+ids+'&f_debut_date='+debut_date,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+' Veuillez recommencer.'+'</label>' );
              obj_q.addClass('saisir');
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==true)
              {
                $('#report_ids').val(ids);
                $('#report_eleve').html(eleve_nom);
                $('#report_item').html(item_nom);
                $('#tbody_report_notes').html(responseJSON['value']);
                $('#champ_modif').hide(0);
                $('#ajax_msg_modifier').removeAttr('class').html('');
                $.fancybox( { href:'#zone_voir_modifier_evaluations' , minWidth:800 } );
              }
              else
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              obj_q.addClass('saisir');
            }
          }
        );
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic pour voir les messages des élèves
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#voir_messages').click
    (
      function()
      {
        $.fancybox( { href:'#zone_messages' } );
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Charger le select f_devoir en ajax
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_evaluation()
    {
      $('#f_devoir').html('<option value="">&nbsp;</option>');
      $('#ajax_maj1').attr('class','loader').html('En cours&hellip;');
      var eval_type = $('#f_qui option:selected').val();
      var groupe_id = $('#f_groupe_id').val();
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eval',
          data : 'f_objet=demande_eval_prof'+'&f_eval_type='+eval_type+'&f_groupe_id='+groupe_id,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj1').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj1').removeAttr('class').html('');
              $('#f_devoir').html(responseJSON['value']).show();
              maj_dates();
            }
            else
            {
              $('#ajax_maj1').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Tout cocher ou tout décocher
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'q.cocher_tout , q.cocher_rien',
      function()
      {
        var etat = ( $(this).attr('class').substring(7) == 'tout' ) ? true : false ;
        $('#table_action td.nu input[type=checkbox]').prop('checked',etat);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer visuellement la note cochée
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#step_saisir, #champ_modif').on
    (
      'click',
      'input[type=radio]',
      function()
      {
        $('#ajax_msg_modifier').removeAttr('class').html('');
        $(this).parent().parent().find('label').removeClass('check');
        $(this).parent().addClass('check');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le checkbox pour choisir ou non une description du devoir
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#box_autodescription').click
    (
      function()
      {
        if($(this).is(':checked'))
        {
          $(this).next().show(0).next().hide(0);
        }
        else
        {
          $(this).next().hide(0).next().show(0).children('input').focus();
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Éléments dynamiques du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Récupérer les noms des items des checkbox cochés pour la description de l’évaluation
    $('#table_action').on
    (
      'click',
      'input[type=checkbox]',
      function()
      {
        // Récupérer les checkbox cochés
        var listing_refs = '';
        $('#table_action input[type=checkbox]:checked').each
        (
          function()
          {
            var item = $(this).parent().next().next().text();
            var ref  = ' ' + item.substring( item.indexOf('.')+1 , item.length-1 );
            if(listing_refs.indexOf(ref)==-1)
            {
              listing_refs += ref;
            }
          }
        );
        if(listing_refs.length)
        {
          $('#f_description').val('Demande'+listing_refs);
        }
        $('#ajax_msg_gestion').removeAttr('class').html('');
        $('#step_saisir').children('label').removeClass('check');
        $('input[name=f_note]').prop('checked',false);
      }
    );

    // Afficher / masquer les éléments suivants du formulaire suivant le choix du select "f_quoi"
    // Si "f_quoi" vaut "completer" alors charger le select "f_devoir" en ajax
    $('#f_quoi').change
    (
      function()
      {
        $('#bilan').hide();
        var quoi = $('#f_quoi option:selected').val();
        if(quoi=='completer')
        {
          maj_evaluation();
        }
        $('#step_qui'      ).hideshow( (quoi=='creer') || (quoi=='completer') );
        $('#step_saisir'   ).hideshow( quoi=='saisir' );
        $('#step_creer'    ).hideshow( quoi=='creer' );
        $('#step_completer').hideshow( quoi=='completer' );
        $('#step_module'   ).hideshow( quoi=='generer_enonces' );
        $('#step_suite'    ).hideshow( (quoi=='creer') || (quoi=='completer') );
        $('#step_message'  ).hideshow( (quoi!='') && (quoi!='saisir') && (quoi!='generer_enonces') );
        if(quoi!='')
        {
          $('#step_valider').show(0);
        }
      }
    );

    // Charger le select "f_devoir" en ajax si "f_qui" change et que "f_quoi" est à "completer"
    $('#f_qui').change
    (
      function()
      {
        $('#bilan').hide();
        if( $('#f_quoi option:selected').val() == 'completer')
        {
          maj_evaluation();
        }
      }
    );

    $('#f_quoi , #f_devoir').change
    (
      function()
      {
        maj_dates();
      }
    );

    // Indiquer le nombre de caractères restants autorisés dans le textarea
    // input permet d’intercepter à la fois les saisies au clavier et les copier-coller à la souris (clic droit)
    $('#step_message').on( 'input' , '#f_message' , function() { afficher_textarea_reste( $(this) , nb_caracteres_max ); } );

    $('#tbody_report_notes').on
    (
      'click',
      'input',
      function()
      {
        $('#ajax_msg_modifier').removeAttr('class').html('');
        $('#p_note_modif').children('label').removeClass('check');
        $('input[name=f_note_modif]').prop('checked',false);
        $('#champ_modif').show(0);
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Choisir les professeurs associés à une évaluation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#step_creer').on
    (
      'click',
      'q.choisir_prof',
      function()
      {
        selectionner_profs_option( $('#f_prof_liste').val() );
        // Afficher la zone
        $.fancybox( { href:'#zone_profs' , modal:true , minWidth:700 } );
        if( !IsTouch )
        {
          $(document).tooltip('destroy');display_infobulle(); // Sinon, bug avec l’infobulle contenu dans le fancybox qui ne disparait pas au clic...
        }
      }
    );

    $('#f_groupe_profs').change
    (
      // Récupération du listing des profs associés à un regroupement
      function()
      {
        var obj_option = $(this).find('option:selected');
        var prof_groupe_id = obj_option.val();
        var listing_profs_concernes = obj_option.data('listing');
        if( prof_groupe_id && (typeof(listing_profs_concernes)=='undefined') )
        {
          $('#appliquer_droit_prof_groupe').prop('disabled',true);
          $('#load_profs_groupe').attr('class','loader').html('Recherche&hellip;');
          var prof_groupe_type = obj_option.parent().attr('label');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page=_maj_listing_professeurs',
              data : 'f_groupe_id='+prof_groupe_id+'&f_groupe_type='+prof_groupe_type,
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#appliquer_droit_prof_groupe').prop('disabled',false);
                $('#load_profs_groupe').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                if(responseJSON['statut']==true)
                {
                  obj_option.data('listing',responseJSON['value']);
                  $('#load_profs_groupe').removeAttr('class').html('');
                }
                else
                {
                  $('#load_profs_groupe').attr('class','alerte').html(responseJSON['value']);
                }
                $('#appliquer_droit_prof_groupe').prop('disabled',false);
              }
            }
          );
        }
      }
    );

    $('#appliquer_droit_prof_groupe').click
    (
      // Modification du choix du droit pour un lot de profs
      function()
      {
        var valeur = $('input[name=prof_check_all]:checked').val();
        if(typeof(valeur)=='undefined')
        {
          $('#load_profs_groupe').attr('class','erreur').html('Cocher un niveau de droit !');
          return false;
        }
        var listing_profs_concernes = $('#f_groupe_profs').find('option:selected').data('listing');
        if(typeof(listing_profs_concernes)=='undefined')
        {
          $('#load_profs_groupe').attr('class','erreur').html('Liste des collègues non récupérée !');
          return false;
        }
        var tab_profs_concernes = listing_profs_concernes.split(',');
        $('.prof_liste').find('select:enabled').each
        (
          function()
          {
            var prof_id = $(this).attr('id').substring(2); // "p_" + id
            var prof_valeur = ( tab_profs_concernes.indexOf(prof_id) == -1 ) ? 'x' : valeur ;
            $(this).find('option[value='+prof_valeur+']').prop('selected',true);
            $(this).next('span').attr('class','select_img droit_'+prof_valeur);
          }
        );
      }
    );

    $('#zone_profs').on
    (
      'change',
      'select',
      // Modification du choix du droit pour un prof
      function()
      {
        var val_option = $(this).find('option:selected').val();
        $(this).next('span').attr('class','select_img droit_'+val_option);
      }
    );

    $('#annuler_profs').click
    (
      function()
      {
        $.fancybox.close();
      }
    );

    $('#valider_profs').click
    (
      function()
      {
        var liste = '';
        var nombre = 0;
        $('.prof_liste').find('select').each
        (
          function()
          {
            var val_option = $(this).find('option:selected').val();
            if( (val_option!='x') && (val_option!='z') )
            {
              var tab_val = $(this).attr('id').split('_');
              var id_prof = tab_val[1];
              liste += val_option+id_prof+'_';
              nombre++;
            }
          }
        );
        liste  = (!nombre) ? '' : liste.substring(0,liste.length-1) ;
        nombre = (!nombre) ? 'non' : (nombre+1)+' profs' ;
        $('#f_prof_liste').val(liste);
        $('#f_prof_nombre').val(nombre);
        $('#annuler_profs').click();
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du formulaire principal
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // On sépare en 2 parties pour traiter les évaluations à la volée à part.
    $('#bouton_valider').click
    (
      function()
      {
        if($('#f_quoi').val()!='saisir')
        {
          formulaire.submit();
        }
        else
        {
          valider_envoi_saisie();
        }
      }
    );

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_gestion');

    // Ajout d’une méthode pour valider les dates de la forme jj/mm/aaaa (trouvé dans le zip du plugin, corrige en plus un bug avec Safari)
    // méthode dateITA déjà ajoutée

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_ids                  : { required:true },
          f_quoi                 : { required:true },
          f_qui                  : { required:function(){var quoi=$('#f_quoi').val(); return (quoi=='creer')||(quoi=='completer');} },
          f_choix_devoir_visible : { required:function(){return $('#f_quoi').val()=='creer';} },
          f_choix_saisie_visible : { required:function(){return $('#f_quoi').val()=='creer';} },
          f_date_devoir          : { required:function(){return $('#f_quoi').val()=='creer';} , dateITA:true },
          f_date_devoir_visible  : { required:function(){return ($('#f_quoi').val()=='creer')&&($('#f_choix_devoir_visible').val()=='perso');} , dateITA:true },
          f_date_saisie_visible  : { required:function(){return ($('#f_quoi').val()=='creer')&&($('#f_choix_saisie_visible').val()=='perso');} , dateITA:true },
          f_date_autoeval        : { required:function(){return ($('#f_quoi').val()=='creer')&&(!$('#box_autoeval').is(':checked'));} , dateITA:true },
          f_description          : { required:function(){var quoi=$('#f_quoi').val(); return (quoi=='creer')||(quoi=='completer');} , maxlength:60 },
          f_prof_liste           : { required:false },
          f_devoir               : { required:function(){return $('#f_quoi').val()=='completer';} },
          f_suite                : { required:function(){var quoi=$('#f_quoi').val(); return (quoi=='creer')||(quoi=='completer');} },
          f_message              : { required:false }
        },
        messages :
        {
          f_ids                  : { required:'cocher au moins une demande' },
          f_quoi                 : { required:'action manquante' },
          f_qui                  : { required:'groupe manquant' },
          f_choix_devoir_visible : { required:'choix manquant' },
          f_choix_saisie_visible : { required:'choix manquant' },
          f_date_devoir          : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_date_devoir_visible  : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_date_saisie_visible  : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_date_autoeval        : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_description          : { required:'nom manquant' , maxlength:'60 caractères maximum' },
          f_prof_liste           : { },
          f_devoir               : { required:'évaluation manquante' },
          f_suite                : { required:'suite manquante' },
          f_message              : { }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          if(element.is('select')) {element.after(error);}
          else if(element.attr('id')=='f_description') {element.after(error);}
          else if(element.attr('type')=='text') {element.next().after(error);}
          else if(element.attr('type')=='checkbox') {$('#ajax_msg_gestion').after(error);}
        }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_gestion',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        // grouper les checkbox multiples => normalement pas besoin si name de la forme nom[], mais ça pose pb à jquery.validate.js d’avoir un id avec []
        // alors j’ai copié le tableau dans un champ hidden...
        var f_ids = []; $('input[name=f_ids]:checked').each(function(){f_ids.push($(this).val());});
        $('#ids').val(f_ids);
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg_gestion').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        $('button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      $('button').prop('disabled',false);
      $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      $('button').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        var qui   = $('#f_qui'  ).val();
        var quoi  = $('#f_quoi' ).val();
        var suite = $('#f_suite').val();
        var new_statut = (quoi=='changer_request') ? 'request' : false ;
        var new_statut = ( ((quoi=='creer')&&(suite=='changer_aggree')) || ((quoi=='completer')&&(suite=='changer_aggree')) || (quoi=='changer_aggree') ) ? 'aggree' : new_statut ;
        var new_statut = ( ((quoi=='creer')&&(suite=='changer_ready' )) || ((quoi=='completer')&&(suite=='changer_ready' )) || (quoi=='changer_ready' ) ) ? 'ready' : new_statut ;
        var is_retirer = ( ((quoi=='creer')&&(suite=='changer_done'  )) || ((quoi=='completer')&&(suite=='changer_done'  )) || (quoi=='supprimer'     ) ) ? true : false ;
        if(new_statut)
        {
          // Changer le statut des demandes cochées
          if(new_statut=='request') { var tab = [ 'br' , 'en attente d’étude'   ]; }
          if(new_statut=='aggree' ) { var tab = [ 'bj' , 'acceptée, à préparer' ]; }
          if(new_statut=='ready'  ) { var tab = [ 'bv' , 'évaluation prête'     ]; }
          $('#table_action input[type=checkbox]:checked').each
          (
            function()
            {
              this.checked = false;
              $(this).parent().parent().find('td').eq(9).html(tab[1]).attr('class','label '+tab[0]);
            }
          );
          tableau_gestion_maj(); // sinon, un clic ultérieur pour retrier par statut ne fonctionne pas
        }
        else if(is_retirer)
        {
          // Retirer les demandes cochées
          $('#table_action input[type=checkbox]:checked').each
          (
            function()
            {
              $(this).parent().parent().remove();
            }
          );
        }
        // lien vers le devoir
        if( (quoi=='creer') || (quoi=='completer') )
        {
          var section = (qui=='select') ? 'selection' : 'groupe' ;
          $('#bilan_lien').attr('href','./index.php?page=evaluation&section=gestion_'+section+'&devoir_id='+responseJSON['devoir_id']+'&groupe_type='+responseJSON['groupe_type']+'&groupe_id='+responseJSON['groupe_id']);
          $('#bilan').show();
        }
        if(quoi=='generer_enonces')
        {
          $.fancybox( { href:responseJSON['value'] , type:'iframe' } );
        }
        $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
      }
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Enregistrement d’une évaluation à la volée
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function valider_envoi_saisie()
    {
      var tab_ids = []; $('input[name=f_ids]:checked').each(function(){tab_ids.push($(this).val());});
      var valeur = $('#step_saisir input[name=f_note]:checked').val();
      var devoir_id = $('#f_saisir_devoir').val();
      var groupe_id = $('#f_saisir_groupe').val();
      var box_autodescription = $('#box_autodescription').is(':checked') ? 1 : 0 ;
      var f_autodescription = $('#f_autodescription').val();
      $('#ids').val(tab_ids);
      if(!tab_ids.length)
      {
        $('#ajax_msg_gestion').attr('class','erreur').html('Cocher au moins une demande !');
        return false;
      }
      else if(typeof(valeur)=='undefined')
      {
        $('#ajax_msg_gestion').attr('class','erreur').html('Choisir une note !');
        return false;
      }
      else if( !box_autodescription && !f_autodescription )
      {
        $('#ajax_msg_gestion').attr('class','erreur').html('Choisir un intitulé ou cocher la case !');
        $('#f_autodescription').focus();
        return false;
      }
      else
      {
        $('button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
        enregistrer_saisie( tab_ids , valeur , devoir_id , groupe_id , box_autodescription , f_autodescription );
      }
    }

    function enregistrer_saisie( tab_ids , valeur , devoir_id , groupe_id , box_autodescription , f_autodescription )
    {
      var ids = tab_ids[0];
      tab_ids.shift();
      var tab = ids.split('x');
      var user_id = tab[1];
      var item_id = tab[2];
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=evaluation_ponctuelle',
          data : 'csrf='+window.CSRF+'&f_action=enregistrer_note'+'&f_item='+item_id+'&f_eleve='+user_id+'&f_note='+valeur+'&f_devoir='+devoir_id+'&f_groupe='+groupe_id+'&box_autodescription='+box_autodescription+'&f_description='+encodeURIComponent(f_autodescription),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('button').prop('disabled',false);
            $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              // On enregistre la note pour la demande suivante
              if(tab_ids.length)
              {
                enregistrer_saisie( tab_ids , valeur , responseJSON['devoir_id'] , responseJSON['groupe_id'] , box_autodescription , f_autodescription );
              }
              // ... ou on passe à la suppression des demandes
              else
              {
                archiver_demandes( responseJSON['devoir_id'] , responseJSON['groupe_id'] );
              }
            }
            else
            {
              $('button').prop('disabled',false);
              $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    function archiver_demandes( devoir_id , groupe_id )
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=changer_done'+'&devoir_saisie='+devoir_id+'&'+'ids='+$('#ids').val(),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('button').prop('disabled',false);
            $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('button').prop('disabled',false);
            if(responseJSON['statut']==false)
            {
              $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
            }
            else
            {
              $('#table_action input[type=checkbox]:checked').each
              (
                function()
                {
                  $(this).parent().parent().remove();
                }
              );
              $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
              $('#bilan_lien').attr('href','./index.php?page=evaluation&section=gestion_selection&devoir_id='+devoir_id+'&groupe_type='+'E'+'&groupe_id='+groupe_id);
              $('#bilan').show();
            }
          }
        }
      );
    }

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Enregistrement d’une saisie d’évaluation en remplacement d’une note précédente
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_modifier').click
    (
      function()
      {
        var obj_input_devoir = $('#tbody_report_notes input[name=devoir]:checked');
        var obj_input_note   = $('#p_note_modif input[name=f_note_modif]:checked');
        var f_ids      = $('#report_ids').val();
        var decription = obj_input_devoir.parent().next().next().next().text();
        var devoir_id  = obj_input_devoir.val();
        var valeur     = obj_input_note.val();
        var note_image = obj_input_note.next().clone();
        if(typeof(devoir_id)=='undefined')	// normalement impossible, sauf si par exemple on triche avec la barre d’outils Web Developer...
        {
          $('#ajax_msg_modifier').attr('class','erreur').html('Cocher l’évaluation concernée !');
          return false;
        }
        else if(typeof(valeur)=='undefined')
        {
          $('#ajax_msg_modifier').attr('class','erreur').html('Choisir une nouvelle note en remplacement !');
          return false;
        }
        else
        {
          $('#bouton_modifier').prop('disabled',true);
          $('#ajax_msg_modifier').attr('class','loader').html('En cours&hellip;');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&f_action=modifier_saisie'+'&f_devoir='+devoir_id+'_0'+'&f_note='+valeur+'&ids='+f_ids+'&f_description='+encodeURIComponent(decription),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#bouton_modifier').prop('disabled',false);
                $('#ajax_msg_modifier').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                $('#bouton_modifier').prop('disabled',false);
                if(responseJSON['statut']==false)
                {
                  $('#ajax_msg_modifier').attr('class','alerte').html(responseJSON['value']);
                }
                else
                {
                  $('#tr_'+f_ids).remove();
                  $('#devoir_'+devoir_id).parent().next().html(note_image);
                  $('#ajax_msg_modifier').attr('class','valide').html('Demande réalisée !');
                }
              }
            }
          );
        }
      }
    );

  }
);
