<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if(($_SESSION['SESAMATH_ID']==ID_DEMO)&&($_POST['f_action']!='initialiser')){Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action          = Clean::post('f_action'       , 'texte');
$modaccomp_id    = Clean::post('f_id'           , 'entier');
$modaccomp_code  = Clean::post('f_modaccomp'    , 'texte');
$commentaire     = Clean::post('f_commentaire'  , 'texte');
$only_groupes_id = Clean::post('only_groupes_id', 'texte');
$periodicite     = Clean::post('f_periodicite'  , 'entier');
// Pour les élèves avant c’était un tableau qui est transmis, mais à cause d’une limitation possible "suhosin" / "max input vars", on est passé à une concaténation en chaine...
$tab_periode = Clean::post('f_periodes', array('array',','));
$tab_eleve   = Clean::post('f_eleve'   , array('array',','));
$tab_periode = array_filter( Clean::map('entier',$tab_periode) , 'positif' );
$tab_eleve   = array_filter( Clean::map('entier',$tab_eleve)   , 'positif' );

// Noms des périodes
$tab_periode_livret = array(
  21 => 'Semestre 1/2',
  22 => 'Semestre 2/2',
  31 => 'Trimestre 1/3',
  32 => 'Trimestre 2/3',
  33 => 'Trimestre 3/3',
  41 => 'Bimestre 1/4',
  42 => 'Bimestre 2/4',
  43 => 'Bimestre 3/4',
  44 => 'Bimestre 4/4',
  51 => 'Période 1/5',
  52 => 'Période 2/5',
  53 => 'Période 3/5',
  54 => 'Période 4/5',
  55 => 'Période 5/5',
  ','=> ' &#9474; ',
);
$tab_bad = array_keys($tab_periode_livret);
$tab_bon = array_values($tab_periode_livret);

//
// Retirer une association
//

if($action=='retirer')
{
  // vérifs
  if( !$modaccomp_id )
  {
    Json::end( FALSE , 'Erreur avec les données transmises !' );
  }
  // go
  DB_STRUCTURE_LIVRET::DB_supprimer_eleve_modaccomp( $modaccomp_id );
  // on s’arrête là
  Json::end( TRUE );
}

//
// Modifier les périodes pour un élève
//

if($action=='modifier_periodes')
{
  // vérifs
  if( !$modaccomp_id || empty($tab_periode) || !$periodicite )
  {
    Json::end( FALSE , 'Erreur avec les données transmises !' );
  }
  // go
  for( $periode_num = 1 ; $periode_num <= $periodicite ; $periode_num++ )
  {
    $periode_livret = $periodicite*10 + $periode_num;
    $is_actif = in_array($periode_livret,$tab_periode) ? TRUE : FALSE ;
    DB_STRUCTURE_LIVRET::DB_modifier_eleve_modaccomp_periode( $modaccomp_id , $periode_livret , $is_actif );
  }
  // on s’arrête là
  $user_periodes = implode(',',$tab_periode);
  Json::end( TRUE , '<td data-periodicite="'.$periodicite.'" data-periodes="['.$user_periodes.']">'.str_replace( $tab_bad , $tab_bon , $user_periodes).'</td>' );
}

//
// Modifier un commentaire
//

if($action=='modifier_commentaire')
{
  // vérifs
  if( !$modaccomp_id )
  {
    Json::end( FALSE , 'Erreur avec les données transmises !' );
  }
  if( !$commentaire )
  {
    Json::end( FALSE , 'Descriptif du '.$modaccomp_code.' manquant !' );
  }
  // go
  DB_STRUCTURE_LIVRET::DB_modifier_eleve_modaccomp_complement( $modaccomp_id , $commentaire );
  // on s’arrête là
  Json::add_row( 'html', html(Outil::afficher_texte_tronque($commentaire,50)).' <q class="modifier"'.infobulle('Modifier ce commentaire.').'></q>' );
  Json::end( TRUE );
}

//
// Ajouter des associations (à placer ici dans le code car pas de point d’arrêt, rechargement du tableau)
//

if($action=='associer')
{
  // vérifs
  if( empty($tab_eleve) )
  {
    Json::end( FALSE , 'Aucun compte élève récupéré !' );
  }
  if( ( ($modaccomp_code=='PPRE') || ($modaccomp_code=='CTR') ) && !$commentaire )
  {
    Json::end( FALSE , 'Descriptif du PPRE manquant !' );
  }
  if( ($modaccomp_code!='PPRE') && ($modaccomp_code!='CTR') && $commentaire )
  {
    $commentaire = '';
  }
  if( !$modaccomp_code || !DB_STRUCTURE_LIVRET::DB_tester_modaccomp($modaccomp_code) )
  {
    Json::end( FALSE , 'Modalité non transmise ou inconnue ("'.html($modaccomp_code).'") !' );
  }
  // go
  $DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_periodes_eleves( implode(',',$tab_eleve) );
  foreach($DB_TAB as $DB_ROW)
  {
    if(empty($DB_ROW['listing_periodes']))
    {
      Json::end( FALSE , 'Élève(s) dans une classe sans association de période du livret scolaire !' );
    }
    list( $modaccomp_id , $nb_modif_lignes ) = DB_STRUCTURE_LIVRET::DB_ajouter_eleve_modaccomp( $DB_ROW['user_id'] , $modaccomp_code , $commentaire );
    if( $nb_modif_lignes ) // vaut 1 si l’INSERT est passé ou 0 si ON DUPLICATE KEY UPDATE appliqué
    {
      $tab_periodes = explode(',',$DB_ROW['listing_periodes']);
      foreach($tab_periodes as $periode_livret)
      {
        DB_STRUCTURE_LIVRET::DB_modifier_eleve_modaccomp_periode( $modaccomp_id , $periode_livret , TRUE );
      }
    }
  }
}

//
// Affichage du bilan des affectations des dispositifs aux élèves
//

if($only_groupes_id)
{
  $tab_id = explode(',',$only_groupes_id);
  $tab_id = Clean::map('entier',$tab_id);
  $tab_id = array_filter($tab_id,'positif');
  $only_groupes_id = implode(',',$tab_id);
}

$DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_eleve_modaccomp_gestion( $only_groupes_id );
if(empty($DB_TAB))
{
  Json::end( TRUE  , '<tr class="vide"><td class="nu" colspan="5"></td><td class="nu"></td></tr>' );
}
$tab_js_commentaire = array();
foreach($DB_TAB as $DB_ROW)
{
  if($DB_ROW['info_complement'])
  {
    $info_complement = html(Outil::afficher_texte_tronque($DB_ROW['info_complement'],50)).' <q class="modifier"'.infobulle('Modifier ce commentaire.').'></q>';
    $tab_js_commentaire[$DB_ROW['livret_modaccomp_id']] = html($DB_ROW['info_complement']);
  }
  else
  {
    $info_complement = '-' ;
  }
  $user_periodicite = $DB_ROW['listing_periodes'][0];
  $user_periodes    = $DB_ROW['listing_periodes'];
  Json::add_row( 'html' , '<tr id="id_'.$DB_ROW['livret_modaccomp_id'].'">');
  Json::add_row( 'html' ,   '<td>'.html($DB_ROW['groupe_nom']).'</td>');
  Json::add_row( 'html' ,   '<td data-user="'.$DB_ROW['user_id'].'">'.html($DB_ROW['user_nom'].' '.$DB_ROW['user_prenom']).'</td>');
  Json::add_row( 'html' ,   '<td>'.$DB_ROW['livret_modaccomp_code'].'</td>');
  Json::add_row( 'html' ,   '<td data-periodicite="'.$user_periodicite.'" data-periodes="['.$user_periodes.']">'.str_replace( $tab_bad , $tab_bon , $user_periodes).'</td>');
  Json::add_row( 'html' ,   '<td>'.$info_complement.'</td>');
  Json::add_row( 'html' ,   '<td class="nu">');
  Json::add_row( 'html' ,     '<q class="date_periode"'.infobulle('Modifier les périodes concernées.').'></q>');
  Json::add_row( 'html' ,     '<q class="supprimer"'.infobulle('Retirer ce dispositif (aucune confirmation ne sera demandée).').'></q>');
  Json::add_row( 'html' ,   '</td>');
  Json::add_row( 'html' , '</tr>');
}
Json::add_row( 'tab_commentaire', json_encode($tab_js_commentaire) );
Json::end( TRUE );
?>
