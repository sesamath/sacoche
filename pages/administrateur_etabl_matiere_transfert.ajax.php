<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action     = Clean::post('f_action'   , 'texte');
$id_avant   = Clean::post('f_id_avant' , 'entier');
$id_apres   = Clean::post('f_id_apres' , 'entier');
$nom_avant  = Clean::post('f_nom_avant', 'texte');
$nom_apres  = Clean::post('f_nom_apres', 'texte');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Déplacer les référentiels d’une matière vers une autre
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='deplacer_referentiels') && $id_avant && $id_apres && ($id_avant!=$id_apres) && $nom_avant && $nom_apres )
{
  // Déplacement après vérification que c’est possible (matière de destination vierge de données)
  // 
  $is_ok = DB_STRUCTURE_MATIERE::DB_deplacer_referentiel_matiere($id_avant,$id_apres);
  if(!$is_ok)
  {
    Json::end( FALSE , 'La nouvelle matière contient déjà des données !' );
  }
  // Retirer l’ancienne matière partagée || Supprimer l’ancienne matière spécifique existante
  if($id_avant>ID_MATIERE_PARTAGEE_MAX)
  {
    DB_STRUCTURE_MATIERE::DB_supprimer_matiere_specifique($id_avant);
    $log_fin = 'avec suppression de la matière spécifique "'.$nom_avant.'"';
  }
  else
  {
    DB_STRUCTURE_MATIERE::modifier_matiere_partagee_activation($id_avant,0);
    $log_fin = 'avec retrait de la matière partagée "'.$nom_avant.'"';
  }
  // Log de l’action
  SACocheLog::ajouter('Déplacement des référentiels de "'.$nom_avant.'" ('.$id_avant.') vers "'.$nom_apres.'" ('.$id_apres.'), '.$log_fin.'.');
  // Notifications (rendues visibles ultérieurement)
  $notification_contenu = date('d-m-Y H:i:s').' '.$_SESSION['USER_PRENOM'].' '.$_SESSION['USER_NOM'].' a déplacé des référentiels de "'.$nom_avant.'" ('.$id_avant.') vers "'.$nom_apres.'" ('.$id_apres.'), '.$log_fin.'.'."\r\n";
  DB_STRUCTURE_NOTIFICATION::enregistrer_action_admin( $notification_contenu , $_SESSION['USER_ID'] );
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );
?>
