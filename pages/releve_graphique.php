<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Synthèse graphique'));

if( ($_SESSION['USER_PROFIL_TYPE']=='parent') && (!$_SESSION['NB_ENFANTS']) )
{
  echo'<p class="danger">'.$_SESSION['OPT_PARENT_ENFANTS'].'</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}

if(in_array($_SESSION['USER_PROFIL_TYPE'],array('parent','eleve')))
{
  if( !Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_ETAT_ACQUISITION']) )
  {
    echo'<p class="danger">'.html(Lang::_('Vous n’êtes pas habilité à accéder à cette fonctionnalité !')).'</p>'.NL;
    echo'<div class="astuce">En effet, les administrateurs n’ont pas autorisé que vous accédiez aux états d’acquisitions&hellip;</div>'.NL;
    return; // Ne pas exécuter la suite de ce fichier inclus.
  }
}

// L’élève ne choisit évidemment pas sa classe ni son nom, mais on construit qd même les formulaires, on les remplit et on les cache (permet un code unique et une transmission des infos en ajax comme pour les autres profils).
Form::load_choix_memo();
$check_aucune_courbe      = (Form::$tab_options['indicateur']=='aucun')              ? ' checked' : '' ;
$check_moyenne_scores     = (Form::$tab_options['indicateur']=='moyenne_scores')     ? ' checked' : '' ;
$check_pourcentage_acquis = (Form::$tab_options['indicateur']=='pourcentage_acquis') ? ' checked' : '' ;
$check_conversion_sur_20  = (Form::$tab_options['conversion_sur_20'])                ? ' checked' : '' ;
$class_conversion_sur_20  = ($check_moyenne_scores || $check_pourcentage_acquis)   ? 'show' : 'hide' ;
if(in_array($_SESSION['USER_PROFIL_TYPE'],array('parent','eleve')))
{
  // Une éventuelle restriction d’accès doit surcharger toute mémorisation antérieure de formulaire
  $check_aucune_courbe      = ( Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_MOYENNE_SCORE']) || Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_MOYENNE_SCORE']) ) ? $check_aucune_courbe : ' checked' ;
  $check_moyenne_scores     = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_MOYENNE_SCORE'])      ? $check_moyenne_scores     : '' ;
  $check_pourcentage_acquis = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_POURCENTAGE_ACQUIS']) ? $check_pourcentage_acquis : '' ;
  $check_conversion_sur_20  = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_CONVERSION_SUR_20'])  ? $check_conversion_sur_20  : '' ;
  $class_conversion_sur_20  = ($check_moyenne_scores || $check_pourcentage_acquis)                     ? 'show' : 'hide' ;
  $moyenne_scores     = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_MOYENNE_SCORE'])      ? '<label for="f_indicateur_MS"><input type="radio" id="f_indicateur_MS" name="f_indicateur" value="moyenne_scores"'.$check_moyenne_scores.'> Moyenne des scores</label>'                                                     : '<del>Moyenne des scores</del>' ;
  $pourcentage_acquis = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_POURCENTAGE_ACQUIS']) ? '<label for="f_indicateur_PA"><input type="radio" id="f_indicateur_PA" name="f_indicateur" value="pourcentage_acquis"'.$check_pourcentage_acquis.'> Pourcentage d’items acquis</label>'                                    : '<del>Pourcentage d’items acquis</del>' ;
  $conversion_sur_20  = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_CONVERSION_SUR_20'])  ? '<label for="f_conversion_sur_20" class="'.$class_conversion_sur_20.'"><input type="checkbox" id="f_conversion_sur_20" name="f_conversion_sur_20" value="1"'.$check_conversion_sur_20.'> Conversion en note sur 20</label>' : '<del>Conversion en note sur 20</del>' ;
}
else
{
  $moyenne_scores     = '<label for="f_indicateur_MS"><input type="radio" id="f_indicateur_MS" name="f_indicateur" value="moyenne_scores"'.$check_moyenne_scores.'> Moyenne des scores</label>';
  $pourcentage_acquis = '<label for="f_indicateur_PA"><input type="radio" id="f_indicateur_PA" name="f_indicateur" value="pourcentage_acquis"'.$check_pourcentage_acquis.'> Pourcentage d’items acquis</label>';
  $conversion_sur_20  = '<label for="f_conversion_sur_20" class="'.$class_conversion_sur_20.'"><input type="checkbox" id="f_conversion_sur_20" name="f_conversion_sur_20" value="1"'.$check_conversion_sur_20.'> Conversion en note sur 20</label>';
}
$aucune_courbe = '<label for="f_indicateur_aucun"><input type="radio" id="f_indicateur_aucun" name="f_indicateur" value="aucun"'.$check_aucune_courbe.'> Aucun</label>';

$check_retroactif_auto     = (Form::$tab_options['retroactif']=='auto')   ? ' checked' : '' ;
$check_retroactif_non      = (Form::$tab_options['retroactif']=='non')    ? ' checked' : '' ;
$check_retroactif_oui      = (Form::$tab_options['retroactif']=='oui')    ? ' checked' : '' ;
$check_retroactif_annuel   = (Form::$tab_options['retroactif']=='annuel') ? ' checked' : '' ;
$check_aff_prop_sans_score = (Form::$tab_options['aff_prop_sans_score'])  ? ' checked' : '' ;
$check_only_prof           = (Form::$tab_options['only_prof'])            ? ' checked' : '' ;
$check_only_socle          = (Form::$tab_options['only_socle'])           ? ' checked' : '' ;

$bouton_modifier_profs    = '';

if($_SESSION['USER_PROFIL_TYPE']=='directeur')
{
  $objet_selection = '';
  $tab_groupes  = DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl();
  $tab_profs    = 'Choisir d’abord un groupe ci-dessous...'; // maj en ajax suivant le choix du groupe
  $of_groupe  = '';
  $sel_groupe = FALSE;
  $class_form_eleve   = 'show';
  $class_form_periode = 'hide';
  $class_report_eleve = 'hide';
  $class_navig_eleve  = 'show';
  $class_only_prof    = 'hide';
  $class_info_prof    = 'show';
  $select_eleve = '<option>&nbsp;</option>'; // maj en ajax suivant le choix du groupe
}
if($_SESSION['USER_PROFIL_TYPE']=='professeur')
{
  $objet_selection = '';
  $tab_groupes  = ($_SESSION['USER_JOIN_GROUPES']=='config') ? DB_STRUCTURE_COMMUN::DB_OPT_groupes_professeur($_SESSION['USER_ID']) : DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl() ;
  $tab_profs    = array(0=>array('valeur'=>$_SESSION['USER_ID'],'texte'=>To::texte_genre_identite($_SESSION['USER_NOM'],FALSE,$_SESSION['USER_PRENOM'],TRUE,$_SESSION['USER_GENRE'])));
  $of_groupe  = '';
  $sel_groupe = FALSE;
  $class_form_eleve   = 'show';
  $class_form_periode = 'hide';
  $class_report_eleve = 'hide';
  $class_navig_eleve  = 'show';
  $class_only_prof    = 'hide';
  $class_info_prof    = 'show';
  $select_eleve = '<option>&nbsp;</option>'; // maj en ajax suivant le choix du groupe
  $bouton_modifier_profs = '<button id="ajouter_prof" type="button" class="form_ajouter"'.infobulle('Lister les enseignants du regroupement').'>&plusmn;</button><button id="retirer_prof" type="button" class="form_retirer hide"'.infobulle('Revenir à moi seul').'>&plusmn;</button>';
}
if( ($_SESSION['USER_PROFIL_TYPE']=='parent') && ($_SESSION['NB_ENFANTS']>1) )
{
  $objet_selection = ' disabled';
  $tab_groupes  = $_SESSION['OPT_PARENT_CLASSES'];
  $tab_profs    = 'Choisir d’abord un groupe ci-dessous...'; // maj en ajax suivant le choix du groupe
  $of_groupe  = '';
  $sel_groupe = FALSE;
  $class_form_eleve   = 'show';
  $class_form_periode = 'hide';
  $class_report_eleve = 'show';
  $class_navig_eleve  = 'hide';
  $class_only_prof    = 'hide';
  $class_info_prof    = 'show';
  $select_eleve = '<option>&nbsp;</option>'; // maj en ajax suivant le choix du groupe
}
if( ($_SESSION['USER_PROFIL_TYPE']=='parent') && ($_SESSION['NB_ENFANTS']==1) )
{
  $objet_selection = ' disabled';
  $tab_groupes  = array(0=>array('valeur'=>$_SESSION['ELEVE_CLASSE_ID'],'texte'=>$_SESSION['ELEVE_CLASSE_NOM'],'optgroup'=>'classe'));
  $tab_profs    = DB_STRUCTURE_COMMUN::DB_OPT_profs_groupe('classe',$_SESSION['ELEVE_CLASSE_ID']);
  $of_groupe  = FALSE;
  $sel_groupe = TRUE;
  $class_form_eleve   = 'hide';
  $class_form_periode = 'show';
  $class_report_eleve = 'show';
  $class_navig_eleve  = 'hide';
  $class_only_prof    = 'show';
  $class_info_prof    = 'hide';
  $select_eleve = '<option value="'.$_SESSION['OPT_PARENT_ENFANTS'][0]['valeur'].'" selected>'.html($_SESSION['OPT_PARENT_ENFANTS'][0]['texte']).'</option>';
}
if($_SESSION['USER_PROFIL_TYPE']=='eleve')
{
  $objet_selection = ' disabled';
  $tab_groupes  = array(0=>array('valeur'=>$_SESSION['ELEVE_CLASSE_ID'],'texte'=>$_SESSION['ELEVE_CLASSE_NOM'],'optgroup'=>'classe'));
  $tab_profs    = DB_STRUCTURE_COMMUN::DB_OPT_profs_groupe('classe',$_SESSION['ELEVE_CLASSE_ID']);
  $of_groupe  = FALSE;
  $sel_groupe = TRUE; 
  $class_form_eleve   = 'hide'; 
  $class_form_periode = 'show';
  $class_report_eleve = 'show';
  $class_navig_eleve  = 'hide';
  $class_only_prof    = 'show';
  $class_info_prof    = 'hide';
  $select_eleve = '<option value="'.$_SESSION['USER_ID'].'" selected>'.html($_SESSION['USER_NOM'].' '.$_SESSION['USER_PRENOM']).'</option>';
}

$tab_eleves_ordre = ($_SESSION['USER_PROFIL_TYPE']!='professeur') ? Form::$tab_select_eleves_ordre : array_merge( Form::$tab_select_eleves_ordre , DB_STRUCTURE_PROFESSEUR_PLAN::DB_OPT_lister_plans_prof_groupe( $_SESSION['USER_ID'] ) ) ;

$tab_periodes = DB_STRUCTURE_COMMUN::DB_OPT_periodes_etabl();

$select_groupe          = HtmlForm::afficher_select($tab_groupes                      , 'f_groupe'          /*select_nom*/ ,              $of_groupe /*option_first*/ , $sel_groupe                           /*selection*/ , 'regroupements' /*optgroup*/ );
$select_eleves_ordre    = HtmlForm::afficher_select($tab_eleves_ordre                 , 'f_eleves_ordre'    /*select_nom*/ ,                   FALSE /*option_first*/ , Form::$tab_options['eleves_ordre']    /*selection*/ ,  'eleves_ordre' /*optgroup*/ );
$select_professeur      = HtmlForm::afficher_select($tab_profs                        , 'f_prof'            /*select_nom*/ ,                   FALSE /*option_first*/ , Form::$tab_options['prof_id']         /*selection*/ ,              '' /*optgroup*/ );
$select_periode         = HtmlForm::afficher_select($tab_periodes                     , 'f_periode'         /*select_nom*/ , 'periode_personnalisee' /*option_first*/ , FALSE                                 /*selection*/ ,              '' /*optgroup*/ );
$select_only_diagnostic = HtmlForm::afficher_select(Form::$tab_select_only_diagnostic , 'f_only_diagnostic' /*select_nom*/ ,                   FALSE /*option_first*/ , Form::$tab_options['only_diagnostic'] /*selection*/ ,              '' /*optgroup*/ );

// Pour variable js BACKGROUND_COLORS
$tab_css_couleurs = array();
$key_couleur = $_SESSION['USER_DALTONISME'] ? 'GRIS' : 'COULEUR' ;
foreach( $_SESSION['ACQUIS'] as $acquis_id => $tab_acquis_info )
{
  $tab_css_couleurs[$acquis_id] = '"'.$tab_acquis_info[$key_couleur].'"';
}
// Si demande d’indiquer aussi la proportion d’items évalués sans score
$tab_css_couleurs[0] = '"#ffffff"';

// Javascript
Layout::add( 'js_inline_before' , 'window.BACKGROUND_COLORS = ['.implode(',',$tab_css_couleurs).'];' );
Layout::add( 'js_inline_before' , 'window.TODAY_SQL         = "'.TODAY_SQL.'";' );
Layout::add( 'js_inline_before' , 'window.USER_ID           = '.$_SESSION['USER_ID'].';' );
Layout::add( 'js_inline_before' , 'window.USER_TEXTE        = "'.html(To::texte_genre_identite($_SESSION['USER_NOM'],FALSE,$_SESSION['USER_PRENOM'],TRUE,$_SESSION['USER_GENRE'])).'";' );
Layout::add( 'js_inline_before' , 'window.USER_PROFIL_TYPE  = "'.$_SESSION['USER_PROFIL_TYPE'].'";' );
// Fabrication du tableau javascript "tab_groupe_periode" pour les jointures groupes/périodes
HtmlForm::fabriquer_tab_js_jointure_groupe( $tab_groupes , TRUE /*tab_groupe_periode*/ , FALSE /*tab_groupe_niveau*/ );
?>

<div id="zone_preliminaire">
  <div><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=releves_bilans__synthese_graphique">DOC : Synthèse graphique.</a></span></div>
  <hr>
</div>

<div id="cadre_photo"><button id="voir_photo" type="button" class="voir_photo">Photo</button></div>

<form action="#" method="post" id="form_select"><fieldset>

  <p>
    <label class="tab">Indicateur :</label><?php echo $aucune_courbe.'&nbsp;&nbsp;&nbsp;'.$moyenne_scores.'&nbsp;&nbsp;&nbsp;'.$pourcentage_acquis.'&nbsp;&nbsp;&nbsp;'.$conversion_sur_20 ?>
  </p>
  <p class="<?php echo $class_form_eleve ?>">
    <label class="tab" for="f_groupe">Classe / groupe :</label><?php echo $select_groupe ?><input type="hidden" id="f_groupe_type" name="f_groupe_type" value=""> <span id="bloc_ordre" class="hide"><?php echo $select_eleves_ordre ?></span><label id="ajax_maj">&nbsp;</label><br>
    <label class="tab" for="f_eleve">Élève :</label><select id="f_eleve" name="f_eleve"><?php echo $select_eleve ?></select><input type="hidden" id="f_nom_prenom" name="f_nom_prenom" value="">
  </p>
  <p id="zone_periodes" class="<?php echo $class_form_periode ?>">
    <label class="tab" for="f_periode"><?php echo infobulle('Les items pris en compte sont ceux qui sont évalués'.BRJS.'au moins une fois sur cette période.',TRUE) ?> Période :</label><?php echo $select_periode ?>
    <span id="dates_perso" class="show">
      du <input id="f_date_debut" name="f_date_debut" size="9" type="text" value="<?php echo To::jour_debut_annee_scolaire('fr') ?>"><q class="date_calendrier"<?php echo infobulle('Cliquer sur cette image pour importer une date depuis un calendrier !') ?>></q>
      au <input id="f_date_fin" name="f_date_fin" size="9" type="text" value="<?php echo TODAY_FR ?>"><q class="date_calendrier"<?php echo infobulle('Cliquer sur cette image pour importer une date depuis un calendrier !') ?>></q>
    </span><br>
    <span class="radio"><?php echo infobulle('Le bilan peut être établi uniquement sur la période considérée'.BRJS.'ou en tenant compte d’évaluations antérieures des items concernés.'.BRJS.'En automatique, les paramètres enregistrés pour chaque référentiel s’appliquent.',TRUE) ?> Prise en compte des évaluations antérieures :</span>
      <label for="f_retroactif_auto"><input type="radio" id="f_retroactif_auto" name="f_retroactif" value="auto"<?php echo $check_retroactif_auto ?>> automatique (selon référentiels)</label>&nbsp;&nbsp;&nbsp;
      <label for="f_retroactif_non"><input type="radio" id="f_retroactif_non" name="f_retroactif" value="non"<?php echo $check_retroactif_non ?>> non</label>&nbsp;&nbsp;&nbsp;
      <label for="f_retroactif_oui"><input type="radio" id="f_retroactif_oui" name="f_retroactif" value="oui"<?php echo $check_retroactif_oui ?>> oui (sans limite)</label>&nbsp;&nbsp;&nbsp;
      <label for="f_retroactif_annuel"><input type="radio" id="f_retroactif_annuel" name="f_retroactif" value="annuel"<?php echo $check_retroactif_annuel ?>> de l’année scolaire</label>
  </p>
  <div class="toggle">
    <span class="tab"></span><a href="#" class="puce_plus toggle">Afficher plus d’options</a>
  </div>
  <div class="toggle hide">
    <span class="tab"></span><a href="#" class="puce_moins toggle">Afficher moins d’options</a><br>
    <label class="tab">Codes neutres :</label><label for="f_prop_sans_score"><input type="checkbox" id="f_prop_sans_score" name="f_prop_sans_score" value="1"<?php echo $check_aff_prop_sans_score ?>> Indiquer la proportion des items évalués sans score</label><br>
    <label class="tab">Restrictions :</label><?php echo $select_only_diagnostic ?><br>
    <span class="tab"></span><label for="f_only_prof"><input type="checkbox" id="f_only_prof" name="f_only_prof" value="1"<?php echo $check_only_prof ?>> Uniquement les évaluations d’un enseignant </label><span id="zone_profs" class="<?php echo $class_only_prof ?>">: <?php echo $select_professeur ?><?php echo $bouton_modifier_profs ?><input type="hidden" id="f_prof_texte" name="f_prof_texte" value=""></span><span id="info_profs" class="<?php echo $class_info_prof ?>">&rarr; <span class="astuce">sélectionner un regroupement pour pouvoir choisir l’enseignant</span></span><br>
    <span class="tab"></span><label for="f_only_socle"><input type="checkbox" id="f_only_socle" name="f_only_socle" value="1"<?php echo $check_only_socle ?>> Uniquement les items liés au socle</label>
  </div>
  <p><span class="tab"></span><button id="bouton_valider" type="submit" class="generer">Générer.</button><label id="ajax_msg">&nbsp;</label></p>
</fieldset></form>

<div id="bilan" class="hide">
  <h3 id="report_titre">Contenu dynamique</h3>
  <h3 id="report_eleve" class="<?php echo $class_report_eleve ?>">Contenu dynamique</h3>
  <div><span id="navigation_eleve" class="<?php echo $class_navig_eleve ?>"><button class="go_premier" type="button" id="go_premier_eleve">Premier</button> <button class="go_precedent" type="button" id="go_precedent_eleve">Précédent</button> <select class="b" name="go_selection" id="go_selection_eleve"><option>&nbsp;</option></select> <button class="go_suivant" type="button" id="go_suivant_eleve">Suivant</button> <button class="go_dernier" type="button" id="go_dernier_eleve">Dernier</button></span>&nbsp;&nbsp;&nbsp;<button class="retourner" type="button" id="fermer_zone_bilan">Retour</button></div>
</div>
<div id="div_graphique_releve" class="hide"></div>

