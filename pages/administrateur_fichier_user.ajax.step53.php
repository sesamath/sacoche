<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if(!isset($STEP))       {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Étape 53 - Crypter les mots de passe des nouveaux utilisateurs et les enregistrer dans la base
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$etape_numero = Clean::post('f_etape_numero', 'entier');
$etape_nombre = Clean::post('f_etape_nombre', 'entier');

if( is_null($etape_numero) || is_null($etape_nombre) )
{
  Json::end( FALSE , 'Erreur avec les données transmises !' );
}

if( $etape_numero < $etape_nombre )
{
  $tab_user = FileSystem::recuperer_fichier_infos_serializees(CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'tab_user.txt');
  $tab_pass = FileSystem::recuperer_fichier_infos_serializees(CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'tab_pass.txt');
  $nombre_iterations_par_etape = Outil::$crypt_nb_iterations_script;
  $indice_min = ($etape_numero-1)*$nombre_iterations_par_etape;
  $indice_max = min( $etape_numero*$nombre_iterations_par_etape , count($tab_user) );
  for( $indice = $indice_min ; $indice < $indice_max ; $indice++ )
  {
    DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $tab_user[$indice] , array( ':password' => Outil::crypter_mdp($tab_pass[$indice]) ) );
  }
}
else
{
  $tab_infos = FileSystem::recuperer_fichier_infos_serializees(CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'tab_infos.txt');
  $champ = ($import_profil=='eleve') ? 'Classe' : 'Profil' ;
  Json::add_str($tab_infos['phrase_bilan'].NL);
  if($mode=='complet')
  {
    Json::add_str('<table>'.NL);
    Json::add_str(  '<thead>'.NL);
    Json::add_str(    '<tr><th>Id Sconet</th><th>N° Sconet</th><th>Référence</th><th>'.$champ.'</th><th>Nom</th><th>Prénom</th><th>Login</th><th>Mot de passe</th><th>Entrée</th><th>Sortie</th></tr>'.NL);
    Json::add_str(  '</thead>'.NL);
    Json::add_str(  '<tbody>'.NL);
    Json::add_str(    $tab_infos['tr_contenu']);
    Json::add_str(  '</tbody>'.NL);
    Json::add_str('</table>'.NL);
  }
  if( $etape_nombre > 1 )
  {
    $_SESSION['tmp_ajout_user'] = TRUE ;
    Json::add_str('<ul class="puce p"><li><a href="#" class="step54">Récupérer les identifiants de tout nouvel utilisateur inscrit.</a><input id="archive" name="archive" type="hidden" value="'.$tab_infos['fichier_nom'].'"><label id="ajax_msg">&nbsp;</label></li></ul>'.NL);
  }
  else
  {
    $_SESSION['tmp_ajout_user'] = FALSE ;
    Json::add_str('<p class="astuce">Il n’y a aucun nouvel utilisateur inscrit, donc pas d’identifiants à récupérer.</p>'.NL);
    switch($import_origine.'+'.$import_profil)
    {
      case 'siecle+eleve'       : $etape = 6; $STEP = 61; break;
      case 'siecle+professeur'  : $etape = 6; $STEP = 61; break;
      case 'tableur+eleve'      : $etape = 6; $STEP = 61; break;
      case 'tableur+professeur' : $etape = 6; $STEP = 61; break;
      case 'siecle+parent'      : $etape = 4; $STEP = 71; break;
      case 'tableur+parent'     : $etape = 4; $STEP = 71; break;
      case 'onde+parent'        : $etape = 4; $STEP = 71; break;
      case 'factos+parent'      : $etape = 4; $STEP = 71; break;
      case 'onde+eleve'         : $etape = 5; $STEP = 90; break;
      case 'factos+eleve'       : $etape = 5; $STEP = 90; break;
    }
    Json::add_str('<ul class="puce p"><li><a href="#step'.$STEP.'" id="passer_etape_suivante">Passer à l’étape '.$etape.'.</a><label id="ajax_msg">&nbsp;</label></li></ul>'.NL);
  }
  FileSystem::supprimer_fichier( CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'tab_infos.txt' , FALSE /*verif_exist*/ );
  FileSystem::supprimer_fichier( CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'tab_user.txt'  , FALSE /*verif_exist*/ );
  FileSystem::supprimer_fichier( CHEMIN_DOSSIER_IMPORT.$fichier_nom_debut.'tab_pass.txt'  , FALSE /*verif_exist*/ );
}

?>
