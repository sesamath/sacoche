<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {}

$action      = Clean::post('f_action'     , 'texte');
$groupe_type = Clean::post('f_groupe_type', 'lettres'); // d n c g b
$groupe_id   = Clean::post('f_groupe_id'  , 'entier');
$groupe_nom  = Clean::post('f_groupe_nom' , 'texte');

$tab_groupe_multiple = Clean::post('f_groupe_multiple', array('array',','));
$tab_groupe_multiple = array_fill_keys( array_filter( Clean::map('entier',$tab_groupe_multiple) , 'positif' ) , TRUE );

$tab_types   = array('d'=>'all' , 'n'=>'niveau' , 'c'=>'classe' , 'g'=>'groupe' , 'b'=>'besoin');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction commune
// ////////////////////////////////////////////////////////////////////////////////////////////////////

function recuperer_photos($DB_TAB)
{
  $tab_vignettes = array();
  $img_height = PHOTO_DIMENSION_MAXI;
  $img_width  = PHOTO_DIMENSION_MAXI*2/3;
  foreach($DB_TAB as $DB_ROW)
  {
    $tab_vignettes[$DB_ROW['user_id']] = array(
      'user_nom'    => $DB_ROW['user_nom'],
      'user_prenom' => $DB_ROW['user_prenom'],
      'img_width'   => $img_width,
      'img_height'  => $img_height,
      'img_src'     => '',
      'img_title'   => TRUE,
    );
  }
  $listing_user_id = implode(',',array_keys($tab_vignettes));
  $DB_TAB = DB_STRUCTURE_IMAGE::DB_lister_images( $listing_user_id , 'photo' );
  if(empty($DB_TAB))
  {
    return FALSE;
  }
  foreach($DB_TAB as $DB_ROW)
  {
    $tab_vignettes[$DB_ROW['user_id']]['img_width']  = $DB_ROW['image_largeur'];
    $tab_vignettes[$DB_ROW['user_id']]['img_height'] = $DB_ROW['image_hauteur'];
    $tab_vignettes[$DB_ROW['user_id']]['img_src']    = $DB_ROW['image_contenu'];
    $tab_vignettes[$DB_ROW['user_id']]['img_title']  = FALSE;
  }
  return $tab_vignettes;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Afficher les élèves et leurs photos si existantes pour un regroupement donné
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( $action == 'generer_groupe' )
{
  if( !$groupe_id || !$groupe_nom || !isset($tab_types[$groupe_type]) )
  {
    Json::end( FALSE , 'Erreur avec les données transmises !' );
  }
  // On récupère les élèves
  $DB_TAB = DB_STRUCTURE_COMMUN::DB_lister_users_regroupement( 'eleve' /*profil_type*/ , 1 /*statut*/ , $tab_types[$groupe_type] , $groupe_id , 'nom' /*eleves_ordre*/ );
  if(empty($DB_TAB))
  {
    Json::end( FALSE , 'Aucun élève trouvé dans ce regroupement.' );
  }
  // On récupère les photos
  $tab_vignettes = recuperer_photos($DB_TAB);
  if(!$tab_vignettes)
  {
    Json::end( FALSE , 'Aucune photo disponible pour ces élèves !' );
  }
  // Génération de la sortie HTML (affichée directement) et de la sortie PDF (enregistrée dans un fichier)
  $fnom_pdf = 'trombinoscope_'.$_SESSION['BASE'].'_'.Clean::fichier($groupe_nom).'_'.FileSystem::generer_fin_nom_fichier__date_et_alea().'.pdf';
  $lien_telechargement = '<a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.$fnom_pdf.'"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a> &rarr; <span class="noprint">Afin de préserver l’environnement, n’imprimer que si nécessaire !</span>';
  $pdf = new PDF_trombinoscope( FALSE /*officiel*/ , 'A4' /*page_size*/ , 'portrait' /*orientation*/ , 5 /*marge_gauche*/ , 5 /*marge_droite*/ , 5 /*marge_haut*/ , 7 /*marge_bas*/ );
  $pdf->initialiser($groupe_nom);
  // On passe les élèves en revue (on a toutes les infos déjà disponibles)
  $affichage = '';
  $tab_js_eleve_image = array();
  $tab_js_eleve_texte = array();
  foreach($tab_vignettes as $user_id => $tab)
  {
    $pdf->vignette($tab);
    $img_src   = ($tab['img_src'])   ? ' src="data:'.image_type_to_mime_type(IMAGETYPE_JPEG).';base64,'.$tab['img_src'].'"' : ' src="./_img/trombinoscope_vide.png"' ;
    $img_title = ($tab['img_title']) ? infobulle('absence de photo') : '' ;
    $affichage .= '<div id="div_'.$user_id.'" class="photo"><img width="'.$tab['img_width'].'" height="'.$tab['img_height'].'" alt=""'.$img_src.$img_title.'><br>'.html($tab['user_nom']).'<br>'.html($tab['user_prenom']).'</div>';
    if($tab['img_src'])
    {
      $tab_js_eleve_image[] = 'data:'.image_type_to_mime_type(IMAGETYPE_JPEG).';base64,'.$tab['img_src'];
      $tab_js_eleve_texte[] = html($tab['user_prenom'].' '.$tab['user_nom']);
    }
  }
  // Enregistrement du PDF
  FileSystem::ecrire_objet_pdf( CHEMIN_DOSSIER_EXPORT.$fnom_pdf , $pdf );
  // Affichage du HTML
  Json::end( TRUE , array( 'lien_telechargement'=>$lien_telechargement , 'affichage'=>$affichage , 'tab_eleve_image'=>json_encode($tab_js_eleve_image) , 'tab_eleve_texte'=>json_encode($tab_js_eleve_texte) ) );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Générer un PDF pour plusieurs regroupements d’un coup
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( $action == 'generer_multiple' )
{
  if( empty($tab_groupe_multiple) )
  {
    Json::end( FALSE , 'Erreur avec les données transmises !' );
  }
  // On va à la fois récupérer les types et noms de groupes, mais aussi vérifier que les groupes transmis sont accessibles à l'utilisateur
  $tab_groupes = ($_SESSION['USER_JOIN_GROUPES']=='config') ? DB_STRUCTURE_COMMUN::DB_OPT_groupes_professeur($_SESSION['USER_ID']) : DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl() ;
  foreach( $tab_groupes as $tab_option )
  {
    if( isset($tab_groupe_multiple[$tab_option['valeur']]) )
    {
      $tab_groupe_multiple[$tab_option['valeur']] = array(
        'nom'  => $tab_option['texte'],
        'type' => $tab_option['optgroup'],
      );
    }
  }
  // Ok, on continue
  $fnom_pdf = 'trombinoscope_'.$_SESSION['BASE'].'_multiregroupements_'.FileSystem::generer_fin_nom_fichier__date_et_alea().'.pdf';
  $lien_telechargement = '<a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.$fnom_pdf.'"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a> &rarr; <span class="noprint">Afin de préserver l’environnement, n’imprimer que si nécessaire !</span>';
  $pdf = new PDF_trombinoscope( FALSE /*officiel*/ , 'A4' /*page_size*/ , 'portrait' /*orientation*/ , 5 /*marge_gauche*/ , 5 /*marge_droite*/ , 5 /*marge_haut*/ , 7 /*marge_bas*/ );
  $test_presence_photo = FALSE;
  foreach($tab_groupe_multiple as $groupe_id => $tab_groupe)
  {
    if( is_array($tab_groupe) )
    {
      // On récupère les élèves
      $DB_TAB = DB_STRUCTURE_COMMUN::DB_lister_users_regroupement( 'eleve' /*profil_type*/ , 1 /*statut*/ , $tab_groupe['type'] , $groupe_id , 'nom' /*eleves_ordre*/ );
      if(!empty($DB_TAB))
      {
        // On récupère les photos
        $tab_vignettes = recuperer_photos($DB_TAB);
        if($tab_vignettes)
        {
          $test_presence_photo = TRUE;
          $pdf->initialiser($tab_groupe['nom']);
          // On passe les élèves en revue (on a toutes les infos déjà disponibles)
          foreach($tab_vignettes as $user_id => $tab_vignette)
          {
            $pdf->vignette($tab_vignette);
          }
        }
      }
    }
  }
  if(!$test_presence_photo)
  {
    Json::end( FALSE , 'Aucune photo disponible pour ces élèves !' );
  }
  // Enregistrement du PDF
  FileSystem::ecrire_objet_pdf( CHEMIN_DOSSIER_EXPORT.$fnom_pdf , $pdf );
  // Affichage du retour
  Json::end( TRUE , $lien_telechargement );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
