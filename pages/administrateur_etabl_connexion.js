/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function () {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Intercepter la touche entrée
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_mode input').keydown
    (
      function (e) {
        if (e.which == 13)  // touche entrée
        {
          $('#bouton_valider_mode').click();
        }
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Alerter sur la nécessité de valider
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_mode select , #form_mode input').change
    (
      function () {
        $('#ajax_msg_mode').attr('class', 'alerte').html('Penser à valider les modifications.');
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Afficher / masquer le formulaire CAS
// Afficher / masquer le formulaire GEPI
// Afficher / masquer le formulaire Domaine préfixe
// Afficher / masquer l’adresse de connexion directe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function actualiser_formulaire () {
      // on masque
      $('#cas_options ,#cas_domaine ,#cas_port , #cas_verif , #lien_direct , #info_inacheve , #info_hors_sesamath , #info_hors_actualite , #info_hors_ent , #info_heberg_acad , #info_conv_acad , #info_conv_etabl').hide();
      if (!window.IS_HEBERGEMENT_SESAMATH) {
        $('#info_hors_sesamath').show();
      } else if (!window.CONVENTION_ENT_REQUISE) {
        $('#info_hors_actualite').show();
      }
      // on récupère les infos
      var valeur = $('#connexion_mode_nom option:selected').val();
      var tab_infos = valeur.split('~');
      var connexion_mode = tab_infos[0];
      var connexion_ref = tab_infos[1];
      if (connexion_mode == 'cas') {
        var tab_infos = window.tab_param[connexion_mode][connexion_ref];
        var type_convention = tab_infos[0];
        var is_domaine_edit = tab_infos[1];
        var is_port_edit = tab_infos[2];
        var is_operationnel = tab_infos[3];
        var host_subdomain = tab_infos[4];
        var host_domain = tab_infos[5];
        var serveur_host = (host_subdomain != '') ? host_subdomain + '.' + host_domain : host_domain;
        var serveur_port = (tab_infos[6]) ? tab_infos[6] : 8443;
        $('#cas_serveur_host').val(serveur_host);
        $('#cas_serveur_port').val(tab_infos[6]);
        $('#cas_serveur_root').val(tab_infos[7]);
        $('#cas_serveur_url_login').val(tab_infos[8]);
        $('#cas_serveur_url_logout').val(tab_infos[9]);
        $('#cas_serveur_url_validate').val(tab_infos[10]);
        $('#serveur_host_subdomain').val(host_subdomain);
        $('#serveur_host_domain').val(host_domain);
        $('#serveur_port').val(serveur_port);
        if (window.IS_HEBERGEMENT_SESAMATH && window.CONVENTION_ENT_REQUISE) {
          $('#info_' + type_convention).show();
        }
        if (connexion_ref == '|perso') {
          $('#cas_options').show();
        }
        if (is_domaine_edit == 'oui') {
          $('#cas_domaine').show();
        }
        if (is_port_edit == 'oui') {
          $('#cas_port').show();
        }
        if (connexion_mode == 'cas') {
          $('#cas_verif').show();
        }
        if (is_operationnel == '1') {
          $('#bouton_valider_mode').prop('disabled', false);
          $('#lien_direct').show();
        } else {
          $('#bouton_valider_mode').prop('disabled', true);
          $('#info_inacheve').show();
        }
      } else if (connexion_mode == 'shibboleth') {
        if (window.IS_HEBERGEMENT_SESAMATH && window.CONVENTION_ENT_REQUISE) {
          $('#info_hors_ent').show();
        }
        var is_operationnel = window.tab_param[connexion_mode][connexion_ref];
        if (is_operationnel == '1') {
          $('#bouton_valider_mode').prop('disabled', false);
          $('#lien_direct').show();
        } else {
          $('#bouton_valider_mode').prop('disabled', true);
          $('#info_inacheve').show();
        }
      } else {
        if (window.IS_HEBERGEMENT_SESAMATH && window.CONVENTION_ENT_REQUISE) {
          $('#info_hors_ent').show();
        }
        $('#bouton_valider_mode').prop('disabled', false);
      }
    }

    $('#connexion_mode_nom').change
    (
      function () {
        actualiser_formulaire();
      }
    );

    // Initialisation au chargement de la page
    actualiser_formulaire();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Mode d’identification (normal, CAS...) & paramètres associés
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider_mode').click
    (
      function () {
        var connexion_mode_nom = $('#connexion_mode_nom option:selected').val();
        var tab_infos = connexion_mode_nom.split('~');
        var connexion_mode = tab_infos[0];
        var connexion_ref = tab_infos[1];
        $('#bouton_valider_mode').prop('disabled', true);
        $('#ajax_msg_mode').attr('class', 'loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type: 'POST',
            url: 'ajax.php?page=' + window.PAGE,
            data: 'csrf=' + window.CSRF + '&f_action=' + 'enregistrer_mode_identification' + '&f_connexion_mode=' + connexion_mode + '&f_connexion_ref=' + connexion_ref + '&' + $('#form_mode').serialize(),
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
              $('#bouton_valider_mode').prop('disabled', false);
              $('#ajax_msg_mode').attr('class', 'alerte').html(afficher_json_message_erreur(jqXHR, textStatus));
              return false;
            },
            success: function (responseJSON) {
              initialiser_compteur();
              $('#bouton_valider_mode').prop('disabled', false);
              if (responseJSON['statut'] == true) {
                $('#ajax_msg_mode').attr('class', 'valide').html('Mode de connexion enregistré !');
              } else {
                $('#ajax_msg_mode').attr('class', 'alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Afficher toutes les lignes du tableau
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_voir_tout').click
    (
      function () {
        $(this).parent().parent().remove();
        $('#table_action').children('tbody').find('tr').removeAttr('class');
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter une convention : mise en place du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_ajouter').click
    (
      function () {
        var connexion_mode_texte = $('#connexion_mode_nom option:selected').text();
        $('#f_connexion_texte').val(connexion_mode_texte);
        $('#ajax_msg_ajout').removeAttr('class').html('');
        $('#form_ajout label[generated=true]').removeAttr('class').html('');
        $.fancybox({href: '#form_ajout', modal: true, minWidth: 700});
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter une convention : fermer le formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_annuler_ajout').click
    (
      function () {
        $.fancybox.close();
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter une convention : soumettre le formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider_ajout').click
    (
      function () {
        var connexion_mode_nom = $('#connexion_mode_nom option:selected').val();
        var tab_infos = connexion_mode_nom.split('~');
        var connexion_mode = tab_infos[0];
        var connexion_ref = tab_infos[1];
        var f_annee = $('#f_annee option:selected').val();
        if (f_annee == '-1') {
          $('#ajax_msg_ajout').attr('class', 'erreur').html('Période manquante !');
          return false;
        }
        $('#form_ajout button').prop('disabled', true);
        $('#ajax_msg_ajout').attr('class', 'loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type: 'POST',
            url: 'ajax.php?page=' + window.PAGE,
            data: 'csrf=' + window.CSRF + '&f_action=' + 'ajouter_convention' + '&f_connexion_mode=' + connexion_mode + '&f_connexion_ref=' + connexion_ref + '&f_annee=' + f_annee,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
              $('#form_ajout button').prop('disabled', false);
              $('#ajax_msg_ajout').attr('class', 'alerte').html(afficher_json_message_erreur(jqXHR, textStatus));
              return false;
            },
            success: function (responseJSON) {
              initialiser_compteur();
              $('#form_ajout button').prop('disabled', false);
              if (responseJSON['statut'] == true) {
                $('#table_action tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
                $('#table_action tbody').prepend(responseJSON['value']);
                $.fancybox('<label class="valide">Convention ajoutée : détail des instructions envoyé par courriel.</label>');
              } else {
                $('#ajax_msg_ajout').attr('class', 'alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Imprimer une convention ou une facture
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'a',
      function () {
        var objet = $(this).attr('href').substring(1);
        var convention_id = $(this).parent().parent().data('id');
        $.fancybox('<label class="loader">' + 'En cours&hellip;' + '</label>');
        $.ajax
        (
          {
            type: 'POST',
            url: 'ajax.php?page=' + window.PAGE,
            data: 'csrf=' + window.CSRF + '&f_action=' + 'imprimer_document' + '&f_objet=' + objet + '&f_convention_id=' + convention_id,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
              $.fancybox('<label class="alerte">' + afficher_json_message_erreur(jqXHR, textStatus) + '</label>');
              return false;
            },
            success: function (responseJSON) {
              if (responseJSON['statut'] == true) {
                $.fancybox( { href:responseJSON['value'] , type:'iframe' , width:'100%', height:'100%' , autoSize:false } );
              } else {
                $.fancybox('<label class="alerte">' + responseJSON['value'] + '</label>');
              }
            }
          }
        );
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Chorus : enregistrement des choix / paramètres
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider_chorus').click
    (
      function () {
        $('#bouton_valider_chorus').prop('disabled', true);
        $('#ajax_msg_chorus').attr('class', 'loader').html('En cours&hellip; merci de patienter');
        $.ajax
        (
          {
            type: 'POST',
            url: 'ajax.php?page=' + window.PAGE,
            data: 'csrf=' + window.CSRF + '&f_action=' + 'chorus_enregistrer_configuration&' + $('#form_chorus').serialize(),
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
              $('#bouton_valider_chorus').prop('disabled', false);
              $('#ajax_msg_chorus').attr('class', 'alerte').html(afficher_json_message_erreur(jqXHR, textStatus));
              return false;
            },
            success: function (responseJSON) {
              initialiser_compteur();
              $('#bouton_valider_chorus').prop('disabled', false);
              if (responseJSON['statut'] == true) {
                $('#ajax_msg_chorus').attr('class', 'valide').html('Configuration enregistrée.');
              } else {
                $('#ajax_msg_chorus').attr('class', 'alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    $('#f_chorus_envoi_automatique , #f_chorus_code_service , #f_chorus_numero_engagement').change
    (
      function()
      {
        $('#ajax_msg_chorus').attr('class','alerte').html('Penser à enregistrer !');
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Chorus : envoi de la facture
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'click',
      'button.eclair',
      function () {
        var objet_button = $(this);
        var convention_id = objet_button.parent().parent().data('id');
        objet_button.html('En cours&hellip;').prop('disabled',true);
        $.ajax
        (
          {
            type: 'POST',
            url: 'ajax.php?page=' + window.PAGE,
            data: 'csrf=' + window.CSRF + '&f_action=' + 'chorus_envoyer_facture' + '&f_convention_id=' + convention_id,
            dataType: 'json',
            error: function (jqXHR, textStatus, errorThrown) {
              $.fancybox('<label class="alerte">' + afficher_json_message_erreur(jqXHR, textStatus) + '</label>');
              objet_button.html('Envoyer vers Chorus').prop('disabled',false);
              return false;
            },
            success: function (responseJSON) {
              if (responseJSON['statut'] == true) {
                initialiser_compteur();
                objet_button.replaceWith(responseJSON['value']);
              } else {
                $.fancybox('<label class="alerte">' + responseJSON['value'] + '</label>');
                objet_button.html('Envoyer vers Chorus').prop('disabled',false);
              }
            }
          }
        );
      }
    );

  }
);
