<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Reporter des notes -> redirection vers la page pour le traiter
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( isset($_POST['f_action']) && ($_POST['f_action']=='reporter_notes') )
{
  require(CHEMIN_DOSSIER_INCLUDE.'code_report_notes_releve_to_bulletin.php');
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Autres cas
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$releve_modele            = Clean::post('f_objet'             , 'texte');
$releve_individuel_format = Clean::post('f_individuel_format' , 'texte');
$eleves_sans_note         = Clean::post('f_eleves_sans_note'  , 'bool');
$aff_etat_acquisition     = Clean::post('f_etat_acquisition'  , 'bool');
$aff_moyenne_scores       = Clean::post('f_moyenne_scores'    , 'bool');
$aff_pourcentage_acquis   = Clean::post('f_pourcentage_acquis', 'bool');
$conversion_sur_20        = Clean::post('f_conversion_sur_20' , 'bool');
$tableau_synthese_format  = Clean::post('f_synthese_format'   , 'texte');
$tableau_tri_etat_mode    = Clean::post('f_tri_etat_mode'     , 'texte');
$repeter_entete           = Clean::post('f_repeter_entete'    , 'bool');
$with_coef                = Clean::post('f_with_coef'         , 'bool');
$groupe_id                = Clean::post('f_groupe'            , 'entier');
$groupe_nom               = Clean::post('f_groupe_nom'        , 'texte');
$groupe_type              = Clean::post('f_groupe_type'       , 'lettres');
$matiere_id               = Clean::post('f_matiere'           , 'entier');
$matiere_nom              = Clean::post('f_matiere_nom'       , 'texte');
$evaluation_nom           = Clean::post('f_evaluation_nom'    , 'texte');
$periode_id               = Clean::post('f_periode'           , 'entier');
$date_debut               = Clean::post('f_date_debut'        , 'date_fr');
$date_fin                 = Clean::post('f_date_fin'          , 'date_fr');
$retroactif               = Clean::post('f_retroactif'        , 'calcul_retroactif');
$only_etat                = Clean::post('f_only_etat'         , 'texte');
$only_diagnostic          = Clean::post('f_only_diagnostic'   , 'texte');
$only_socle               = Clean::post('f_only_socle'        , 'bool');
$only_valeur              = Clean::post('f_only_valeur'       , 'bool');
$only_prof                = Clean::post('f_only_prof'         , 'bool');
$aff_reference            = Clean::post('f_reference'         , 'bool');
$aff_coef                 = Clean::post('f_coef'              , 'bool');
$aff_socle                = Clean::post('f_socle'             , 'bool');
$aff_comm                 = Clean::post('f_comm'              , 'bool');
$aff_lien                 = Clean::post('f_lien'              , 'bool');
$aff_panier               = Clean::post('f_panier'            , 'bool');
$aff_domaine              = Clean::post('f_domaine'           , 'bool');
$aff_theme                = Clean::post('f_theme'             , 'bool');
$releve_tri               = Clean::post('f_releve_tri'        , 'texte');
$orientation              = Clean::post('f_orientation'       , 'texte');
$couleur                  = Clean::post('f_couleur'           , 'texte');
$fond                     = Clean::post('f_fond'              , 'texte');
$legende                  = Clean::post('f_legende'           , 'texte');
$marge_min                = Clean::post('f_marge_min'         , 'entier');
$pages_nb                 = Clean::post('f_pages_nb'          , 'texte');
$cases_auto               = Clean::post('f_cases_auto'        , 'bool');
$cases_nb                 = Clean::post('f_cases_nb'          , 'entier');
$cases_largeur            = Clean::post('f_cases_largeur'     , 'entier');
$eleves_ordre             = Clean::post('f_eleves_ordre'      , 'eleves_ordre', 'nom'); // Non transmis par Safari si dans le <span> avec la classe "hide".
$prof_objet_id            = Clean::post('f_prof_objet'        , 'entier');
$prof_only_id             = Clean::post('f_prof_only'         , 'entier');
$prof_texte_objet         = Clean::post('f_prof_texte_objet'  , 'texte');
$prof_texte_only          = Clean::post('f_prof_texte_only'   , 'texte');
$highlight_id             = Clean::post('f_highlight_id'      , 'entier');
$origine                  = Clean::post('f_origine'           , 'texte');

// tableaux
$tab_matiere = Clean::post('f_matieres'    , array('array',','));
$tab_eleve   = Clean::post('f_eleve'       , array('array',','));
$tab_type    = Clean::post('f_type'        , array('array',','));
$tab_items   = Clean::post('f_compet_liste', array('array','_'));
$tab_evals   = Clean::post('f_evaluation'  , array('array',','));

$tab_matiere = array_filter( Clean::map('entier',$tab_matiere) , 'positif' );
$tab_eleve   = array_filter( Clean::map('entier',$tab_eleve)   , 'positif' );
$tab_items   = array_filter( Clean::map('entier',$tab_items)   , 'positif' );
$tab_evals   = array_filter( Clean::map('entier',$tab_evals)   , 'positif' );
$tab_type    = Clean::map('texte',$tab_type);

// Appel depuis la page evaluation_ponctuelle.php
if($origine=='evaluation_ponctuelle')
{
  $releve_modele            = 'selection';
  $tab_type                 = array('individuel');
  $releve_individuel_format = 'item';
  $aff_etat_acquisition     = 1;
  $eleves_sans_note         = 1;
  $date_debut               = To::jour_debut_annee_scolaire('fr');
  $date_fin                 = TODAY_FR;
  $retroactif               = 'auto';
  $only_etat                = 'tous';
  $only_diagnostic          = 'non';
  $releve_tri               = 'none';
  $orientation              = 'portrait';
  $couleur                  = 'oui';
  $fond                     = 'gris';
  $legende                  = 'non';
  $marge_min                = 5;
  $pages_nb                 = 'optimise';
  $cases_auto               = 1;
  $cases_nb                 = 4;
  $cases_largeur            = 5;
  $affichage_direct         = TRUE;
}

$liste_matiere_id = implode(',',$tab_matiere);

// En cas de manipulation du formulaire (avec les outils de développements intégrés au navigateur ou un module complémentaire)...
if( ($releve_modele=='matieres') || ($releve_modele=='multimatiere') )
{
  $tab_type = array('individuel');
}
if(in_array($_SESSION['USER_PROFIL_TYPE'],array('parent','eleve')))
{
  $releve_individuel_format = 'eleve';
  $eleves_sans_note         = 0;
  $aff_moyenne_scores       = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_MOYENNE_SCORE'])      ? $aff_moyenne_scores     : 0 ;
  $aff_pourcentage_acquis   = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_POURCENTAGE_ACQUIS']) ? $aff_pourcentage_acquis : 0 ;
  $conversion_sur_20        = Outil::test_user_droit_specifique($_SESSION['DROIT_RELEVE_CONVERSION_SUR_20'])  ? $conversion_sur_20      : 0 ;
  $tab_type                 = array('individuel');
  $aff_panier               = 1;
}

// Pour un élève on surcharge avec les données de session
if($_SESSION['USER_PROFIL_TYPE']=='eleve')
{
  $tab_eleve  = array($_SESSION['USER_ID']);
  $groupe_id  = $_SESSION['ELEVE_CLASSE_ID'];
  $groupe_nom = $_SESSION['ELEVE_CLASSE_NOM'];
}

// Pour un parent on vérifie que c’est bien un de ses enfants
if( isset($tab_eleve[0]) && ($_SESSION['USER_PROFIL_TYPE']=='parent') )
{
  Outil::verif_enfant_parent( $tab_eleve[0] );
}

// Pour un professeur on vérifie que ce sont bien ses élèves
if( ($_SESSION['USER_PROFIL_TYPE']=='professeur') && ($_SESSION['USER_JOIN_GROUPES']=='config') )
{
  Outil::verif_eleves_prof( $tab_eleve );
}

$type_individuel = in_array('individuel',$tab_type) ? 1 : 0 ;
$type_synthese   = in_array('synthese',$tab_type)   ? 1 : 0 ;
$type_bulletin   = in_array('bulletin',$tab_type)   ? 1 : 0 ;

$liste_eleve = implode(',',$tab_eleve);

$tab_modele = array(
  'matiere'      => TRUE,
  'matieres'     => TRUE,
  'multimatiere' => TRUE,
  'selection'    => TRUE,
  'evaluation'   => TRUE,
  'tableau_eval' => TRUE,
  'professeur'   => TRUE,
);

if(
    !isset($tab_modele[$releve_modele]) ||
    ( ($releve_modele=='matiere') && ( !$matiere_id || !$matiere_nom ) ) ||
    ( ($releve_modele=='matieres') && count($tab_matiere)<2 ) ||
    ( ($releve_modele=='professeur') && !$prof_objet_id ) ||
    ( ($releve_modele=='evaluation') && !count($tab_evals) ) ||
    ( ($releve_modele=='tableau_eval') && !count($tab_evals) ) ||
    ( ($releve_modele=='selection') && !count($tab_items) ) ||
    ( ($releve_modele!='tableau_eval') && !$retroactif ) ||
    !$releve_tri || !$orientation || !$couleur || !$fond || !$legende || !$marge_min || !$pages_nb || is_null($cases_nb) || !$cases_largeur ||
    ( !$periode_id && (!$date_debut || !$date_fin) ) || !$only_etat || !$only_diagnostic || ( $only_prof && !$prof_only_id ) ||
    !$groupe_id || !$groupe_nom || !$groupe_type || !count($tab_eleve) || !count($tab_type) || !$eleves_ordre
  )
{
  Json::end( FALSE , 'Erreur avec les données transmises !');
}

Form::save_choix('releve_items');

// Pour les évaluations à la volée.
if($_SESSION['USER_PROFIL_TYPE']=='professeur')
{
  Session::generer_jeton_anti_CSRF('evaluation_ponctuelle');
  $CSRF_eval_eclair = Session::$_CSRF_value;
}

// Fermeture de session (mais pas destruction, juste écriture et libération des données pour éviter un verrouillage en écriture)
Session::write_close();

// Bricoles restantes

if($releve_modele=='evaluation')
{
  $retroactif = 'non';
}
if($releve_modele=='tableau_eval')
{
  $retroactif = 'non';
  $type_individuel   = 0;
  $type_synthese     = 0;
  $type_bulletin     = 0;
  $type_tableau_eval = 1;
}
else
{
  $type_tableau_eval = 0;
}
$marge_gauche = $marge_droite = $marge_haut = $marge_bas = $marge_min ;

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// INCLUSION DU CODE COMMUN À PLUSIEURS PAGES
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$make_officiel = FALSE;
$make_brevet   = FALSE;
$make_action   = '';
$make_html     = TRUE;
$make_pdf      = ($origine!='evaluation_ponctuelle')    ? TRUE : FALSE ;
$make_csv      = ($releve_individuel_format == 'eleve') ? TRUE : FALSE ;
$make_json     = ( ($releve_individuel_format == 'eleve') && $aff_etat_acquisition ) ? TRUE : FALSE ;
$make_graph    = FALSE;

require(CHEMIN_DOSSIER_INCLUDE.'noyau_items_releve.php');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Affichage du résultat
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$retour = '';

if($affichage_direct)
{
  $retour .=
    '<hr>'.NL
  . '<ul class="puce">'.NL
  .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
  . '</ul>'.NL
  . $html;
}
else
{
  if($type_individuel)
  {
    $li_individuel_csv  = ($make_csv)  ? '<li><a target="_blank" rel="noopener noreferrer" href="force_download.php?fichier='.str_replace('<REPLACE>','individuel',$fichier_nom).'.csv"><span class="file file_txt">Exploitation tableur (format <em>csv</em>).</span></a></li>'.NL : '' ;
    $li_individuel_json = ($make_json) ? '<li><a target="_blank" rel="noopener noreferrer" href="force_download.php?fichier='.str_replace('<REPLACE>','individuel',$fichier_nom).'.json"><span class="file file_json">Extraction des données (format <em>json</em>).</span></a></li>'.NL : '' ;
    $retour .=
      '<h2>Relevé individuel</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','individuel',$fichier_nom).'"><span class="file file_htm">Explorer / Manipuler (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    .   $li_individuel_csv
    .   $li_individuel_json
    . '</ul>'.NL;
  }
  if($type_synthese)
  {
    $retour .=
      '<h2>Synthèse collective</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','synthese',$fichier_nom).'"><span class="file file_htm">Explorer / Manipuler (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','synthese',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','synthese',$fichier_nom).'.csv"><span class="file file_txt">Exploitation tableur (format <em>csv</em>).</span></a></li>'.NL
    . '</ul>'.NL;
  }
  if($type_tableau_eval)
  {
    $retour .=
      '<h2>Tableau des saisies</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','tableau',$fichier_nom).'"><span class="file file_htm">Voir / Afficher (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','tableau',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    . '</ul>'.NL;
  }
  if($type_bulletin)
  {
    $retour .=
      '<h2>Moyenne sur 20 - Élément d’appréciation</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','bulletin',$fichier_nom).'"><span class="file file_htm">Explorer / Manipuler (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','bulletin',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','bulletin',$fichier_nom).'.csv"><span class="file file_txt">Exploitation tableur (format <em>csv</em>).</span></a></li>'.NL
    . '</ul>'.NL;
    if($_SESSION['USER_PROFIL_TYPE']=='professeur')
    {
      $retour .=
        '<h2>Bulletin SACoche</h2>'.NL
      . '<ul class="puce">'.NL
      . $bulletin_form
      . '</ul>'.NL
      . $bulletin_alerte
      . '<h2>Bulletin Gepi</h2>'.NL
      . '<ul class="puce">'.NL
      .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','bulletin_note_appreciation'            ,$fichier_nom).'.csv"><span class="file file_txt">Récupérer notes (moyennes scores) et appréciations (% items acquis) à importer dans GEPI (format <em>csv</em>).</span></a></li>'.NL
      .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','bulletin_note'                         ,$fichier_nom).'.csv"><span class="file file_txt">Récupérer les notes (moyennes scores) à importer dans GEPI (format <em>csv</em>).</span></a></li>'.NL
      .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','bulletin_appreciation_pourcent_acquis' ,$fichier_nom).'.csv"><span class="file file_txt">Récupérer les appréciations (% items acquis) à importer dans GEPI (format <em>csv</em>).</span></a></li>'.NL
      .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','bulletin_appreciation_moyennes_scores' ,$fichier_nom).'.csv"><span class="file file_txt">Récupérer les appréciations (moyennes scores) à importer dans GEPI (format <em>csv</em>).</span></a></li>'.NL
      . '</ul>'.NL
      . '<h2>Devoir Pronote</h2>'.NL
      . '<ul class="puce">'.NL
      .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','pronote',$fichier_nom).'.csv"><span class="file file_txt">Récupérer les notes (moyennes scores) et appréciations (% items acquis) à importer dans Pronote (format <em>csv</em>).</span></a></li>'.NL
      . '</ul>'.NL;
    }
  }
}

Json::add_tab( array(
  'direct' => $affichage_direct ,
  'bilan'  => $retour ,
) );
Json::end( TRUE );

?>
