<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO){Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action      = Clean::post('f_action'     , 'texte');
$matiere_id  = Clean::post('f_matiere_id' , 'entier');
$item_id     = Clean::post('f_item_id'    , 'entier');
$eleve_id    = Clean::post('f_eleve_id'   , 'entier');
$devoir_id   = Clean::post('f_devoir_id'  , 'entier');
$note_val    = Clean::post('f_note'       , 'texte');
$description = Clean::post('f_description', 'texte', 60);

$tab_notes = array_merge( $_SESSION['NOTE_ACTIF'] , array( 'NN' , 'NE' , 'NF' , 'NR' , 'AB' , 'DI' ) ); // , 'PA' , 'X'

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Vérifier les infos, voir les évaluations d’un item
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='charger_formulaire') && $matiere_id && $item_id && $eleve_id )
{
  // récup infos élève
  $DB_ROW_ELEVE = DB_STRUCTURE_PROFESSEUR::DB_recuperer_eleve_infos( $eleve_id );
  if(empty($DB_ROW_ELEVE))
  {
    Json::end( FALSE , 'Élève introuvable.' );
  }
  $eleve_nom        = $DB_ROW_ELEVE['user_nom'];
  $eleve_prenom     = $DB_ROW_ELEVE['user_prenom'];
  $eleve_classe_id  = $DB_ROW_ELEVE['eleve_classe_id'];
  $tab_eleve_groupe = explode(',',(string)$DB_ROW_ELEVE['listing_groupes']);
  // récup infos item
  $DB_ROW_ITEM = DB_STRUCTURE_COMMUN::DB_recuperer_item_infos( $item_id );
  if(empty($DB_ROW_ITEM))
  {
    Json::end( FALSE , 'Item introuvable.' );
  }
  $item_nom = $DB_ROW_ITEM['item_nom'];
  // vérif lien item / matière
  if( $DB_ROW_ITEM['matiere_id'] != $matiere_id )
  {
    Json::end( FALSE , 'Incohérence item / matière.' );
  }
  // vérif lien matière / prof
  $listing_prof_matieres_id = DB_STRUCTURE_COMMUN::DB_recuperer_matieres_professeur($_SESSION['USER_ID']);
  $tab_prof_matieres_id = !empty($listing_prof_matieres_id) ? explode(',',$listing_prof_matieres_id) : array() ;
  if(!in_array($matiere_id,$tab_prof_matieres_id))
  {
    Json::end( FALSE , 'Vous n’êtes pas rattaché à la matière de cet item.' );
  }
  // vérif lien élève / prof
  if($_SESSION['USER_JOIN_GROUPES']=='config')
  {
    $tab_groupes = DB_STRUCTURE_COMMUN::DB_OPT_groupes_professeur($_SESSION['USER_ID']);
    if(is_string($tab_groupes))
    {
      Json::end( FALSE , $tab_groupes );
    }
    $find_regroupement_commun = FALSE;
    foreach($tab_groupes as $tab)
    {
      if( ( ($tab['optgroup']=='classe') && ($tab['valeur']==$eleve_classe_id) ) || ( ($tab['optgroup']!='classe') && in_array($tab['valeur'],$tab_eleve_groupe) ) )
      {
        $find_regroupement_commun = TRUE;
        break;
      }
    }
    if(!$find_regroupement_commun)
    {
      Json::end( FALSE , 'Cet élève ne vous est pas affecté.' );
    }
  }
  // liste des évaluations
  $tab_tr = array();
  $DB_TAB = DB_STRUCTURE_DEMANDE::DB_lister_result_eleve_item( $eleve_id , $item_id , NULL /*date_sql_debut*/ , TRUE /*with_saisie_info*/ );
  foreach($DB_TAB as $DB_ROW)
  {
    // On ne permet de modifier une note que si on est l’auteur de la saisie et que c’est au cours de l’année scolaire actuelle.
    $input = ( ( $DB_ROW['date'] < To::jour_debut_annee_scolaire('sql') ) || ( $DB_ROW['prof_id'] != $_SESSION['USER_ID'] ) ) ? '' : '<input type="radio" id="devoir_'.$DB_ROW['devoir_id'].'" name="devoir" value="'.$DB_ROW['devoir_id'].'">' ;
    $tab_tr[] = 
      '<tr>'.
        '<td class="nu">'.$input.'</td>'.
        '<td class="label hc">'.Html::note_image($DB_ROW['note'],'','',FALSE).'</td>'.
        '<td class="label">'.To::date_sql_to_french($DB_ROW['date']).'</td>'.
        '<td class="label">'.html($DB_ROW['saisie_info']).'</td>'.
      '</tr>'
    ;
  }
  $lignes_eval = implode('',$tab_tr);
  if( strpos($lignes_eval,'<input') === FALSE )
  {
    Json::end( FALSE , 'Vous n’êtes l’auteur d’aucune saisie sur cette année scolaire.' );
  }
  // boutons radio
  $tab_radio_boutons = array();
  $tab_notes = array_merge( $_SESSION['NOTE_ACTIF'] , array( 'NN' , 'NE' , 'NF' , 'NR' , 'AB' , 'DI' ) ); // , 'PA' , 'X'
  foreach($tab_notes as $note)
  {
    $tab_radio_boutons[] = '<label for="f_note_modif_'.$note.'" class="note"><input type="radio" id="f_note_modif_'.$note.'" name="f_note_modif" value="'.$note.'"><img alt="'.$note.'" src="'.Html::note_src($note).'"></label>';
  }
  $radio_boutons = implode(' ',$tab_radio_boutons);
  // retour
  Json::add_tab( array(
    'eleve_nom'    => html($eleve_nom) ,
    'eleve_prenom' => html($eleve_prenom) ,
    'item_nom'     => html($item_nom) ,
    'item_id'      => $item_id ,
    'eleve_id'     => $eleve_id ,
    'lignes_eval'  => $lignes_eval ,
    'label_radio'  => $radio_boutons ,
  ) );
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier une note saisie
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier_saisie') && $devoir_id && $item_id && $eleve_id && in_array($note_val,$tab_notes) && $description )
{
  $recup_note = DB_STRUCTURE_PROFESSEUR::DB_tester_note_modifiable( $eleve_id , $devoir_id , $item_id );
  if(!$recup_note)
  {
    Json::end( FALSE , 'Note non modifiable (pas de vous ou hors année scolaire) !' );
  }
  if( $recup_note == $note_val )
  {
    Json::end( FALSE , 'Note identique !' );
  }
  $info = $description; // Comporte déjà le nom de l’évaluateur
  $is_modif = DB_STRUCTURE_PROFESSEUR::DB_modifier_saisie( $_SESSION['USER_ID'] , $eleve_id , $devoir_id , $item_id , $note_val , $info );
  if( !$is_modif )
  {
    // normalement on ne peut pas passer ici avec le test précédent
    Json::end( FALSE , 'Note introuvable ou identique !' );
  }
  // Retour
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );


?>
