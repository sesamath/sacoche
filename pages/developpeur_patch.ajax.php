<?php
if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

$action   = Clean::post('f_action'  , 'texte');
$base_id  = Clean::post('f_base_id' , 'entier');
$filename = Clean::post('f_filename', 'texte');
$date_sql = Clean::post('f_date_sql', 'texte');

if(HEBERGEUR_INSTALLATION=='mono-structures')
{
  Json::end( FALSE , 'Multi-structures uniquement !' );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Lister les établissements affectés par le bug d’initialisation annuelle 2017-2018
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($action=='lister')
{
  $date_version_bug_debut_sql = '2017-11-26';
  $date_version_bug_fin_sql   = '2018-05-16';
  Json::add_str('<tr><th>Base</th><th>Date purge</th><th>Incrément</th><th>Date base restauree</th><th class="nu"></th></tr>');
  $DB_TAB = DB_WEBMESTRE_WEBMESTRE::DB_lister_structures();
  foreach($DB_TAB as $DB_ROW)
  {
    $base_id = $DB_ROW['sacoche_base'];
    $fichier_log_contenu = SACocheLog::lire($base_id);
    if($fichier_log_contenu!==NULL)
    {
      $tab_lignes = SACocheLog::extraire_lignes($fichier_log_contenu);
      $nb_lignes = count($tab_lignes);
      for( $indice_ligne=0 ; $indice_ligne<$nb_lignes ; $indice_ligne++ )
      {
        list( $balise_debut , $date_heure , $utilisateur , $action , $balise_fin ) = explode("\t",$tab_lignes[$indice_ligne]);
        $date_sql = To::date_french_to_sql( str_replace('-','/',substr($date_heure,0,10)) );
        if( ($date_sql>=$date_version_bug_debut_sql) && ($date_sql<=$date_version_bug_fin_sql) && strpos($action,'Purge annuelle de la base en cours') )
        {
          Json::add_str('<tr><td>'.$base_id.'</td><td>'.$date_sql.'</td><td></td><td></td><td class="nu"><q class="modifier"></q></td></tr>');
        }
      }
    }
  }
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Retourner les infos complémentaires pour un établissement donné
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='renseigner') && $base_id )
{
  // Charger les paramètres de connexion à cette base afin de pouvoir y effectuer des requêtes
  DBextra::charger_parametres_sql_supplementaires($base_id);
  $DB_TAB = DB_STRUCTURE_PARAMETRE::DB_lister_parametres('"log_bug_2018_may_purge_date","log_bug_2018_may_id_increment","log_bug_2018_may_restore_date"');
  foreach($DB_TAB as $DB_ROW)
  {
    ${$DB_ROW['parametre_nom']} = $DB_ROW['parametre_valeur'];
  }
  Json::add_str('<td>'.$base_id.'</td><td>'.$log_bug_2018_may_purge_date.'</td><td>'.$log_bug_2018_may_id_increment.'</td><td>'.$log_bug_2018_may_restore_date.'</td><td class="nu"><q class="modifier"></q></td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Application de requêtes correctives sur une base
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='uploader') && $base_id )
{
  // Récupération du fichier
  $fichier_nom = 'patch_'.$base_id.'_'.FileSystem::generer_fin_nom_fichier__date_et_alea().'.<EXT>';
  $result = FileSystem::recuperer_upload( CHEMIN_DOSSIER_IMPORT /*fichier_chemin*/ , $fichier_nom , array('zip','sql') , NULL /*tab_extensions_interdites*/ , NULL /*taille_maxi*/ , NULL /*filename_in_zip*/ );
  if($result!==TRUE)
  {
    Json::end( FALSE , $result );
  }
  Json::end( TRUE , FileSystem::$file_saved_name );
}

if( ($action=='appliquer') && $base_id && $date_sql && $filename )
{
  if(!is_file(CHEMIN_DOSSIER_IMPORT.$filename))
  {
    Json::end( FALSE , 'Fichier uploadé "'.$filename.'" non retrouvé !' );
  }
  $sql = file_get_contents(CHEMIN_DOSSIER_IMPORT.$filename);
  DBextra::charger_parametres_sql_supplementaires($base_id);
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $sql );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$date_sql.'" WHERE parametre_nom="log_bug_2018_may_restore_date"' );
  DB::close(SACOCHE_STRUCTURE_BD_NAME);
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Il se peut que rien n’ait été récupéré à cause de l’upload d’un fichier trop lourd
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if(empty($_POST))
{
  Json::end( FALSE , 'Aucune donnée reçue ! Fichier trop lourd ? '.InfoServeur::minimum_limitations_upload() );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
