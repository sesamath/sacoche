<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Photos des élèves'));

// Fabrication des éléments select du formulaire
$select_groupe          = HtmlForm::afficher_select(DB_STRUCTURE_COMMUN::DB_OPT_regroupements_etabl( FALSE /*sans*/ , FALSE /*tout*/ ) , 'f_groupe'          /*select_nom*/ , ''    /*option_first*/ , FALSE /*selection*/ , 'regroupements' /*optgroup*/ );
$select_groupe_multiple = HtmlForm::afficher_select(DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl()                                , 'f_groupe_multiple' /*select_nom*/ , FALSE /*option_first*/ , FALSE /*selection*/ , 'regroupements' /*optgroup*/ , TRUE /*multiple*/ , TRUE /*filter*/ );
?>

<p><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_administrateur__photos_eleves">DOC : Photos des élèves</a></span></p>
<div class="danger">Respectez les conditions légales d’utilisation (droit à l’image précisé dans la documentation ci-dessus).</div>

<?php /* Ce formulaire est à placer avant #zone_drop sinon cela déclenche dmUploader() ; je n’ai pas compris pourquoi, ce n’est pas à cause du name="userfile" identique... */ ?>

<form action="#" method="post" id="form_photo" class="hide"><fieldset>
  <input type="hidden" id="f_user_id" name="f_user_id" value=""><input type="hidden" name="f_action" value="envoyer_photo"><input id="f_photo" type="file" name="userfile"><button id="bouton_photo" type="button" class="fichier_import">Parcourir...</button>
</fieldset></form>

<hr>

<h2>Ajout multiple</h2>

<form action="#" method="post" id="form_photos"><fieldset>
  <p class="astuce">
    Taille maximale du fichier <em>zip</em> : <b><?php echo InfoServeur::minimum_limitations_upload('string' /*format_retour*/) ?></b>. <?php echo infobulle('Le webmestre du serveur a accès au détail de la configuration PHP expliquant cette limite.',TRUE) ?>
  </p>
  <label class="tab" for="f_masque">Forme noms fichiers :</label><input id="f_masque" name="f_masque" maxlength="255" size="75" type="text" value=""><br>
  <label class="tab p">Upload fichier(s) :</label><span class="u">Méthode n°1</span> &rarr; envoi d’un <em>zip</em> <input type="hidden" name="f_action" value="envoyer_zip"><input id="f_photos" type="file" name="userfile"><button id="bouton_photos_zip" type="button" class="fichier_import">Parcourir...</button><br>
  <span class="tab"></span><span class="u">Méthode n°2</span> &rarr; lot d’images <span id="zone_drop">Glisser-déposer vos fichiers ici<br>ou <input type="file" name="multifile"><button id="bouton_photos_multi" type="button" class="fichier_import">Sélection multiple...</button></span><br>
  <span class="tab"></span><label id="ajax_msg_photos">&nbsp;</label>
</fieldset></form>

<hr>

<h2>Suppression multiple</h2>

<p class="astuce">
  Usage inutile si on utilise l’ajout multiple ci-dessus car les nouvelles photos remplaceront alors automatiquement les anciennes.
</p>

<form action="#" method="post" id="form_select_multiple"><fieldset>
  <label class="tab" for="f_groupe_multiple">Regroupements :</label><?php echo $select_groupe_multiple; ?>
  <p><span class="tab"></span><button id="bouton_supprimer_multiple" type="button" class="supprimer">Supprimer.</button><label id="ajax_msg_multiple">&nbsp;</label></p>
</fieldset></form>

<hr>

<h2>Gestion individuelle</h2>

<form action="#" method="post" id="form_select"><fieldset>
  <label class="tab" for="f_groupe">Regroupement :</label><?php echo $select_groupe ?> <label id="ajax_msg">&nbsp;</label>
</fieldset></form>

<p id="liste_eleves">
</p>
