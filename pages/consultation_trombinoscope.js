/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// Variables globales à ne pas définir plus tard sinon la minification les renomme et cela pose ensuite souci.
var tab_eleve_image = [];
var tab_eleve_texte = [];

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var memo_choix          = '';
    var tab_eleve_numero    = [];
    var tab_eleve_echec     = [];
    var eleve_indice_actuel = 0;
    var eleves_nombre       = 0;
    var nb_passages         = 0;
    var action_groupe       = 'ajouter';
    var groupe_id           = 0;

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gestion du choix de la fonctionnalité
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_choix').change
    (
      function()
      {
        memo_choix = $('#f_choix option:selected').val();
        var titre = $('#f_choix option:selected').text();
        $('#h2_choix').html(titre);
        if(memo_choix=='voir_regroupement')
        {
          $('#form_select_multiple, #lien_telechargement_multiple, #zone_jeu').hide(0);
          $('#form_select_groupe, #affichage_formulaire, #lien_telechargement_groupe , #zone_affichage').show(0);
        }
        else if(memo_choix=='deviner_regroupement')
        {
          $('#form_select_multiple, #lien_telechargement_groupe, #lien_telechargement_multiple, #zone_affichage').hide(0);
          $('#form_select_groupe, #affichage_formulaire, #zone_jeu').show(0);
        }
        else if(memo_choix=='generer_pdf_multiple')
        {
          $('#form_select_groupe, #lien_telechargement_groupe, #zone_affichage, #zone_jeu').hide(0);
          $('#form_select_multiple, #affichage_formulaire, #lien_telechargement_multiple').show(0);
        }
        else
        {
          $('#form_select_groupe, #form_select_multiple, #affichage_formulaire, #lien_telechargement_groupe, #lien_telechargement_multiple , #zone_affichage , #zone_jeu').hide(0);
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Actualiser l’affichage des vignettes élèves au changement du select pour un regroupement donné
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_affichage()
    {
      $('#lien_telechargement_groupe, #zone_affichage , #zone_jeu').html('');
      // On récupère le regroupement
      var groupe_val = $('#f_groupe option:selected').val();
      if(!groupe_val)
      {
        $('#ajax_msg_groupe').removeAttr('class').html('');
        return false
      }
      // Pour un directeur ou un administrateur, groupe_val est de la forme d3 / n2 / c51 / g44
      if(isNaN(entier(groupe_val)))
      {
        var groupe_type = groupe_val.substring(0,1);
        var groupe_id   = groupe_val.substring(1);
      }
      // Pour un professeur, groupe_val est un entier, et il faut récupérer la 1ère lettre du label parent
      else
      {
        var groupe_type = $('#f_groupe option:selected').parent().attr('label').substring(0,1).toLowerCase();
        var groupe_id   = groupe_val;
      }
      var groupe_nom = $('#f_groupe option:selected').text();
      $('#ajax_msg_groupe').attr('class','loader').html('En cours&hellip;');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=generer_groupe'+'&f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_groupe_nom='+groupe_nom,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_msg_groupe').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==false)
            {
              $('#ajax_msg_groupe').attr('class','alerte').html(responseJSON['value']);
            }
            else
            {
              $('#ajax_msg_groupe').attr('class','valide').html('');
              $('#lien_telechargement_groupe').html(responseJSON['lien_telechargement']);
              $('#zone_affichage').html(responseJSON['affichage']);
              tab_eleve_image = JSON.parse(responseJSON['tab_eleve_image']);
              tab_eleve_texte = JSON.parse(responseJSON['tab_eleve_texte']);
              jeu_initialiser();
            }
          }
        }
      );
    }

    $('#f_groupe').change
    (
      function()
      {
        maj_affichage();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger tous les regroupements ou seulement les regroupements affectées (pour un prof)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#ajouter_groupe, #retirer_groupe').click
    (
      function()
      {
        $('button').prop('disabled',true);
        groupe_id = $('#f_groupe option:selected').val();
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=_maj_select_groupes_prof',
            data : 'f_groupe='+groupe_id+'&f_action='+action_groupe,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('button').prop('disabled',false);
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                action_groupe = (action_groupe=='ajouter') ? 'retirer' : 'ajouter' ;
                $('#ajouter_groupe').hideshow( action_groupe == 'ajouter' );
                $('#retirer_groupe').hideshow( action_groupe == 'retirer' );
                $('#f_groupe').html(responseJSON['value']);
                var groupe_val = $('#f_groupe').val();
                if(!groupe_val)
                {
                  $('#lien_telechargement_groupe, #zone_affichage , #zone_jeu').html('');
                }
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Générer un PDF pour plusieurs regroupements d’un coup
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider_multiple').click
    (
      function()
      {
        $('#lien_telechargement_multiple').html('');
        if( !$('#f_groupe_multiple input:checked:enabled').length )
        {
          $('#ajax_msg_multiple').attr('class','erreur').html('Sélectionnez au moins un regroupement !');
          return false;
        }
        $('#form_select_multiple button').prop('disabled',true);
        $('#ajax_msg_multiple').attr('class','loader').html('En cours&hellip;');
        // Grouper les checkbox dans un champ unique afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
        var tab_groupe_multiple = [];
        $('#f_groupe_multiple input:checked:enabled').each
        (
          function()
          {
            tab_groupe_multiple.push($(this).val());
          }
        );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=generer_multiple'+'&f_groupe_multiple='+tab_groupe_multiple,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#form_select_multiple button').prop('disabled',false);
              $('#ajax_msg_multiple').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_select_multiple button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#ajax_msg_multiple').attr('class','valide').html('');
                $('#lien_telechargement_multiple').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg_multiple').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    $('#form_select_multiple input').change
    (
      function()
      {
        $('#lien_telechargement_multiple').html('');
        $('#ajax_msg_multiple').removeAttr('class').html('');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gérer le jeu
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // à la souris
    $('#zone_jeu').on( 'click' , '#voir_reponse'       , jeu_voir_reponse );
    $('#zone_jeu').on( 'click' , '#eleve_reussi'       , jeu_passer_eleve_suivant );
    $('#zone_jeu').on( 'click' , '#eleve_echec'        , jeu_memoriser_eleve_echec );
    $('#zone_jeu').on( 'click' , '#recommencer_tout'   , jeu_initialiser );
    $('#zone_jeu').on( 'click' , '#recommencer_echecs' , jeu_recommencer_eleves_echec );

    // ou au clavier
    $(document).keyup
    (
      function(e)
      {
        switch (e.which)
        {
          case 39: // flèche droite
            if($('#voir_reponse').length)
            {
              jeu_voir_reponse();
              return false;
            }
            if($('#recommencer_echecs').length)
            {
              jeu_recommencer_eleves_echec();
              return false;
            }
            break;
          case 38: // flèche haut
            if($('#eleve_reussi').length)
            {
              jeu_passer_eleve_suivant();
              return false;
            }
            break;
          case 40: // flèche bas
            if($('#eleve_echec').length)
            {
              jeu_memoriser_eleve_echec();
              return false;
            }
            break;
          case 37: // flèche gauche
            if($('#recommencer_tout').length)
            {
              jeu_initialiser();
              return false;
            }
            break;
        }
      }
    );

    function randomize(tab)
    {
      var i, j, tmp;
      for (i = tab.length - 1; i > 0; i--)
      {
        j = Math.floor(Math.random() * (i + 1));
        tmp = tab[i];
        tab[i] = tab[j];
        tab[j] = tmp;
      }
      return tab;
    }

    function jeu_initialiser()
    {
      eleves_nombre = tab_eleve_image.length;
      tab_eleve_numero = [];
      tab_eleve_echec = [];
      for (var i=0;i<eleves_nombre;i++)
      {
        tab_eleve_numero.push(i);
      }
      randomize(tab_eleve_numero);
      eleve_indice_actuel = 0;
      nb_passages = 1;
      jeu_afficher_eleve();
    }

    function jeu_afficher_eleve()
    {
      $('#zone_jeu').html(
          '<table class="p ml"><tbody><tr>'
        + '<td id="jeu_image"><img src="'+tab_eleve_image[tab_eleve_numero[eleve_indice_actuel]]+'"></td>'
        + '<td id="jeu_propositions" class="hc"><p class="b">Qui suis-je ?</p><p><input type="button" id="voir_reponse" value="&#128064; Voir la réponse."> <kbd>&rarr;</kbd></p></td>'
        + '</tr></tbody></table>'
      );
      $('#f_choix, #f_groupe').blur();
    }

    function jeu_voir_reponse()
    {
      $('#jeu_propositions').html(
          '<p class="b">'+tab_eleve_texte[tab_eleve_numero[eleve_indice_actuel]]+'</p>'
        + '<p><input type="button" id="eleve_reussi" value="&#128522; J’avais trouvé."> <kbd>&uarr;</kbd></p>'
        + '<p><input type="button" id="eleve_echec" value="&#128545; Je n’avais pas trouvé."> <kbd>&darr;</kbd></p>'
      );
      $('#f_choix, #f_groupe').blur();
    }

    function jeu_memoriser_eleve_echec()
    {
      tab_eleve_echec.push( tab_eleve_numero[eleve_indice_actuel] );
      jeu_passer_eleve_suivant();
    }

    function jeu_passer_eleve_suivant()
    {
      eleve_indice_actuel++;
      if( eleve_indice_actuel != eleves_nombre )
      {
        jeu_afficher_eleve();
      }
      else
      {
        var nb_echecs = tab_eleve_echec.length;
        if(!nb_echecs)
        {
          if(nb_passages==1)
          {
            var phrase = '<p>&#128079; Bravo : vous connaissez tous les prénoms des élèves de ce regroupement !</p>';
            
          }
          else
          {
            var phrase = '<p>&#128077; Vous avez trouvé tous les élèves. Essayez maintenant de réussir du premier coup !</p>';
            
          }
        }
        else if(nb_echecs == 1)
        {
          var phrase = '<p>&#128077; Un seul élève non trouvé ('+tab_eleve_texte[tab_eleve_echec[0]]+'). Essayez maintenant de faire un sans faute !</p>';
        }
        else
        {
          
          var phrase = '<p><input type="button" id="recommencer_echecs" value="&#128260; Recommencer en mélangeant les '+nb_echecs+' élèves que je n’ai pas trouvés."> <kbd>&rarr;</kbd></p>';
          
        }
        $('#zone_jeu').html(
            '<p class="b">Toutes les photos ont été visionnées.</p>'
          + phrase
          + '<p><input type="button" id="recommencer_tout" value="&#8617;&#65039; Recommencer en mélangeant tous les élèves."> <kbd>&larr;</kbd></p>'
        );
        $('#f_choix, #f_groupe').blur();
      }
    }

    function jeu_recommencer_eleves_echec()
    {
      eleves_nombre = tab_eleve_echec.length;
      randomize(tab_eleve_echec);
      tab_eleve_numero = tab_eleve_echec;
      tab_eleve_echec = [];
      eleve_indice_actuel = 0;
      nb_passages++;
      jeu_afficher_eleve();
    }

  }
);
