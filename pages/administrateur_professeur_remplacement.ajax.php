<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action         = Clean::post('f_action'        , 'texte');
$absent_id      = Clean::post('f_absent_id'     , 'entier');
$remplacant_id  = Clean::post('f_remplacant_id' , 'entier');
$absent_nom     = Clean::post('f_absent_nom'    , 'texte');
$remplacant_nom = Clean::post('f_remplacant_nom', 'texte');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter un remplacement
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='ajouter') && $absent_id && $remplacant_id && ($absent_id != $remplacant_id) && $absent_nom && $remplacant_nom )
{
  // Vérifier que ce remplacement n’existe pas déjà
  if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_prof_remplacement($absent_id, $remplacant_id) )
  {
    Json::end( FALSE , 'Remplacement déjà existant !' );
  }
  // Insérer l’enregistrement
  DB_STRUCTURE_ADMINISTRATEUR::DB_ajouter_jointure_prof_remplacement($absent_id, $remplacant_id);
  // Afficher le retour
  Json::add_str('<tr class="new">');
  Json::add_str(  '<td data-id="'.$absent_id.'">'.html($absent_nom).'</td>');
  Json::add_str(  '<td data-id="'.$remplacant_id.'">'.html($remplacant_nom).'</td>');
  Json::add_str(  '<td class="nu">');
  Json::add_str(    '<q class="supprimer"'.infobulle('Supprimer ce remplacement (aucune confirmation ne sera demandée).').'></q>');
  Json::add_str(  '</td>');
  Json::add_str('</tr>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Supprimer un remplacement
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='supprimer') && $absent_id && $remplacant_id )
{
  DB_STRUCTURE_ADMINISTRATEUR::DB_supprimer_jointure_prof_remplacement($absent_id, $remplacant_id);
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
