/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// Variable globale Highcharts
var graphique;
var ChartOptions;

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var nb_caracteres_max = 1000;

    // Un établissement avec 430 groupes, mais du coup 1230 découpages en groupes de classes
    // s’est retrouvé avec une page très lourde à charger pour les CPE / DOC qui sont automatiquement affecté à tous les regroupements
    // (à multiplier par 4 périodes cela fait 5000 cellules de tableau, avec pour chacune 8 icônes d’action ayant des infobulles...).
    // La page générée de 2,2 Mo mettait environ 50 secondes avant d’apparaître, quand cela ne provoquait pas une erreur serveur.
    // Du coup des optimisations ont été mises en place pour alléger la masse de données envoyées
    // et compléter après coup le tableau en javacript (avec des contenus souvent identiques).
    // Le poids de la page est alors passé à 0,83 Mo, et le temps de chargement à 20 secondes sur ce serveur : soit un gain de 60 %.
    // Ce temps reste long mais cela vient de leur serveur : localement l’affichage correspondant ne prend que 2 à 3 secondes
    // (et charger 1 à 2 Mo de données depuis un serveur usuel ne prend que quelques secondes).
    for(var td_id in window.tab_td_numero_label)
    {
      $('#'+td_id).attr('class','hc notnow').html( window.tab_label[window.tab_td_numero_label[td_id]] );
    }
    for(var td_id in window.tab_td_numero_icone)
    {
      $('#'+td_id).append( window.tab_icone[window.tab_td_numero_icone[td_id]] );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Options de base pour le graphique : sont complétées ensuite avec les données personnalisées
    // @see   http://www.highcharts.com/ --> http://api.highcharts.com/highcharts
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    ChartOptions = {
      chart: {
        renderTo: 'div_graphique_officiel',
        alignTicks: false,
        type: 'column',
        spacingTop: 10
       },
      colors: 
        window.BACKGROUND_COLORS
      ,
      title: {
        style: { color: '#333' } ,
        text: null // Pourrait être MAJ ensuite
      },
      xAxis: {
        labels: {
          style: { color: '#000' },
          autoRotationLimit: 0
        },
        categories: [] // MAJ ensuite
      },
      yAxis: [
        {
          labels: { enabled: false },
          reversedStacks: false,
          min: 0,
          max: 100,
          gridLineWidth: 0,
          title: { style: { color: '#333' } , text: 'Items acquis' }
        },
        {} // MAJ ensuite
      ],
      tooltip: {
        formatter: function() {
          return this.series.name +' : '+ (this.y);
        }
      },
      plotOptions: {
        column: {
          stacking: 'percent',
          // des élèves mal intentionnés font disparaître de la synthèse graphique les plus mauvais niveaux de maitrises en cliquant sur la légende,
          // et ainsi présenter uniquement les compétences maîtrisées à leur famille !
          // décision est prise de retirer cette fonctionnalité pour tous car elle n’a pas vraiment d’utilité (y compris pour les bulletins scolaires)
          events: {
            legendItemClick: function () {
                return false; 
            }
          }
        }
      },
/*
// Demande http://sesaprof.sesamath.net/forum/viewtopic.php?id=2166
// Pour afficher les valeurs sans survol : @see http://jsfiddle.net/gh/get/jquery/2.2.4/highslide-software/highcharts.com/tree/master/samples/highcharts/demo/column-stacked/
// Problème des valeurs 0 affichées (on ne peut pas ne pas les envoyer sinon ça décale tout, et pas trouvé comment ne pas les afficher
// + Il faut ce choix d’affichage paramétrable (@see http://www.highcharts.com/demo/chart-update), c’est du boulot.
// Stoppé passé 30 minutes dessus.
      plotOptions: {
        column: {
          stacking: 'percent',
          dataLabels: {
            enabled: true
          }
        },
        series: {
          dataLabels: {
            enabled: true
          }
        }
      },
*/
      series: [] // MAJ ensuite
      ,
      credits: {
        enabled: false
      }
    };

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic pour tout cocher ou tout décocher
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_accueil').on
    (
      'click',
      'q.cocher_tout , q.cocher_rien',
      function()
      {
        var id_mask = $(this).attr('id').replace('_deb1_','^=').replace('_deb2_','^=').replace('_fin1_','$=').replace('_fin2_','$=');
        var etat = ( $(this).attr('class').substring(7) == 'tout' ) ? true : false ;
        $('input['+id_mask+']').prop('checked',etat);
      }
    );

    $('#rubrique_check_all').click
    (
      function()
      {
        $('#zone_chx_rubriques input[type=checkbox]').prop('checked',true);
        return false;
      }
    );
    $('#rubrique_uncheck_all').click
    (
      function()
      {
        $('#zone_chx_rubriques input[type=checkbox]').prop('checked',false);
        return false;
      }
    );

    $('#table_action').on
    (
      'click',
      'q.cocher_tout , q.cocher_rien',
      function()
      {
        var etat = ( $(this).attr('class').substring(7) == 'tout' ) ? true : false ;
        $('#table_action td.nu input[type=checkbox]').prop('checked',etat);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enregistrer les modifications de types et/ou d’accès
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider').click
    (
      function()
      {
        if(!$('#cadre_statut input[type=radio]:checked').length)
        {
          $('#ajax_msg_gestion').attr('class','erreur').html('Aucun statut coché !');
          return false;
        }
        var listing_id = []; $('#table_accueil input[type=checkbox]:checked').each(function(){listing_id.push($(this).attr('id'));});
        if(!listing_id.length)
        {
          $('#ajax_msg_gestion').attr('class','erreur').html('Aucune case du tableau cochée !');
          return false;
        }
        $('#ajax_msg_gestion').attr('class','loader').html('Envoi&hellip;'); // volontairement court
        $('#listing_ids').val(listing_id);
        $('#csrf').val(window.CSRF);
        var form = document.getElementById('cadre_statut');
        form.action = './index.php?page=officiel&section=accueil_'+window.BILAN_TYPE;
        form.method = 'post';
        form.submit();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation de variables utiles accessibles depuis toute fonction
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var memo_saisie        = [];
    var memo_objet         = '';
    var memo_section       = '';
    var memo_classe        = 0;
    var memo_groupe        = 0;
    var memo_periode       = 0;
 // var memo_eleve         = 0; // déjà défini de façon globale par script.js car besoin dans script.js
    var memo_rubrique_nom  = 0;
    var memo_rubrique_type = '';
    var memo_rubrique_id   = 0;
    var memo_html          = '';
    var memo_long_max      = '';
    var memo_auto_next     = false;
    var memo_auto_prev     = false;
    var memo_eleve_first   = 0;
    var memo_eleve_last    = 0;
    var memo_classe_first  = 0;
    var memo_classe_last   = 0;
    var memo_config        = 0;
    var memo_details       = [];


    var tab_classe_action_to_section = [];
    tab_classe_action_to_section['modifier']        = 'officiel_saisir';
    tab_classe_action_to_section['saisir_multiple'] = 'officiel_saisir_multiple';
    tab_classe_action_to_section['tamponner']       = 'officiel_saisir';
    tab_classe_action_to_section['detailler']       = 'officiel_examiner';
    tab_classe_action_to_section['voir']            = 'officiel_consulter';
    tab_classe_action_to_section['tableau']         = 'officiel_archiver';
    tab_classe_action_to_section['imprimer']        = 'officiel_imprimer';
    tab_classe_action_to_section['voir_archive']    = 'officiel_imprimer';

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur une image action
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_accueil td q').click
    (
      function()
      {
        memo_objet = $(this).attr('class');
        memo_section = tab_classe_action_to_section[memo_objet];
        if(typeof(memo_section)!='undefined')
        {
          var tab_ids = $(this).parent().attr('id').split('_');
          memo_classe  = tab_ids[1];
          memo_groupe  = tab_ids[2];
          memo_periode = tab_ids[3];
          memo_config  = window.tab_classe_config_ref[memo_classe];
          $('#f_objet').val(memo_objet);
          if( (memo_section=='officiel_saisir') || (memo_section=='officiel_saisir_multiple') || (memo_section=='officiel_consulter') )
          {
            // Masquer le tableau ; Afficher la zone action et charger son contenu
            $('#cadre_statut , #table_accueil').hide(0);
            $('#zone_action_eleve').html('<label class="loader">En cours&hellip;</label>').show(0);
            $.ajax
            (
              {
                type : 'POST',
                url : 'ajax.php?page='+window.PAGE,
                data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'initialiser'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
                dataType : 'json',
                error : function(jqXHR, textStatus, errorThrown)
                {
                  var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : 'Erreur 500&hellip; Mémoire insuffisante ? Sélectionner moins d’élèves à la fois ou demander à votre hébergeur d’augmenter la valeur "memory_limit".' ;
                  $('#zone_action_eleve').html('<label class="alerte">'+message+' <button id="fermer_zone_action_eleve" type="button" class="retourner">Retour</button></label>');
                  return false;
                },
                success : function(responseJSON)
                {
                  initialiser_compteur();
                  if(responseJSON['statut']==false)
                  {
                    $('#zone_action_eleve').html('<label class="alerte">'+responseJSON['value']+'</label> <button id="fermer_zone_action_eleve" type="button" class="retourner">Retour</button>');
                  }
                  else
                  {
                    $('#zone_action_eleve').html(responseJSON['html']);
                    $("#info_recalcul").html( window.tab_config[memo_config]['info_recalcul'] );
                    if( typeof(responseJSON['script']) !== 'undefined' )
                    {
                      eval( responseJSON['script'] ); // ChartOptions
                      graphique = new Highcharts.Chart(ChartOptions);
                    }
                    if(memo_section=='officiel_saisir_multiple')
                    {
                      // La mémorisation du contenu des champs s’effectue maintenant car ensuite un glisser-déposer ne déclenche pas l’événement focus
                      $('textarea[name=f_saisie_express] , input[name=f_saisie_express]').each
                      (
                        function()
                        {
                          var obj_dom = $(this);
                          var obj_id  = obj_dom.attr('id');
                          memo_saisie[obj_id] = obj_dom.val().trim();
                        }
                      );
                    }
                    else
                    {
                      catalogue_prenom = $('#go_selection_eleve option:selected').data('prenom');
                      memo_eleve       =  entier( $('#go_selection_eleve option:selected').val() );
                      memo_eleve_first =  entier( $('#go_selection_eleve option:first'   ).val() );
                      memo_eleve_last  =  entier( $('#go_selection_eleve option:last'    ).val() );
                      charger_photo_eleve(memo_eleve,'maj');
                    }
                    masquer_element_navigation_choix_eleve();
                    $('#cadre_photo').show(0);
                  }
                }
              }
            );
          }
          else if(memo_section=='officiel_examiner')
          {
            // Masquer le tableau ; Afficher la zone de choix des rubriques
            $('#cadre_statut , #table_accueil').hide(0);
            $('#zone_action_classe h2').html('Recherche de saisies manquantes');
            if( window.tab_config[memo_config]['APP_RUBRIQUE_LONGUEUR'] && window.tab_config[memo_config]['APP_GENERALE_LONGUEUR'] )
            {
              $('#zone_chx_rubriques').find('input').prop('disabled',false);
            }
            else if( window.tab_config[memo_config]['APP_RUBRIQUE_LONGUEUR'] && !window.tab_config[memo_config]['APP_GENERALE_LONGUEUR'] )
            {
              $('#zone_chx_rubriques').find('input').prop('disabled',false);
              $('#rubrique_0').prop('disabled',true);
            }
            else if( !window.tab_config[memo_config]['APP_RUBRIQUE_LONGUEUR'] && window.tab_config[memo_config]['APP_GENERALE_LONGUEUR'] )
            {
              $('#zone_chx_rubriques').find('input').prop('disabled',true);
              $('#rubrique_0').prop('disabled',false);
            }
            else if( !window.tab_config[memo_config]['APP_RUBRIQUE_LONGUEUR'] && !window.tab_config[memo_config]['APP_GENERALE_LONGUEUR'] )
            {
              $('#zone_chx_rubriques').find('input').prop('disabled',true);
            }
            $('#zone_chx_rubriques').show(0);
            $("#info_recalcul").html( window.tab_config[memo_config]['info_recalcul'] );
          }
          else if(memo_section=='officiel_imprimer')
          {
            // Masquer le tableau ; Afficher la zone de choix des élèves, avec info si les bulletins sont déjà imprimés
            var titre = (memo_objet=='imprimer') ? 'Imprimer le bilan (PDF)' : 'Consulter un bilan imprimé (PDF)' ;
            configurer_form_choix_classe();
            $('#cadre_statut , #table_accueil').hide(0);
            $('#zone_action_classe h2').html(titre);
            $('#report_periode').html( $('#periode_'+memo_periode).text()+' :' );
            $('#zone_action_classe , #zone_'+memo_objet).show(0);
            charger_formulaire_imprimer();
          }
          else if(memo_section=='officiel_archiver')
          {
            // Masquer le tableau ; Afficher la zone de choix des données à archiver
            configurer_form_choix_classe();
            masquer_element_navigation_choix_classe();
            $('#cadre_statut , #table_accueil').hide(0);
            $('#zone_action_classe h2').html('Tableaux de positionnements / d’appréciations');
            $('#report_periode').html( $('#periode_'+memo_periode).text()+' :' );
            $('#ajax_msg_tableau').removeAttr('class').html('');
            tableaux_visibilite();
            $('#zone_action_classe , #zone_'+memo_objet).show(0);
          }
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire #zone_action_deport : envoyer un import csv (saisie déportée)
    // Upload d’un fichier (avec jquery.form.js)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire_saisie_deportee = $('#zone_action_deport');

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions_saisie_deportee =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#msg_import',
      error : retour_form_erreur_saisie_deportee,
      success : retour_form_valide_saisie_deportee
    };

    // Vérifications précédant l’envoi du formulaire, déclenchées au choix d’un fichier
    $('#f_saisie_deportee').change
    (
      function()
      {
        var file = this.files[0];
        if( typeof(file) == 'undefined' )
        {
          $('#msg_import').removeAttr('class').html('');
          return false;
        }
        else
        {
          var fichier_nom = file.name;
          var fichier_ext = fichier_nom.split('.').pop().toLowerCase();
          if( '.csv.txt.'.indexOf('.'+fichier_ext+'.') == -1 )
          {
            $('#msg_import').attr('class','erreur').html('Le fichier "'+escapeHtml(fichier_nom)+'" n’a pas l’extension "csv" ou "txt".');
            return false;
          }
          else
          {
            $('#f_upload_bilan_type').val( window.BILAN_TYPE );
            $('#f_upload_classe'    ).val( memo_classe );
            $('#f_upload_groupe'    ).val( memo_groupe );
            $('#f_upload_periode'   ).val( memo_periode );
            $('#f_upload_objet'     ).val( $('#f_objet').val() );
            $('#f_upload_mode'      ).val( $('#f_mode').val() );
            $('#bouton_choisir_saisie_deportee').prop('disabled',true);
            $('#msg_import').attr('class','loader').html('En cours&hellip;');
            formulaire_saisie_deportee.submit();
          }
        }
      }
    );

    // Envoi du formulaire (avec jquery.form.js)
    formulaire_saisie_deportee.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions_saisie_deportee);
        return false;
      }
    );

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur_saisie_deportee(jqXHR, textStatus, errorThrown)
    {
      $('#f_saisie_deportee').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#bouton_choisir_saisie_deportee').prop('disabled',false);
      $('#msg_import').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide_saisie_deportee(responseJSON)
    {
      $('#f_saisie_deportee').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#bouton_choisir_saisie_deportee').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#msg_import').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        initialiser_compteur();
        $('#f_import_info').val(responseJSON['filename']);
        $('#msg_import').removeAttr('class').html('');
        $('#table_import_analyse').html(responseJSON['html']);
        $.fancybox( { href:'#zone_action_import' , modal:true , minHeight:300 } );
        $('#bouton_choisir_saisie_deportee').prop('disabled',false);
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du clic sur le bouton pour confirmer le traitement d’un import csv (saisie déportée)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_import').on
    (
      'click',
      '#valider_importer',
      function()
      {
        $('#zone_action_import button').prop('disabled',true);
        $('#ajax_msg_importer').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+'officiel_importer'+'&f_action='+'enregistrer_saisie_csv'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&f_import_info='+$('#f_import_info').val()+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg_importer').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              $('#zone_action_import button').prop('disabled',false);
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_importer').attr('class','alerte').html(responseJSON['value']);
                $('#zone_action_import button').prop('disabled',false);
              }
              else
              {
                $('#table_import_analyse').html('');
                $('#ajax_msg_importer').attr('class','valide').html(responseJSON['value']);
                $('#fermer_zone_importer').prop('disabled',false);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour fermer la zone action_eleve
    // Clic sur le bouton pour fermer la zone de choix des rubriques
    // Clic sur le bouton pour fermer la zone zone_action_classe
    // Clic sur le bouton pour fermer la zone zone_action_import
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      '#fermer_zone_action_eleve',
      function()
      {
        $('#zone_action_eleve').html('').hide(0);
        $('#zone_action_deport').hide(0);
        $('#msg_import').removeAttr('class').html('');
        $('#cadre_photo').hide(0);
        $('#cadre_statut , #table_accueil').show(0);
        $("#info_recalcul").html("La mise à jour des positionnements (recalcul automatique ou modification manuelle) peut dépendre de la configuration du bulletin.");
        return false;
      }
    );

    $('#fermer_zone_chx_rubriques').click
    (
      function()
      {
        $('#zone_chx_rubriques').hide(0);
        $('#cadre_statut , #table_accueil').show(0);
        $("#info_recalcul").html("La mise à jour des positionnements (recalcul automatique ou modification manuelle) peut dépendre de la configuration du bulletin.");
        return false;
      }
    );

    $('#fermer_zone_action_classe').click
    (
      function()
      {
        $('#zone_resultat_classe').html('');
        var colspan = (memo_objet=='imprimer') ? 3 : 2 ;
        $('#zone_'+memo_objet+' table tbody').html('<tr><td class="nu" colspan="'+colspan+'"></td></tr>');
        $('#zone_action_classe , #zone_imprimer , #zone_voir_archive, #zone_tableau').hide(0);
        $('#ajax_msg_imprimer , #ajax_msg_voir_archive , #ajax_msg_tableau').removeAttr('class').html('');
        $('#cadre_statut , #table_accueil').show(0);
        $("#info_recalcul").html("La mise à jour des positionnements (recalcul automatique ou modification manuelle) peut dépendre de la configuration du bulletin.");
        return false;
      }
    );

    $('#fermer_zone_importer').click
    (
      function()
      {
        $.fancybox.close();
        $('#ajax_msg_importer').removeAttr('class').html('');
        $('#zone_action_import button').prop('disabled',false);
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir|officiel_consulter] Navigation d’un élève à un autre
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function charger_nouvel_eleve(eleve_id,reload)
    {
      if( (eleve_id==memo_eleve) && (!reload) )
      {
        return false;
      }
      memoriser_details();
      $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',true);
      $('#zone_resultat_eleve').html('<label class="loader">En cours&hellip;</label>');
      $('#msg_import').removeAttr('class').html('');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'charger'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&f_user='+eleve_id+'&'+$('#form_hidden').serialize(),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#zone_resultat_eleve').html('<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>');
            $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
            if(responseJSON['statut']==false)
            {
              $('#zone_resultat_eleve').html('<label class="alerte">'+responseJSON['value']+'</label>');
            }
            else
            {
              $('#go_selection_eleve option[value='+eleve_id+']').prop('selected',true);
              catalogue_prenom = $('#go_selection_eleve option:selected').data('prenom');
              charger_photo_eleve(eleve_id,'maj');
              $('#zone_resultat_eleve').html(responseJSON['html']);
              if( typeof(responseJSON['script']) !== 'undefined' )
              {
                eval( responseJSON['script'] ); // ChartOptions
                graphique = new Highcharts.Chart(ChartOptions);
              }
              if(memo_auto_next || memo_auto_prev)
              {
                memo_auto_next = false;
                memo_auto_prev = false;
                $('#'+memo_rubrique_nom).find('button').click();
              }
              restituer_details(memo_eleve,eleve_id);
              memo_eleve = eleve_id;
              masquer_element_navigation_choix_eleve();
            }
          }
        }
      );
    }

    function masquer_element_navigation_choix_eleve()
    {
      $('#form_choix_eleve button').css('visibility','visible');
      if(memo_eleve==memo_eleve_first)
      {
        $('#go_premier_eleve , #go_precedent_eleve').css('visibility','hidden');
      }
      if(memo_eleve==memo_eleve_last)
      {
        $('#go_dernier_eleve , #go_suivant_eleve').css('visibility','hidden');
      }
    }

    $('#zone_action_eleve').on
    (
      'click',
      '#go_premier_eleve',
      function()
      {
        var eleve_id = $('#go_selection_eleve option:first').val();
        charger_nouvel_eleve(eleve_id,false);
      }
    );

    $('#zone_action_eleve').on
    (
      'click',
      '#go_dernier_eleve',
      function()
      {
        var eleve_id = $('#go_selection_eleve option:last').val();
        charger_nouvel_eleve(eleve_id,false);
      }
    );

    $('#zone_action_eleve').on
    (
      'click',
      '#go_precedent_eleve',
      function()
      {
        if( $('#go_selection_eleve option:selected').prev().length )
        {
          var eleve_id = $('#go_selection_eleve option:selected').prev().val();
          charger_nouvel_eleve(eleve_id,false);
        }
      }
    );

    $('#zone_action_eleve').on
    (
      'click',
      '#go_suivant_eleve',
      function()
      {
        if( $('#go_selection_eleve option:selected').next().length )
        {
          var eleve_id = $('#go_selection_eleve option:selected').next().val();
          charger_nouvel_eleve(eleve_id,false);
        }
      }
    );

    $('#zone_action_eleve').on
    (
      'change',
      '#go_selection_eleve',
      function()
      {
        var eleve_id = $('#go_selection_eleve option:selected').val();
        charger_nouvel_eleve(eleve_id,false);
      }
    );

    $('#zone_action_eleve').on
    (
      'click',
      '#change_mode',
      function()
      {
        if($('#f_mode').val()=='texte')
        {
          $('#change_mode').attr('class','texte').html('Interface détaillée');
          $('#f_mode').val('graphique');
        }
        else
        {
          $('#change_mode').attr('class','stats').html('Interface graphique');
          $('#f_mode').val('texte');
        }
        var eleve_id = $('#go_selection_eleve option:selected').val();
        charger_nouvel_eleve(eleve_id,true);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir] Clic sur le bouton pour afficher le formulaire "Saisie déportée"
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      '#saisir_deport',
      function()
      {
        $('#msg_import').removeAttr('class').html('');
        $.fancybox( '<label class="loader">'+'En cours&hellip;'+'</label>' );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+'officiel_importer'+'&f_action='+'generer_csv_vierge'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : 'Erreur 500&hellip; Mémoire insuffisante ? Demander à votre hébergeur d’augmenter la valeur "memory_limit".' ;
              $.fancybox( '<label class="alerte">'+message+'</label>' );
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                $('#export_file_saisie_deportee').attr('href', './force_download.php?fichier='+responseJSON['value'] );
                $.fancybox( { href:'#zone_action_deport' , minHeight:300 } );
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_archiver] Clic sur un lien pour archiver / imprimer des saisies
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_tableau button').click
    (
      function()
      {
        // On ne désactive pas tous les boutons pour ne pas perdre l’état déjà désactivé de certains lors de la réactivation de tous...
        $('#ajax_msg_tableau').attr('class','loader').html('En cours&hellip;');
        var f_action = $(this).attr('id');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+'officiel_archiver'+'&f_action='+f_action+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg_tableau').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_tableau').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg_tableau').removeAttr('class').html(responseJSON['value']);
              }
              // Afin de ne pas manquer l’affichage qui s’effectue en dessous...
              window.scrollTo(0,10000);
            }
          }
        );
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_consulter] Clic sur le bouton pour tester l’impression finale d’un bilan
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      '#simuler_impression',
      function()
      {
        $('#f_parite').val(0);
        $('#f_listing_eleves').val(memo_eleve);
        $.fancybox( '<label class="loader">'+'En cours&hellip;'+'</label>' );
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'imprimer'+'&f_etape='+'1'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : 'Erreur 500&hellip; Mémoire insuffisante ? Demander à votre hébergeur d’augmenter la valeur "memory_limit".' ;
              $.fancybox( '<label class="alerte">'+message+'</label>' );
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                $.fancybox( '<h3>Test impression PDF finale</h3><p class="astuce">Ce fichier comprend l’exemplaire archivé ainsi que le ou les exemplaires pour les responsables légaux.</p><div id="imprimer_liens"><ul class="puce"><li><a target="_blank" rel="noopener noreferrer" href="'+responseJSON['value']+'"><span class="file file_pdf">Récupérer le test d’impression du bilan officiel demandé.</span></a></li></ul></div>' );
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir] Clic sur le bouton pour ajouter une appréciation (une note ne s’ajoute pas, mais elle peut se modifier ou se recalculer si NULL)
    // [officiel_saisir] Clic sur le bouton pour modifier une note ou une saisie d’appréciation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_textarea_appreciation_ou_input_moyenne(obj_lieu,champ_contenu)
    {
      // fabriquer le formulaire textarea ou input
      if(memo_rubrique_type=='appr')
      {
        memo_html = obj_lieu.closest('td').html();
        memo_long_max = (memo_rubrique_id) ? window.tab_config[memo_config]['APP_RUBRIQUE_LONGUEUR'] : window.tab_config[memo_config]['APP_GENERALE_LONGUEUR'] ;
        var nb_lignes = entier(memo_long_max/100);
        // éventuellement formulaires de décision du conseil de classe
        var form_mention     = '' ;
        var form_engagement  = '' ;
        var form_orientation = '' ;
        if( !memo_rubrique_id && $('#div_mention').length )
        {
          var val = $('#div_mention').data('value');
          form_mention = '<div class="ti">Mention éventuelle : <select name="f_mention">'+$('#source_mention').html().replace('value="'+val+'"','value="'+val+'" selected')+'</select></div>';
        }
        if( !memo_rubrique_id && $('#div_engagement').length )
        {
          var val = $('#div_engagement').data('value');
          form_engagement = '<div class="ti">Engagement éventuel : <select name="f_engagement">'+$('#source_engagement').html().replace('value="'+val+'"','value="'+val+'" selected')+'</select></div>';
        }
        if( !memo_rubrique_id && $('#div_orientation').length )
        {
          var val = $('#div_orientation').data('value');
          form_orientation = '<div class="ti">Orientation éventuelle : <select name="f_orientation">'+$('#source_orientation').html().replace('value="'+val+'"','value="'+val+'" selected')+'</select></div>';
        }
        var formulaire_saisie = '<div class="ti"><b>Appréciation / Conseils pour progresser [ '+$('#go_selection_eleve option:selected').text()+' ] :</b></div>'
                              + '<div class="ti"><a href="?page=compte_catalogue_appreciations" target="_blank" class="no_puce"><q class="catalogue_editer"'+infobulle('Éditer le catalogue (ouvre un nouvel onglet).')+'></q></a><q class="catalogue_actualiser"'+infobulle('Actualiser la liste déroulante ci-contre.')+' data-id="catalogue_f_appreciation"></q> <select id="catalogue_f_appreciation" class="catalogue">'+$('#source_catalogue').html()+'</select></div>'
                              + '<div class="ti"><textarea id="f_appreciation" name="f_appreciation" rows="'+nb_lignes+'" cols="125"></textarea></div>'
                              + '<div class="ti"><label id="f_appreciation_reste"></label></div>'
                              + form_mention + form_engagement + form_orientation
                              + '<div class="ti"><button id="valider_appr_precedent" type="button" class="valider_prev">Précédent</button> <button id="valider_appr" type="button" class="valider">Valider</button> <button id="valider_appr_suivant" type="button" class="valider_next">Suivant</button> <button id="annuler_appr_precedent" type="button" class="annuler_prev">Précédent</button> <button id="annuler_appr" type="button" class="annuler">Annuler</button> <button id="annuler_appr_suivant" type="button" class="annuler_next">Suivant</button><label id="ajax_msg_appr"></label></div>';
      }
      if(memo_rubrique_type=='note')
      {
        memo_html = obj_lieu.closest('tr').html();
        var pourcent = (window.tab_config[memo_config]['CONVERSION_SUR_20']) ? '' : '%' ;
        var texte    = (window.tab_config[memo_config]['CONVERSION_SUR_20']) ? 'en note sur 20' : 'en pourcentage' ;
        var formulaire_saisie = '<div><b>Moyenne '+texte+' [ '+$('#go_selection_eleve option:selected').text()+' ] :</b> <input id="f_moyenne" name="f_moyenne" type="text" size="3" value="">'+pourcent+'</div>'
                              + '<div><button id="valider_note_precedent" type="button" class="valider_prev">Précédent</button> <button id="valider_note" type="button" class="valider">Valider</button> <button id="valider_note_suivant" type="button" class="valider_next">Suivant</button> <button id="annuler_note_precedent" type="button" class="annuler_prev">Précédent</button> <button id="annuler_note" type="button" class="annuler">Annuler</button> <button id="annuler_note_suivant" type="button" class="annuler_next">Suivant</button><label id="ajax_msg_note"></label></div>';
      }
      // modif affichage
      $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',true);
      obj_lieu.closest('td').html(formulaire_saisie);
      if(memo_eleve==memo_eleve_first)
      {
        $('#valider_'+memo_rubrique_type+'_precedent , #annuler_'+memo_rubrique_type+'_precedent').css('visibility','hidden');
      }
      if(memo_eleve==memo_eleve_last)
      {
        $('#valider_'+memo_rubrique_type+'_suivant , #annuler_'+memo_rubrique_type+'_suivant').css('visibility','hidden');
      }
      // finalisation (remplissage et focus)
      if(memo_rubrique_type=='appr')
      {
        // report d’une appréciation préremplie
        var is_report = (memo_rubrique_id) ? window.tab_config[memo_config]['APP_RUBRIQUE_REPORT'] : window.tab_config[memo_config]['APP_GENERALE_REPORT'] ;
        if( !champ_contenu && is_report )
        {
          champ_contenu = (memo_rubrique_id) ? window.tab_config[memo_config]['APP_RUBRIQUE_MODELE'] : window.tab_config[memo_config]['APP_GENERALE_MODELE'] ;
        }
        $('#f_appreciation').focus().html(champ_contenu);
        afficher_textarea_reste( $('#f_appreciation') , memo_long_max );
        window.scrollBy(0,100); // Pour avoir à l’écran les bouton de validation et d’annulation situés en dessous du textarea
      }
      if(memo_rubrique_type=='note')
      {
        var valeur = (window.tab_config[memo_config]['CONVERSION_SUR_20']) ? decimal(champ_contenu,1) : entier(champ_contenu.substring(0,champ_contenu.length-1)) ;
        valeur = (isNaN(valeur)) ? '' : valeur ;
        $('#f_moyenne').focus().val(valeur);
      }
    }

    $('#zone_action_eleve').on
    (
      'click',
      'button.ajouter , button.modifier',
      function()
      {
        memo_rubrique_nom  = $(this).closest('tr').attr('id');
        var tab_ids = memo_rubrique_nom.split('_');
        memo_rubrique_type = tab_ids[0]; // note | appr
        memo_rubrique_id   = entier(tab_ids[1]);
        if($(this).attr('class')=='modifier')
        {
          var contenu = (memo_rubrique_type=='appr') ? $(this).parent().next().html() : $(this).closest('td').prev().html() ;
        }
        else
        {
          var contenu = '' ;
        }
        afficher_textarea_appreciation_ou_input_moyenne( $(this) , contenu );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir|officiel_consulter] Indiquer le nombre de caractères restants autorisés dans le textarea
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // input permet d’intercepter à la fois les saisies au clavier et les copier-coller à la souris (clic droit)
    $('#zone_action_eleve').on( 'input' , '#f_appreciation' , function() { afficher_textarea_reste( $(this) , memo_long_max ); } );
    $('#zone_action_eleve').on( 'input' , 'textarea[name=f_saisie_express]' , function() { afficher_textarea_reste($(this), window.tab_config[memo_config]['APP_RUBRIQUE_LONGUEUR'] ); } );
    $('#section_corriger' ).on( 'input' , '#f_appreciation' , function() { afficher_textarea_reste( $(this) , memo_long_max ); } );
    $('#section_signaler' ).on( 'input' , '#f_message_contenu' , function() { afficher_textarea_reste( $(this) , nb_caracteres_max ); } );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir] Clic sur un bouton pour annuler une saisie de note ou d’appréciation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      '#annuler_appr , #annuler_appr_suivant , #annuler_appr_precedent , #annuler_note , #annuler_note_suivant , #annuler_note_precedent',
      function()
      {
        if(memo_rubrique_type=='appr')
        {
          $(this).closest('td').html(memo_html);
        }
        else if(memo_rubrique_type=='note')
        {
          $(this).closest('tr').html(memo_html);
        }
        $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
        memo_auto_next = ($(this).attr('id')=='annuler_'+memo_rubrique_type+'_suivant')   ? true : false ;
        memo_auto_prev = ($(this).attr('id')=='annuler_'+memo_rubrique_type+'_precedent') ? true : false ;
        if(memo_auto_next) { $('#go_suivant_eleve').click(); }
        if(memo_auto_prev) { $('#go_precedent_eleve').click(); }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir_multiple] Focus dans un champ de saisie express
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'focus',
      'textarea[name=f_saisie_express] , input[name=f_saisie_express]',
      function()
      {
        var eleve_id = $(this).parent().parent().data('user_id');
        if( eleve_id != memo_eleve )
        {
          memo_eleve = eleve_id;
          charger_photo_eleve(memo_eleve,'maj');
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir_multiple] Perte du focus ou glisser-déposer dans un champ de saisie express -> enregistrement
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'blur', // intercepte les saisies au clavier ainsi que les copier-coller à la souris
      'textarea[name=f_saisie_express] , input[name=f_saisie_express]',
      function()
      {
        enregistrement_automatique( $(this) );
      }
    );

    $('#zone_action_eleve').on
    (
      'mouseout', // déclencher sur drop pour un glisser-déposer ne fonctionne pas car le contenu n’est alors pas encore mis à jour
      'textarea[name=f_saisie_express] , input[name=f_saisie_express]',
      function()
      {
        enregistrement_automatique( $(this) );
      }
    );

    function enregistrement_automatique(obj_dom)
    {
      var obj_id      = obj_dom.attr('id');
      var rubrique_id = obj_dom.parent().data('rubrique_id');
      var eleve_id    = obj_dom.parent().parent().data('user_id');
      var nature      = obj_id.substring(0,4); // appr| note
      var contenu     = obj_dom.val().trim();
      var f_action    = (contenu.length) ? 'enregistrer_'+nature : 'supprimer_'+nature ;
      if( contenu && ( nature == 'note' ) )
      {
        contenu = (window.tab_config[memo_config]['CONVERSION_SUR_20']) ? decimal(contenu,1) : entier(contenu) ;
        if( isNaN(contenu) )
        {
          obj_dom.attr('class','invalide');
          $.fancybox( '<label class="alerte">Saisie incorrecte !</label>' );
          return false;
        }
        if( (contenu<0) || ((contenu>window.MOYENNE_MAXI)&&(window.tab_config[memo_config]['CONVERSION_SUR_20'])) || ((contenu>window.POURCENTAGE_MAXI)&&(!window.tab_config[memo_config]['CONVERSION_SUR_20'])) )
        {
          obj_dom.attr('class','invalide');
          $.fancybox( '<label class="alerte">Valeur incorrecte !</label>' );
          return false;
        }
      }
      if( contenu != memo_saisie[obj_id] )
      {
        memo_saisie[obj_id] = contenu;
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+f_action+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&f_user='+eleve_id+'&f_rubrique='+rubrique_id+'&'+$('#form_hidden').serialize()+'&f_'+nature+'='+encodeURIComponent(contenu),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              obj_dom.attr('class','invalide');
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                obj_dom.attr('class','invalide');
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                if( nature == 'note' )
                {
                  obj_dom.val(contenu);
                }
                obj_dom.attr('class','valide');
                setTimeout( function(){obj_dom.removeAttr('class')}, 1000);
              }
            }
          }
        );
      }
      else if ( nature == 'note' )
      {
        obj_dom.val(contenu);
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir] Clic sur un bouton pour valider une saisie de note ou d’appréciation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      '#valider_appr , #valider_appr_suivant , #valider_appr_precedent , #valider_note , #valider_note_suivant , #valider_note_precedent',
      function()
      {
        if(memo_rubrique_type=='appr')
        {
          if( !$('#f_appreciation').val().trim().length )
          {
            $('#ajax_msg_'+memo_rubrique_type).attr('class','erreur').html('Absence d’appréciation !');
            $('#f_appreciation').focus();
            return false;
          }
        }
        if(memo_rubrique_type=='note')
        {
          var note = (window.tab_config[memo_config]['CONVERSION_SUR_20']) ? decimal($('#f_moyenne').val(),1) : entier($('#f_moyenne').val()) ;
          if( isNaN(note) )
          {
            $('#ajax_msg_'+memo_rubrique_type).attr('class','erreur').html('Saisie incorrecte !');
            $('#f_moyenne').focus();
            return false;
          }
          if( (note<0) || ((note>window.MOYENNE_MAXI)&&(window.tab_config[memo_config]['CONVERSION_SUR_20'])) || ((note>window.POURCENTAGE_MAXI)&&(!window.tab_config[memo_config]['CONVERSION_SUR_20'])) )
          {
            $('#ajax_msg_'+memo_rubrique_type).attr('class','erreur').html('Valeur incorrecte !');
            $('#f_moyenne').focus();
            return false;
          }
        }
        memo_auto_next = ($(this).attr('id')=='valider_'+memo_rubrique_type+'_suivant')   ? true : false ;
        memo_auto_prev = ($(this).attr('id')=='valider_'+memo_rubrique_type+'_precedent') ? true : false ;
        $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',true);
        $('#ajax_msg_'+memo_rubrique_type).attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'enregistrer_'+memo_rubrique_type+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&f_user='+memo_eleve+'&f_rubrique='+memo_rubrique_id+'&'+$('#form_hidden').serialize()+'&'+$('#zone_resultat_eleve').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg_'+memo_rubrique_type).attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_'+memo_rubrique_type).attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                if(memo_rubrique_type=='appr')
                {
                  $('#ajax_msg_'+memo_rubrique_type).closest('td').html(responseJSON['value']);
                }
                else if(memo_rubrique_type=='note')
                {
                  $('#ajax_msg_'+memo_rubrique_type).closest('tr').html(responseJSON['value']);
                }
                if(memo_auto_next) { $('#go_suivant_eleve').click(); }
                if(memo_auto_prev) { $('#go_precedent_eleve').click(); }
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir] Clic sur le bouton pour supprimer une saisie de note ou d’appréciation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      'button.supprimer',
      function()
      {
        var obj_bouton = $(this);
        memo_rubrique_nom  = $(this).closest('tr').attr('id');
        var tab_ids = memo_rubrique_nom.split('_');
        memo_rubrique_type = tab_ids[0]; // note | appr
        memo_rubrique_id   = entier(tab_ids[1]);
        $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',true);
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'supprimer_'+memo_rubrique_type+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&f_user='+memo_eleve+'&f_rubrique='+memo_rubrique_id+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+' Veuillez recommencer.'+'</label>' );
              $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                if(memo_rubrique_type=='appr')
                {
                  obj_bouton.closest('td').html(responseJSON['value']);
                }
                else if(memo_rubrique_type=='note')
                {
                  obj_bouton.closest('tr').html(responseJSON['value']);
                }
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir] Clic sur le bouton pour recalculer une note (soit effacée - NULL - soit figée car reportée manuellement)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      'button.nettoyer',
      function()
      {
        var obj_bouton = $(this);
        memo_rubrique_nom  = $(this).closest('tr').attr('id');
        var tab_ids = memo_rubrique_nom.split('_');
        memo_rubrique_type = tab_ids[0]; // note | appr
        memo_rubrique_id   = entier(tab_ids[1]);
        $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',true);
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'recalculer_note'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&f_user='+memo_eleve+'&f_rubrique='+memo_rubrique_id+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+' Veuillez recommencer.'+'</label>' );
              $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_choix_eleve button , #form_choix_eleve select , #zone_resultat_eleve button').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              }
              else
              {
                obj_bouton.closest('tr').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_examiner] Charger le contenu (résultat de l’examen pour une classe)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#lancer_recherche').click
    (
      function()
      {
        var listing_id = []; $('#zone_chx_rubriques input[type=checkbox]:enabled:checked').each(function(){listing_id.push($(this).val());});
        if(!listing_id.length)
        {
          $('#ajax_msg_recherche').attr('class','erreur').html('Aucune rubrique cochée !');
          return false;
        }
        $('#f_listing_rubriques').val(listing_id);
        $('#zone_chx_rubriques button').prop('disabled',true);
        $('#ajax_msg_recherche').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : 'Erreur 500&hellip; Mémoire insuffisante ? Sélectionner moins d’élèves à la fois ou demander à votre hébergeur d’augmenter la valeur "memory_limit".' ;
              $('#ajax_msg_recherche').attr('class','alerte').html(message);
              $('#zone_chx_rubriques button').prop('disabled',false);
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#zone_chx_rubriques button').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_recherche').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                configurer_form_choix_classe();
                masquer_element_navigation_choix_classe();
                $('#ajax_msg_recherche').removeAttr('class').html('');
                $('#report_periode').html( $('#periode_'+memo_periode).text()+' :' );
                $('#zone_resultat_classe').html(responseJSON['value']);
                $('#zone_chx_rubriques').hide(0);
                $('#zone_action_classe').show(0);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_imprimer] Lancer l’impression pour une liste d’élèves
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function imprimer(etape)
    {
      $('#ajax_msg_imprimer').attr('class','loader').html('En cours&hellip; Étape '+etape+'/4.');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'imprimer'+'&f_etape='+etape+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : ( (etape==1) ? 'Erreur 500&hellip; Mémoire insuffisante ? Sélectionner moins d’élèves à la fois ou demander à votre hébergeur d’augmenter la valeur "memory_limit".' : 'Erreur 500&hellip; Temps alloué insuffisant ? Sélectionner moins d’élèves à la fois ou demander à votre hébergeur d’augmenter la valeur "max_execution_time".' ) ;
            $('#ajax_msg_imprimer').attr('class','alerte').html(message);
            $('#form_choix_classe button , #form_choix_classe select , #valider_imprimer').prop('disabled',false);
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==false)
            {
              $('#form_choix_classe button , #form_choix_classe select , #valider_imprimer').prop('disabled',false);
              $('#ajax_msg_imprimer').attr('class','alerte').html(responseJSON['value']);
            }
            else if(etape<4)
            {
              etape++;
              imprimer(etape);
            }
            else
            {
              $('#form_choix_classe button , #form_choix_classe select , #valider_imprimer').prop('disabled',false);
              var tab_listing_id = $('#f_listing_eleves').val().split(',');
              for ( var key in tab_listing_id )
              {
                $('#id_'+tab_listing_id[key]).children('td:first').children('input').prop('checked',false);
                $('#id_'+tab_listing_id[key]).children('td:last').html('Oui, le '+window.TODAY_FR);
              }
              $('#ajax_msg_imprimer').removeAttr('class').html('');
              $.fancybox( '<h3>Bilans PDF imprimés</h3>'+'<p class="danger">Archivez ces documents : seul l’exemplaire générique sans le bloc adresse est conservé par <em>SACoche</em> !</p>'+'<div id="imprimer_liens">'+responseJSON['value']+'</div>' );
            }
          }
        }
      );
    }

    $('#valider_imprimer').click
    (
      function()
      {
        var listing_id = []; $('#form_choix_eleves input[type=checkbox]:checked').each(function(){listing_id.push($(this).val());});
        if(!listing_id.length)
        {
          $('#ajax_msg_imprimer').attr('class','erreur').html('Aucun élève coché !');
          return false;
        }
        var date_visible_sql = '';
        if( window.DROIT_VOIR_ARCHIVE.indexOf(',TUT,') !== -1 )
        {
          var date_saisie_fr = $('#f_date_saisie').val();
          date_visible_sql = test_dateITA( date_saisie_fr , true );
          if( date_visible_sql == false )
          {
            $('#ajax_msg_imprimer').attr('class','erreur').html('Date d’accès / visibilité incorrecte !');
            return false;
          }
          if( date_visible_sql < window.TODAY_SQL )
          {
            $('#ajax_msg_imprimer').attr('class','erreur').html('Date d’accès / visibilité antérieure à aujourd’hui !');
            return false;
          }
          if( ( date_saisie_fr != window.tab_config[memo_config]['DATE_VISIBILITE'] ) && ( date_visible_sql > window.DAY_SQL_MAX ) )
          {
            $('#ajax_msg_imprimer').attr('class','erreur').html('Date d’accès / visibilité trop lointaine !');
            return false;
          }
        }
        $('#f_listing_eleves').val(listing_id);
        var notification = ($('#f_envoi_notif_parent').is(':checked')) ? 1 : 0 ;
        $('#f_notification').val(notification);
        $('#f_date_visible').val(date_visible_sql);
        var parite = $('#check_parite').is(':checked') ? 1 : 0 ;
        $('#f_parite').val(parite);
        $('#form_choix_classe button , #form_choix_classe select , #valider_imprimer').prop('disabled',true);
        imprimer(1);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_imprimer] Charger la liste de choix des élèves, avec info si les bulletins sont déjà imprimés
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function charger_formulaire_imprimer()
    {
      // Start - § info_visibilite_et_notification
      if(window.DROIT_VOIR_ARCHIVE.indexOf(',TUT,')==-1)
      {
        var message = (window.PROFIL_TYPE=='administrateur')
          ? '<label class="alerte">Pas de notifications par courriel aux parents car ils n’ont pas <a href="index.php?page=administrateur_etabl_autorisations">l’autorisation d’accéder aux archives</a>.</label>'
          : '<label class="alerte">Pas de notifications par courriel aux parents car ils n’ont pas l’autorisation d’accéder aux archives (<a class="pop_up" href="'+window.SERVEUR_DOCUMENTAIRE+'?fichier=support_administrateur__gestion_autorisations#toggle_bilans_officiels">DOC</a></span>).</label>'
          ;
      }
      else
      {
        if(!window.tab_config[memo_config]['DELAI_CONSULTATION_FAMILLE'])
        {
          var precision_delai = 'immédiatement';
          var precision_envoi = 'seront envoyés dès aujourd’hui';
        }
        else if(window.tab_config[memo_config]['DELAI_CONSULTATION_FAMILLE']==1)
        {
          var precision_delai = 'demain';
          var precision_envoi = 'seront envoyés à ce moment là';
        }
        else
        {
          var precision_delai = 'dans '+window.tab_config[memo_config]['DELAI_CONSULTATION_FAMILLE']+' jours (après l’impression)';
          var precision_envoi = 'seront envoyés à ce moment là';
        }
        if(window.COURRIEL_NOTIFICATION=='non')
        {
          var message = '<div class="danger">Le webmestre du serveur a désactivé l’envoi des notifications par courriel.</div>'
                      + '<div class="astuce">Le bilan est configuré pour être visible des familles '+precision_delai+', sans limite de durée.</div>';
        }
        else if(!window.tab_config[memo_config]['ENVOI_MAIL_PARENT'])
        {
          var message = '<div class="danger">Pas de <a href="index.php?page=officiel&amp;section=reglages_configuration">notifications par courriel aux parents</a> imposées (seuls seront notifiés ceux qui l’ont paramétré).</div>'
                      + '<div class="astuce">Ce bilan est configuré pour être visible des familles '+precision_delai+', sans limite de durée ; les éventuels courriels '+precision_envoi+'.</div>';
        }
        else
        {
          var message = '<label for="f_envoi_notif_parent"><input type="checkbox" id="f_envoi_notif_parent" name="f_envoi_notif_parent" value="1" checked> Envoyer aux parents un courriel avec un lien permettant de récupérer le bilan généré (si leur courriel est connu et fonctionnel).</label><br>'
                      + '<div class="astuce">Ce bilan est configuré pour être visible des familles '+precision_delai+', sans limite de durée ; les courriels '+precision_envoi+'.</div>';
        }
      }
        message += '<ul class="puce"><li>Si souhaité, vous pouvez ajuster la date d’accès / visibilité des familles (appliquée si nouvelle impression) : <input type="text" id="f_date_saisie" value="'+window.tab_config[memo_config]['DATE_VISIBILITE']+'" size="8" maxlength="10">.</li></ul>';
      $('#info_visibilite_et_notification').html(message);
      // End - § info_visibilite_et_notification
      var colspan = (memo_objet=='imprimer') ? 3 : 2 ;
      $('#zone_'+memo_objet+' table tbody').html('<tr><td class="nu" colspan="'+colspan+'"></td></tr>');
      $('#zone_voir_archive table tbody').html('<tr><td class="nu" colspan="2"></td></tr>');
      $('#form_choix_classe button , #form_choix_classe select , #valider_imprimer').prop('disabled',true);
      $('#ajax_msg_'+memo_objet).attr('class','loader').html('En cours&hellip;');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_action='+'initialiser'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_msg_'+memo_objet).attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            $('#form_choix_classe button , #form_choix_classe select').prop('disabled',false);
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==false)
            {
              $('#ajax_msg_'+memo_objet).attr('class','alerte').html(responseJSON['value']);
              $('#form_choix_classe button , #form_choix_classe select').prop('disabled',false);
            }
            else
            {
              masquer_element_navigation_choix_classe();
              $('#zone_'+memo_objet+' table tbody').html(responseJSON['value']);
              $('#ajax_msg_'+memo_objet).removeAttr('class').html('');
              $('#form_choix_classe button , #form_choix_classe select , #valider_imprimer').prop('disabled',false);
              if(memo_objet=='imprimer')
              {
                $("#info_recalcul").html( window.tab_config[memo_config]['info_recalcul'] );
              }
            }
          }
        }
      );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_archiver] Afficher / Masquer les éléments
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function tableaux_visibilite()
    {
      var is_bulletin_avec_moyenne = ( (window.BILAN_TYPE=='bulletin') && window.tab_config[memo_config]['MOYENNE_SCORES'] ) ? true : false ;
      $('#imprimer_donnees_eleves_prof').prop('disabled', window.PROFIL_TYPE=='administrateur' );
      $('#imprimer_donnees_eleves_syntheses').parent().hideshow( window.tab_config[memo_config]['APP_GENERALE_LONGUEUR'] );
      $('#imprimer_donnees_eleves_positionnements').parent().hideshow(is_bulletin_avec_moyenne);
      $('#imprimer_donnees_eleves_recapitulatif').parent().hideshow(is_bulletin_avec_moyenne);
      $('#imprimer_donnees_eleves_mentions').parent().hideshow( window.tab_config[memo_config]['DECISION_MENTION'] );
      $('#ajax_msg_tableau').removeAttr('class').html('');
      masquer_element_navigation_choix_classe();
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_examiner|officiel_imprimer|officiel_archiver] Actualiser l’état enabled/disabled des options du formulaire de navigation dans les classes, masquer les boutons de navigation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function masquer_element_navigation_choix_classe()
    {
      $('#go_selection_classe option[value='+memo_classe+'_'+memo_groupe+']').prop('selected',true);
      $('#form_choix_classe button').css('visibility','visible');
      if( memo_classe+'_'+memo_groupe == memo_classe_first )
      {
        $('#go_precedent_classe').css('visibility','hidden');
      }
      if( memo_classe+'_'+memo_groupe == memo_classe_last )
      {
        $('#go_suivant_classe').css('visibility','hidden');
      }
    }

    // La recherche de la bonne option après appui sur "classe précédente" ou "classe suivante" n’est pas évident à cause des options désactivées.
    // D'où la mise en place de deux tableaux supplémentaires :
    var tab_id_option_to_numero = [];
    var tab_numero_to_id_option = [];

    function configurer_form_choix_classe()
    {
      var numero = 0;
      if(memo_section=='officiel_examiner')
      {
        var indice = 'examiner';
      }
      else if(memo_section=='officiel_archiver')
      {
        var indice = 'archiver';
      }
      else
      {
        var indice = (memo_objet=='imprimer') ? 'imprimer' : 'voir_pdf' ;
      }
      $('#go_selection_classe option').each
      (
        function()
        {
          var id_option = $(this).val();
          var etat = window.tab_disabled[indice][id_option+'_'+memo_periode] ? true : false ;
          $(this).prop( 'disabled' , etat );
          if(etat==false)
          {
            numero++;
            tab_id_option_to_numero[id_option] = [numero];
            tab_numero_to_id_option[numero] = [id_option];
          }
        }
      );
      memo_classe_first = tab_numero_to_id_option[1];
      memo_classe_last = tab_numero_to_id_option[numero];
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_examiner|officiel_imprimer|officiel_archiver] Navigation d’une classe à une autre
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function charger_nouvelle_classe(classe_groupe_id)
    {
      if( classe_groupe_id == memo_classe+'_'+memo_groupe )
      {
        return false;
      }
      var tab_indices = classe_groupe_id.toString().split('_'); // Sans toString() on obtient "error: split is not a function"
      memo_classe = tab_indices[0];
      memo_groupe = tab_indices[1];
      memo_config = window.tab_classe_config_ref[memo_classe];
      if(memo_section=='officiel_imprimer')
      {
        charger_formulaire_imprimer();
      }
      else if(memo_section=='officiel_archiver')
      {
        tableaux_visibilite();
      }
      else if(memo_section=='officiel_examiner')
      {
        $('#form_choix_classe button , #form_choix_classe select').prop('disabled',true);
        $('#zone_resultat_classe').html('<label class="loader">En cours&hellip;</label>');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_section='+memo_section+'&f_page='+window.BILAN_TYPE+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&'+$('#form_hidden').serialize(),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#zone_resultat_classe').html('<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>');
              $('#form_choix_classe button , #form_choix_classe select').prop('disabled',false);
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_choix_classe button , #form_choix_classe select').prop('disabled',false);
              if(responseJSON['statut']==false)
              {
                $('#zone_resultat_classe').html('<label class="alerte">'+responseJSON['value']+'</label>');
              }
              else
              {
                masquer_element_navigation_choix_classe();
                $('#zone_resultat_classe').html(responseJSON['value']);
              }
            }
          }
        );
      }
    }

    $('#go_precedent_classe').click
    (
      function()
      {
        var id_option = $('#go_selection_classe option:selected').val();
        var numero = tab_id_option_to_numero[id_option];
        numero--;
        if( tab_numero_to_id_option[numero].length )
        {
          charger_nouvelle_classe( tab_numero_to_id_option[numero] );
        }
      }
    );

    $('#go_suivant_classe').click
    (
      function()
      {
        var id_option = $('#go_selection_classe option:selected').val();
        var numero = tab_id_option_to_numero[id_option];
        numero++;
        if( tab_numero_to_id_option[numero].length )
        {
          charger_nouvelle_classe( tab_numero_to_id_option[numero] );
        }
      }
    );

    $('#go_selection_classe').change
    (
      function()
      {
        var classe_groupe_id = $('#go_selection_classe option:selected').val();
        charger_nouvelle_classe(classe_groupe_id);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir|officiel_consulter] Afficher le formulaire pour signaler ou corriger une faute
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#zone_action_eleve').on
    (
      'click',
      'button.signaler , button.corriger',
      function()
      {
        var objet        = $(this).attr('class');
        var tab_ids      = $(this).closest('tr').attr('id').split('_');
        var prof_id      = entier(tab_ids[2]);
        memo_rubrique_id = entier(tab_ids[1]);
        // Préparation de l’affichage
        $('#f_action').val(objet+'_faute');
        $('#zone_signaler_corriger h2').html(objet[0].toUpperCase() + objet.substring(1) + ' une faute');
        var appreciation_contenu = $(this).parent().parent().children('.appreciation').text(); // dans un div ou un span selon les cas
        var message_contenu = $('h1').text()+' - '+$('#periode_'+memo_periode).text()+' - '+$('#groupe_'+memo_classe+'_'+memo_groupe).text()+'\n\n'+'Concernant '+$('#go_selection_eleve option:selected').text();
        $('#f_destinataire_id').val(prof_id);
        $('#f_appreciation_old').val(appreciation_contenu);
        if(objet=='corriger')
        {
          $('#section_signaler').hide(0);
          $('#section_corriger').show(0);
          message_contenu += ' je me suis permis de corriger l’appréciation saisie.';
          memo_long_max = (memo_rubrique_id) ? window.tab_config[memo_config]['APP_RUBRIQUE_LONGUEUR'] : window.tab_config[memo_config]['APP_GENERALE_LONGUEUR'] ;
          var nb_lignes = entier(memo_long_max/100);
          $('#f_message_contenu').val(message_contenu);
          $('#f_appreciation').focus().val(appreciation_contenu).attr('rows',nb_lignes);
          afficher_textarea_reste( $('#f_appreciation') , memo_long_max );
        }
        else if(objet=='signaler')
        {
          $('#section_corriger').hide(0);
          $('#section_signaler').show(0);
          message_contenu += ' je pense qu’il y a un souci dans l’appréciation : SAISIR_EXPLICATION_ICI';
          $('#f_message_contenu').focus().val(message_contenu);
          afficher_textarea_reste( $('#f_message_contenu') , nb_caracteres_max );
        }
        // Afficher la zone
        $.fancybox( { href:'#zone_signaler_corriger' , modal:true } );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir|officiel_consulter] Clic sur le bouton pour fermer le cadre de rédaction d’un signalement d’une faute (annuler / retour)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#annuler_signaler_corriger').click
    (
      function()
      {
        $('#ajax_msg_signaler_corriger').removeAttr('class').html('');
        $.fancybox.close();
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // [officiel_saisir|officiel_consulter] Valider le formulaire pour signaler ou corriger une faute
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#valider_signaler_corriger').click
    (
      function()
      {
        var action  = $('#f_action').val();
        var prof_id = $('#f_destinataire_id').val();
        // On vérifie que le collègue a renseigné les textes à modifier
        if(action=='signaler_faute')
        {
          var message_contenu = $('#f_message_contenu').val();
          if( message_contenu.indexOf('SAISIR_EXPLICATION_ICI') != -1 )
          {
            $('#ajax_msg_signaler_corriger').attr('class','erreur').html('Remplacez "SAISIR_EXPLICATION_ICI" par votre explication !');
            return false;
          }
        }
        // ok, on y va
        $('#zone_signaler_corriger button').prop('disabled',true);
        $('#ajax_msg_signaler_corriger').attr('class','loader').html('En cours&hellip;');
        // Signaler la faute (signalement simple, ou signalement d’une correction)
        if( prof_id != window.USER_ID )
        {
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page='+window.PAGE,
              data : 'csrf='+window.CSRF+'&'+$('#zone_signaler_corriger').serialize(),
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#ajax_msg_signaler_corriger').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                $('#zone_signaler_corriger button').prop('disabled',false);
                return false;
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                $('#zone_signaler_corriger button').prop('disabled',false);
                if(responseJSON['statut']==false)
                {
                  $('#ajax_msg_signaler_corriger').attr('class','alerte').html(responseJSON['value']);
                  return false;
                }
                else if(action=='signaler_faute')
                {
                  $('#ajax_msg_signaler_corriger').removeAttr('class').html('');
                  $('#annuler_signaler_corriger').click();
                  return false;
                }
                else if(action=='corriger_faute')
                {
                  // Corriger la faute
                  corriger_faute(prof_id);
                }
              }
            }
          );
        }
        else
        {
          // Ici il ne peut s'agir que d'un prof qui corrige sa propre appréciation
          corriger_faute(prof_id);
        }
      }
    );

    function corriger_faute(prof_id)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_section='+'officiel_saisir'+'&f_bilan_type='+window.BILAN_TYPE+'&f_classe='+memo_classe+'&f_groupe='+memo_groupe+'&f_periode='+memo_periode+'&f_user='+memo_eleve+'&f_rubrique='+memo_rubrique_id+'&f_prof='+prof_id+'&'+$('#form_hidden').serialize()+'&'+$('#zone_signaler_corriger').serialize(),
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_msg_signaler_corriger').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            $('#zone_signaler_corriger button').prop('disabled',false);
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('#zone_signaler_corriger button').prop('disabled',false);
            if(responseJSON['statut']==false)
            {
              $('#ajax_msg_signaler_corriger').attr('class','alerte').html(responseJSON['value']);
              return false;
            }
            else
            {
              $('#appr_'+memo_rubrique_id+'_'+prof_id).find('.appreciation').html(responseJSON['value']); // dans un div ou un span selon les cas
              $('#ajax_msg_signaler_corriger').removeAttr('class').html('');
              $('#annuler_signaler_corriger').click();
            }
          }
        }
      );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Voir / masquer tous les détails ou seulement certains
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function montrer_details()
    {
      $('#zone_resultat_eleve').find('a.toggle_plus').click();
      $('#montrer_details').replaceWith('<a href="#" id="masquer_details">tout masquer</a>');
      return false;
    }

    function masquer_details()
    {
      $('#zone_resultat_eleve').find('a.toggle_moins').click();
      $('#masquer_details').replaceWith('<a href="#" id="montrer_details">tout montrer</a>');
      return false;
    }

    $('#zone_action_eleve').on
    (
      'click',
      '#montrer_details',
      function()
      {
        montrer_details();
      }
    );

    $('#zone_action_eleve').on
    (
      'click',
      '#masquer_details',
      function()
      {
        masquer_details();
      }
    );

    function memoriser_details()
    {
      memo_details = [];
      $('#zone_resultat_eleve').find('a.toggle_moins').each
      (
        function()
        {
          memo_details.push( $(this).attr('id') );
          console.log('memo '+$(this).attr('id'));
        }
      );
    }

    function restituer_details(eleve_id_old,eleve_id_new)
    {
      memo_details.forEach
      (
        function(id)
        {
          id = id.replace('to_'+eleve_id_old+'_', 'to_'+eleve_id_new+'_');
          $('#'+id).click();
        }
      );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Élement saisissable / déplaçable
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $( '#cadre_statut' ).draggable({cursor:'move'});

  }
);
