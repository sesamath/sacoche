/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Lister les établissements affectés par le bug d’initialisation annuelle 2017-2018
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#lister').click
    (
      function()
      {
        $('#ajax_lister').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'lister',
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_lister').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $('#ajax_lister').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                $('#form_lister').html('<table><tbody>'+responseJSON['value']+'</tbody></table>');
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Retourner les infos complémentaires pour un établissement donné
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_lister').on(
      'click' ,
      'q.modifier' ,
      function()
      {
        var obj_tr  = $(this).parent().parent();
        var base_id = obj_tr.find('td').eq(0).html();
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'renseigner'+'&f_base_id='+base_id,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              log('info',afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                log('info',responseJSON['value']);
              }
              else
              {
                obj_tr.html(responseJSON['value']);
                $('#f_base_id').val(base_id);
                $('#f_date_sql').val( obj_tr.find('td').eq(1).html().substring(0,8) + '01' );
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Application de requêtes correctives sur une base
    // Upload d’un fichier (avec jquery.form.js)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire_sql = $('#form_sql');

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions_sql =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_sql',
      error : retour_form_erreur_sql,
      success : retour_form_valide_sql
    };

    // Vérifications précédant l’envoi du formulaire, déclenchées au choix d’un fichier
    $('#f_sql').change
    (
      function()
      {
        var file = this.files[0];
        if( typeof(file) == 'undefined' )
        {
          $('#ajax_msg_sql').removeAttr('class').html('');
          return false;
        }
        else
        {
          var fichier_nom = file.name;
          var fichier_ext = fichier_nom.split('.').pop().toLowerCase();
          if( '.sql.zip.'.indexOf('.'+fichier_ext+'.') == -1 )
          {
            $('#ajax_msg_sql').attr('class','erreur').html('Le fichier "'+escapeHtml(fichier_nom)+'" n’a pas une extension autorisée (sql zip).');
            return false;
          }
          else
          {
            $('#ajax_msg_sql').attr('class','loader').html('En cours&hellip;');
            formulaire_sql.submit();
          }
        }
      }
    );

    // Envoi du formulaire (avec jquery.form.js)
    formulaire_sql.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions_sql);
        return false;
      }
    );

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur_sql(jqXHR, textStatus, errorThrown)
    {
      $('#f_sql').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#bouton_choisir_sql').prop('disabled',false);
      $('#ajax_msg_sql').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide_sql(responseJSON)
    {
      $('#f_sql').clearFields(); // Sinon si on fournit de nouveau un fichier de même nom alors l’événement change() ne se déclenche pas
      $('#bouton_choisir_sql').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_sql').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        initialiser_compteur();
        $('#ajax_msg_sql').attr('class','valide').html('Fichier uploadé, lancement des requêtes...');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'appliquer'+'&f_base_id='+$('#f_base_id').val()+'&f_date_sql='+$('#f_date_sql').val()+'&f_filename='+responseJSON['value'],
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg_sql').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              if(responseJSON['statut']==false)
              {
                $('#ajax_msg_sql').attr('class','alerte').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg_sql').attr('class','valide').html('ok');
              }
            }
          }
        );
      }
    }

  }
);
