<?php
if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = 'Patch...';
?>

<h2>Établissements concernés par le bug d’initialisation annuelle 2017-2018</h2>
<form action="#" method="post" id="form_lister" class="p">
  <button type="button" id="lister">Lister</button><label id="ajax_lister">&nbsp;</label>
</form>

<hr>

<h2>Application de requêtes correctives sur une base</h2>
<form action="#" method="post" id="form_sql" class="p">
  <p><label class="tab" for="f_base_id">Id etabl :</label><input id="f_base_id" name="f_base_id" size="8" type="int" value=""></p>
  <p><label class="tab" for="f_date_sql">Date svg :</label><input id="f_date_sql" name="f_date_sql" size="10" type="text" value="AAAA-MM-JJ"></p>
  <p><label class="tab" for="f_sql">Fichier :</label><input type="hidden" name="f_action" value="uploader"><input id="f_sql" type="file" name="userfile"><button id="bouton_choisir_fichier" type="button" class="fichier_import">Parcourir...</button><label id="ajax_msg_sql">&nbsp;</label></p>
</form>
