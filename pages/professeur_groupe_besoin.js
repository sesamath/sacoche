/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialisation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    var mode = false;

    // tri du tableau (avec jquery.tablesorter.js).
    $('#table_action').tablesorter({ sortLocaleCompare : true, headers:{2:{sorter:false},3:{sorter:false},4:{sorter:false}} });
    var tableau_tri = function(){ $('#table_action').trigger( 'sorton' , [ [[0,0],[1,0]] ] ); };
    var tableau_maj = function(){ $('#table_action').trigger( 'update' , [ true ] ); };
    tableau_tri();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonctions utilisées
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_form_gestion( mode , id , niveau_nom , nom , eleve_nombre , eleve_liste , prof_nombre , prof_liste )
    {
      $('#f_action').val(mode);
      $('#f_id').val(id);
      $('#f_niveau').html(window.select_niveau.replace('>'+niveau_nom,' selected>'+niveau_nom));
      $('#f_nom').val(nom);
      $('#f_eleve_nombre').val(eleve_nombre);
      $('#f_eleve_liste').val(eleve_liste);
      $('#f_prof_nombre').val(prof_nombre);
      $('#f_prof_liste').val(prof_liste);
      // pour finir
      $('#form_gestion h2').html(mode[0].toUpperCase() + mode.substring(1) + ' un groupe de besoin');
      $('#gestion_delete_identite').html( escapeHtml(nom) );
      $('#gestion_edit').hideshow( mode != 'supprimer' );
      $('#gestion_delete').hideshow( mode == 'supprimer' );
      $('#ajax_msg_gestion').removeAttr('class').html('');
      $('#form_gestion label[generated=true]').removeAttr('class').html('');
      $.fancybox( { href:'#form_gestion' , modal:true , minWidth:600 } );
      if(mode=='ajouter') { $('#f_nom').focus(); }
    }

    /**
     * Ajouter un groupe de besoin : mise en place du formulaire
     * @return void
     */
    var ajouter = function()
    {
      mode = $(this).attr('class');
      // Report des valeurs transmises via un formulaire depuis un tableau de synthèse bilan
      if(window.reception_todo)
      {
        window.reception_todo = false;
      }
      else
      {
        window.reception_users_texte  = 'aucun';
        window.reception_users_liste  = '';
      }
      // Afficher le formulaire
      afficher_form_gestion( mode , '' /*id*/ , '' /*niveau_nom*/ , '' /*nom*/ , window.reception_users_texte /*eleve_nombre*/ , window.reception_users_liste /*eleve_liste*/ , 'moi seul' /*prof_nombre*/ , '' /*prof_liste*/ );
    };

    /**
     * Modifier un groupe de besoin : mise en place du formulaire
     * @return void
     */
    var modifier = function()
    {
      mode = $(this).attr('class');
      var objet_tr     = $(this).parent().parent();
      var objet_tds    = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var id           = objet_tr.attr('id').substring(3);
      var niveau_nom   = objet_tds.eq(0).html();
      var nom          = objet_tds.eq(1).html();
      var eleve_nombre = objet_tds.eq(2).html();
      var prof_nombre  = objet_tds.eq(3).html();
      // liste des élèves et des profs
      var eleve_liste   = window.tab_eleves[id];
      var prof_liste    = window.tab_profs[id];
      // Afficher le formulaire
      afficher_form_gestion( mode , id , niveau_nom /* volontairement sans unescapeHtml() */ , unescapeHtml(nom) , eleve_nombre , eleve_liste , prof_nombre , prof_liste );
    };

    /**
     * Retirer un groupe de besoin : mise en place du formulaire
     * @return void
     */
    var supprimer = function()
    {
      mode = $(this).attr('class');
      var objet_tr     = $(this).parent().parent();
      var objet_tds    = objet_tr.find('td');
      // Récupérer les informations de la ligne concernée
      var id           = objet_tr.attr('id').substring(3);
      var nom          = objet_tds.eq(1).html();
      // Afficher le formulaire
      afficher_form_gestion( mode , id , '' /*niveau_nom*/ , unescapeHtml(nom) , '' /*eleve_nombre*/ , '' /*eleve_liste*/ , '' /*prof_nombre*/ , '' /*prof_liste*/ );
    };

    /**
     * Annuler une action
     * @return void
     */
    var annuler = function()
    {
      $.fancybox.close();
      mode = false;
    };

    /**
     * Intercepter la touche entrée ou escape pour valider ou annuler les modifications
     * @return void
     */
    function intercepter(e)
    {
      if(mode)
      {
        if(e.which==13)  // touche entrée
        {
          $('#bouton_valider').click();
        }
        else if(e.which==27)  // touche escape
        {
          $('#bouton_annuler').click();
        }
      }
    }

    /**
     * Choisir les élèves associés à un groupe : mise en place du formulaire
     * @return void
     */
    var choisir_eleve = function()
    {
      // Ne pas changer ici la valeur de "mode" (qui est à "ajouter" ou "modifier" ou "dupliquer").
      $('#zone_eleve li.li_m1 span.gradient_pourcent').html('');
      cocher_eleves( $('#f_eleve_liste').val() );
      // Afficher la zone
      $.fancybox( { href:'#zone_eleve' , modal:true } );
      if( !IsTouch )
      {
        $(document).tooltip('destroy');display_infobulle(); // Sinon, bug avec l’infobulle contenu dans le fancybox qui ne disparait pas au clic...
      }
    };

    /**
     * Choisir les professeurs associés à un groupe : mise en place du formulaire
     * @return void
     */
    var choisir_prof = function()
    {
      cocher_profs( $('#f_prof_liste').val() );
      // Afficher la zone
      $.fancybox( { href:'#zone_profs' , modal:true , minWidth:700 } );
      if( !IsTouch )
      {
        $(document).tooltip('destroy');display_infobulle(); // Sinon, bug avec l’infobulle contenu dans le fancybox qui ne disparait pas au clic...
      }
    };

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Appel des fonctions en fonction des événements
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on( 'click'   , 'q.ajouter'       , ajouter );
    $('#table_action').on( 'click'   , 'q.modifier'      , modifier );
    $('#table_action').on( 'click'   , 'q.supprimer'     , supprimer );

    $('#form_gestion').on( 'click'   , '#bouton_annuler' , annuler );
    $('#form_gestion').on( 'click'   , '#bouton_valider' , function(){formulaire.submit();} );
    $('#form_gestion').on( 'keydown' , 'input,select'    , function(e){intercepter(e);} );

    $('#form_gestion').on( 'click'   , 'q.choisir_eleve' , choisir_eleve );
    $('#form_gestion').on( 'click'   , 'q.choisir_prof'  , choisir_prof );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer au survol une liste de profs associés à une évaluation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'mouseover',
      'td.bulle_profs',
      function()
      {
        var obj_td     = $(this);
        var groupe_id  = obj_td.parent().attr('id').substring(3); // "id_" + groupe_id
        var prof_liste = window.tab_profs[groupe_id];
        var tab_texte  = [];
        if(prof_liste.length)
        {
          var tab_id = prof_liste.split('_');
          var prof_nombre = tab_id.length;
          for(var i in tab_id)
          {
            if( (prof_nombre>=12) && (i>=10) )
            {
              var nombre_reste = prof_nombre - i;
              tab_texte[i] = '... et '+nombre_reste+' collègues supplémentaires';
              break;
            }
            var prof_id  = tab_id[i];
            var input_id = 'p'+'_'+prof_id;
            if($('#'+input_id).length)
            {
              tab_texte[i] = $('#'+input_id).parent().text();
            }
            else
            {
              tab_texte[i] = 'collègue n°'+prof_id+'... ?';
            }
          }
          tab_texte.sort();
        }
        obj_td.attr( 'title' , tab_texte.join('<br>') );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Indiquer au survol une liste d’élèves associés à une évaluation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_action').on
    (
      'mouseover',
      'td.bulle_eleves',
      function()
      {
        var obj_td      = $(this);
        var groupe_id   = obj_td.parent().attr('id').substring(3); // "id_" + groupe_id
        var eleve_liste = window.tab_eleves[groupe_id];
        var tab_texte   = [];
        if(eleve_liste.length)
        {
          var tab_id = eleve_liste.split('_');
          var eleve_nombre = tab_id.length;
          for(var i in tab_id)
          {
            if( (eleve_nombre>=12) && (i>=10) )
            {
              var nombre_reste = eleve_nombre - i;
              tab_texte[i] = '... et '+nombre_reste+' élèves supplémentaires';
              break;
            }
            var id_debut = 'id_'+tab_id[i]+'_';
            if($('input[id^='+id_debut+']').length)
            {
              tab_texte[i] = $('input[id^='+id_debut+']').first().next().text();
            }
            else
            {
              tab_texte[i] = 'élève n°'+tab_id[i]+' (ne vous est pas affecté)';
            }
          }
          tab_texte.sort();
        }
        obj_td.attr( 'title' , tab_texte.join('<br>') );
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Récupération du listing des profs associés à un regroupement
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_groupe_profs').change
    (
      function()
      {
        var obj_option = $(this).find('option:selected');
        var prof_groupe_id = obj_option.val();
        var listing_profs_concernes = obj_option.data('listing');
        if( prof_groupe_id && (typeof(listing_profs_concernes)=='undefined') )
        {
          $('#appliquer_droit_prof_groupe').prop('disabled',true);
          $('#load_profs_groupe').attr('class','loader').html('Recherche&hellip;');
          var prof_groupe_type = obj_option.parent().attr('label');
          $.ajax
          (
            {
              type : 'POST',
              url : 'ajax.php?page=_maj_listing_professeurs',
              data : 'f_groupe_id='+prof_groupe_id+'&f_groupe_type='+prof_groupe_type,
              dataType : 'json',
              error : function(jqXHR, textStatus, errorThrown)
              {
                $('#appliquer_droit_prof_groupe').prop('disabled',false);
                $('#load_profs_groupe').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              },
              success : function(responseJSON)
              {
                initialiser_compteur();
                if(responseJSON['statut']==true)
                {
                  obj_option.data('listing',responseJSON['value']);
                  $('#load_profs_groupe').removeAttr('class').html('');
                }
                else
                {
                  $('#load_profs_groupe').attr('class','alerte').html(responseJSON['value']);
                }
                $('#appliquer_droit_prof_groupe').prop('disabled',false);
              }
            }
          );
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Modification du choix pour un lot de profs
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#prof_check_all').click
    (
      function()
      {
        var listing_profs_concernes = $('#f_groupe_profs').find('option:selected').data('listing');
        if(typeof(listing_profs_concernes)=='undefined')
        {
          $('#load_profs_groupe').attr('class','erreur').html('Liste des collègues non récupérée !');
          return false;
        }
        var tab_profs_concernes = listing_profs_concernes.split(',');
        $('.prof_liste').find('input:enabled').each
        (
          function()
          {
            var prof_id = $(this).attr('id').substring(2); // "p_" + id
            var check = ( tab_profs_concernes.indexOf(prof_id) == -1 ) ? false : true ;
            $(this).prop('checked',check);
          }
        );
      }
    );

    $('#prof_uncheck_all').click
    (
      function()
      {
        $('.prof_liste').find('input:enabled').prop('checked',false);
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour fermer le cadre des élèves associés à un groupe (annuler / retour)
// Clic sur le bouton pour fermer le cadre des professeurs associés à un groupe (annuler / retour)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#annuler_eleve , #annuler_profs').click
    (
      function()
      {
        $.fancybox( { href:'#form_gestion' , modal:true , minWidth:600 } );
        return false;
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour valider le choix des élèves associés à un groupe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#valider_eleve').click
    (
      function()
      {
        var liste = '';
        var nombre = 0;
        var test_doublon = [];
        $('#zone_eleve input[type=checkbox]:checked').each
        (
          function()
          {
            var eleve_id = $(this).val();
            if(typeof(test_doublon[eleve_id])=='undefined')
            {
              test_doublon[eleve_id] = true;
              liste += eleve_id+'_';
              nombre++;
            }
          }
        );
        var eleve_liste  = liste.substring(0,liste.length-1);
        var eleve_nombre = (nombre==0) ? 'aucun' : ( (nombre>1) ? nombre+' élèves' : nombre+' élève' ) ;
        $('#f_eleve_liste').val(eleve_liste);
        $('#f_eleve_nombre').val(eleve_nombre);
        $('#annuler_eleve').click();
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour valider le choix des profs associés à un groupe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#valider_profs').click
    (
      function()
      {
        var liste = '';
        var nombre = 0;
        $('#zone_profs input[type=checkbox]:checked').each
        (
          function()
          {
            liste += $(this).val()+'_';
            nombre++;
          }
        );
        liste  = (nombre==1) ? '' : liste.substring(0,liste.length-1) ;
        nombre = (nombre==1) ? 'moi seul' : nombre+' profs' ;
        $('#f_prof_liste').val(liste);
        $('#f_prof_nombre').val(nombre);
        $('#annuler_profs').click();
      }
    );

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Traitement du formulaire
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_gestion');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_niveau      : { required:true },
          f_nom         : { required:true , maxlength:40 },
          f_eleve_liste : { required:true },
          f_prof_liste  : { required:false }
        },
        messages :
        {
          f_niveau      : { required:'niveau manquant' },
          f_nom         : { required:'nom manquant' , maxlength:'40 caractères maximum' },
          f_eleve_liste : { required:'élève(s) manquant(s)' },
          f_prof_liste  : { }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          element.after(error);
        }
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg_gestion',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        if (!please_wait)
        {
          $(this).ajaxSubmit(ajaxOptions);
          return false;
        }
        else
        {
          return false;
        }
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg_gestion').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        please_wait = true;
        $('#form_gestion button').prop('disabled',true);
        $('#ajax_msg_gestion').attr('class','loader').html('En cours&hellip;');
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      please_wait = false;
      $('#form_gestion button').prop('disabled',false);
      $('#ajax_msg_gestion').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      please_wait = false;
      $('#form_gestion button').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg_gestion').attr('class','alerte').html(responseJSON['value']);
      }
      else
      {
        $('#ajax_msg_gestion').attr('class','valide').html('Demande réalisée !');
        var groupe_id = responseJSON['id'];
        switch (mode)
        {
          case 'ajouter':
            $('#table_action tbody tr.vide').remove(); // En cas de tableau avec une ligne vide pour la conformité XHTML
            var niveau_nom = $('#f_niveau option:selected').text();
            var new_tr = responseJSON['html'].replace('<td>{{NIVEAU_NOM}}</td>','<td data-text="'+window.tab_niveau_ordre[niveau_nom]+'">'+niveau_nom+'</td>');
            $('#table_action tbody').prepend(new_tr);
            window.tab_eleves[groupe_id] = responseJSON['eleves'];
            window.tab_profs[ groupe_id] = responseJSON['profs'];
            $('#f_groupe_profs').find('optgroup[label=Besoins]').append('<option value="'+groupe_id+'" data-listing="'+window.tab_profs[groupe_id].replaceAll('_',',')+'">'+unescapeHtml($('#f_nom').val())+'</option>');
            break;
          case 'modifier':
            var niveau_nom = $('#f_niveau option:selected').text();
            var new_tds = responseJSON['html'].replace('<td>{{NIVEAU_NOM}}</td>','<td data-text="'+window.tab_niveau_ordre[niveau_nom]+'">'+niveau_nom+'</td>');
            $('#id_'+groupe_id).addClass('new').html(new_tds);
            window.tab_eleves[groupe_id] = responseJSON['eleves'];
            window.tab_profs[ groupe_id] = responseJSON['profs'];
            $('#f_groupe_profs').find('optgroup[label=Besoins]').find('option[value='+groupe_id+']').data('listing',window.tab_profs[groupe_id].replaceAll('_',','));
            break;
          case 'supprimer':
            $('#id_'+groupe_id).remove();
            $('#f_groupe_profs').find('optgroup[label=Besoins]').find('option[value='+groupe_id+']').remove();
            break;
        }
        tableau_maj();
        $.fancybox.close();
        mode = false;
      }
    }

    // Initialiser l’affichage au démarrage
    if( window.reception_todo )
    {
      $('q.ajouter').click();
    }

  }
);
