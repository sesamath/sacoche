<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Plans de classes'));

$tab_groupes = ($_SESSION['USER_JOIN_GROUPES']=='config') ? DB_STRUCTURE_COMMUN::DB_OPT_groupes_professeur($_SESSION['USER_ID']) : DB_STRUCTURE_COMMUN::DB_OPT_classes_groupes_etabl() ;
$select_groupe = HtmlForm::afficher_select($tab_groupes , 'f_groupe' /*select_nom*/ , '' /*option_first*/ , '' /*selection*/ , 'regroupements' /*optgroup*/ );
?>

<p><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_professeur__plan_classe">DOC : Plans de classes</a></span></p>

<hr>

<table id="table_action" class="form hsort">
  <thead>
    <tr>
      <th>Regroupement</th>
      <th>Dénomination</th>
      <th>Nb rangées</th>
      <th>Nb colonnes</th>
      <th class="nu"><q class="ajouter"<?php echo infobulle('Ajouter un plan de classe.') ?>></q><q class="importer_plan"<?php echo infobulle('Importer le plan d’un(e) collègue.') ?>></q></th>
    </tr>
  </thead>
  <tbody>
    <?php
    // Lister les plans de classes
    $DB_TAB = DB_STRUCTURE_PROFESSEUR_PLAN::DB_lister_plans_prof( $_SESSION['USER_ID'] );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        // Afficher une ligne du tableau
        echo'<tr id="id_'.$DB_ROW['plan_id'].'">';
        echo  '<td data-id="'.$DB_ROW['groupe_id'].'">'.html($DB_ROW['groupe_nom']).'</td>';
        echo  '<td>'.html($DB_ROW['plan_nom']).'</td>';
        echo  '<td>'.$DB_ROW['plan_nb_rangees'].'</td>';
        echo  '<td>'.$DB_ROW['plan_nb_colonnes'].'</td>';
        echo  '<td class="nu">';
        echo    '<q class="placer_eleves"'.infobulle('Placer les élèves.').'></q>';
        echo    '<q class="imprimer"'.infobulle('Imprimer ce plan.').'></q>';
        echo    '<q class="dupliquer"'.infobulle('Dupliquer ce plan.').'></q>';
        echo    '<q class="modifier"'.infobulle('Modifier ce plan.').'></q>';
        echo    '<q class="supprimer"'.infobulle('Supprimer ce plan.').'></q>';
        echo  '</td>';
        echo'</tr>'.NL;
      }
    }
    else
    {
      echo'<tr class="vide"><td class="nu" colspan="4"></td><td class="nu"></td></tr>'.NL;
    }
    ?>
  </tbody>
</table>

<form action="#" method="post" id="form_gestion" class="hide">
  <h2>Ajouter | Dupliquer | Modifier | Supprimer un plan</h2>
  <div id="gestion_edit">
    <p>
      <label class="tab" for="f_groupe">Classe / groupe :</label><?php echo $select_groupe ?><input type="hidden" id="f_groupe_type" name="f_groupe_type" value=""><br>
      <label class="tab" for="f_nom">Dénomination :</label><input id="f_nom" name="f_nom" type="text" value="" size="30" maxlength="40"><br>
      <label class="tab" for="f_rangees">Nb rangées :</label><input id="f_rangees" name="f_rangees" type="number" min="2" max="15"><br>
      <label class="tab" for="f_colonnes">Nb colonnes :</label><input id="f_colonnes" name="f_colonnes" type="number" min="2" max="15">
    </p>
  </div>
  <div id="gestion_delete">
    <p>Confirmez-vous la suppression du plan de classe &laquo;&nbsp;<b id="gestion_delete_plan_nom"></b>&nbsp;&raquo; ?</p>
  </div>
  <p>
    <span class="tab"></span><input id="f_action" name="f_action" type="hidden" value=""><input id="f_id" name="f_id" type="hidden" value=""><button id="bouton_valider" type="button" class="valider">Valider.</button> <button id="bouton_annuler" type="button" class="annuler">Annuler.</button><label id="ajax_msg_gestion">&nbsp;</label>
  </p>
</form>

<form action="#" method="post" id="form_importer" class="hide">
  <h2>Importer le plan d’un(e) collègue</h2>
  <p>
    <label class="tab" for="f_groupe_import">Classe / groupe :</label><?php echo str_replace('id="f_groupe"','id="f_groupe_import"',$select_groupe) ?><input type="hidden" id="f_groupe_type_import" name="f_groupe_type" value=""><br>
    <label class="tab" for="f_import_id">Plan(s) trouvé(s) :</label><select id="f_import_id" name="f_id"><option value="" disabled>Sélectionner un regroupement.</option></select>
  </p>
  <ul id="ul_import">
    <li></li>
  </ul>
  <p>
    <span class="tab"></span><button id="bouton_importer" type="button" class="valider" disabled>Confirmer l’import.</button> <button id="bouton_annuler_import" type="button" class="annuler">Annuler.</button><label id="ajax_msg_import">&nbsp;</label>
  </p>
</form>

<form action="#" method="post" id="zone_imprimer" class="hide"><fieldset>
  <h2>Imprimer un plan de classe</h2>
  <p>
    <label class="tab">Plan :</label><span id="titre_imprimer"></span><br>
    <label class="tab">Éléments :</label><label for="f_imprimer_nom"><input type="checkbox" id="f_imprimer_nom" name="f_is_nom" value="1" checked> Nom</label><br>
    <span class="tab"></span><label for="f_imprimer_prenom"><input type="checkbox" id="f_imprimer_prenom" name="f_is_prenom" value="1" checked> Prénom</label><br>
    <span class="tab"></span><label for="f_imprimer_photo"><input type="checkbox" id="f_imprimer_photo" name="f_is_photo" value="1" checked> Photo</label><br>
    <span class="tab"></span><label for="f_imprimer_ordre"><input type="checkbox" id="f_imprimer_ordre" name="f_is_ordre" value="1"> Numéro d’ordre</label><br>
    <span class="tab"></span><label for="f_imprimer_equipe"><input type="checkbox" id="f_imprimer_equipe" name="f_is_equipe" value="1"> Équipe</label><br>
    <span class="tab"></span><span id="span_role" class="hide"><label for="f_imprimer_role"><input type="checkbox" id="f_imprimer_role" name="f_is_role" value="1"> Rôle</label></span>
  </p>
  <p>
    <span class="tab"></span><button id="valider_imprimer" type="button" class="valider">Générer le PDF</button> <button id="fermer_zone_imprimer" type="button" class="retourner">Retour</button> <label id="ajax_msg_imprimer">&nbsp;</label>
      <input id="imprimer_id"          name="f_id"          type="hidden" value="">
      <input id="imprimer_nom"         name="f_nom"         type="hidden" value="">
      <input id="imprimer_groupe"      name="f_groupe"      type="hidden" value="">
      <input id="imprimer_groupe_type" name="f_groupe_type" type="hidden" value="">
      <input id="imprimer_rangees"     name="f_rangees"     type="hidden" value="">
      <input id="imprimer_colonnes"    name="f_colonnes"    type="hidden" value="">
  </p>
  <p id="zone_imprimer_retour"></p>
</fieldset></form>

<form action="#" method="post" id="zone_placer" class="hide">
  <h2>Placer les élèves sur un plan - <span id="gestion_placer_plan_nom"></span></h2>
  <p class="astuce">
    Utiliser la touche <kbd>F11</kbd> pour activer (ou quitter) le mode plein écran.<br>
    Double-cliquer sur des vignettes pour les intervertir.
  </p>
  <p class="ml">
    Renuméroter automatiquement
    <select id="ordre_mode">
      <option value="">&nbsp;</option>
      <option value="l2r_u2b">de gauche à droite en partant du haut</option>
      <option value="l2r_b2u">de gauche à droite en partant du bas</option>
      <option value="r2l_u2b">de droite à gauche en partant du haut</option>
      <option value="r2l_b2u">de droite à gauche en partant du bas</option>
      <option value="u2b_l2r">de haut en bas en partant de la gauche</option>
      <option value="u2b_r2l">de haut en bas en partant de la droite</option>
      <option value="b2u_l2r">de bas en haut en partant de la gauche</option>
      <option value="b2u_r2l">de bas en haut en partant de la droite</option>
    </select>
    <select id="ordre_alternance">
      <option value="0">en conservant le sens</option>
      <option value="1">en alternant le sens</option>
    </select>
    <select id="ordre_groupe">
      <option value="1">individuellement</option>
      <option value="2">par table de 2</option>
      <option value="3">par table de 3</option>
    </select><br>
    <button id="valider_place" type="button" class="valider">Enregistrer ces emplacements</button> <button id="fermer_zone_placer" type="button" class="retourner">Retour</button> <label id="ajax_msg_placer">&nbsp;</label>
    <input id="placer_id"          name="f_id"          type="hidden" value="">
    <input id="placer_groupe"      name="f_groupe"      type="hidden" value="">
    <input id="placer_groupe_type" name="f_groupe_type" type="hidden" value="">
    <input id="placer_rangees"     name="f_rangees"     type="hidden" value="">
    <input id="placer_colonnes"    name="f_colonnes"    type="hidden" value="">
  </p>
  <ul id="swap_hv">
    <li></li>
  </ul>
</form>
