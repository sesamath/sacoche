/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// Variable globale Highcharts
var graphique;
var ChartOptions;

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var prof_id      = 0;
    var groupe_id    = 0;
    var groupe_type  = $('#f_groupe option:selected').parent().attr('label'); // Il faut indiquer une valeur initiale au moins pour le profil élève
    var eleves_ordre = '';
    var action_prof  = 'ajouter';

    var bilan_affiche    = false;
 // var memo_eleve         = 0; // déjà défini de façon globale par script.js car besoin dans script.js
    var memo_eleve_first = 0;
    var memo_eleve_last  = 0;

    $('#f_eleve').hide();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Options de base pour le graphique : sont complétées ensuite avec les données personnalisées
    // @see   http://www.highcharts.com/ --> http://api.highcharts.com/highcharts
    //
    // Pour un diagramme en toile d’araignée il faut  :
    // - charger highcharts-more.js
    // - ajouter " polar: true " à " chart: { ... } "
    // - ajouter " groupPadding: 0 " à " plotOptions: { column: { ... } } "
    // - retirer " text: ... " de " ChartOptions.yAxis[1] "
    // Mais ce n’est pas très convaincant : pour la courbe ça va mais pour les états d’acquisition
    // l’utilisation de secteurs circulaires fait que les aires coloriées ne sont pas proportionnelles aux données.
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    ChartOptions = {
      chart: {
        renderTo: 'div_graphique_releve',
        alignTicks: false,
        type: 'column',
        spacingTop: 10
       },
      colors: 
      window.BACKGROUND_COLORS
      ,
      title: {
        text: null
      },
      xAxis: {
        labels: {
          style: { color: '#000' },
          autoRotationLimit: 0
        },
        categories: [] // MAJ ensuite
      },
      yAxis: [
        {
          labels: { enabled: false },
          reversedStacks: false,
          min: 0,
          max: 100,
          gridLineWidth: 0,
          title: { style: { color: '#333' } , text: 'Items acquis' }
        },
        {} // MAJ ensuite
      ],
      tooltip: {
        formatter: function() {
          return this.series.name +' : '+ (this.y);
        }
      },
      plotOptions: {
        column: {
          stacking: 'percent',
          // des élèves mal intentionnés font disparaître de la synthèse graphique les plus mauvais niveaux de maitrises en cliquant sur la légende,
          // et ainsi présenter uniquement les compétences maîtrisées à leur famille !
          // décision est prise de retirer cette fonctionnalité pour tous car elle n’a pas vraiment d’utilité (y compris pour les bulletins scolaires)
          events: {
            legendItemClick: function () {
                return false; 
            }
          }
        }
      },
      series: [] // MAJ ensuite
      ,
      credits: {
        enabled: false
      },
      lang: {
        contextButtonTitle: 'Exporter...'
      },
      exporting: {
        buttons: {
          contextButton: {
            menuItems: ['printChart', 'separator', 'downloadPNG', 'downloadJPEG', 'downloadSVG']
          }
        }
      }
    };

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enlever le message ajax et le résultat précédent au changement d’un select
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('select').change
    (
      function()
      {
        $('#ajax_msg').removeAttr('class').html('');
      }
    );

    $('#f_indicateur_aucun , #f_indicateur_MS , #f_indicateur_PA').click
    (
      function()
      {
        $('label[for=f_conversion_sur_20]').hideshow( $('#f_indicateur_MS').is(':checked') || $('#f_indicateur_PA').is(':checked') );
      }
    );

    var autoperiode = true; // Tant qu’on ne modifie pas manuellement le choix des périodes, modification automatique du formulaire

    function view_dates_perso()
    {
      var periode_val = $('#f_periode').val();
      if(periode_val!=0)
      {
        $('#dates_perso').attr('class','hide');
      }
      else
      {
        $('#dates_perso').attr('class','show');
      }
    }

    $('#f_periode').change
    (
      function()
      {
        view_dates_perso();
        autoperiode = false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger tous les profs d’une classe (approximativement) ou n’affiche que le prof connecté
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_prof_connecte()
    {
      $('#f_prof').html('<option value="'+window.USER_ID+'">'+window.USER_TEXTE+'</option>');
      action_prof = 'ajouter';
      $('#retirer_prof').hide(0);
      $('#ajouter_prof').show(0);
    }

    function charger_profs_groupe()
    {
      $('button').prop('disabled',true);
      prof_id     = $('#f_prof   option:selected').val();
      groupe_id   = $('#f_groupe option:selected').val();
      groupe_type = $('#f_groupe option:selected').parent().attr('label');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_profs_groupe',
          data : 'f_prof='+prof_id+'&f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('button').prop('disabled',false);
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('button').prop('disabled',false);
            if(responseJSON['statut']==true)
            {
              $('#f_prof').html(responseJSON['value']);
              action_prof = 'retirer';
              $('#ajouter_prof').hide(0);
              $('#retirer_prof').show(0);
            }
          }
        }
      );
    }

    $('#ajouter_prof , #retirer_prof').click
    (
      function()
      {
        if(action_prof=='retirer')
        {
          afficher_prof_connecte();
        }
        else if(action_prof=='ajouter')
        {
          charger_profs_groupe();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Changement de groupe
    // -> desactiver les périodes prédéfinies en cas de groupe de besoin (prof uniquement)
    // -> choisir automatiquement la meilleure période si un changement manuel de période n’a jamais été effectué
    // -> afficher le formulaire de périodes s’il est masqué
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function selectionner_periode_adaptee()
    {
      var id_groupe = $('#f_groupe option:selected').val();
      if(typeof(window.tab_groupe_periode[id_groupe])!='undefined')
      {
        for(var id_periode in window.tab_groupe_periode[id_groupe]) // Parcourir un tableau associatif...
        {
          var tab_split = window.tab_groupe_periode[id_groupe][id_periode].split('_');
          if( (window.TODAY_SQL>=tab_split[0]) && (window.TODAY_SQL<=tab_split[1]) )
          {
            $('#f_periode option[value='+id_periode+']').prop('selected',true);
            view_dates_perso();
            break;
          }
        }
      }
    }

    $('#f_groupe').change
    (
      function()
      {
        groupe_type = $('#f_groupe option:selected').parent().attr('label');
        $('#f_periode option').each
        (
          function()
          {
            var periode_id = $(this).val();
            // La période personnalisée est tout le temps accessible
            if(periode_id!=0)
            {
              // classe ou groupe classique -> toutes périodes accessibles
              if(groupe_type!='Besoins')
              {
                $(this).prop('disabled',false);
              }
              // groupe de besoin -> desactiver les périodes prédéfinies
              else
              {
                $(this).prop('disabled',true);
              }
            }
          }
        );
        // Sélectionner si besoin la période personnalisée
        if(groupe_type=='Besoins')
        {
          $('#f_periode option[value=0]').prop('selected',true);
          $('#dates_perso').attr('class','show');
        }
        // Modification automatique du formulaire : périodes
        if(autoperiode)
        {
          if( (typeof(groupe_type)!='undefined') && (groupe_type!='Besoins') )
          {
            // Rechercher automatiquement la meilleure période
            selectionner_periode_adaptee();
          }
        }
        // Afficher la zone de choix des périodes, et celle des enseignants
        if(typeof(groupe_type)!='undefined')
        {
          $('#zone_periodes , #zone_profs').removeAttr('class');
          $('#info_profs').addClass('hide');
        }
        else
        {
          $('#zone_periodes , #zone_profs').addClass('hide');
          $('#info_profs').removeAttr('class');
        }
        // Rechercher automatiquement la liste des profs
        if( (typeof(groupe_type)!='undefined') && (groupe_type!='Besoins') )
        {
          if( (window.USER_PROFIL_TYPE!='professeur') || (action_prof=='retirer') )
          {
            charger_profs_groupe();
          }
        }
        else
        {
          afficher_prof_connecte();
        }
      }
    );

    // Rechercher automatiquement la meilleure période au chargement de la page (uniquement pour un élève, seul cas où la classe est préselectionnée)
    selectionner_periode_adaptee();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger les selects f_eleve (pour le professeur et le directeur et les parents de plusieurs enfants)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_choix_tri_eleves(groupe_id,groupe_type)
    {
      if( (groupe_type=='Classes') && (window.PROFIL_TYPE!='professeur') )
      {
        $('#bloc_ordre').hide();
      }
      else
      {
        var nb_options_possibles = 0;
        $('#f_eleves_ordre option').each
        (
          function()
          {
            var value = $(this).val();
            if( (value=='nom') || (value=='prenom') )
            {
              $(this).prop('disabled',false);
              nb_options_possibles++;
            }
            else if(value=='classe')
            {
              if(groupe_type=='Classes')
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
              else
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
            }
            else
            {
              var plan_groupe = $(this).data('data');
              if(groupe_id==plan_groupe)
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
              else
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
            }
          }
        );
        $('#bloc_ordre').hideshow( nb_options_possibles > 1 );
      }
    }

    function maj_eleve(groupe_id,groupe_type,eleves_ordre)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eleves',
          data : 'f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_eleves_ordre='+eleves_ordre+'&f_statut=1'+'&f_multiple=0'+'&f_nofirst=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj').removeAttr('class').html('');
              $('#f_eleve').html(responseJSON['value']).show();
              if($('#f_eleve option').length==2)
              {
                // Cas d’un seul élève retourné dans le regroupement (en particulier pour un parent de plusieurs enfants)
                $('#f_eleve option').eq(1).prop('selected',true);
              }
            }
            else
            {
              $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    $('#f_groupe').change
    (
      function()
      {
        // Pour un directeur, un professeur ou un parent de plusieurs enfants, on met à jour f_eleve
        // Pour un élève ou un parent d’un seul enfant cette fonction n’est pas appelée puisque son groupe (masqué) ne peut être changé
        $('#f_eleve').html('<option value="">&nbsp;</option>').hide();
        groupe_id = $('#f_groupe option:selected').val();
        if(groupe_id)
        {
          $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
          groupe_type  = $('#f_groupe option:selected').parent().attr('label');
          maj_choix_tri_eleves(groupe_id,groupe_type);
          eleves_ordre = $('#f_eleves_ordre option:selected').val();
          maj_eleve(groupe_id,groupe_type,eleves_ordre);
        }
        else
        {
          $('#bloc_ordre').hide();
          $('#ajax_maj').removeAttr('class').html('');
        }
      }
    );

    $('#f_eleves_ordre').change
    (
      function()
      {
        groupe_id    = $('#f_groupe option:selected').val();
        groupe_type  = $('#f_groupe option:selected').parent().attr('label');
        eleves_ordre = $('#f_eleves_ordre option:selected').val();
        $('#f_eleve').html('<option value="">&nbsp;</option>').hide();
        $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
        maj_eleve(groupe_id,groupe_type,eleves_ordre);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Soumettre le formulaire principal
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_select');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_indicateur        : { required:true },
          f_conversion_sur_20 : { required:false },
          f_groupe            : { required:true },
          f_eleve             : { required:true },
          f_eleves_ordre      : { required:true },
          f_periode           : { required:true },
          f_date_debut        : { required:function(){return $('#f_periode').val()==0;} , dateITA:true },
          f_date_fin          : { required:function(){return $('#f_periode').val()==0;} , dateITA:true },
          f_retroactif        : { required:true },
          f_prop_sans_score   : { required:false },
          f_only_diagnostic   : { required:true },
          f_only_socle        : { required:false },
          f_only_prof         : { required:false },
          f_prof              : { required:function(){return $('#f_only_prof').is(':checked');} }
        },
        messages :
        {
          f_indicateur        : { required:'choix manquant' },
          f_conversion_sur_20 : { },
          f_groupe            : { required:'groupe manquant' },
          f_eleve             : { required:'élève manquant' },
          f_eleves_ordre      : { required:'ordre manquant' },
          f_periode           : { required:'période manquante' },
          f_date_debut        : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_date_fin          : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_retroactif        : { required:'choix manquant' },
          f_prop_sans_score   : { },
          f_only_diagnostic   : { required:'choix manquant' },
          f_only_socle        : { },
          f_only_prof         : { },
          f_prof              : { required:'enseignant manquant' }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          if(element.is('select')) {element.after(error);}
          else if(element.attr('type')=='text') {element.next().after(error);}
          else if(element.attr('type')=='radio') {element.parent().next().next().after(error);}
          else if(element.attr('type')=='checkbox') {element.parent().next().next().after(error);}
        }
        // success: function(label) {label.text('ok').attr('class','valide');} Pas pour des champs soumis à vérification PHP
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        // récupération d’éléments
        $('#f_nom_prenom').val( $('#f_eleve option:selected').text() );
        $('#f_prof_texte').val( $('#f_prof  option:selected').text() );
        $('#f_groupe_type').val( groupe_type );
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      if(bilan_affiche)
      {
        return true;
      }
      else
      {
        $('#ajax_msg').removeAttr('class').html('');
        var readytogo = validation.form();
        if(readytogo)
        {
          $('#form_select button').prop('disabled',true);
          $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
        }
        return readytogo;
      }
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : 'Erreur 500&hellip; Mémoire insuffisante ? Sélectionner une période plus restreinte ou demander à votre hébergeur d’augmenter la valeur "memory_limit".' ;
      if(bilan_affiche)
      {
        $('#bilan button , #bilan select').prop('disabled',false);
        $('#div_graphique_releve').html('<label class="alerte">'+message+'</label>');
      }
      else
      {
        $('#form_select button').prop('disabled',false);
        $('#ajax_msg').attr('class','alerte').html(message);
      }
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      if(bilan_affiche)
      {
        $('#bilan button , #bilan select').prop('disabled',false);
      }
      else
      {
        $('#form_select button').prop('disabled',false);
      }
      if(responseJSON['statut']==false)
      {
        if(bilan_affiche)
        {
          $('#div_graphique_releve').html('<label class="alerte">'+responseJSON['value']+'</label>');
        }
        else
        {
          $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
        }
      }
      else
      {
        var titre  = responseJSON['titre'];
        var script = responseJSON['script'];
        if( (window.PROFIL_TYPE=='professeur') || (window.PROFIL_TYPE=='directeur') )
        {
          charger_photo_eleve(memo_eleve,'maj');
          $('#cadre_photo').show(0);
        }
        if(bilan_affiche)
        {
          $('#go_selection_eleve option[value='+memo_eleve+']').prop('selected',true);
          masquer_element_navigation_choix_eleve();
          eval(script); // ChartOptions
          graphique = new Highcharts.Chart(ChartOptions);
        }
        else
        {
          if( (window.PROFIL_TYPE=='professeur') || (window.PROFIL_TYPE=='directeur') )
          {
            memo_eleve = $('#f_eleve option:selected').val();
            $('#go_selection_eleve').html( $('#f_eleve').html() );
            $('#go_selection_eleve option[value='+memo_eleve+']').prop('selected',true);
            memo_eleve_first = $('#go_selection_eleve option:first').val();
            memo_eleve_last  = $('#go_selection_eleve option:last').val();
            masquer_element_navigation_choix_eleve();
          }
          else
          {
            $('#report_eleve').html( $('#f_nom_prenom').val() );
          }
          $('#ajax_msg').removeAttr('class').html('');
          $('#form_select , #zone_preliminaire').hide();
          $('#report_titre').html(titre);
          $('#bilan , #div_graphique_releve').show();
          eval(script); // ChartOptions
          graphique = new Highcharts.Chart(ChartOptions);
          bilan_affiche = true;
        }
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Navigation d’un élève à un autre
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function charger_nouvel_eleve(eleve_id)
    {
      $('#f_eleve option[value='+eleve_id+']').prop('selected',true);
      memo_eleve = eleve_id;
      $('#bilan button , #bilan select').prop('disabled',true);
      $('#div_graphique_releve').html('<label class="loader">En cours&hellip;</label>');
      formulaire.submit();
    }

    function masquer_element_navigation_choix_eleve()
    {
      $('#bilan button').css('visibility','visible');
      if(memo_eleve==memo_eleve_first)
      {
        $('#go_premier_eleve , #go_precedent_eleve').css('visibility','hidden');
      }
      if(memo_eleve==memo_eleve_last)
      {
        $('#go_dernier_eleve , #go_suivant_eleve').css('visibility','hidden');
      }
    }

    $('#go_premier_eleve').click
    (
      function()
      {
        var eleve_id = $('#go_selection_eleve option:first').val();
        charger_nouvel_eleve(eleve_id);
      }
    );

    $('#go_dernier_eleve').click
    (
      function()
      {
        var eleve_id = $('#go_selection_eleve option:last').val();
        charger_nouvel_eleve(eleve_id);
      }
    );

    $('#go_precedent_eleve').click
    (
      function()
      {
        if( $('#go_selection_eleve option:selected').prev().length )
        {
          var eleve_id = $('#go_selection_eleve option:selected').prev().val();
          charger_nouvel_eleve(eleve_id);
        }
      }
    );

    $('#go_suivant_eleve').click
    (
      function()
      {
        if( $('#go_selection_eleve option:selected').next().length )
        {
          var eleve_id = $('#go_selection_eleve option:selected').next().val();
          charger_nouvel_eleve(eleve_id);
        }
      }
    );

    $('#go_selection_eleve').change
    (
      function()
      {
        var eleve_id = $('#go_selection_eleve option:selected').val();
        charger_nouvel_eleve(eleve_id);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour fermer la zone bilan
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#fermer_zone_bilan').click
    (
      function()
      {
        $('#bilan , #div_graphique_releve').hide();
        $('#cadre_photo').hide(0);
        $('#form_select , #zone_preliminaire').show();
        bilan_affiche = false;
        return false;
      }
    );

  }
);
