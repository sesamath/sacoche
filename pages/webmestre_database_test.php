<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = 'Test des droits SQL'; // Pas de traduction car pas de choix de langue pour ce profil.

// Page réservée aux installations multi-structures ; le menu webmestre d’une installation mono-structure ne permet normalement pas d’arriver ici
if(HEBERGEUR_INSTALLATION=='mono-structure')
{
  echo'<p class="astuce">L’installation étant de type mono-structure, cette fonctionnalité de <em>SACoche</em> est sans objet vous concernant.</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}

// Ajout exceptionnel de css
Layout::add( 'css_inline' , 'code{display:block;font-weight:bold;white-space:pre;padding:1px;margin:2px;background-color:#EEE} code.g{color:#333} code.v{color:#080} code.r{color:#A00}' );
?>

<p><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_webmestre__droits_sql">DOC : Droits SQL requis (multi-structures).</a></span></p>

<hr>

<form action="./index.php?page=webmestre_database_test" method="post">
  <p class="ti"><input id="lancer_test" name="lancer_test" type="hidden" value="go"><button id="bouton_newsletter" type="submit" class="parametre">Lancer le test.</button></p>
</form>

<hr>

<?php
// Pas de passage par la page ajax.php, mais pas besoin ici de protection contre attaques type CSRF

function afficher_PDO_exception($e)
{
  // mb_detect_encoding() et perso_mb_detect_encoding_utf8() détectent que $e est en UTF-8 alors que ce n’est pas le cas.
  // Du coup, html($e) renvoie une chaine vide.
  // D’où l’usage de html(utf8_encode())
  // juillet 2020 : je ne constate pas ça en local, au contraire utf8_encode() corromp l’affichage, du coup je reviens à la normale
  return html( $e->getMessage() );
}


if(isset($_POST['lancer_test']))
{
  $PDO_options = array( PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION );

  define('SACOCHE_STRUCTURE_BD_HOST',SACOCHE_WEBMESTRE_BD_HOST);
  define('SACOCHE_STRUCTURE_BD_PORT',SACOCHE_WEBMESTRE_BD_PORT);
  define('SACOCHE_STRUCTURE_BD_NAME','sac_base_0');
  define('SACOCHE_STRUCTURE_BD_USER','sac_user_0');
  define('SACOCHE_STRUCTURE_BD_PASS','sac_pass_0');

  echo'<h2>1.1 Paramètres de connexion du webmestre</h2>'.NL;
  echo'<code>BD_HOST : '.SACOCHE_WEBMESTRE_BD_HOST.'</code>'.NL;
  echo'<code>BD_PORT : '.SACOCHE_WEBMESTRE_BD_PORT.'</code>'.NL;
  echo'<code>BD_NAME : '.SACOCHE_WEBMESTRE_BD_NAME.'</code>'.NL;
  echo'<code>BD_USER : '.SACOCHE_WEBMESTRE_BD_USER.'</code>'.NL;
  echo'<code>BD_PASS : '.'--masqué--'.'</code>'.NL;

  echo'<h2>1.2 Paramètres de connexion établissement</h2>'.NL;
  echo'<code>BD_HOST : '.SACOCHE_STRUCTURE_BD_HOST.'</code>'.NL;
  echo'<code>BD_PORT : '.SACOCHE_STRUCTURE_BD_PORT.'</code>'.NL;
  echo'<code>BD_NAME : '.SACOCHE_STRUCTURE_BD_NAME.' (pour ce test uniquement)</code>'.NL;
  echo'<code>BD_USER : '.SACOCHE_STRUCTURE_BD_USER.' (pour ce test uniquement)</code>'.NL;
  echo'<code>BD_PASS : '.SACOCHE_STRUCTURE_BD_PASS.' (pour ce test uniquement)</code>'.NL;

  echo'<hr>'.NL;

  echo'<h2>2.1 Se connecter au serveur SQL comme webmestre</h2>'.NL;
  echo'<code>new PDO(mysql:host='.SACOCHE_WEBMESTRE_BD_HOST.';port='.SACOCHE_WEBMESTRE_BD_PORT.','.SACOCHE_WEBMESTRE_BD_USER.','.'--masqué--'.', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION])</code>'.NL;
  try
  {
    $BD_handler_root = new PDO( 'mysql:host='.SACOCHE_WEBMESTRE_BD_HOST.';port='.SACOCHE_WEBMESTRE_BD_PORT , SACOCHE_WEBMESTRE_BD_USER , SACOCHE_WEBMESTRE_BD_PASS , $PDO_options );
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
    return;
  }
  echo'<code class="v">OK</code>'.NL;

$BD_handler_root->setAttribute(PDO::ATTR_CASE, PDO::CASE_NATURAL);

  echo'<h2>2.2 Voir les droits du webmestre</h2>'.NL;
  $query = 'SHOW GRANTS FOR CURRENT_USER()';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">'.str_replace(', ','<br>',current( $result->fetch(PDO::FETCH_NUM) )).'</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>2.3 Créer une base "sac_base_0"</h2>'.NL;
  $query = 'CREATE DATABASE '.SACOCHE_STRUCTURE_BD_NAME.' DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>2.4 Créer un user "sac_user_0" sur localhost et sur %</h2>'.NL;
  $query = 'CREATE USER '.SACOCHE_STRUCTURE_BD_USER.'@"localhost" IDENTIFIED BY "'.SACOCHE_STRUCTURE_BD_PASS.'"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }
  $query = 'CREATE USER '.SACOCHE_STRUCTURE_BD_USER.'@"%" IDENTIFIED BY "'.SACOCHE_STRUCTURE_BD_PASS.'"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>2.5 Attribuer des droits à ce user</h2>'.NL;
  $query = 'GRANT ALTER, CREATE, DELETE, DROP, INDEX, INSERT, SELECT, UPDATE ON '.SACOCHE_STRUCTURE_BD_NAME.'.* TO '.SACOCHE_STRUCTURE_BD_USER.'@"localhost"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }
  $query = 'GRANT ALTER, CREATE, DELETE, DROP, INDEX, INSERT, SELECT, UPDATE ON '.SACOCHE_STRUCTURE_BD_NAME.'.* TO '.SACOCHE_STRUCTURE_BD_USER.'@"%"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>2.6 Vérifier les droits du user : base mysql.user</h2>'.NL;
  $query = 'SELECT host, user, Select_priv FROM mysql.user WHERE user="'.SACOCHE_STRUCTURE_BD_USER.'"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
    echo'<code class="v">';foreach($result as $row){print_r($row);}echo'</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>2.7 Vérifier les droits du user : base mysql.db</h2>'.NL;
  $query = 'SELECT host, user, Select_priv FROM mysql.db WHERE user="'.SACOCHE_STRUCTURE_BD_USER.'"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
    echo'<code class="v">';foreach($result as $row){print_r($row);}echo'</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>2.8 Fermer la connexion webmestre</h2>'.NL;
  $BD_handler_root = NULL;
  echo'<code>RAS</code>'.NL;

  echo'<hr>'.NL;

  echo'<h2>3.1 Se connecter à la base "sac_base_0" avec le user "sac_user_0"</h2>'.NL;
  echo'<code>new PDO(mysql:host='.SACOCHE_STRUCTURE_BD_HOST.';port='.SACOCHE_STRUCTURE_BD_PORT.';dbname='.SACOCHE_STRUCTURE_BD_NAME.','.SACOCHE_STRUCTURE_BD_USER.','.SACOCHE_STRUCTURE_BD_PASS.', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION])</code>'.NL;
  try
  {
    $BD_handler_root = new PDO( 'mysql:host='.SACOCHE_STRUCTURE_BD_HOST.';port='.SACOCHE_STRUCTURE_BD_PORT.';dbname='.SACOCHE_STRUCTURE_BD_NAME , SACOCHE_STRUCTURE_BD_USER , SACOCHE_STRUCTURE_BD_PASS , $PDO_options );
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
    return;
  }
  echo'<code class="v">OK</code>'.NL;

  echo'<h2>3.2 Créer une table dans la base "sac_base_0" avec le user "sac_user_0"</h2>'.NL;
  $query = 'CREATE TABLE IF NOT EXISTS sacoche_test (test_id MEDIUMINT(8) UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (test_id) )';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>3.3 Fermer la connexion sac_user_0</h2>'.NL;
  $BD_handler_root = NULL;
  echo'<code>RAS</code>'.NL;

  echo'<hr>'.NL;

  echo'<h2>4.1 Se connecter au serveur SQL comme webmestre</h2>'.NL;
  echo'<code>new PDO(mysql:host='.SACOCHE_WEBMESTRE_BD_HOST.';port='.SACOCHE_WEBMESTRE_BD_PORT.','.SACOCHE_WEBMESTRE_BD_USER.','.'--masqué--'.', [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION])</code>'.NL;
  try
  {
    $BD_handler_root = new PDO( 'mysql:host='.SACOCHE_WEBMESTRE_BD_HOST.';port='.SACOCHE_WEBMESTRE_BD_PORT , SACOCHE_WEBMESTRE_BD_USER , SACOCHE_WEBMESTRE_BD_PASS , $PDO_options );
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
    return;
  }
  echo'<code class="v">OK</code>'.NL;

  echo'<h2>4.2 Supprimer la base "sac_base_0"</h2>'.NL;
  $query = 'DROP DATABASE '.SACOCHE_STRUCTURE_BD_NAME;
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>4.3 Retirer les droits de "sac_user_0"</h2>'.NL;
  $query = 'REVOKE ALL PRIVILEGES, GRANT OPTION FROM '.SACOCHE_STRUCTURE_BD_USER.'@"localhost"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }
  $query = 'REVOKE ALL PRIVILEGES, GRANT OPTION FROM '.SACOCHE_STRUCTURE_BD_USER.'@"%"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>4.4 Supprimer le user "sac_user_0"</h2>'.NL;
  $query = 'DROP USER '.SACOCHE_STRUCTURE_BD_USER.'@"localhost"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }
  $query = 'DROP USER '.SACOCHE_STRUCTURE_BD_USER.'@"%"';
  echo'<code>'.$query.'</code>'.NL;
  try
  {
    $result = $BD_handler_root->query($query);
    echo'<code class="v">OK</code>'.NL;
  }
  catch (PDOException $e)
  {
    echo'<code class="r">'.afficher_PDO_exception($e).'</code>'.NL;
  }

  echo'<h2>4.5 Fermer la connexion webmestre</h2>'.NL;
  $BD_handler_root = NULL;
  echo'<code>RAS</code>'.NL;

  echo'<hr>'.NL;

}

?>
