<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Courriels en erreur'));

// Ajout exceptionnel de css
Layout::add( 'css_inline' , 'tr.new td.detail {white-space:pre-wrap}' );
Layout::add( 'css_inline' , 'tr.vue td.detail {font-size:0;opacity:0}' );
// Javascript
Layout::add( 'js_inline_before' , 'window.COURRIEL_LONGUEUR_MAX = '.COURRIEL_LONGUEUR_MAX.';' );
// $js_tab_erreur_infos = array();
?>

<p>
  L’envoi de messages à des adresses erronées nuit à la réputation du serveur.<br>
  Son adresse IP peut alors être mise sur liste noire par les fournisseurs de messagerie.<br>
  Ce qui provoque le rejet de tous les courriels (légitimes), alors considérés comme indésirables par ces serveurs.<br>
  D’où l’utilité de recenser autant que possible les adresses concernées.
</p>

<p class="astuce">
  Ajouter une adresse la retire aux utilisateurs concernés et empêche sa ré-utilisation ultérieure.<br>
  Il n’est pas proposé de modification ou de suppression d’une adresse saisie car une mauvaise adresse ne devrait pas devenir valide.
</p>

<p class="astuce">
  Rectifier l’adresse de courriel associée à un utilisateur ne retire pas le bannissement de l’ancienne mauvaise adresse, qui reste donc définitivement listée ci-dessous à titre informatif.
</p>

<hr>

<p class="b">Double-cliquer sur une ligne du tableau pour voir ou masquer le commentaire associé.</p>

<table id="table_action" class="form hsort">
  <thead>
    <tr>
      <th>Courriel</th>
      <th>Date d’ajout</th>
      <th>Utilisateur(s)</th>
      <th>Commentaire</th>
      <th class="nu"><q class="ajouter"<?php echo infobulle('Ajouter une adresse.') ?>></q></th>
    </tr>
  </thead>
  <tbody id="table_body">
    <?php
    // Lister les adresses
    $DB_TAB = DB_STRUCTURE_COURRIEL_ERREUR::DB_lister_erreurs();
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        // Afficher une ligne du tableau
        echo'<tr id="id_'.$DB_ROW['erreur_id'].'" class="vue">';
        echo  '<td>'.html($DB_ROW['erreur_email']).'</td>';
        echo  '<td>'.To::date_sql_to_french($DB_ROW['erreur_date']).'</td>';
        echo  '<td>'.html($DB_ROW['erreur_users']).'</td>';
        echo  '<td class="detail">'.html($DB_ROW['erreur_info']).'</td>'; // Outil::afficher_texte_tronque($DB_ROW['erreur_info'],60)
        echo  '<td class="nu">';
        // echo    '<q class="modifier"'.infobulle('Modifier cette adresse ou son commentaire.').'></q>';
        // echo    '<q class="supprimer"'.infobulle('Débloquer cette adresse.').'></q>';
        echo  '</td>';
        echo'</tr>'.NL;
        // Javascript
        // $js_tab_erreur_infos[$DB_ROW['erreur_id']] = convertCRtoJS(html($DB_ROW['erreur_info']));
      }
    }
    else
    {
      echo'<tr class="vide"><td class="nu" colspan="4"></td><td class="nu"></td></tr>'.NL;
    }
    // Layout::add( 'js_inline_before' , 'window.js_tab_erreur_infos = '.json_encode_unescapeCR($js_tab_erreur_infos).';' );
    ?>
  </tbody>
</table>

<form action="#" method="post" id="form_gestion" class="hide">
  <h2>Ajouter | Modifier | Supprimer une adresse</h2>
  <div id="gestion_edit">
    <p>
      <label class="tab" for="f_courriel">Courriel :</label><input id="f_courriel" name="f_courriel" type="text" value="" size="75" maxlength="<?php echo COURRIEL_LONGUEUR_MAX ?>"><br>
      <label class="tab" for="f_users">Utilisateur(s) :</label><input id="f_users" name="f_users" type="text" value="" size="75" maxlength="200"><br>
      <label class="tab" for="f_message">Commentaire :</label><textarea id="f_message" name="f_message" rows="5" cols="75"></textarea><br>
      <span class="tab"></span><label id="f_message_reste"></label><br>
    </p>
  </div>
  <div id="gestion_delete">
    <p>Confirmez-vous le déblocage de l’adresse &laquo;&nbsp;<b id="gestion_delete_courriel"></b>&hellip;&nbsp;&raquo; ?</p>
  </div>
  <p>
    <span class="tab"></span><input id="f_action" name="f_action" type="hidden" value=""><input id="f_id" name="f_id" type="hidden" value=""><input id="f_date" name="f_date" type="hidden" value=""><button id="bouton_valider" type="button" class="valider">Valider.</button> <button id="bouton_annuler" type="button" class="annuler">Annuler.</button><label id="ajax_msg_gestion">&nbsp;</label>
  </p>
</form>
