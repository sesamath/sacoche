<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if(($_SESSION['SESAMATH_ID']==ID_DEMO)&&($_POST['f_action']!='charger_referentiel')){Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action        = Clean::post('f_action'    , 'texte');
$matiere_id    = Clean::post('f_matiere'   , 'entier');
$niveau_id     = Clean::post('f_niveau'    , 'entier');
$cycle_id      = Clean::post('f_cycle'     , 'entier');
$composante_id = Clean::post('f_composante', 'entier');
$item_id       = Clean::post('f_item'      , 'entier');
$etat          = Clean::post('f_etat'      , 'entier');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Afficher un référentiel avec ses liaisons au socle
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='charger_referentiel') && $matiere_id && $niveau_id )
{
  // Une 1ère requête pour les liaisons au socle commun
  $DB_TAB_socle2016 = DB_STRUCTURE_REFERENTIEL::DB_recuperer_socle2016_for_referentiel_matiere_niveau( $matiere_id , $niveau_id , 'ids' /*format*/ );
  // On passe au référentiel
  $DB_TAB = DB_STRUCTURE_COMMUN::DB_recuperer_arborescence( 0 /*prof_id*/ , $matiere_id , $niveau_id , FALSE /*only_socle*/ , TRUE /*only_item*/ , FALSE /*s2016_count*/ , FALSE /*item_comm*/ , FALSE /*item_module*/ );
  $tab_js_retour = array();
  $domaine_id = 0;
  $theme_id   = 0;
  $item_id    = 0;
  foreach($DB_TAB as $DB_ROW)
  {
    if($DB_ROW['domaine_id']!=$domaine_id)
    {
      $domaine_id  = $DB_ROW['domaine_id'];
      $domaine_nom = $DB_ROW['domaine_nom'];
    }
    if($DB_ROW['theme_id']!=$theme_id)
    {
      $theme_id = $DB_ROW['theme_id'];
      $tab_js_retour[] = array(
        'id'  => 0,
        'nom' => html($DB_ROW['domaine_nom']).'<br>'.html($DB_ROW['theme_nom']),
      );
    }
    if($DB_ROW['item_id']!=$item_id)
    {
      $item_id = $DB_ROW['item_id'];
      $tab_socle = empty($DB_TAB_socle2016[$item_id]) ? NULL : $DB_TAB_socle2016[$item_id] ;
      $tab_js_retour[] = array(
        'id'    => (int)$item_id,
        'nom'   => html($DB_ROW['item_nom']),
        'socle' => $tab_socle,
      );
    }
  }
  Json::add_row( 'tab_retour' , json_encode($tab_js_retour) );
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modification d’une liaison au socle
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier_etat') && $cycle_id && $composante_id && $item_id && !is_null($etat) )
{
  $valeur = $cycle_id*100+$composante_id;
  $test_modif = DB_STRUCTURE_REFERENTIEL::DB_modifier_referentiel_items( 'item' /*granulosite*/ , 0 /*matiere_id*/ , $item_id , 'socle2016' , $valeur , $etat );
  if($test_modif)
  {
    Json::end( TRUE );
  }
  else
  {
    Json::end( FALSE , 'Contenu inchangé ou item non trouvé !' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
