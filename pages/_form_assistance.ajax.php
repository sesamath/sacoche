<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

if( !$_SESSION['USER_EMAIL'] )
{
  Json::end( FALSE , 'Sinon, pour pouvoir contacter l’assistance, votre adresse de courriel est nécessaire, or elle n’est pas renseignée&nbsp;: <a href="./index.php?page=compte_email">veuillez y remédier avant de poursuivre.</a>' );
}

if( !$_SESSION['SESAMATH_ID'] || !$_SESSION['SESAMATH_KEY'] )
{
  Json::end( FALSE , 'Sinon, pour pouvoir contacter l’assistance, un administrateur doit avoir identifié l’établissement dans la base Sésamath (<span class="manuel"><a class="pop_up" href="'.SERVEUR_DOCUMENTAIRE.'?fichier=support_administrateur__gestion_informations_structure">DOC : Gestion de l’identité de l’établissement</a></span>).' );
}

$form = '<input type="hidden" name="f_action" value="contacter_support">'
      . '<input type="hidden" name="f_user_id" value="'.$_SESSION['USER_ID'].'">'
      . '<input type="hidden" name="f_user_profil" value="'.html($_SESSION['USER_PROFIL_TYPE']).'">'
      . '<input type="hidden" name="f_user_nom" value="'.html($_SESSION['USER_NOM']).'">'
      . '<input type="hidden" name="f_user_prenom" value="'.html($_SESSION['USER_PRENOM']).'">'
      . '<input type="hidden" name="f_user_courriel" value="'.html($_SESSION['USER_EMAIL']).'">'
      . '<input type="hidden" name="f_sesamath_id" value="'.$_SESSION['SESAMATH_ID'].'">'
      . '<input type="hidden" name="f_sesamath_uai" value="'.html($_SESSION['SESAMATH_UAI']).'">'
      . '<input type="hidden" name="f_sesamath_type_nom" value="'.html($_SESSION['SESAMATH_TYPE_NOM']).'">'
      . '<input type="hidden" name="f_etablissement_id" value="'.$_SESSION['BASE'].'">'
      . '<input type="hidden" name="f_etablissement_adresse" value="'.html($_SESSION['ETABLISSEMENT']['ADRESSE1'].' / '.$_SESSION['ETABLISSEMENT']['ADRESSE2'].' / '.$_SESSION['ETABLISSEMENT']['ADRESSE3']).'">'
      . '<input type="hidden" name="f_serveur_url" value="'.html(URL_INSTALL_SACOCHE).'">'
      . '<input type="hidden" name="f_serveur_version" value="'.VERSION_PROG.'">'
      . '<button type="submit" class="mail_ecrire">formulez votre demande ici</button>';
Json::end( TRUE , 'En complément, pour signaler une anomalie ou si vous avez besoin d’un contact personnel avec l’équipe de développement, alors '.$form.'.' );

?>
