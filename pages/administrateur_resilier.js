/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    $('#bouton_resiliation').click
    (
      function()
      {
        $.prompt(prompt_etapes);
      }
    );

    var prompt_etapes = {
      etape_2: {
        title   : 'Demande de confirmation (2/3)',
        html    : 'Toutes les données seront supprimées !<br>Souhaitez-vous vraiment résilier l’inscription ?',
        buttons : {
          'Non, c’est une erreur !' : false ,
          'Oui, je confirme !' : true
        },
        submit  : function(event, value, message, formVals) {
          if(value) {
            event.preventDefault();
            $.prompt.goToState('etape_3');
            return false;
          }
          else {
            $('#bouton_annuler').click();
          }
        }
      },
      etape_3: {
        title   : 'Demande de confirmation (3/3)',
        html    : 'Attention : dernière demande de confirmation !!!<br>Pour passer la main, il suffit d’inscrire un autre administrateur.<br>Êtes-vous bien certain de vouloir supprimer l’inscription ?<br>Est-ce définitivement votre dernier mot ???',
        buttons : {
          'Oui, j’insiste !' : true ,
          'Non, surtout pas !' : false
        },
        submit  : function(event, value, message, formVals) {
          if(value) {
            envoyer_action_confirmee();
            return true;
          }
          else {
            $('#bouton_annuler').click();
          }
        }
      }
    };

    function envoyer_action_confirmee()
    {
      $.fancybox( '<label class="loader">'+'En cours&hellip;'+'</label>' );
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page='+window.PAGE,
          data : 'csrf='+window.CSRF+'&f_action=envoyer_courriel_resiliation',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $.fancybox( '<label class="alerte">'+afficher_json_message_erreur(jqXHR,textStatus)+'</label>' );
            return false;
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==false)
            {
              $.fancybox( '<label class="alerte">'+responseJSON['value']+'</label>' );
              return false;
            }
            else
            {
              $.fancybox( '<label class="valide">'+'Demande envoyée au webmestre'+'</label>' );
            }
          }
        }
      );
    }

  }
);
