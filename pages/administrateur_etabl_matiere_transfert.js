/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Clic sur le bouton pour déplacer les référentiels d’une matière vers une autre
// ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#deplacer_referentiels').click
    (
      function()
      {
        var matiere_id_avant = entier( $('#f_matiere_avant option:selected').val() );
        var matiere_id_apres = entier( $('#f_matiere_apres option:selected').val() );
        if(!matiere_id_avant)
        {
          $('#ajax_msg_move').attr('class','erreur').html('Sélectionner une ancienne matière !');
          $('#f_matiere_avant').focus();
          return false;
        }
        if(!matiere_id_apres)
        {
          $('#ajax_msg_move').attr('class','erreur').html('Sélectionner une nouvelle matière !');
          $('#f_matiere_apres').focus();
          return false;
        }
        if(matiere_id_avant==matiere_id_apres)
        {
          $('#ajax_msg_move').attr('class','erreur').html('Sélectionner des matières différentes !');
          return false;
        }
        var matiere_nom_avant = $('#f_matiere_avant option:selected').text();
        var matiere_nom_apres = $('#f_matiere_apres option:selected').text();
        $('button').prop('disabled',true);
        $('#ajax_msg_move').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=deplacer_referentiels'+'&f_id_avant='+matiere_id_avant+'&f_id_apres='+matiere_id_apres+'&f_nom_avant='+encodeURIComponent(matiere_nom_avant)+'&f_nom_apres='+encodeURIComponent(matiere_nom_apres),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('button').prop('disabled',false);
              $('#ajax_msg_move').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#f_matiere_avant option[value='+matiere_id_avant+']').remove();
                $('#f_matiere_apres option[value='+matiere_id_avant+']').remove();
                $('#f_matiere_apres option[value=0]').prop('selected',true);
                $('#id_'+matiere_id_avant).remove();
                $('#ajax_msg_move').attr('class','valide').html('Transfert effectué.');
              }
              else
              {
                $('#ajax_msg_move').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

  }
);
