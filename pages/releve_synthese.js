/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var objet           = $('#f_objet option:selected').val();
    var matiere_id      = 0;
    var prof_id         = 0;
    var groupe_id       = $('#f_groupe option:selected').val();
    var groupe_type     = $('#f_groupe option:selected').parent().attr('label'); // Il faut indiquer une valeur initiale au moins pour le profil élève
    var eleves_ordre    = '';
    var action_matiere  = 'ajouter';
    var action_matieres = 'ajouter';
    var action_prof     = 'ajouter';

    // Rechercher automatiquement la meilleure période et le niveau du groupe au chargement de la page (uniquement pour un élève, seul cas où la classe est préselectionnée)
    selectionner_periode_adaptee();
    reporter_niveau_groupe();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Voir / masquer tous les détails (pour les élèves et les parents c'est affiché dans la page)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bilan').on
    (
      'click',
      '#montrer_details',
      function()
      {
        $('body').find('a.toggle_plus').click();
        $(this).replaceWith('<a href="#" id="masquer_details">tout masquer</a>');
        return false;
      }
    );

    $('#bilan').on
    (
      'click',
      '#masquer_details',
      function()
      {
        $('body').find('a.toggle_moins').click();
        $(this).replaceWith('<a href="#" id="montrer_details">tout montrer</a>');
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enlever le message ajax et le résultat précédent au changement d’un élément de formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_select').on
    (
      'change',
      'select, input',
      function()
      {
        $('#ajax_msg').removeAttr('class').html('');
        $('#bilan').html('');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Afficher / masquer des éléments du formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_objet').change
    (
      function()
      {
        // on masque
        $('#choix_matiere , #choix_matieres , #span_not_multimatiere').hide();
        // on affiche
        objet = $('#f_objet option:selected').val();
        if(objet=='matiere')
        {
          $('#choix_matiere , #span_not_multimatiere').show();
        }
        else if(objet=='matieres')
        {
          $('#choix_matieres').show();
        }
        else if(objet=='multimatiere')
        {
          // Rien à afficher de particulier
        }
      }
    );

    function visibility_option_with_coef()
    {
      if( $('#f_type_synthese').is(':checked') && $('#f_synthese_indicateur_moyenne_pourcentage').is(':checked') )
      {
        $('#option_with_coef').attr('class','show');
      }
      else
      {
        $('#option_with_coef').attr('class','hide');
      }
    }

    $('#f_type_synthese').click
    (
      function()
      {
        $('#options_synthese').toggle();
        visibility_option_with_coef();
      }
    );

    $('#f_synthese_indicateur_moyenne_pourcentage, #f_synthese_indicateur_pourcentage_acquis').click
    (
      function()
      {
        visibility_option_with_coef();
      }
    );

    var autoperiode = true; // Tant qu’on ne modifie pas manuellement le choix des périodes, modification automatique du formulaire

    function view_dates_perso()
    {
      var periode_val = $('#f_periode').val();
      if(periode_val!=0)
      {
        $('#dates_perso').attr('class','hide');
      }
      else
      {
        $('#dates_perso').attr('class','show');
      }
    }

    $('#f_periode').change
    (
      function()
      {
        view_dates_perso();
        autoperiode = false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger tous les profs d’une classe (approximativement) ou n’affiche que le prof connecté
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_prof_connecte()
    {
      $('#f_prof').html('<option value="'+window.USER_ID+'">'+window.USER_TEXTE+'</option>');
      action_prof = 'ajouter';
      $('#retirer_prof').hide(0);
      $('#ajouter_prof').show(0);
    }

    function charger_profs_groupe()
    {
      $('button').prop('disabled',true);
      prof_id     = $('#f_prof   option:selected').val();
      groupe_id   = $('#f_groupe option:selected').val();
      groupe_type = $('#f_groupe option:selected').parent().attr('label');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_profs_groupe',
          data : 'f_prof='+prof_id+'&f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('button').prop('disabled',false);
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('button').prop('disabled',false);
            if(responseJSON['statut']==true)
            {
              $('#f_prof').html(responseJSON['value']);
              action_prof = 'retirer';
              $('#ajouter_prof').hide(0);
              $('#retirer_prof').show(0);
            }
          }
        }
      );
    }

    $('#ajouter_prof , #retirer_prof').click
    (
      function()
      {
        if(action_prof=='retirer')
        {
          afficher_prof_connecte();
        }
        else if(action_prof=='ajouter')
        {
          charger_profs_groupe();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Changement de groupe
    // -> choisir automatiquement la meilleure période si un changement manuel de période n’a jamais été effectué
    // -> afficher le formulaire de périodes s’il est masqué
    // -> choisir automatiquement le niveau du groupe associé
    // -> afficher le formulaire des options s’il est masqué
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function selectionner_periode_adaptee()
    {
      if(typeof(window.tab_groupe_periode[groupe_id])!='undefined')
      {
        for(var periode_id in window.tab_groupe_periode[groupe_id]) // Parcourir un tableau associatif...
        {
          var tab_split = window.tab_groupe_periode[groupe_id][periode_id].split('_');
          if( (window.TODAY_SQL>=tab_split[0]) && (window.TODAY_SQL<=tab_split[1]) )
          {
            $('#f_periode option[value='+periode_id+']').prop('selected',true);
            view_dates_perso();
            break;
          }
        }
      }
    }

    function reporter_niveau_groupe()
    {
      if(typeof(window.tab_groupe_niveau[groupe_id])!='undefined')
      {
        $('#f_niveau').val(window.tab_groupe_niveau[groupe_id][0]);
        $('#niveau_nom').html(window.tab_groupe_niveau[groupe_id][1]);
      }
    }

    $('#f_groupe').change
    (
      function()
      {
        // remettre à jour ces deux valeurs
        groupe_id   = $('#f_groupe option:selected').val();
        groupe_type = $('#f_groupe option:selected').parent().attr('label');
        $('#f_periode option').each
        (
          function()
          {
            var periode_id = $(this).val();
            // La période personnalisée est tout le temps accessible
            if(periode_id!=0)
            {
              // classe ou groupe classique -> toutes périodes accessibles
              if(groupe_type!='Besoins')
              {
                $(this).prop('disabled',false);
              }
              // groupe de besoin -> desactiver les périodes prédéfinies
              else
              {
                $(this).prop('disabled',true);
              }
            }
          }
        );
        // Sélectionner si besoin la période personnalisée
        if(groupe_type=='Besoins')
        {
          $('#f_periode option[value=0]').prop('selected',true);
          $('#dates_perso').attr('class','show');
        }
        // Modification automatique du formulaire : périodes
        if(autoperiode)
        {
          if( (typeof(groupe_type)!='undefined') && (groupe_type!='Besoins') )
          {
            // Rechercher automatiquement la meilleure période
            selectionner_periode_adaptee();
          }
          // Afficher la zone de choix des périodes
          if(typeof(groupe_type)!='undefined')
          {
            $('#zone_periodes').removeAttr('class');
          }
          else
          {
            $('#zone_periodes').addClass('hide');
          }
        }
        // Modification automatique du formulaire : niveau du groupe
        $('#f_restriction_niveau').prop('disabled', groupe_type=='Besoins' );
        if(groupe_id)
        {
          reporter_niveau_groupe();
          // Afficher la zone des options
          if($('#zone_options').hasClass('hide'))
          {
            $('#zone_options').removeAttr('class');
          }
        }
        else
        {
          $('#zone_options').addClass('hide');
        }
        // Afficher la zone de choix des enseignants
        if(typeof(groupe_type)!='undefined')
        {
          $('#info_profs').addClass('hide');
          $('#zone_profs').removeAttr('class');
        }
        else
        {
          $('#zone_profs').addClass('hide');
          $('#info_profs').removeAttr('class');
        }
        // Rechercher automatiquement la liste des profs
        if( (typeof(groupe_type)!='undefined') && (groupe_type!='Besoins') )
        {
          if( (window.USER_PROFIL_TYPE!='professeur') || (action_prof=='retirer') )
          {
            charger_profs_groupe();
          }
        }
        else
        {
          afficher_prof_connecte();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger les selects f_eleve (pour le professeur et le directeur) et f_matiere (pour le directeur) en ajax
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_matiere(groupe_id,matiere_id) // Uniquement pour un directeur
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_matieres',
          data : 'f_groupe='+groupe_id+'&f_matiere='+matiere_id+'&f_multiple=0',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#f_matiere').html(responseJSON['value']).show();
              maj_eleve(groupe_id,groupe_type,eleves_ordre);
            }
            else
            {
              $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    function maj_choix_tri_eleves(groupe_id,groupe_type)
    {
      if( (groupe_type=='Classes') && (window.PROFIL_TYPE!='professeur') )
      {
        $('#bloc_ordre').hide();
      }
      else
      {
        var nb_options_possibles = 0;
        $('#f_eleves_ordre option').each
        (
          function()
          {
            var value = $(this).val();
            if( (value=='nom') || (value=='prenom') )
            {
              $(this).prop('disabled',false);
              nb_options_possibles++;
            }
            else if(value=='classe')
            {
              if(groupe_type=='Classes')
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
              else
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
            }
            else
            {
              var plan_groupe = $(this).data('data');
              if(groupe_id==plan_groupe)
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
              else
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
            }
          }
        );
        $('#bloc_ordre').hideshow( nb_options_possibles > 1 );
      }
    }

    function maj_eleve(groupe_id,groupe_type,eleves_ordre)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eleves',
          data : 'f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_eleves_ordre='+eleves_ordre+'&f_statut=1'+'&f_multiple='+window.is_multiple+'&f_filter='+window.is_multiple+'&f_selection=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj').removeAttr('class').html('');
              $(window.champ_eleve).html(responseJSON['value']).parent().show();
              if( !window.is_multiple && ($('#f_eleve option').length==2) )
              {
                // Cas d’un seul élève retourné dans le regroupement (en particulier pour un parent de plusieurs enfants)
                $('#f_eleve option').eq(1).prop('selected',true);
              }
            }
            else
            {
              $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    $('#f_groupe').change
    (
      function()
      {
        // Pour un directeur, on met à jour f_matiere (on mémorise avant matiere_id) puis f_eleve
        // Pour un professeur ou un parent de plusieurs enfants, on met à jour f_eleve uniquement
        // Pour un élève ou un parent d’un seul enfant cette fonction n’est pas appelée puisque son groupe (masqué) ne peut être changé
        if(window.PROFIL_TYPE=='directeur')
        {
          matiere_id = $('#f_matiere').val();
          $('#f_matiere').html('<option value="">&nbsp;</option>').hide();
        }
        $(window.champ_eleve).html('').parent().hide();
        groupe_id = $('#f_groupe option:selected').val();
        if(groupe_id)
        {
          $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
          groupe_type  = $('#f_groupe option:selected').parent().attr('label');
          maj_choix_tri_eleves(groupe_id,groupe_type);
          eleves_ordre = $('#f_eleves_ordre option:selected').val();
          if(window.PROFIL_TYPE=='directeur')
          {
            maj_matiere(groupe_id,matiere_id);
          }
          else if( (window.PROFIL_TYPE=='professeur') || (window.PROFIL_TYPE=='parent') )
          {
            maj_eleve(groupe_id,groupe_type,eleves_ordre);
          }
        }
        else
        {
          $('#bloc_ordre').hide();
          $('#ajax_maj').removeAttr('class').html('');
        }
      }
    );

    $('#f_eleves_ordre').change
    (
      function()
      {
        $(window.champ_eleve).html('').parent().hide();
        $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
        groupe_id    = $('#f_groupe option:selected').val();
        groupe_type  = $('#f_groupe option:selected').parent().attr('label');
        maj_choix_tri_eleves(groupe_id,groupe_type);
        eleves_ordre = $('#f_eleves_ordre option:selected').val();
        maj_eleve(groupe_id,groupe_type,eleves_ordre);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger toutes les matières ou seulement les matières affectées (pour un prof)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#ajouter_matiere, #retirer_matiere').click
    (
      function()
      {
        $('button').prop('disabled',true);
        var matiere_id = $('#f_matiere option:selected').val();
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=_maj_select_matieres_prof',
            data : 'f_matiere='+matiere_id+'&f_action='+action_matiere+'&f_multiple=0',
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('button').prop('disabled',false);
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                action_matiere = (action_matiere=='ajouter') ? 'retirer' : 'ajouter' ;
                $('#ajouter_matiere').hideshow( action_matiere == 'ajouter' );
                $('#retirer_matiere').hideshow( action_matiere == 'retirer' );
                $('#f_matiere').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    $('#ajouter_matieres, #retirer_matieres').click
    (
      function()
      {
        $('button').prop('disabled',true);
        var selection_id = []; $('#f_matieres input:checked').each(function(){selection_id.push($(this).val());});
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=_maj_select_matieres_prof',
            data : 'f_selection='+selection_id+'&f_action='+action_matieres+'&f_multiple=1',
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('button').prop('disabled',false);
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                action_matieres = (action_matieres=='ajouter') ? 'retirer' : 'ajouter' ;
                $('#ajouter_matieres').hideshow( action_matieres == 'ajouter' );
                $('#retirer_matieres').hideshow( action_matieres == 'retirer' );
                $('#ajax_matieres').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Soumettre le formulaire principal
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_select');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_objet               : { required:true },
          'f_matieres[]'        : { required:function(){return objet=='matieres';} , minlength:2 },
          f_matiere             : { required:function(){return objet=='matiere';} },
          'f_type[]'            : { required:true },
          f_synthese_indicateur : { required:function(){return $('#f_type_synthese').is(':checked');} },
          f_groupe              : { required:true },
          'f_eleve[]'           : { required:true },
          f_eleves_ordre        : { required:true },
          f_periode             : { required:true },
          f_date_debut          : { required:function(){return $('#f_periode').val()==0;} , dateITA:true },
          f_date_fin            : { required:function(){return $('#f_periode').val()==0;} , dateITA:true },
          f_retroactif          : { required:true },
          f_mode_synthese       : { required:function(){return objet=='matiere';} },
          f_fusion_niveaux      : { required:false },
          f_coef                : { required:false },
          f_socle               : { required:false },
          f_lien                : { required:false },
          f_panier              : { required:false },
          f_start               : { required:false },
          f_prop_sans_score     : { required:false },
          f_restriction_socle   : { required:false },
          f_only_diagnostic     : { required:true },
          f_only_prof           : { required:false },
          f_prof                : { required:function(){return $('#f_only_prof').is(':checked');} },
          f_restriction_niveau  : { required:false },
          f_couleur             : { required:true },
          f_fond                : { required:true },
          f_legende             : { required:true },
          f_marge_min           : { required:true }
        },
        messages :
        {
          f_objet               : { required:'objet manquant' },
          'f_matieres[]'        : { required:'matière(s) manquante(s)' , minlength:'cocher plusieurs matières ou choisir l’objet dédié à une seule matière' },
          f_matiere             : { required:'matière manquante' },
          'f_type[]'            : { required:'type(s) manquant(s)' },
          f_synthese_indicateur : { required:'choix manquant' },
          f_groupe              : { required:'groupe manquant' },
          'f_eleve[]'           : { required:'élève(s) manquant(s)' },
          f_eleves_ordre        : { required:'ordre manquant' },
          f_periode             : { required:'période manquante' },
          f_date_debut          : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_date_fin            : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_retroactif          : { required:'choix manquant' },
          f_mode_synthese       : { required:'choix manquant' },
          f_fusion_niveaux      : { },
          f_coef                : { },
          f_socle               : { },
          f_lien                : { },
          f_panier              : { },
          f_start               : { },
          f_prop_sans_score     : { },
          f_restriction_socle   : { },
          f_only_diagnostic     : { required:'choix manquant' },
          f_only_prof           : { },
          f_prof                : { required:'enseignant manquant' },
          f_restriction_niveau  : { },
          f_couleur             : { required:'couleur manquante' },
          f_fond                : { required:'fond manquant' },
          f_legende             : { required:'légende manquante' },
          f_marge_min           : { required:'marge mini manquante' }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          if(element.attr('id')=='f_matiere') { element.next().after(error); }
          else if(element.is('select')) {element.after(error);}
          else if(element.attr('type')=='text') {element.next().after(error);}
          else if(element.attr('type')=='radio') {element.parent().next().after(error);}
          else if(element.attr('type')=='checkbox') {
            if(element.parent().parent().hasClass('select_multiple')) {element.parent().parent().next().after(error);}
            else {element.parent().next().after(error);}
          }
        }
        // success: function(label) {label.text('ok').attr('class','valide');} Pas pour des champs soumis à vérification PHP
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      filtering: filtrer_checkbox_multiple,
      beforeSerialize : action_form_avant_serialize,
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        // récupération d’éléments
        $('#f_matiere_nom').val( $('#f_matiere option:selected').text() );
        $('#f_groupe_nom' ).val( $('#f_groupe  option:selected').text() );
        $('#f_prof_texte' ).val( $('#f_prof    option:selected').text() );
        $('#f_groupe_type').val( groupe_type );
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function filtrer_checkbox_multiple(el, index)
    {
      // Éviter la soumission des checkbox nombreux afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
      if( ( $(el).attr('type')!='checkbox' ) || ( $(el).attr('name')!='f_eleve[]' ) )
      {
        return el;
      }
    }

    // Fonction précédent le traitement du formulaire (avec jquery.form.js)
    function action_form_avant_serialize(jqForm, options)
    {
      // Grouper les checkbox dans un champ unique afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
      var tab_eleve = [];
      $('#f_eleve input:checked:enabled').each
      (
        function()
        {
          tab_eleve.push($(this).val());
        }
      );
      $('#f_eleve_report').val(tab_eleve);
    }

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        // Il faut rajouter un test car jquery.validate.js considère corrects les input:checked:disabled
        // Test $(’#f_eleve input’).length car pour un élève ou un parent c’est un select classique.
        if( $('#f_eleve input').length && ( $('#f_eleve input:checked:enabled').length==0 ) )
        {
          $('#ajax_msg').attr('class','erreur').html('élève(s) filtré(s) manquant(s)');
          readytogo = false;
        }
        else
        {
          $('#bouton_valider').prop('disabled',true);
          $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
          $('#bilan').html('');
        }
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      $('#bouton_valider').prop('disabled',false);
      var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : 'Erreur 500&hellip; Mémoire insuffisante ? Sélectionner moins d’élèves à la fois ou demander à votre hébergeur d’augmenter la valeur "memory_limit".' ;
      $('#ajax_msg').attr('class','alerte').html(message);
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      $('#bouton_valider').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
      }
      else if(responseJSON['direct']==true)
      {
        $('#ajax_msg').attr('class','valide').html('Résultat ci-dessous.');
        $('#bilan').html(responseJSON['bilan']);
      }
      else if(responseJSON['direct']==false)
      {
        $('#ajax_msg').removeAttr('class').html('');
        // Mis dans le div bilan et pas balancé directement dans le fancybox sinon la mise en forme des liens nécessite un peu plus de largeur que le fancybox ne recalcule pas (et $.fancybox.update(); ne change rien).
        // Malgré tout, pour Chrome par exemple, la largeur est mal calculée et provoque des retours à la ligne, d’où le minWidth ajouté.
        $('#bilan').html('<p class="noprint">Afin de préserver l’environnement, n’imprimer que si nécessaire !</p>'+responseJSON['bilan']);
        $.fancybox( { href:'#bilan' , onClosed:function(){$('#bilan').html('');} , minWidth:450 } );
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Affichage au chargement
    if(window.PROFIL_TYPE=='eleve')
    {
      $('#bouton_valider').click();
    }

  }
);
