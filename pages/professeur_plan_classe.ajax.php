<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action      = Clean::post('f_action'     , 'texte');
$plan_id     = Clean::post('f_id'         , 'entier');
$groupe_id   = Clean::post('f_groupe'     , 'entier');
$groupe_type = Clean::post('f_groupe_type', 'lettres');
$plan_nom    = Clean::post('f_nom'        , 'texte', 40);
$nb_rangees  = Clean::post('f_rangees'    , 'entier');
$nb_colonnes = Clean::post('f_colonnes'   , 'entier');
$is_nom      = Clean::post('f_is_nom'     , 'bool');
$is_prenom   = Clean::post('f_is_prenom'  , 'bool');
$is_photo    = Clean::post('f_is_photo'   , 'bool');
$is_ordre    = Clean::post('f_is_ordre'   , 'bool');
$is_equipe   = Clean::post('f_is_equipe'  , 'bool');
$is_role     = Clean::post('f_is_role'    , 'bool');
$tab_eleve   = Clean::map( 'entier' , Clean::post('tab_eleve' , array('array',',')) );
$tab_place   = Clean::map( 'texte'  , Clean::post('tab_place' , array('array',',')) );
$tab_ordre   = Clean::map( 'entier' , Clean::post('tab_ordre' , array('array',',')) );
$tab_equipe  = Clean::map( 'texte'  , Clean::post('tab_equipe', array('array',',')) );
$tab_role    = Clean::map( 'texte'  , Clean::post('tab_role'  , array('array','⁞')) );

$verif_post = ($nb_rangees>=2) && ($nb_rangees<=15) && ($nb_colonnes>=2) && ($nb_colonnes<=15);

// Groupe
$tab_types = array(
  'Classes' => 'classe' ,
  'Groupes' => 'groupe' ,
  'Besoins' => 'besoin' ,
);
if( (!$groupe_id) || (!isset($tab_types[$groupe_type])) )
{
  Json::end( FALSE , 'Erreur avec les données transmises !' );
}
$groupe_type = $tab_types[$groupe_type];

// Élèves
$DB_TAB = DB_STRUCTURE_COMMUN::DB_lister_users_regroupement( 'eleve' /*profil_type*/ , 1 /*statut*/ , $groupe_type , $groupe_id , 'nom' /*eleves_ordre*/ );
$nb_eleves = count($DB_TAB);
if(!$nb_eleves)
{
  Json::end( FALSE , 'Aucun élève trouvé dans ce regroupement !' );
}
if($nb_eleves>99)
{
  Json::end( FALSE , 'Trop d’élèves présents dans ce regroupement !' );
}

$tab_eleve_base = array();
foreach($DB_TAB as $DB_ROW)
{
  $tab_eleve_base[$DB_ROW['user_id']] = array(
    'nom'    => $DB_ROW['user_nom'],
    'prenom' => $DB_ROW['user_prenom'],
  );
}
// On vérifie que ce sont bien les élèves du professeur
if($_SESSION['USER_JOIN_GROUPES']=='config')
{
  Outil::verif_eleves_prof( array_keys($tab_eleve_base) );
}

// Fermeture de session (mais pas destruction, juste écriture et libération des données pour éviter un verrouillage en écriture)
Session::write_close();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter un nouveau plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ( ($action=='ajouter') || ( ($action=='dupliquer') && $plan_id ) ) && $plan_nom && $verif_post )
{
  // Vérifier que le plan de classe est assez grand
  $nb_places = $nb_rangees * $nb_colonnes;
  if( $nb_eleves > $nb_places )
  {
    Json::end( FALSE , 'Plan trop petit ('.$nb_places.' places pour '.$nb_eleves.' élèves) !' );
  }
  // Vérifier que le plan de classe est bien à l’enseignant, et récupérer le positionnement des élèves
  if($action=='dupliquer')
  {
    if( !DB_STRUCTURE_PROFESSEUR_PLAN::DB_tester_plan_classe_prof( $plan_id , $_SESSION['USER_ID'] ) )
    {
      Json::end( FALSE , 'Plan de classe introuvable ou pas à vous !' );
    }
    $DB_TAB = DB_STRUCTURE_PROFESSEUR_PLAN::DB_lister_plan_eleves( $plan_id );
  }
  // Insérer l’enregistrement
  $plan_id = DB_STRUCTURE_PROFESSEUR_PLAN::DB_ajouter_plan_classe( $_SESSION['USER_ID'] , $groupe_id , $plan_nom , $nb_rangees , $nb_colonnes );
  // En cas de duplication, si c’est le même groupe, on essaye de placer les élèves au même endroit
  if($action=='dupliquer')
  {
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        if( isset($tab_eleve_base[$DB_ROW['eleve_id']]) && ($DB_ROW['jointure_rangee']<$nb_rangees) && ($DB_ROW['jointure_colonne']<$nb_colonnes) )
        {
          DB_STRUCTURE_PROFESSEUR_PLAN::DB_ajouter_plan_eleve( $plan_id , $DB_ROW['eleve_id'] , $DB_ROW['jointure_rangee'] , $DB_ROW['jointure_colonne'] , $DB_ROW['jointure_ordre'] , $DB_ROW['jointure_equipe'] , $DB_ROW['jointure_role'] );
        }
      }
    }
  }
  // Afficher le retour
  Json::add_str('<tr id="id_'.$plan_id.'" class="new">');
  Json::add_str(  '<td data-id="'.$groupe_id.'">{{GROUPE_NOM}}</td>');
  Json::add_str(  '<td>'.html($plan_nom).'</td>');
  Json::add_str(  '<td>'.$nb_rangees.'</td>');
  Json::add_str(  '<td>'.$nb_colonnes.'</td>');
  Json::add_str(  '<td class="nu">');
  Json::add_str(    '<q class="placer_eleves"'.infobulle('Placer les élèves.').'></q>');
  Json::add_str(    '<q class="imprimer"'.infobulle('Imprimer ce plan.').'></q>');
  Json::add_str(    '<q class="dupliquer"'.infobulle('Dupliquer ce plan.').'></q>');
  Json::add_str(    '<q class="modifier"'.infobulle('Modifier ce plan.').'></q>');
  Json::add_str(    '<q class="supprimer"'.infobulle('Supprimer ce plan.').'></q>');
  Json::add_str(  '</td>');
  Json::add_str('</tr>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier un plan de classe existant
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier') && $plan_id && $plan_nom && $verif_post )
{
  // Vérifier que le plan de classe est assez grand
  $nb_places = $nb_rangees * $nb_colonnes;
  if( $nb_eleves > $nb_places )
  {
    Json::end( FALSE , 'Plan trop petit ('.$nb_places.' places pour '.$nb_eleves.' élèves) !' );
  }
  // Vérifier que le plan de classe est bien à l’enseignant
  if( !DB_STRUCTURE_PROFESSEUR_PLAN::DB_tester_plan_classe_prof( $plan_id , $_SESSION['USER_ID'] ) )
  {
    Json::end( FALSE , 'Plan de classe introuvable ou pas à vous !' );
  }
  // Modifier l’enregistrement
  DB_STRUCTURE_PROFESSEUR_PLAN::DB_modifier_plan_classe( $plan_id , $groupe_id , $plan_nom , $nb_rangees , $nb_colonnes );
  // Afficher le retour
  Json::add_str('<td data-id="'.$groupe_id.'">{{GROUPE_NOM}}</td>');
  Json::add_str('<td>'.html($plan_nom).'</td>');
  Json::add_str('<td>'.$nb_rangees.'</td>');
  Json::add_str('<td>'.$nb_colonnes.'</td>');
  Json::add_str('<td class="nu">');
  Json::add_str(  '<q class="placer_eleves"'.infobulle('Placer les élèves.').'></q>');
  Json::add_str(  '<q class="imprimer"'.infobulle('Imprimer ce plan.').'></q>');
  Json::add_str(  '<q class="dupliquer"'.infobulle('Dupliquer ce plan.').'></q>');
  Json::add_str(  '<q class="modifier"'.infobulle('Modifier ce plan.').'></q>');
  Json::add_str(  '<q class="supprimer"'.infobulle('Supprimer ce plan.').'></q>');
  Json::add_str('</td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Supprimer un plan de classe existant
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='supprimer') && $plan_id )
{
  // Vérifier que le plan de classe est bien à l’enseignant
  if( !DB_STRUCTURE_PROFESSEUR_PLAN::DB_tester_plan_classe_prof( $plan_id , $_SESSION['USER_ID'] ) )
  {
    Json::end( FALSE , 'Plan de classe introuvable ou pas à vous !' );
  }
  // Effacer l’enregistrement
  DB_STRUCTURE_PROFESSEUR_PLAN::DB_supprimer_plan_classe( $plan_id );
  // Afficher le retour
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Charger un plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='charger_eleves') && $plan_id && $verif_post )
{
  // Vérifier que le plan de classe est assez grand
  $nb_places = $nb_rangees * $nb_colonnes;
  if( $nb_eleves > $nb_places )
  {
    Json::end( FALSE , 'Plan trop petit ('.$nb_places.' places pour '.$nb_eleves.' élèves) !' );
  }
  // Vérifier que le plan de classe est bien à l’enseignant
  if( !DB_STRUCTURE_PROFESSEUR_PLAN::DB_tester_plan_classe_prof( $plan_id , $_SESSION['USER_ID'] ) )
  {
    Json::end( FALSE , 'Plan de classe introuvable ou pas à vous !' );
  }
  // On récupère les photos si elles existent
  $coef_reduction = 0.5;
  $img_height = PHOTO_DIMENSION_MAXI * $coef_reduction;
  $img_width  = PHOTO_DIMENSION_MAXI*2/3 * $coef_reduction;
  foreach($tab_eleve_base as $eleve_id => $tab)
  {
    $tab_eleve_base[$eleve_id] += array(
      'img_width'  => $img_width,
      'img_height' => $img_height,
      'img_src'    => '',
      'img_title'  => TRUE,
    );
  }
  $listing_user_id = implode(',',array_keys($tab_eleve_base));
  $DB_TAB = DB_STRUCTURE_IMAGE::DB_lister_images( $listing_user_id , 'photo' );
  if(!empty($DB_TAB))
  {
    foreach($DB_TAB as $DB_ROW)
    {
      $tab_eleve_base[$DB_ROW['user_id']]['img_width']  = $DB_ROW['image_largeur'] * $coef_reduction;
      $tab_eleve_base[$DB_ROW['user_id']]['img_height'] = $DB_ROW['image_hauteur'] * $coef_reduction;
      $tab_eleve_base[$DB_ROW['user_id']]['img_src']    = $DB_ROW['image_contenu'];
      $tab_eleve_base[$DB_ROW['user_id']]['img_title']  = FALSE;
    }
  }
  // Récupèrer le placement des élèves, positionner au passage les élèves s’ils ne l’ont pas été, corriger d’éventuelles anomalies
  $tab_places_occupees = Outil::recuperer_ajuster_places_eleves( $plan_id , $nb_rangees , $nb_colonnes , $tab_eleve_base );
  // Pour les équipes
  $tab_alphabet = str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZ');
  $options_equipe = '<option value=""></option>';
  foreach($tab_alphabet as $n => $lettre)
  {
    $options_equipe .= '<option value="'.$lettre.'" class="e'.$lettre.'">'.$lettre.'</option>';
  }
  $select_equipe = '<select name="f_equipe">'.$options_equipe.'</select>';
  // Retour
  foreach($tab_places_occupees as $jointure_rangee => $tab_colonnes)
  {
    foreach($tab_colonnes as $jointure_colonne => $tab)
    {
      if(is_null($tab))
      {
        $div = '';
      }
      else
      {
        $img_src   = ($tab['img_src'])   ? ' src="data:'.image_type_to_mime_type(IMAGETYPE_JPEG).';base64,'.$tab['img_src'].'"' : ' src="./_img/trombinoscope_vide.png"' ;
        $img_title = ($tab['img_title']) ? infobulle('absence de photo') : '' ;
        $img_html  = '<img width="'.$tab['img_width'].'" height="'.$tab['img_height'].'" alt=""'.$img_src.$img_title.'>';
        $select    = ($tab['equipe']) ? str_replace(array('name="f_equipe"','value="'.$tab['equipe'].'"'),array('name="f_equipe" class="e'.$tab['equipe'].'"','value="'.$tab['equipe'].'" selected="selected"'),$select_equipe) : $select_equipe ;
        $class     = ($tab['equipe']) ? 'e'.$tab['equipe'] : 'hide' ;
        $div = '<div id="id'.$tab['id'].'">'
              . $img_html.html($tab['nom']).'<br>'
              . html($tab['prenom']).'<br>'
              . '<input name="f_ordre" type="number" size="2" min="0" max="99" value="'.$tab['ordre'].'">'.'<br>'
              . $select.'<br>'
              . '<input name="f_role" type="text" size="8" maxlength="15" value="'.html($tab['role']).'" placeholder="rôle" class="'.$class.'">'
              . '</div>' ;
      }
      Json::add_str('<li id="'.$jointure_rangee.'x'.$jointure_colonne.'">'.$div.'</li>');
    }
  }
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Placer les élèves sur un plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='placer_eleves') && $plan_id && (count($tab_eleve)==$nb_eleves) && (count($tab_place)==$nb_eleves) && (count($tab_ordre)==$nb_eleves) && (count($tab_equipe)==$nb_eleves) && (count($tab_role)==$nb_eleves) && $verif_post )
{
  // On récupère les données transmises
  $tab_post = array();
  foreach($tab_eleve as $key => $eleve_id)
  {
    list( $jointure_rangee , $jointure_colonne ) = explode('x',$tab_place[$key]);
    $jointure_ordre  = $tab_ordre[$key];
    $jointure_equipe = $tab_equipe[$key];
    $jointure_role   = $tab_role[$key];
    $jointure_rangee  = Clean::entier($jointure_rangee);
    $jointure_colonne = Clean::entier($jointure_colonne);
    if( !isset($tab_eleve_base[$eleve_id]) || isset($tab_eleve_base[$eleve_id]['ordre']) || ($jointure_rangee<0) || ($jointure_rangee>=$nb_rangees) || ($jointure_colonne<0) || ($jointure_colonne>=$nb_colonnes) || ($jointure_ordre<0) || ($jointure_ordre>99) )
    {
      Json::end( FALSE , 'Erreur avec les données transmises !' );
    }
    $tab_eleve_base[$eleve_id] += array(
      'rangee'  => $jointure_rangee,
      'colonne' => $jointure_colonne,
      'ordre'   => $jointure_ordre,
      'equipe'  => $jointure_equipe,
      'role'    => $jointure_role,
    );
  }
  // Vérifier que le plan de classe est bien à l’enseignant
  if( !DB_STRUCTURE_PROFESSEUR_PLAN::DB_tester_plan_classe_prof( $plan_id , $_SESSION['USER_ID'] ) )
  {
    Json::end( FALSE , 'Plan de classe introuvable ou pas à vous !' );
  }
  // Récupérer le placement enregistré afin de le comparer
  // Les anomalies éventuelles ayant déjà été corrigées à l’affichage, et les données transmises bien vérifiées, on n’effectue que des UPDATE
  $DB_TAB = DB_STRUCTURE_PROFESSEUR_PLAN::DB_lister_plan_eleves( $plan_id );
  if(count($DB_TAB)!=$nb_eleves)
  {
    Json::end( FALSE , 'Incohérence avec les données enregistrées !' );
  }
  foreach($DB_TAB as $DB_ROW)
  {
    $eleve_id = $DB_ROW['eleve_id'];
    if( ($tab_eleve_base[$eleve_id]['rangee']!=$DB_ROW['jointure_rangee']) || ($tab_eleve_base[$eleve_id]['colonne']!=$DB_ROW['jointure_colonne']) || ($tab_eleve_base[$eleve_id]['ordre']!=$DB_ROW['jointure_ordre']) || ($tab_eleve_base[$eleve_id]['equipe']!=$DB_ROW['jointure_equipe']) || ($tab_eleve_base[$eleve_id]['role']!=$DB_ROW['jointure_role']) )
    {
      DB_STRUCTURE_PROFESSEUR_PLAN::DB_modifier_plan_eleve( $plan_id , $eleve_id , $tab_eleve_base[$eleve_id]['rangee'] , $tab_eleve_base[$eleve_id]['colonne'] , $tab_eleve_base[$eleve_id]['ordre'] , $tab_eleve_base[$eleve_id]['equipe'] , $tab_eleve_base[$eleve_id]['role'] );
    }
  }
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Imprimer un plan de classe
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='imprimer') && $plan_id && $plan_nom && $verif_post )
{
  // Vérifier que le plan de classe est assez grand
  $nb_places = $nb_rangees * $nb_colonnes;
  if( $nb_eleves > $nb_places )
  {
    Json::end( FALSE , 'Plan trop petit ('.$nb_places.' places pour '.$nb_eleves.' élèves) !' );
  }
  // On récupère les photos si elles existent (et si demandées)
  if($is_photo)
  {
    $coef_reduction = 1 - ( max($nb_rangees,$nb_colonnes) - 2 ) * 0.05;
    $img_height = PHOTO_DIMENSION_MAXI * $coef_reduction;
    $img_width  = PHOTO_DIMENSION_MAXI*2/3 * $coef_reduction;
    foreach($tab_eleve_base as $eleve_id => $tab)
    {
      $tab_eleve_base[$eleve_id] += array(
        'img_width'  => $img_width,
        'img_height' => $img_height,
        'img_src'    => '',
      );
    }
    $listing_user_id = implode(',',array_keys($tab_eleve_base));
    $DB_TAB = DB_STRUCTURE_IMAGE::DB_lister_images( $listing_user_id , 'photo' );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        $tab_eleve_base[$DB_ROW['user_id']]['img_width']  = $DB_ROW['image_largeur'] * $coef_reduction;
        $tab_eleve_base[$DB_ROW['user_id']]['img_height'] = $DB_ROW['image_hauteur'] * $coef_reduction;
        $tab_eleve_base[$DB_ROW['user_id']]['img_src']    = $DB_ROW['image_contenu'];
      }
    }
  }
  // Récupèrer le placement des élèves, positionner au passage les élèves s’ils ne l’ont pas été, corriger d’éventuelles anomalies
  $tab_places_occupees = Outil::recuperer_ajuster_places_eleves( $plan_id , $nb_rangees , $nb_colonnes , $tab_eleve_base );
  // On attaque le PDF
  $orientation = ( $nb_rangees - 2 < $nb_colonnes ) ? 'landscape' : 'portrait' ;
  $pdf = new PDF_plan_classe( FALSE /*officiel*/ , 'A4' /*page_size*/ , $orientation , 7.5 /*marge_gauche*/ , 7.5 /*marge_droite*/ , 7.5 /*marge_haut*/ , 7.5 /*marge_bas*/ );
  $pdf->initialiser( $plan_nom , $nb_rangees , $nb_colonnes , $is_nom , $is_prenom , $is_photo , $is_ordre , $is_equipe , $is_role );
  foreach($tab_places_occupees as $jointure_rangee => $tab_colonnes)
  {
    foreach($tab_colonnes as $jointure_colonne => $tab)
    {
      $pdf->place_eleve( $jointure_rangee , $jointure_colonne , $tab);
    }
  }
  // Enregistrement du PDF
  $fnom_pdf = 'plan-de-classe_'.$_SESSION['BASE'].'_'.Clean::fichier($plan_nom).'_'.FileSystem::generer_fin_nom_fichier__date_et_alea().'.pdf';
  FileSystem::ecrire_objet_pdf( CHEMIN_DOSSIER_EXPORT.$fnom_pdf , $pdf );
  // Retour
  Json::end( TRUE , '<a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.$fnom_pdf.'"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a>' );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Lister les plans des collègues
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($action=='chercher_plans_collegues')
{
  $DB_TAB = DB_STRUCTURE_PROFESSEUR_PLAN::DB_OPT_lister_plans_collegues_groupe( $_SESSION['USER_ID'] , $groupe_id );
  if(empty($DB_TAB))
  {
    Json::end( TRUE , '<option value="" disabled>&nbsp;</option>' );
  }
  elseif(count($DB_TAB)==1)
  {
    Json::end( TRUE , HtmlForm::afficher_select($DB_TAB , FALSE /*select_nom*/ , FALSE /*option_first*/ , TRUE /*selection*/ , '' /*optgroup*/ ) );
  }
  else
  {
    Json::end( TRUE , HtmlForm::afficher_select($DB_TAB , FALSE /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ , '' /*optgroup*/ ) );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Prévisualiser le plan d’un(e) collègue
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='previsualiser_plan_collegue') && $plan_id )
{
  $DB_ROW = DB_STRUCTURE_PROFESSEUR_PLAN::DB_recuperer_plan_prof( $plan_id );
  if(empty($DB_ROW))
  {
    Json::end( FALSE , 'Plan de classe introuvable !' );
  }
  if( $DB_ROW['groupe_id'] != $groupe_id )
  {
    Json::end( FALSE , 'Plan de classe d’un autre regroupement !' );
  }
  // Vérifier que le plan de classe est assez grand
  $nb_places = $DB_ROW['plan_nb_rangees'] * $DB_ROW['plan_nb_colonnes'];
  if( $nb_eleves > $nb_places )
  {
    Json::end( FALSE , 'Plan trop petit ('.$nb_places.' places pour '.$nb_eleves.' élèves) !' );
  }
  // Récupèrer le placement des élèves, positionner au passage les élèves s’ils ne l’ont pas été, corriger d’éventuelles anomalies
  $tab_places_occupees = Outil::recuperer_ajuster_places_eleves( $plan_id , $DB_ROW['plan_nb_rangees'] , $DB_ROW['plan_nb_colonnes'] , $tab_eleve_base );
  // Retour
  $li = '';
  Json::add_row( 'nb_rangees'  , $DB_ROW['plan_nb_rangees']  );
  Json::add_row( 'nb_colonnes' , $DB_ROW['plan_nb_colonnes'] );
  foreach($tab_places_occupees as $jointure_rangee => $tab_colonnes)
  {
    foreach($tab_colonnes as $jointure_colonne => $tab)
    {
      if(is_null($tab))
      {
        $div = '';
      }
      else
      {
        $div = '<div id="id'.$tab['id'].'">'.html($tab['nom']).'<br>'.html($tab['prenom']).'<br>n°'.$tab['ordre'].'</div>' ;
      }
      $li .= '<li id="'.$jointure_rangee.'x'.$jointure_colonne.'">'.$div.'</li>';
    }
  }
  Json::add_row( 'li' , $li );
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Importer le plan d’un(e) collègue
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='importer_plan_collegue') && $plan_id )
{
  $DB_ROW = DB_STRUCTURE_PROFESSEUR_PLAN::DB_recuperer_plan_prof( $plan_id );
  if(empty($DB_ROW))
  {
    Json::end( FALSE , 'Plan de classe introuvable !' );
  }
  if( $DB_ROW['groupe_id'] != $groupe_id )
  {
    Json::end( FALSE , 'Plan de classe d’un autre regroupement !' );
  }
  $plan_nom    = $DB_ROW['plan_nom'];
  $nb_rangees  = $DB_ROW['plan_nb_rangees'];
  $nb_colonnes = $DB_ROW['plan_nb_colonnes'];
  // Récupérer le positionnement des élèves
  $DB_TAB = DB_STRUCTURE_PROFESSEUR_PLAN::DB_lister_plan_eleves( $plan_id );
  // Insérer l’enregistrement
  $plan_id = DB_STRUCTURE_PROFESSEUR_PLAN::DB_ajouter_plan_classe( $_SESSION['USER_ID'] , $groupe_id , $plan_nom , $nb_rangees , $nb_colonnes );
  // On place les élèves au même endroit
  if(!empty($DB_TAB))
  {
    foreach($DB_TAB as $DB_ROW)
    {
      if( isset($tab_eleve_base[$DB_ROW['eleve_id']]) && ($DB_ROW['jointure_rangee']<$nb_rangees) && ($DB_ROW['jointure_colonne']<$nb_colonnes) )
      {
        DB_STRUCTURE_PROFESSEUR_PLAN::DB_ajouter_plan_eleve( $plan_id , $DB_ROW['eleve_id'] , $DB_ROW['jointure_rangee'] , $DB_ROW['jointure_colonne'] , $DB_ROW['jointure_ordre'] , $DB_ROW['jointure_equipe'] , $DB_ROW['jointure_role'] );
      }
    }
  }
  // Afficher le retour
  Json::add_str('<tr id="id_'.$plan_id.'" class="new">');
  Json::add_str(  '<td data-id="'.$groupe_id.'">{{GROUPE_NOM}}</td>');
  Json::add_str(  '<td>'.html($plan_nom).'</td>');
  Json::add_str(  '<td>'.$nb_rangees.'</td>');
  Json::add_str(  '<td>'.$nb_colonnes.'</td>');
  Json::add_str(  '<td class="nu">');
  Json::add_str(    '<q class="placer_eleves"'.infobulle('Placer les élèves.').'></q>');
  Json::add_str(    '<q class="imprimer"'.infobulle('Imprimer ce plan.').'></q>');
  Json::add_str(    '<q class="dupliquer"'.infobulle('Dupliquer ce plan').'></q>');
  Json::add_str(    '<q class="modifier"'.infobulle('Modifier ce plan').'></q>');
  Json::add_str(    '<q class="supprimer"'.infobulle('Supprimer ce plan').'></q>');
  Json::add_str(  '</td>');
  Json::add_str('</tr>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
