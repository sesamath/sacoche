/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Intercepter la soumission du formulaire de recherche
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_recherche').on(
      'keydown' ,
      '#nom_actuel' ,
      function(e)
      {
        if(e.which==13)  // touche entrée
        {
          $('#bouton_chercher_actuel').click();
        }
      }
    );

    $('#form_recherche').on(
      'keydown' ,
      '#f_separation_date' ,
      function(e)
      {
        if(e.which==13)  // touche entrée
        {
          $('#bouton_selectionner').click();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Activation ou pas du bouton de sélection
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function change_etat_bouton_selection()
    {
      if( $('#f_id_actuel option:selected').val() && $('#f_separation_date').val() )
      {
        $('#bouton_selectionner').prop('disabled',false);
      }
      else
      {
        $('#bouton_selectionner').prop('disabled',true);
      }
    }

    $('#f_id_actuel , #f_separation_date').change
    (
      function()
      {
        change_etat_bouton_selection();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire de recherche
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_chercher_actuel').click
    (
      function()
      {
        $('#bouton_selectionner').prop('disabled',true);
        $('#ajax_msg').removeAttr('class').html('');
        var nom = $('#nom_actuel').val();
        if( !nom )
        {
          $('#ajax_msg_actuel').attr('class','erreur').html('Entrer un nom !');
          return false;
        }
        $('#bouton_chercher_actuel').prop('disabled',true);
        $('#ajax_msg_actuel').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action='+'chercher'+'&f_nom='+encodeURIComponent(nom),
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg_actuel').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              $('#bouton_chercher_actuel').prop('disabled',false);
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#bouton_chercher_actuel').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#ajax_msg_actuel').attr('class','valide').html('');
                $('#f_id_actuel').html(responseJSON['value']).show(0);
                change_etat_bouton_selection();
              }
              else
              {
                $('#ajax_msg_actuel').attr('class','alerte').html(responseJSON['value']);
                $('#f_id_actuel').hide(0);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du formulaire de sélection
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_recherche');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_id_actuel       : { required:true },
          f_separation_date : { required:true , dateITA:true }
        },
        messages :
        {
          f_id_actuel       : { required:'compte manquant' },
          f_separation_date : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          if(element.attr('id')=='f_separation_date') { element.next().after(error); }
          else {element.after(error);}
        }
        // success: function(label) {label.text('ok').attr('class','valide');} Pas pour des champs soumis à vérification PHP
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg').removeAttr('class').html('');
      var readytogo = validation.form();
      if(readytogo)
      {
        $('#bouton_selectionner').prop('disabled',true);
        $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      $('#bouton_selectionner').prop('disabled',false);
      $('#ajax_msg').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      if(responseJSON['statut']==true)
      {
        $('#ajax_msg').attr('class','valide').html('Comptes séparés.');
        $('#nom_actuel').val('');
        $('#f_separation_date').val('');
        $('#f_id_actuel').html('<option value="">&nbsp;</option>').hide(0);
        $('#ajax_msg_actuel').removeAttr('class').html('');
        $('#bouton_selectionner').prop('disabled',true);
      }
      else
      {
        $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
        $('#bouton_selectionner').prop('disabled',false);
      }
    }

  }
);
