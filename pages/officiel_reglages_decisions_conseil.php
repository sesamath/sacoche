<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Décisions du conseil de classe'));
?>

<div><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=officiel__decisions_conseil">DOC : Décisions du conseil de classe</a></span></div>

<hr>

<h2>Mentions</h2>

<table id="table_mention" class="form hsort">
  <thead>
    <tr>
      <th>Ordre</th>
      <th>Synthèse</th>
      <th>Contenu</th>
      <th class="nu"><q class="ajouter"<?php echo infobulle('Ajouter une décision.') ?>></q></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $DB_TAB = DB_STRUCTURE_OFFICIEL::DB_lister_officiel_decision( 'mention' , TRUE /*avec_usage*/ );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        // Afficher une ligne du tableau
        echo'<tr id="id_'.$DB_ROW['decision_id'].'" data-used="'.$DB_ROW['nombre'].'">';
        echo  '<td>'.$DB_ROW['decision_ordre'].'</td>';
        echo  '<td>'.html($DB_ROW['decision_synthese']).'</td>';
        echo  '<td>'.html($DB_ROW['decision_contenu']).'</td>';
        echo  '<td class="nu">';
        echo    '<q class="modifier"'.infobulle('Modifier cette décision.').'></q>';
        echo    '<q class="dupliquer"'.infobulle('Dupliquer cette décision.').'></q>';
        echo    '<q class="supprimer"'.infobulle('Supprimer cette décision.').'></q>';
        echo  '</td>';
        echo'</tr>'.NL;
      }
    }
    else
    {
      echo'<tr class="vide"><td class="nu" colspan="3">Cliquer sur l’icône ci-dessus (symbole "+" dans un rond vert) pour ajouter un contenu.</td><td class="nu"></td></tr>'.NL;
    }
    ?>
  </tbody>
</table>

<p>

<h2>Engagement dans l’établissement</h2>

<table id="table_engagement" class="form hsort">
  <thead>
    <tr>
      <th>Ordre</th>
      <th>Synthèse</th>
      <th>Contenu</th>
      <th class="nu"><q class="ajouter"<?php echo infobulle('Ajouter une décision.') ?>></q></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $DB_TAB = DB_STRUCTURE_OFFICIEL::DB_lister_officiel_decision( 'engagement' , TRUE /*avec_usage*/ );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        // Afficher une ligne du tableau
        echo'<tr id="id_'.$DB_ROW['decision_id'].'" data-used="'.$DB_ROW['nombre'].'">';
        echo  '<td>'.$DB_ROW['decision_ordre'].'</td>';
        echo  '<td>'.html($DB_ROW['decision_synthese']).'</td>';
        echo  '<td>'.html($DB_ROW['decision_contenu']).'</td>';
        echo  '<td class="nu">';
        echo    '<q class="modifier"'.infobulle('Modifier cette décision.').'></q>';
        echo    '<q class="dupliquer"'.infobulle('Dupliquer cette décision.').'></q>';
        echo    '<q class="supprimer"'.infobulle('Supprimer cette décision.').'></q>';
        echo  '</td>';
        echo'</tr>'.NL;
      }
    }
    else
    {
      echo'<tr class="vide"><td class="nu" colspan="3">Cliquer sur l’icône ci-dessus (symbole "+" dans un rond vert) pour ajouter un contenu.</td><td class="nu"></td></tr>'.NL;
    }
    ?>
  </tbody>
</table>

<p>

<h2>Décisions d’orientation</h2>

<table id="table_orientation" class="form hsort">
  <thead>
    <tr>
      <th>Ordre</th>
      <th>Synthèse</th>
      <th>Contenu</th>
      <th class="nu"><q class="ajouter"<?php echo infobulle('Ajouter une décision.') ?>></q></th>
    </tr>
  </thead>
  <tbody>
    <?php
    $DB_TAB = DB_STRUCTURE_OFFICIEL::DB_lister_officiel_decision( 'orientation' , TRUE /*avec_usage*/ );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        // Afficher une ligne du tableau
        echo'<tr id="id_'.$DB_ROW['decision_id'].'" data-used="'.$DB_ROW['nombre'].'">';
        echo  '<td>'.$DB_ROW['decision_ordre'].'</td>';
        echo  '<td>'.html($DB_ROW['decision_synthese']).'</td>';
        echo  '<td>'.html($DB_ROW['decision_contenu']).'</td>';
        echo  '<td class="nu">';
        echo    '<q class="modifier"'.infobulle('Modifier cette décision.').'></q>';
        echo    '<q class="dupliquer"'.infobulle('Dupliquer cette décision.').'></q>';
        echo    '<q class="supprimer"'.infobulle('Supprimer cette décision.').'></q>';
        echo  '</td>';
        echo'</tr>'.NL;
      }
    }
    else
    {
      echo'<tr class="vide"><td class="nu" colspan="3">Cliquer sur l’icône ci-dessus (symbole "+" dans un rond vert) pour ajouter un contenu.</td><td class="nu"></td></tr>'.NL;
    }
    ?>
  </tbody>
</table>

<p>

<form action="#" method="post" id="form_gestion" class="hide">
  <h2><span id="gestion_titre_action">Ajouter | Modifier | Dupliquer | Supprimer</span> un contenu</h2>
  <div id="gestion_edit">
    <p>
      <label class="tab" for="f_categorie_view">Catégorie :</label><input id="f_categorie_view" name="f_categorie_view" value="" type="text" size="12" readonly><br>
      <label class="tab" for="f_ordre">Ordre :</label><input id="f_ordre" name="f_ordre" value="" type="number" min="1" max="99"><br>
      <label class="tab" for="f_synthese">Synthèse :</label><input id="f_synthese" name="f_synthese" type="text" value="" size="20" maxlength="20"><br>
      <label class="tab" for="f_contenu">Contenu :</label><input id="f_contenu" name="f_contenu" type="text" value="" size="60" maxlength="80"><br>
    </p>
  </div>
  <div id="gestion_delete">
    <p>Confirmez-vous la suppression de cette décision ?</p>
  </div>
  <p id="alerte_used" class="fluo"><input id="f_usage" name="f_usage" type="hidden" value=""><span class="danger b">Cette décision est utilisée sur des bilans de cette année scolaire.<br>Un tel contenu déjà utilisé ne devrait pas être modifié, et encore moins supprimé.</span></p>
  <p>
    <span class="tab"></span><input id="f_action" name="f_action" type="hidden" value=""><input id="f_categorie" name="f_categorie" type="hidden" value=""><input id="f_id" name="f_id" type="hidden" value=""><button id="bouton_valider" type="button" class="valider">Valider.</button> <button id="bouton_annuler" type="button" class="annuler">Annuler.</button><label id="ajax_msg_gestion">&nbsp;</label>
  </p>
</form>
