/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var objet           = $('#f_objet option:selected').val();
    var prof_id         = 0;
    var matiere_id      = 0;
    var groupe_id       = 0;
    var groupe_type     = $('#f_groupe option:selected').parent().attr('label'); // Il faut indiquer une valeur initiale au moins pour le profil élève
    var eleves_ordre    = '';
    var action_matiere  = 'ajouter';
    var action_matieres = 'ajouter';
    var action_prof     = 'ajouter';

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enlever le message ajax et le résultat précédent au changement d’un élément de formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#form_select').on
    (
      'change',
      'select, input',
      function()
      {
        $('#ajax_msg').removeAttr('class').html('');
        $('#bilan').html('');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Afficher / masquer des éléments du formulaire
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_objet').change
    (
      function()
      {
        // on masque
        $('#choix_matiere , #choix_matieres , #choix_multimatiere , #choix_selection , #choix_professeur , #choix_evaluation , #choix_tableau_eval').hide();
        // on affiche
        objet = $('#f_objet option:selected').val();
        if(objet)
        {
          $('#choix_'+objet).show();
          if(objet=='tableau_eval')
          {
            $('#choix_evaluation').show();
          }
        }
        if( $('#f_groupe option:selected').val() )
        {
          $('#zone_periodes').removeAttr('class');
        }
        if( (objet=='multimatiere') || (objet=='matieres') || (objet=='tableau_eval') )
        {
          $('#div_not_multimatiere').hide();
          if(!$('#f_type_individuel').is(':checked'))
          {
            $('#f_type_individuel').click();
          }
          if(objet=='tableau_eval')
          {
            $('#div_not_tableau_eval , #span_retroactif').hide();
          }
          else
          {
            $('#div_not_tableau_eval , #span_retroactif').show();
          }
        }
        else
        {
          $('#div_not_multimatiere , #div_not_tableau_eval , #span_retroactif').show();
        }
        if( (objet=='evaluation') || (objet=='tableau_eval') )
        {
          $('#label_evaluation').show();
          charger_evaluations_prof();
        }
        else
        {
          $('#label_evaluation').hide();
        }
      }
    );

    function visibility_option_with_coef()
    {
      if( ($('#f_type_synthese').is(':checked')) || ($('#f_type_bulletin').is(':checked')) || ( ($('#f_type_individuel').is(':checked')) && ($('#f_moyenne_scores').is(':checked')) ) )
      {
        $('#option_with_coef').attr('class','show');
      }
      else
      {
        $('#option_with_coef').attr('class','hide');
      }
    }

    $('#f_type_individuel').click
    (
      function()
      {
        $('#options_individuel').toggle();
        $('#span_tri_individuel').toggle();
        visibility_option_with_coef();
      }
    );

    $('#f_type_synthese').click
    (
      function()
      {
        $('#options_synthese').toggle();
        visibility_option_with_coef();
      }
    );

    $('#f_type_bulletin').click
    (
      function()
      {
        visibility_option_with_coef();
        if($(this).is(':checked'))
        {
          $('#f_individuel_format option[value=eleve]').prop('selected',true);
        }
      }
    );

    $('#f_individuel_format').change
    (
      function()
      {
        if($(this).val()=='item')
        {
          $('#f_type_bulletin').prop('checked',false);
          $('#span_eleves_sans_note').show(0);
        }
        else
        {
          $('#span_eleves_sans_note').hide(0);
        }
      }
    );

    $('#f_etat_acquisition').click
    (
      function()
      {
        $('#span_etat_acquisition').toggle();
        $('#span_tri_acquisition').toggle();
      }
    );

    $('#f_moyenne_scores , #f_pourcentage_acquis').click
    (
      function()
      {
        $('label[for=f_conversion_sur_20]').hideshow( $('#f_moyenne_scores').is(':checked') || $('#f_pourcentage_acquis').is(':checked') );
        visibility_option_with_coef();
      }
    );

    var autoperiode = true; // Tant qu’on ne modifie pas manuellement le choix des périodes, modification automatique du formulaire

    function view_dates_perso()
    {
      var periode_val = $('#f_periode').val();
      if(periode_val!=0)
      {
        $('#dates_perso').attr('class','hide');
      }
      else
      {
        $('#dates_perso').attr('class','show');
      }
    }

    $('#f_periode').change
    (
      function()
      {
        view_dates_perso();
        autoperiode = false;
        if( (objet=='evaluation') || (objet=='tableau_eval') )
        {
          charger_evaluations_prof();
        }
      }
    );

    $('#f_date_debut, #f_date_fin').change
    (
      function()
      {
        if( (objet=='evaluation') || (objet=='tableau_eval') )
        {
          charger_evaluations_prof();
        }
      }
    );

    $('#f_cases_auto').click
    (
      function()
      {
        $('#span_cases_auto').toggle();
        $('#span_cases_manuel').toggle();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Intercepter la touche entrée
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_liste_items_nom').keydown
    (
      function(e)
      {
        if(e.which==13) // touche entrée
        {
          $('#f_enregistrer_items').click();
          return false;
        }
      }
    );

    $('#f_date_debut, #f_date_fin').keydown
    (
      function(e)
      {
        if(e.which==13) // touche entrée
        {
          if( (objet=='evaluation') || (objet=='tableau_eval') )
          {
            charger_evaluations_prof();
            return false;
          }
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger tous les profs d’une classe (approximativement) ou n’affiche que le prof connecté
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function afficher_prof_connecte(suffixe)
    {
      $('#f_prof'+suffixe).html('<option value="'+window.USER_ID+'">'+window.USER_TEXTE+'</option>');
      action_prof = 'ajouter';
      $('#retirer_prof'+suffixe).hide(0);
      $('#ajouter_prof'+suffixe).show(0);
    }

    function charger_profs_groupe(suffixe)
    {
      $('button').prop('disabled',true);
      prof_id     = $('#f_prof'+suffixe+' option:selected').val();
      groupe_id   = $('#f_groupe option:selected').val();
      groupe_type = $('#f_groupe option:selected').parent().attr('label');
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_profs_groupe',
          data : 'f_prof='+prof_id+'&f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type,
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('button').prop('disabled',false);
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            $('button').prop('disabled',false);
            if(responseJSON['statut']==true)
            {
              $('#f_prof'+suffixe).html(responseJSON['value']);
              action_prof = 'retirer';
              $('#ajouter_prof'+suffixe).hide(0);
              $('#retirer_prof'+suffixe).show(0);
            }
          }
        }
      );
    }

    $('#ajouter_prof_objet , #retirer_prof_objet').click
    (
      function()
      {
        if(action_prof=='retirer')
        {
          afficher_prof_connecte('_objet');
        }
        else if(action_prof=='ajouter')
        {
          charger_profs_groupe('_objet');
        }
      }
    );

    $('#ajouter_prof_only , #retirer_prof_only').click
    (
      function()
      {
        if(action_prof=='retirer')
        {
          afficher_prof_connecte('_only');
        }
        else if(action_prof=='ajouter')
        {
          charger_profs_groupe('_only');
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger toutes les évaluations d’un profs, sur un regroupement ou non
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function charger_evaluations_prof()
    {
      // Pour un professeur uniquement
      if(window.PROFIL_TYPE=='professeur')
      {
        $('#ajax_evaluations').html('');
        groupe_id = $('#f_groupe option:selected').val();
        groupe_type = $('#f_groupe option:selected').parent().attr('label');
        if(!groupe_id)
        {
          $('#zone_evals').addClass('hide');
        }
        else
        {
          $('#zone_evals').removeAttr('class');
          var periode    = $('#f_periode').val();
          var date_debut = $('#f_date_debut').val();
          var date_fin   = $('#f_date_fin').val();
          if( !periode && ( !test_dateITA(date_debut) || !test_dateITA(date_fin) ) )
          {
            $('#ajax_maj_evals').attr('class','erreur').html('Choisir une période ou renseigner des dates correctes.');
          }
          else
          {
            $('#ajax_maj_evals').attr('class','loader').html('En cours&hellip;');
            var diagnostic = $('#f_only_diagnostic').val();
            $.ajax
            (
              {
                type : 'POST',
                url : 'ajax.php?page=_maj_select_eval',
                data : 'f_objet=releve_items'+'&f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_diagnostic='+diagnostic+'&f_periode='+periode+'&f_date_debut='+date_debut+'&f_date_fin='+date_fin,
                dataType : 'json',
                error : function(jqXHR, textStatus, errorThrown)
                {
                  $('#ajax_maj_evals').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
                },
                success : function(responseJSON)
                {
                  initialiser_compteur();
                  if(responseJSON['statut']==true)
                  {
                    $('#ajax_maj_evals').removeAttr('class').html('');
                    $('#ajax_evaluations').html(responseJSON['value']);
                  }
                  else
                  {
                    $('#ajax_maj_evals').attr('class','alerte').html(responseJSON['value']);
                  }
                }
              }
            );
          }
        }
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Changement de groupe
    // -> desactiver les périodes prédéfinies en cas de groupe de besoin (prof uniquement)
    // -> choisir automatiquement la meilleure période si un changement manuel de période n’a jamais été effectué
    // -> afficher le formulaire de périodes s’il est masqué
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function selectionner_periode_adaptee()
    {
      var id_groupe = $('#f_groupe option:selected').val();
      if(typeof(window.tab_groupe_periode[id_groupe])!='undefined')
      {
        for(var id_periode in window.tab_groupe_periode[id_groupe]) // Parcourir un tableau associatif...
        {
          var tab_split = window.tab_groupe_periode[id_groupe][id_periode].split('_');
          if( (window.TODAY_SQL>=tab_split[0]) && (window.TODAY_SQL<=tab_split[1]) )
          {
            $('#f_periode option[value='+id_periode+']').prop('selected',true);
            view_dates_perso();
            break;
          }
        }
      }
    }

    $('#f_groupe').change
    (
      function()
      {
        groupe_type = $('#f_groupe option:selected').parent().attr('label');
        $('#f_periode option').each
        (
          function()
          {
            var periode_id = $(this).val();
            // La période personnalisée est tout le temps accessible
            if(periode_id!=0)
            {
              // classe ou groupe classique -> toutes périodes accessibles
              if(groupe_type!='Besoins')
              {
                $(this).prop('disabled',false);
              }
              // groupe de besoin -> desactiver les périodes prédéfinies
              else
              {
                $(this).prop('disabled',true);
              }
            }
          }
        );
        // Sélectionner si besoin la période personnalisée
        if(groupe_type=='Besoins')
        {
          $('#f_periode option[value=0]').prop('selected',true);
          $('#dates_perso').attr('class','show');
        }
        // Modification automatique du formulaire : périodes
        if(autoperiode)
        {
          if( (typeof(groupe_type)!='undefined') && (groupe_type!='Besoins') )
          {
            // Rechercher automatiquement la meilleure période
            selectionner_periode_adaptee();
          }
        }
        // Afficher la zone de choix des périodes, des enseignants, des évaluations
        if(typeof(groupe_type)!='undefined')
        {
          $('#info_profs').addClass('hide');
          $('#zone_periodes , #zone_profs').removeAttr('class');
          if( (objet=='evaluation') || (objet=='tableau_eval') )
          {
            charger_evaluations_prof();
          }
        }
        else
        {
          $('#zone_periodes , #zone_profs , #zone_evals').addClass('hide');
          $('#info_profs').removeAttr('class');
        }
        // Rechercher automatiquement la liste des profs
        if( (typeof(groupe_type)!='undefined') && (groupe_type!='Besoins') )
        {
          if( (window.USER_PROFIL_TYPE!='professeur') || (action_prof=='retirer') )
          {
            charger_profs_groupe('_objet');
            charger_profs_groupe('_only');
          }
        }
        else
        {
          afficher_prof_connecte('_objet');
          afficher_prof_connecte('_only');
        }
      }
    );

    // Rechercher automatiquement la meilleure période au chargement de la page (uniquement pour un élève, seul cas où la classe est préselectionnée)
    selectionner_periode_adaptee();

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger les selects f_eleve (pour le professeur et le directeur et les parents de plusieurs enfants) et f_matiere (pour le directeur) en ajax
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_matiere(groupe_id,matiere_id) // Uniquement pour un directeur
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_matieres',
          data : 'f_groupe='+groupe_id+'&f_matiere='+matiere_id+'&f_multiple=0',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#f_matiere').html(responseJSON['value']).show();
              maj_eleve(groupe_id,groupe_type,eleves_ordre);
            }
            else
            {
              $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    function maj_choix_tri_eleves(groupe_id,groupe_type)
    {
      if( (groupe_type=='Classes') && (window.PROFIL_TYPE!='professeur') )
      {
        $('#bloc_ordre').hide();
      }
      else
      {
        var nb_options_possibles = 0;
        $('#f_eleves_ordre option').each
        (
          function()
          {
            var value = $(this).val();
            if( (value=='nom') || (value=='prenom') )
            {
              $(this).prop('disabled',false);
              nb_options_possibles++;
            }
            else if(value=='classe')
            {
              if(groupe_type=='Classes')
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
              else
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
            }
            else
            {
              var plan_groupe = $(this).data('data');
              if(groupe_id==plan_groupe)
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
              else
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
            }
          }
        );
        $('#bloc_ordre').hideshow( nb_options_possibles > 1 );
      }
    }

    function maj_eleve(groupe_id,groupe_type,eleves_ordre)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eleves',
          data : 'f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_eleves_ordre='+eleves_ordre+'&f_statut=1'+'&f_multiple='+window.is_multiple+'&f_filter='+window.is_multiple+'&f_selection=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj').removeAttr('class').html('');
              $(window.champ_eleve).html(responseJSON['value']).parent().show();
              // Demande d’affichage du relevé pour un parent avec plusieurs enfants
              if(window.auto_voir_releve)
              {
                $('#f_eleve option[value='+window.auto_voir_releve+']').prop('selected',true);
                $('#bouton_valider').click();
                window.auto_voir_releve = false;
                window.auto_voir_releve = false;
              }
              else if( !window.is_multiple && ($('#f_eleve option').length==2) )
              {
                // Cas d’un seul élève retourné dans le regroupement (en particulier pour un parent de plusieurs enfants)
                $('#f_eleve option').eq(1).prop('selected',true);
              }
            }
            else
            {
              $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    $('#f_groupe').change
    (
      function()
      {
        // Pour un directeur, on met à jour f_matiere (on mémorise avant matiere_id) puis f_eleve
        // Pour un professeur ou un parent de plusieurs enfants, on met à jour f_eleve uniquement
        // Pour un élève ou un parent d’un seul enfant cette fonction n’est pas appelée puisque son groupe (masqué) ne peut être changé
        if(window.PROFIL_TYPE=='directeur')
        {
          matiere_id = $('#f_matiere').val();
          $('#f_matiere').html('<option value="">&nbsp;</option>').hide();
        }
        $(window.champ_eleve).html('').parent().hide();
        groupe_id = $('#f_groupe option:selected').val();
        if(groupe_id)
        {
          $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
          groupe_type  = $('#f_groupe option:selected').parent().attr('label');
          maj_choix_tri_eleves(groupe_id,groupe_type);
          eleves_ordre = $('#f_eleves_ordre option:selected').val();
          if(window.PROFIL_TYPE=='directeur')
          {
            maj_matiere(groupe_id,matiere_id);
          }
          else if( (window.PROFIL_TYPE=='professeur') || (window.PROFIL_TYPE=='parent') )
          {
            maj_eleve(groupe_id,groupe_type,eleves_ordre);
          }
        }
        else
        {
          $('#bloc_ordre').hide();
          $('#ajax_maj').removeAttr('class').html('');
        }
      }
    );

    $('#f_eleves_ordre').change
    (
      function()
      {
        groupe_id    = $('#f_groupe option:selected').val();
        groupe_type  = $('#f_groupe option:selected').parent().attr('label');
        eleves_ordre = $('#f_eleves_ordre option:selected').val();
        $(window.champ_eleve).html('').parent().hide();
        $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
        maj_eleve(groupe_id,groupe_type,eleves_ordre);
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger toutes les matières ou seulement les matières affectées (pour un prof)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#ajouter_matiere, #retirer_matiere').click
    (
      function()
      {
        $('button').prop('disabled',true);
        matiere_id = $('#f_matiere option:selected').val();
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=_maj_select_matieres_prof',
            data : 'f_matiere='+matiere_id+'&f_action='+action_matiere+'&f_multiple=0',
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('button').prop('disabled',false);
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                action_matiere = (action_matiere=='ajouter') ? 'retirer' : 'ajouter' ;
                $('#ajouter_matiere').hideshow( action_matiere == 'ajouter' );
                $('#retirer_matiere').hideshow( action_matiere == 'retirer' );
                $('#f_matiere').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    $('#ajouter_matieres, #retirer_matieres').click
    (
      function()
      {
        $('button').prop('disabled',true);
        var selection_id = []; $('#f_matieres input:checked').each(function(){selection_id.push($(this).val());});
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page=_maj_select_matieres_prof',
            data : 'f_selection='+selection_id+'&f_action='+action_matieres+'&f_multiple=1',
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('button').prop('disabled',false);
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                action_matieres = (action_matieres=='ajouter') ? 'retirer' : 'ajouter' ;
                $('#ajouter_matieres').hideshow( action_matieres == 'ajouter' );
                $('#retirer_matieres').hideshow( action_matieres == 'retirer' );
                $('#ajax_matieres').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Recharger la liste des évaluations, diagnostiques ou non
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_only_diagnostic').change
    (
      function()
      {
        if( (objet=='evaluation') || (objet=='tableau_eval') )
        {
          charger_evaluations_prof();
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Choisir les items : mise en place du formulaire
    // (pas d’utilisation de fancybox car il y a un temps de latence de 1 à 3 secondes avec toutes les matières)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var choisir_compet = function()
    {
      $('#f_selection_items option:first').prop('selected',true);
      $('#f_liste_items_nom').val('');
      $('#selection_id').val('');
      $('#action_modifier').prop('checked',false);
      $('#span_selection_modifier').hide(0);
      cocher_matieres_items( $('#f_compet_liste').val() );
      $('#form_select').hide(0);
      $('#zone_matieres_items').show(0);
    };

    $('q.choisir_compet').click( choisir_compet );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour fermer le cadre des items choisis (annuler / retour)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#annuler_compet').click
    (
      function()
      {
        $('#zone_matieres_items').hide(0);
        $('#form_select').show(0);
        return false;
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour valider le choix des items
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#valider_compet').click
    (
      function()
      {
        var liste = '';
        var nombre = 0;
        $('#zone_matieres_items input[type=checkbox]:checked').each
        (
          function()
          {
            liste += $(this).val()+'_';
            nombre++;
          }
        );
        var compet_liste  = liste.substring(0,liste.length-1);
        var compet_nombre = (nombre==0) ? 'aucun' : ( (nombre>1) ? nombre+' items' : nombre+' item' ) ;
        $('#f_compet_liste').val(compet_liste);
        $('#f_compet_nombre').val(compet_nombre);
        $('#annuler_compet').click();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Demande pour sélectionner d’une liste d’items mémorisés
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_selection_items').change
    (
      function()
      {
        cocher_matieres_items( $('#f_selection_items').val() );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Clic sur le bouton pour mémoriser un choix d’items
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#f_enregistrer_items').click
    (
      function()
      {
        memoriser_selection_matieres_items( $('#f_liste_items_nom').val() );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Soumettre le formulaire principal
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Le formulaire qui va être analysé et traité en AJAX
    var formulaire = $('#form_select');

    // Vérifier la validité du formulaire (avec jquery.validate.js)
    var validation = formulaire.validate
    (
      {
        rules :
        {
          f_objet              : { required:true },
          'f_matieres[]'       : { required:function(){return objet=='matieres';} , minlength:2 },
          f_matiere            : { required:function(){return objet=='matiere';} },
          f_compet_liste       : { required:function(){return objet=='selection';} },
          f_prof_objet         : { required:function(){return objet=='professeur';} },
          'f_type[]'           : { required:function(){return ( (objet!='multimatiere') && (objet!='tableau_eval') );} },
          'f_evaluation[]'     : { required:function(){return ( (objet=='evaluation') || (objet=='tableau_eval') );} },
          f_individuel_format  : { required:true },
          f_eleves_sans_note   : { required:false },
          f_etat_acquisition   : { required:false },
          f_moyenne_scores     : { required:false },
          f_pourcentage_acquis : { required:false },
          f_conversion_sur_20  : { required:false },
          f_synthese_format    : { required:true },
          f_tri_etat_mode      : { required:true },
          f_repeter_entete     : { required:false },
          f_with_coef          : { required:false },
          f_groupe             : { required:true },
          'f_eleve[]'          : { required:true },
          f_eleves_ordre       : { required:true },
          f_periode            : { required:true },
          f_date_debut         : { required:function(){return $('#f_periode').val()==0;} , dateITA:true },
          f_date_fin           : { required:function(){return $('#f_periode').val()==0;} , dateITA:true },
          f_retroactif         : { required:function(){return objet!='tableau_eval';} },
          f_only_etat          : { required:true },
          f_only_diagnostic    : { required:true },
          f_only_socle         : { required:false },
          f_only_valeur        : { required:false },
          f_only_prof          : { required:false },
          f_prof_only          : { required:function(){return $('#f_only_prof').is(':checked');} },
          f_reference          : { required:false },
          f_coef               : { required:false },
          f_socle              : { required:false },
          f_comm               : { required:false },
          f_lien               : { required:false },
          f_panier             : { required:false },
          f_domaine            : { required:false },
          f_theme              : { required:false },
          f_releve_tri         : { required:true },
          f_orientation        : { required:true },
          f_couleur            : { required:true },
          f_fond               : { required:true },
          f_legende            : { required:true },
          f_marge_min          : { required:true },
          f_pages_nb           : { required:true },
          f_cases_auto         : { required:false },
          f_cases_nb           : { required:true },
          f_cases_larg         : { required:true },
          f_highlight_id       : { required:false }
        },
        messages :
        {
          f_objet              : { required:'objet manquant' },
          'f_matieres[]'       : { required:'matière(s) manquante(s)' , minlength:'cocher plusieurs matières ou choisir l’objet dédié à une seule matière' },
          f_matiere            : { required:'matière manquante' },
          f_compet_liste       : { required:'item(s) manquant(s)' },
          f_prof_objet         : { required:'enseignant manquant' },
          'f_type[]'           : { required:'type(s) manquant(s)' },
          'f_evaluation[]'     : { required:'évaluation(s) manquante(s)' },
          f_individuel_format  : { required:'choix manquant' },
          f_eleves_sans_note   : { },
          f_etat_acquisition   : { },
          f_moyenne_scores     : { },
          f_pourcentage_acquis : { },
          f_conversion_sur_20  : { },
          f_synthese_format    : { required:'choix manquant' },
          f_tri_etat_mode      : { required:'choix manquant' },
          f_repeter_entete     : { },
          f_with_coef          : { },
          f_groupe             : { required:'groupe manquant' },
          'f_eleve[]'          : { required:'élève(s) manquant(s)' },
          f_eleves_ordre       : { required:'ordre manquant' },
          f_periode            : { required:'période manquante' },
          f_date_debut         : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_date_fin           : { required:'date manquante' , dateITA:'format JJ/MM/AAAA non respecté' },
          f_retroactif         : { required:'choix manquant' },
          f_only_etat          : { required:'choix manquant' },
          f_only_diagnostic    : { required:'choix manquant' },
          f_only_socle         : { },
          f_only_valeur        : { },
          f_only_prof          : { },
          f_prof_only          : { required:'enseignant manquant' },
          f_reference          : { },
          f_coef               : { },
          f_socle              : { },
          f_comm               : { },
          f_lien               : { },
          f_panier             : { },
          f_domaine            : { },
          f_theme              : { },
          f_releve_tri         : { required:'choix de tri manquant' },
          f_orientation        : { required:'orientation manquante' },
          f_couleur            : { required:'couleur manquante' },
          f_fond               : { required:'fond manquant' },
          f_legende            : { required:'légende manquante' },
          f_marge_min          : { required:'marge mini manquante' },
          f_pages_nb           : { required:'choix manquant' },
          f_cases_auto         : { },
          f_cases_nb           : { required:'nombre manquant' },
          f_cases_larg         : { required:'largeur manquante' },
          f_highlight_id       : { }
        },
        errorElement : 'label',
        errorClass : 'erreur',
        errorPlacement : function(error,element)
        {
          if(element.attr('id')=='f_matiere') { element.next().after(error); }
          else if(element.is('select')) {element.after(error);}
          else if(element.attr('type')=='text') {element.next().after(error);}
          else if(element.attr('type')=='radio') {element.parent().next().next().after(error);}
          else if(element.attr('type')=='checkbox') {
            if(element.parent().parent().hasClass('select_multiple')) {element.parent().parent().next().after(error);}
            else {element.parent().next().next().after(error);}
          }
        }
        // success: function(label) {label.text('ok').attr('class','valide');} Pas pour des champs soumis à vérification PHP
      }
    );

    // Options d’envoi du formulaire (avec jquery.form.js)
    var ajaxOptions =
    {
      url : 'ajax.php?page='+window.PAGE+'&csrf='+window.CSRF,
      type : 'POST',
      dataType : 'json',
      clearForm : false,
      resetForm : false,
      target : '#ajax_msg',
      filtering: filtrer_checkbox_multiple,
      beforeSerialize : action_form_avant_serialize,
      beforeSubmit : test_form_avant_envoi,
      error : retour_form_erreur,
      success : retour_form_valide
    };

    // Envoi du formulaire (avec jquery.form.js)
    formulaire.submit
    (
      function()
      {
        // récupération d’éléments
        var evaluation_nom = ( $('#f_evaluation input:checked:enabled').length == 1 ) ? $('#f_evaluation input:checked:enabled').parent().text() : '' ;
        $('#f_evaluation_nom'  ).val( evaluation_nom );
        $('#f_matiere_nom'     ).val( $('#f_matiere    option:selected').text() );
        $('#f_groupe_nom'      ).val( $('#f_groupe     option:selected').text() );
        $('#f_prof_texte_objet').val( $('#f_prof_objet option:selected').text() );
        $('#f_prof_texte_only' ).val( $('#f_prof_only  option:selected').text() );
        $('#f_groupe_type'     ).val( groupe_type );
        $(this).ajaxSubmit(ajaxOptions);
        return false;
      }
    );

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function filtrer_checkbox_multiple(el, index)
    {
      // Éviter la soumission des checkbox nombreux afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
      if( ( $(el).attr('type')!='checkbox' ) || ( ( $(el).attr('name')!='f_eleve[]' ) && ( $(el).attr('name')!='f_evaluation[]' ) ) )
      {
        return el;
      }
    }

    // Fonction précédent le traitement du formulaire (avec jquery.form.js)
    function action_form_avant_serialize(jqForm, options)
    {
      // Grouper les checkbox dans un champ unique afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
      var tab_eleve = [];
      $('#f_eleve input:checked:enabled').each
      (
        function()
        {
          tab_eleve.push($(this).val());
        }
      );
      $('#f_eleve_report').val(tab_eleve);
      var tab_evaluation = [];
      $('#f_evaluation input:checked:enabled').each
      (
        function()
        {
          tab_evaluation.push($(this).val());
        }
      );
      $('#f_evaluation_report').val(tab_evaluation);
    }

    // Fonction précédant l’envoi du formulaire (avec jquery.form.js)
    function test_form_avant_envoi(formData, jqForm, options)
    {
      $('#ajax_msg').removeAttr('class').html('');
      if( ($('#f_type_individuel').is(':checked')) && (!$('#f_etat_acquisition').is(':checked')) && ($('#f_cases_nb option:selected').val()==0) )
      {
        $('#ajax_msg').attr('class','erreur').html('Choisir au moins une indication à faire figurer sur le relevé individuel !');
        return false;
      }
      var readytogo = validation.form();
      if(readytogo)
      {
        // Il faut rajouter un test car jquery.validate.js considère corrects les input:checked:disabled
        // Test $(’#f_eleve input’).length car pour un élève ou un parent c’est un select classique.
        if( $('#f_eleve input').length && ( $('#f_eleve input:checked:enabled').length==0 ) )
        {
          $('#ajax_msg').attr('class','erreur').html('élève(s) filtré(s) manquant(s)');
          readytogo = false;
        }
        else if( ( (objet=='evaluation') || (objet=='tableau_eval') ) && ( $('#f_evaluation input:checked:enabled').length==0 ) )
        {
          $('#ajax_msg').attr('class','erreur').html('évaluation(s) filtrée(s) manquante(s)');
          readytogo = false;
        }
        else
        {
          $('#bouton_valider').prop('disabled',true);
          $('#ajax_msg').attr('class','loader').html('En cours&hellip;');
          $('#bilan').html('');
        }
      }
      return readytogo;
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_erreur(jqXHR, textStatus, errorThrown)
    {
      $('#bouton_valider').prop('disabled',false);
      var message = (jqXHR.status!=500) ? afficher_json_message_erreur(jqXHR,textStatus) : 'Erreur 500&hellip; Mémoire insuffisante ? Sélectionner moins d’élèves à la fois ou demander à votre hébergeur d’augmenter la valeur "memory_limit".' ;
      $('#ajax_msg').attr('class','alerte').html(message);
    }

    // Fonction suivant l’envoi du formulaire (avec jquery.form.js)
    function retour_form_valide(responseJSON)
    {
      initialiser_compteur();
      $('#bouton_valider').prop('disabled',false);
      if(responseJSON['statut']==false)
      {
        $('#ajax_msg').attr('class','alerte').html(responseJSON['value']);
      }
      else if(responseJSON['direct']==true)
      {
        $('#ajax_msg').attr('class','valide').html('Résultat ci-dessous.');
        $('#bilan').html(responseJSON['bilan']);
      }
      else if(responseJSON['direct']==false)
      {
        $('#ajax_msg').removeAttr('class').html('');
        // Mis dans le div bilan et pas balancé directement dans le fancybox sinon la mise en forme des liens nécessite un peu plus de largeur que le fancybox ne recalcule pas (et $.fancybox.update(); ne change rien).
        // Malgré tout, pour Chrome par exemple, la largeur est mal calculée et provoque des retours à la ligne, d’où le minWidth ajouté.
        $('#bilan').html('<p class="noprint">Afin de préserver l’environnement, n’imprimer que si nécessaire !</p>'+responseJSON['bilan']);
        $.fancybox( { href:'#bilan' , onClosed:function(){$('#bilan').html('');} , minHeight:400 , minWidth:600 } );
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Forcer le report de notes vers un bulletin SACoche
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bilan').on
    (
      'click',
      '#bouton_report',
      function()
      {
        $('#form_report_bulletin button, #form_report_bulletin select').prop('disabled',true);
        $('#ajax_msg_report').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=reporter_notes'+'&f_periode_eleves='+$('#f_periode_eleves').val()+'&f_eleves_moyennes='+$('#f_eleves_moyennes').val()+'&f_rubrique='+$('#f_rubrique').val(),
            // data : $('#form_report_bulletin').serialize(), le select f_rubrique n’est curieusement pas envoyé...
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#ajax_msg_report').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              $('#form_report_bulletin button, #form_report_bulletin select').prop('disabled',false);
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_report_bulletin button, #form_report_bulletin select').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $('#ajax_msg_report').attr('class','valide').html(responseJSON['value']);
              }
              else
              {
                $('#ajax_msg_report').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Afficher un relevé au chargement
    if(window.auto_voir_releve)
    {
      // Parent avec plusieurs enfants : d’abord charger la liste des enfants de la classe
      if(window.auto_voir_releve)
      {
        groupe_id = $('#f_groupe option:selected').val();
        groupe_type = 'Classes';
        maj_eleve(groupe_id,groupe_type,eleves_ordre);
      }
      // Parent avec un seul enfant ou élève : on peut lancer le formulaire de suite
      else
      {
        $('#bouton_valider').click();
        window.auto_voir_releve = false;
      }
    }
    else if(window.PROFIL_TYPE=='eleve')
    {
      $('#bouton_valider').click();
    }

    // Récupéré après le chargement de la page car potentiellement lourd pour les directeurs et les PP (bloque l’affichage plusieurs secondes)
    if( (window.PROFIL_TYPE=='professeur') || (window.PROFIL_TYPE=='directeur') )
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_load_arborescence',
          data : 'f_objet=referentiels'+'&f_item_comm=0'+'&f_all_if_pp=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#arborescence label').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
            return false;
          },
          success : function(responseJSON)
          {
            $('#arborescence').replaceWith(responseJSON['value']);
          }
        }
      );
    }

  }
);
