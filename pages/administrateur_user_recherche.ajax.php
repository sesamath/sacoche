<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

$action          = Clean::post('f_action'       , 'texte');
$champ_nom       = Clean::post('champ_nom'      , 'lettres');
$champ_val       = Clean::post('champ_val'      , 'texte');
$id              = Clean::post('f_id'           , 'entier');
$id_ent          = Clean::post('f_id_ent'       , 'id_ent');
$id_gepi         = Clean::post('f_id_gepi'      , 'id_ent');
$sconet_id       = Clean::post('f_sconet_id'    , 'entier');
$sconet_num      = Clean::post('f_sconet_num'   , 'entier');
$reference       = Clean::post('f_reference'    , 'ref');
$profil          = Clean::post('f_profil'       , 'lettres');
$genre           = Clean::post('f_genre'        , 'lettres');
$nom             = Clean::post('f_nom'          , 'nom');
$prenom          = Clean::post('f_prenom'       , 'prenom');
$login           = Clean::post('f_login'        , 'login');
$courriel        = Clean::post('f_courriel'     , 'courriel');
$sortie_date     = Clean::post('f_sortie_date'  , 'date_fr');
$box_sortie_date = Clean::post('box_sortie_date', 'bool');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Rechercher un utilisateur
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='rechercher') && in_array($champ_nom,array('id_ent','id_gepi','sconet_id','sconet_elenoet','reference','login','email','nom','prenom')) && $champ_val )
{
  $DB_TAB = DB_STRUCTURE_ADMINISTRATEUR::DB_rechercher_users( $champ_nom , $champ_val );
  $nb_reponses = count($DB_TAB) ;
  if($nb_reponses==0)
  {
    Json::end( FALSE , 'Aucun utilisateur trouvé selon ce critère.' );
  }
  else if($nb_reponses>100)
  {
    Json::end( FALSE , $nb_reponses.' réponses : restreignez votre recherche !' );
  }
  else
  {
    // Tableau avec noms des profils en session pour usage si modification de l’utilisateur
    foreach($DB_TAB as $DB_ROW)
    {
      $genre_key = ($DB_ROW['user_profil_sigle']=='ELV') ? 'enfant' : 'adulte' ;
      // Formater la date
      $date_sql    = $DB_ROW['user_sortie_date'];
      $date_sortie = ($date_sql!=SORTIE_DEFAUT_SQL) ? To::date_sql_to_french($date_sql) : '-' ;
      // Afficher une ligne du tableau
      Json::add_str('<tr id="id_'.$DB_ROW['user_id'].'">');
      Json::add_str(  '<td>'.html($DB_ROW['user_id_ent']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_id_gepi']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_sconet_id']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_sconet_elenoet']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_reference']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_profil_sigle']).' '.infobulle($DB_ROW['user_profil_nom_long_singulier'],TRUE).'</td>');
      Json::add_str(  '<td>'.Html::$tab_genre[$genre_key][$DB_ROW['user_genre']].'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_nom']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_prenom']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_login']).'</td>');
      Json::add_str(  '<td>'.html($DB_ROW['user_email']).'</td>');
      Json::add_str(  '<td>'.$date_sortie.'</td>');
      Json::add_str(  '<td class="nu">');
      Json::add_str(    '<q class="modifier"'.infobulle('Modifier cet utilisateur.').'></q>');
      Json::add_str(  '</td>');
      Json::add_str('</tr>'.NL);
    }
    Json::end( TRUE );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier un utilisateur existant
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier') && $id && $profil && isset(Html::$tab_genre['adulte'][$genre]) && $nom && $prenom && $login && ($box_sortie_date || $sortie_date) )
{
  $tab_donnees = array();
  // Vérifier le profil
  if(!isset($_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]))
  {
    Json::end( FALSE , 'Profil incorrect !' );
  }
  // Vérifier que l’identifiant ENT est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_ent)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_ent',$id_ent,$id) )
    {
      Json::end( FALSE , 'Identifiant ENT déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant GEPI est disponible (parmi tous les utilisateurs de l’établissement)
  if($id_gepi)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('id_gepi',$id_gepi,$id) )
    {
      Json::end( FALSE , 'Identifiant Gepi déjà utilisé !' );
    }
  }
  // Vérifier que l’identifiant sconet est disponible (parmi les utilisateurs de même type de profil)
  if($sconet_id)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('sconet_id',$sconet_id,$id,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Identifiant Sconet déjà utilisé !' );
    }
  }
  // Vérifier que le n° sconet est disponible (parmi les utilisateurs de même type de profil)
  if($sconet_num)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('sconet_elenoet',$sconet_num,$id,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Numéro Sconet déjà utilisé !' );
    }
  }
  // Vérifier que la référence est disponible (parmi les utilisateurs de même type de profil)
  if($reference)
  {
    if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('reference',$reference,$id,$_SESSION['TAB_PROFILS_ADMIN']['TYPE'][$profil]) )
    {
      Json::end( FALSE , 'Référence déjà utilisée !' );
    }
  }
  // Vérifier que le login transmis est disponible (parmi tous les utilisateurs de l’établissement)
  if( DB_STRUCTURE_ADMINISTRATEUR::DB_tester_utilisateur_identifiant('login',$login,$id) )
  {
    Json::end( FALSE , 'Login déjà utilisé !' );
  }
  // Vérifier le domaine du serveur mail seulement en mode multi-structures car ce peut être sinon une installation sur un serveur local non ouvert sur l’extérieur.
  if($courriel)
  {
    if(HEBERGEUR_INSTALLATION=='multi-structures')
    {
      list($mail_domaine,$is_domaine_valide) = Outil::tester_domaine_courriel_valide($courriel);
      if(!$is_domaine_valide)
      {
        Json::end( FALSE , 'Erreur avec le domaine "'.$mail_domaine.'" !' );
      }
    }
    $tab_donnees[':email_origine'] = 'admin';
  }
  else
  {
    $tab_donnees[':email_origine'] = '';
  }
  // Cas de la date de sortie
  if($box_sortie_date)
  {
    $sortie_date = '-' ;
    $sortie_date_sql = SORTIE_DEFAUT_SQL;
  }
  else
  {
    $sortie_date_sql = To::date_french_to_sql($sortie_date);
  }
  // Mettre à jour l’enregistrement
  $tab_donnees += array(
    ':sconet_id'   => $sconet_id,
    ':sconet_num'  => $sconet_num,
    ':reference'   => $reference,
    ':genre'       => $genre,
    ':nom'         => $nom,
    ':prenom'      => $prenom,
    ':courriel'    => $courriel,
    ':login'       => $login,
    ':id_ent'      => $id_ent,
    ':id_gepi'     => $id_gepi,
    ':sortie_date' => $sortie_date_sql,
  );
  DB_STRUCTURE_ADMINISTRATEUR::DB_modifier_user( $id , $tab_donnees );
  // En cas de sortie d’un élève, retirer les notes AB etc ultérieures à cette date de sortie, afin d’éviter des bulletins totalement vides
  if( ($profil=='ELV') && !$box_sortie_date )
  {
    DB_STRUCTURE_ADMINISTRATEUR::DB_supprimer_user_saisies_absences_apres_sortie( $id , $sortie_date_sql );
  }
  // Mettre à jour aussi éventuellement la session
  if($id==$_SESSION['USER_ID'])
  {
    $email_origine = isset($tab_donnees[':email_origine']) ? $tab_donnees[':email_origine'] : $_SESSION['USER_EMAIL_ORIGINE'] ; // si le mail n’a pas été changé alors il ne faut pas non plus modifier cette valeur
    Session::_set('USER_GENRE'         , $genre );
    Session::_set('USER_NOM'           , $nom );
    Session::_set('USER_PRENOM'        , $prenom );
    Session::_set('USER_EMAIL'         , $courriel );
    Session::_set('USER_EMAIL_ORIGINE' , $email_origine);
    Session::_set('USER_LOGIN'         , $login );
    Session::_set('USER_ID_ENT'        , $id_ent );
    Session::_set('USER_ID_GEPI'       , $id_gepi );
  }
  // Afficher le retour
  $genre_key = ($profil=='ELV') ? 'enfant' : 'adulte' ;
  Json::add_str('<td>'.html($id_ent).'</td>');
  Json::add_str('<td>'.html($id_gepi).'</td>');
  Json::add_str('<td>'.html($sconet_id).'</td>');
  Json::add_str('<td>'.html($sconet_num).'</td>');
  Json::add_str('<td>'.html($reference).'</td>');
  Json::add_str('<td>'.html($profil).' '.infobulle('{{PROFIL}}',TRUE).'</td>');
  Json::add_str('<td>'.Html::$tab_genre[$genre_key][$genre].'</td>');
  Json::add_str('<td>'.html($nom).'</td>');
  Json::add_str('<td>'.html($prenom).'</td>');
  Json::add_str('<td>'.html($login).'</td>');
  Json::add_str('<td>'.html($courriel).'</td>');
  Json::add_str('<td>'.$sortie_date.'</td>');
  Json::add_str('<td class="nu">');
  Json::add_str(  '<q class="modifier"'.infobulle('Modifier cet utilisateur.').'></q>');
  Json::add_str('</td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
