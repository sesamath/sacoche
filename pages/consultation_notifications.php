<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Notifications reçues'));

// Ajout exceptionnel de css
Layout::add( 'css_inline' , 'tr.new td.notif {white-space:pre-wrap}' );
Layout::add( 'css_inline' , 'tr.vue td.notif {font-size:0;opacity:0}' );

$menu = ($_SESSION['USER_PROFIL_TYPE']!='administrateur') ? '['.html(Lang::_('Paramétrages')).']' : '['.html(Lang::_('Paramétrages personnels')).']' ;
?>

<p><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=environnement_generalites__email_notifications">DOC : Adresse e-mail / Abonnements / Notifications</a></span></p>

<p>
  <span class="astuce">Pour gérer ses abonnements aux notifications, utiliser le menu <a href="./index.php?page=compte_email"><?php echo $menu ?> [<?php echo html(Lang::_('Adresse e-mail & Abonnements')) ?>]</a>.</span><br>
  <span class="astuce"><?php echo html(Lang::_('Les notifications consultées sont automatiquement retirées passé un délai de 2 mois.')) ?></span>
</p>

<hr>

<?php
// Lister les notifications qu’un utilisateur peut consulter
$DB_TAB = DB_STRUCTURE_NOTIFICATION::DB_lister_notifications_consultables_for_user( $_SESSION['USER_ID'] );

if(empty($DB_TAB))
{
  echo'<p class="b astuce">Aucune notification actuellement enregistrée.</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
?>

<p class="b">Double-cliquer sur une ligne du tableau pour marquer une notification comme "consultée" (ou le contraire).</p>

<table id="table_notifications" class="form hsort">
  <thead>
    <tr>
      <th>Date</th>
      <th>Objet</th>
      <th>Statut</th>
      <th>Contenu</th>
    </tr>
  </thead>
  <tbody id="table_body">
    <?php
    function conservation_balises_finediff($notification)
    {
      $tab_bad = array( '&lt;del&gt;' , '&lt;/del&gt;' , '&lt;ins&gt;' , '&lt;/ins&gt;' );
      $tab_bon = array(    '<del>'    ,    '</del>'    ,    '<ins>'    ,    '</ins>'    );
      return str_replace( $tab_bad , $tab_bon , $notification );
    }
    foreach($DB_TAB as $DB_ROW)
    {
      $class = ($DB_ROW['notification_statut']=='consultable') ? ' class="new"'  : ' class="vue"' ;
      $datetime_affich = To::datetime_sql_to_french( $DB_ROW['notification_date'] , TRUE /*return_time*/ );
      // Afficher une ligne du tableau
      echo'<tr id="id_'.$DB_ROW['notification_id'].'"'.$class.'>';
      echo  '<td>'.$datetime_affich.'</td>';
      echo  '<td>'.$DB_ROW['abonnement_objet'].'</td>';
      echo  '<td>'.$DB_ROW['notification_statut'].'</td>';
      echo  '<td class="notif">'.conservation_balises_finediff(html($DB_ROW['notification_contenu'])).'</td>';
      echo'</tr>'.NL;
    }
    ?>
  </tbody>
</table>
<p>&nbsp;</p>
