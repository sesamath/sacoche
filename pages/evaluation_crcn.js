/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <http://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <http://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Activer le mode strict afin de tendre vers un code rigoureux
'use strict';

// jQuery !
$(document).ready
(
  function()
  {

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Élement saisissable / déplaçable
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $( '#cadre_tactile' ).draggable({cursor:'move'});

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Initialisation
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var memo_pilotage  = 'clavier';
    var memo_direction = 'right';
    var memo_colonne   = 1;
    var memo_ligne     = 1;
    var memo_input_id  = false;
    var nb_colonnes    = 0;
    var nb_lignes      = 0;

    if( $('#table_crcn').length )
    {
      nb_colonnes = window.nb_eleves;
      nb_lignes   = window.nb_competences;
      $('#radio_'+memo_pilotage).click();
      $('#L'+memo_ligne+'C'+memo_colonne).click();
      $('#arrow_continue_'+memo_direction).click();
      if(memo_pilotage=='clavier')
      {
        $('#L'+memo_ligne+'C'+memo_colonne).focus();
        if(IsTouch)
        {
          $('#cadre_tactile').show();
        }
      }
      else
      {
        $('#arrow_continue').hide();
      }
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Traitement du premier formulaire pour afficher la liste des élèves d’un regroupement
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    var recharger_page = function()
    {
      var groupe_id = $('#f_groupe_id').val();
      if(groupe_id)
      {
        var groupe_type = $('#f_groupe_id option:selected').parent().attr('label');
        $('#f_groupe_type').val(groupe_type);
      }
      $('#form_prechoix').attr('action','./index.php?page=evaluation&section=crcn');
      $('#form_prechoix').submit();
    }

    $('#bouton_annuler').click
    (
      function()
      {
        $(window).off('beforeunload', confirmOnLeave );
        $('#f_groupe_id').prop('disabled',false);
        recharger_page();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Actualiser le select f_eleves_ordre (au chargement)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_choix_tri_eleves()
    {
      var groupe_id = $('#f_groupe_id').val();
      if(groupe_id)
      {
        var groupe_type = $('#f_groupe_id option:selected').parent().attr('label');
        var nb_options_possibles = 0;
        $('#f_eleves_ordre option').each
        (
          function()
          {
            var value = $(this).val();
            if( (value=='nom') || (value=='prenom') )
            {
              $(this).prop('disabled',false);
              nb_options_possibles++;
            }
            else if(value=='classe')
            {
              if(groupe_type=='Classes')
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
              else
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
            }
            else
            {
              var plan_groupe = $(this).data('data');
              if(groupe_id==plan_groupe)
              {
                $(this).prop('disabled',false);
                nb_options_possibles++;
              }
              else
              {
                $(this).prop('disabled',true).prop('selected',false);
              }
            }
          }
        );
        $('#bloc_ordre').hideshow( nb_options_possibles > 1 );
      }
    }

    if( window.PROFIL_TYPE == 'professeur' )
    {
      maj_choix_tri_eleves();
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Charger le select f_eleve_id en ajax (pour un parent de plusieurs enfants)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function maj_eleve(groupe_id,groupe_type)
    {
      $.ajax
      (
        {
          type : 'POST',
          url : 'ajax.php?page=_maj_select_eleves',
          data : 'f_groupe_id='+groupe_id+'&f_groupe_type='+groupe_type+'&f_eleves_ordre=nom'+'&f_statut=1',
          dataType : 'json',
          error : function(jqXHR, textStatus, errorThrown)
          {
            $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
          },
          success : function(responseJSON)
          {
            initialiser_compteur();
            if(responseJSON['statut']==true)
            {
              $('#ajax_maj').removeAttr('class').html('');
              $('#f_eleve_id').html(responseJSON['value']).parent().show();
              if($('#f_eleve_id option').length==2)
              {
                // Cas d’un seul élève retourné dans le regroupement (en particulier pour un parent de plusieurs enfants)
                $('#f_eleve_id option').eq(1).prop('selected',true);
                recharger_page();
              }
            }
            else
            {
              $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
            }
          }
        }
      );
    }

    $('#f_groupe_id').change
    (
      function()
      {
        if(window.USER_PROFIL_TYPE=='parent')
        {
          var groupe_type = $('#f_groupe_id option:selected').parent().attr('label');
          // Pour un parent de plusieurs enfants, on met à jour f_eleve_id
          $('#f_eleve_id').html('<option value="">&nbsp;</option>').parent().hide();
          $('#ajax_maj').removeAttr('class').html('');
          var groupe_id = $('#f_groupe_id option:selected').val();
          if(groupe_id)
          {
            $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
            maj_eleve(groupe_id,groupe_type);
          }
          else
          {
            $('#label_crcn , #table_crcn').remove();
            $('#ajax_maj').removeAttr('class').html('');
          }
        }
        else
        {
          recharger_page();
        }
      }
    );

    $('#f_eleve_id').change
    (
      function()
      {
        if(window.USER_PROFIL_TYPE=='parent')
        {
          var eleve_id = $('#f_eleve_id option:selected').val();
          if(eleve_id)
          {
            recharger_page();
          }
          else
          {
            $('#label_crcn , #table_crcn').remove();
            $('#ajax_maj').removeAttr('class').html('');
          }
        }
      }
    );

    $('#f_eleves_ordre').change
    (
      function()
      {
        recharger_page();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Choix du mode de pilotage pour la saisie des résultats
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_crcn').on
    (
      'click',
      'input[name=mode_saisie]',
      function()
      {
        memo_pilotage = $(this).val();
        if(memo_pilotage=='clavier')
        {
          $('#arrow_continue').show(0);
          $('#L'+memo_ligne+'C'+memo_colonne).focus();
          if(IsTouch)
          {
            $('#cadre_tactile').show();
          }
        }
        else
        {
          $('#arrow_continue').hide(0);
          if(IsTouch)
          {
            $('#cadre_tactile').hide();
          }
        }
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Choix du sens de parcours pour la saisie des résultats (si pilotage au clavier)
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#table_crcn').on
    (
      'click',
      'input[name=arrow_continue]',
      function()
      {
        memo_direction = $(this).val();
        $('#L'+memo_ligne+'C'+memo_colonne).focus();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gérer la mise en valeur des entêtes de ligne et de colonne ou de la cellule en cours, que ce soit au clavier ou à la souris
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function valoriser_th( objet_input )
    {
      var numero_ligne   = objet_input.data('ligne');
      var numero_colonne = objet_input.data('colonne');
      $('#table_crcn').find('th').removeClass('highlight');
      $('#L'+numero_ligne).addClass('highlight');
      $('#C'+numero_colonne).addClass('highlight');
    }

    $('#table_crcn').on
    (
      'mouseover',
      'tbody td',
      function()
      {
        valoriser_th( $(this).children('input') );
      }
    );

    $('#table_crcn').on
    (
      'mouseout',
      'tbody',
      function()
      {
        $('#table_crcn').find('th').removeClass('highlight');
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Alerter si modification
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function alerter_modification()
    {
      $('#kbd_27').attr('class','img annuler');
      $('#f_groupe_id , #f_eleves_ordre').prop('disabled',true);
      $('#ajax_maj').attr('class','alerte').html('Confirmez ou annulez vos modifications avant de changer de classe / groupe.');
      $(window).on('beforeunload', confirmOnLeave );
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gérer la saisie des résultats au clavier ou avec un dispositif tactile
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    function focus_cellule_suivante_en_evitant_sortie_tableau()
    {
      if(memo_colonne==0)
      {
        memo_colonne = nb_colonnes;
        memo_ligne = (memo_ligne!=1) ? memo_ligne-1 : nb_lignes ;
      }
      else if(memo_colonne>nb_colonnes)
      {
        memo_colonne = 1;
        memo_ligne = (memo_ligne!=nb_lignes) ? memo_ligne+1 : 1 ;
      }
      else if(memo_ligne==0)
      {
        memo_ligne = nb_lignes;
        memo_colonne = (memo_colonne!=1) ? memo_colonne-1 : nb_colonnes ;
      }
      else if(memo_ligne>nb_lignes)
      {
        memo_ligne = 1;
        memo_colonne = (memo_colonne!=nb_colonnes) ? memo_colonne+1 : 1 ;
      }
      var objet_input = $('#'+'L'+memo_ligne+'C'+memo_colonne);
      objet_input.focus();
      valoriser_th( objet_input );
    }

    $('#cadre_tactile').on
    (
      'click',
      'span',
      function()
      {
        var code = entier( $(this).attr('id').substring(4) ); // "kbd_" + ref
        navigation_clavier(code);
      }
    );

    $('#table_crcn').on
    (
      'click',
      'tbody td input',
      function()
      {
        memo_ligne   = $(this).data('ligne');
        memo_colonne = $(this).data('colonne');
      }
    );

    $('#table_crcn').on
    (
      'keydown',  // keydown au lieu de keyup permet de laisser appuyer sur la touche pour répéter une action
      'input[data-cible]',
      function(e)
      {
        if(memo_pilotage=='clavier')
        {
          memo_ligne   = $(this).data('ligne');
          memo_colonne = $(this).data('colonne');
          navigation_clavier(e.which);
        }
      }
    );

    function navigation_clavier(touche_code)
    {
      var findme = '.'+touche_code+'.';
      if( '.8.46.48.49.50.51.52.53.96.97.98.99.100.101.'.indexOf(findme) != -1 )
      {
        // Une touche de positionnement a été pressée
        switch (touche_code)
        {
          case   8: var niveau =  ''; break; // backspace
          case  46: var niveau =  ''; break; // suppr
          case  48: var niveau =  ''; break; // 0
          case  49: var niveau = '1'; break; // 1
          case  50: var niveau = '2'; break; // 2
          case  51: var niveau = '3'; break; // 3
          case  52: var niveau = '4'; break; // 4
          case  53: var niveau = '5'; break; // 5
          case  96: var niveau =  ''; break; // 0
          case  97: var niveau = '1'; break; // 1
          case  98: var niveau = '2'; break; // 2
          case  99: var niveau = '3'; break; // 3
          case 100: var niveau = '4'; break; // 4
          case 101: var niveau = '5'; break; // 5
        }
        var case_ref = 'L'+memo_ligne+'C'+memo_colonne;
        $('#'+case_ref).val(niveau);
        if(memo_direction=='down')
        {
          memo_ligne++;
        }
        else
        {
          memo_colonne++;
        }
        alerter_modification();
        focus_cellule_suivante_en_evitant_sortie_tableau();
      }
      else if('.37.38.39.40.'.indexOf(findme)!=-1)
      {
        // Une flèche a été pressée
        switch (touche_code)
        {
          case 37: memo_colonne--; break; // flèche gauche
          case 38: memo_ligne--;   break; // flèche haut
          case 39: memo_colonne++; break; // flèche droit
          case 40: memo_ligne++;   break; // flèche bas
        }
        focus_cellule_suivante_en_evitant_sortie_tableau();
      }
      else if(touche_code==13)  // touche entrée
      {
        // La touche entrée a été pressée
        $('#bouton_valider').click();
      }
      else if(touche_code==27)
      {
        // La touche escape a été pressée
        $('#bouton_annuler').click();
      }
      return false; // Evite notamment qu’IE fasse "page précédente" si on appuie sur backspace.
    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Gérer la saisie des résultats à la souris
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    // Remplacer la cellule par les choix possibles
    $('#table_crcn').on
    (
      'mouseover',
      'tbody td.td_clavier',
      function()
      {
        if(memo_pilotage=='souris')
        {
          // Test si un précédent td n’a pas été remis en place (js a du mal à suivre le mouseleave sinon)
          if(memo_input_id)
          {
            $('#'+memo_input_id).parent().attr('class','td_clavier').children('div').remove();
            $('#'+memo_input_id).show();
            memo_input_id = false;
          }
          else
          {
            var objet_td    = $(this);
            var objet_input = objet_td.children('input');
            // Récupérer les infos associées
            memo_input_id = objet_input.attr('id');
            memo_ligne   = $(this).data('ligne');
            memo_colonne = $(this).data('colonne');
            var valeur = objet_input.val();
            valeur = (valeur) ? valeur : 0 ;
            objet_input.hide();
            objet_td.attr('class','td_souris').append( $('#td_souris_container').html() ).find('span').eq(valeur).addClass('on');
          }
        }
      }
    );

    // Revenir à la cellule initiale ; mouseout ne fonctionne pas à cause des éléments contenus dans le div ; mouseleave est mieux, mais pb qd même avec les select du calendrier
    $('#table_crcn').on
    (
      'mouseleave',
      'tbody td',
      function()
      {
        if(memo_pilotage=='souris')
        {
          if(memo_input_id)
          {
            $('#'+memo_input_id).parent().attr('class','td_clavier').children('div').remove();
            $('#'+memo_input_id).show();
            memo_input_id = false;
          }
        }
      }
    );

    // Renvoyer l’information dans la cellule
    $('#table_crcn').on
    (
      'click',
      'div.td_souris span',
      function()
      {
        var note = $(this).data('val');
        $('#'+memo_input_id).val(note);
        $(this).parent().children('span').removeAttr('class');
        $(this).addClass('on');
        alerter_modification();
      }
    );

    // ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Enregistrer les changements
    // ////////////////////////////////////////////////////////////////////////////////////////////////////

    $('#bouton_valider').click
    (
      function()
      {
        // Grouper les valeurs dans un champ unique afin d’éviter tout problème avec une limitation du module "suhosin" (voir par exemple http://xuxu.fr/2008/12/04/nombre-de-variables-post-limite-ou-tronque) ou "max input vars" généralement fixé à 1000.
        var tab_eleve      = [];
        var tab_competence = [];
        var tab_niveau     = [];
        $('#table_crcn').children('tbody').children('tr').children('td').children('input').each
        (
          function()
          {
            var objet_input = $(this);
            var eleve      = objet_input.data('eleve');
            var competence = objet_input.data('competence');
            var niveau     = objet_input.val();
            niveau = (isNaN(entier(niveau))) ? 0 : niveau ;
            tab_eleve.push(eleve);
            tab_competence.push(competence);
            tab_niveau.push(niveau);
          }
        );
        $('#form_prechoix button').prop('disabled',true);
        $('#ajax_maj').attr('class','loader').html('En cours&hellip;');
        $.ajax
        (
          {
            type : 'POST',
            url : 'ajax.php?page='+window.PAGE,
            data : 'csrf='+window.CSRF+'&f_action=enregistrer'+'&f_eleve_liste='+tab_eleve+'&f_competence_liste='+tab_competence+'&f_niveau_liste='+tab_niveau,
            dataType : 'json',
            error : function(jqXHR, textStatus, errorThrown)
            {
              $('#form_prechoix button').prop('disabled',false);
              $('#ajax_maj').attr('class','alerte').html(afficher_json_message_erreur(jqXHR,textStatus));
              return false;
            },
            success : function(responseJSON)
            {
              initialiser_compteur();
              $('#form_prechoix button').prop('disabled',false);
              if(responseJSON['statut']==true)
              {
                $(window).off('beforeunload', confirmOnLeave );
                $('#ajax_maj').attr('class','valide').html('Demande réalisée !');
                $('#f_groupe_id , #f_eleves_ordre').prop('disabled',false);
                $('#kbd_27').attr('class','img retourner');
             }
              else
              {
                $('#ajax_maj').attr('class','alerte').html(responseJSON['value']);
              }
            }
          }
        );
      }
    );

  }
);
