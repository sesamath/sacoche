<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Professeur remplaçant'));

// Fabrication des éléments select du formulaire
$DB_TAB_profs = DB_STRUCTURE_COMMUN::DB_OPT_professeurs_etabl('config'); // du coup ne concerne pas les CPE ou profs documentalistes
$select_absent     = HtmlForm::afficher_select( $DB_TAB_profs , 'f_absent_id'     /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ , '' /*optgroup*/ , FALSE /*multiple*/ );
$select_remplacant = HtmlForm::afficher_select( $DB_TAB_profs , 'f_remplacant_id' /*select_nom*/ , '' /*option_first*/ , FALSE /*selection*/ , '' /*optgroup*/ , FALSE /*multiple*/ );
?>

<p><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=support_administrateur__gestion_professeurs#toggle_professeur_remplacant">DOC : Gestion des professeurs et personnels</a></span></p>

<hr>

<table id="table_action" class="form hsort">
  <thead>
    <tr>
      <th>Enseignant absent</th>
      <th>Remplaçant</th>
      <th class="nu"><q class="ajouter"<?php echo infobulle('Ajouter un remplacement.') ?>></q></th>
    </tr>
  </thead>
  <tbody>
    <?php
    // Lister les personnels (professeurs, directeurs, etc.)
    $DB_TAB = DB_STRUCTURE_ADMINISTRATEUR::DB_lister_profs_remplacants();
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        echo'<tr>';
        echo  '<td data-id="'.$DB_ROW['prof_absent_id'].'">'.html($DB_ROW['absent_nom'].' '.$DB_ROW['absent_prenom']).'</td>';
        echo  '<td data-id="'.$DB_ROW['prof_remplacant_id'].'">'.html($DB_ROW['remplacant_nom'].' '.$DB_ROW['remplacant_prenom']).'</td>';
        echo  '<td class="nu">';
        echo    '<q class="supprimer"'.infobulle('Supprimer ce remplacement (aucune confirmation ne sera demandée).').'></q>';
        echo  '</td>';
        echo'</tr>'.NL;
      }
    }
    else
    {
      echo'<tr class="vide"><td class="nu" colspan="2"></td><td class="nu"></td></tr>'.NL;
    }
    ?>
  </tbody>
</table>

<form action="#" method="post" id="form_gestion" class="hide">
  <h2>Ajouter un remplacement</h2>
  <p>
    <label class="tab" for="f_absent_id">Prof absent :</label><?php echo $select_absent ?><br>
    <label class="tab" for="f_remplacant_id">Remplaçant :</label><?php echo $select_remplacant ?>
  </p>
  <p>
    <span class="tab"></span><button id="bouton_valider" type="button" class="valider">Valider.</button> <button id="bouton_annuler" type="button" class="annuler">Annuler.</button><label id="ajax_msg_gestion">&nbsp;</label>
  </p>
</form>
