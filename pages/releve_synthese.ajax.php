<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Générer une synthèse d’items
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$synthese_modele     = Clean::post('f_objet'              , 'texte');
$matiere_id          = Clean::post('f_matiere'            , 'entier');
$matiere_nom         = Clean::post('f_matiere_nom'        , 'texte');
$groupe_id           = Clean::post('f_groupe'             , 'entier');
$groupe_nom          = Clean::post('f_groupe_nom'         , 'texte');
$groupe_type         = Clean::post('f_groupe_type'        , 'lettres');
$periode_id          = Clean::post('f_periode'            , 'entier');
$date_debut          = Clean::post('f_date_debut'         , 'date_fr');
$date_fin            = Clean::post('f_date_fin'           , 'date_fr');
$retroactif          = Clean::post('f_retroactif'         , 'calcul_retroactif');
$niveau_id           = Clean::post('f_niveau'             , 'entier'); // Niveau transmis uniquement si on restreint sur un niveau
$fusion_niveaux      = Clean::post('f_fusion_niveaux'     , 'bool');
$aff_coef            = Clean::post('f_coef'               , 'bool');
$aff_socle           = Clean::post('f_socle'              , 'bool');
$aff_lien            = Clean::post('f_lien'               , 'bool');
$aff_panier          = Clean::post('f_panier'             , 'bool');
$aff_start           = Clean::post('f_start'              , 'bool');
$aff_prop_sans_score = Clean::post('f_prop_sans_score'    , 'bool');
$only_socle          = Clean::post('f_restriction_socle'  , 'bool');
$only_diagnostic     = Clean::post('f_only_diagnostic'    , 'texte');
$only_prof           = Clean::post('f_only_prof'          , 'bool');
$only_niveau         = !empty($_POST['f_restriction_niveau']) ? $niveau_id : 0;
$synthese_indicateur = Clean::post('f_synthese_indicateur', 'texte');
$with_coef           = Clean::post('f_with_coef'          , 'bool');
$mode_synthese       = Clean::post('f_mode_synthese'      , 'texte');
$couleur             = Clean::post('f_couleur'            , 'texte');
$fond                = Clean::post('f_fond'               , 'texte');
$legende             = Clean::post('f_legende'            , 'texte');
$marge_min           = Clean::post('f_marge_min'          , 'entier');
$eleves_ordre        = Clean::post('f_eleves_ordre'       , 'eleves_ordre', 'nom'); // Non transmis par Safari si dans le <span> avec la classe "hide".
$prof_id             = Clean::post('f_prof'               , 'entier');
$prof_texte          = Clean::post('f_prof_texte'         , 'texte');

// tableaux
$tab_matiere = Clean::post('f_matieres', array('array',','));
$tab_type    = Clean::post('f_type'    , array('array',','));
$tab_eleve   = Clean::post('f_eleve'   , array('array',','));
$tab_matiere = array_filter( Clean::map('entier',$tab_matiere) , 'positif' );
$tab_eleve   = array_filter( Clean::map('entier',$tab_eleve)   , 'positif' );
$tab_type    = Clean::map('texte',$tab_type);

$liste_matiere_id = implode(',',$tab_matiere);

// En cas de manipulation du formulaire (avec les outils de développements intégrés au navigateur ou un module complémentaire)...

if(in_array($_SESSION['USER_PROFIL_TYPE'],array('parent','eleve')))
{
  $tab_type   = array('individuel');
  $aff_panier = 1;
}

// Pour un élève on surcharge avec les données de session
if($_SESSION['USER_PROFIL_TYPE']=='eleve')
{
  $tab_eleve  = array($_SESSION['USER_ID']);
  $groupe_id  = $_SESSION['ELEVE_CLASSE_ID'];
  $groupe_nom = $_SESSION['ELEVE_CLASSE_NOM'];
}

// Pour un parent on vérifie que c’est bien un de ses enfants
if( isset($tab_eleve[0]) && ($_SESSION['USER_PROFIL_TYPE']=='parent') )
{
  Outil::verif_enfant_parent( $tab_eleve[0] );
}

// Pour un professeur on vérifie que ce sont bien ses élèves
if( ($_SESSION['USER_PROFIL_TYPE']=='professeur') && ($_SESSION['USER_JOIN_GROUPES']=='config') )
{
  Outil::verif_eleves_prof( $tab_eleve );
}

$type_individuel = (in_array('individuel',$tab_type)) ? 1 : 0 ;
$type_synthese   = (in_array('synthese',$tab_type))   ? 1 : 0 ;

$liste_eleve = implode(',',$tab_eleve);

$tab_modele = array(
  'matiere'      => TRUE,
  'matieres'     => TRUE,
  'multimatiere' => TRUE,
);

if(
    !isset($tab_modele[$synthese_modele]) ||
    ( ($synthese_modele=='matiere') && ( !$matiere_id || !$matiere_nom || !$mode_synthese ) ) ||
    ( ($synthese_modele=='matieres') && count($tab_matiere)<2 ) ||
    !$groupe_id || !$groupe_nom || !$groupe_type || !count($tab_eleve) || !count($tab_type) ||
    ( !$periode_id && (!$date_debut || !$date_fin) ) || !$retroactif || !$only_diagnostic || ( $only_prof && !$prof_id )
    || !$couleur || !$fond || !$legende || !$marge_min || !$eleves_ordre
  )
{
  Json::end( FALSE , 'Erreur avec les données transmises !' );
}

Form::save_choix('releve_synthese');

// Pour les évaluations à la volée.
if($_SESSION['USER_PROFIL_TYPE']=='professeur')
{
  Session::generer_jeton_anti_CSRF('evaluation_ponctuelle');
  $CSRF_eval_eclair = Session::$_CSRF_value;
}

// Fermeture de session (mais pas destruction, juste écriture et libération des données pour éviter un verrouillage en écriture)
Session::write_close();

// Bricoles restantes
$marge_gauche = $marge_droite = $marge_haut = $marge_bas = $marge_min ;

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// INCLUSION DU CODE COMMUN À PLUSIEURS PAGES
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$make_officiel = FALSE;
$make_brevet   = FALSE;
$make_action   = '';
$make_html     = TRUE;
$make_pdf      = TRUE;
$make_graph    = FALSE;

require(CHEMIN_DOSSIER_INCLUDE.'noyau_items_synthese.php');

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Affichage du résultat
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$retour = '';

if($affichage_direct)
{
  $retour .=
    '<hr>'.NL
  . '<ul class="puce">'.NL
  .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
  . '</ul>'.NL
  . $html;
}
else
{
  if($type_individuel)
  {
    $retour .=
      '<h2>Relevé individuel</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','individuel',$fichier_nom).'"><span class="file file_htm">Explorer / Détailler (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    . '</ul>'.NL;
  }
  if($type_synthese)
  {
    $retour .=
      '<h2>Synthèse collective</h2>'.NL
    . '<ul class="puce">'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./releve_html.php?fichier='.str_replace('<REPLACE>','synthese',$fichier_nom).'"><span class="file file_htm">Voir (format <em>html</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="'.URL_DIR_EXPORT.str_replace('<REPLACE>','synthese',$fichier_nom).'.pdf"><span class="file file_pdf">Archiver / Imprimer (format <em>pdf</em>).</span></a></li>'.NL
    .   '<li><a target="_blank" rel="noopener noreferrer" href="./force_download.php?fichier='.str_replace('<REPLACE>','synthese',$fichier_nom).'.csv"><span class="file file_txt">Exploitation tableur (format <em>csv</em>).</span></a></li>'.NL
    . '</ul>'.NL;
  }
}

Json::add_tab( array(
  'direct' => $affichage_direct ,
  'bilan'  => $retour ,
) );
Json::end( TRUE );

?>
