<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
if($_SESSION['SESAMATH_ID']==ID_DEMO) {Json::end( FALSE , 'Action désactivée pour la démo.' );}

$action    = Clean::post('f_action'   , 'texte');
$usage     = Clean::post('f_usage'    , 'entier');
$categorie = Clean::post('f_categorie', 'texte');
$id        = Clean::post('f_id'       , 'entier');
$ordre     = Clean::post('f_ordre'    , 'entier');
$synthese  = Clean::post('f_synthese' , 'texte');
$contenu   = Clean::post('f_contenu'  , 'texte');

$tab_categorie = array( 'mention' => TRUE , 'engagement' => TRUE , 'orientation' => TRUE );

$verif_post = isset($tab_categorie[$categorie]) && $ordre && $synthese && $contenu;

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Ajouter une nouvelle décision / Dupliquer une décision existante
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( (($action=='ajouter')||($action=='dupliquer')) && $verif_post )
{
  // Vérifier que la synthese de la décision est disponible
  if( DB_STRUCTURE_OFFICIEL::DB_tester_officiel_decision($synthese) )
  {
    Json::end( FALSE , 'Synthèse déjà utilisée !' );
  }
  // Insérer l’enregistrement
  $decision_id = DB_STRUCTURE_OFFICIEL::DB_ajouter_officiel_decision( $categorie , $ordre , $synthese , $contenu );
  // Afficher le retour
  Json::add_str('<tr id="id_'.$decision_id.'" data-used="0" class="new">');
  Json::add_str(  '<td>'.$ordre.'</td>');
  Json::add_str(  '<td>'.html($synthese).'</td>');
  Json::add_str(  '<td>'.html($contenu).'</td>');
  Json::add_str(  '<td class="nu">');
  Json::add_str(    '<q class="modifier"'.infobulle('Modifier cette décision.').'></q>');
  Json::add_str(    '<q class="dupliquer"'.infobulle('Dupliquer cette décision.').'></q>');
  Json::add_str(    '<q class="supprimer"'.infobulle('Supprimer cette décision.').'></q>');
  Json::add_str(  '</td>');
  Json::add_str('</tr>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Modifier une décision existante
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='modifier') && $id && $verif_post )
{
  // Vérifier que la synthese de la décision est disponible
  if( DB_STRUCTURE_OFFICIEL::DB_tester_officiel_decision($synthese,$id) )
  {
    Json::end( FALSE , 'Synthèse déjà utilisée !' );
  }
  // Mettre à jour l’enregistrement
  DB_STRUCTURE_OFFICIEL::DB_modifier_officiel_decision( $id , $ordre , $synthese , $contenu );
  // Afficher le retour
  Json::add_str('<td>'.$ordre.'</td>');
  Json::add_str('<td>'.html($synthese).'</td>');
  Json::add_str('<td>'.html($contenu).'</td>');
  Json::add_str('<td class="nu">');
  Json::add_str(  '<q class="modifier"'.infobulle('Modifier cette décision.').'></q>');
  Json::add_str(  '<q class="dupliquer"'.infobulle('Dupliquer cette décision.').'></q>');
  Json::add_str(  '<q class="supprimer"'.infobulle('Supprimer cette décision.').'></q>');
  Json::add_str('</td>');
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Supprimer une décision existante
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if( ($action=='supprimer') && $id && isset($tab_categorie[$categorie]) && !is_null($usage) && $synthese )
{
  // Effacer l’enregistrement
  DB_STRUCTURE_OFFICIEL::DB_supprimer_officiel_decision( $id , $categorie , $usage );
  // Log d’une action sensible
  if($usage)
  {
    SACocheLog::ajouter('Suppression de la décision "'.$synthese.'" ('.$categorie.' n°'.$id.'), et donc son usage dans les bilans officiels.');
    // Notifications (rendues visibles ultérieurement)
    $notification_contenu = date('d-m-Y H:i:s').' '.$_SESSION['USER_PRENOM'].' '.$_SESSION['USER_NOM'].' a supprimé la décision "'.$synthese.'" ('.$categorie.' n°'.$id.'), et donc son usage dans les bilans officiels.'."\r\n";
    DB_STRUCTURE_NOTIFICATION::enregistrer_action_admin( $notification_contenu , $_SESSION['USER_ID'] );
  }
  // Afficher le retour
  Json::end( TRUE );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On ne devrait pas en arriver là...
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Json::end( FALSE , 'Erreur avec les données transmises !' );

?>
