<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = html(Lang::_('Livret Scolaire')).' &rarr; '.html(Lang::_('Épreuves du DNB'));
?>

<ul class="puce">
  <li><span class="manuel"><a class="pop_up" href="<?php echo SERVEUR_DOCUMENTAIRE ?>?fichier=officiel__livret_scolaire_administration#toggle_dnb">DOC : Administration du Livret Scolaire &rarr; Épreuves du DNB</a></span></li>
  <li><span class="astuce">Ce menu ne sert que pour le <b>bilan de fin de cycle 4</b> (sans objet pour les <b>bilans périodiques</b> ou de <b>fin d’autres cycles</b>).</span></li>
  <li><span class="probleme">Ce menu est facultatif ; les données éventuellement saisies sur cette page sont à usage interne, sans caractère officiel et non transmises à <em>LSU</em> ou <em>Cyclades</em>.</span></li>
</ul>

<hr>

<?php
$annee_siecle = To::annee_scolaire('siecle');
if($annee_siecle==2019)
{
  $tab_epreuves = array(
    'fr'       => 'Contrôle continu<br>français',
    'maths'    => 'Contrôle continu<br>mathématiques',
    'hgemc'    => 'Contrôle continu<br>histoire-géographie EMC',
    'sciences' => 'Contrôle continu<br>sciences',
  );
}
else
{
  $tab_epreuves = array(
    'oral'     => 'Épreuve orale<br>soutenance d’un projet',
    'fr'       => 'Épreuve écrite<br>français',
    'maths'    => 'Épreuve écrite<br>mathématiques',
    'hgemc'    => 'Épreuve écrite<br>histoire-géographie EMC',
    'sciences' => 'Épreuve écrite<br>sciences',
  );
}

// Classes

$DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_classes_dnb();
if( empty($DB_TAB) )
{
  echo'<p class="danger">Aucune classe n’est associée à une page du livret concernée par ce dispositif !<br>Si besoin, commencez par <a href="./index.php?page=livret&amp;section=classes">associer les classes au livret scolaire</a>.</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
$tab_classe = array();
foreach($DB_TAB as $DB_ROW)
{
  $tab_classe[$DB_ROW['groupe_id']] = array(
    'nom'    => '<h2>'.html($DB_ROW['groupe_nom']).'</h2>',
    'eleves' => array(),
  );
}

// Élèves

$DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_eleve_dnb();
if( empty($DB_TAB) )
{
  echo'<p class="danger">Aucun élève présent dans les classes concernées par ce dispositif !</p>'.NL;
  return; // Ne pas exécuter la suite de ce fichier inclus.
}
$tab_notes = array_fill_keys( array_keys($tab_epreuves) , NULL );
$tab_eleve = array();
foreach($DB_TAB as $DB_ROW)
{
  $tab_classe[$DB_ROW['groupe_id']]['eleves'][] = $DB_ROW['user_id'];
  $tab_eleve[$DB_ROW['user_id']] = array(
    'nom'   => html($DB_ROW['user_nom'].' '.$DB_ROW['user_prenom']),
    'notes' => $tab_notes,
  );
}

// Notes

$liste_eleve = implode( ',' , array_keys($tab_eleve) );
$DB_TAB = DB_STRUCTURE_LIVRET::DB_lister_note_dnb( $liste_eleve );
foreach($DB_TAB as $DB_ROW)
{
  $tab_eleve[$DB_ROW['eleve_id']]['notes'][$DB_ROW['dnb_epreuve']] = $DB_ROW['dnb_note'];
}

// Affichage

$javascript = '';
foreach($tab_classe as $groupe_id => $tab_classe_infos)
{
  $javascript .= 'window.memo_change['.$groupe_id.'] = [];';
  echo $tab_classe_infos['nom'];
  echo '<form action="#" method="post" id="form_'.$groupe_id.'">';
  echo '<table><thead><tr><td class="nu"><button id="save_'.$groupe_id.'" type="button" class="valider">Enregistrer</button><br><label id="ajax_'.$groupe_id.'"></label></td>';
  foreach($tab_epreuves as $epreuve_nom)
  {
    echo '<th class="hc">'.$epreuve_nom.'</th>';
  }
  echo '</tr></thead><tbody id="tbody_'.$groupe_id.'">';
  foreach($tab_classe_infos['eleves'] as $eleve_id)
  {
    $tab_eleve_infos = $tab_eleve[$eleve_id];
    echo '<tr data-eleve="'.$eleve_id.'"><th>'.$tab_eleve_infos['nom'].'</th>';
    foreach($tab_eleve_infos['notes'] as $dnb_epreuve => $dnb_note)
    {
      $max = ( ( $dnb_epreuve != 'hgemc' ) && ( $dnb_epreuve != 'sciences' ) ) ? 100 : 50 ;
      echo '<td class="hc"><input type="number" min="0" max="'.$max.'" step="1" id="'.$dnb_epreuve.$eleve_id.'" data-epreuve="'.$dnb_epreuve.'" value="'.$dnb_note.'"></td>';
    }
    echo '</tr>';
  }
  echo '</tbody></table>';
  echo '</form><hr>';
}

// Javascript
Layout::add( 'js_inline_before' , 'window.memo_change = [];'.$javascript );
?>
