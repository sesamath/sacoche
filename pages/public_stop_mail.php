<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}
$TITRE = 'Arrêt de tout envoi de courriel'; // Pas de traduction car pas de choix de langue à ce niveau.

// Récupération du code
// Pour l’instant on n’effectue aucune vérification afin de ne pas donner d’indice, on fera toutes les vérifications après l’appel ajax.
// On teste seulement si le code correspond à une installation et une structure compatibles.
$stop_code = Clean::get('stop_code', 'code');

// Protection contre les attaques par force brute des robots (piratage compte ou envoi intempestif de courriels)
list($html_imgs,$captcha_soluce) = Outil::captcha();
Session::_set('TMP','CAPTCHA' , array(
  'TIME'   => $_SERVER['REQUEST_TIME'] ,
  'DELAI'  => 4, // en secondes, est ensuite incrémenté en cas d’erreur
  'SOLUCE' => $captcha_soluce,
) );

?>

<form id="form_stop" action="#" method="post">
  <p class="danger">Cela concernera aussi les notifications de mise à disposition de bilans officiels ainsi que la possibilité de récupérer un nouveau mot de passe.</p>
  <p class="astuce">Pour confirmer votre décision, veuiller soumettre ce formulaire.</p>
  <div><label class="tab">Code :</label><input id="f_code" name="f_code" size="55" type="text" value="<?php echo $stop_code ?>" readonly></div>
  <div><label class="tab">Anti-robot :</label><span id="captcha_game">Cliquer du plus petit au plus grand <?php echo $html_imgs ?></span><span id="captcha_init" class="hide">Ordre enregistré. <button type="button" class="actualiser">Recommencer.</button></span><input id="f_captcha" name="f_captcha" type="text" value="" class="invisible"></div>
  <p><span class="tab"></span><button id="f_bouton_valider" type="submit" class="valider">Valider.</button><label id="ajax_msg"></label></p>
</form>

<form id="zone_confirmation" class="hide">
  <div><label class="tab">Établissement :</label><span id="report_structure"></span></div>
  <div><label class="tab">Courriel :</label><span id="report_courriel"></span></div>
  <div><label class="tab">Compte :</label><span id="report_user"></span></div>
  <p><label class="valide">Demande confirmée : <em>SACoche</em> n’enverra plus de courriel au compte ci-dessus.</label></p>
  <p><label class="astuce">Si votre adresse est associée à plusieurs comptes <em>SACoche</em>, alors la démarche est à effectuer pour chacun d’eux.</label></p>
</form>

<hr>

<div class="hc"><a href="./index.php">[ Retour en page d’accueil ]</a></div>
