# AGPL-3.0

## Licence de SACoche

[GNU Affero General Public License v3.0](https://sacoche.sesamath.net/sacoche/COPYING)

## Licences des librairies incluses

<https://sacoche.sesamath.net/sacoche/LICENCES.html>

