![SACoche](https://sacoche.sesamath.net/sacoche/_img/logo_grand.gif "SACoche")

**SACoche** est un site internet qui permet :

- d’évaluer les élèves par compétences
- de conserver un historique de leur parcours
- de déterminer un état d’acquisition de chaque compétence
- de les collecter pour estimer la maitrise du socle commun

Consultez [le site officiel du projet](https://sacoche.sesamath.net) pour toute information concernant **SACoche**.

## Présentation

<https://sacoche.sesamath.net?page=presentation>

## Utilisation

<https://sacoche.sesamath.net?page=utilisation>

Si installation sur un serveur personnel :
- [configuration minimale](https://sacoche.sesamath.net/index.php?page=utilisation__serveur_perso__config_mini)
- [téléchargement](https://sacoche.sesamath.net/index.php?page=utilisation__serveur_perso__download)

## Documentations

<https://sacoche.sesamath.net?page=documentation>

## Support / Contact

<https://sacoche.sesamath.net?page=support>

## Échanger / Contribuer

<https://sacoche.sesamath.net?page=echanger>

Vous pouvez aussi éventuellement [ouvrir un ticket](https://forge.apps.education.fr/sesamath/sacoche/-/issues) sur le dépôt GIT.

## Licence

[GNU Affero General Public License v3.0](https://sacoche.sesamath.net/sacoche/LICENCES.html)

## CNIL / RGPD

<https://sacoche.sesamath.net?page=cnil>

[Mentions légales](https://sacoche.sesamath.net?page=presentation__accueil__mentions_legales) du portail du projet.
