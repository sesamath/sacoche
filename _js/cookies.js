// Pour info.
// Ces scripts ont été minifiés et ajoutés à la fin de jquery-librairies.js

/**
 * Fonction pour écrire un cookie
 * @see https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie
 *
 * @param name   nom du cookie
 * @param value  valeur du cookie
 * @return void
 */
function SetCookie( name , value , expires , path , domain , security , samesite )
{
  // Pas de valeurs par défaut dans les params d'une fonction avant ECMAScript 2015 (https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Fonctions/Valeurs_par_d%C3%A9faut_des_arguments)
  // Paramètres aussi définis dans les fichier de classes PHP Cookie & Session
  var expires  = (typeof expires  !== 'undefined') ? expires  : null;
  var path     = (typeof path     !== 'undefined') ? path     : null;
  var domain   = (typeof domain   !== 'undefined') ? domain   : null;
  var security = (typeof security !== 'undefined') ? security : null;
  var samesite = (typeof samesite !== 'undefined') ? samesite : 'Lax';
  document.cookie = name + '=' + escape(value)
                  + ( (expires ==null) ? '' : ('; expires='+expires.toGMTString()) )
                  + ( (path    ==null) ? '' : ('; path='+path) )
                  + ( (domain  ==null) ? '' : ('; domain='+domain) )
                  + ( (security==null) ? '' : '; secure' )
                  + ( (samesite==null) ? '' : '; samesite='+samesite ) ;
}

/**
 * Fonction pour lire un cookie
 *
 * @param name   nom du cookie
 * @return string
 */
function GetCookie(name)
{
  var arg  = name + '=';
  var alen = arg.length;
  var clen = document.cookie.length;
  var i = 0;
  while(i<clen)
  {
    var j = i+alen;
    if(document.cookie.substring(i,j)==arg)
    {
      return getCookieVal(j);
    }
    i = document.cookie.indexOf(' ',i)+1;
    if(i==0)
    {
      break;
    }
  }
  return null;
}
function getCookieVal(offset)
{
  var endstr = document.cookie.indexOf(';', offset);
  if (endstr==-1)
  {
    endstr = document.cookie.length;
  }
  return unescape(document.cookie.substring(offset, endstr));
}
