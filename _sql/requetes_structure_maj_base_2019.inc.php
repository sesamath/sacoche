<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-11-30 => 2019-01-23
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-11-30')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-01-23';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification des spécifications du Livret Scolaire pour "langue et culture régionale"
    if(empty($reload_sacoche_livret_langcultregion))
    {
      $reload_sacoche_livret_langcultregion = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_langcultregion.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // ajout de lignes à [sacoche_livret_enscompl]
    if(empty($reload_sacoche_livret_enscompl))
    {
      $reload_sacoche_livret_enscompl = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_enscompl.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-01-23 => 2019-02-02
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-01-23')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-02-02';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"envoi_mail_parent\\"" , "\\"delai_consultation_famille\\":0,\\"envoi_mail_parent\\"" ) ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"ENVOI_MAIL_PARENT\\"" , "\\"DELAI_CONSULTATION_FAMILLE\\":0,\\"ENVOI_MAIL_PARENT\\"" ) ' );
    // ajout colonne à [sacoche_officiel_archive]
    if(empty($reload_sacoche_officiel_archive))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_officiel_archive ADD archive_date_visibilite_famille DATE DEFAULT NULL COMMENT "Ne vaut normalement jamais NULL." AFTER archive_date_generation ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_date_visibilite_famille = archive_date_generation ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-02-02 => 2019-02-06
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-02-02')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-02-06';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un champ à la table [sacoche_user]
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user ADD user_csv_encodage VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT "Windows-1252" AFTER user_form_options' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-02-06 => 2019-02-13
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-02-06')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-02-13';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // correction de codes matières issus de SIECE ayant pu mal être mémorisés, ce qui peut provoquer un export LSU incorrect
    $tab_SIECLE = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('sts_emp_UAI');
    if( !empty($tab_SIECLE) && !empty($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE']) )
    {
      $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT matiere_id, matiere_ref FROM sacoche_matiere WHERE matiere_id>'.ID_MATIERE_PARTAGEE_MAX.' AND matiere_siecle=1 AND matiere_code="0" ' );
      if(!empty($DB_TAB))
      {
        $tab_matiere = array();
        foreach($DB_TAB as $DB_ROW)
        {
          $tab_matiere[$DB_ROW['matiere_ref']] = $DB_ROW['matiere_id'];
        }
        foreach($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE'] as $tab)
        {
          $code_matiere = (string)$tab['@attributes']['CODE'];
          $code_gestion = $tab['CODE_GESTION'];
          if(isset($tab_matiere[$code_gestion]))
          {
            $matiere_id = $tab_matiere[$code_gestion];
            DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_code="'.$code_matiere.'" WHERE matiere_id='.$matiere_id );
          }
        }
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-02-13 => 2019-03-22
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-02-13')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-03-22';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout colonne à [sacoche_courriel_erreur]
    if(empty($reload_sacoche_courriel_erreur))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_courriel_erreur ADD erreur_users VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" AFTER erreur_date ' );
    }
    // modification colonne de [sacoche_catalogue_appreciation]
    if(empty($reload_sacoche_catalogue_appreciation))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_catalogue_appreciation CHANGE appreciation_contenu appreciation_contenu VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" ' );
    }
    // Pour le livret scolaire, $_SESSION['OFFICIEL'] n’était pas mémorisé dans sacoche_officiel_archive, ce qui a commencé à poser problème avec l’usage de LIVRET_AFFICHER_LOGO_EN
    // Du coup on l’ajoute rétroactivement aux archives le mieux possible
    // Lever si besoin une limitation de GROUP_CONCAT (group_concat_max_len est par défaut limité à une chaîne de 1024 caractères) ; éviter plus de 8096 (http://www.glpi-project.org/forum/viewtopic.php?id=23767).
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'SET group_concat_max_len = 8096');
    $listing_id = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT GROUP_CONCAT(officiel_archive_id) FROM sacoche_officiel_archive WHERE archive_type="livret" AND archive_contenu NOT LIKE "%LIVRET_AFFICHER_LOGO_EN%" ' );
    if(!empty($listing_id))
    {
      $tab_configuration = DB_STRUCTURE_OFFICIEL_CONFIG::DB_recuperer_configuration( 'livret' , 'defaut' );
      $tab_officiel = array('OFFICIEL'=>array());
      foreach($tab_configuration as $key => $val)
      {
        $tab_officiel['OFFICIEL']['LIVRET_'.Clean::upper($key)] = $val;
      }
      $string_officiel = substr( str_replace( '"' , '\\"' , json_encode($tab_officiel) ) , 1 , -1);
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"LIVRET\\":" , "'.$string_officiel.',\\"LIVRET\\":" ) WHERE officiel_archive_id IN('.$listing_id.') ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-03-22 => 2019-05-29
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-03-22')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-05-29';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    if(empty($reload_sacoche_matiere_famille))
    {
      // Ajout de familles de matières
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT sacoche_matiere_famille VALUES ( 49, 2, "Biotechnologie génie biol.- biochimique (suite)") ');
    }
    // L’ajout de matières cause des conflits lorsque matiere_ref est déjà pris par une matière spécifique.
    // Il s’avère plus stratégique de 
    // 1) récupérer les matières spécifiques et les matières partagées utilisées
    // 2) recharger toute la table des matières partagées
    // 3)recréer ou convertir les matières spécifiques et réactiver les matières partagées utilisées
    // PS. On laisse tomber toutes les matières en doublon "option", "hors établissement", "par correspondance"...
    // Go :
    // récupération des informations sur les matières
    $DB_TAB_communes    = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE (matiere_active=1 OR matiere_siecle=1) AND matiere_id<='.ID_MATIERE_PARTAGEE_MAX);
    $DB_TAB_specifiques = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE matiere_id>'.ID_MATIERE_PARTAGEE_MAX);
    if(empty($reload_sacoche_matiere))
    {
      // rechargement de la table sacoche_matiere
      $reload_sacoche_matiere = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_matiere.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // on remet en place les matières partagées
    foreach($DB_TAB_communes as $DB_ROW)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active='.$DB_ROW['matiere_active'].', matiere_siecle='.$DB_ROW['matiere_siecle'].', matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$DB_ROW['matiere_id'] );
    }
    // on remet en place les matières spécifiques
    foreach($DB_TAB_specifiques as $DB_ROW)
    {
      $matiere_id_partage = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT matiere_id FROM sacoche_matiere WHERE matiere_ref="'.$DB_ROW['matiere_ref'].'" LIMIT 1');
      if(!$matiere_id_partage)
      {
        // Pas de conflit : on remet la matière spécifique telle qu’elle était
        $DB_SQL = 'INSERT INTO sacoche_matiere(matiere_id, matiere_active, matiere_siecle, matiere_usuelle, matiere_famille_id, matiere_nb_demandes, matiere_ordre, matiere_code, matiere_ref, matiere_nom) '
                . 'VALUES(:matiere_id, :matiere_active, :matiere_siecle, 0, 0, :matiere_nb_demandes, :matiere_ordre, :matiere_code, :matiere_ref, :matiere_nom) ';
        $DB_VAR = array(
          ':matiere_id'          => $DB_ROW['matiere_id'],
          ':matiere_active'      => $DB_ROW['matiere_active'],
          ':matiere_siecle'      => $DB_ROW['matiere_siecle'],
          ':matiere_nb_demandes' => $DB_ROW['matiere_nb_demandes'],
          ':matiere_ordre'       => $DB_ROW['matiere_ordre'],
          ':matiere_code'        => $DB_ROW['matiere_code'],
          ':matiere_ref'         => $DB_ROW['matiere_ref'],
          ':matiere_nom'         => $DB_ROW['matiere_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
      else
      {
        // Conflit : on la convertit en matière partagée
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active=1, matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$matiere_id_partage );
        DB_STRUCTURE_MATIERE::DB_deplacer_referentiel_matiere( $DB_ROW['matiere_id'] , $matiere_id_partage );
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-05-29 => 2019-05-30
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-05-29')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-05-30';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Lors de la précédente maj de la base, pour les matières, on a oublié de conserver matiere_siecle (code revu depuis)
    $tab_SIECLE = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('sts_emp_UAI');
    if(!empty($tab_SIECLE))
    {
      if(!empty($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE']))
      {
        $DB_SQL = 'UPDATE sacoche_matiere SET matiere_siecle=1 WHERE matiere_ref=:matiere_ref ';
        foreach($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE'] as $tab)
        {
          $DB_VAR = array( ':matiere_ref' => $tab['CODE_GESTION'] );
          DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
        }
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-05-30 => 2019-06-05
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-05-30')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-06-05';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Une clef unique comprenant une colonne valant NULL ne tient pas compte de l’unicité de cette valeur...
    // Du coup la colonne [jointure_periode] pour 3 tables a été changée pour ne plus pouvoir valoir NULL
    // 1) Il faut déjà nettoyer d’éventuels doublons, en particulier pour la table [sacoche_livret_saisie] (seul cas remonté où le problème s’est posé)
    $DB_SQL = 'SELECT CONVERT( GROUP_CONCAT(livret_saisie_id SEPARATOR ",") , CHAR) AS listing_id, '
            . 'COUNT(*) AS nombre '
            . 'FROM sacoche_livret_saisie '
            . 'GROUP BY livret_page_ref,livret_page_periodicite,jointure_periode,rubrique_type,rubrique_id,cible_nature,cible_id,saisie_objet '
            . 'HAVING nombre>1 ';
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        $listing_id = $DB_ROW['listing_id'];
        $tab_saisie_id = explode( ',' , $listing_id );
        sort($tab_saisie_id);
        $saisie_id_conserve = array_pop($tab_saisie_id);
        $listing_id_delete = implode(',',$tab_saisie_id);
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_livret_saisie WHERE livret_saisie_id IN('.$listing_id_delete.') ' );
      }
    }
    $DB_SQL = 'SELECT user_id, livret_page_periodicite, jointure_periode, '
            . 'COUNT(*) AS nombre '
            . 'FROM sacoche_livret_export '
            . 'GROUP BY user_id,livret_page_periodicite,jointure_periode '
            . 'HAVING nombre>1 ';
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        // Pas de clef primaire ici, on ne fait pas dans la dentelle... A priori ce cas est peu probable de toutes façons...
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_livret_export WHERE user_id='.$DB_ROW['user_id'].' AND livret_page_periodicite="'.$DB_ROW['livret_page_periodicite'].'" AND jointure_periode IS NULL ' );
      }
    }
    $DB_SQL = 'SELECT groupe_id, livret_page_ref, livret_page_periodicite, jointure_periode, '
            . 'COUNT(*) AS nombre '
            . 'FROM sacoche_livret_jointure_groupe '
            . 'GROUP BY groupe_id,livret_page_ref,livret_page_periodicite,jointure_periode '
            . 'HAVING nombre>1 ';
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL );
    if(!empty($DB_TAB))
    {
      foreach($DB_TAB as $DB_ROW)
      {
        // Pas de clef primaire ici, on ne fait pas dans la dentelle... A priori ce cas est peu probable de toutes façons...
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_livret_jointure_groupe WHERE groupe_id='.$DB_ROW['groupe_id'].' AND livret_page_ref="'.$DB_ROW['livret_page_ref'].'" AND livret_page_periodicite="'.$DB_ROW['livret_page_periodicite'].'" AND jointure_periode IS NULL ' );
      }
    }
    // 2) On modifie la structure des tables concernées
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_saisie          SET jointure_periode=0   WHERE jointure_periode IS NULL ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export          SET jointure_periode="0" WHERE jointure_periode IS NULL OR jointure_periode="" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_jointure_groupe SET jointure_periode=0   WHERE jointure_periode IS NULL ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_saisie          CHANGE jointure_periode jointure_periode TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "Renseigné si livret_page_periodicite = periode ; @see sacoche_periode.periode_livret" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_export          CHANGE jointure_periode jointure_periode VARCHAR(2) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "0" COMMENT "Renseigné si livret_page_periodicite = periode ; @see sacoche_periode.periode_livret" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_jointure_groupe CHANGE jointure_periode jointure_periode TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "Renseigné si livret_page_periodicite = periode ; @see sacoche_periode.periode_livret" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-06-05 => 2019-06-12
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-06-05')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-06-12';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // correction de codes matières perdus lors de la maj de la table des matières (code revu depuis)
    $tab_SIECLE = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('sts_emp_UAI');
    if( !empty($tab_SIECLE) && !empty($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE']) )
    {
      $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT matiere_id, matiere_ref FROM sacoche_matiere WHERE matiere_id>'.ID_MATIERE_PARTAGEE_MAX.' AND matiere_siecle=1 AND matiere_code="0" ' );
      if(!empty($DB_TAB))
      {
        $tab_matiere = array();
        foreach($DB_TAB as $DB_ROW)
        {
          $tab_matiere[$DB_ROW['matiere_ref']] = $DB_ROW['matiere_id'];
        }
        foreach($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE'] as $tab)
        {
          $code_matiere = (string)$tab['@attributes']['CODE'];
          $code_gestion = $tab['CODE_GESTION'];
          if(isset($tab_matiere[$code_gestion]))
          {
            $matiere_id = $tab_matiere[$code_gestion];
            DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_code="'.$code_matiere.'" WHERE matiere_id='.$matiere_id );
          }
        }
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-06-12 => 2019-06-24
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-06-12')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-06-24';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // changement de nom d’un ENT
    // /!\ Il faut penser à modifier aussi la table webmestre si multi-structures
    // UPDATE sacoche_webmestre.sacoche_convention SET connexion_nom="kosmos_auvergne-rhone-alpes" WHERE connexion_nom="kosmos_rhone-alpes"
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="kosmos_auvergne-rhone-alpes" WHERE parametre_nom="connexion_nom" AND parametre_valeur="kosmos_rhone-alpes" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-06-24 => 2019-07-11
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-06-24')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-07-11';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // insertion de lignes à [sacoche_officiel_decision]
    if(empty($reload_sacoche_officiel_decision))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_officiel_decision CHANGE decision_categorie decision_categorie VARCHAR(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "mention | orientation | engagement" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT sacoche_officiel_decision VALUES (NULL, "engagement", 1, "délégué de classe" , "Délégué de classe.") ');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT sacoche_officiel_decision VALUES (NULL, "engagement", 2, "délégué CA"        , "Délégué au conseil d’administration.") ');
    }
    // ajout colonne à [sacoche_officiel_jointure_decision]
    if(empty($reload_sacoche_officiel_jointure_decision))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_officiel_jointure_decision ADD decision_engagement SMALLINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "sacoche_officiel_decision.decision_id pour sacoche_officiel_decision.decision_categorie = engagement" AFTER decision_mention ' );
    }
    // ajout de paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"envoi_mail_parent\\"" , "\\"decision_engagement\\":0,\\"envoi_mail_parent\\"" ) ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"RELEVE_ENVOI_MAIL_PARENT\\"" , "\\"RELEVE_DECISION_ENGAGEMENT\\":0,\\"RELEVE_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="releve" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"LIVRET_ENVOI_MAIL_PARENT\\"" , "\\"LIVRET_DECISION_ENGAGEMENT\\":0,\\"LIVRET_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="livret" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"BULLETIN_ENVOI_MAIL_PARENT\\"" , "\\"BULLETIN_DECISION_ENGAGEMENT\\":0,\\"BULLETIN_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="bulletin" ' );
    // modification sacoche_parametre (connecteur CAS pour ENT)
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='kosmos_auvergne-rhone-alpes')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="cas.ent.auvergnerhonealpes.fr" WHERE parametre_nom="cas_serveur_host" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-07-11 => 2019-08-29
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-07-11')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-08-29';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // changement d’ENT
    // /!\ Il faut penser à modifier aussi la table webmestre si multi-structures
    // UPDATE sacoche_webmestre.sacoche_convention SET connexion_nom="kosmos_savoirsnumeriques62" WHERE connexion_nom="kosmos_savoirsnumeriques5962";
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="kosmos_savoirsnumeriques62" WHERE parametre_nom="connexion_nom" AND parametre_valeur="kosmos_savoirsnumeriques5962" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="cas.savoirsnumeriques62.fr" WHERE parametre_nom="cas_serveur_host" AND parametre_valeur="cas.savoirsnumeriques5962.fr" ' );
    // changement d’ENT
    // /!\ Il faut penser à modifier aussi la table webmestre si multi-structures
    // UPDATE sacoche_webmestre.sacoche_convention SET connexion_nom="entlibre_hauts-de-france" WHERE connexion_nom="entlibre_picardie";
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="entlibre_hauts-de-france" WHERE parametre_nom="connexion_nom" AND parametre_valeur="entlibre_picardie" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="enthdf.fr" WHERE parametre_nom="cas_serveur_host" AND parametre_valeur="ent.picardie.fr" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-08-29 => 2019-08-30
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-08-29')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-08-30';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // changement d’ENT
    // /!\ Il faut penser à modifier aussi la table webmestre si multi-structures
    // UPDATE sacoche_webmestre.sacoche_convention SET connexion_nom="kosmos_occitanie" WHERE connexion_nom="kosmos_entmip";
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="kosmos_occitanie" WHERE parametre_nom="connexion_nom" AND parametre_valeur="kosmos_entmip" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="cas.mon-ent-occitanie.fr" WHERE parametre_nom="cas_serveur_host" AND parametre_valeur="cas.entmip.fr" ' );
    // modification sacoche_parametre (connecteur CAS pour ENT)
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='itslearning_93')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="fdi93.itslfr-aws.com" WHERE parametre_nom="cas_serveur_host" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="access" WHERE parametre_nom="cas_serveur_root" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-08-30 => 2019-09-13
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-08-30')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-09-13';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // erreur de renseignement d'un paramètrage (connecteur CAS pour ENT)
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='kosmos_savoirsnumeriques62')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="cas.savoirsnumeriques62.fr" WHERE parametre_nom="cas_serveur_host" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-09-13 => 2019-09-17
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-09-13')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-09-17';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif colonne table sacoche_devoir
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir CHANGE devoir_eleves_ordre devoir_eleves_ordre VARCHAR(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT "nom" COMMENT "nom | prenom | classe | n° de plan de classe" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_devoir SET devoir_eleves_ordre="nom" WHERE devoir_eleves_ordre="alpha"' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-09-17 => 2019-09-26
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-09-17')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-09-26';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif colonne table sacoche_devoir
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user CHANGE user_reference user_reference VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Dans Sconet, ID_NATIONAL pour un élève (pour un prof ce pourrait être le NUMEN mais il n’est pas renseigné). Ce champ sert aussi pour un import tableur." ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-09-26 => 2019-10-05
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-09-26')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-10-05';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // mémoriser les niveaux spécifiques et les niveaux actifs
    $DB_SQL = 'SELECT niveau_id FROM sacoche_niveau WHERE niveau_actif = 1 AND niveau_id <= '.ID_NIVEAU_PARTAGE_MAX;
    $DB_COL_actifs = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    $DB_SQL = 'SELECT niveau_id , niveau_ref , niveau_nom FROM sacoche_niveau WHERE niveau_id > '.ID_NIVEAU_PARTAGE_MAX;
    $DB_TAB_persos = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    if(empty($reload_sacoche_niveau_famille))
    {
      //  rechargement de la table niveau_famille
      $reload_sacoche_niveau_famille = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau_famille.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    if(empty($reload_sacoche_niveau))
    {
      //  rechargement de la table niveau
      $reload_sacoche_niveau = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // remise en place des niveaux actifs
    if(!empty($DB_COL_actifs))
    {
      foreach($DB_COL_actifs as $niveau_id)
      {
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_actif = 1 WHERE niveau_id = '.$niveau_id );
      }
    }
    // remise en place des niveaux spécifiques
    if(!empty($DB_TAB_persos))
    {
      $DB_SQL = 'INSERT INTO sacoche_niveau(niveau_id, niveau_actif, niveau_usuel, niveau_famille_id, niveau_ordre, niveau_ref, code_mef, niveau_nom) '
              . 'VALUES(                   :niveau_id,:niveau_actif,:niveau_usuel,:niveau_famille_id,:niveau_ordre,:niveau_ref,:code_mef,:niveau_nom)';
      foreach($DB_TAB_persos as $DB_ROW)
      {
        $DB_VAR = array(
          ':niveau_id'         => $DB_ROW['niveau_id'],
          ':niveau_actif'      => 1,
          ':niveau_usuel'      => 0,
          ':niveau_famille_id' => 0,
          ':niveau_ordre'      => 999,
          ':niveau_ref'        => $DB_ROW['niveau_ref'],
          ':code_mef'          => "",
          ':niveau_nom'        => $DB_ROW['niveau_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-10-05 => 2019-10-14
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-10-05')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-10-14';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif ligne sacoche_niveau
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_usuel = 1, niveau_famille_id = 1, niveau_ref = "SCO", niveau_nom = "Cycle scolaire (tous niveaux)" WHERE niveau_id = 999999 ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-10-14 => 2019-10-31
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-10-14')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-10-31';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification sacoche_parametre (connecteur CAS pour ENT)
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='toutatice')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="idp/profile/cas" WHERE parametre_nom="cas_serveur_root" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="" WHERE parametre_nom="cas_serveur_url_validate" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-10-31 => 2019-11-08
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-10-31')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-11-08';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout colonne à [sacoche_livret_jointure_devoirsfaits_eleve]
    if(empty($reload_sacoche_livret_jointure_devoirsfaits_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_jointure_devoirsfaits_eleve DROP PRIMARY KEY, ADD UNIQUE eleve_id (eleve_id) ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_jointure_devoirsfaits_eleve ADD livret_devoirsfaits_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (livret_devoirsfaits_id) ' );
    }
    // ajout colonne à [sacoche_livret_jointure_modaccomp_eleve]
    if(empty($reload_sacoche_livret_jointure_modaccomp_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_jointure_modaccomp_eleve DROP PRIMARY KEY, DROP INDEX eleve_id, ADD UNIQUE modaccomp_key (eleve_id, livret_modaccomp_code), ADD INDEX (livret_modaccomp_code) ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_jointure_modaccomp_eleve ADD livret_modaccomp_id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (livret_modaccomp_id) ' );
    }
    // nouvelle table [sacoche_livret_jointure_devoirsfaits_periode]
    $reload_sacoche_livret_jointure_devoirsfaits_periode = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_jointure_devoirsfaits_periode.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_livret_jointure_modaccomp_periode]
    $reload_sacoche_livret_jointure_modaccomp_periode = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_jointure_modaccomp_periode.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // remplissage [sacoche_livret_jointure_devoirsfaits_periode] ; on initialise en affectant toutes les périodes aux élèves (selon leur classe)
    $DB_SQL = 'SELECT livret_devoirsfaits_id, periode_livret '
            . 'FROM sacoche_livret_jointure_devoirsfaits_eleve '
            . 'LEFT JOIN sacoche_user ON sacoche_livret_jointure_devoirsfaits_eleve.eleve_id = sacoche_user.user_id '
            . 'LEFT JOIN sacoche_jointure_groupe_periode ON sacoche_user.eleve_classe_id = sacoche_jointure_groupe_periode.groupe_id '
            . 'LEFT JOIN sacoche_periode USING(periode_id) '
            . 'WHERE periode_livret IS NOT NULL '
            . 'GROUP BY livret_devoirsfaits_id, periode_id ';
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL );
    if(!empty($DB_TAB))
    {
      $DB_SQL = 'INSERT INTO sacoche_livret_jointure_devoirsfaits_periode( livret_devoirsfaits_id, periode_livret) '
              . 'VALUES                                                  (:livret_devoirsfaits_id,:periode_livret) ';
      foreach($DB_TAB as $DB_ROW)
      {
        $DB_VAR = array(
          ':livret_devoirsfaits_id' => $DB_ROW['livret_devoirsfaits_id'] ,
          ':periode_livret'         => $DB_ROW['periode_livret'] ,
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
    // remplissage [sacoche_livret_jointure_modaccomp_periode] ; on initialise en affectant toutes les périodes aux élèves (selon leur classe)
    $DB_SQL = 'SELECT livret_modaccomp_id, periode_livret '
            . 'FROM sacoche_livret_jointure_modaccomp_eleve '
            . 'LEFT JOIN sacoche_user ON sacoche_livret_jointure_modaccomp_eleve.eleve_id = sacoche_user.user_id '
            . 'LEFT JOIN sacoche_jointure_groupe_periode ON sacoche_user.eleve_classe_id = sacoche_jointure_groupe_periode.groupe_id '
            . 'LEFT JOIN sacoche_periode USING(periode_id) '
            . 'WHERE periode_livret IS NOT NULL '
            . 'GROUP BY livret_modaccomp_id, periode_id ';
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL );
    if(!empty($DB_TAB))
    {
      $DB_SQL = 'INSERT INTO sacoche_livret_jointure_modaccomp_periode( livret_modaccomp_id, periode_livret) '
              . 'VALUES                                               (:livret_modaccomp_id,:periode_livret) ';
      foreach($DB_TAB as $DB_ROW)
      {
        $DB_VAR = array(
          ':livret_modaccomp_id' => $DB_ROW['livret_modaccomp_id'] ,
          ':periode_livret'      => $DB_ROW['periode_livret'] ,
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-11-08 => 2019-11-11
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-11-08')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2019-11-11';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // la précédente mise à jour, depuis corrigée, récupérait mal les périodes associées à la classe de l’élève
    // du coup on recommence la réaffectation des dispositifs aux périodes
    // [sacoche_livret_jointure_devoirsfaits_periode]
    $DB_SQL = 'SELECT livret_devoirsfaits_id, periode_livret '
            . 'FROM sacoche_livret_jointure_devoirsfaits_eleve '
            . 'LEFT JOIN sacoche_user ON sacoche_livret_jointure_devoirsfaits_eleve.eleve_id = sacoche_user.user_id '
            . 'LEFT JOIN sacoche_jointure_groupe_periode ON sacoche_user.eleve_classe_id = sacoche_jointure_groupe_periode.groupe_id '
            . 'LEFT JOIN sacoche_periode USING(periode_id) '
            . 'WHERE periode_livret IS NOT NULL '
            . 'GROUP BY livret_devoirsfaits_id, periode_id ';
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL );
    if(!empty($DB_TAB))
    {
      $DB_SQL = 'INSERT INTO sacoche_livret_jointure_devoirsfaits_periode( livret_devoirsfaits_id, periode_livret) '
              . 'VALUES                                                  (:livret_devoirsfaits_id,:periode_livret) '
              . 'ON DUPLICATE KEY UPDATE periode_livret=:periode_livret ';
      foreach($DB_TAB as $DB_ROW)
      {
        $DB_VAR = array(
          ':livret_devoirsfaits_id' => $DB_ROW['livret_devoirsfaits_id'] ,
          ':periode_livret'         => $DB_ROW['periode_livret'] ,
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
    // [sacoche_livret_jointure_modaccomp_periode]
    $DB_SQL = 'SELECT livret_modaccomp_id, periode_livret '
            . 'FROM sacoche_livret_jointure_modaccomp_eleve '
            . 'LEFT JOIN sacoche_user ON sacoche_livret_jointure_modaccomp_eleve.eleve_id = sacoche_user.user_id '
            . 'LEFT JOIN sacoche_jointure_groupe_periode ON sacoche_user.eleve_classe_id = sacoche_jointure_groupe_periode.groupe_id '
            . 'LEFT JOIN sacoche_periode USING(periode_id) '
            . 'WHERE periode_livret IS NOT NULL '
            . 'GROUP BY livret_modaccomp_id, periode_id ';
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL );
    if(!empty($DB_TAB))
    {
      $DB_SQL = 'INSERT INTO sacoche_livret_jointure_modaccomp_periode( livret_modaccomp_id, periode_livret) '
              . 'VALUES                                               (:livret_modaccomp_id,:periode_livret) '
              . 'ON DUPLICATE KEY UPDATE periode_livret=:periode_livret ';
      foreach($DB_TAB as $DB_ROW)
      {
        $DB_VAR = array(
          ':livret_modaccomp_id' => $DB_ROW['livret_modaccomp_id'] ,
          ':periode_livret'      => $DB_ROW['periode_livret'] ,
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
  }
}

?>
