<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2019-11-11 => 2020-03-09
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2019-11-11')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-03-09';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // nouvelle table [sacoche_crcn_domaine]
    $reload_sacoche_crcn_domaine = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_crcn_domaine.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_crcn_competence]
    $reload_sacoche_crcn_competence = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_crcn_competence.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_crcn_niveau]
    $reload_sacoche_crcn_niveau = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_crcn_niveau.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_crcn_saisie]
    $reload_sacoche_crcn_saisie = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_crcn_saisie.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // ajout de colonne à sacoche_livret_page
    if(empty($reload_sacoche_livret_page))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_page ADD livret_page_crcn TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "Si le dernier bilan périodique doit comporter les compétences numériques ; indique le niveau de maitrise maximal." AFTER livret_page_parcours' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_page SET livret_page_crcn=3 WHERE livret_page_ref IN("cm2","6e")' );
    }
    // ajout de paramètre
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("droit_gerer_livret_crcn" , "DIR,ENS")' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-03-09 => 2020-03-19
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-03-09')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-03-19';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // suppression de colonne
    if(empty($reload_sacoche_crcn_domaine))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_crcn_domaine DROP crcn_domaine_couleur ' );
    }
    if(empty($reload_sacoche_crcn_niveau))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_crcn_niveau DROP crcn_niveau_couleur ' );
    }
    // retrait de paramètre
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_parametre WHERE parametre_valeur="droit_gerer_livret_crcn"' );
    // modif exports LSU archivés
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export SET export_contenu = REPLACE( export_contenu , "\"epi-thematique\":" , "\"crcn\":[],\"epi-thematique\":" ) ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-03-19 => 2020-04-30
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-03-19')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-04-30';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // remplacement d’apostrophes dactylographiques par des apostrophes typographiques
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_abonnement SET abonnement_objet = REPLACE( abonnement_objet , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_abonnement SET abonnement_descriptif = REPLACE( abonnement_descriptif , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_geo_departement SET geo_departement_nom = REPLACE( geo_departement_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_colonne SET livret_colonne_titre = REPLACE( livret_colonne_titre , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_colonne SET livret_colonne_legende = REPLACE( livret_colonne_legende , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_element_item SET livret_element_item_nom = REPLACE( livret_element_item_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_element_theme SET livret_element_theme_nom = REPLACE( livret_element_theme_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_enscompl SET livret_enscompl_nom = REPLACE( livret_enscompl_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_epi_theme SET livret_epi_theme_nom = REPLACE( livret_epi_theme_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_langcultregion SET livret_langcultregion_nom = REPLACE( livret_langcultregion_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_modaccomp SET livret_modaccomp_nom = REPLACE( livret_modaccomp_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_page SET livret_page_resume = REPLACE( livret_page_resume , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_parcours_type SET livret_parcours_type_nom = REPLACE( livret_parcours_type_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_rubrique SET livret_rubrique_domaine = REPLACE( livret_rubrique_domaine , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_rubrique SET livret_rubrique_sous_domaine = REPLACE( livret_rubrique_sous_domaine , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_nom = REPLACE( matiere_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere_famille SET matiere_famille_nom = REPLACE( matiere_famille_nom , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_socle_composante SET socle_composante_nom_simple = REPLACE( socle_composante_nom_simple , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_socle_composante SET socle_composante_nom_officiel = REPLACE( socle_composante_nom_officiel , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_socle_domaine SET socle_domaine_nom_officiel = REPLACE( socle_domaine_nom_officiel , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user_profil SET user_profil_nom_long_singulier = REPLACE( user_profil_nom_long_singulier , "\'" , "’" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user_profil SET user_profil_nom_long_pluriel = REPLACE( user_profil_nom_long_pluriel , "\'" , "’" ) ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-04-30 => 2020-05-01
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-04-30')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-05-01';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Correction de noms de niveaux
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_nom = "Seconde 2 ens. exploration" WHERE niveau_id = 200002 ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_nom = "Seconde 3 ens. exploration" WHERE niveau_id = 200003 ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_nom = "Seconde 1 ens. exploration" WHERE niveau_id = 200004 ' );
    // Activation forcée des nouveaux niveaux de lycée pour les lycées
    $nb_niveaux_lycee_actifs = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT COUNT(*) FROM sacoche_niveau WHERE niveau_famille_id = 200 AND niveau_actif = 1 ' );
    if($nb_niveaux_lycee_actifs)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_actif = 1 WHERE niveau_id IN( 200005 , 201000 , 202000 ) ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-05-01 => 2020-06-06
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-05-01')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-06-06';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // nouvelle table [sacoche_livret_jointure_dnb_eleve]
    $reload_sacoche_livret_jointure_dnb_eleve = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_jointure_dnb_eleve.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-06-06 => 2020-06-22
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-06-06')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-06-22';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // archives des bilans officiels à mettre à jour suite l’ajout d’un paramètre dans une fonction
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\"__construct\",[true" , "\"__construct\",[true,\"A4\"" ) ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-06-22 => 2020-07-06
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-06-22')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-07-06';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout colonne table sacoche_devoir
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir ADD devoir_pluriannuel TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER devoir_diagnostic ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir ADD INDEX devoir_pluriannuel(devoir_pluriannuel)' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-07-06 => 2020-07-10
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-07-06')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-07-10';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de la colonne sacoche_referentiel_item.item_module
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_referentiel_item ADD item_module VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" AFTER item_lien' );
    if(empty($reload_sacoche_matiere_famille))
    {
      // Ajout de familles de matières
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT sacoche_matiere_famille VALUES ( 51, 3, "Services") ');
    }
    // L’ajout de matières cause des conflits lorsque matiere_ref est déjà pris par une matière spécifique.
    // Il s’avère plus stratégique de 
    // 1) récupérer les matières spécifiques et les matières partagées utilisées
    // 2) recharger toute la table des matières partagées
    // 3)recréer ou convertir les matières spécifiques et réactiver les matières partagées utilisées
    // PS. On laisse tomber toutes les matières en doublon "option", "hors établissement", "par correspondance"...
    // Go :
    // récupération des informations sur les matières
    $DB_TAB_communes    = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE (matiere_active=1 OR matiere_siecle=1) AND matiere_id<='.ID_MATIERE_PARTAGEE_MAX);
    $DB_TAB_specifiques = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE matiere_id>'.ID_MATIERE_PARTAGEE_MAX);
    if(empty($reload_sacoche_matiere))
    {
      // rechargement de la table sacoche_matiere
      $reload_sacoche_matiere = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_matiere.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // on remet en place les matières partagées
    foreach($DB_TAB_communes as $DB_ROW)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active='.$DB_ROW['matiere_active'].', matiere_siecle='.$DB_ROW['matiere_siecle'].', matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$DB_ROW['matiere_id'] );
    }
    // on remet en place les matières spécifiques
    foreach($DB_TAB_specifiques as $DB_ROW)
    {
      $matiere_id_partage = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT matiere_id FROM sacoche_matiere WHERE matiere_ref="'.$DB_ROW['matiere_ref'].'" LIMIT 1');
      if(!$matiere_id_partage)
      {
        // Pas de conflit : on remet la matière spécifique telle qu’elle était
        $DB_SQL = 'INSERT INTO sacoche_matiere( matiere_id, matiere_active, matiere_siecle, matiere_usuelle, matiere_famille_id, matiere_nb_demandes, matiere_ordre, matiere_code, matiere_ref, matiere_nom) '
                . 'VALUES                     (:matiere_id,:matiere_active,:matiere_siecle,:matiere_usuelle,:matiere_famille_id,:matiere_nb_demandes,:matiere_ordre,:matiere_code,:matiere_ref,:matiere_nom) ';
        $DB_VAR = array(
          ':matiere_id'          => $DB_ROW['matiere_id'],
          ':matiere_active'      => $DB_ROW['matiere_active'],
          ':matiere_siecle'      => $DB_ROW['matiere_siecle'],
          ':matiere_usuelle'     => 0,
          ':matiere_famille_id'  => 0,
          ':matiere_nb_demandes' => $DB_ROW['matiere_nb_demandes'],
          ':matiere_ordre'       => $DB_ROW['matiere_ordre'],
          ':matiere_code'        => $DB_ROW['matiere_code'],
          ':matiere_ref'         => $DB_ROW['matiere_ref'],
          ':matiere_nom'         => $DB_ROW['matiere_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
      else
      {
        // Conflit : on la convertit en matière partagée
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active=1, matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$matiere_id_partage );
        DB_STRUCTURE_MATIERE::DB_deplacer_referentiel_matiere( $DB_ROW['matiere_id'] , $matiere_id_partage );
      }
    }
    // Pour l’ajout ou la modification de niveaux, comme pour les matières, 
    // plutôt que de lister toutes les différences il est plus aisé de :
    // 1) récupérer les niveaux spécifiques et les niveaux partagés utilisés
    // 2) recharger toute la table des niveaux partagés
    // 3)recréer ou convertir les niveaux spécifiques et réactiver les niveaux partagés utilisés
    // Go :
    // mémoriser les niveaux spécifiques et les niveaux actifs
    $DB_SQL = 'SELECT niveau_id FROM sacoche_niveau WHERE niveau_actif = 1 AND niveau_id <= '.ID_NIVEAU_PARTAGE_MAX;
    $DB_COL_actifs = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    $DB_SQL = 'SELECT niveau_id , niveau_ref , niveau_nom FROM sacoche_niveau WHERE niveau_id > '.ID_NIVEAU_PARTAGE_MAX;
    $DB_TAB_persos = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    if(empty($reload_sacoche_niveau))
    {
      //  rechargement de la table niveau
      $reload_sacoche_niveau = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // remise en place des niveaux actifs
    if(!empty($DB_COL_actifs))
    {
      foreach($DB_COL_actifs as $niveau_id)
      {
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_actif = 1 WHERE niveau_id = '.$niveau_id );
      }
    }
    // remise en place des niveaux spécifiques
    if(!empty($DB_TAB_persos))
    {
      $DB_SQL = 'INSERT INTO sacoche_niveau( niveau_id, niveau_actif, niveau_usuel, niveau_famille_id, niveau_ordre, niveau_ref, code_mef, niveau_nom) '
              . 'VALUES                    (:niveau_id,:niveau_actif,:niveau_usuel,:niveau_famille_id,:niveau_ordre,:niveau_ref,:code_mef,:niveau_nom) ';
      foreach($DB_TAB_persos as $DB_ROW)
      {
        $DB_VAR = array(
          ':niveau_id'         => $DB_ROW['niveau_id'],
          ':niveau_actif'      => 1,
          ':niveau_usuel'      => 0,
          ':niveau_famille_id' => 0,
          ':niveau_ordre'      => 999,
          ':niveau_ref'        => $DB_ROW['niveau_ref'],
          ':code_mef'          => '',
          ':niveau_nom'        => $DB_ROW['niveau_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-07-10 => 2020-07-11
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-07-10')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-07-11';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Les commandes de création des tables [sacoche_crcn_competence] et [sacoche_crcn_niveau] comportaient une erreur
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SHOW TABLES FROM '.SACOCHE_STRUCTURE_BD_NAME.' LIKE "sacoche_crcn_competence"');
    if(empty($DB_TAB))
    {
      $reload_sacoche_crcn_competence = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_crcn_competence.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SHOW TABLES FROM '.SACOCHE_STRUCTURE_BD_NAME.' LIKE "sacoche_crcn_niveau"');
    if(empty($DB_TAB))
    {
      $reload_sacoche_crcn_niveau = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_crcn_niveau.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-07-11 => 2020-08-17
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-07-11')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-08-17';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"fusion_niveaux\\"" , "\\"format\\":\\"classique\\",\\"fusion_niveaux\\"" ) ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"BULLETIN_FUSION_NIVEAUX\\"" , "\\"BULLETIN_FORMAT\\": \\"classique\\",\\"BULLETIN_FUSION_NIVEAUX\\"" ) WHERE archive_ref="bulletin" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-08-17 => 2020-10-05
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-08-17')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-10-05';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif colonne sacoche_livret_saisie_memo_detail
    if(empty($reload_sacoche_livret_saisie_memo_detail))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_saisie_memo_detail CHANGE acquis_detail acquis_detail MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL' );
    }
    // ajout de paramètre
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("cas_service_api_key" , "")' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-10-05 => 2020-10-23
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-10-05')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-10-23';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif valeurs de sacoche_user.user_form_options
    $tab_modif = array(
      '\"cart_restriction_item\";b:0'  => '\"cart_restriction_item\";s:3:\"non\"',
      '\"cart_restriction_item\";b:1'  => '\"cart_restriction_item\";s:11:\"only_saisie\"',
      '\"cart_restriction_eleve\";b:0' => '\"cart_restriction_eleve\";s:3:\"non\"',
      '\"cart_restriction_eleve\";b:1' => '\"cart_restriction_eleve\";s:11:\"only_valeur\"',
    );
    foreach($tab_modif as $old => $new)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_form_options = REPLACE( user_form_options , "'.$old.'" , "'.$new.'" ) ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-10-23 => 2020-11-07
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-10-23')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-11-07';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif colonne de sacoche_structure_origine
    if(empty($reload_sacoche_structure_origine))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_structure_origine CHANGE structure_courriel structure_courriel VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-11-07 => 2020-12-01
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-11-07')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-12-01';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // retrait d’un point à la fin de certaines valeurs
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre_acquis SET acquis_legende = SUBSTRING(acquis_legende,1,LENGTH(acquis_legende)-1) WHERE SUBSTRING(acquis_legende,-1,1) = "."' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre_note SET note_legende = SUBSTRING(note_legende,1,LENGTH(note_legende)-1) WHERE SUBSTRING(note_legende,-1,1) = "."' );
    // ajout de paramètre
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("etat_acquisition_neutre_legende" , "Indéterminé")' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Version 2020-11-30 ???
// ////////////////////////////////////////////////////////////////////////////////////////////////////

// établissement mystérieusement retrouvé dans cette situation sur le serveur de Montpellier
if( $version_base_structure_actuelle == '2020-11-30' )
{
  $version_base_structure_actuelle = '2020-12-01';
  DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-12-01 => 2020-12-12
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-12-01')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2020-12-12';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification de colonnes à sacoche_jointure_devoir_eleve
    if(empty($reload_sacoche_jointure_devoir_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve CHANGE jointure_texte         jointure_texte         TEXT COLLATE utf8_unicode_ci DEFAULT NULL COMMENT "URL d’un fichier txt contenant le commentaire" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve CHANGE jointure_audio         jointure_audio         TEXT COLLATE utf8_unicode_ci DEFAULT NULL COMMENT "URL d’un fichier audio" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve CHANGE jointure_memo_autoeval jointure_memo_autoeval TEXT COLLATE utf8_unicode_ci DEFAULT NULL COMMENT "Au format json" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve CHANGE jointure_doc_sujet     jointure_doc_sujet     TEXT COLLATE utf8_unicode_ci DEFAULT NULL COMMENT "URL du document" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve CHANGE jointure_doc_corrige   jointure_doc_corrige   TEXT COLLATE utf8_unicode_ci DEFAULT NULL COMMENT "URL du document" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_jointure_devoir_eleve SET jointure_texte         = NULL WHERE jointure_texte         = "" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_jointure_devoir_eleve SET jointure_audio         = NULL WHERE jointure_audio         = "" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_jointure_devoir_eleve SET jointure_memo_autoeval = NULL WHERE jointure_memo_autoeval = "" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_jointure_devoir_eleve SET jointure_doc_sujet     = NULL WHERE jointure_doc_sujet     = "" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_jointure_devoir_eleve SET jointure_doc_corrige   = NULL WHERE jointure_doc_corrige   = "" ' );
    }
  }
}

?>
