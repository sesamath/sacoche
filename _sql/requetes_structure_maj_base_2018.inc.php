<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2017-12-22 => 2018-01-09
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2017-12-22')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-01-09';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification sacoche_parametre (nom connecteur CAS pour ENT)
    // Attention, penser à effectuer aussi la modif sur la table sacoche_webmestre.sacoche_convention
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='openent_monlycee')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="entlibre_monlycee" WHERE parametre_nom="connexion_nom" ' );
    }
    if($connexion_nom=='openent_pcn')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="entlibre_pcn" WHERE parametre_nom="connexion_nom" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-01-09 => 2018-01-14
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-01-09')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-01-14';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’une colonne à sacoche_jointure_devoir_eleve, et correction au passage du type erroné d’une autre colonne
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve CHANGE jointure_texte jointure_texte VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" ' );
    if(empty($reload_sacoche_jointure_devoir_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve ADD jointure_memo_autoeval VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Au format json" AFTER jointure_audio ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-01-14 => 2018-01-17
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-01-14')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-01-17';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif valeurs de sacoche_user_profil
    if(empty($reload_sacoche_user_profil))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user_profil SET user_profil_nom_court_singulier="PsyEN", user_profil_nom_court_pluriel="PsyEN", user_profil_nom_long_singulier="psychologue Éducation Nationale", user_profil_nom_long_pluriel="psychologues Éducation Nationale" WHERE user_profil_sigle="ORI" ');
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-01-17 => 2018-01-24
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-01-17')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-01-24';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de 2 colonnes à sacoche_jointure_devoir_eleve
    if(empty($reload_sacoche_jointure_devoir_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve ADD jointure_doc_sujet   VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "URL du document" AFTER jointure_memo_autoeval ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve ADD jointure_doc_corrige VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "URL du document" AFTER jointure_doc_sujet ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-01-24 => 2018-01-26
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-01-24')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-01-26';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout colonne table sacoche_devoir
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir ADD devoir_diagnostic TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER devoir_fini ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir ADD INDEX devoir_diagnostic(devoir_diagnostic)' );
    // correction de données incorrectes dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"cases_largeur\\":0" , "\\"cases_largeur\\":5" ) WHERE officiel_type="releve" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-01-26 => 2018-01-31
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-01-26')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-01-31';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification des archives suite à l’ajout d’un paramètre à une fonction
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\"multimatiere\",\"eleve\",1" , "\"multimatiere\",\"eleve\",1,true" ) WHERE archive_ref="releve" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\"multimatiere\",\"eleve\",0" , "\"multimatiere\",\"eleve\",0,true" ) WHERE archive_ref="releve" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-01-31 => 2018-02-02
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-01-31')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-02-02';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // l’archivage des nouveaux relevés ne tenait pas compte de l’ajout du paramètre précédent
    // du coup il faut relancer le patch pour les bilans générés entre ces deux mises à jour
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\"multimatiere\",\"eleve\",1,false,0" , "\"multimatiere\",\"eleve\",1,true,false,0" ) WHERE archive_ref="releve" AND sacoche_version="2018-01-31" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\"multimatiere\",\"eleve\",0,false,0" , "\"multimatiere\",\"eleve\",0,true,false,0" ) WHERE archive_ref="releve" AND sacoche_version="2018-01-31" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-02-02 => 2018-02-07
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-02-02')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-02-07';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif colonne de [sacoche_livret_jointure_modaccomp_eleve]
    if(empty($reload_sacoche_livret_jointure_modaccomp_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_jointure_modaccomp_eleve CHANGE info_complement info_complement TEXT COLLATE utf8_unicode_ci NOT NULL COMMENT "Dans le cas où la modalité d’accompagnement est PPRE ou CTR."' );
    }
    // ajout d’une ligne à [sacoche_livret_modaccomp]
    if(empty($reload_sacoche_livret_modaccomp))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_livret_modaccomp VALUES ("CTR","Contrat de réussite")' );
    }
    // nouvelle table [sacoche_livret_jointure_devoirsfaits_eleve]
    $reload_sacoche_livret_jointure_devoirsfaits_eleve = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_jointure_devoirsfaits_eleve.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_livret_langcultregion]
    $reload_sacoche_livret_langcultregion = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_langcultregion.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_livret_jointure_langcultregion_eleve]
    $reload_sacoche_livret_jointure_langcultregion_eleve = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_jointure_langcultregion_eleve.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // modif colonne de la table [sacoche_livret_saisie]
    if(empty($reload_sacoche_livret_saisie))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_saisie CHANGE rubrique_type rubrique_type VARCHAR(14) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "eval | socle | ap | epi | parcours | viesco | bilan | enscompl | langcultregion | attitude" ' );
    }
    // modif colonne de la table [sacoche_livret_export]
    if(empty($reload_sacoche_livret_export))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_export CHANGE jointure_periode jointure_periode VARCHAR(2) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT "Renseigné si livret_page_periodicite = periode ; @see sacoche_periode.periode_livret" ' );
    }
    // ajout de paramètres
    $droit_gerer_livret_modaccomp = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_gerer_livret_modaccomp"' );
    $droit_gerer_livret_enscompl  = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_gerer_livret_enscompl"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("droit_gerer_livret_devoirsfaits"   , "'.$droit_gerer_livret_modaccomp.'")' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("droit_gerer_livret_langcultregion" , "'.$droit_gerer_livret_enscompl.'")' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-02-07 => 2018-02-14
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-02-07')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-02-14';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"appreciation_generale_modele\\"" , "\\"appreciation_generale_position\\":\\"apres\\",\\"appreciation_generale_modele\\"" ) WHERE officiel_type="releve" ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"RELEVE_APPRECIATION_GENERALE_MODELE\\"" , "\\"RELEVE_APPRECIATION_GENERALE_POSITION\\":\\"apres\\",\\"RELEVE_APPRECIATION_GENERALE_MODELE\\"" ) WHERE archive_ref="releve" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-02-14 => 2018-02-23
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-02-14')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-02-23';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"envoi_mail_parent\\"" , "\\"page_bilan_classe\\":0,\\"envoi_mail_parent\\"" ) ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"RELEVE_ENVOI_MAIL_PARENT\\"" , "\\"RELEVE_PAGE_BILAN_CLASSE\\":0,\\"RELEVE_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="releve" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"LIVRET_ENVOI_MAIL_PARENT\\"" , "\\"LIVRET_PAGE_BILAN_CLASSE\\":0,\\"LIVRET_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="livret" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"BULLETIN_ENVOI_MAIL_PARENT\\"" , "\\"BULLETIN_PAGE_BILAN_CLASSE\\":0,\\"BULLETIN_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="bulletin" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-02-23 => 2018-03-02
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-02-23')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-03-02';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de paramètres dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"envoi_mail_parent\\"" , "\\"decision_mention\\":1,\\"decision_orientation\\":0,\\"envoi_mail_parent\\"" ) ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"RELEVE_ENVOI_MAIL_PARENT\\"" , "\\"RELEVE_DECISION_MENTION\\":1,\\"RELEVE_DECISION_ORIENTATION\\":0,\\"RELEVE_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="releve" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"LIVRET_ENVOI_MAIL_PARENT\\"" , "\\"LIVRET_DECISION_MENTION\\":1,\\"LIVRET_DECISION_ORIENTATION\\":0,\\"LIVRET_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="livret" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"BULLETIN_ENVOI_MAIL_PARENT\\"" , "\\"BULLETIN_DECISION_MENTION\\":1,\\"BULLETIN_DECISION_ORIENTATION\\":0,\\"BULLETIN_ENVOI_MAIL_PARENT\\"" ) WHERE archive_ref="bulletin" ' );
    // nouvelle table [sacoche_officiel_decision]
    $reload_sacoche_officiel_decision = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_officiel_decision.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_officiel_jointure_decision]
    $reload_sacoche_officiel_jointure_decision = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_officiel_jointure_decision.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-03-02 => 2018-03-07
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-03-02')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-03-07';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un champ à la table [sacoche_user]
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user ADD user_form_options TEXT COLLATE utf8_unicode_ci COMMENT "Choix d’options des formulaires enregistrées sérializées." AFTER user_param_favori' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_form_options = NULL' );
    // Y reporter les préférences auparavant enregistrées dans des fichiers
    $dossier_cookie = CHEMIN_DOSSIER_TMP.'cookie'.DS.$_SESSION['BASE'].DS; // CHEMIN_DOSSIER_COOKIE non utilisé car peut ne plus être défini
    if(is_dir($dossier_cookie))
    {
      $DB_SQL = 'UPDATE sacoche_user SET user_form_options =:form_options WHERE user_id=:user_id';
      $tab_fichier = FileSystem::lister_contenu_dossier($dossier_cookie);
      foreach($tab_fichier as $fichier_cookie)
      {
        if( substr($fichier_cookie,0,4) == 'user' )
        {
          $contenu = file_get_contents($dossier_cookie.$fichier_cookie);
          $user_id = (int)substr($fichier_cookie,4,-4); // userXXX.txt
          $DB_VAR = array(
            ':form_options' => $contenu,
            ':user_id'      => $user_id,
          );
          DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
        }
      }
      FileSystem::supprimer_dossier($dossier_cookie);
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-03-07 => 2018-03-14
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-03-07')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-03-14';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"moyenne_classe\\"" , "\\"aff_prop_sans_score\\":0,\\"moyenne_classe\\"" ) WHERE officiel_type="bulletin" ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"BULLETIN_MOYENNE_CLASSE\\"" , "\\"BULLETIN_AFF_PROP_SANS_SCORE\\":0,\\"BULLETIN_MOYENNE_CLASSE\\"" ) WHERE archive_ref="bulletin" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-03-14 => 2018-03-21
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-03-14')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-03-21';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // nouvelle table [sacoche_courriel_erreur]
    $reload_sacoche_courriel_erreur = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_courriel_erreur.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-03-21 => 2018-03-25
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-03-21')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-03-25';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // nouvelle table [sacoche_catalogue_categorie]
    $reload_sacoche_catalogue_categorie = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_catalogue_categorie.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_catalogue_appreciation]
    $reload_sacoche_catalogue_appreciation = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_catalogue_appreciation.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-03-25 => 2018-04-02
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-03-25')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-04-02';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("officiel_signature_transparence" , "non")' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-04-02 => 2018-04-03
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-04-02')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-04-03';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification des spécifications du Livret Scolaire pour "langue et culture régionale"
    if(empty($reload_sacoche_livret_langcultregion))
    {
      $reload_sacoche_livret_langcultregion = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_langcultregion.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_jointure_langcultregion_eleve SET livret_langcultregion_code="BAQ" WHERE livret_langcultregion_code="BAS"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_jointure_langcultregion_eleve SET livret_langcultregion_code="COS" WHERE livret_langcultregion_code="COR"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_jointure_langcultregion_eleve SET livret_langcultregion_code="OCI" WHERE livret_langcultregion_code="OCC"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_jointure_langcultregion_eleve SET livret_langcultregion_code="GSW" WHERE livret_langcultregion_code="ALS"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_livret_jointure_langcultregion_eleve WHERE livret_langcultregion_code="MOS"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_livret_saisie WHERE rubrique_type="langcultregion" AND rubrique_id=6' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export SET export_contenu = REPLACE( export_contenu , "{\"code\":\"BAS\"," , "{\"code\":\"BAQ\"," ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export SET export_contenu = REPLACE( export_contenu , "{\"code\":\"COR\"," , "{\"code\":\"COS\"," ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export SET export_contenu = REPLACE( export_contenu , "{\"code\":\"OCC\"," , "{\"code\":\"OCI\"," ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export SET export_contenu = REPLACE( export_contenu , "{\"code\":\"ALS\"," , "{\"code\":\"GSW\"," ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export SET export_contenu = REPLACE( export_contenu , ",\"langcultregion\":{\"code\":\"MOS\",\"positionnement\":2}" , "" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_export SET export_contenu = REPLACE( export_contenu , ",\"langcultregion\":{\"code\":\"MOS\",\"positionnement\":1}" , "" ) ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-04-03 => 2018-04-09
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-04-03')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-04-09';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif colonne table sacoche_devoir
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir CHANGE devoir_eleves_ordre devoir_eleves_ordre VARCHAR(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT "alpha" COMMENT "alpha | classe | n° de plan de classe" ' );
    // modification des menus
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_param_menu = REPLACE( user_param_menu , "information" , "information_perso,information_general" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_param_menu = REPLACE( user_param_menu , "parametrage" , "parametrage_compte,parametrage_pedago" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre_profil SET profil_param_menu = REPLACE( profil_param_menu , "information" , "information_perso,information_general" ) ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre_profil SET profil_param_menu = REPLACE( profil_param_menu , "parametrage" , "parametrage_compte,parametrage_pedago" ) ' );
    // nouvelle table [sacoche_plan_classe]
    $reload_sacoche_plan_classe = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_plan_classe.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // nouvelle table [sacoche_jointure_plan_eleve]
    $reload_sacoche_jointure_plan_eleve = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_jointure_plan_eleve.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
    // on en profite pour supprimer un dossier qui ne sert plus
    $dossier_badge = CHEMIN_DOSSIER_TMP.'badge'.DS.$_SESSION['BASE'].DS; // CHEMIN_DOSSIER_BADGE non utilisé car peut ne plus être défini
    if(is_dir($dossier_badge))
    {
      FileSystem::supprimer_dossier($dossier_badge);
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-04-09 => 2018-04-26
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-04-09')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-04-26';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"import_bulletin_notes\\"" , "\\"cycle_import_synthese_periode\\":0,\\"import_bulletin_notes\\"" ) WHERE officiel_type="livret" ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"IMPORT_BULLETIN_NOTES\\"" , "\\"CYCLE_IMPORT_SYNTHESE_PERIODE\\":0,\\"IMPORT_BULLETIN_NOTES\\"" ) WHERE archive_ref="livret" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-04-26 => 2018-05-17
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-04-26')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-05-17';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Correction d’un pb de TRUNCATE appliqué sur la table sacoche_devoir lors de l’initialisation annuelle, ce qui a initialisé son auto-incrément
    // On s’appuie sur le fichier de log des actions sensibles pour détecter l’erreur, donc il faut se trouver sur le serveur concerné...
    $date_version_bug_debut_sql = '2017-11-26';
    $date_version_bug_fin_sql   = '2018-05-16';
    $date_bug_sql = '';
    $devoir_id_max  = 0;
    $fichier_log_contenu = SACocheLog::lire($_SESSION['BASE']);
    if($fichier_log_contenu!==NULL)
    {
      $tab_lignes = SACocheLog::extraire_lignes($fichier_log_contenu);
      $nb_lignes = count($tab_lignes);
      for( $indice_ligne=0 ; $indice_ligne<$nb_lignes ; $indice_ligne++ )
      {
        list( $balise_debut , $date_heure , $utilisateur , $action , $balise_fin ) = explode("\t",$tab_lignes[$indice_ligne]);
        $date_sql = To::date_french_to_sql( str_replace('-','/',substr($date_heure,0,10)) );
        if( ($date_sql>=$date_version_bug_debut_sql) && ($date_sql<=$date_version_bug_fin_sql) && strpos($action,'Purge annuelle de la base en cours') )
        {
          $date_bug_sql = $date_sql;
          break;
        }
        else if ($date_sql>$date_version_bug_fin_sql)
        {
          break;
        }
      }
    }
    if($date_bug_sql)
    {
      // On est dans le cas où l’initialisation annuelle a entraîné le bug
      $devoir_id_max_devoir = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT MAX(devoir_id) FROM sacoche_devoir ' );
      $devoir_id_max_saisie = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT MAX(devoir_id) FROM sacoche_saisie ' );
      $devoir_id_max = max( $devoir_id_max_devoir , $devoir_id_max_saisie );
      if($devoir_id_max)
      {
        $tab_tables = array( 'sacoche_devoir' , 'sacoche_jointure_devoir_item' , 'sacoche_jointure_devoir_prof' , 'sacoche_jointure_devoir_eleve' );
        foreach( $tab_tables as $table )
        {
          DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE '.$table.' SET devoir_id=devoir_id+'.$devoir_id_max );
        }
        // Pour les saisies c’est plus embêtant, leur date a pu être actualisée, on ne peut pas s’y fier, alors on restreint aux prof et items concernés.
        $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT devoir_id, proprio_id FROM sacoche_devoir' );
        foreach($DB_TAB as $DB_ROW)
        {
          $devoir_id_new = (int)$DB_ROW['devoir_id'];
          $prof_id       = (int)$DB_ROW['proprio_id'];
          $items = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT GROUP_CONCAT(item_id) FROM sacoche_jointure_devoir_item WHERE devoir_id='.$devoir_id_new );
          if($items)
          {
            DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_saisie SET devoir_id='.$devoir_id_new.' WHERE devoir_id='.($devoir_id_new-$devoir_id_max).' AND item_id IN('.$items.') AND prof_id='.$prof_id );
          }
        }
        // Il reste le problème d’infos associées aux saisies qui ont été modifiées : saisie_date ; saisie_visible_date ; saisie_info
        // Pas d’autre solution pour ces cas là que de faire des REPLACE INTO à partir d’un extrait d’une sauvegarde antérieure au problème...
      }
    }
    // ajout de paramètres pour logguer l’existence du bug, même partiellement corrigé
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("log_bug_2018_may_purge_date"   , "'.$date_bug_sql.'")' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("log_bug_2018_may_id_increment" , "'.$devoir_id_max.'")' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("log_bug_2018_may_restore_date" , "")' ); // à renseigner si restauration ultérieure d’une svg à une date donnée
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-05-17 => 2018-05-25
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-05-17')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-05-25';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // augmentation de longueur du champ avec hash du mdp
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user CHANGE user_password user_password VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Hashage avec un salage aléatoire." ');
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-05-25 => 2018-05-28
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-05-25')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-05-28';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"cycle_import_synthese_periode\\"" , "\\"cycle_stop_recalcul_step\\":6,\\"cycle_import_synthese_periode\\"" ) WHERE officiel_type="livret" ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"CYCLE_IMPORT_SYNTHESE_PERIODE\\"" , "\\"CYCLE_STOP_RECALCUL_STEP\\":6,\\"CYCLE_IMPORT_SYNTHESE_PERIODE\\"" ) WHERE archive_ref="livret" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-05-28 => 2018-06-15
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-05-28')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-06-15';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif colonne de la table [sacoche_livret_export]
    if(empty($reload_sacoche_livret_export))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_export CHANGE export_contenu export_contenu MEDIUMTEXT COLLATE utf8_unicode_ci DEFAULT NULL ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-06-15 => 2018-06-18
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-06-15')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-06-18';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // nouvelle table [sacoche_acces_historique]
    $reload_sacoche_acces_historique = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_acces_historique.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-06-18 => 2018-06-27
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-06-18')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-06-27';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Présence sur 2 bases de bilans archivés de cycle curieux avec jointure_periode qui vaut 0 au lieu de "" ce qui cause des doublons à l’export.
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_livret_export WHERE jointure_periode="0" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-06-27 => 2018-08-29
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-06-27')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-08-29';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification sacoche_parametre (connecteur CAS pour ENT)
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='itop_oze_90')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="www.entcolleges90.fr" WHERE parametre_nom="cas_serveur_host" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-08-29 => 2018-09-13
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-08-29')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-09-13';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification sacoche_parametre (connecteur CAS pour ENT) ; déjà pris en compte mais oublié de le modifier dans tableau_sso.php
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='itop_oze_90')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="www.entcolleges90.fr" WHERE parametre_nom="cas_serveur_host" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-09-13 => 2018-09-18
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-09-13')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-09-18';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification sacoche_parametre (connecteur CAS pour ENT) ; déjà pris en compte mais oublié de le modifier dans tableau_sso.php
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='itslearning_elyco')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="access" WHERE parametre_nom="cas_serveur_root" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-09-18 => 2018-09-25
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-09-18')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-09-25';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Ajout de matières
    if(empty($reload_sacoche_matiere))
    {
      // Retiré : voir MAJ suivante pour explications
      /*
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES ( 240, 0, 0, 0,   2, 0, 255, "024000", "LCALG", "Langues et cultures de l’Antiquité latin et grec")' );

      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'UPDATE sacoche_matiere SET matiere_famille_id=3 WHERE matiere_id=9339' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (9391, 0, 0, 0,   3, 0, 255, "033901", "COE1" , "Coréen LV1")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (9392, 0, 0, 0,   3, 0, 255, "033902", "COE2" , "Coréen LV2")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (9393, 0, 0, 0,   3, 0, 255, "033903", "COE3" , "Coréen LV3")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (9394, 0, 0, 0,   3, 0, 255, "033904", "COE4" , "Coréen renforcé")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (9395, 0, 0, 0,   3, 0, 255, "033909", "COE9" , "Coréen langue de section")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (9396, 0, 0, 0,   3, 0, 255, "033919", "COE9C", "Coréen lettres étrangères")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (9397, 0, 0, 0,   3, 0, 255, "033908", "COE8" , "Littérature étrangère en coréen")' );

      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES ( 744, 0, 0, 0,   7, 0, 255, "074400", "EGLS", "Enseignements généraux liés à la specialité")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES ( 745, 0, 0, 0,   7, 0, 255, "074500", "PRIND", "Production industrielle")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1076, 0, 0, 0,  10, 0, 255, "107600", "HOGAZ", "Hockey sur gazon")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1077, 0, 0, 0,  10, 0, 255, "107700", "HOGLA", "Hockey sur glace")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1078, 0, 0, 0,  10, 0, 255, "107800", "PATIG", "Patinage sur glace")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1079, 0, 0, 0,  10, 0, 255, "107900", "DANSP", "Danse sportive")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1080, 0, 0, 0,  10, 0, 255, "108000", "TRAMP", "Trampoline")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1081, 0, 0, 0,  10, 0, 255, "108100", "SPMEC", "Sports mécaniques (auto-moto)")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1082, 0, 0, 0,  10, 0, 255, "108200", "HANDI", "Handisport")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1083, 0, 0, 0,  10, 0, 255, "108300", "COUOR", "Course d’orientation")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1084, 0, 0, 0,  10, 0, 255, "108400", "PELBA", "Pelote basque")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1085, 0, 0, 0,  10, 0, 255, "108500", "SPAER", "Sports aériens")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1086, 0, 0, 0,  10, 0, 255, "108600", "PENTA", "Pentathlon")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (1755, 0, 0, 0,  17, 0, 255, "175500", "HABIT", "Habitat")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2229, 0, 0, 0,  22, 0, 255, "222900", "MASEN", "Maintenance des systèmes électroniques navals")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2230, 0, 0, 0,  22, 0, 255, "223000", "MSCCR", "Maintenance des systèmes de contrôle-commande et de régulation")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2231, 0, 0, 0,  22, 0, 255, "223100", "DTLPL", "Dessins techniques et lecture des plans")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2323, 0, 0, 0,  23, 0, 255, "232300", "MASTN", "Maintenance des systèmes électrotechniques navals")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2422, 0, 0, 0,  24, 0, 255, "242200", "MSINA", "Maintenance des systèmes informatiques navals")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2611, 0, 0, 0,  26, 0, 255, "261100", "DENAV", "Description du navire")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2612, 0, 0, 0,  26, 0, 255, "261200", "CONAV", "Conduite du navire")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2613, 0, 0, 0,  26, 0, 255, "261300", "MAMAR", "Machines marines")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2614, 0, 0, 0,  26, 0, 255, "261400", "MATEL", "Matelotage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2615, 0, 0, 0,  26, 0, 255, "261500", "MANOE", "Manoeuvre")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2616, 0, 0, 0,  26, 0, 255, "261600", "STABI", "Stabilité")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2617, 0, 0, 0,  26, 0, 255, "261700", "OCEAN", "Océanographie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2618, 0, 0, 0,  26, 0, 255, "261800", "TECPE", "Techniques de pêche")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2619, 0, 0, 0,  26, 0, 255, "261900", "RAMEN", "Ramendage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2620, 0, 0, 0,  26, 0, 255, "262000", "SECSU", "Sécurité-survie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2621, 0, 0, 0,  26, 0, 255, "262100", "SYSCO", "Systèmes de commandes")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2622, 0, 0, 0,  26, 0, 255, "262200", "METEO", "Météorologie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2623, 0, 0, 0,  26, 0, 255, "262300", "MOLYS", "Molysmologie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2624, 0, 0, 0,  26, 0, 255, "262400", "NAVIG", "Navigation")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2625, 0, 0, 0,  26, 0, 255, "262500", "PROCP", "Processus de production")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2626, 0, 0, 0,  26, 0, 255, "262600", "MOYPR", "Moyens de production")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2627, 0, 0, 0,  26, 0, 255, "262700", "GRMAR", "Gestion des ressources marines")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2628, 0, 0, 0,  26, 0, 255, "262800", "CEMAR", "Cultures et elevages marins")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2629, 0, 0, 0,  26, 0, 255, "262900", "TRCAP", "Traitement des captures")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2630, 0, 0, 0,  26, 0, 255, "263000", "VACAP", "Valorisation des captures")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2631, 0, 0, 0,  26, 0, 255, "263100", "ECPEC", "Economie des pêches")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2632, 0, 0, 0,  26, 0, 255, "263200", "CMNAU", "Conduite moyens nautiques")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2633, 0, 0, 0,  26, 0, 255, "263300", "CMTER", "Conduite moyens terrestres")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2634, 0, 0, 0,  26, 0, 255, "263400", "COPEC", "Conduite de la pêche")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2635, 0, 0, 0,  26, 0, 255, "263500", "FECON", "Fonctionnement entreprise conchylicole")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2636, 0, 0, 0,  26, 0, 255, "263600", "ENNAV", "Entretien du navire")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2637, 0, 0, 0,  26, 0, 255, "263700", "CARGA", "Cargaison")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2638, 0, 0, 0,  26, 0, 255, "263800", "PRVEL", "Propulsion vélique")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2639, 0, 0, 0,  26, 0, 255, "263900", "RADAR", "Stage radar")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2640, 0, 0, 0,  26, 0, 255, "264000", "REBTQ", "Règles de barre tenue du quart")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2641, 0, 0, 0,  26, 0, 255, "264100", "ENREP", "Entretien réparation")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2642, 0, 0, 0,  26, 0, 255, "264200", "ARMAN", "Arrimage et manutention")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (2643, 0, 0, 0,  26, 0, 255, "264300", "DENST", "Description entretien stabilité")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (3094, 0, 0, 0,  30, 0, 255, "309400", "ERENV", "Espace rural et environnement")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (3095, 0, 0, 0,  30, 0, 255, "309500", "ECOLO", "Ecologie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (3096, 0, 0, 0,  30, 0, 255, "309600", "DEVDU", "Développement durable")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (3135, 0, 0, 0,  31, 0, 255, "313500", "HYASE", "Hygiène alimentation services")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (3474, 0, 0, 0,  34, 0, 255, "347400", "VDMAG", "Vente distribution magasinage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (3835, 0, 0, 0,  38, 0, 255, "383500", "RCTOU", "Relation commerciale et tourisme")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (3836, 0, 0, 0,  38, 0, 255, "383600", "ECOCO", "Economie et commercialisation")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4362, 0, 0, 0,  43, 0, 255, "436200", "OPRAD", "Optimisation processus administratifs")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4363, 0, 0, 0,  43, 0, 255, "436300", "COGRH", "Collaboration à la gestion des ressources humaines")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4364, 0, 0, 0,  43, 0, 255, "436400", "GMENT", "Gestion management des entreprises")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4365, 0, 0, 0,  43, 0, 255, "436500", "GEHTE", "Gestion de l’environnement humain et technique")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4366, 0, 0, 0,  43, 0, 255, "436600", "GEFIN", "Gestion économique et financière")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4604, 0, 0, 0,  46, 0, 255, "460400", "HUMCU", "Humanités et cultures")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4605, 0, 0, 0,  46, 0, 255, "460500", "METEC", "Méthodologies et techniques")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4606, 0, 0, 0,  46, 0, 255, "460600", "ATCRE", "Ateliers de création")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (4705, 0, 0, 0,  47, 0, 255, "470500", "PROFE", "Professionnalisation")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (5012, 0, 0, 0,  50, 0, 255, "501200", "ENSLA", "Enseignement de specialité en anglais")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (6005, 0, 0, 0,  60, 0, 255, "600500", "HIPPO", "Hippologie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (6202, 0, 0, 0,  62, 0, 255, "620200", "ANIMA", "Animalerie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (6609, 0, 0, 0,  66, 0, 255, "660900", "PVEGE", "Priductions végétales")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (6610, 0, 0, 0,  66, 0, 255, "661000", "AQUAC", "Aquaculture")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (6611, 0, 0, 0,  66, 0, 255, "661100", "PHORT", "Productions horticoles")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (6612, 0, 0, 0,  66, 0, 255, "661200", "PANIM", "Productions animales")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME ,  'INSERT INTO sacoche_matiere VALUES (6954, 0, 0, 0,  69, 0, 255, "695400", "BCHMB", "Biochimie microbiologie biotechnologie")' );
      // ordonner
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_matiere ORDER BY matiere_id ' );
      */
    }
    // Ajout / Modification de niveaux ouvrant au 01/09/2017 non encore traités, ou au 01/09/2018, ou après si allant avec une filière sur plusieurs années)
    if(empty($reload_sacoche_niveau_famille))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau_famille VALUES (317, 3,  6, "DMA (Diplôme des Métiers d’Art) en 3 ans") ' );
      // ordonner
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_niveau_famille ORDER BY niveau_famille_id ' );
    }
    if(empty($reload_sacoche_niveau))
    {
      // 240
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 240194, 0, 0, 240, 240,  "1CAP1", "2402543711.", "1CAP1 réalisations industrielles option A chaudronnerie")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 240195, 0, 0, 240, 240,  "1CAP1", "2402543811.", "1CAP1 réalisations industrielles option B soudage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2402344511.", niveau_nom="1CAP1 ébéniste" WHERE niveau_id=240083' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2402432211.", niveau_nom="1CAP1 maroquinerie" WHERE niveau_id=240107' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2403222711.", niveau_nom="1CAP1 signalétique et décors graphiques" WHERE niveau_id=240166' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2403222811.", niveau_nom="1CAP1 arts de la reliure" WHERE niveau_id=240160' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2403320411.", niveau_nom="1CAP1 accompagnant éducatif petite enfance" WHERE niveau_id=240178' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2403341211.", niveau_nom="1CAP1 commercialisation et services en hôtel-café-restaurant" WHERE niveau_id=240179' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2403112311.", niveau_nom="1CAP1 opérateur/opératrice de service-relation client et livraison" WHERE niveau_id=240149' );
      // 241
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241381, 0, 0, 241, 241,  "1CAP2", "2412120321.", "1CAP2 conchyliculture")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241382, 0, 0, 241, 241,  "2CAP2", "2412120322.", "2CAP2 conchyliculture")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241383, 0, 0, 241, 241,  "1CAP2", "2412214021.", "1CAP2 crémier-fromager")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241384, 0, 0, 241, 241,  "2CAP2", "2412214022.", "2CAP2 crémier-fromager")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241385, 0, 0, 241, 241,  "1CAP2", "2412271521.", "1CAP2 monteur en installations thermiques")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241386, 0, 0, 241, 241,  "1CAP2", "2412271522.", "1CAP2 monteur en installations thermiques")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241387, 0, 0, 241, 241,  "2CAP2", "2412332421.", "2CAP2 monteur en installations sanitaires")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241388, 0, 0, 241, 241,  "2CAP2", "2412332422.", "2CAP2 monteur en installations sanitaires")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241389, 0, 0, 241, 241,  "1CAP2", "2412543821.", "1CAP2 réalisations industrielles option B soudage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241390, 0, 0, 241, 241,  "2CAP2", "2412543822.", "2CAP2 réalisations industrielles option B soudage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241391, 0, 0, 241, 241,  "1CAP2", "2412552421.", "1CAP2 électricien")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241392, 0, 0, 241, 241,  "2CAP2", "2412552422.", "2CAP2 électricien")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241393, 0, 0, 241, 241,  "1CAP2", "2413122221.", "1CAP2 primeur")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241394, 0, 0, 241, 241,  "2CAP2", "2413122222.", "2CAP2 primeur")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241395, 0, 0, 241, 241,  "1CAP2", "2413000121.", "1CAP2 1ère année de CAP services")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 241396, 0, 0, 241, 241,  "1CAP2", "2412000621.", "1CAP2 1ère année de CAP production")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412130721.", niveau_nom="1CAP2 matelot" WHERE niveau_id=241011' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412130722.", niveau_nom="2CAP2 matelot" WHERE niveau_id=241012' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412344521.", niveau_nom="1CAP2 ébéniste" WHERE niveau_id=241163' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412344522.", niveau_nom="2CAP2 ébéniste" WHERE niveau_id=241164' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412432221.", niveau_nom="1CAP2 maroquinerie" WHERE niveau_id=241211' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412432222.", niveau_nom="2CAP2 maroquinerie" WHERE niveau_id=241212' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412543721.", niveau_nom="1CAP2 réalisations industrielles option A chaudronnerie" WHERE niveau_id=241273' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2412543722.", niveau_nom="2CAP2 réalisations industrielles option A chaudronnerie" WHERE niveau_id=241274' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413122321.", niveau_nom="1CAP2 fleuriste" WHERE niveau_id=241306' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413122322.", niveau_nom="2CAP2 fleuriste" WHERE niveau_id=241307' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413222721.", niveau_nom="1CAP2 signalétique et décors graphiques" WHERE niveau_id=241326' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413222722.", niveau_nom="2CAP2 signalétique et décors graphiques" WHERE niveau_id=241327' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413222821.", niveau_nom="1CAP2 arts de la reliure" WHERE niveau_id=241314' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413222822.", niveau_nom="2CAP2 arts de la reliure" WHERE niveau_id=241315' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413320421.", niveau_nom="1CAP2 accompagnant éducatif petite enfance" WHERE niveau_id=241350' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413320422.", niveau_nom="2CAP2 accompagnant éducatif petite enfance" WHERE niveau_id=241351' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413341221.", niveau_nom="1CAP2 commercialisation et services en hôtel-café-restaurant" WHERE niveau_id=241352' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413341222.", niveau_nom="2CAP2 commercialisation et services en hôtel-café-restaurant" WHERE niveau_id=241353' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413361521.", niveau_nom="1CAP2 esthétique cosmétique parfumerie" WHERE niveau_id=241366' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413361522.", niveau_nom="2CAP2 esthétique cosmétique parfumerie" WHERE niveau_id=241367' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413112321.", niveau_nom="1CAP2 opérateur/opératrice de service-relation client et livraison" WHERE niveau_id=241292' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2413112322.", niveau_nom="2CAP2 opérateur/opératrice de service-relation client et livraison" WHERE niveau_id=241293' );
      // 247
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 247316, 0, 0, 247, 247, "2NDPRO", "2472270531.", "2ndPro technicien gaz")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 247317, 0, 0, 247, 247, "1ERPRO", "2472270532.", "1erPro technicien gaz")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 247318, 0, 0, 247, 247, "TLEPRO", "2472270533.", "TlePro technicien gaz")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 247319, 0, 0, 247, 247, "TLEPRO", "2473111133.", "Tlepro conduite et gestion des entr. maritimes option plaisance profess.")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 247320, 0, 0, 247, 247, "2NDPRO", "2473000231.", "2ndPro 2nde professionnelle services")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 247321, 0, 0, 247, 247, "2NDPRO", "2472000531.", "2ndPro 2nde professionnelle production")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2472130331.", niveau_nom="2ndPro conduite et gestion des entreprises maritimes option pêche" WHERE niveau_id=247014' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2472130332.", niveau_nom="1erPro conduite et gestion des entreprises maritimes option pêche" WHERE niveau_id=247015' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2472130333.", niveau_nom="TlePro conduite et gestion des entreprises maritimes option pêche" WHERE niveau_id=247016' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2472541131.", niveau_nom="2ndPro tech.chaudronnerie industrielle" WHERE niveau_id=247191' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2472541132.", niveau_nom="1erPro tech.chaudronnerie industrielle" WHERE niveau_id=247192' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2472541133.", niveau_nom="TlePro tech.chaudronnerie industrielle" WHERE niveau_id=247193' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473310331.", niveau_nom="2ndPro technicien en appareillage orthopédique" WHERE niveau_id=247313' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473310332.", niveau_nom="1erPro technicien en appareillage orthopédique" WHERE niveau_id=247314' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473310333.", niveau_nom="TlePro technicien en appareillage orthopédique" WHERE niveau_id=247315' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473111031.", niveau_nom="2ndPro conduite et gestion des entreprises maritimes option commerce" WHERE niveau_id=247209' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473111032.", niveau_nom="1erPro conduite et gestion des entreprises maritimes option commerce" WHERE niveau_id=247210' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473111033.", niveau_nom="TlePro conduite et gestion des entreprises maritimes option commerce" WHERE niveau_id=247211' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473360331.", niveau_nom="2ndPro esthétique cosmétique parfumerie" WHERE niveau_id=247271' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473360332.", niveau_nom="1erPro esthétique cosmétique parfumerie" WHERE niveau_id=247272' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2473360333.", niveau_nom="TlePro esthétique cosmétique parfumerie" WHERE niveau_id=247273' );
      // 253
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 253055, 0, 0, 253, 253,     "MC", "2533350111.", "MC animation-gestion de projets dans le secteur sportif (mc4)")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 253056, 0, 0, 253, 253,     "MC", "2532540811.", "MC technicien(ne) en chaudronnerie aéronautique et spatiale")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 253057, 0, 0, 253, 253,     "MC", "2532540711.", "MC technicien(ne) en tuyauterie")' );
      // 254
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 254112, 0, 0, 254, 254,   "1BP2", "2542551621.", "1BP2 électricien(ne)")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 254113, 0, 0, 254, 254,   "2BP2", "2542551622.", "2BP2 électricien(ne)")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542321521.", niveau_nom="1BP2 métiers de la piscine" WHERE niveau_id=254037' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542321522.", niveau_nom="2BP2 métiers de la piscine" WHERE niveau_id=254038' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542321621.", niveau_nom="1BP2 maçon" WHERE niveau_id=254041' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542321622.", niveau_nom="2BP2 maçon" WHERE niveau_id=254042' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331621.", niveau_nom="1BP2 carreleur mosaïste" WHERE niveau_id=254049' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331622.", niveau_nom="2BP2 carreleur mosaïste" WHERE niveau_id=254050' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331721.", niveau_nom="1BP2 étanchéité du bâtiment et des travaux publics" WHERE niveau_id=254047' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331722.", niveau_nom="2BP2 étanchéité du bâtiment et des travaux publics" WHERE niveau_id=254048' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331421.", niveau_nom="1BP2 métiers du plâtre et de l’isolation" WHERE niveau_id=254051' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331422.", niveau_nom="2BP2 métiers du plâtre et de l’isolation" WHERE niveau_id=254052' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331521.", niveau_nom="1BP2 peintre applicateur de revêtements" WHERE niveau_id=254053' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2542331522.", niveau_nom="2BP2 peintre applicateur de revêtements" WHERE niveau_id=254054' );
      // 276
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2762100432.", niveau_nom="1eProA cgea" WHERE niveau_id=276006' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2762100433.", niveau_nom="TlProA cgea" WHERE niveau_id=276007' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2762111332.", niveau_nom="1eProA cge vitivinicole" WHERE niveau_id=276008' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="2762111333.", niveau_nom="TlProA cge vitivinicole" WHERE niveau_id=276009' );
      // 300
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 300003, 0, 0, 301, 300, "MAR-NI", "3001104511.", "CPGE préparation concours navigant ingénieur")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 300004, 0, 0, 301, 300, "1CPEC1", "3001201811.", "1CPGE1 classe prépa. Économique et commerciale voie professionnelle")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3001104411.", niveau_ref="MAR-OM", niveau_nom="CPGE préparation concours d’officier chef de quart marine-chef" WHERE niveau_id=300002' );
      // 301
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 301049, 0, 0, 301, 301, "1CYRLE", "3011300821.", "CPGE1 école spéciale militaire St-Cyr option lettres")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 301050, 0, 0, 301, 301, "2CYRLE", "3011300822.", "CPGE2 école spéciale militaire St-Cyr option lettres")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3011201721.", niveau_ref="1CYRES", niveau_nom="CPGE1 école spéciale militaire St-Cyr option économique et sociale" WHERE niveau_id=301044' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3011201722.", niveau_ref="2CYRES", niveau_nom="CPGE2 école spéciale militaire St-Cyr option économique et sociale" WHERE niveau_id=301045' );
      // 310
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 310122, 0, 0, 310, 310,  "1BTS1", "3102541411.", "1BTS1 conception des processus de découpe et d’emboutissage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 310123, 0, 0, 310, 310,  "1BTS1", "3103221011.", "1BTS1 études réal. projet communic. option b réal. produits plurimedia")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 310124, 0, 0, 310, 310,  "1BTS1", "3103221111.", "1BTS1 études réal. projet communic. option b réal. produits imprimés")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3102521811.", niveau_nom="1BTS1 maintenance des matériels de construction et de manutention" WHERE niveau_id=310051' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3102541311.", niveau_nom="1BTS1 conception et industrialisation en construction navale" WHERE niveau_id=310057' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3103131111.", niveau_nom="1BTS1 assurance" WHERE niveau_id=310072' );
      // 311
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 311240, 0, 0, 311, 311,  "1BTS2", "3112541421.", "1BTS2 conception des processus de découpe et d’emboutissage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 311241, 0, 0, 311, 311,  "2BTS2", "3112541422.", "2BTS2 conception des processus de découpe et d’emboutissage")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 311242, 0, 0, 311, 311,  "1BTS2", "3113220921.", "1BTS2 études de réal. d’un projet de communication 1ère année commune")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 311243, 0, 0, 311, 311,  "2BTS2", "3113221022.", "2BTS2 études réal. projet communic. option b réal. produits plurimedia")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 311244, 0, 0, 311, 311,  "2BTS2", "3113221122.", "2BTS2 études réal. projet communic. option b réal. produits imprimés")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 311245, 0, 0, 311, 311,  "2BTS2", "3113342522.", "2BTS2 management en hôtellerie-restauration option C hébergement")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112011421.", niveau_nom="1BTS2 métiers de l’eau" WHERE niveau_id=311213' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112011422.", niveau_nom="2BTS2 métiers de l’eau" WHERE niveau_id=311214' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112501021.", niveau_nom="1BTS2 maintenance des systèmes option A systèmes de production" WHERE niveau_id=311098' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112501022.", niveau_nom="2BTS2 maintenance des systèmes option A systèmes de production" WHERE niveau_id=311099' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112501121.", niveau_nom="1BTS2 maintenance des systèmes option B syst. énergétiques et fluidiques" WHERE niveau_id=311100' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112501122.", niveau_nom="2BTS2 maintenance des systèmes option B syst. énergétiques et fluidiques" WHERE niveau_id=311101' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112501221.", niveau_nom="1BTS2 maintenance des systèmes option C systèmes éoliens" WHERE niveau_id=311102' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112501222.", niveau_nom="2BTS2 maintenance des systèmes option C systèmes éoliens" WHERE niveau_id=311103' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112521821.", niveau_nom="1BTS2 maintenance des matériels de construction et de manutention" WHERE niveau_id=311106' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112521822.", niveau_nom="2BTS2 maintenance des matériels de construction et de manutention" WHERE niveau_id=311107' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112541321.", niveau_nom="1BTS2 conception et industrialisation en construction navale" WHERE niveau_id=311118' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112541322.", niveau_nom="2BTS2 conception et industrialisation en construction navale" WHERE niveau_id=311119' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113131121.", niveau_nom="1BTS2 assurance" WHERE niveau_id=311146' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113131122.", niveau_nom="2BTS2 assurance" WHERE niveau_id=311147' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112541521.", niveau_nom="1BTS2 conception et réalisation en chaudronnerie industrielle" WHERE niveau_id=311124' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112541522.", niveau_nom="2BTS2 conception et réalisation en chaudronnerie industrielle" WHERE niveau_id=311125' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112541621.", niveau_nom="1BTS2 architectures en métal : conception et réalisation" WHERE niveau_id=311120' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3112541622.", niveau_nom="2BTS2 architectures en métal : conception et réalisation" WHERE niveau_id=311121' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113121221.", niveau_nom="1BTS2 négociation et digitalisation de la relation client" WHERE niveau_id=311142' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113121222.", niveau_nom="2BTS2 négociation et digitalisation de la relation client" WHERE niveau_id=311143' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113140921.", niveau_nom="1BTS2 gestion de la PME" WHERE niveau_id=311153' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113140922.", niveau_nom="2BTS2 gestion de la PME" WHERE niveau_id=311154' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113240921.", niveau_nom="1BTS2 support à l’action managériale" WHERE niveau_id=311183' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113240922.", niveau_nom="2BTS2 support à l’action managériale" WHERE niveau_id=311184' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3113342221.", niveau_nom="1BTS2 management en hôtellerie-restauration 1ère année commune" WHERE niveau_id=311200' );
      // 317
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 317001, 0, 0, 317, 317,  "1DMA3", "3171340131.", "1DMA3 diplôme national des métiers d’art et du design")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 317002, 0, 0, 317, 317,  "2DMA3", "3171340132.", "2DMA3 diplôme national des métiers d’art et du design")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 317003, 0, 0, 317, 317,  "3DMA3", "3171340133.", "3DMA3 diplôme national des métiers d’art et du design")' );
      // 320
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET code_mef="3203342111.", niveau_nom="Mise à niveau en hôtellerie-restauration" WHERE niveau_id=320003' );
      // 391
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 391001, 0, 0, 390, 391, "1PD3-2", "3912220121.", "1PD3-2 biophysicien de laboratoire")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 391002, 0, 0, 390, 391, "2PD3-2", "3912220122.", "2PD3-2 biophysicien de laboratoire")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 391003, 0, 0, 390, 391, "1PD3-2", "3912230121.", "1PD3-2 physico-métallographe de laboratoire")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 391004, 0, 0, 390, 391, "2PD3-2", "3912230122.", "2PD3-2 physico-métallographe de laboratoire")' );
      // 441
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441004, 0, 0, 390, 441,   "1PD2", "4413320331.", "1ère année DE éducateur de jeunes enfants niveau II, 1ère année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441005, 0, 0, 390, 441,   "2PD2", "4413320332.", "1ère année DE éducateur de jeunes enfants niveau II, 2ème année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441006, 0, 0, 390, 441,   "3PD2", "4413320333.", "1ère année DE éducateur de jeunes enfants niveau II, 3ème année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441007, 0, 0, 390, 441,   "1PD2", "4413320431.", "1ère année DE éducateur technique spécialisé niveau II, 1ère année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441008, 0, 0, 390, 441,   "2PD2", "4413320432.", "1ère année DE éducateur technique spécialisé niveau II, 2ème année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441009, 0, 0, 390, 441,   "3PD2", "4413320433.", "1ère année DE éducateur technique spécialisé niveau II, 3ème année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441010, 0, 0, 390, 441,   "1PD2", "4413320231.", "1ère année DE éducateur spécialisé niveau II, 1ère année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441011, 0, 0, 390, 441,   "2PD2", "4413320232.", "1ère année DE éducateur spécialisé niveau II, 2ème année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441012, 0, 0, 390, 441,   "3PD2", "4413320233.", "1ère année DE éducateur spécialisé niveau II, 3ème année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441013, 0, 0, 390, 441,   "1PD2", "4413320531.", "1ère année DE assistant social niveau II, 1ère année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441014, 0, 0, 390, 441,   "2PD2", "4413320532.", "1ère année DE assistant social niveau II, 2ème année")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 441015, 0, 0, 390, 441,   "3PD2", "4413320533.", "1ère année DE assistant social niveau II, 3ème année")' );
      // 741
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 741002, 0, 0, 740, 741, "CL-PAS", "7412000111.", "Classe passerelle BTS production")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 741003, 0, 0, 740, 741, "CL-PAS", "7413000111.", "Classe passerelle BTS services")' );
      // 753
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_niveau VALUES ( 753002, 0, 0, 740, 753,    "APF", "7534100211.", "Parcours aménagé de la formation initiale")' );
      // ordonner
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_niveau ORDER BY niveau_id ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-09-25 => 2018-09-26
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-09-25')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-09-26';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // L’ajout de matières via la mise à jour de la veille cause des conflits lorsque matiere_ref est déjà pris par une matière spécifique.
    // Il s’avère plus stratégique de 
    // 1) récupérer les matières spécifiques et les matières partagées utilisées
    // 2) recharger toute la table des matières partagées
    // 3)recréer ou convertir les matières spécifiques et réactiver les matières partagées utilisées
    // Go :
    // récupération des informations sur les matières
    $DB_TAB_communes    = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE (matiere_active=1 OR matiere_siecle=1) AND matiere_id<='.ID_MATIERE_PARTAGEE_MAX);
    $DB_TAB_specifiques = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE matiere_id>'.ID_MATIERE_PARTAGEE_MAX);
    if(empty($reload_sacoche_matiere))
    {
      // rechargement de la table sacoche_matiere
      $reload_sacoche_matiere = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_matiere.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // on remet en place les matières partagées
    foreach($DB_TAB_communes as $DB_ROW)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active='.$DB_ROW['matiere_active'].', matiere_siecle='.$DB_ROW['matiere_siecle'].', matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$DB_ROW['matiere_id'] );
    }
    // on remet en place les matières spécifiques
    foreach($DB_TAB_specifiques as $DB_ROW)
    {
      $matiere_id_partage = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT matiere_id FROM sacoche_matiere WHERE matiere_ref="'.$DB_ROW['matiere_ref'].'" LIMIT 1');
      if(!$matiere_id_partage)
      {
        // Pas de conflit : on remet la matière spécifique telle qu’elle était
        $DB_SQL = 'INSERT INTO sacoche_matiere(matiere_id, matiere_active, matiere_siecle, matiere_usuelle, matiere_famille_id, matiere_nb_demandes, matiere_ordre, matiere_code, matiere_ref, matiere_nom) '
                . 'VALUES(:matiere_id, :matiere_active, :matiere_siecle, 0, 0, :matiere_nb_demandes, :matiere_ordre, 0, :matiere_ref, :matiere_nom) ';
        $DB_VAR = array(
          ':matiere_id'          => $DB_ROW['matiere_id'],
          ':matiere_active'      => $DB_ROW['matiere_active'],
          ':matiere_siecle'      => $DB_ROW['matiere_siecle'],
          ':matiere_nb_demandes' => $DB_ROW['matiere_nb_demandes'],
          ':matiere_ordre'       => $DB_ROW['matiere_ordre'],
          ':matiere_ref'         => $DB_ROW['matiere_ref'],
          ':matiere_nom'         => $DB_ROW['matiere_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
      else
      {
        // Conflit : on la convertit en matière partagée
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active=1, matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$matiere_id_partage );
        DB_STRUCTURE_MATIERE::DB_deplacer_referentiel_matiere( $DB_ROW['matiere_id'] , $matiere_id_partage );
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-09-26 => 2018-10-02
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-09-26')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-10-02';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Lors de la précédente maj de la base, pour les matières, on a oublié de conserver matiere_siecle (code revu depuis)
    $tab_SIECLE = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('sts_emp_UAI');
    if(!empty($tab_SIECLE))
    {
      if(!empty($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE']))
      {
        $DB_SQL = 'UPDATE sacoche_matiere SET matiere_siecle=1 WHERE matiere_ref=:matiere_ref ';
        foreach($tab_SIECLE['NOMENCLATURES']['MATIERES']['MATIERE'] as $tab)
        {
          $DB_VAR = array( ':matiere_ref' => $tab['CODE_GESTION'] );
          DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
        }
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-10-02 => 2018-10-08
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-10-02')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-10-08';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"appreciation_generale_longueur\\"" , "\\"afficher_logo_en\\":1,\\"appreciation_generale_longueur\\"" ) WHERE officiel_type="livret" ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"APPRECIATION_GENERALE_LONGUEUR\\"" , "\\"AFFICHER_LOGO_EN\\":1,\\"APPRECIATION_GENERALE_LONGUEUR\\"" ) WHERE archive_ref="livret" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-10-08 => 2018-11-20
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-10-08')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-11-20';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de lignes à [sacoche_livret_enscompl]
    if(empty($reload_sacoche_livret_enscompl))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_livret_enscompl VALUES (6, "LCE", "Langues et cultures européennes")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_livret_enscompl VALUES (7, "CHK", "Chant choral")' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2018-11-20 => 2018-11-30
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2018-11-20')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2018-11-30';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // J’ai encore trouvée une base où la table [sacoche_notification] n’était pas créée...
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SHOW TABLES FROM '.SACOCHE_STRUCTURE_BD_NAME.' LIKE "sacoche_notification"');
    if(empty($DB_TAB))
    {
      $reload_sacoche_notification = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_notification.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
  }
}

?>
