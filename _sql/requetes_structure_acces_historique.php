<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent qu’une base STRUCTURE.
// Ces méthodes ne concernent que la table "sacoche_acces_historique".

class DB_STRUCTURE_ACCES_HISTORIQUE
{

/**
 * Ajout une entrée à l’historique des accès, et retire au passage les vieux accès
 *
 * @param int    $user_id
 * @param string $acces_mode
 * @param string $acces_info
 * @return void
 */
public static function DB_ajouter( $user_id , $acces_mode , $acces_info )
{
  $DB_SQL = 'INSERT INTO sacoche_acces_historique( user_id, acces_date, acces_mode, acces_info) '
          . 'VALUES                              (:user_id, NOW()     ,:acces_mode,:acces_info) '
          . 'ON DUPLICATE KEY UPDATE acces_mode=:acces_mode, acces_info=:acces_info ';
  $DB_VAR = array(
    ':user_id'    => $user_id,
    ':acces_mode' => $acces_mode,
    ':acces_info' => $acces_info,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  DB_STRUCTURE_ACCES_HISTORIQUE::DB_supprimer_ancien();
}

/**
 * Supprimer les entrées de l’historique des accès de plus d’un an
 *
 * @param void
 * @return void
 */
public static function DB_supprimer_ancien()
{
  $DB_SQL = 'DELETE FROM sacoche_acces_historique '
          . 'WHERE acces_date < DATE_SUB(NOW(),INTERVAL 1 YEAR)';
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , NULL);
}

/**
 * lister_commentaires_eleves_dates
 *
 * @param int    $prof_id
 * @param string $liste_eleve_id   id des élèves séparés par des virgules
 * @param string $date_sql_debut
 * @param string $date_sql_fin
 * @return array
 */
public static function DB_lister_for_user( $user_id )
{
  $DB_SQL = 'SELECT acces_date, acces_mode, acces_info '
          . 'FROM sacoche_acces_historique '
          . 'WHERE user_id=:user_id '
          . 'ORDER BY acces_date DESC ';
  $DB_VAR = array( ':user_id' => $user_id );
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}


}
?>