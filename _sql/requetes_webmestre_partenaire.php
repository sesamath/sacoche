<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent que la base WEBMESTRE (donc une installation multi-structures).
// Ces méthodes ne concernent que les partenaires ENT conventionnés.

class DB_WEBMESTRE_PARTENAIRE
{

/**
 * modifier_mdp_utilisateur
 *
 * @param int    $partenaire_id
 * @param string $password_ancien   NULL si mdp inchangé (on veut seulement améliorer le hashage)
 * @param string $password_nouveau
 * @return bool   TRUE si ok | FALSE si le mot de passe actuel est incorrect.
 */
public static function DB_modifier_mdp_partenaire( $partenaire_id , $password_ancien , $password_nouveau )
{
  if(!is_null($password_ancien))
  {
    // Tester si l’ancien mot de passe correspond à celui enregistré
    $DB_SQL = 'SELECT partenaire_password '
            . 'FROM sacoche_partenaire '
            . 'WHERE partenaire_id=:partenaire_id ';
    $DB_VAR = array(
      ':partenaire_id' => $partenaire_id,
    );
    $password_hash = DB::queryOne(SACOCHE_WEBMESTRE_BD_NAME , $DB_SQL , $DB_VAR);
    if( !Outil::verifier_mdp( $password_ancien , $password_hash ) )
    {
      return FALSE;
    }
  }
  // Remplacer par le nouveau mot de passe
  $DB_SQL = 'UPDATE sacoche_partenaire '
          . 'SET partenaire_password=:password_crypte '
          . 'WHERE partenaire_id=:partenaire_id ';
  $DB_VAR = array(
    ':partenaire_id'   => $partenaire_id,
    ':password_crypte' => Outil::crypter_mdp($password_nouveau),
  );
  DB::query(SACOCHE_WEBMESTRE_BD_NAME , $DB_SQL , $DB_VAR);
  return TRUE;
}

}
?>