<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-11-07 => 2024-02-12
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-11-07')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2024-02-12';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Ajout de 2 colonnes à la table [sacoche_jointure_plan_eleve]
    if(empty($reload_sacoche_jointure_plan_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_plan_eleve ADD jointure_equipe CHAR(1) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Permet par la suite d’assigner une note à tous les membres de l’équipe." AFTER jointure_ordre' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_plan_eleve ADD jointure_role VARCHAR(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Rôle éventuel dans l’équipe (info pour le prof uniquement)." AFTER jointure_equipe' );
    }
    // Ajout du champ [devoir_equipe] à la table [sacoche_devoir]
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir ADD devoir_equipe TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER devoir_eleves_ordre ' );
    // Correction d’un souci d’accès à des archives du livret scolaire à cause d’un numéro de version SACoche enregistré au 2023-01-31 au lieu de 2024-01-31.
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET sacoche_version="2024-01-31" WHERE sacoche_version="2023-01-31" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2024-02-12 => 2024-02-28
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2024-02-12')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2024-02-28';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de paramètres
    $droit = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_officiel_livret_positionner_socle"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("chorus_envoi_automatique" , "1")' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("chorus_code_service" , "")' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("chorus_numero_engagement" , "")' );
    // réordonner la table sacoche_parametre (ligne à déplacer vers la dernière MAJ lors d’ajout dans sacoche_parametre)
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_parametre ORDER BY parametre_nom' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2024-02-28 => 2024-03-09
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2024-02-28')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2024-03-09';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    //
    // Relance d’un ajout similaire aux maj du 2019-03-22 et 2023-08-22 car quelques vielles archives d’un établissement (2017-2018) ont été trouvées sans les infos de $_SESSION['OFFICIEL']
    //
    $tab_archive_id = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , 'SELECT officiel_archive_id FROM sacoche_officiel_archive WHERE archive_type="livret" AND archive_contenu NOT LIKE "%LIVRET_AFFICHER_LOGO_EN%" ' );
    if(!empty($tab_archive_id))
    {
      $tab_configuration = DB_STRUCTURE_OFFICIEL_CONFIG::DB_recuperer_configuration( 'livret' , 'defaut' );
      $tab_officiel = array('OFFICIEL'=>array());
      foreach($tab_configuration as $key => $val)
      {
        $tab_officiel['OFFICIEL']['LIVRET_'.Clean::upper($key)] = $val;
      }
      $string_officiel = str_replace( '"' , '\\"' , json_encode($tab_officiel) );
      $listing_id = implode(',',$tab_archive_id);
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "[]]],[\\"initialiser\\"" , "'.$string_officiel.']],[\\"initialiser\\"" ) WHERE officiel_archive_id IN('.$listing_id.') ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2024-03-09 => 2024-03-15
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2024-03-09')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2024-03-15';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Modif nom colonne de la table [sacoche_jointure_user_module]
    if(empty($reload_sacoche_jointure_user_module))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_user_module CHANGE module_url module_value VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "URL ou jeton suivant les cas" ' );
    }
    // Modif taille colonne de la table [sacoche_acces_historique]
    if(empty($reload_sacoche_acces_historique))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_acces_historique CHANGE acces_info acces_info VARCHAR(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2024-03-15 => 2024-04-01
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2024-03-15')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2024-04-01';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Ajout colonne tables [sacoche_livret_ap] [sacoche_livret_epi] [sacoche_livret_parcours]
    if(empty($reload_sacoche_livret_ap))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_ap ADD livret_ap_report_auto TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER livret_ap_titre' );
    }
    if(empty($reload_sacoche_livret_epi))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_epi ADD livret_epi_report_auto TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER livret_epi_titre' );
    }
    if(empty($reload_sacoche_livret_parcours))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_livret_parcours ADD livret_parcours_report_auto TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER groupe_id' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2024-04-01 => 2024-08-12
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2024-04-01')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2024-08-12';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // recharger [sacoche_livret_rubrique] (LSU 1D : ajout de cinq sous-domaines pour la discipline "Sciences et technologie" en cycle 3 sur les niveaux CM1 et CM2)
    if(empty($reload_sacoche_livret_rubrique))
    {
      $reload_sacoche_livret_rubrique = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_livret_rubrique.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // LSU 2D : positionnements des bilans périodiques des élèves de 3e maintenant obligatoirement en note sur 20 ou pourcentage
    $type_positionnement = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT livret_page_colonne FROM sacoche_livret_page WHERE livret_page_ref="3e"' );
    if(in_array($type_positionnement,array('objectif','position')))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_livret_page SET livret_page_colonne="moyenne" WHERE livret_page_ref="3e"' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// NE PAS OUBLIER de modifier aussi le nécessaire dans ./_sql/structure/ en fonction des évolutions !!!
// ////////////////////////////////////////////////////////////////////////////////////////////////////

// ///////////////////////////////////////////////////////////////////////////////////////////////////////
// NE PAS OUBLIER de maj les 2 tables de la base sacoche_projet en cas modif de matières ou de niveaux !!!
// ///////////////////////////////////////////////////////////////////////////////////////////////////////

?>
