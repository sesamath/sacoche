<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2020-12-12 => 2021-01-16
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2020-12-12')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-01-16';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Augmentation de la taille du champ pour le nom d’une classe (souhait d’un établissement...)
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_groupe CHANGE groupe_nom groupe_nom VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-01-16 => 2021-03-15
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-01-16')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-03-15';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // retrait de 3 paramètres
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DELETE FROM sacoche_parametre WHERE parametre_nom IN ( "gepi_certificat_empreinte","gepi_rne","gepi_url" )' );
    // L’ajout de matières cause des conflits lorsque matiere_ref est déjà pris par une matière spécifique.
    // Il s’avère plus stratégique de 
    // 1) récupérer les matières spécifiques et les matières partagées utilisées
    // 2) recharger toute la table des matières partagées
    // 3)recréer ou convertir les matières spécifiques et réactiver les matières partagées utilisées
    // PS. On laisse tomber toutes les matières en doublon "option", "hors établissement", "par correspondance"...
    // Go :
    // récupération des informations sur les matières
    $DB_TAB_communes    = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE (matiere_active=1 OR matiere_siecle=1) AND matiere_id<='.ID_MATIERE_PARTAGEE_MAX);
    $DB_TAB_specifiques = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE matiere_id>'.ID_MATIERE_PARTAGEE_MAX);
    if(empty($reload_sacoche_matiere))
    {
      // rechargement de la table sacoche_matiere
      $reload_sacoche_matiere = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_matiere.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // on remet en place les matières partagées
    foreach($DB_TAB_communes as $DB_ROW)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active='.$DB_ROW['matiere_active'].', matiere_siecle='.$DB_ROW['matiere_siecle'].', matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$DB_ROW['matiere_id'] );
    }
    // on remet en place les matières spécifiques
    foreach($DB_TAB_specifiques as $DB_ROW)
    {
      $matiere_id_partage = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT matiere_id FROM sacoche_matiere WHERE matiere_ref="'.$DB_ROW['matiere_ref'].'" LIMIT 1');
      if(!$matiere_id_partage)
      {
        // Pas de conflit : on remet la matière spécifique telle qu’elle était
        $DB_SQL = 'INSERT INTO sacoche_matiere( matiere_id, matiere_active, matiere_siecle, matiere_usuelle, matiere_famille_id, matiere_nb_demandes, matiere_ordre, matiere_code, matiere_ref, matiere_nom) '
                . 'VALUES                     (:matiere_id,:matiere_active,:matiere_siecle,:matiere_usuelle,:matiere_famille_id,:matiere_nb_demandes,:matiere_ordre,:matiere_code,:matiere_ref,:matiere_nom) ';
        $DB_VAR = array(
          ':matiere_id'          => $DB_ROW['matiere_id'],
          ':matiere_active'      => $DB_ROW['matiere_active'],
          ':matiere_siecle'      => $DB_ROW['matiere_siecle'],
          ':matiere_usuelle'     => 0,
          ':matiere_famille_id'  => 0,
          ':matiere_nb_demandes' => $DB_ROW['matiere_nb_demandes'],
          ':matiere_ordre'       => $DB_ROW['matiere_ordre'],
          ':matiere_code'        => $DB_ROW['matiere_code'],
          ':matiere_ref'         => $DB_ROW['matiere_ref'],
          ':matiere_nom'         => $DB_ROW['matiere_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
      else
      {
        // Conflit : on la convertit en matière partagée
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active=1, matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$matiere_id_partage );
        DB_STRUCTURE_MATIERE::DB_deplacer_referentiel_matiere( $DB_ROW['matiere_id'] , $matiere_id_partage );
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-03-15 => 2021-04-02
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-03-15')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-04-02';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Augmentation de la taille du champ pour mémoriser les infos d’une saisie (prénom 50 + nom 50 + info devoir 60 + 3 caractères).
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_saisie CHANGE saisie_info saisie_info VARCHAR(180) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Enregistrement statique du nom du devoir et du professeur, conservé les années suivantes." ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-04-02 => 2021-08-16
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-04-02')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-08-16';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Ajout colonne et index à [sacoche_matiere]
    if(empty($reload_sacoche_matiere))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_matiere ADD matiere_experimentale TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER matiere_active ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_matiere ADD INDEX matiere_experimentale (matiere_experimentale) ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-08-16 => 2021-08-18
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-08-16')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-08-18';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification sacoche_parametre (connecteur CAS pour ENT)
    $connexion_mode        = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_mode"' );
    $connexion_departement = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_departement"' );
    if( ($connexion_mode=='cas') && in_array( (int)$connexion_departement , array(24,33,40,47,64) ) )
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="lyceeconnecte_bordeaux" WHERE parametre_nom="connexion_nom" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="shibboleth" WHERE parametre_nom="connexion_mode" ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-08-18 => 2021-10-04
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-08-18')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-10-04';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Modif colonne de [sacoche_notification]
    if(empty($reload_sacoche_notification))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_notification CHANGE notification_contenu notification_contenu MEDIUMTEXT COLLATE utf8_unicode_ci NOT NULL COMMENT "Le type TEXT peut ne pas suffire si multiples modifications cumulées de référentiel." ' );
    }
    // Modif colonne de [sacoche_user]
    if(empty($reload_sacoche_user))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user CHANGE user_sconet_elenoet user_sconet_elenoet MEDIUMINT UNSIGNED NOT NULL DEFAULT 0 COMMENT "ELENOET pour un élève (entre 2000 et 5000 ; parfois appelé n° GEP avec un 0 devant). Ce champ sert aussi pour un import Factos (élèves et parents)." ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-10-04 => 2021-11-12
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-10-04')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-11-12';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Modif colonne de [sacoche_devoir]
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir CHANGE devoir_autoeval_date devoir_autoeval_date DATETIME NULL DEFAULT NULL' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_devoir SET devoir_autoeval_date = REPLACE(devoir_autoeval_date,"00:00:00","23:59:00")' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-11-12 => 2021-11-27
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-11-12')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-11-27';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification valeur paramètre dans sacoche_officiel_configuration
    $DB_TAB_configs = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT officiel_type, configuration_ref, configuration_contenu FROM sacoche_officiel_configuration ' , NULL);
    foreach($DB_TAB_configs as $DB_ROW)
    {
      if(!empty($DB_ROW['configuration_contenu']))
      {
        $tab_config = json_decode($DB_ROW['configuration_contenu'], TRUE);
        $tab_config['delai_consultation_famille'] = $tab_config['delai_consultation_famille']*7;
        $DB_SQL = 'UPDATE sacoche_officiel_configuration SET configuration_contenu=:configuration_contenu '
                . 'WHERE officiel_type=:officiel_type AND configuration_ref=:configuration_ref ';
        $DB_VAR = array(
          ':officiel_type'         => $DB_ROW['officiel_type'],
          ':configuration_ref'     => $DB_ROW['configuration_ref'],
          ':configuration_contenu' => json_encode($tab_config),
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-11-27 => 2021-12-03
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-11-27')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2021-12-03';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de colonne à sacoche_jointure_devoir_eleve
    if(empty($reload_sacoche_jointure_devoir_eleve))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_jointure_devoir_eleve ADD jointure_doc_copie TEXT COLLATE utf8_unicode_ci DEFAULT NULL COMMENT "URL de la copie de l’élève" AFTER jointure_doc_corrige ' );
    }
  }
}

?>
