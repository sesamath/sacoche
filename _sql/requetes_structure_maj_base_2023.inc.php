<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-12-16 => 2023-01-03
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-12-16')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-01-03';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Depuis la maj précédente la requête de création de la table sacoche_referentiel n’était pas bonne.
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SHOW TABLES FROM '.SACOCHE_STRUCTURE_BD_NAME.' LIKE "sacoche_referentiel"');
    if(empty($DB_TAB))
    {
      $reload_sacoche_referentiel = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_referentiel.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-01-03 => 2023-02-20
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-01-03')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-02-20';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Test de paramètres probablement initialisés avec une valeur inadéquate
    $droit_gerer_livret_parcours       = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_gerer_livret_parcours"');
    $droit_gerer_livret_enscompl       = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_gerer_livret_enscompl"');
    $droit_gerer_livret_langcultregion = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_gerer_livret_langcultregion"');
    if( $droit_gerer_livret_parcours && !$droit_gerer_livret_enscompl && !$droit_gerer_livret_langcultregion )
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$droit_gerer_livret_parcours.'" WHERE parametre_nom="droit_gerer_livret_enscompl"' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$droit_gerer_livret_parcours.'" WHERE parametre_nom="droit_gerer_livret_langcultregion"' );
    }
    // Test de paramètres ajoutés en mai 2022 aux installations existantes mais oublié pour les nouvelles installations ultérieures
    $droit_positionner_enscompl = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_officiel_livret_positionner_enscompl"');
    if(is_null($droit_positionner_enscompl))
    {
      $droit_positionner_socle = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_officiel_livret_positionner_socle"' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("droit_officiel_livret_positionner_enscompl" , "'.$droit_positionner_socle.'")' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("droit_officiel_livret_positionner_langcultregion" , "'.$droit_positionner_socle.'")' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-02-20 => 2023-03-03
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-02-20')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-03-03';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    if(empty($reload_sacoche_user))
    {
      // ajout de la colonne user_entree_date à la table [sacoche_user]
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user ADD user_entree_date DATE NOT NULL DEFAULT "1000-01-01" COMMENT "Une valeur NULL par défaut compliquerait les requêtes (il faudrait tester NULL || < date )." AFTER user_connexion_date' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user ADD INDEX user_entree_date ( user_entree_date ) ' );
      // et initialisation de valeurs SIECLE si un import est disponible
      $tab_SIECLE = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('Eleves');
      if(!empty($tab_SIECLE))
      {
        if(!empty($tab_SIECLE['DONNEES']['ELEVES']['ELEVE']))
        {
          foreach($tab_SIECLE['DONNEES']['ELEVES']['ELEVE'] as $tab)
          {
            if( isset($tab['DATE_ENTREE']) )
            {
              $sconet_id = $tab['@attributes']['ELEVE_ID'];
              $date_sql_entree = To::date_french_to_sql($tab['DATE_ENTREE']);
              $DB_VAR = array(
                ':sconet_id'   => $sconet_id,
                ':entree_date' => $date_sql_entree,
              );
              DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_entree_date=:entree_date WHERE user_sconet_id=:sconet_id' , $DB_VAR );
            }
          }
        }
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-03-03 => 2023-03-26b Auparavant 2023-03-26, puis passé à 2023-03-26b vu que c'est le même code
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-03-03')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-03-26b';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Depuis la maj 2022-09-29 un nom de niveau était trop long ; sur le serveur de Bordeaux la table n’était carrément pas créée... je suppose à cause de celà...
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SHOW TABLES FROM '.SACOCHE_STRUCTURE_BD_NAME.' LIKE "sacoche_niveau"');
    if(empty($DB_TAB))
    {
      $reload_sacoche_niveau = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-03-26 => 2023-03-26b
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-03-26')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-03-26b';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Finalement si sur le serveur de Bordeaux la table n’était pas créée c’est parce que l’insert multiple était trop long...
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SHOW TABLES FROM '.SACOCHE_STRUCTURE_BD_NAME.' LIKE "sacoche_niveau"');
    if(empty($DB_TAB))
    {
      $reload_sacoche_niveau = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-03-26b => 2023-05-05
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-03-26b')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-05-05';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout champ sacoche_devoir
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir ADD devoir_saisie_visible_date DATE DEFAULT NULL COMMENT "NULL si visible sans délai" AFTER devoir_visible_date ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_devoir SET devoir_saisie_visible_date = NULL WHERE devoir_visible_date IS NULL ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_devoir SET devoir_saisie_visible_date = devoir_visible_date WHERE devoir_visible_date IS NOT NULL ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-05-05 => 2023-06-06
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-05-05')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-06-06';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif champ sacoche_catalogue_appreciation.appreciation_ordre
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_catalogue_appreciation CHANGE appreciation_ordre appreciation_ordre SMALLINT UNSIGNED NOT NULL DEFAULT 0 ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-06-06 => 2023-06-19
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-06-06')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-06-19';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif champs de sacoche_officiel_assiduite
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_officiel_assiduite
      CHANGE assiduite_absence assiduite_absence SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT "nombre total d’absences",
      CHANGE assiduite_absence_nj assiduite_absence_nj SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT "nombre d’absences non justifiées",
      CHANGE assiduite_retard assiduite_retard SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT "nombre total de retards",
      CHANGE assiduite_retard_nj assiduite_retard_nj SMALLINT UNSIGNED NULL DEFAULT NULL COMMENT "nombre de retards non justifiés"
    ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-06-19 => 2023-08-22
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-06-19')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-08-22';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif champ sacoche_referentiel_item.item_lien
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_referentiel_item CHANGE item_lien item_lien VARCHAR(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" ' );
    // modification sacoche_parametre (connecteur CAS pour ENT)
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='itop_oze_enc92')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="auth/realms/oze_hds/protocol/cas" WHERE parametre_nom="cas_serveur_root" ' );
    }
    // On recommence un élément de maj du 2019-03-22 car il reste des scories dans certaines bases à cause du group_concat_max_len qui est limitant
    //
    // Pour le livret scolaire, $_SESSION['OFFICIEL'] n’était pas mémorisé dans sacoche_officiel_archive, ce qui a commencé à poser problème avec l’usage de LIVRET_AFFICHER_LOGO_EN
    // Du coup on l’ajoute rétroactivement aux archives le mieux possible
    $tab_archive_id = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , 'SELECT officiel_archive_id FROM sacoche_officiel_archive WHERE archive_type="livret" AND archive_contenu NOT LIKE "%LIVRET_AFFICHER_LOGO_EN%" ' );
    if(!empty($tab_archive_id))
    {
      $tab_configuration = DB_STRUCTURE_OFFICIEL_CONFIG::DB_recuperer_configuration( 'livret' , 'defaut' );
      $tab_officiel = array('OFFICIEL'=>array());
      foreach($tab_configuration as $key => $val)
      {
        $tab_officiel['OFFICIEL']['LIVRET_'.Clean::upper($key)] = $val;
      }
      $string_officiel = substr( str_replace( '"' , '\\"' , json_encode($tab_officiel) ) , 1 , -1);
      $listing_id = implode(',',$tab_archive_id);
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"LIVRET\\":" , "'.$string_officiel.',\\"LIVRET\\":" ) WHERE officiel_archive_id IN('.$listing_id.') ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-08-22 => 2023-10-23
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-08-22')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-10-23';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Suite à la fusion administrative des académies de Caen et Rouen, les SI de ces deux anciennes académies sont progressivement fusionnés.
    // Les clés techniques auto-incrémentées, utilisées comme clés primaires (contrainte d’unicité) et comme clés étrangères sont modifiées afin d’éviter les conflits lors de la fusion des données.
    // Ex-académie de Caen : ID = ID X 100 + 5 (départements 014 ; 050 ; 061 et 975 pour Saint-Pierre-et-Miquelon hébergé dans le SI de Caen)
    // Ex-académie de Rouen : ID = ID X 100 + 21 (départements 027 ; 076)
    // Enfin, attention, il ne faut pas toucher aux identifiants STS contrairement à une précédente communication à ce sujet...
    $departement = substr( DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="webmestre_uai"' ) , 0 , 3);
    $is_Caen  = in_array( $departement , array('014','050','061','975') ) ? TRUE : FALSE ;
    $is_Rouen = in_array( $departement , array('027','076') ) ? TRUE : FALSE ;
    if($is_Caen || $is_Rouen)
    {
      $code_academie = ($is_Caen) ? 5 : 21 ;
      // Modifs en base de données : Id SIECLE ou ONDE des utilisateurs et Id de la classe pour le 1D (plus compliqué car champ texte concaténé en BDD SACoche)
      // Modif aussi des fichiers SIECLE importés et archivés, car réutilisés pour l’export LSU...
      // - utilisateurs en BDD (1D & 2D)
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_sconet_id = user_sconet_id * 100 + '.$code_academie.' WHERE user_sconet_id != 0 AND user_profil_sigle IN ("ELV","TUT") ' );
      $tab_SIECLE_Onde = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('Onde');
      if(!empty($tab_SIECLE_Onde))
      {
        // - classes en BDD (1D)
        $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT groupe_id , groupe_ref FROM sacoche_groupe WHERE groupe_type = "classe" ');
        foreach($DB_TAB as $DB_ROW)
        {
          list( $classe_value , $classe_niveau ) = explode('_',$DB_ROW['groupe_ref'],2) + array_fill(0,2,NULL); // Evite des NOTICE en initialisant les valeurs manquantes
          if( is_numeric($classe_value) && $classe_niveau )
          {
            $groupe_ref_new = ($classe_value * 100 + $code_academie) . '_' . $classe_niveau ;
            DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_groupe SET groupe_ref = "'.$groupe_ref_new.'" WHERE groupe_id = '.$DB_ROW['groupe_id'] );
          }
        }
        // - classes dans l’archive SIECLE (1D)
        $numero_colonne_classe_id  = -1;
        $tab_elements = $tab_SIECLE_Onde[0];
        if(is_array($tab_elements))
        {
          foreach ($tab_elements as $numero => $element)
          {
            $numero_colonne_classe_id  = ($element=='Identifiant classe') ? $numero : $numero_colonne_classe_id ;
          }
          if($numero_colonne_classe_id!=-1)
          {
            foreach ($tab_SIECLE_Onde as $key => $tab_elements)
            {
              if( $key && (count($tab_elements)>$numero_colonne_classe_id) )
              {
                $onde_classe_id = $tab_elements[$numero_colonne_classe_id];
                $tab_SIECLE_Onde[$key][$numero_colonne_classe_id] = $onde_classe_id * 100 + $code_academie;
              }
            }
            DB_STRUCTURE_SIECLE::DB_ajouter_import( 'Onde' , NULL , $tab_SIECLE_Onde );
          }
        }
      }
      // - élèves dans l’archive SIECLE (2D)
      //   dans ce fichier il y a aussi les id des élèves dans les associations aux divisions et aux options mais on laisse tomber, ce n’est pas réutilisé
      $tab_SIECLE_Eleves = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('Eleves');
      if(!empty($tab_SIECLE_Eleves))
      {
        if(!empty($tab_SIECLE_Eleves['DONNEES']['ELEVES']['ELEVE']))
        {
          // S’il n’y a qu’une seule entrée ce n’est pas la même architecture
          if(!isset($tab_SIECLE_Eleves['DONNEES']['ELEVES']['ELEVE'][0]))
          {
            $tab_SIECLE_Eleves['DONNEES']['ELEVES']['ELEVE'] = array( 0 => $tab_SIECLE_Eleves['DONNEES']['ELEVES']['ELEVE'] );
          }
          foreach($tab_SIECLE_Eleves['DONNEES']['ELEVES']['ELEVE'] as $key => $tab)
          {
            $id_sts = $tab['@attributes']['ELEVE_ID'];
            $tab_SIECLE_Eleves['DONNEES']['ELEVES']['ELEVE'][$key]['@attributes']['ELEVE_ID'] = $id_sts * 100 + $code_academie;
          }
        }
        DB_STRUCTURE_SIECLE::DB_ajouter_import( 'Eleves' , NULL , $tab_SIECLE_Eleves );
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2023-10-23 => 2023-11-07
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2023-10-23')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2023-11-07';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification sacoche_parametre (nom connecteur CAS pour ENT)
    // Pas besoin de modif sur la table sacoche_webmestre.sacoche_convention ni dans le fichier sacoche/sesamath/_inc/classes/EntConventions.php car académie de Versailles
    $connexion_nom = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="connexion_nom"' );
    if($connexion_nom=='itop_oze_enc92')
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="docaposte_oze_enc92" WHERE parametre_nom="connexion_nom" ' );
    }
  }
}

?>
