<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent qu’une base STRUCTURE.
// Ces méthodes ne concernent que les utilisateurs non identifiés (sauf pour DB_version_base() lors de la MAJ d’une base après restauration).

class DB_STRUCTURE_PUBLIC
{

/**
 * Récuperer, à partir d’un identifiant, les données d’un utilisateur tentant de se connecter (le mdp est comparé ensuite)
 *
 * @param string $mode_connection   'normal' | 'normal_test' | 'cas:user' | 'cas:sconet' | 'cas:attributes' | 'shibboleth' | 'siecle' | 'vecteur_parent' | 'switch' | 'api'
 * @param string $user_identifiant
 * @param string $nom              facultatif, seulement pour $mode_connection = 'cas:attributes' | 'vecteur_parent'
 * @param string $prenom           facultatif, seulement pour $mode_connection = 'cas:attributes' | 'vecteur_parent'
 * @param string $user_profil_type facultatif, seulement pour $mode_connection = 'cas:attributes'
 * @param string $birthdate        facultatif, seulement pour $mode_connection = 'cas:attributes'
 * @return array
 */
public static function DB_recuperer_donnees_utilisateur( $mode_connection , $user_identifiant , $nom='' , $prenom='' , $user_profil_type='' , $birthdate='' )
{
  switch($mode_connection)
  {
    case 'normal'         : $champ = 'user_login';     break;
    case 'normal_test'    : $champ = 'user_login';     break;
    case 'cas:user'       : $champ = 'user_id_ent';    break;
    case 'cas:attributes' : $champ = 'user_id';        break; // Le user_id de l’utilisateur trouvé après une première requête
    case 'api'            : $champ = 'module_value';   break;
    case 'shibboleth'     : $champ = 'user_id_ent';    break;
    case 'cas:sconet'     : $champ = 'user_sconet_id'; break;
    case 'siecle'         : $champ = 'user_sconet_id'; break;
    case 'vecteur_parent' : $champ = 'user_id';        break; // C’est le user_sconet_id de l’élève qui est transmis, mais le user_id du parent trouvé qui est finalement utilisé dans la requête.
    case 'switch'         : $champ = 'user_id';        break;
  }
  // On cherche l’utilisateur à partir d’un nom, d’un prénom, et éventuellement d’un profil et/ou d’une date de naissance
  if($mode_connection=='cas:attributes')
  {
    $DB_VAR = array(
      ':user_nom'         => $nom,
      ':user_prenom'      => $prenom,
      ':user_profil_type' => $user_profil_type,
      ':user_birthdate'   => To::date_french_to_sql($birthdate),
    );
    // on commence juste avec le nom et le prénom
    $DB_SQL = 'SELECT user_id '
    . 'FROM sacoche_user '
    . 'WHERE user_nom = :user_nom AND user_prenom = :user_prenom ';
    $DB_COL = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
    if(empty($DB_COL)) return NULL;
    // s'il y a plusieurs lignes trouvées on restreint avec les autres paramètres si disponibles
    if(count($DB_COL)>1)
    {
      if($user_profil_type)
      {
        $DB_SQL = 'SELECT user_id '
        . 'FROM sacoche_user '
        . 'LEFT JOIN sacoche_user_profil USING (user_profil_sigle) '
        . 'WHERE user_nom = :user_nom AND user_prenom = :user_prenom AND user_profil_type = :user_profil_type ';
        $DB_COL = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
      if( empty($DB_COL) || (count($DB_COL)>1) )
      {
        // que pour les élèves pour l'instant dans la base SACoche
        if($birthdate)
        {
          $DB_SQL = 'SELECT user_id '
          . 'FROM sacoche_user '
          . 'WHERE user_nom = :user_nom AND user_prenom = :user_prenom AND user_naissance_date = :user_birthdate ';
          $DB_COL = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
        }
      }
      if( empty($DB_COL) || (count($DB_COL)>1) ) return NULL;
    }
    $user_identifiant = (int)current($DB_COL);
  }
  // On cherche le parent à partir de l’Id Sconet de l’enfant
  if($mode_connection=='vecteur_parent')
  {
    // LIKE utilisé pour la restriction sur nom / prénom afin d’essayer d’éviter des pbs potentiels de prénoms composés ou de noms patronymiques non uniformisés...
    $DB_SQL = 'SELECT parent.user_id '
            . 'FROM sacoche_user AS eleve '
            . 'LEFT JOIN sacoche_jointure_parent_eleve ON eleve.user_id=sacoche_jointure_parent_eleve.eleve_id '
            . 'LEFT JOIN sacoche_user AS parent ON sacoche_jointure_parent_eleve.parent_id=parent.user_id '
            . 'WHERE eleve.user_sconet_id=:eleve_sconet_id AND parent.user_nom LIKE :parent_nom_like AND parent.user_prenom LIKE :parent_prenom_like ';
    $DB_VAR = array(
      ':eleve_sconet_id'    => $user_identifiant,
      ':parent_nom_like'    => '%'.$nom.'%',
      ':parent_prenom_like' => '%'.$prenom.'%',
    );
    $user_identifiant = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
    if(empty($user_identifiant)) return NULL;
  }
  $from  = ($mode_connection!='api') ? 'FROM sacoche_user ' : 'FROM sacoche_jointure_user_module LEFT JOIN sacoche_user USING (user_id) ' ;
  $where = ($mode_connection!='api') ? '' : 'module_objet="api" AND ' ;
  $DB_SQL = 'SELECT sacoche_user.*, sacoche_user_profil.*, sacoche_groupe.groupe_nom, user_switch_id, '
          . 'TIME_TO_SEC(TIMEDIFF(NOW(),sacoche_user.user_connexion_date)) AS delai_connexion_secondes ' // TIMEDIFF() est plafonné à 839h, soit ~35j, mais peu importe ici.
          . $from
          . 'LEFT JOIN sacoche_user_profil USING (user_profil_sigle) '
          . 'LEFT JOIN sacoche_groupe ON sacoche_user.eleve_classe_id=sacoche_groupe.groupe_id '
          . 'LEFT JOIN sacoche_user_switch ON user_switch_liste LIKE CONCAT("%,",user_id,",%") '
          . 'WHERE '.$where.$champ.'=:identifiant ';
  // LIMIT 1 a priori pas utile, et de surcroît queryRow ne renverra qu’une ligne
  $DB_VAR = array(':identifiant'=>$user_identifiant);
  return DB::queryRow(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * Lister les utilisateurs à partir d’une adresse mail
 *
 * @param string $user_email
 * @return array
 */
public static function DB_lister_user_for_mail($user_email)
{
  $DB_SQL = 'SELECT user_id, user_nom, user_prenom, user_profil_nom_court_singulier '
          . 'FROM sacoche_user '
          . 'LEFT JOIN sacoche_user_profil USING (user_profil_sigle) '
          . 'WHERE user_email=:user_email ';
  $DB_VAR = array(':user_email'=>$user_email);
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * Récuperer, à partir d’un identifiant ou d’un code transmis, quelques données d’un utilisateur demandant la génération d’un nouveau mdp ou l’arrêt d’envoi de courriels
 *
 * @param string $champ_nom   user_id | user_pass_key
 * @param void   $champ_val
 * @return array
 */
public static function DB_recuperer_user_pour_requete_par_mail( $champ_nom , $champ_val )
{
  $DB_SQL = 'SELECT user_id, user_nom, user_prenom, user_email, user_email_refus, user_login, user_password, user_connexion_date, user_profil_nom_court_singulier '
          . 'FROM sacoche_user '
          . 'LEFT JOIN sacoche_user_profil USING (user_profil_sigle) '
          . 'WHERE '.$champ_nom.'=:champ_val ';
  // LIMIT 1 a priori pas utile, et de surcroît queryRow ne renverra qu’une ligne
  $DB_VAR = array(':champ_val'=>$champ_val);
  return DB::queryRow(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * Modifier la date de dernière connexion
 *
 * @param int     $user_id
 * @return void
 */
public static function DB_enregistrer_date_connexion($user_id)
{
  $DB_SQL = 'UPDATE sacoche_user '
          . 'SET user_connexion_date=NOW(), user_pass_key="" '
          . 'WHERE user_id=:user_id ';
  $DB_VAR = array(':user_id'=>$user_id);
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * Modifier le mdp d’un utilisateur ou la clef pour en obtenir un autre (demande de génération si mdp perdu)
 *
 * @param int     $user_id
 * @param string  $user_password   vide pour ne pas le modifier
 * @param string  $user_pass_key
 * @return void
 */
public static function DB_modifier_user_password_or_key( $user_id , $user_password , $user_pass_key )
{
  $set_password = ($user_password) ? ', user_password=:user_password ' : '' ;
  $DB_SQL = 'UPDATE sacoche_user '
          . 'SET user_pass_key=:user_pass_key '.$set_password
          . 'WHERE user_id=:user_id ';
  $DB_VAR = array(
    ':user_id'       => $user_id,
    ':user_pass_key' => $user_pass_key,
    ':user_password' => $user_password,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

}
?>