<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent qu’une base STRUCTURE.
// Ces méthodes ne concernent que les tables "sacoche_catalogue_categorie" et "sacoche_catalogue_appreciation".

class DB_STRUCTURE_CATALOGUE
{

/**
 * Retourner un tableau [valeur texte optgroup] des appréciations par catégorie d’un personnel identifié
 * Ne retourne que les catégories avec appréciation(s).
 *
 * @param int    $user_id
 * @return array
 */
public static function DB_OPT_lister_appreciations($user_id)
{
  $DB_SQL = 'SELECT appreciation_id AS valeur, appreciation_contenu AS texte, categorie_id AS optgroup, categorie_titre '
          . 'FROM sacoche_catalogue_categorie '
          . 'INNER JOIN sacoche_catalogue_appreciation USING(categorie_id,user_id) '
          . 'WHERE user_id=:user_id '
          . 'ORDER BY categorie_ordre ASC, appreciation_ordre ASC ';
  $DB_VAR = array(
    ':user_id' => $user_id,
  );
  $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  foreach($DB_TAB as $DB_ROW)
  {
    Form::$tab_select_optgroup['appr_categorie'][$DB_ROW['optgroup']] = $DB_ROW['categorie_titre'];
    unset($DB_ROW['categorie_titre']);
  }
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * lister_appreciations_avec_categories
 * Retourne aussi les catégories vides (sans appréciations).
 *
 * @param int    $user_id
 * @return array
 */
public static function DB_lister_appreciations_avec_categories($user_id)
{
  // Les 2 premiers champs suivants sont présents dans les 2 tables mais retournés 2 fois dont une fois NULL si catégorie sans appréciation
  $DB_SQL = 'SELECT sacoche_catalogue_categorie.categorie_id, sacoche_catalogue_categorie.user_id, '
          . 'categorie_ordre, categorie_titre, appreciation_id, appreciation_ordre, appreciation_contenu '
          . 'FROM sacoche_catalogue_categorie '
          . 'LEFT JOIN sacoche_catalogue_appreciation USING(categorie_id,user_id) '
          . 'WHERE user_id=:user_id '
          . 'ORDER BY categorie_ordre ASC, appreciation_ordre ASC ';
  $DB_VAR = array(
    ':user_id' => $user_id,
  );
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * ajouter_categorie
 *
 * @param int    $user_id
 * @param int    $categorie_ordre
 * @param string $categorie_titre
 * @return int
 */
public static function DB_ajouter_categorie( $user_id , $categorie_ordre , $categorie_titre )
{
  $DB_SQL = 'INSERT INTO sacoche_catalogue_categorie( user_id, categorie_ordre, categorie_titre) '
          . 'VALUES(                                 :user_id,:categorie_ordre,:categorie_titre)';
  $DB_VAR = array(
    ':user_id'         => $user_id,
    ':categorie_ordre' => $categorie_ordre,
    ':categorie_titre' => $categorie_titre,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  return DB::getLastOid(SACOCHE_STRUCTURE_BD_NAME);
}

/**
 * ajouter_appreciation
 *
 * @param int    $user_id
 * @param int    $categorie_id
 * @param int    $appreciation_ordre
 * @param string $appreciation_contenu
 * @return int
 */
public static function DB_ajouter_appreciation( $user_id , $categorie_id , $appreciation_ordre , $appreciation_contenu )
{
  $DB_SQL = 'INSERT INTO sacoche_catalogue_appreciation( user_id, categorie_id, appreciation_ordre, appreciation_contenu) '
          . 'VALUES(                                    :user_id,:categorie_id,:appreciation_ordre,:appreciation_contenu)';
  $DB_VAR = array(
    ':user_id'              => $user_id,
    ':categorie_id'         => $categorie_id,
    ':appreciation_ordre'   => $appreciation_ordre,
    ':appreciation_contenu' => $appreciation_contenu,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  return DB::getLastOid(SACOCHE_STRUCTURE_BD_NAME);
}

/**
 * modifier_categorie
 *
 * @param int    $categorie_id
 * @param int    $user_id
 * @param int    $categorie_ordre
 * @param string $categorie_titre
 * @return void
 */
public static function DB_modifier_categorie( $categorie_id , $user_id , $categorie_ordre , $categorie_titre )
{
  $DB_SQL = 'UPDATE sacoche_catalogue_categorie '
          . 'SET categorie_ordre=:categorie_ordre, categorie_titre=:categorie_titre '
          . 'WHERE categorie_id=:categorie_id AND user_id=:user_id ';
  $DB_VAR = array(
    ':categorie_id'    => $categorie_id,
    ':user_id'         => $user_id,
    ':categorie_ordre' => $categorie_ordre,
    ':categorie_titre' => $categorie_titre,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * modifier_appreciation
 *
 * @param int    $appreciation_id
 * @param int    $user_id
 * @param int    $categorie_id
 * @param int    $appreciation_ordre
 * @param string $appreciation_contenu
 * @return void
 */
public static function DB_modifier_appreciation( $appreciation_id , $user_id , $categorie_id , $appreciation_ordre , $appreciation_contenu )
{
  $DB_SQL = 'UPDATE sacoche_catalogue_appreciation '
          . 'SET categorie_id=:categorie_id, appreciation_ordre=:appreciation_ordre, appreciation_contenu=:appreciation_contenu '
          . 'WHERE appreciation_id=:appreciation_id AND user_id=:user_id ';
  $DB_VAR = array(
    ':appreciation_id'      => $appreciation_id,
    ':user_id'              => $user_id,
    ':categorie_id'         => $categorie_id,
    ':appreciation_ordre'   => $appreciation_ordre,
    ':appreciation_contenu' => $appreciation_contenu,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * supprimer_categorie
 *
 * @param int    $erreur_id
 * @return void
 */
public static function DB_supprimer_categorie($categorie_id)
{
  $DB_SQL = 'DELETE FROM sacoche_catalogue_categorie '
          . 'WHERE categorie_id=:categorie_id ';
  $DB_VAR = array(
    ':categorie_id' => $categorie_id,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * supprimer_appreciation
 *
 * @param int    $erreur_id
 * @return void
 */
public static function DB_supprimer_appreciation($appreciation_id)
{
  $DB_SQL = 'DELETE FROM sacoche_catalogue_appreciation '
          . 'WHERE appreciation_id=:appreciation_id ';
  $DB_VAR = array(
    ':appreciation_id' => $appreciation_id,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

}
?>