<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent qu’une base STRUCTURE.
// Ces méthodes ne concernent que les tables "sacoche_plan_classe" et "sacoche_jointure_plan_eleve".

class DB_STRUCTURE_PROFESSEUR_PLAN
{

/**
 * Retourner un tableau [valeur texte] des plans de classe d’un professeur
 *
 * @param int   $prof_id
 * @param bool  $msg_si_rien
 * @return array
 */
public static function DB_OPT_lister_plans_prof_groupe( $prof_id , $msg_si_rien=FALSE )
{
  $DB_SQL = 'SELECT plan_id AS valeur, CONCAT("plan ",plan_nom) AS texte, 1 AS optgroup, groupe_id AS data '
          . 'FROM sacoche_plan_classe '
          . 'LEFT JOIN sacoche_groupe USING(groupe_id) '
          . 'WHERE prof_id=:prof_id '
          . 'ORDER BY groupe_nom ASC, plan_nom ASC ';
  $DB_VAR = array(
    ':prof_id' => $prof_id,
  );
  $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  return ( !empty($DB_TAB) || !$msg_si_rien ) ? $DB_TAB : 'Vous n’avez défini aucun plan de classe !' ;
}

/**
 * Retourner un tableau [valeur texte] des plans de classe de collègues pour un groupe donné
 *
 * @param int $prof_id
 * @param int $groupe_id
 * @return array
 */
public static function DB_OPT_lister_plans_collegues_groupe( $prof_id , $groupe_id )
{
  $DB_SQL = 'SELECT plan_id AS valeur, CONCAT(user_nom," ",user_prenom," | plan ",plan_nom) AS texte '
          . 'FROM sacoche_plan_classe '
          . 'LEFT JOIN sacoche_user ON sacoche_plan_classe.prof_id = sacoche_user.user_id '
          . 'WHERE groupe_id = :groupe_id AND prof_id != :prof_id '
          . 'ORDER BY plan_nom ASC ';
  $DB_VAR = array(
    ':prof_id'   => $prof_id,
    ':groupe_id' => $groupe_id,
  );
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * tester_plan_classe_prof
 *
 * @param int $plan_id
 * @param int $prof_id
 * @return void
 */
public static function DB_tester_plan_classe_prof( $plan_id , $prof_id )
{
  $DB_SQL = 'SELECT prof_id '
          . 'FROM sacoche_plan_classe '
          . 'WHERE plan_id=:plan_id ';
  $DB_VAR = array(
    ':plan_id' => $plan_id,
  );
  return ( $prof_id == DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR) );
}

/**
 * Retourne les plans de classe d’un professeur
 * On ne fait pas de jointure sur sacoche_niveau afin de trier selon niveau_ordre car peu utile et compliqué d’actualiser par la suite le tri du tableau affiché
 *
 * @param int $prof_id
 * @return array
 */
public static function DB_lister_plans_prof( $prof_id )
{
  $DB_SQL = 'SELECT sacoche_plan_classe.* , groupe_nom '
          . 'FROM sacoche_plan_classe '
          . 'LEFT JOIN sacoche_groupe USING(groupe_id) '
          . 'WHERE prof_id=:prof_id '
          . 'ORDER BY groupe_nom ASC, plan_nom ASC ';
  $DB_VAR = array(
    ':prof_id' => $prof_id,
  );
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * Retourne les informations d’un plan de classe donné
 *
 * @param int $plan_id
 * @return array
 */
public static function DB_recuperer_plan_prof( $plan_id )
{
  $DB_SQL = 'SELECT sacoche_plan_classe.* '
          . 'FROM sacoche_plan_classe '
          . 'WHERE plan_id=:plan_id ';
  $DB_VAR = array(
    ':plan_id' => $plan_id,
  );
  return DB::queryRow(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * Retourne les infos élèves d’un plan de classe d’un professeur
 * La vérification que c’est bien le plan du prof connecté a déjà été effectuée.
 *
 * @param int $plan_id
 * @return array
 */
public static function DB_lister_plan_eleves( $plan_id )
{
  $DB_SQL = 'SELECT eleve_id, jointure_rangee, jointure_colonne, jointure_ordre, jointure_equipe, jointure_role '
          . 'FROM sacoche_jointure_plan_eleve '
          . 'WHERE plan_id=:plan_id '
          . 'ORDER BY jointure_rangee ASC, jointure_colonne ASC ';
  $DB_VAR = array(
    ':plan_id' => $plan_id,
  );
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * ajouter_plan_classe
 *
 * @param int    $prof_id
 * @param int    $groupe_id
 * @param string $plan_nom
 * @param int    $plan_nb_rangees
 * @param int    $plan_nb_colonnes
 * @return int
 */
public static function DB_ajouter_plan_classe( $prof_id , $groupe_id , $plan_nom , $plan_nb_rangees , $plan_nb_colonnes )
{
  $DB_SQL = 'INSERT INTO sacoche_plan_classe( prof_id, groupe_id, plan_nom, plan_nb_rangees, plan_nb_colonnes) '
          . 'VALUES(                         :prof_id,:groupe_id,:plan_nom,:plan_nb_rangees,:plan_nb_colonnes)';
  $DB_VAR = array(
    ':prof_id'          => $prof_id,
    ':groupe_id'        => $groupe_id,
    ':plan_nom'         => $plan_nom,
    ':plan_nb_rangees'  => $plan_nb_rangees,
    ':plan_nb_colonnes' => $plan_nb_colonnes,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  return DB::getLastOid(SACOCHE_STRUCTURE_BD_NAME);
}

/**
 * modifier_plan_classe
 * La vérification que c’est bien le plan du prof connecté a déjà été effectuée.
 *
 * @param int    $plan_id
 * @param string $plan_nom
 * @return void
 */
public static function DB_modifier_plan_classe( $plan_id , $groupe_id , $plan_nom , $plan_nb_rangees , $plan_nb_colonnes )
{
  $DB_SQL = 'UPDATE sacoche_plan_classe '
          . 'SET groupe_id=:groupe_id, plan_nom=:plan_nom, plan_nb_rangees=:plan_nb_rangees, plan_nb_colonnes=:plan_nb_colonnes '
          . 'WHERE plan_id=:plan_id ';
  $DB_VAR = array(
    ':plan_id'          => $plan_id,
    ':groupe_id'        => $groupe_id,
    ':plan_nom'         => $plan_nom,
    ':plan_nb_rangees'  => $plan_nb_rangees,
    ':plan_nb_colonnes' => $plan_nb_colonnes,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * supprimer_plan_classe
 * La vérification que c’est bien le plan du prof connecté a déjà été effectuée.
 *
 * @param int $plan_id
 * @return void
 */
public static function DB_supprimer_plan_classe($plan_id)
{
  $DB_VAR = array(
    ':plan_id' => $plan_id,
  );
  // Suppression du plan
  $DB_SQL = 'DELETE sacoche_plan_classe, sacoche_jointure_plan_eleve '
          . 'FROM sacoche_plan_classe '
          . 'LEFT JOIN sacoche_jointure_plan_eleve USING (plan_id) '
          . 'WHERE plan_id=:plan_id ';
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  // Mise à jour des devoirs éventuels
  $DB_SQL = 'UPDATE sacoche_devoir '
          . 'SET devoir_eleves_ordre = "nom" '
          . 'WHERE devoir_eleves_ordre = CAST( :plan_id AS CHAR ) COLLATE utf8_unicode_ci ';
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  
}

/**
 * ajouter_plan_eleve
 *
 * @param int    $plan_id
 * @param int    $eleve_id
 * @param int    $jointure_rangee
 * @param int    $jointure_colonne
 * @param int    $jointure_ordre
 * @param char   $jointure_equipe
 * @param string $jointure_role
 * @return void
 */
public static function DB_ajouter_plan_eleve( $plan_id , $eleve_id , $jointure_rangee , $jointure_colonne , $jointure_ordre, $jointure_equipe, $jointure_role )
{
  $DB_SQL = 'INSERT INTO sacoche_jointure_plan_eleve( plan_id, eleve_id, jointure_rangee, jointure_colonne, jointure_ordre, jointure_equipe, jointure_role) '
          . 'VALUES(                                 :plan_id,:eleve_id,:jointure_rangee,:jointure_colonne,:jointure_ordre,:jointure_equipe,:jointure_role)';
  $DB_VAR = array(
    ':plan_id'          => $plan_id,
    ':eleve_id'         => $eleve_id,
    ':jointure_rangee'  => $jointure_rangee,
    ':jointure_colonne' => $jointure_colonne,
    ':jointure_ordre'   => $jointure_ordre,
    ':jointure_equipe'  => $jointure_equipe,
    ':jointure_role'    => $jointure_role,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * modifier_plan_eleve
 *
 * @param int    $plan_id
 * @param int    $eleve_id
 * @param int    $jointure_rangee
 * @param int    $jointure_colonne
 * @param int    $jointure_ordre
 * @param char   $jointure_equipe
 * @param string $jointure_role
 * @return void
 */
public static function DB_modifier_plan_eleve( $plan_id , $eleve_id , $jointure_rangee , $jointure_colonne , $jointure_ordre, $jointure_equipe, $jointure_role )
{
  $DB_SQL = 'UPDATE sacoche_jointure_plan_eleve '
          . 'SET jointure_rangee=:jointure_rangee, jointure_colonne=:jointure_colonne, jointure_ordre=:jointure_ordre, jointure_equipe=:jointure_equipe, jointure_role=:jointure_role '
          . 'WHERE plan_id=:plan_id AND eleve_id=:eleve_id ';
  $DB_VAR = array(
    ':plan_id'          => $plan_id,
    ':eleve_id'         => $eleve_id,
    ':jointure_rangee'  => $jointure_rangee,
    ':jointure_colonne' => $jointure_colonne,
    ':jointure_ordre'   => $jointure_ordre,
    ':jointure_equipe'  => $jointure_equipe,
    ':jointure_role'    => $jointure_role,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * supprimer_plan_eleve
 *
 * @param int $plan_id
 * @param int $eleve_id
 * @return void
 */
public static function DB_supprimer_plan_eleve($plan_id , $eleve_id)
{
  $DB_SQL = 'DELETE FROM sacoche_jointure_plan_eleve '
          . 'WHERE plan_id=:plan_id AND eleve_id=:eleve_id ';
  $DB_VAR = array(
    ':plan_id'  => $plan_id,
    ':eleve_id' => $eleve_id,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

}
?>