<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent qu’une base STRUCTURE.
// Ces méthodes ne concernent que la table "sacoche_courriel_erreur".

class DB_STRUCTURE_COURRIEL_ERREUR
{

/**
 * recuperer_courriels
 *
 * @param void
 * @return array
 */
public static function DB_recuperer_courriels()
{
  $DB_SQL = 'SELECT erreur_email '
          . 'FROM sacoche_courriel_erreur ';
  return DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , NULL);
}

/**
 * lister_erreurs
 *
 * @param void
 * @return array
 */
public static function DB_lister_erreurs()
{
  $DB_SQL = 'SELECT * '
          . 'FROM sacoche_courriel_erreur '
          . 'ORDER BY erreur_date DESC, erreur_email ASC ';
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , NULL);
}

/**
 * compter_erreurs_recentes
 *
 * @param int $nb_jours
 * @return int
 */
public static function DB_compter_erreurs_recentes($nb_jours)
{
  $DB_SQL = 'SELECT COUNT(*) AS nombre '
          . 'FROM sacoche_courriel_erreur '
          . 'WHERE erreur_date > DATE_SUB( NOW() , INTERVAL :nb_jours DAY ) ';
  $DB_VAR = array( ':nb_jours' => $nb_jours );
  return DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * tester_courriel
 *
 * @param string $erreur_email
 * @param int    $erreur_id    inutile si recherche pour un ajout, mais id à éviter si recherche pour une modification
 * @return int
 */
public static function DB_tester_courriel( $erreur_email , $erreur_id=FALSE )
{
  $where_erreur_id = ($erreur_id) ? 'AND erreur_id!=:erreur_id ' : '' ;
  $DB_SQL = 'SELECT erreur_id '
          . 'FROM sacoche_courriel_erreur '
          . 'WHERE erreur_email=:erreur_email '.$where_erreur_id
          . 'LIMIT 1 '; // utile
  $DB_VAR = array(
    ':erreur_email' => $erreur_email,
    ':erreur_id'    => $erreur_id,
  );
  return (int)DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * ajouter_courriel
 *
 * @param string $erreur_email
 * @param string $erreur_users
 * @param string $erreur_info
 * @return int
 */
public static function DB_ajouter_courriel( $erreur_email , $erreur_users , $erreur_info )
{
  $DB_SQL = 'INSERT INTO sacoche_courriel_erreur( erreur_email, erreur_date, erreur_users, erreur_info) '
          . 'VALUES(                             :erreur_email, NOW()      ,:erreur_users,:erreur_info)';
  $DB_VAR = array(
    ':erreur_email' => $erreur_email,
    ':erreur_users' => $erreur_users,
    ':erreur_info'  => $erreur_info,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  return DB::getLastOid(SACOCHE_STRUCTURE_BD_NAME);
}

/**
 * modifier_courriel
 *
 * @param int    $erreur_id
 * @param string $erreur_email
 * @param string $erreur_users
 * @param string $erreur_info
 * @return void
 */
public static function DB_modifier_courriel( $erreur_id , $erreur_email , $erreur_users , $erreur_info )
{
  $DB_SQL = 'UPDATE sacoche_courriel_erreur '
          . 'SET erreur_email=:erreur_email, erreur_users=:erreur_users, erreur_info=:erreur_info '
          . 'WHERE erreur_id=:erreur_id ';
  $DB_VAR = array(
    ':erreur_id'    => $erreur_id,
    ':erreur_email' => $erreur_email,
    ':erreur_users' => $erreur_users,
    ':erreur_info'  => $erreur_info,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * supprimer_courriel
 *
 * @param int    $erreur_id
 * @return void
 */
public static function DB_supprimer_courriel($erreur_id)
{
  $DB_SQL = 'DELETE FROM sacoche_courriel_erreur '
          . 'WHERE erreur_id=:erreur_id ';
  $DB_VAR = array(
    ':erreur_id' => $erreur_id,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

}
?>