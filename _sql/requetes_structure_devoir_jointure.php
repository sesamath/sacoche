<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent qu’une base STRUCTURE.
// Ces méthodes ne concernent que la table "sacoche_jointure_devoir_eleve".

class DB_STRUCTURE_DEVOIR_JOINTURE
{

/**
 * lister_commentaires_eleves_dates
 * Attention : peut remonter aussi des lignes superflues pour des documents joints ou des commentaires supprimés.
 *
 * @param int    $prof_id
 * @param string $liste_eleve_id   id des élèves séparés par des virgules
 * @param string $date_sql_debut
 * @param string $date_sql_fin
 * @return array
 */
public static function DB_lister_commentaires_eleves_dates( $prof_id , $liste_eleve_id , $date_sql_debut , $date_sql_fin)
{
  $DB_SQL = 'SELECT eleve_id, devoir_info, devoir_date, jointure_texte, jointure_audio '
          . 'FROM sacoche_devoir '
          . 'LEFT JOIN sacoche_jointure_devoir_eleve USING (devoir_id) '
          . 'WHERE proprio_id=:proprio_id AND devoir_date>=:date_debut AND devoir_date<=:date_fin AND eleve_id IN('.$liste_eleve_id.') '
          . 'ORDER BY devoir_date ASC ';
  $DB_VAR = array(
    ':proprio_id' => $prof_id,
    ':date_debut' => $date_sql_debut,
    ':date_fin'   => $date_sql_fin,
  );
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * lister_elements
 * Attention : peut remonter aussi des lignes superflues pour des commentaires ou des documents supprimés.
 *
 * @param int   $devoir_id
 * @return array
 */
public static function DB_lister_elements($devoir_id)
{
  $DB_SQL = 'SELECT eleve_id, jointure_texte, jointure_audio, jointure_memo_autoeval, jointure_doc_copie '
          . 'FROM sacoche_jointure_devoir_eleve '
          . 'WHERE devoir_id=:devoir_id ';
  $DB_VAR = array(':devoir_id'=>$devoir_id);
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * lister_documents
 * Attention : peut remonter aussi des lignes superflues pour des commentaires ou des documents supprimés.
 *
 * @param int   $devoir_id
 * @return array
 */
public static function DB_lister_documents($devoir_id)
{
  $DB_SQL = 'SELECT eleve_id, jointure_doc_sujet, jointure_doc_corrige '
          . 'FROM sacoche_jointure_devoir_eleve '
          . 'WHERE devoir_id=:devoir_id ';
  $DB_VAR = array(':devoir_id'=>$devoir_id);
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * lister_copies
 *
 * @param int   $devoir_id
 * @return array
 */
public static function DB_lister_copies($devoir_id)
{
  $DB_SQL = 'SELECT user_nom, user_prenom, jointure_doc_copie '
          . 'FROM sacoche_jointure_devoir_eleve '
          . 'LEFT JOIN sacoche_user ON sacoche_jointure_devoir_eleve.eleve_id = sacoche_user.user_id '
          . 'WHERE devoir_id=:devoir_id AND jointure_doc_copie IS NOT NULL ';
  $DB_VAR = array(':devoir_id'=>$devoir_id);
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * recuperer_commentaires
 * Attention : peut remonter aussi des lignes superflues pour des commentaires ou des documents supprimés.
 *
 * @param int    $devoir_id
 * @param int    $eleve_id
 * @return array
 */
public static function DB_recuperer_commentaires($devoir_id,$eleve_id)
{
  $DB_SQL = 'SELECT jointure_texte, jointure_audio, jointure_memo_autoeval '
          . 'FROM sacoche_jointure_devoir_eleve '
          . 'WHERE devoir_id=:devoir_id AND eleve_id=:eleve_id ';
  $DB_VAR = array(
    ':devoir_id' => $devoir_id,
    ':eleve_id'  => $eleve_id,
  );
  return DB::queryRow(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * recuperer_commentaire
 *
 * @param int    $devoir_id
 * @param int    $eleve_id
 * @param string $msg_objet   texte | audio | memo_autoeval
 * @return string
 */
public static function DB_recuperer_commentaire($devoir_id,$eleve_id,$msg_objet)
{
  $jointure = 'jointure_'.$msg_objet;
  $DB_SQL = 'SELECT '.$jointure.' '
          . 'FROM sacoche_jointure_devoir_eleve '
          . 'WHERE devoir_id=:devoir_id AND eleve_id=:eleve_id ';
  $DB_VAR = array(
    ':devoir_id' => $devoir_id,
    ':eleve_id'  => $eleve_id,
  );
  return DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * remplacer_objet
 *
 * @param int    $devoir_id
 * @param int    $eleve_id
 * @param string $msg_objet   texte | audio | memo_autoeval | doc_sujet | doc_corrige | doc_copie
 * @param string $msg_data   url | json | NULL
 * @return void
 */
public static function DB_remplacer_objet( $devoir_id , $eleve_id , $msg_objet , $msg_data )
{
  $jointure = 'jointure_'.$msg_objet;
  $DB_SQL = 'INSERT INTO sacoche_jointure_devoir_eleve( devoir_id, eleve_id, '.$jointure.') '
          . 'VALUES                                   (:devoir_id,:eleve_id,:'.$jointure.') '
          . 'ON DUPLICATE KEY UPDATE '.$jointure.'=:'.$jointure.' ';
  $DB_VAR = array(
    ':devoir_id'  => $devoir_id,
    ':eleve_id'   => $eleve_id,
    ':'.$jointure => $msg_data,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}


}
?>