<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2021-12-03 => 2022-01-12
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2021-12-03')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-01-12';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif champ ENUM de sacoche_demande
    if(empty($reload_sacoche_demande))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_demande CHANGE demande_statut demande_statut ENUM("eleve","prof","request","aggree","ready") CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "eleve" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_demande SET demande_statut="request" WHERE demande_statut="eleve" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_demande SET demande_statut="aggree" WHERE demande_statut="prof" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_demande CHANGE demande_statut demande_statut ENUM("request","aggree","ready") CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "request" COMMENT "[request] pour une demande à étudier ; [aggree] pour une demande acceptée ; [ready] pour une évaluation prête ; une annulation de l’élève ou du prof efface l’enregistrement" ' );
    }
    // ajout d’un champ à la table [sacoche_user]
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user ADD user_email_refus TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER user_email_origine' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-01-12 => 2022-02-01
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-01-12')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-02-01';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modif champ ENUM de sacoche_demande (bis !)
    if(empty($reload_sacoche_demande))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_demande CHANGE demande_statut demande_statut ENUM("request","aggree","ready","done") CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "request" COMMENT "[request] = demande à étudier ; [aggree] = demande acceptée ; [ready] = évaluation prête ; [done] = évaluation effectuée" ' );
    }
    // et retrait de clef unique
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_demande DROP INDEX demande_key, ADD INDEX eleve_id (eleve_id) ' );
    // ajout d’un paramètre dans sacoche_officiel_configuration
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"only_socle\\"" , "\\"only_valeur\\":0,\\"only_socle\\"" ) WHERE officiel_type="releve" ' );
    // il faut aussi mettre à jour les archives des bilans officiels
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"RELEVE_ONLY_SOCLE\\"" , "\\"RELEVE_ONLY_VALEUR\\":0,\\"RELEVE_ONLY_SOCLE\\"" ) WHERE archive_ref="releve" ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-02-01 => 2022-02-14
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-02-01')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-02-14';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // nouvelle table [sacoche_jointure_prof_remplacement]
    $reload_sacoche_jointure_prof_remplacement = TRUE;
    $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_jointure_prof_remplacement.sql');
    DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
    DB::close(SACOCHE_STRUCTURE_BD_NAME);
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-02-14 => 2022-04-19
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-02-14')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-04-19';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // modification d’un champ de la table [sacoche_officiel_archive]
    if(empty($reload_sacoche_officiel_archive))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_officiel_archive CHANGE structure_denomination structure_denomination VARCHAR(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Pour garder trace de cette chaîne." ' );
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-04-19 => 2022-05-31
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-04-19')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-05-31';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout de paramètres
    $droit = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT parametre_valeur FROM sacoche_parametre WHERE parametre_nom="droit_officiel_livret_positionner_socle"' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("droit_officiel_livret_positionner_enscompl" , "'.$droit.'")' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT INTO sacoche_parametre VALUES ("droit_officiel_livret_positionner_langcultregion" , "'.$droit.'")' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-05-31 => 2022-06-06
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-05-31')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-06-06';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // ajout colonne table sacoche_devoir
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_devoir ADD devoir_voir_repartition TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER devoir_doc_corrige ' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-06-06 => 2022-07-05
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-06-06')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-07-05';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    if(empty($reload_sacoche_matiere_famille))
    {
      // Ajout de familles de matières
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'INSERT sacoche_matiere_famille VALUES ( 52, 2, "Éducation physique et sportive (suite)") ');
      // réordonner la table sacoche_matiere_famille (ligne à déplacer vers la dernière MAJ lors d’ajouts dans sacoche_matiere_famille)
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_matiere_famille ORDER BY matiere_famille_id' );
    }
    // L’ajout de matières cause des conflits lorsque matiere_ref est déjà pris par une matière spécifique.
    // Il s’avère plus stratégique de 
    // 1) récupérer les matières spécifiques et les matières partagées utilisées
    // 2) recharger toute la table des matières partagées
    // 3)recréer ou convertir les matières spécifiques et réactiver les matières partagées utilisées
    // PS. On laisse tomber toutes les matières en doublon "option", "hors établissement", "par correspondance"...
    // Go :
    // récupération des informations sur les matières
    $DB_TAB_communes    = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE (matiere_active=1 OR matiere_siecle=1) AND matiere_id<='.ID_MATIERE_PARTAGEE_MAX);
    $DB_TAB_specifiques = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SELECT * FROM sacoche_matiere WHERE matiere_id>'.ID_MATIERE_PARTAGEE_MAX);
    if(empty($reload_sacoche_matiere))
    {
      // rechargement de la table sacoche_matiere
      $reload_sacoche_matiere = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_matiere.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // on remet en place les matières partagées
    foreach($DB_TAB_communes as $DB_ROW)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active='.$DB_ROW['matiere_active'].', matiere_siecle='.$DB_ROW['matiere_siecle'].', matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$DB_ROW['matiere_id'] );
    }
    // on remet en place les matières spécifiques
    foreach($DB_TAB_specifiques as $DB_ROW)
    {
      $matiere_id_partage = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT matiere_id FROM sacoche_matiere WHERE matiere_ref="'.$DB_ROW['matiere_ref'].'" LIMIT 1');
      if(!$matiere_id_partage)
      {
        // Pas de conflit : on remet la matière spécifique telle qu’elle était
        $DB_SQL = 'INSERT INTO sacoche_matiere( matiere_id, matiere_active, matiere_siecle, matiere_usuelle, matiere_famille_id, matiere_nb_demandes, matiere_ordre, matiere_code, matiere_ref, matiere_nom) '
                . 'VALUES                     (:matiere_id,:matiere_active,:matiere_siecle,:matiere_usuelle,:matiere_famille_id,:matiere_nb_demandes,:matiere_ordre,:matiere_code,:matiere_ref,:matiere_nom) ';
        $DB_VAR = array(
          ':matiere_id'          => $DB_ROW['matiere_id'],
          ':matiere_active'      => $DB_ROW['matiere_active'],
          ':matiere_siecle'      => $DB_ROW['matiere_siecle'],
          ':matiere_usuelle'     => 0,
          ':matiere_famille_id'  => 0,
          ':matiere_nb_demandes' => $DB_ROW['matiere_nb_demandes'],
          ':matiere_ordre'       => $DB_ROW['matiere_ordre'],
          ':matiere_code'        => $DB_ROW['matiere_code'],
          ':matiere_ref'         => $DB_ROW['matiere_ref'],
          ':matiere_nom'         => $DB_ROW['matiere_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
      else
      {
        // Conflit : on la convertit en matière partagée
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_matiere SET matiere_active=1, matiere_nb_demandes='.$DB_ROW['matiere_nb_demandes'].', matiere_ordre='.$DB_ROW['matiere_ordre'].' WHERE matiere_id='.$matiere_id_partage );
        DB_STRUCTURE_MATIERE::DB_deplacer_referentiel_matiere( $DB_ROW['matiere_id'] , $matiere_id_partage );
      }
    }
    // Pour l’ajout ou la modification de niveaux, comme pour les matières, 
    // plutôt que de lister toutes les différences il est plus aisé de :
    // 1) récupérer les niveaux spécifiques et les niveaux partagés utilisés
    // 2) recharger toute la table des niveaux partagés
    // 3)recréer ou convertir les niveaux spécifiques et réactiver les niveaux partagés utilisés
    // Go :
    // mémoriser les niveaux spécifiques et les niveaux actifs
    $DB_SQL = 'SELECT niveau_id FROM sacoche_niveau WHERE niveau_actif = 1 AND niveau_id <= '.ID_NIVEAU_PARTAGE_MAX;
    $DB_COL_actifs = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    $DB_SQL = 'SELECT niveau_id , niveau_ref , niveau_nom FROM sacoche_niveau WHERE niveau_id > '.ID_NIVEAU_PARTAGE_MAX;
    $DB_TAB_persos = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    if(empty($reload_sacoche_niveau_famille))
    {
      //  rechargement de la table niveau_famille
      $reload_sacoche_niveau_famille = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau_famille.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    if(empty($reload_sacoche_niveau))
    {
      //  rechargement de la table niveau
      $reload_sacoche_niveau = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // remise en place des niveaux actifs
    if(!empty($DB_COL_actifs))
    {
      foreach($DB_COL_actifs as $niveau_id)
      {
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_actif = 1 WHERE niveau_id = '.$niveau_id );
      }
    }
    // remise en place des niveaux spécifiques
    if(!empty($DB_TAB_persos))
    {
      $DB_SQL = 'INSERT INTO sacoche_niveau( niveau_id, niveau_actif, niveau_usuel, niveau_famille_id, niveau_ordre, niveau_ref, code_mef, niveau_nom) '
              . 'VALUES                    (:niveau_id,:niveau_actif,:niveau_usuel,:niveau_famille_id,:niveau_ordre,:niveau_ref,:code_mef,:niveau_nom) ';
      foreach($DB_TAB_persos as $DB_ROW)
      {
        $DB_VAR = array(
          ':niveau_id'         => $DB_ROW['niveau_id'],
          ':niveau_actif'      => 1,
          ':niveau_usuel'      => 0,
          ':niveau_famille_id' => 0,
          ':niveau_ordre'      => 999,
          ':niveau_ref'        => $DB_ROW['niveau_ref'],
          ':code_mef'          => '',
          ':niveau_nom'        => $DB_ROW['niveau_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-07-05 => 2022-09-28
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-07-05')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-09-28';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // correction profils utilisateurs activés potentiellement effacés
    $test_pb = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT COUNT(*) FROM sacoche_user_profil WHERE user_profil_obligatoire = 1 AND user_profil_actif = 0' );
    if($test_pb)
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user_profil SET user_profil_actif = 1 WHERE user_profil_obligatoire = 1' );
      $DB_COL = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , 'SELECT DISTINCT user_profil_sigle FROM sacoche_user' );
      foreach($DB_COL as $user_profil_sigle)
      {
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user_profil SET user_profil_actif = 1 WHERE user_profil_sigle = "'.$user_profil_sigle.'"' );
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-09-28 => 2022-09-29
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-09-28')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-09-29';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Lors de la dernière mise à jour de la table des niveaux, il y avait des doublons de valeurs pour niveau_id qui ont pu corrompre les données
    // (à partir de l’id 371039, heureusement élevé)
    // Pour l’ajout ou la modification de niveaux, comme pour les matières, 
    // plutôt que de lister toutes les différences il est plus aisé de :
    // 1) récupérer les niveaux spécifiques et les niveaux partagés utilisés
    // 2) recharger toute la table des niveaux partagés
    // 3)recréer ou convertir les niveaux spécifiques et réactiver les niveaux partagés utilisés
    // Go :
    // mémoriser les niveaux spécifiques et les niveaux actifs
    $DB_SQL = 'SELECT niveau_id FROM sacoche_niveau WHERE niveau_actif = 1 AND niveau_id <= '.ID_NIVEAU_PARTAGE_MAX;
    $DB_COL_actifs = DB::queryCol(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    $DB_SQL = 'SELECT niveau_id , niveau_ref , niveau_nom FROM sacoche_niveau WHERE niveau_id > '.ID_NIVEAU_PARTAGE_MAX;
    $DB_TAB_persos = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL);
    if(empty($reload_sacoche_niveau))
    {
      //  rechargement de la table niveau
      $reload_sacoche_niveau = TRUE;
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_niveau.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // remise en place des niveaux actifs
    if(!empty($DB_COL_actifs))
    {
      foreach($DB_COL_actifs as $niveau_id)
      {
        DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_niveau SET niveau_actif = 1 WHERE niveau_id = '.$niveau_id );
      }
    }
    // remise en place des niveaux spécifiques
    if(!empty($DB_TAB_persos))
    {
      $DB_SQL = 'INSERT INTO sacoche_niveau( niveau_id, niveau_actif, niveau_usuel, niveau_famille_id, niveau_ordre, niveau_ref, code_mef, niveau_nom) '
              . 'VALUES                    (:niveau_id,:niveau_actif,:niveau_usuel,:niveau_famille_id,:niveau_ordre,:niveau_ref,:code_mef,:niveau_nom) ';
      foreach($DB_TAB_persos as $DB_ROW)
      {
        $DB_VAR = array(
          ':niveau_id'         => $DB_ROW['niveau_id'],
          ':niveau_actif'      => 1,
          ':niveau_usuel'      => 0,
          ':niveau_famille_id' => 0,
          ':niveau_ordre'      => 999,
          ':niveau_ref'        => $DB_ROW['niveau_ref'],
          ':code_mef'          => '',
          ':niveau_nom'        => $DB_ROW['niveau_nom'],
        );
        DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// MAJ 2022-09-29 => 2022-12-16
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($version_base_structure_actuelle=='2022-09-29')
{
  if($version_base_structure_actuelle==DB_STRUCTURE_MAJ_BASE::DB_version_base())
  {
    $version_base_structure_actuelle = '2022-12-16';
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_parametre SET parametre_valeur="'.$version_base_structure_actuelle.'" WHERE parametre_nom="version_base"' );
    // Ajout d’une clef
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_referentiel ADD INDEX referentiel_mode_synthese (referentiel_mode_synthese) ' );
    // J’ai encore trouvée une base où la table [sacoche_notification] n’était pas créée...
    // En fait si, elle existe, mais elle est corrompue et pas réparable.
    // Du coup il faut insérer une requête de suppression
    $DB_TAB = DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , 'SHOW TABLES FROM '.SACOCHE_STRUCTURE_BD_NAME.' LIKE "sacoche_notification"');
    if(empty($DB_TAB))
    {
      $reload_sacoche_notification = TRUE;
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'DROP TABLE IF EXISTS sacoche_notification' );
      $requetes = file_get_contents(CHEMIN_DOSSIER_SQL_STRUCTURE.'sacoche_notification.sql');
      DB::query(SACOCHE_STRUCTURE_BD_NAME , $requetes );
      DB::close(SACOCHE_STRUCTURE_BD_NAME);
    }
    // ajout de paramètre dans [sacoche_officiel_configuration]
    // il faut aussi mettre à jour les archives des bilans officiels (même si ce nouveau paramètre n'y sera pas utilisé)
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"fond\\"" , "\\"etape_max_maj_positionnements\\":\\"5complet\\",\\"fond\\"" ) WHERE officiel_type="livret" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_configuration SET configuration_contenu = REPLACE( configuration_contenu , "\\"fond\\"" , "\\"etape_max_maj_positionnements\\":\\"5complet\\",\\"fond\\"" ) WHERE officiel_type="bulletin" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu ,   "\\"LIVRET_FOND\\"" ,   "\\"LIVRET_ETAPE_MAX_MAJ_POSITIONNEMENTS\\":\\"5complet\\",\\"LIVRET_FOND\\"" )   WHERE archive_ref="livret" ' );
    DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_officiel_archive SET archive_contenu = REPLACE( archive_contenu , "\\"BULLETIN_FOND\\"" , "\\"BULLETIN_ETAPE_MAX_MAJ_POSITIONNEMENTS\\":\\"5complet\\",\\"BULLETIN_FOND\\"" ) WHERE archive_ref="bulletin" ' );
    // Modif colonne de [sacoche_user] et tentative de rétablissement des bons identifiants STS si un import précédent a dépassé la valeur limite de MEDIUMINT UNSIGNED
    if(empty($reload_sacoche_user))
    {
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user CHANGE user_sconet_id user_sconet_id INT UNSIGNED NOT NULL DEFAULT 0 COMMENT "ELEVE.ELEVE.ID pour un élève ; INDIVIDU_ID pour un prof ; PERSONNE_ID pour un parent" ' );
      DB::query(SACOCHE_STRUCTURE_BD_NAME , 'ALTER TABLE sacoche_user CHANGE user_sconet_elenoet user_sconet_elenoet INT UNSIGNED NOT NULL DEFAULT 0 COMMENT "ELENOET pour un élève (entre 2000 et 5000 ; parfois appelé n° GEP avec un 0 devant). Ce champ sert aussi pour un import Factos (élèves et parents)." ' );
    }
    $max_mediumint = 16777215;
    $test_pb = DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , 'SELECT COUNT(*) FROM sacoche_user WHERE user_sconet_id = '.$max_mediumint );
    if($test_pb)
    {
      $tab_SIECLE = DB_STRUCTURE_SIECLE::DB_recuperer_import_contenu('sts_emp_UAI');
      if(!empty($tab_SIECLE))
      {
        if(!empty($tab_SIECLE['DONNEES']['INDIVIDUS']['INDIVIDU']))
        {
          // S’il n’y a qu’une seule entrée ce n’est pas la même architecture
          if(!isset($tab_SIECLE['DONNEES']['INDIVIDUS']['INDIVIDU'][0]))
          {
            $tab_SIECLE['DONNEES']['INDIVIDUS']['INDIVIDU'] = array( 0 => $tab_SIECLE['DONNEES']['INDIVIDUS']['INDIVIDU'] );
          }
          foreach($tab_SIECLE['DONNEES']['INDIVIDUS']['INDIVIDU'] as $tab)
          {
            $id_sts = $tab['@attributes']['ID'];
            $nom    = Clean::nom($tab['NOM_USAGE']);
            $prenom = Clean::prenom($tab['PRENOM']);
            $DB_VAR = array(
              ':id_sts_new' => $id_sts,
              ':id_sts_old' => $max_mediumint,
              ':nom'        => $nom,
              ':prenom'     => $prenom,
            );
            DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_sconet_id=:id_sts_new WHERE user_sconet_id=:id_sts_old AND user_nom=:nom AND user_prenom=:prenom' , $DB_VAR );
          }
        }
        if(!empty($tab_SIECLE['DONNEES']['SUPPLEANTS']['SUPPLEANT']))
        {
          // S’il n’y a qu’une seule entrée ce n’est pas la même architecture
          if(!isset($tab_SIECLE['DONNEES']['SUPPLEANTS']['SUPPLEANT'][0]))
          {
            $tab_SIECLE['DONNEES']['SUPPLEANTS']['SUPPLEANT'] = array( 0 => $tab_SIECLE['DONNEES']['SUPPLEANTS']['SUPPLEANT'] );
          }
          foreach($tab_SIECLE['DONNEES']['SUPPLEANTS']['SUPPLEANT'] as $tab)
          {
            $id_sts = $tab['@attributes']['ID'];
            $nom    = Clean::nom($tab['NOM_USAGE']);
            $prenom = Clean::prenom($tab['PRENOM']);
            $DB_VAR = array(
              ':id_sts_new' => $id_sts,
              ':id_sts_old' => $max_mediumint,
              ':nom'        => $nom,
              ':prenom'     => $prenom,
            );
            DB::query(SACOCHE_STRUCTURE_BD_NAME , 'UPDATE sacoche_user SET user_sconet_id=:id_sts_new WHERE user_sconet_id=:id_sts_old AND user_nom=:nom AND user_prenom=:prenom' , $DB_VAR );
          }
        }
      }
    }
  }
}

?>
