<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend DB (pour permettre l’autoload)

// Ces méthodes ne concernent qu’une base STRUCTURE.
// Ces méthodes sont en rapport avec les matières (tables "sacoche_niveau" + "sacoche_matiere_famille" + "sacoche_jointure_user_matiere").

class DB_STRUCTURE_NIVEAU
{

/**
 * lister_niveaux
 *
 * @param bool   is_specifique
 * @return array
 */
public static function DB_lister_niveaux($is_specifique)
{
  $where_niveau = ($is_specifique) ? 'niveau_id>'.ID_NIVEAU_PARTAGE_MAX.' ' : 'niveau_actif=1 AND niveau_id<='.ID_NIVEAU_PARTAGE_MAX.' ' ;
  $order_niveau = ($is_specifique) ? 'niveau_nom ASC ' : 'niveau_ordre ASC ' ;
  $DB_SQL = 'SELECT niveau_id, niveau_ref, niveau_nom '
          . 'FROM sacoche_niveau '
          . 'WHERE '.$where_niveau
          . 'ORDER BY '.$order_niveau;
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , NULL);
}

/**
 * lister_niveaux_etablissement
 *
 * @param bool $with_particuliers
 * @return array
 */
public static function DB_lister_niveaux_etablissement($with_particuliers)
{
  $join_niveau_famille  = ($with_particuliers) ? '' : 'LEFT JOIN sacoche_niveau_famille USING (niveau_famille_id) ' ;
  $where_niveau_famille = ($with_particuliers) ? '' : 'AND niveau_famille_categorie=3 ' ;
  $DB_SQL = 'SELECT niveau_id, niveau_ordre, niveau_ref, code_mef, niveau_nom '
          . 'FROM sacoche_niveau '
          . $join_niveau_famille
          . 'WHERE niveau_actif=1 '.$where_niveau_famille
          . 'ORDER BY niveau_ordre ASC ';
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , NULL);
}

/**
 * lister_niveaux_famille
 *
 * @param int   niveau_famille_id
 * @return array
 */
public static function DB_lister_niveaux_famille($niveau_famille_id)
{
  if($niveau_famille_id==ID_FAMILLE_NIVEAU_USUEL)
  {
    $where_niveau = 'niveau_usuel=1 ';
  }
  else
  {
    // Ajouter, si pertinent, des niveaux de cycle qui sinon ne sont pas trouvés car pas dans la famille...
    require(CHEMIN_DOSSIER_INCLUDE.'tableau_niveaux_generiques.php');
    $where_niveau = 'niveau_famille_id='.$niveau_famille_id.' '.sql_niveau_generique($niveau_famille_id);
  }
  $DB_SQL = 'SELECT niveau_id, niveau_ref, niveau_nom, niveau_actif '
          . 'FROM sacoche_niveau '
          . 'WHERE '.$where_niveau
          . 'ORDER BY niveau_ordre ASC, niveau_nom ASC';
  return DB::queryTab(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , NULL);
}

/**
 * tester_niveau_reference
 *
 * @param string $niveau_ref
 * @param int    $niveau_id    inutile si recherche pour un ajout, mais id à éviter si recherche pour une modification
 * @return int
 */
public static function DB_tester_niveau_reference($niveau_ref,$niveau_id=FALSE)
{
  $where_niveau_id = ($niveau_id) ? 'AND niveau_id!=:niveau_id ' : '' ;
  $DB_SQL = 'SELECT niveau_id '
          . 'FROM sacoche_niveau '
          . 'WHERE niveau_ref=:niveau_ref '.$where_niveau_id
          . 'LIMIT 1 '; // utile
  $DB_VAR = array(
    ':niveau_ref' => $niveau_ref,
    ':niveau_id'  => $niveau_id,
  );
  return (int)DB::queryOne(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * ajouter_niveau_specifique
 *
 * @param string $niveau_ref
 * @param string $niveau_nom
 * @return int
 */
public static function DB_ajouter_niveau_specifique($niveau_ref,$niveau_nom)
{
  $DB_SQL = 'INSERT INTO sacoche_niveau(niveau_actif, niveau_usuel, niveau_famille_id, niveau_ordre, niveau_ref, code_mef, niveau_nom) '
          . 'VALUES(                   :niveau_actif,:niveau_usuel,:niveau_famille_id,:niveau_ordre,:niveau_ref,:code_mef,:niveau_nom) ';
  $DB_VAR = array(
    ':niveau_actif'      => 1,
    ':niveau_usuel'      => 0,
    ':niveau_famille_id' => 0,
    ':niveau_ordre'      => 999,
    ':niveau_ref'        => $niveau_ref,
    ':code_mef'          => "",
    ':niveau_nom'        => $niveau_nom,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  return DB::getLastOid(SACOCHE_STRUCTURE_BD_NAME);
}

/**
 * modifier_niveau_partage
 *
 * @param int    $niveau_id
 * @param int    $niveau_actif   (0/1)
 * @return void
 */
public static function DB_modifier_niveau_partage($niveau_id,$niveau_actif)
{
  $DB_SQL = 'UPDATE sacoche_niveau '
          . 'SET niveau_actif=:niveau_actif '
          . 'WHERE niveau_id=:niveau_id ';
  $DB_VAR = array(
    ':niveau_id'    => $niveau_id,
    ':niveau_actif' => $niveau_actif,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  // On laisse les référentiels en sommeil, au cas où...
}

/**
 * modifier_niveau_specifique
 *
 * @param int    $niveau_id
 * @param string $niveau_ref
 * @param string $niveau_nom
 * @return void
 */
public static function DB_modifier_niveau_specifique($niveau_id,$niveau_ref,$niveau_nom)
{
  $DB_SQL = 'UPDATE sacoche_niveau '
          . 'SET niveau_ref=:niveau_ref,niveau_nom=:niveau_nom '
          . 'WHERE niveau_id=:niveau_id ';
  $DB_VAR = array(
    ':niveau_id'  => $niveau_id,
    ':niveau_ref' => $niveau_ref,
    ':niveau_nom' => $niveau_nom,
  );
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
}

/**
 * Supprimer un niveau spécifique
 *
 * @param int $niveau_id
 * @return void
 */
public static function DB_supprimer_niveau_specifique($niveau_id)
{
  $DB_SQL = 'DELETE sacoche_niveau, sacoche_jointure_message_destinataire '
          . 'FROM sacoche_niveau '
          . 'LEFT JOIN sacoche_jointure_message_destinataire ON sacoche_niveau.niveau_id=sacoche_jointure_message_destinataire.destinataire_id AND destinataire_type="niveau" '
          . 'WHERE niveau_id=:niveau_id ';
  $DB_VAR = array(':niveau_id'=>$niveau_id);
  DB::query(SACOCHE_STRUCTURE_BD_NAME , $DB_SQL , $DB_VAR);
  // Il faut aussi supprimer les référentiels associés, et donc tous les scores associés (orphelins du niveau)
  DB_STRUCTURE_ADMINISTRATEUR::DB_supprimer_referentiels( 'niveau_id' , $niveau_id );
}

}
?>