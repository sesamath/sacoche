DROP TABLE IF EXISTS sacoche_chorus_config;

-- Attention : pas d’apostrophes droites dans les lignes commentées sinon on peut obtenir un bug d’analyse dans la classe pdo de SebR : "SQLSTATE[HY093]: Invalid parameter number: no parameters were bound ..."
-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB

CREATE TABLE sacoche_chorus_config (
  cle    VARCHAR(64) COLLATE utf8_unicode_ci NOT NULL     COMMENT "Pour un type entier, utiliser la colonne txt pour stocker autre chose.",
  valeur INT         UNSIGNED                DEFAULT NULL COMMENT "Pour stocker autre chose qu’un entier.",
  txt    TEXT        COLLATE utf8_unicode_ci,
  PRIMARY KEY (cle)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
