DROP TABLE IF EXISTS sacoche_convention;

-- Attention : pas d’apostrophes droites dans les lignes commentées sinon on peut obtenir un bug d’analyse dans la classe pdo de SebR : "SQLSTATE[HY093]: Invalid parameter number: no parameters were bound ..."
-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB
-- Attention : pour un champ DATE ou DATETIME, DEFAULT NOW() ne fonctionne qu’à partir de MySQL 5.6.5
-- Attention : pour un champ DATE ou DATETIME, la configuration NO_ZERO_DATE (incluse dans le mode strict de MySQL 5.7.4 à 5.7.7), interdit les valeurs en dehors de 1000-01-01 00:00:00 à 9999-12-31 23:59:59

CREATE TABLE sacoche_convention (
  convention_id                         SMALLINT    UNSIGNED                NOT NULL AUTO_INCREMENT,
  sacoche_base                          MEDIUMINT   UNSIGNED                NOT NULL DEFAULT 0,
  connexion_nom                         VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '""',
  convention_date_debut                 DATE                                         DEFAULT NULL COMMENT "Ne vaut normalement jamais NULL.",
  convention_date_fin                   DATE                                         DEFAULT NULL COMMENT "Ne vaut normalement jamais NULL.",
  convention_creation                   DATE                                         DEFAULT NULL COMMENT "Ne vaut normalement jamais NULL.",
  convention_signature                  DATE                                         DEFAULT NULL,
  convention_paiement                   DATE                                         DEFAULT NULL,
  convention_relance                    DATE                                         DEFAULT NULL,
  convention_activation                 TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  convention_mail_renouv                DATE                                         DEFAULT NULL,
  convention_chorus_date                DATE                                         DEFAULT NULL COMMENT "Date de soumission vers Chorus Pro.",
  convention_chorus_facture_id          INT         UNSIGNED                         DEFAULT NULL,
  convention_chorus_service_code        VARCHAR(64)                                  DEFAULT NULL,
  convention_chorus_numero_engagement   VARCHAR(64) COLLATE utf8_unicode_ci          DEFAULT NULL,
  convention_chorus_facture_statut      VARCHAR(32) COLLATE utf8_unicode_ci          DEFAULT NULL,
  convention_chorus_facture_statut_date DATE                                         DEFAULT NULL,
  convention_chorus_facture_historique  TEXT        COLLATE utf8_unicode_ci,
  convention_commentaire                TEXT        COLLATE utf8_unicode_ci,
  PRIMARY KEY (convention_id),
  UNIQUE KEY (sacoche_base,connexion_nom,convention_date_debut),
  KEY convention_date_fin (convention_date_fin),
  KEY convention_activation (convention_activation)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT "Pour les conventions ENT établissements (serveur Sésamath).";
