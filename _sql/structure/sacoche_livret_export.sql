DROP TABLE IF EXISTS sacoche_livret_export;

-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB... sauf NULL !

CREATE TABLE sacoche_livret_export (
  user_id                 MEDIUMINT               UNSIGNED                NOT NULL DEFAULT 0,
  livret_page_ref         VARCHAR(6)              COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  livret_page_periodicite ENUM("periode","cycle") COLLATE utf8_unicode_ci NOT NULL DEFAULT "periode",
  jointure_periode        VARCHAR(2)              COLLATE utf8_unicode_ci NOT NULL DEFAULT "0" COMMENT "Renseigné si livret_page_periodicite = periode ; @see sacoche_periode.periode_livret",
  sacoche_version         VARCHAR(11)             COLLATE utf8_unicode_ci NOT NULL DEFAULT ""  COMMENT "Pour écarter des exports obsolètes si besoin.",
  export_contenu          MEDIUMTEXT              COLLATE utf8_unicode_ci          DEFAULT NULL, -- Le type TEXT n’a pas suffit pour un établissement avec bcp d’éléments travaillés...
  UNIQUE KEY export_id ( user_id , livret_page_periodicite , jointure_periode ), -- Attention, les valeurs NULL ne sont pas prise en compte dans le test d’unicité d’une clef, du coup jointure_periode est à "0" pour un cycle
  KEY livret_page_ref (livret_page_ref),
  KEY livret_page_periodicite (livret_page_periodicite),
  KEY jointure_periode (jointure_periode)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
