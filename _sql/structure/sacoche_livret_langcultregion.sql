DROP TABLE IF EXISTS sacoche_livret_langcultregion;

-- Attention : pas d’apostrophes droites dans les lignes commentées sinon on peut obtenir un bug d’analyse dans la classe pdo de SebR : "SQLSTATE[HY093]: Invalid parameter number: no parameters were bound ..."

CREATE TABLE sacoche_livret_langcultregion (
  livret_langcultregion_id   TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  livret_langcultregion_code VARCHAR(3)  COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  livret_langcultregion_nom  VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  PRIMARY KEY (livret_langcultregion_id),
  UNIQUE KEY (livret_langcultregion_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Enseignements de complément";

ALTER TABLE sacoche_livret_langcultregion DISABLE KEYS;

INSERT INTO sacoche_livret_langcultregion VALUES
( 0, "AUC", "Aucune"),
( 1, "BAQ", "Basque"),                                -- Auparavant BAS
( 2, "BRE", "Breton"),
( 3, "COS", "Corse"),                                 -- Auparavant COR
( 4, "OCI", "Occitan langue d’oc"),                   -- Auparavant OCC
( 5, "GSW", "Langues régionales d’Alsace"),           -- Auparavant ALS
( 6, "MOL", "Langues régionales des pays mosellans"), -- Auparavant MOS -- suppression avril 2018 -- réintroduction décembre 2018
( 7, "CAT", "Catalan"),                               -- ajout avril 2018
( 8, "CPF", "Créole"),                                -- ajout décembre 2018
( 9, "FUD", "Futunien"),                              -- ajout décembre 2018
(10, "GAL", "Gallo"),                                 -- ajout décembre 2018
(11, "MEL", "Langues mélanésiennes"),                 -- ajout décembre 2018
(12, "TAH", "Tahitien"),                              -- ajout décembre 2018
(13, "WLS", "Wallisien");                             -- ajout décembre 2018

ALTER TABLE sacoche_livret_langcultregion ENABLE KEYS;
