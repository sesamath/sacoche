DROP TABLE IF EXISTS sacoche_crcn_competence;

-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB... sauf NULL !

CREATE TABLE sacoche_crcn_competence (
  crcn_competence_id          TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  crcn_domaine_id             TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  crcn_competence_ordre       TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  crcn_competence_ref         CHAR(3)     COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  crcn_competence_libelle     VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  crcn_competence_description TEXT        COLLATE utf8_unicode_ci          DEFAULT NULL,
  crcn_competence_niveau_1    TEXT        COLLATE utf8_unicode_ci          DEFAULT NULL,
  crcn_competence_niveau_2    TEXT        COLLATE utf8_unicode_ci          DEFAULT NULL,
  crcn_competence_niveau_3    TEXT        COLLATE utf8_unicode_ci          DEFAULT NULL,
  crcn_competence_niveau_4    TEXT        COLLATE utf8_unicode_ci          DEFAULT NULL,
  crcn_competence_niveau_5    TEXT        COLLATE utf8_unicode_ci          DEFAULT NULL,
  PRIMARY KEY (crcn_competence_id),
  KEY crcn_domaine_id (crcn_domaine_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE sacoche_crcn_competence DISABLE KEYS;

INSERT INTO sacoche_crcn_competence VALUES 
(11, 1, 1, "MEN", "Mener une recherche et une veille d’information",
  "Mener une recherche et une veille d’information pour répondre à un besoin d’information et se tenir au courant de l’actualité d’un sujet tout en étant en mesure de vérifier les sources et la fiabilité de l’information (avec un moteur de recherche, au sein d’un réseau social, par abonnement à des flux ou des lettres d’information, ou tout autre moyen).",
  "Lire et repérer des informations sur un support numérique.\r\nEffectuer une recherche simple en ligne en utilisant un moteur de recherche.",
  "Reformuler sa requête en modifiant les mots-clés pour obtenir de meilleurs résultats.\r\nQuestionner la fiabilité et la pertinence des sources.",
  "Effectuer une recherche dans des environnements numériques divers.\r\nExpliquer sa stratégie de recherche.\r\nConnaître les principaux critères permettant d’évaluer la fiabilité et la pertinence de diverses sources.",
  "Construire une stratégie de recherche en fonction de ses besoins et de ses centres d’intérêt.\r\nUtiliser des options de recherche avancées pour obtenir les meilleurs résultats.\r\nÉvaluer la fiabilité et la pertinence de diverses sources.",
  "Constituer une documentation sur un sujet : sélectionner des sources, citer les sources, élaborer une sitographie.\r\nUtiliser un ou plusieurs logiciels spécialisés pour mettre en place une veille."
),
(12, 1, 2, "GER", "Gérer des données",
  "Stocker et organiser des données pour les retrouver, les conserver et en faciliter l’accès et la gestion (avec un gestionnaire de fichiers, un espace de stockage en ligne, des classeurs, des bases de données, un système d’information, etc.).",
  "Sauvegarder des fichiers dans l’ordinateur utilisé et les retrouver.",
  "Sauvegarder des fichiers dans l’ordinateur utilisé, et dans un espace de stockage partagé et sécurisé, afin de pouvoir les réutiliser.",
  "Savoir distinguer les différents types d’espaces de stockage.\r\nStocker et organiser les données dans des environnements numériques sécurisés de sorte qu’elles soient facilement accessibles.",
  "Stocker et organiser les données pour qu’elles soient accessibles dans des environnements numériques locaux et distants.\r\nPartager des données en ligne et attribuer les droits d’accès.\r\nConcevoir une organisation efficace de rangement de dossiers en tenant compte des formats de fichiers.",
  "Sauvegarder un fichier sous différents formats.\r\nComprendre les métadonnées et leur fonctionnement.\r\nSynchroniser des données locales avec un espace de stockage en ligne."
),
(13, 1, 3, "TRA", "Traiter des données",
  "Appliquer des traitements à des données pour les analyser et les interpréter (avec un tableur, un programme, un logiciel de traitement d’enquête, une requête de calcul dans une base de données, etc.).",
  "Sélectionner et mettre en relation des informations issues de ressources numériques.",
  "Insérer, saisir, et trier des données dans un tableur pour les exploiter.",
  "Saisir, organiser, trier et filtrer des données dans une application.\r\nAppliquer une formule simple pour résoudre un problème.",
  "Traiter des données pour analyser une problématique.\r\nAppliquer une fonction statistique simple.",
  "Automatiser un traitement de données.\r\nAdapter le format d’une donnée (texte/nombre).\r\nConcevoir une formule conditionnelle."
);

INSERT INTO sacoche_crcn_competence VALUES 
(21, 2, 1, "INT", "Interagir",
  "Interagir avec des individus et des petits groupes pour échanger dans divers contextes liés à la vie privée ou à une activité professionnelle, de façon ponctuelle et récurrente (avec une messagerie électronique, une messagerie instantanée, un système de visio-conférence, etc.).",
  "Comprendre que des contenus sur Internet peuvent être inappropriés et savoir réagir.",
  "Utiliser un outil ou un service numérique pour communiquer.\r\nConnaître et utiliser les règles de civilité lors des interactions en ligne.",
  "Utiliser différents outils ou services de communication numérique.\r\nAdapter ses pratiques de communication en tenant compte de l’espace de publication considéré.\r\nRespecter les principales règles de civilité et le droit des personnes lors des interactions en ligne.",
  "Sélectionner les outils ou services de communication numérique adaptés au contexte et à la situation de communication.\r\nAdapter son expression publique en ligne en fonction de ses interlocuteurs.\r\nFiltrer et éviter les contenus inappropriés.",
  "Mettre en œuvre différentes stratégies et attitudes en fonction du contexte d’usage et des interlocuteurs.\r\nModérer les interactions en ligne pour garantir le respect des règles de civilité et le droit des personnes."
),
(22, 2, 2, "PAR", "Partager et publier",
  "Partager et publier des informations et des contenus pour communiquer ses propres productions ou opinions, relayer celles des autres en contexte de communication publique en apportant un regard critique sur la nature du contenu (avec des plateformes de partage, des réseaux sociaux, des blogs, des espaces de forum et de commentaires, de système de gestion de contenu CMS, etc.).",
  "Publier des contenus en ligne.\r\n",
  "Partager des contenus numériques en ligne en diffusion publique ou privée.\r\nModifier les informations attachées à son profil dans un environnement numérique en fonction du contexte d’usage.\r\nSavoir que certains contenus sont protégés par un droit d’auteur.\r\nIdentifier l’origine des informations et des contenus partagés.",
  "Utiliser un outil approprié pour partager des contenus avec un public large ou restreint.\r\nCiter les sources d’information dans un document partagé.\r\nConnaître et appliquer quelques éléments du droit de la propriété intellectuelle ainsi que les licences associées au partage de contenus.\r\nUtiliser des identités numériques multiples, adaptées aux différents contextes et usages.",
  "Choisir un outil approprié pour partager des contenus et réagir sur des contenus publiés.\r\nParamétrer la visibilité d’un contenu partagé.\r\nSavoir créer des identités numériques multiples, adaptées aux différents contextes et usages.",
  "Maîtriser son expression publique en ligne en fonction du média.\r\nConnaître et appliquer les bases du droit de la propriété intellectuelle ainsi que les licences associées au partage de contenus."
),
(23, 2, 3, "COL", "Collaborer",
  "Collaborer dans un groupe pour réaliser un projet, co-produire des ressources, des connaissances, des données, et pour apprendre (avec des plateformes de travail collaboratif et de partage de document, des éditeurs en ligne, des fonctionnalités de suivi de modifications ou de gestion de versions, etc.).",
  "Utiliser un dispositif d’écriture collaborative.",
  "Utiliser un dispositif d’écriture collaborative adapté à un projet afin de partager des idées et de coproduire des contenus.",
  "Utiliser un service numérique adapté pour partager des idées et coproduire des contenus dans le cadre d’un projet.",
  "Animer ou participer activement à un travail collaboratif avec divers outils numériques.",
  "Organiser et encourager des pratiques de travail collaboratif adaptées aux besoins d’un projet."
),
(24, 2, 4, "SIN", "S’insérer dans le monde numérique",
  "Maîtriser les enjeux de la présence en ligne, développer des stratégies et des pratiques autonomes en respectant les règles, les droits et les valeurs qui leur sont liés, pour se positionner en tant qu’acteur social, économique et citoyen dans le monde numérique, et répondre à des objectifs (avec les réseaux sociaux et les outils permettant de développer une présence publique sur Internet, et en lien avec la vie citoyenne, la vie professionnelle, la vie privée, etc.).",
  "Comprendre la nécessité de protéger la vie privée de chacun.",
  "Utiliser des moyens simples pour protéger les données personnelles.",
  "Comprendre le concept d’identité numérique et comment les traces numériques dessinent une réputation en ligne.\r\nCréer et paramétrer un profil au sein d’un environnement numérique.\r\nSurveiller son identité numérique et sa réputation en ligne.",
  "Respecter les règles de civilité et le droit des personnes lors des interactions en ligne.\r\nProtéger sa e-réputation dans des environnements numériques divers.\r\nGérer, actualiser et améliorer son identité numérique publique.",
  "Définir des stratégies de protection de la e-réputation dans des environnements numériques divers.\r\nChoisir le niveau de confidentialité d’une publication en ligne.\r\nConnaître ses droits d’information, d’accès, de rectification, d’opposition, de suppression et de déréférencement.\r\nPrendre conscience des enjeux économiques, sociaux juridiques et politiques du numérique."
);

INSERT INTO sacoche_crcn_competence VALUES 
(31, 3, 1, "TEX", "Développer des documents textuels",
  "Produire des documents à contenu majoritairement textuel pour communiquer des idées, rendre compte et valoriser ses travaux (avec des logiciels de traitement de texte, de présentation, de création de page web, de carte conceptuelle, etc.).",
  "Utiliser les fonctions simples d’un traitement de texte.",
  "Utiliser les fonctions simples d’une application pour produire des contenus majoritairement textuels associés à une image, un son ou une vidéo.",
  "Créer des contenus majoritairement textuels à l’aide de différentes applications.\r\nEnrichir un document en y intégrant des objets numériques variés.",
  "Concevoir, organiser et éditorialiser des contenus majoritairement textuels sur différents supports et dans différents formats.\r\nImporter, éditer et modifier des contenus existants en y intégrant de nouveaux objets numériques.",
  "Choisir le format de diffusion d’une publication en ligne.\r\nSystématiser la mise en forme."
),
(32, 3, 2, "MUL", "Développer des documents multimédia",
  "Développer des documents à contenu multimédia pour créer ses propres productions multimédia, enrichir ses créations textuelles (avec des logiciels de capture et d’édition d’image /son/vidéo/animation, etc...).",
  "Produire ou numériser une image ou un son.",
  "Produire et enregistrer un document multimédia.",
  "Produire une image, un son ou une vidéo avec différents outils numériques.\r\nUtiliser des procédures simples pour modifier un document multimédia.",
  "Acquérir, produire et modifier des objets multimédia.\r\nTraiter des images et des sons.\r\nRéaliser des créations multimédia comportant des programmes de génération automatique (de texte, image, son...).",
  "Créer un objet multimédia."
),
(33, 3, 3, "ADA", "Adapter les documents à leur finalité",
  "Adapter des documents de tous types en fonction de l’usage envisagé et maîtriser l’usage des licences pour permettre, faciliter et encadrer l’utilisation dans divers contextes (mise à jour fréquente, diffusion multicanale, impression, mise en ligne, projection, etc.) (avec les fonctionnalités des logiciels liées à la préparation d’impression, de projection, de mise en ligne, les outils de conversion de format, etc.).",
  "Utiliser des fonctions simples de mise en page d’un document pour répondre à un objectif de diffusion.",
  "Connaître et respecter les règles élémentaires du droit d’auteur, du droit à l’image et du droit à la protection des données personnelles.",
  "Organiser et optimiser des contenus numériques pour les publier en ligne.\r\nConvertir un document numérique en différents formats.\r\nUtiliser des fonctionnalités simples pour permettre l’accessibilité d’un document.\r\nAppliquer les règles du droit d’auteur, du droit à l’image et du droit à la protection des données personnelles.",
  "Organiser des contenus numériques en vue de leur consultation et/ou de leur réutilisation par autrui.\r\nAdapter les formats des contenus numériques pour les diffuser selon des modalités variées.",
  "Adapter les caractéristiques d’une image (définition, format compression) à une intégration dans une page Internet.\r\nFavoriser l’accessibilité des documents numériques."
),
(34, 3, 4, "PRO", "Programmer",
  "Écrire des programmes et des algorithmes pour répondre à un besoin (automatiser une tâche répétitive, accomplir des tâches complexes ou chronophages, résoudre un problème logique, etc.) et pour développer un contenu riche (jeu, site web, etc.) (avec des environnements de développement informatique simples, des logiciels de planification de tâches, etc.).",
  "Lire et construire un algorithme qui comprend des instructions simples.",
  "Réaliser un programme simple.",
  "Développer un programme pour répondre à un problème à partir d’instructions simples d’un langage de programmation.\r\nModifier un algorithme simple en faisant évoluer ses éléments de programmation.\r\nMettre au point et exécuter un programme simple commandant un système réel ou un système numérique.",
  "Inscrire l’écriture et le développement des programmes dans un travail collaboratif et constructif.\r\nModifier le comportement d’un objet régi par un programme simple.",
  "Créer un programme animant un objet graphique ou réel.\r\nÉcrire et développer des programmes pour répondre à des problèmes et modéliser des phénomènes physiques, économiques et sociaux."
);

INSERT INTO sacoche_crcn_competence VALUES 
(41, 4, 1, "SEC", "Sécuriser l’environnement numérique",
  "Sécuriser les équipements, les communications et les données pour se prémunir contre les attaques, pièges, désagréments et incidents susceptibles de nuire au bon fonctionnement des matériels, logiciels, sites internet, et de compromettre les transactions et les données (avec des logiciels de protection, la maîtrise de bonnes pratiques, etc.).",
  "",
  "Identifier les risques principaux qui menacent son environnement informatique.",
  "Choisir et appliquer des mesures simples de protection de son environnement informatique.",
  "Identifier différents risques numériques et mettre en œuvre des stratégies de protection des ressources matérielles et logicielles.\r\nVérifier l’absence de menace dans un contenu avant action (ouverture, activation, installation).\r\nSécuriser ses accès aux environnements numériques.",
  "Vérifier l’identité certifiée associée à un site Internet sécurisé.\r\nConnaître les risques liés à un réseau wifi ouvert."
),
(42, 4, 2, "DON", "Protéger les données personnelles et la vie privée",
  "Maîtriser ses traces et gérer les données personnelles pour protéger sa vie privée et celle des autres, et adopter une pratique éclairée (avec le paramétrage des paramètres de confidentialité, la surveillance régulière de ses traces, etc.).",
  "Identifier les données à caractère personnel et celles à ne pas partager.",
  "Connaître les règles attachées à la protection des données personnelles.\r\nConnaître le concept de traces de navigation.\r\nSavoir que les traces peuvent être vues, collectées ou analysées par d’autres personnes.",
  "Appliquer des procédures pour protéger les données personnelles.\r\nSécuriser et paramétrer la confidentialité d’un profil numérique.\r\nÊtre attentif aux traces personnelles laissées lors de l’utilisation de services en ligne.\r\nComprendre les grandes lignes des conditions générales d’utilisation d’un service en ligne.",
  "Mettre en œuvre des stratégies de protection de sa vie privée et de ses données personnelles et respecter celles des autres.\r\nRepérer les traces personnelles laissées lors des utilisations de services en ligne.\r\nSécuriser sa navigation en ligne et analyser les pages et fichiers consultés et utilisés.\r\nTrouver et interpréter les conditions générales d’utilisation d’un service en ligne.",
  "Prendre conscience des enjeux économiques, sociaux, politiques et juridiques de la traçabilité.\r\nComprendre les incidences concrètes des conditions générales d’utilisation d’un service. Évaluer la pertinence d’une collecte de données par un service en ligne et en comprendre les finalités."
),
(43, 4, 3, "SAN", "Protéger la santé, le bien-être et l’environnement",
  "Prévenir et limiter les risques générés par le numérique sur la santé, le bien-être et l’environnement mais aussi tirer parti de ses potentialités pour favoriser le développement personnel, le soin, l’inclusion dans la société et la qualité des conditions de vie, pour soi et pour les autres (avec la connaissance des effets du numérique sur la santé physique et psychique et sur l’environnement, et des pratiques, services et outils numériques dédiés au bien-être, à la santé, à l’accessibilité, etc.).",
  "Comprendre que l’utilisation non réfléchie des technologies numériques peut avoir des impacts négatifs sur sa santé et son équilibre social et psychologique.",
  "Utiliser des moyens simples pour préserver sa santé en adaptant son espace de travail et en régulant ses pratiques.\r\nReconnaître les comportements et contenus qui relèvent du cyber-harcèlement.\r\nÊtre conscient que l’utilisation des technologies numériques peut avoir un impact sur l’environnement pour adopter des comportements simples pour économiser de l’énergie et des ressources.",
  "Connaître les conséquences principales de l’utilisation des technologies numériques sur la santé et l’équilibre social et psychologique.\r\nAdapter son utilisation des technologies numériques pour protéger sa santé et son équilibre social et psychologique.\r\nRéagir pour soi ou pour autrui à des situations de cyber-harcèlement.\r\nIdentifier des aspects positifs et négatifs de ses usages numériques sur l’environnement.\r\nAdopter des comportements simples pour économiser de l’énergie et des ressources.",
  "Mettre en œuvre des stratégies de protection de sa santé et de celle des autres dans un environnement numérique..\r\nPrendre des mesures pour protéger l’environnement des impacts négatifs de l’utilisation d’appareils numériques.\r\nPrendre des mesures pour économiser de l’énergie et des ressources à travers l’utilisation de moyens technologiques.",
  "Choisir et promouvoir des stratégies de protection de sa santé et de celle des autres dans un environnement numérique.\r\nLimiter pour soi le stress associé à la connexion permanente."
);

INSERT INTO sacoche_crcn_competence VALUES 
(51, 5, 1, "RES", "Résoudre des problèmes techniques",
  "Résoudre des problèmes techniques pour garantir et rétablir le bon fonctionnement d’un environnement informatique (avec les outils de configuration et de maintenance des logiciels ou des systèmes d’exploitation, et en mobilisant les ressources techniques ou humaines nécessaires, etc.).",
  "Savoir décrire l’architecture simple d’un ordinateur et de ses périphériques.",
  "Résoudre des problèmes simples empêchant l’accès à un service numérique usuel.",
  "Identifier des problèmes techniques liés à un environnement informatique.\r\nRésoudre des problèmes simples liés au stockage ou au partage de données.",
  "Différencier et interpréter les problèmes liés à l’utilisation des technologies (erreur humaine, défaillance technique, etc.).\r\nChoisir et mettre en œuvre des stratégies pour résoudre des problèmes techniques en utilisant des outils et réseaux numériques.",
  "Entretenir le système d’exploitation, les données, les connexions, les équipements.\r\nPartager des solutions à des problèmes techniques."
),
(52, 5, 2, "EVO", "Évoluer dans un environnement numérique",
  "Installer, configurer et enrichir un environnement numérique (matériels, outils, services) pour disposer d’un cadre adapté aux activités menées, à leur contexte d’exercice ou à des valeurs (avec les outils de configuration des logiciels et des systèmes d’exploitation, l’installation de nouveaux logiciels ou la souscription à des services, etc.).",
  "Se connecter à un environnement numérique.\r\nUtiliser les fonctionnalités élémentaires d’un environnement numérique.",
  "Retrouver des ressources et des contenus dans un environnement numérique.",
  "Personnaliser un environnement numérique.\r\nOrganiser ses contenus et ses ressources dans son environnement numérique.",
  "Prendre conscience de l’évolution des matériels et des logiciels pour développer sa culture numérique.\r\nSélectionner des technologies et outils numériques afin de concevoir et produire de nouveaux savoirs et objets.",
  "Soutenir ses pairs dans le développement de leurs compétences numériques.\r\nUtiliser des ressources pour mettre à jour et améliorer ses compétences numériques, notamment pour de nouveaux outils et de nouvelles aptitudes.\r\nConnaître les grandes lignes des modèles économiques du numérique."
);

ALTER TABLE sacoche_crcn_competence ENABLE KEYS;
