DROP TABLE IF EXISTS sacoche_jointure_prof_remplacement;

CREATE TABLE sacoche_jointure_prof_remplacement (
  prof_absent_id     MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  prof_remplacant_id MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY ( prof_absent_id , prof_remplacant_id ),
  KEY prof_remplacant_id (prof_remplacant_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
