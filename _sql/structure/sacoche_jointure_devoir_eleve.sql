DROP TABLE IF EXISTS sacoche_jointure_devoir_eleve;

-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB... sauf NULL !

CREATE TABLE sacoche_jointure_devoir_eleve (
  devoir_id              MEDIUMINT    UNSIGNED                NOT NULL DEFAULT 0,
  eleve_id               MEDIUMINT    UNSIGNED                NOT NULL DEFAULT 0,
  jointure_texte         TEXT      COLLATE utf8_unicode_ci             DEFAULT NULL COMMENT "URL d’un fichier txt contenant le commentaire",
  jointure_audio         TEXT      COLLATE utf8_unicode_ci             DEFAULT NULL COMMENT "URL d’un fichier audio",
  jointure_memo_autoeval TEXT      COLLATE utf8_unicode_ci             DEFAULT NULL COMMENT "Au format json", -- Le type VARCHAR(255) n’a pas suffit pour un devoir avec bcp d’items...
  jointure_doc_sujet     TEXT      COLLATE utf8_unicode_ci             DEFAULT NULL COMMENT "URL du sujet du devoir",
  jointure_doc_corrige   TEXT      COLLATE utf8_unicode_ci             DEFAULT NULL COMMENT "URL du corrigé du devoir",
  jointure_doc_copie     TEXT      COLLATE utf8_unicode_ci             DEFAULT NULL COMMENT "URL de la copie de l’élève",
  PRIMARY KEY ( devoir_id , eleve_id ),
  KEY eleve_id (eleve_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
