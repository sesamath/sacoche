DROP TABLE IF EXISTS sacoche_crcn_niveau;

-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB... sauf NULL !

CREATE TABLE sacoche_crcn_niveau (
  crcn_niveau_numero      TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  crcn_niveau_cycle       TINYINT     UNSIGNED                NOT NULL DEFAULT 0 COMMENT "3 pour le cycle 3 (cm2/6e) ; 5 pour le collège (cycle 4) et le lycée ; 6 pour l’enseignement supérieur et formation d’adultes",
  crcn_niveau_categorie   VARCHAR(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  crcn_niveau_description TEXT        COLLATE utf8_unicode_ci          DEFAULT NULL,
  PRIMARY KEY (crcn_niveau_numero)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE sacoche_crcn_niveau DISABLE KEYS;

INSERT INTO sacoche_crcn_niveau VALUES 
(0, 0, ""           , ""),
(1, 3, "Novice"     , "L’individu est capable de réaliser des actions élémentaires associées aux situations les plus courantes.\r\nIl peut appliquer une procédure simple en étant guidé, et en ayant parfois recours à l’aide d’un tiers."),
(2, 3, "Novice"     , "L’individu est capable de réaliser des actions élémentaires associées aux situations les plus courantes.\r\nIl peut appliquer seul une procédure simple tant que ne survient pas de difficulté.\r\nIl cherche des solutions avec d’autres lorsqu’il est confronté à des imprévus.\r\nIl peut répondre ponctuellement à une demande d’aide."),
(3, 3, "Indépendant", "L’individu est capable de réaliser des actions simples dans la plupart des situations courantes.\r\nIl peut élaborer de façon autonome une procédure pour accomplir une de ces actions."),
(4, 5, "Indépendant", "L’individu est capable de réaliser des actions simples dans toutes les situations courantes.\r\nIl peut élaborer de façon autonome une procédure adaptée et l’appliquer efficacement pour accomplir une de ces actions.\r\nIl peut venir en aide à d’autres selon une modalité d’entraide informelle."),
(5, 5, "Avancé"     , "L’individu est capable de mettre en œuvre des pratiques avancées dans des situations nouvelles pour lui, ou imposant un cadre d’exigence particulier.\r\nIl peut choisir une démarche adaptée pour atteindre son but, parmi des approches déjà établies."),
(6, 6, "Avancé"     , "L’individu est capable de mettre en œuvre des pratiques avancées dans des situations nouvelles pour lui, ou imposant un cadre d’exigence particulier.\r\nIl peut concevoir et mettre en œuvre une démarche adaptée pour atteindre son but, en combinant de façon créative les solutions existantes.\r\nIl peut transmettre avec aisance ses compétences à d’autres."),
(7, 6, "Expert"     , "L’individu est capable de mettre en œuvre des pratiques complexes dans des situations potentiellement inédites, imprévisibles ou contraignantes.\r\nIl peut analyser un besoin et élaborer une solution mobilisant le numérique de façon originale pour y répondre."),
(8, 6, "Expert"     , "L’individu est capable de mettre en œuvre des pratiques complexes dans des situations potentiellement inédites, imprévisibles ou contraignantes.\r\nIl peut analyser un besoin et élaborer une solution mobilisant le numérique de façon originale pour y répondre.\r\nIl met ses productions numériques à la disposition d’autres, qui les utilisent, traduisant ainsi son rayonnement et son influence dans la sphère numérique.");

ALTER TABLE sacoche_crcn_niveau ENABLE KEYS;
