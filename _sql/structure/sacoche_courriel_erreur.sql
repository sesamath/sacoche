DROP TABLE IF EXISTS sacoche_courriel_erreur;

-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB... sauf NULL !
-- Attention : pour un champ DATE ou DATETIME, DEFAULT NOW() ne fonctionne qu’à partir de MySQL 5.6.5
-- Attention : pour un champ DATE ou DATETIME, la configuration NO_ZERO_DATE (incluse dans le mode strict de MySQL 5.7.4 à 5.7.7), interdit les valeurs en dehors de 1000-01-01 00:00:00 à 9999-12-31 23:59:59

CREATE TABLE sacoche_courriel_erreur (
  erreur_id    SMALLINT     UNSIGNED                NOT NULL AUTO_INCREMENT,
  erreur_email VARCHAR(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  erreur_date  DATE                                          DEFAULT NULL,
  erreur_users VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  erreur_info  TINYTEXT     COLLATE utf8_unicode_ci          DEFAULT NULL,
  PRIMARY KEY (erreur_id),
  UNIQUE KEY erreur_email (erreur_email)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
