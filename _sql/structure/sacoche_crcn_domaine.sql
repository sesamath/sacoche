DROP TABLE IF EXISTS sacoche_crcn_domaine;

CREATE TABLE sacoche_crcn_domaine (
  crcn_domaine_id      TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  crcn_domaine_ordre   TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  crcn_domaine_ref     CHAR(3)     COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  crcn_domaine_libelle VARCHAR(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  PRIMARY KEY (crcn_domaine_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE sacoche_crcn_domaine DISABLE KEYS;

INSERT INTO sacoche_crcn_domaine VALUES
(1, 1, "INF", "Information et données"        ),
(2, 2, "COM", "Communication et collaboration"),
(3, 3, "CRE", "Création de contenus"          ),
(4, 4, "PRO", "Protection et sécurité"        ),
(5, 5, "ENV", "Environnement numérique"       );

ALTER TABLE sacoche_crcn_domaine ENABLE KEYS;
