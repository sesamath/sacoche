DROP TABLE IF EXISTS sacoche_catalogue_appreciation;

CREATE TABLE sacoche_catalogue_appreciation (
  appreciation_id      MEDIUMINT    UNSIGNED                NOT NULL AUTO_INCREMENT,
  user_id              MEDIUMINT    UNSIGNED                NOT NULL DEFAULT 0 COMMENT "Pas obligatoire car déjà lié au user via categorie_id, mais plus clair quand même ainsi",
  categorie_id         SMALLINT     UNSIGNED                NOT NULL DEFAULT 0,
  appreciation_ordre   SMALLINT     UNSIGNED                NOT NULL DEFAULT 0,
  appreciation_contenu VARCHAR(500) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  PRIMARY KEY (appreciation_id),
  KEY user_id (user_id),
  KEY categorie_id (categorie_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
