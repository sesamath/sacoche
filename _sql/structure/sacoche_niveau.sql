DROP TABLE IF EXISTS sacoche_niveau;

-- Attention : pas d’apostrophes droites dans les lignes commentées sinon on peut obtenir un bug d’analyse dans la classe pdo de SebR : "SQLSTATE[HY093]: Invalid parameter number: no parameters were bound ..."

CREATE TABLE sacoche_niveau (
  niveau_id         MEDIUMINT   UNSIGNED                NOT NULL AUTO_INCREMENT,
  niveau_actif      TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  niveau_usuel      TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  niveau_famille_id SMALLINT    UNSIGNED                NOT NULL DEFAULT 0,
  niveau_ordre      SMALLINT    UNSIGNED                NOT NULL DEFAULT 0,
  niveau_ref        VARCHAR(6)  COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  code_mef          CHAR(11)    COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Masque à comparer avec le code_mef d’une classe (nomenclature Sconet).",
  niveau_nom        VARCHAR(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  PRIMARY KEY (niveau_id),
  KEY niveau_actif (niveau_actif),
  KEY niveau_famille_id (niveau_famille_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE sacoche_niveau DISABLE KEYS;

-- Attention, sur certains LCS le module pdo_mysql bloque au dela de 40 instructions envoyées d’un coup (mais un INSERT multiple avec des milliers de lignes ne pose pas de pb).
-- Par contre sur le serveur de Bordeaux ça a fini par planter.
-- J’ai scindé en plusieurs INSERT mais le problème est resté.
-- C’est donc la taille d’une requête (>300Ko) qui doit poser souci.
-- Du coup je scinde quand même en quelques INSERT, mais je ne les envoie pas tous d’un seul coup...

INSERT INTO sacoche_niveau VALUES 

-- 1 Cycles (regroupements de niveaux)

(      1, 0, 1,   1,   5,     "P1",            "", "Cycle 1 (PS-MS-GS)"),
(      2, 0, 1,   1,  30,     "P2",            "", "Cycle 2 (CP-CE1-CE2)"),
(      3, 0, 1,   1,  90,     "P3",            "", "Cycle 3 (CM1-CM2-6E)"),
(      4, 0, 1,   1, 150,     "P4",            "", "Cycle 4 (5E-4E-3E)"),
(     10, 0, 1,   1, 160,   "COLL",            "", "Cycle Collège"),
(     16, 0, 0,   1, 170,  "SEGPA",            "", "Cycle SEGPA"),
(     20, 0, 1,   1, 205,  "LYCEE",            "", "Cycle Lycée"),
(     23, 0, 0,   1, 233,     "BT",            "", "Cycle BT"),
(     24, 0, 0,   1, 244,    "CAP",            "", "Cycle CAP"),
(     25, 0, 0,   1, 244,    "BEP",            "", "Cycle BEP"),
(     26, 0, 0,   1, 248,    "PRO",            "", "Cycle Bac Pro"),
(     27, 0, 0,   1, 252,    "BMA",            "", "Cycle BMA"),
(     28, 0, 0,   1, 255,     "BP",            "", "Cycle BP"),
(     29, 0, 0,   1, 272,   "CAPa",            "", "Cycle CAP Agricole"),
(     30, 0, 0,   1, 277,   "PROa",            "", "Cycle Bac Pro Agricole"),
(     31, 0, 0,   1, 302,   "CPGE",            "", "Cycle CPGE"),
(     32, 0, 0,   1, 313,    "BTS",            "", "Cycle BTS"),
(     33, 0, 0,   1, 318,    "DMA",            "", "Cycle DMA"),
(     34, 0, 0,   1, 280,    "CSA",            "", "Cycle Certificat spécialisation agricole"),
(     35, 0, 0,   1, 351,    "DUT",            "", "Cycle DUT"),
(     36, 0, 0,   1, 278,    "BPA",            "", "Cycle Brevet Professionnel Agricole"),
(     37, 0, 0,   1, 372,   "BTSa",            "", "Cycle BTS Agricole"),

-- 2 CECRL (Cadre Européen Commun de Référence pour les Langues)

(    201, 0, 0,   2,  71,     "A1",            "", "A1 : Introductif - Découverte"),
(    202, 0, 0,   2,  72,     "A2",            "", "A2 : Intermédiaire - Usuel"),
(    203, 0, 0,   2,  73,     "B1",            "", "B1 : Seuil"),
(    204, 0, 0,   2,  74,     "B2",            "", "B2 : Avancé - Indépendant"),
(    205, 0, 0,   2,  75,     "C1",            "", "C1 : Autonome"),
(    206, 0, 0,   2,  76,     "C2",            "", "C2 : Maîtrise"),

-- 3 APSA (Activités Physiques, Sportives et Artistiques)

(    301, 0, 0,   3,  81,     "N1",            "", "Niveau 1"),
(    302, 0, 0,   3,  82,     "N2",            "", "Niveau 2"),
(    303, 0, 0,   3,  83,     "N3",            "", "Niveau 3"),
(    304, 0, 0,   3,  84,     "N4",            "", "Niveau 4"),
(    305, 0, 0,   3,  85,     "N5",            "", "Niveau 5"),

-- 4 Niveaux adaptés (pour élèves à besoins particuliers)

(    401, 0, 0,   4,   0, "TPSADA",            "", "TPS adaptée"),
(    402, 0, 0,   4,   1,  "PSADA",            "", "PS adaptée"),
(    403, 0, 0,   4,   2,  "MSADA",            "", "MS adaptée"),
(    404, 0, 0,   4,   3,  "GSADA",            "", "GS adaptée"),
(    405, 0, 1,   4,   5,  "P1ADA",            "", "Cycle 1 adapté"),
(    406, 0, 0,   4,  11,  "CPADA",            "", "CP adapté"),
(    407, 0, 0,   4,  21, "CE1ADA",            "", "CE1 adapté"),
(    408, 0, 0,   4,  22, "CE2ADA",            "", "CE2 adapté"),
(    409, 0, 1,   4,  30,  "P2ADA",            "", "Cycle 2 adapté"),
(    410, 0, 0,   4,  31, "CM1ADA",            "", "CM1 adapté"),
(    411, 0, 0,   4,  32, "CM2ADA",            "", "CM2 adapté"),
(    412, 0, 1,   4,  90,  "P3ADA",            "", "Cycle 3 adapté"),
(    413, 0, 0,   4, 100,   "6ADA",            "", "Sixième adaptée"),
(    414, 0, 0,   4, 101,   "5ADA",            "", "Cinquième adaptée"),
(    415, 0, 0,   4, 102,   "4ADA",            "", "Quatrième adaptée"),
(    416, 0, 0,   4, 103,   "3ADA",            "", "Troisième adaptée"),
(    417, 0, 1,   4, 150,  "P4ADA",            "", "Cycle 4 adapté"),
(    418, 0, 0,   4, 200,   "2ADA",            "", "Seconde adaptée"),
(    419, 0, 0,   4, 201,   "1ADA",            "", "Première adaptée"),
(    420, 0, 0,   4, 202,   "TADA",            "", "Terminale adaptée"),

-- 60 Premier degré -- dispositif de formation 0~4 & 60~62

(   1011, 0, 1,  60,   0,    "TPS", "0041000111.", "TPS (maternelle, toute petite section)"),
(   1031, 0, 1,  60,   1,     "PS", "0001000131.", "PS (maternelle, petite section)"),
(   1032, 0, 1,  60,   2,     "MS", "0001000132.", "MS (maternelle, moyenne section)"),
(   1033, 0, 1,  60,   3,     "GS", "0001000133.", "GS (maternelle, grande section)"),
(  10001, 0, 1,  60,  11,     "CP", "0011000211.", "CP (cours préparatoire)"),
(  20001, 0, 1,  60,  21,    "CE1", "0021000221.", "CE1 (cours élémentaire 1e année)"),
(  20002, 0, 1,  60,  22,    "CE2", "0021000222.", "CE2 (cours élémentaire 2e année)"),
(  30001, 0, 1,  60,  31,    "CM1", "0031000221.", "CM1 (cours moyen 1e année)"),
(  30002, 0, 1,  60,  32,    "CM2", "0031000222.", "CM2 (cours moyen 2e année)"),
(  60002, 0, 0,  60,  60, "1UPE2A", "0601000311.", "UPE2A (unité pédagogique pour élèves allophones arrivants)"),
(  61001, 0, 0,  61,  61,   "ADAP", "0611000411.", "Classe d’adaptation"), -- fermé 31/08/2013
(  62001, 0, 0,  62,  62,   "CLIS", "0621000511.", "CLIS (classe pour l’inclusion scolaire)"),

-- 100 Collège -- dispositif de formation 100~106

( 100001, 0, 0, 100, 100,   "6MUS", "1001000211.", "Sixième musique"),
( 100002, 0, 0, 100, 100, "6UPE2A", "1001000411.", "Sixième UPE2A"),
( 100003, 0, 1, 100, 100,      "6", "1001001211.", "Sixième"),
( 100004, 0, 0, 100, 100, "6DANSE", "1001002211.", "Sixième danse"),
( 100005, 0, 0, 100, 100, "6THEAT", "1001002311.", "Sixième théâtre"),

( 100027, 0, 0, 100, 100,    "6BC", "1001002711.", "Sixième bilangue de continuité"),
( 100028, 0, 0, 100, 100,   "6BCD", "1001002811.", "Sixième bilangue de continuité danse"),
( 100029, 0, 0, 100, 100,   "6BCM", "1001002911.", "Sixième bilangue de continuité musique"),
( 100030, 0, 0, 100, 100,   "6BCT", "1001003011.", "Sixième bilangue de continuité théâtre"),

( 101001, 0, 1, 100, 101,      "5", "1011000111.", "Cinquième"),
( 101002, 0, 0, 100, 101,   "5MUS", "1011000211.", "Cinquième musique"),
( 101003, 0, 0, 100, 101, "5UPE2A", "1011000411.", "Cinquième UPE2A"),
( 101004, 0, 0, 100, 101, "5DANSE", "1011002211.", "Cinquième danse"),
( 101005, 0, 0, 100, 101, "5THEAT", "1011002311.", "Cinquième théâtre"),

( 102001, 0, 1, 100, 102,      "4", "1021000111.", "Quatrième"),
( 102002, 0, 0, 100, 102,   "4MUS", "1021000211.", "Quatrième musique"),
( 102003, 0, 0, 100, 102, "4UPE2A", "1021000411.", "Quatrième UPE2A"),
( 102004, 0, 0, 100, 102,   "4AES", "1021000711.", "Quatrième d’aide et de soutien"), -- fermé 31/08/2010
( 102005, 0, 0, 100, 102, "4E-AGR", "1021002111.", "Quatrième de l’enseignement agricole"),
( 102006, 0, 0, 100, 102, "4DANSE", "1021002211.", "Quatrième danse"),
( 102007, 0, 0, 100, 102, "4THEAT", "1021002311.", "Quatrième théâtre"),

( 103001, 0, 0, 100, 103,   "3MUS", "1031000211.", "Troisième musique"),
( 103002, 0, 0, 100, 103, "3UPE2A", "1031000411.", "Troisième UPE2A"),
( 103003, 0, 1, 100, 103,      "3", "1031001911.", "Troisième"),
( 103004, 0, 0, 100, 103, "3E-AGR", "1031002111.", "Troisième de l’enseignement agricole"),
( 103005, 0, 0, 100, 103, "3DANSE", "1031002211.", "Troisième danse"),
( 103006, 0, 0, 100, 103, "3THEAT", "1031002311.", "Troisième théâtre"),
( 103007, 0, 0, 100, 103,  "3PPRO", "1031002611.", "Troisième prépa pro"), -- fermé 31/08/2019
( 103008, 0, 0, 100, 103,  "3PMET", "1031003111.", "Troisième prépa-métiers"),

( 104001, 0, 0, 100, 104,     "3I", "1041000111.", "Troisième d’insertion"), -- fermé 31/08/2012
( 105001, 0, 0, 100, 105, "CL-REL", "1051001511.", "Classe relais"),
( 105002, 0, 0, 100, 105,  "ATREL", "1051002011.", "Atelier relais"),
( 106001, 0, 0, 100, 106,   "ULIS", "1061001611.", "ULIS (unité localisée pour l’inclusion scolaire)"), -- fermé 31/08/2015

-- 160 SEGPA / Pré-apprentissage" -- dispositif de formation 110~167

( 110001, 0, 0, 160, 110,   "3PVP", "1101000622.", "3PVP (troisième préparatoire à la voie professionnelle)"), -- fermé 31/08/2005
( 112001, 0, 0, 160, 112,    "CPA", "1122199911.", "CPA (classe préparatoire à l’apprentissage)"), -- fermé 2020-08-31
( 113001, 0, 0, 160, 113,  "CLIPA", "1139999911.", "CLIPA (classe d’initiation pré-professionnelle en alternance)"), -- fermé 31/08/2010
( 114001, 0, 0, 160, 114,    "FAJ", "1149999911.", "FAJ (formation d’apprenti junior)"), -- fermé 31/08/2008
( 115001, 0, 0, 160, 115,   "DIMA", "1159999911.", "DIMA (dispositif d’initiation aux métiers en alternance)"), -- fermé 31/08/2019
( 164001, 0, 0, 160, 100, "6SEGPA", "1641000211.", "Sixième SEGPA"),
( 165001, 0, 0, 160, 101, "5SEGPA", "1651000211.", "Cinquième SEGPA"),
( 166001, 0, 0, 160, 102, "4SEGPA", "1661000211.", "Quatrième SEGPA"),
( 167001, 0, 0, 160, 103, "3SEGPA", "1671000211.", "Troisième SEGPA"),

-- 200 Voie générale -- dispositif de formation 200~202

( 200001, 0, 0, 200, 200,      "2", "2001001411.", "Seconde de détermination"), -- fermé 31/08/2019
( 200002, 0, 0, 200, 200,   "2GT2", "2001001511.", "Seconde 2 ens. exploration"), -- fermé 31/08/2019
( 200003, 0, 0, 200, 200,   "2GT3", "2001001611.", "Seconde 3 ens. exploration"), -- fermé 31/08/2019
( 200004, 0, 0, 200, 200,   "2GT1", "2001001711.", "Seconde 1 ens. exploration"), -- fermé 31/08/2019
( 200005, 0, 1, 200, 200,   "2-GT", "2001001811.", "Seconde générale et technologique"),

( 201000, 0, 1, 200, 201,     "1G", "2011001911.", "Première générale"),
( 201001, 0, 0, 200, 201,     "1S", "20111...11.", "Première S (scientifique)"), -- fermé 31/08/2019
( 201002, 0, 0, 200, 201, "1S-SVT", "2011101011.", "Première S (scientifique) option SVT"), -- fermé 31/08/2019
( 201003, 0, 0, 200, 201,  "1S-SI", "2011101111.", "Première S (scientifique) option SI"), -- fermé 31/08/2019
( 201004, 0, 0, 200, 201, "1S-EAT", "2011101311.", "Première S (scientifique) option EAT"), -- fermé 31/08/2019
( 201005, 0, 0, 200, 201,    "1ES", "2011200511.", "Première ES (économique et sociale)"), -- fermé 31/08/2019
( 201006, 0, 0, 200, 201,     "1L", "2011301911.", "Première L (littéraire)"), -- fermé 31/08/2019

( 202000, 0, 1, 200, 202,     "TG", "2021001911.", "Terminale générale"),
( 202001, 0, 0, 200, 202,     "TS", "20211...11.", "Terminale S (scientifique)"), -- fermé 31/08/2020
( 202002, 0, 0, 200, 202, "TS-SVT", "2021101011.", "Terminale S (scientifique) option SVT"), -- fermé 31/08/2020
( 202003, 0, 0, 200, 202,  "TS-SI", "2021101111.", "Terminale S (scientifique) option SI"), -- fermé 31/08/2020
( 202004, 0, 0, 200, 202, "TS-EAT", "2021101311.", "Terminale S (scientifique) option EAT"), -- fermé 31/08/2020
( 202005, 0, 0, 200, 202,    "TES", "2021200511.", "Terminale ES (économique et sociale)"), -- fermé 31/08/2020
( 202006, 0, 0, 200, 202,     "TL", "2021301911.", "Terminale L (littéraire)"), -- fermé 31/08/2020

-- 210 Voie technologique -- dispositif de formation 210~214

( 210000, 0, 1, 210, 210,     "2T", "210.....11.", "Seconde technologique"),
( 210001, 0, 0, 210, 210,   "2INS", "2101330111.", "Seconde musique option instrument"), -- fermé 31/08/2019
( 210002, 0, 0, 210, 210,   "2DAN", "2101330211.", "Seconde musique option danse"), -- fermé 31/08/2019
( 210003, 0, 0, 210, 210,  "2STHR", "2103340211.", "Seconde STHR"),

( 211000, 0, 1, 210, 211,    "1ST", "211.....11.", "Première sciences et technologies"),
( 211001, 0, 0, 210, 211,   "1INS", "2111330111.", "Première musique option instrument"), -- fermé 31/08/2019
( 211002, 0, 0, 210, 211,   "1DAN", "2111330211.", "Première musique option danse"), -- fermé 31/08/2019
( 211003, 0, 0, 210, 211, "1STD2A", "2111340311.", "Première STD2A sc.& tec.design.arts appliqués"),
( 211004, 0, 0, 210, 211, "1STI2D", "2112000311.", "Première STI2D architecture construction"), -- fermé 31/08/2019
( 211005, 0, 0, 210, 211, "1STI2D", "2112000411.", "Première STI2D énergies et environnement"), -- fermé 31/08/2019
( 211006, 0, 0, 210, 211, "1STI2D", "2112000511.", "Première STI2D innov.techno. eco concept."), -- fermé 31/08/2019
( 211007, 0, 0, 210, 211, "1STI2D", "2112000611.", "Première STI2D système info. et numérique"), -- fermé 31/08/2019
( 211008, 0, 0, 210, 211,   "1STL", "2112000711.", "Première STL biotechnologies"), -- fermé 31/08/2019
( 211009, 0, 0, 210, 211,   "1STL", "2112000811.", "Première STL sc.phys.chim. en laboratoire"),
( 211010, 0, 0, 210, 211,  "1STMG", "2113101611.", "Première STMG sc. & techno. management gestion"),
( 211011, 0, 0, 210, 211,  "1ST2S", "2113310411.", "Première ST2S sc. & techno. santé & social"),
( 211012, 0, 0, 210, 211,   "1HOT", "2113340111.", "Première technologique hôtellerie"),
( 211013, 0, 0, 210, 211,  "1STHR", "2113340211.", "Première STHR"),
( 211014, 0, 0, 210, 211, "1S2TMD", "2111330311.", "Première 1S2TMD sciences et techniques de la danse"),
( 211015, 0, 0, 210, 211, "1S2TMD", "2111330411.", "Première 1S2TMD sciences et techniques de la musique"),
( 211016, 0, 0, 210, 211, "1S2TMD", "2111330511.", "Première 1S2TMD sciences et techniques du théâtre"),
( 211017, 0, 0, 210, 211,   "1STL", "2112000911.", "Première STL biochimie-biologie-biotechnologie"),
( 211018, 0, 0, 210, 211, "1STI2D", "2112001011.", "Première 1STI2D sc. & techno. Ingénierie innovation développement durable"),

( 212000, 0, 1, 210, 212,    "TST", "212.....11.", "Terminale sciences et technologies"),
( 212001, 0, 0, 210, 212,   "TINS", "2121330111.", "Terminale musique option instrument"), -- fermé 31/08/2019
( 212002, 0, 0, 210, 212,   "TDAN", "2121330211.", "Terminale musique option danse"), -- fermé 31/08/2019
( 212003, 0, 0, 210, 212, "TSTD2A", "2121340311.", "Terminale STD2A sc.& tec.design-arts appliqués"),
( 212004, 0, 0, 210, 212, "TSTI2D", "2122000311.", "Terminale STI2D architecture construction"),
( 212005, 0, 0, 210, 212, "TSTI2D", "2122000411.", "Terminale STI2D énergies et environnement"),
( 212006, 0, 0, 210, 212, "TSTI2D", "2122000511.", "Terminale STI2D innov.techno. eco concept."),
( 212007, 0, 0, 210, 212, "TSTI2D", "2122000611.", "Terminale STI2D système info. et numérique"),
( 212008, 0, 0, 210, 212,   "TSTL", "2122000711.", "Terminale STL biotechnologies"), -- fermé 31/08/2019
( 212009, 0, 0, 210, 212,   "TSTL", "2122000811.", "Terminale STL sc.phys.chim. en laboratoire"),
( 212010, 0, 0, 210, 212,  "TSTMG", "2123101711.", "Terminale STMG gestion et finance"),
( 212011, 0, 0, 210, 212,  "TSTMG", "2123101811.", "Terminale STMG mercatique (marketing)"),
( 212012, 0, 0, 210, 212,  "TSTMG", "2123101911.", "Terminale STMG ressources humaines et communication"),
( 212013, 0, 0, 210, 212,  "TSTMG", "2123102011.", "Terminale STMG systèmes d’information de gestion"),
( 212014, 0, 0, 210, 212,  "TST2S", "2123310411.", "Terminale ST2S sc. & techno. santé & social"),
( 212015, 0, 0, 210, 212,   "THOT", "2123340111.", "Terminale technologique hôtellerie"),
( 212016, 0, 0, 210, 212,  "TSTHR", "2123340211.", "Terminale STHR sciences et techno. de l’hôtellerie et de la restauration"),
( 212017, 0, 0, 210, 212, "TS2TMD", "2121330311.", "Terminale S2TMD sciences et techniques de la danse"),
( 212018, 0, 0, 210, 212, "TS2TMD", "2121330411.", "Terminale S2TMD sciences et techniques de la musique"),
( 212019, 0, 0, 210, 212, "TS2TMD", "2121330511.", "Terminale S2TMD sciences et techniques du théâtre"),
( 212020, 0, 0, 210, 212,   "TSTL", "2122000911.", "Terminale STL biochimie-biologie-biotechnologie"),

( 213000, 0, 0, 210, 213,     "1T", "213.....11.", "Première technologique"),
( 213001, 0, 0, 210, 213,  "1STAV", "2132100511.", "Première STAV Sciences et Technologies de l’Agronomie et du Vivant"),

( 214000, 0, 0, 210, 214,     "TT", "214.....11.", "Terminale technologique"),
( 214001, 0, 0, 210, 214,  "TSTAV", "2142100411.", "Terminale STAV agronomie, alimentation, environnement, territoires"),

-- 220 BT (Brevet de Technicien) -- dispositif de formation 220~232

( 220000, 0, 1, 220, 220,    "2BT", "220.....11.", "Seconde BT"),
( 220001, 0, 0, 220, 220,    "2BT", "2202240611.", "Seconde BT dessin.arts appliqués : cristallerie"),
( 220002, 0, 0, 220, 220,    "2BT", "2202240711.", "Seconde BT dessin.arts appliqués : céramique"),
( 220003, 0, 0, 220, 220,    "2BT", "2202330411.", "Seconde BT dessin.arts appliqués : volum.archi"),
( 220004, 0, 0, 220, 220,    "2BT", "2202410511.", "Seconde BT dessin.arts appliqués : tapiss.lisse"),
( 220005, 0, 0, 220, 220,    "2BT", "2203230211.", "Seconde BT métiers de la musique"),

( 221000, 0, 1, 220, 221,    "1BT", "221.....11.", "Première BT"),
( 221001, 0, 0, 220, 221,    "1BT", "2212240611.", "Première BT dessin.arts appliqués : cristallerie"),
( 221002, 0, 0, 220, 221,    "1BT", "2212240711.", "Première BT dessin.arts appliqués : céramique"),
( 221003, 0, 0, 220, 221,    "1BT", "2212330411.", "Première BT dessin.arts appliqués : volum.archi"),
( 221004, 0, 0, 220, 221,    "1BT", "2212410511.", "Première BT dessin.arts appliqués : tapiss.lisse"),
( 221005, 0, 0, 220, 221,    "1BT", "2212421011.", "Première BT tec.met.spec : technq.habillag(dip)"),
( 221006, 0, 0, 220, 221,    "1BT", "2213230211.", "Première BT métiers de la musique"),
( 221007, 0, 0, 220, 221,    "1BT", "2213230511.", "Première BT brevet artist.techn. cirque (dip)"),
( 221008, 0, 0, 220, 221,    "1BT", "2213230611.", "Première BT tec.met.spec : machinist.const(dip)"),
( 221009, 0, 0, 220, 221,    "1BT", "2213310311.", "Première BT podo-orthésiste (dip)"),
( 221010, 0, 0, 220, 221,    "1BT", "2213310411.", "Première BT prothésiste-orthésiste (dip)"),

( 222000, 0, 1, 220, 222,    "TBT", "222.....11.", "Terminale BT"),
( 222001, 0, 0, 220, 222,    "TBT", "2222240611.", "Terminale BT dessin.arts appliqués : cristallerie"), -- fermé 31/08/2019
( 222002, 0, 0, 220, 222,    "TBT", "2222240711.", "Terminale BT dessin.arts appliqués : céramique"), -- fermé 31/08/2019
( 222003, 0, 0, 220, 222,    "TBT", "2222330411.", "Terminale BT dessin.arts appliqués : volum.archi"), -- fermé 31/08/2019
( 222004, 0, 0, 220, 222,    "TBT", "2222410511.", "Terminale BT dessin.arts appliqués : tapiss.lisse"), -- fermé 31/08/2019
( 222005, 0, 0, 220, 222,    "TBT", "2222421011.", "Terminale BT tec.met.spec : technq.habillag(dip)"),
( 222006, 0, 0, 220, 222,    "TBT", "2223220711.", "Terminale BT dessin.maquett.option A : arts graphiq"),
( 222007, 0, 0, 220, 222,    "TBT", "2223221011.", "Terminale BT dessin.maquett.option D : cartograph."),
( 222008, 0, 0, 220, 222,    "TBT", "2223230211.", "Terminale BT métiers de la musique"),
( 222009, 0, 0, 220, 222,    "TBT", "2223230511.", "Terminale BT brevet artist.techn. cirque (dip)"),
( 222010, 0, 0, 220, 222,    "TBT", "2223230611.", "Terminale BT tec.met.spec : machinist.const(dip)"),
( 222011, 0, 0, 220, 222,    "TBT", "2223310311.", "Terminale BT podo-orthésiste (dip)"),
( 222012, 0, 0, 220, 222,    "TBT", "2223310411.", "Terminale BT prothésiste-orthésiste (dip)"),

( 223000, 0, 0, 220, 223,   "1BTA", "223.....11.", "Première BTA"), -- fermé 31/08/2013

( 224000, 0, 0, 220, 224,   "TBTA", "224.....11.", "Terminale BTA"), -- fermé 31/08/2013

( 231000, 0, 0, 220, 231,   "1ADN", "231.....11.", "Première d’adaptation BTN"), -- fermé 31/08/2015 

( 232000, 0, 0, 220, 232,    "1AD", "232.....11.", "Première d’adaptation BT"),
( 232001, 0, 0, 220, 232,  "1ADBT", "2322240611.", "Première d’adaptation BT dessin.arts appliqués : cristallerie"),
( 232002, 0, 0, 220, 232,  "1ADBT", "2322240711.", "Première d’adaptation BT dessin.arts appliqués : céramique"),
( 232003, 0, 0, 220, 232,  "1ADBT", "2322330411.", "Première d’adaptation BT dessin.arts appliqués : volum.archi"),
( 232004, 0, 0, 220, 232,  "1ADBT", "2322410511.", "Première d’adaptation BT dessin.arts appliqués : tapiss.lisse"),

-- 240 CAP (Certificat d’Aptitude Professionnelle) en 1 an -- dispositif de formation 240

( 240000, 0, 1, 240, 240,  "1CAP1", "240.....11.", "CAP 1 an"),
( 240001, 0, 0, 240, 240,  "1CAP1", "2402010111.", "1CAP1 conducteur d’installations de production"),
( 240002, 0, 0, 240, 240,  "1CAP1", "2402100211.", "1CAP1 développement : gest.pte expl.rural -tom"),
( 240003, 0, 0, 240, 240,  "1CAP1", "2402130311.", "1CAP1 développement : gest. milieu marin -tom"),
( 240004, 0, 0, 240, 240,  "1CAP1", "2402130511.", "1CAP1 mareyage"),
( 240005, 0, 0, 240, 240,  "1CAP1", "2402130611.", "1CAP1 maritime matelot"),
( 240006, 0, 0, 240, 240,  "1CAP1", "2402200211.", "1CAP1 employé technique de laboratoire"),
( 240007, 0, 0, 240, 240,  "1CAP1", "2402200411.", "1CAP1 opérateur industries du recyclage"),
( 240008, 0, 0, 240, 240,  "1CAP1", "2402200511.", "1CAP1 propreté de l’environnement urbain - collecte et recyclage"),
( 240009, 0, 0, 240, 240,  "1CAP1", "2402212911.", "1CAP1 agent polyvalent de restauration"),
( 240010, 0, 0, 240, 240,  "1CAP1", "2402213011.", "1CAP1 charcutier-traiteur"),
( 240011, 0, 0, 240, 240,  "1CAP1", "2402213911.", "1CAP1 cuisine"),
( 240012, 0, 0, 240, 240,  "1CAP1", "2402213311.", "1CAP1 chocolatier confiseur"),
( 240013, 0, 0, 240, 240,  "1CAP1", "2402213511.", "1CAP1 boucher"),
( 240014, 0, 0, 240, 240,  "1CAP1", "2402214111.", "1CAP1 pâtissier"), -- MEF modifiée
( 240015, 0, 0, 240, 240,  "1CAP1", "2402213711.", "1CAP1 boulanger"),
( 240016, 0, 0, 240, 240,  "1CAP1", "2402213811.", "1CAP1 glacier-fabricant"),
( 240017, 0, 0, 240, 240,  "1CAP1", "2402220111.", "1CAP1 industries chimiques"),
( 240018, 0, 0, 240, 240,  "1CAP1", "2402232411.", "1CAP1 mouleur noyauteur : cuivre bronze"),
( 240019, 0, 0, 240, 240,  "1CAP1", "2402233211.", "1CAP1 doreur à la feuille ornemaniste"),
( 240020, 0, 0, 240, 240,  "1CAP1", "2402233911.", "1CAP1 lapidaire option A : diamant"),
( 240021, 0, 0, 240, 240,  "1CAP1", "2402234011.", "1CAP1 orfèvre option A : monteur en orfèvrerie"),
( 240022, 0, 0, 240, 240,  "1CAP1", "2402234111.", "1CAP1 orfèvre option B : tourneur repousseur en orfèvrerie"),
( 240023, 0, 0, 240, 240,  "1CAP1", "2402234211.", "1CAP1 orfèvre option C : polisseur aviveur en orfèvrerie"),
( 240024, 0, 0, 240, 240,  "1CAP1", "2402234311.", "1CAP1 lapidaire option B : pierres de couleur"),
( 240025, 0, 0, 240, 240,  "1CAP1", "2402234911.", "1CAP1 bronzier option A : monteur en bronze"),
( 240026, 0, 0, 240, 240,  "1CAP1", "2402235011.", "1CAP1 bronzier option B : ciseleur sur bronze"),
( 240027, 0, 0, 240, 240,  "1CAP1", "2402235111.", "1CAP1 bronzier option C : tourneur sur bronze"),
( 240028, 0, 0, 240, 240,  "1CAP1", "2402235311.", "1CAP1 émailleur d’art sur metaux"),
( 240029, 0, 0, 240, 240,  "1CAP1", "2402235411.", "1CAP1 orfèvre option D : planeur en orfèvrerie"),
( 240030, 0, 0, 240, 240,  "1CAP1", "2402235611.", "1CAP1 facteur d’orgues"),
( 240031, 0, 0, 240, 240,  "1CAP1", "2402236011.", "1CAP1 métiers de la fonderie"),
( 240032, 0, 0, 240, 240,  "1CAP1", "2402236111.", "1CAP1 art & techniques bijouterie-joaillerie op. bijouterie-joaillerie"),
( 240033, 0, 0, 240, 240,  "1CAP1", "2402236211.", "1CAP1 art & techniques bijouterie-joaillerie op. bijouterie-sertissage"),
( 240034, 0, 0, 240, 240,  "1CAP1", "2402236311.", "1CAP1 art & techniques bijouterie-joaillerie op. polissage-finition"),
( 240035, 0, 0, 240, 240,  "1CAP1", "2402240611.", "1CAP1 agent maintenance industries matériaux construction & connexes"),
( 240036, 0, 0, 240, 240,  "1CAP1", "2402241911.", "1CAP1 fabrication industrielle des céramiques"),
( 240037, 0, 0, 240, 240,  "1CAP1", "2402242011.", "1CAP1 modèles et moules céramiques"),
( 240038, 0, 0, 240, 240,  "1CAP1", "2402242111.", "1CAP1 tournage en céramique"),
( 240039, 0, 0, 240, 240,  "1CAP1", "2402242211.", "1CAP1 décoration en céramique"),
( 240040, 0, 0, 240, 240,  "1CAP1", "2402242511.", "1CAP1 arts et techniques du verre option vitrailliste"),
( 240041, 0, 0, 240, 240,  "1CAP1", "2402242711.", "1CAP1 arts et techniques du verre option décorateur"),
( 240042, 0, 0, 240, 240,  "1CAP1", "2402242811.", "1CAP1 arts du verre et du cristal"),
( 240043, 0, 0, 240, 240,  "1CAP1", "2402242911.", "1CAP1 souffleur de verre option enseigne lumineuse"),
( 240044, 0, 0, 240, 240,  "1CAP1", "2402243011.", "1CAP1 souffleur de verre option verrerie scientifique"),
( 240045, 0, 0, 240, 240,  "1CAP1", "2402250511.", "1CAP1 mise en oeuvre des caoutchoucs & des élastomères thermoplastiques"),
( 240046, 0, 0, 240, 240,  "1CAP1", "2402250911.", "1CAP1 plasturgie"),
( 240047, 0, 0, 240, 240,  "1CAP1", "2402251011.", "1CAP1 composites, plastiques chaudronnés"),
( 240048, 0, 0, 240, 240,  "1CAP1", "2402260911.", "1CAP1 mécanicien conducteur scieries et industries mécaniques du bois"),
( 240049, 0, 0, 240, 240,  "1CAP1", "2402270611.", "1CAP1 froid et climatisation"),
( 240050, 0, 0, 240, 240,  "1CAP1", "2402271511.", "1CAP1 monteur installations thermiques"), -- MEF modifiée en 2019
( 240051, 0, 0, 240, 240,  "1CAP1", "2402271411.", "1CAP1 installateur en froid et conditionnement d’air"),
( 240052, 0, 0, 240, 240,  "1CAP1", "2402300611.", "1CAP1 intervention en maintenance technique des bâtiments"), -- MEF modifiée en 2022
( 240053, 0, 0, 240, 240,  "1CAP1", "2402300511.", "1CAP1 monteur de structures mobiles"),
( 240054, 0, 0, 240, 240,  "1CAP1", "2402312011.", "1CAP1 constructeur de routes et d’aménagements urbains"), -- MEF modifiée en 2022
( 240055, 0, 0, 240, 240,  "1CAP1", "2402311911.", "1CAP1 constructeur de réseaux de canalisations de travaux publics"), -- MEF modifiée en 2019
( 240056, 0, 0, 240, 240,  "1CAP1", "2402311711.", "1CAP1 constructeur en ouvrages d’art"),
( 240057, 0, 0, 240, 240,  "1CAP1", "2402311811.", "1CAP1 conducteur d’engins : travaux publics et carrières"),
( 240058, 0, 0, 240, 240,  "1CAP1", "2402320411.", "1CAP1 graveur sur pierre"),
( 240059, 0, 0, 240, 240,  "1CAP1", "2402321211.", "1CAP1 développement : const.entret.batimt -tom"),
( 240060, 0, 0, 240, 240,  "1CAP1", "2402322411.", "1CAP1 maçon"), -- MEF modifiée en 2022
( 240061, 0, 0, 240, 240,  "1CAP1", "2402322311.", "1CAP1 couvreur"), -- MEF modifiée en 2022
( 240062, 0, 0, 240, 240,  "1CAP1", "2402322211.", "1CAP1 constructeur d’ouvrages en béton armé"), -- MEF modifiée en 2020
( 240063, 0, 0, 240, 240,  "1CAP1", "2402322011.", "1CAP1 tailleur de pierre"),
( 240064, 0, 0, 240, 240,  "1CAP1", "2402322111.", "1CAP1 marbrier du bâtiment et de la décoration"),
( 240065, 0, 0, 240, 240,  "1CAP1", "2402330211.", "1CAP1 monteur en isolation thermique et acoustique"),
( 240066, 0, 0, 240, 240,  "1CAP1", "2402331111.", "1CAP1 staffeur ornemaniste"),
( 240067, 0, 0, 240, 240,  "1CAP1", "2402332411.", "1CAP1 monteur en installations sanitaires"), -- MEF modifiée en 2019
( 240068, 0, 0, 240, 240,  "1CAP1", "2402332511.", "1CAP1 carreleur mosaïste"), -- MEF modifiée en 2020
( 240069, 0, 0, 240, 240,  "1CAP1", "2402332711.", "1CAP1 peintre applicateur de revêtements"), -- MEF modifiée en 2020
( 240070, 0, 0, 240, 240,  "1CAP1", "2402332011.", "1CAP1 solier-moquettiste"),
( 240071, 0, 0, 240, 240,  "1CAP1", "2402332111.", "1CAP1 étancheur du bâtiment et des travaux publics"),
( 240072, 0, 0, 240, 240,  "1CAP1", "2402332211.", "1CAP1 menuisier aluminium-verre"),
( 240073, 0, 0, 240, 240,  "1CAP1", "2402332611.", "1CAP1 métiers du plâtre et de l’isolation"), -- MEF modifiée en 2020
( 240074, 0, 0, 240, 240,  "1CAP1", "2402341111.", "1CAP1 menuisier en sièges"),
( 240075, 0, 0, 240, 240,  "1CAP1", "2402342011.", "1CAP1 cannage et paillage en ameublement"),
( 240076, 0, 0, 240, 240,  "1CAP1", "2402342711.", "1CAP1 ouvrier archetier"),
( 240077, 0, 0, 240, 240,  "1CAP1", "2402342811.", "1CAP1 lutherie"),
( 240078, 0, 0, 240, 240,  "1CAP1", "2402343011.", "1CAP1 arts du bois option A : sculpteur ornemaniste"),
( 240079, 0, 0, 240, 240,  "1CAP1", "2402343111.", "1CAP1 arts du bois option B : tourneur"),
( 240080, 0, 0, 240, 240,  "1CAP1", "2402343211.", "1CAP1 arts du bois option C : marqueteur"),
( 240081, 0, 0, 240, 240,  "1CAP1", "2402343311.", "1CAP1 encadreur"),
( 240082, 0, 0, 240, 240,  "1CAP1", "2402343511.", "1CAP1 tonnellerie"),
( 240083, 0, 0, 240, 240,  "1CAP1", "2402344511.", "1CAP1 ébéniste"),
( 240084, 0, 0, 240, 240,  "1CAP1", "2402343811.", "1CAP1 vannerie"),
( 240085, 0, 0, 240, 240,  "1CAP1", "2402344611.", "1CAP1 charpentier bois"), -- MEF modifiée en 2020
( 240086, 0, 0, 240, 240,  "1CAP1", "2402344011.", "1CAP1 constructeur bois"),
( 240087, 0, 0, 240, 240,  "1CAP1", "2402344711.", "1CAP1 menuisier fabricant"), -- MEF modifiée en 2022
( 240088, 0, 0, 240, 240,  "1CAP1", "2402344911.", "1CAP1 menuisier installateur"), -- MEF modifiée en 2022
( 240089, 0, 0, 240, 240,  "1CAP1", "2402344311.", "1CAP1 conducteur-opérateur de scierie"),
( 240090, 0, 0, 240, 240,  "1CAP1", "2402344411.", "1CAP1 charpentier de marine"),
( 240091, 0, 0, 240, 240,  "1CAP1", "2402400811.", "1CAP1 métiers de l’entretien des textiles option B : pressing"), -- MEF modifiée en 2022
( 240092, 0, 0, 240, 240,  "1CAP1", "2402400711.", "1CAP1 métiers de l’entretien des textiles option A : blanchisserie"), -- MEF modifiée en 2022
( 240093, 0, 0, 240, 240,  "1CAP1", "2402412511.", "1CAP1 rentrayeur option A : tapis"),
( 240094, 0, 0, 240, 240,  "1CAP1", "2402412611.", "1CAP1 rentrayeur option B : tapisseries"),
( 240095, 0, 0, 240, 240,  "1CAP1", "2402412711.", "1CAP1 arts de la broderie"),
( 240096, 0, 0, 240, 240,  "1CAP1", "2402413011.", "1CAP1 arts de la dentelle option fuseaux"),
( 240097, 0, 0, 240, 240,  "1CAP1", "2402413111.", "1CAP1 arts de la dentelle option aiguille"),
( 240098, 0, 0, 240, 240,  "1CAP1", "2402423111.", "1CAP1 arts du tapis et de la tapisserie de lisse"),
( 240099, 0, 0, 240, 240,  "1CAP1", "2402423811.", "1CAP1 tapissier-tapissière d’ameublement en siège"),
( 240100, 0, 0, 240, 240,  "1CAP1", "2402423911.", "1CAP1 tapissier-tapissière d’ameublement en décoration"),
( 240101, 0, 0, 240, 240,  "1CAP1", "2402424011.", "1CAP1 métiers de la mode - vêtement flou"),
( 240102, 0, 0, 240, 240,  "1CAP1", "2402424111.", "1CAP1 métiers de la mode - vêtement tailleur"),
( 240103, 0, 0, 240, 240,  "1CAP1", "2402424211.", "1CAP1 métiers de la mode : chapelier-modiste"),
( 240104, 0, 0, 240, 240,  "1CAP1", "2402431311.", "1CAP1 cordonnier bottier"),
( 240105, 0, 0, 240, 240,  "1CAP1", "2402431511.", "1CAP1 fourrure"),
( 240106, 0, 0, 240, 240,  "1CAP1", "2402431611.", "1CAP1 chaussure"),
( 240107, 0, 0, 240, 240,  "1CAP1", "2402432211.", "1CAP1 maroquinerie"),
( 240108, 0, 0, 240, 240,  "1CAP1", "2402431811.", "1CAP1 sellerie générale"),
( 240109, 0, 0, 240, 240,  "1CAP1", "2402431911.", "1CAP1 vêtement de peau"),
( 240110, 0, 0, 240, 240,  "1CAP1", "2402432011.", "1CAP1 cordonnerie multiservice"),
( 240111, 0, 0, 240, 240,  "1CAP1", "2402432111.", "1CAP1 sellier harnacheur"),
( 240112, 0, 0, 240, 240,  "1CAP1", "2402500111.", "1CAP1 certif.aptitude au developt -tom"),
( 240113, 0, 0, 240, 240,  "1CAP1", "2402510811.", "1CAP1 instruments coupants et de chirurgie"),
( 240114, 0, 0, 240, 240,  "1CAP1", "2402512311.", "1CAP1 décolletage : opérateur régleur en décolletage"),
( 240115, 0, 0, 240, 240,  "1CAP1", "2402512611.", "1CAP1 outillages en outils à découper et à emboutir"),
( 240116, 0, 0, 240, 240,  "1CAP1", "2402513011.", "1CAP1 développement : fabric.entret.meca. -tom"),
( 240117, 0, 0, 240, 240,  "1CAP1", "2402513311.", "1CAP1 agent vérificateur d’appareils extincteurs"),
( 240118, 0, 0, 240, 240,  "1CAP1", "2402513411.", "1CAP1 transport par câbles et remontées mécaniques"),
( 240119, 0, 0, 240, 240,  "1CAP1", "2402513611.", "1CAP1 armurerie (fabrication et réparation)"),
( 240120, 0, 0, 240, 240,  "1CAP1", "2402513711.", "1CAP1 horlogerie"),
( 240121, 0, 0, 240, 240,  "1CAP1", "2402522111.", "1CAP1 maintenance des matériels option A matériels agricoles"),
( 240122, 0, 0, 240, 240,  "1CAP1", "2402522211.", "1CAP1 maintenance des matériels option B matériels construction manutention"),
( 240123, 0, 0, 240, 240,  "1CAP1", "2402522311.", "1CAP1 maintenance des matériels option C matériels d’espaces verts"),
( 240124, 0, 0, 240, 240,  "1CAP1", "2402521711.", "1CAP1 réparation entretien des embarcations de plaisance"),
( 240125, 0, 0, 240, 240,  "1CAP1", "2402521811.", "1CAP1 maintenance des véhicules option A voitures particulières"),
( 240126, 0, 0, 240, 240,  "1CAP1", "2402521911.", "1CAP1 maintenance des véhicules option B véhicules de transport routier"),
( 240127, 0, 0, 240, 240,  "1CAP1", "2402522011.", "1CAP1 maintenance des véhicules option C motocycles"),
( 240128, 0, 0, 240, 240,  "1CAP1", "2402530511.", "1CAP1 aéronautique option avionique"),
( 240129, 0, 0, 240, 240,  "1CAP1", "2402530611.", "1CAP1 aéronautique option systèmes"),
( 240130, 0, 0, 240, 240,  "1CAP1", "2402530711.", "1CAP1 aéronautique option structures"),
( 240131, 0, 0, 240, 240,  "1CAP1", "2402542111.", "1CAP1 mise en forme des matériaux"),
( 240132, 0, 0, 240, 240,  "1CAP1", "2402542311.", "1CAP1 ferronnier"),
( 240133, 0, 0, 240, 240,  "1CAP1", "2402542511.", "1CAP1 outillages en moules métalliques"),
( 240134, 0, 0, 240, 240,  "1CAP1", "2402542711.", "1CAP1 tuyautier en orgues"),
( 240135, 0, 0, 240, 240,  "1CAP1", "2402543911.", "1CAP1 métallier"), -- MEF modifiée en 2022
( 240136, 0, 0, 240, 240,  "1CAP1", "2402543211.", "1CAP1 construction des carrosseries"),
( 240137, 0, 0, 240, 240,  "1CAP1", "2402543311.", "1CAP1 peinture en carrosserie"),
( 240138, 0, 0, 240, 240,  "1CAP1", "2402543411.", "1CAP1 réparation des carrosseries"),
( 240139, 0, 0, 240, 240,  "1CAP1", "2402543511.", "1CAP1 réalisat. en chaudronnerie indus."),
( 240140, 0, 0, 240, 240,  "1CAP1", "2402543611.", "1CAP1 ferronnier d’art"),
( 240141, 0, 0, 240, 240,  "1CAP1", "2402552111.", "1CAP1 métiers de l’enseigne et de la signalétique"),
( 240142, 0, 0, 240, 240,  "1CAP1", "2402552411.", "1CAP1 électricien"), -- MEF modifiée en 2019
( 240143, 0, 0, 240, 240,  "1CAP1", "2403110711.", "1CAP1 emballeur professionnel"),
( 240144, 0, 0, 240, 240,  "1CAP1", "2403111411.", "1CAP1 agent d’accueil et de conduite routière - transport de voyageurs"),
( 240145, 0, 0, 240, 240,  "1CAP1", "2403111611.", "1CAP1 agent entreposage et messagerie"),
( 240146, 0, 0, 240, 240,  "1CAP1", "2403111711.", "1CAP1 conducteur routier de marchandises"),
( 240147, 0, 0, 240, 240,  "1CAP1", "2403111811.", "1CAP1 conducteur livreur de marchandises"),
( 240148, 0, 0, 240, 240,  "1CAP1", "2403111911.", "1CAP1 déménageur sur véhicule utilitaire léger"),
( 240149, 0, 0, 240, 240,  "1CAP1", "2403112311.", "1CAP1 opérateur/opératrice de service-relation client et livraison"),
( 240150, 0, 0, 240, 240,  "1CAP1", "2403112111.", "1CAP1 transport fluvial"),
( 240151, 0, 0, 240, 240,  "1CAP1", "2403112211.", "1CAP1 opérateur/opératrice logistique"),
( 240152, 0, 0, 240, 240,  "1CAP1", "2403121411.", "1CAP1 employé de commerce multi-spécialités"),
( 240153, 0, 0, 240, 240,  "1CAP1", "2403121511.", "1CAP1 employé de vente spécialisé option A produits alimentaires"),
( 240154, 0, 0, 240, 240,  "1CAP1", "2403121611.", "1CAP1 employé de vente spécialisé option B produits d’équipement courant"),
( 240155, 0, 0, 240, 240,  "1CAP1", "2403121711.", "1CAP1 vendeur-magasinier pièces de rechange & équipements automobiles"),
( 240156, 0, 0, 240, 240,  "1CAP1", "2403122311.", "1CAP1 fleuriste"), -- MEF modifiée en 2019
( 240157, 0, 0, 240, 240,  "1CAP1", "2403121911.", "1CAP1 employé de vente spécialisé option C service à la clientèle"),
( 240158, 0, 0, 240, 240,  "1CAP1", "2403122011.", "1CAP1 employé de vente spécialisé option D librairie-papeterie-presse"),
( 240159, 0, 0, 240, 240,  "1CAP1", "2403122111.", "1CAP1 poissonnier écailler"),
( 240160, 0, 0, 240, 240,  "1CAP1", "2403222811.", "1CAP1 arts de la reliure"),
( 240161, 0, 0, 240, 240,  "1CAP1", "2403222011.", "1CAP1 métiers de la gravure option A : gravure d’ornementation"),
( 240162, 0, 0, 240, 240,  "1CAP1", "2403222111.", "1CAP1 métiers de la gravure option B : gravure d’impression"),
( 240163, 0, 0, 240, 240,  "1CAP1", "2403222211.", "1CAP1 métiers de la gravure option C : gravure en modèle"),
( 240164, 0, 0, 240, 240,  "1CAP1", "2403222311.", "1CAP1 métiers de la gravure option D : marquage poinçonnage"),
( 240165, 0, 0, 240, 240,  "1CAP1", "2403222511.", "1CAP1 sérigraphie industrielle"),
( 240166, 0, 0, 240, 240,  "1CAP1", "2403222711.", "1CAP1 signalétique et décors graphiques"),
( 240167, 0, 0, 240, 240,  "1CAP1", "2403230611.", "1CAP1 monteur en chapiteaux"),
( 240168, 0, 0, 240, 240,  "1CAP1", "2403230711.", "1CAP1 accessoiriste réalisateur"),
( 240169, 0, 0, 240, 240,  "1CAP1", "2403230811.", "1CAP1 accordeur de piano"),
( 240170, 0, 0, 240, 240,  "1CAP1", "2403231011.", "1CAP1 assistant technique en instruments de musique option accordéon"),
( 240171, 0, 0, 240, 240,  "1CAP1", "2403231111.", "1CAP1 assistant technique instruments musique option instruments à vent"),
( 240172, 0, 0, 240, 240,  "1CAP1", "2403231211.", "1CAP1 assistant technique en instruments de musique option piano"),
( 240173, 0, 0, 240, 240,  "1CAP1", "2403231311.", "1CAP1 assistant technique en instruments de musique option guitare"),
( 240174, 0, 0, 240, 240,  "1CAP1", "2403231411.", "1CAP1 opérateur projectionniste cinéma"),
( 240175, 0, 0, 240, 240,  "1CAP1", "2403310611.", "1CAP1 ortho-prothésiste"),
( 240176, 0, 0, 240, 240,  "1CAP1", "2403310711.", "1CAP1 podo-orthésiste"),
( 240177, 0, 0, 240, 240,  "1CAP1", "2403320111.", "1CAP1 développement : act.famil.artis.tour. -tom"),
( 240178, 0, 0, 240, 240,  "1CAP1", "2403320511.", "1CAP1 accompagnant éducatif petite enfance"), -- MEF modifiée en 2022
( 240179, 0, 0, 240, 240,  "1CAP1", "2403341211.", "1CAP1 commercialisation et services en hôtel-café-restaurant"),
( 240180, 0, 0, 240, 240,  "1CAP1", "2403340911.", "1CAP1 restaurant"), -- fermé
( 240181, 0, 0, 240, 240,  "1CAP1", "2403341011.", "1CAP1 services en brasserie-café"), -- fermé
( 240182, 0, 0, 240, 240,  "1CAP1", "2403341111.", "1CAP1 assistant(e) technique en milieux familial et collectif"),
( 240183, 0, 0, 240, 240,  "1CAP1", "2403350111.", "1CAP1 métiers du football"),
( 240184, 0, 0, 240, 240,  "1CAP1", "2403361711.", "1CAP1 taxidermiste"), -- MEF modifiée en 2020
( 240185, 0, 0, 240, 240,  "1CAP1", "2403361611.", "1CAP1 métiers de la coiffure"), -- MEF modifiée en 2020
( 240186, 0, 0, 240, 240,  "1CAP1", "2403361511.", "1CAP1 esthétique cosmétique parfumerie"), -- MEF modifiée en 2019
( 240187, 0, 0, 240, 240,  "1CAP1", "2403400211.", "1CAP1 agent de prévention et de médiation"),
( 240188, 0, 0, 240, 240,  "1CAP1", "2403430111.", "1CAP1 agent de la qualité de l’eau"),
( 240189, 0, 0, 240, 240,  "1CAP1", "2403430311.", "1CAP1 agent d’assainissement et de collecte déchets liquides spéciaux"),
( 240190, 0, 0, 240, 240,  "1CAP1", "2403430511.", "1CAP1 gestion déchets propreté urbaine"),
( 240191, 0, 0, 240, 240,  "1CAP1", "2403430711.", "1CAP1 agent de propreté et d’hygiène"),
( 240192, 0, 0, 240, 240,  "1CAP1", "2403440411.", "1CAP1 gardien d’immeuble"),
( 240193, 0, 0, 240, 240,  "1CAP1", "2403440511.", "1CAP1 agent de sécurité"),
( 240194, 0, 0, 240, 240,  "1CAP1", "2402543711.", "1CAP1 réalisations industrielles option A chaudronnerie"),
( 240195, 0, 0, 240, 240,  "1CAP1", "2402543811.", "1CAP1 réalisations industrielles option B soudage"),
( 240196, 0, 0, 240, 240,  "1CAP1", "2402214011.", "1CAP1 crémier-fromager"),
( 240197, 0, 0, 240, 240,  "1CAP1", "2403122211.", "1CAP1 primeur"),
( 240198, 0, 0, 240, 240,  "1CAP1", "2402214211.", "1CAP1 production/service en restauration rapide, collective, cafétéria"),
( 240199, 0, 0, 240, 240,  "1CAP1", "2403122411.", "1CAP1 équipier polyvalent du commerce"),
( 240200, 0, 0, 240, 240,  "1CAP1", "2402345011.", "1CAP1 assistant luthier du quatuor"),

-- 241 CAP (Certificat d’Aptitude Professionnelle) en 2 ans -- dispositif de formation 241

( 241001, 0, 1, 241, 241,  "1CAP2", "241.....21.", "CAP 2 ans, 1e année"),
( 241002, 0, 1, 241, 241,  "2CAP2", "241.....22.", "CAP 2 ans, 2e année"),
( 241003, 0, 0, 241, 241,  "1CAP2", "2412010121.", "1CAP2 conducteur d’installations de production"),
( 241004, 0, 0, 241, 241,  "2CAP2", "2412010122.", "2CAP2 conducteur d’installations de production"),
( 241005, 0, 0, 241, 241,  "1CAP2", "2412100221.", "1CAP2 développement : gest.pte expl.rural -tom"),
( 241006, 0, 0, 241, 241,  "2CAP2", "2412100222.", "2CAP2 développement : gest.pte expl.rural -tom"),
( 241007, 0, 0, 241, 241,  "1CAP2", "2412130321.", "1CAP2 développement : gest. milieu marin -tom"),
( 241008, 0, 0, 241, 241,  "2CAP2", "2412130322.", "2CAP2 développement : gest. milieu marin -tom"),
( 241009, 0, 0, 241, 241,  "1CAP2", "2412130521.", "1CAP2 mareyage"),
( 241010, 0, 0, 241, 241,  "2CAP2", "2412130522.", "2CAP2 mareyage"),
( 241011, 0, 0, 241, 241,  "1CAP2", "2412130821.", "1CAP2 maritime"), -- MEF modifiée en 2019
( 241012, 0, 0, 241, 241,  "2CAP2", "2412130822.", "2CAP2 maritime"), -- MEF modifiée en 2020
( 241013, 0, 0, 241, 241,  "1CAP2", "2412200221.", "1CAP2 employé technique de laboratoire"),
( 241014, 0, 0, 241, 241,  "2CAP2", "2412200222.", "2CAP2 employé technique de laboratoire"),
( 241015, 0, 0, 241, 241,  "2CAP2", "2412200422.", "2CAP2 opérateur industries du recyclage"),
( 241016, 0, 0, 241, 241,  "1CAP2", "2412200521.", "1CAP2 propreté de l’environnement urbain - collecte et recyclage"),
( 241017, 0, 0, 241, 241,  "2CAP2", "2412200522.", "2CAP2 propreté de l’environnement urbain - collecte et recyclage"),
( 241018, 0, 0, 241, 241,  "1CAP2", "2412212921.", "1CAP2 agent polyvalent de restauration"),
( 241019, 0, 0, 241, 241,  "2CAP2", "2412212922.", "2CAP2 agent polyvalent de restauration"),
( 241020, 0, 0, 241, 241,  "1CAP2", "2412213021.", "1CAP2 charcutier-traiteur"),
( 241021, 0, 0, 241, 241,  "2CAP2", "2412213022.", "2CAP2 charcutier-traiteur"),
( 241022, 0, 0, 241, 241,  "1CAP2", "2412213921.", "1CAP2 cuisine"), -- MEF modifiée en 2016
( 241023, 0, 0, 241, 241,  "2CAP2", "2412213922.", "2CAP2 cuisine"), -- MEF modifiée en 2017
( 241024, 0, 0, 241, 241,  "1CAP2", "2412213321.", "1CAP2 chocolatier confiseur"),
( 241025, 0, 0, 241, 241,  "2CAP2", "2412213322.", "2CAP2 chocolatier confiseur"),
( 241026, 0, 0, 241, 241,  "1CAP2", "2412213521.", "1CAP2 boucher"),
( 241027, 0, 0, 241, 241,  "2CAP2", "2412213522.", "2CAP2 boucher"),
( 241028, 0, 0, 241, 241,  "1CAP2", "2412214121.", "1CAP2 pâtissier"), -- MEF modifiée en 2019
( 241029, 0, 0, 241, 241,  "2CAP2", "2412214122.", "2CAP2 pâtissier"), -- MEF modifiée en 2020
( 241030, 0, 0, 241, 241,  "1CAP2", "2412213721.", "1CAP2 boulanger"),
( 241031, 0, 0, 241, 241,  "2CAP2", "2412213722.", "2CAP2 boulanger"),
( 241032, 0, 0, 241, 241,  "1CAP2", "2412213821.", "1CAP2 glacier-fabricant"),
( 241033, 0, 0, 241, 241,  "2CAP2", "2412213822.", "2CAP2 glacier-fabricant"),
( 241034, 0, 0, 241, 241,  "1CAP2", "2412220121.", "1CAP2 industries chimiques"),
( 241035, 0, 0, 241, 241,  "2CAP2", "2412220122.", "2CAP2 industries chimiques"),
( 241036, 0, 0, 241, 241,  "1CAP2", "2412232421.", "1CAP2 mouleur noyauteur : cuivre bronze"),
( 241037, 0, 0, 241, 241,  "2CAP2", "2412232422.", "2CAP2 mouleur noyauteur : cuivre bronze"),
( 241038, 0, 0, 241, 241,  "1CAP2", "2412233221.", "1CAP2 doreur à la feuille ornemaniste"),
( 241039, 0, 0, 241, 241,  "2CAP2", "2412233222.", "2CAP2 doreur à la feuille ornemaniste"),
( 241040, 0, 0, 241, 241,  "1CAP2", "2412233921.", "1CAP2 lapidaire option A : diamant"),
( 241041, 0, 0, 241, 241,  "2CAP2", "2412233922.", "2CAP2 lapidaire option A : diamant"),
( 241042, 0, 0, 241, 241,  "1CAP2", "2412234021.", "1CAP2 orfèvre option A : monteur orfèvrerie"),
( 241043, 0, 0, 241, 241,  "2CAP2", "2412234022.", "2CAP2 orfèvre option A : monteur orfèvrerie"),
( 241044, 0, 0, 241, 241,  "1CAP2", "2412234121.", "1CAP2 orfèvre option B : tourn.repousseur"),
( 241045, 0, 0, 241, 241,  "2CAP2", "2412234122.", "2CAP2 orfèvre option B : tourn.repousseur"),
( 241046, 0, 0, 241, 241,  "1CAP2", "2412234221.", "1CAP2 orfèvre option C : polisseur aviveur"),
( 241047, 0, 0, 241, 241,  "2CAP2", "2412234222.", "2CAP2 orfèvre option C : polisseur aviveur"),
( 241048, 0, 0, 241, 241,  "1CAP2", "2412234321.", "1CAP2 lapidaire option B : pierres de coul."),
( 241049, 0, 0, 241, 241,  "2CAP2", "2412234322.", "2CAP2 lapidaire option B : pierres de coul."),
( 241050, 0, 0, 241, 241,  "1CAP2", "2412234921.", "1CAP2 bronzier option A : monteur en bronze"),
( 241051, 0, 0, 241, 241,  "2CAP2", "2412234922.", "2CAP2 bronzier option A : monteur en bronze"),
( 241052, 0, 0, 241, 241,  "1CAP2", "2412235021.", "1CAP2 bronzier option B : ciseleur sur bronze"),
( 241053, 0, 0, 241, 241,  "2CAP2", "2412235022.", "2CAP2 bronzier option B : ciseleur sur bronze"),
( 241054, 0, 0, 241, 241,  "1CAP2", "2412235121.", "1CAP2 bronzier option C : tourneur sur bronze"),
( 241055, 0, 0, 241, 241,  "2CAP2", "2412235122.", "2CAP2 bronzier option C : tourneur sur bronze"),
( 241056, 0, 0, 241, 241,  "1CAP2", "2412235321.", "1CAP2 émailleur d’art sur metaux"),
( 241057, 0, 0, 241, 241,  "2CAP2", "2412235322.", "2CAP2 émailleur d’art sur metaux"),
( 241058, 0, 0, 241, 241,  "1CAP2", "2412235421.", "1CAP2 orfèvre option D : planeur en orfèvre"),
( 241059, 0, 0, 241, 241,  "2CAP2", "2412235422.", "2CAP2 orfèvre option D : planeur en orfèvre"),
( 241060, 0, 0, 241, 241,  "2CAP2", "2412235622.", "2CAP2 facteur d’orgues"),
( 241061, 0, 0, 241, 241,  "1CAP2", "2412236021.", "1CAP2 métiers de la fonderie"),
( 241062, 0, 0, 241, 241,  "2CAP2", "2412236022.", "2CAP2 métiers de la fonderie"),
( 241063, 0, 0, 241, 241,  "1CAP2", "2412236121.", "1CAP2 art tec. bij. joaill.option bij.joail"),
( 241064, 0, 0, 241, 241,  "2CAP2", "2412236122.", "2CAP2 art tec. bij. joaill.option bij.joail"),
( 241065, 0, 0, 241, 241,  "1CAP2", "2412236221.", "1CAP2 art tec bij.joaill.option bij.sertis."),
( 241066, 0, 0, 241, 241,  "2CAP2", "2412236222.", "2CAP2 art tec bij.joaill.option bij.sertis."),
( 241067, 0, 0, 241, 241,  "1CAP2", "2412236321.", "1CAP2 art tec bij.joaill.op.polis.fini."),
( 241068, 0, 0, 241, 241,  "2CAP2", "2412236322.", "2CAP2 art tec bij.joaill.op.polis.fini."),
( 241069, 0, 0, 241, 241,  "1CAP2", "2412240621.", "1CAP2 maint.indust.materx de construct."),
( 241070, 0, 0, 241, 241,  "2CAP2", "2412240622.", "2CAP2 maint.indust.materx de construct."),
( 241071, 0, 0, 241, 241,  "1CAP2", "2412241921.", "1CAP2 fabric.industriel.des céramiques"),
( 241072, 0, 0, 241, 241,  "2CAP2", "2412241922.", "2CAP2 fabric.industriel.des céramiques"),
( 241073, 0, 0, 241, 241,  "1CAP2", "2412242021.", "1CAP2 modèles et moules céramiques"),
( 241074, 0, 0, 241, 241,  "2CAP2", "2412242022.", "2CAP2 modèles et moules céramiques"),
( 241075, 0, 0, 241, 241,  "1CAP2", "2412242121.", "1CAP2 tournage en céramique"),
( 241076, 0, 0, 241, 241,  "2CAP2", "2412242122.", "2CAP2 tournage en céramique"),
( 241077, 0, 0, 241, 241,  "1CAP2", "2412242221.", "1CAP2 décoration en céramique"),
( 241078, 0, 0, 241, 241,  "2CAP2", "2412242222.", "2CAP2 décoration en céramique"),
( 241079, 0, 0, 241, 241,  "1CAP2", "2412242521.", "1CAP2 arts & tech.verre : vitrailliste"),
( 241080, 0, 0, 241, 241,  "2CAP2", "2412242522.", "2CAP2 arts & tech.verre : vitrailliste"),
( 241081, 0, 0, 241, 241,  "1CAP2", "2412242721.", "1CAP2 arts & tech.verre : décorateur"),
( 241082, 0, 0, 241, 241,  "2CAP2", "2412242722.", "2CAP2 arts & tech.verre : décorateur"),
( 241083, 0, 0, 241, 241,  "1CAP2", "2412242821.", "1CAP2 arts du verre et du cristal"),
( 241084, 0, 0, 241, 241,  "2CAP2", "2412242822.", "2CAP2 arts du verre et du cristal"),
( 241085, 0, 0, 241, 241,  "1CAP2", "2412242921.", "1CAP2 souffleur verre : ens.lumineuse"),
( 241086, 0, 0, 241, 241,  "2CAP2", "2412242922.", "2CAP2 souffleur verre : ens.lumineuse"),
( 241087, 0, 0, 241, 241,  "1CAP2", "2412243021.", "1CAP2 souffl.verre : verrerie scient."),
( 241088, 0, 0, 241, 241,  "2CAP2", "2412243022.", "2CAP2 souffl.verre : verrerie scient."),
( 241089, 0, 0, 241, 241,  "1CAP2", "2412250521.", "1CAP2 mise en oeuv.caoutchoucs & elasto"),
( 241090, 0, 0, 241, 241,  "2CAP2", "2412250522.", "2CAP2 mise en oeuv.caoutchoucs & elasto"),
( 241091, 0, 0, 241, 241,  "1CAP2", "2412250921.", "1CAP2 plasturgie"),
( 241092, 0, 0, 241, 241,  "2CAP2", "2412250922.", "2CAP2 plasturgie"),
( 241093, 0, 0, 241, 241,  "1CAP2", "2412251021.", "1CAP2 composit.plastiq.chaudronnes"),
( 241094, 0, 0, 241, 241,  "2CAP2", "2412251022.", "2CAP2 composit.plastiq.chaudronnes"),
( 241095, 0, 0, 241, 241,  "1CAP2", "2412260921.", "1CAP2 scieries option B : affuteur sciage"),
( 241096, 0, 0, 241, 241,  "2CAP2", "2412260922.", "2CAP2 scieries option B : affuteur sciage"),
( 241097, 0, 0, 241, 241,  "2CAP2", "2412270622.", "2CAP2 froid et climatisation"),
( 241098, 0, 0, 241, 241,  "1CAP2", "2412271521.", "1CAP2 monteur installations thermiques"), -- MEF modifiée en 2018
( 241099, 0, 0, 241, 241,  "2CAP2", "2412271522.", "2CAP2 monteur installations thermiques"), -- MEF modifiée en 2019
( 241100, 0, 0, 241, 241,  "1CAP2", "2412271421.", "1CAP2 installateur en froid et conditionnement d’air"),
( 241101, 0, 0, 241, 241,  "2CAP2", "2412271422.", "2CAP2 installateur en froid et conditionnement d’air"),
( 241102, 0, 0, 241, 241,  "1CAP2", "2412300621.", "1CAP2 intervention en maintenance technique des bâtiments"), -- MEF modifiée en 2021
( 241103, 0, 0, 241, 241,  "2CAP2", "2412300622.", "2CAP2 intervention en maintenance technique des bâtiments"), -- MEF modifiée en 2021
( 241104, 0, 0, 241, 241,  "1CAP2", "2412300521.", "1CAP2 monteur structures mobiles"),
( 241105, 0, 0, 241, 241,  "2CAP2", "2412300522.", "2CAP2 monteur structures mobiles"),
( 241106, 0, 0, 241, 241,  "1CAP2", "2412312021.", "1CAP2 constructeur de routes et d’aménagements urbains"), -- MEF modifiée en 2021
( 241107, 0, 0, 241, 241,  "2CAP2", "2412312022.", "2CAP2 constructeur de routes et d’aménagements urbains"), -- MEF modifiée en 2021
( 241108, 0, 0, 241, 241,  "1CAP2", "2412311921.", "1CAP2 constructeur de réseaux de canalisations de travaux publics"), -- MEF modifiée en 2019
( 241109, 0, 0, 241, 241,  "2CAP2", "2412311922.", "2CAP2 constructeur de réseaux de canalisations de travaux publics"), -- MEF modifiée en 2020
( 241110, 0, 0, 241, 241,  "1CAP2", "2412311721.", "1CAP2 constructeur en ouvrages d’art"), -- fermé 31/08/2019
( 241111, 0, 0, 241, 241,  "2CAP2", "2412311722.", "2CAP2 constructeur en ouvrages d’art"), -- fermé 31/08/2020
( 241112, 0, 0, 241, 241,  "1CAP2", "2412311821.", "1CAP2 conduct engins tvx pub. carrières"),
( 241113, 0, 0, 241, 241,  "2CAP2", "2412311822.", "2CAP2 conduct engins tvx pub. carrières"),
( 241114, 0, 0, 241, 241,  "2CAP2", "2412320422.", "2CAP2 graveur sur pierre"),
( 241115, 0, 0, 241, 241,  "1CAP2", "2412321221.", "1CAP2 développement : const.entret.batimt -tom"),
( 241116, 0, 0, 241, 241,  "2CAP2", "2412321222.", "2CAP2 développement : const.entret.batimt -tom"),
( 241117, 0, 0, 241, 241,  "1CAP2", "2412322421.", "1CAP2 maçon"), -- MEF modifiée en 2021
( 241118, 0, 0, 241, 241,  "2CAP2", "2412322422.", "2CAP2 maçon"), -- MEF modifiée en 2021
( 241119, 0, 0, 241, 241,  "1CAP2", "2412322321.", "1CAP2 couvreur"), -- MEF modifiée en 2021
( 241120, 0, 0, 241, 241,  "2CAP2", "2412322322.", "2CAP2 couvreur"), -- MEF modifiée en 2021
( 241121, 0, 0, 241, 241,  "1CAP2", "2412322221.", "1CAP2 constructeur d’ouvrages en béton armé"), -- MEF modifiée en 2019
( 241122, 0, 0, 241, 241,  "2CAP2", "2412322222.", "2CAP2 constructeur d’ouvrages en béton armé"), -- MEF modifiée en 2020
( 241123, 0, 0, 241, 241,  "1CAP2", "2412322021.", "1CAP2 tailleur de pierre"),
( 241124, 0, 0, 241, 241,  "2CAP2", "2412322022.", "2CAP2 tailleur de pierre"),
( 241125, 0, 0, 241, 241,  "1CAP2", "2412322121.", "1CAP2 marbrier du bâtiment et de la décoration"),
( 241126, 0, 0, 241, 241,  "2CAP2", "2412322122.", "2CAP2 marbrier du bâtiment et de la décoration"),
( 241127, 0, 0, 241, 241,  "1CAP2", "2412330221.", "1CAP2 monteur en isol.thermiq & acoust."),
( 241128, 0, 0, 241, 241,  "2CAP2", "2412330222.", "2CAP2 monteur en isol.thermiq & acoust."),
( 241129, 0, 0, 241, 241,  "1CAP2", "2412331121.", "1CAP2 staffeur ornemaniste"),
( 241130, 0, 0, 241, 241,  "2CAP2", "2412331122.", "2CAP2 staffeur ornemaniste"),
( 241131, 0, 0, 241, 241,  "1CAP2", "2412332421.", "1CAP2 monteur en installations sanitaires"), -- MEF modifiée en 2018
( 241132, 0, 0, 241, 241,  "2CAP2", "2412332422.", "2CAP2 monteur en installations sanitaires"), -- MEF modifiée en 2019
( 241133, 0, 0, 241, 241,  "1CAP2", "2412332521.", "1CAP2 carreleur mosaïste"), -- MEF modifiée en 2019
( 241134, 0, 0, 241, 241,  "2CAP2", "2412332522.", "2CAP2 carreleur mosaïste"), -- MEF modifiée en 2020
( 241135, 0, 0, 241, 241,  "1CAP2", "2412331921.", "1CAP2 peintre-applicateur de revêtement"),
( 241136, 0, 0, 241, 241,  "2CAP2", "2412331922.", "2CAP2 peintre-applicateur de revêtement"),
( 241137, 0, 0, 241, 241,  "1CAP2", "2412332021.", "1CAP2 solier-moquettiste"),
( 241138, 0, 0, 241, 241,  "2CAP2", "2412332022.", "2CAP2 solier-moquettiste"),
( 241139, 0, 0, 241, 241,  "1CAP2", "2412332121.", "1CAP2 étancheur bâtiment travaux public"),
( 241140, 0, 0, 241, 241,  "2CAP2", "2412332122.", "2CAP2 étancheur bâtiment travaux public"),
( 241141, 0, 0, 241, 241,  "1CAP2", "2412332221.", "1CAP2 menuisier aluminium-verre"),
( 241142, 0, 0, 241, 241,  "2CAP2", "2412332222.", "2CAP2 menuisier aluminium-verre"),
( 241143, 0, 0, 241, 241,  "1CAP2", "2412332321.", "1CAP2 plâtrier-plaquiste"),
( 241144, 0, 0, 241, 241,  "2CAP2", "2412332322.", "2CAP2 plâtrier-plaquiste"),
( 241145, 0, 0, 241, 241,  "1CAP2", "2412341121.", "1CAP2 menuisier en sièges"),
( 241146, 0, 0, 241, 241,  "2CAP2", "2412341122.", "2CAP2 menuisier en sièges"),
( 241147, 0, 0, 241, 241,  "1CAP2", "2412342021.", "1CAP2 cannage et paillage ameublement"),
( 241148, 0, 0, 241, 241,  "2CAP2", "2412342022.", "2CAP2 cannage et paillage ameublement"),
( 241149, 0, 0, 241, 241,  "1CAP2", "2412342721.", "1CAP2 ouvrier archetier"),
( 241150, 0, 0, 241, 241,  "2CAP2", "2412342722.", "2CAP2 ouvrier archetier"),
( 241151, 0, 0, 241, 241,  "1CAP2", "2412342821.", "1CAP2 lutherie"),
( 241152, 0, 0, 241, 241,  "2CAP2", "2412342822.", "2CAP2 lutherie"),
( 241153, 0, 0, 241, 241,  "1CAP2", "2412343021.", "1CAP2 arts bois option A : sculpteur ornema."),
( 241154, 0, 0, 241, 241,  "2CAP2", "2412343022.", "2CAP2 arts bois option A : sculpteur ornema."),
( 241155, 0, 0, 241, 241,  "1CAP2", "2412343121.", "1CAP2 arts bois option B : tourneur"),
( 241156, 0, 0, 241, 241,  "2CAP2", "2412343122.", "2CAP2 arts bois option B : tourneur"),
( 241157, 0, 0, 241, 241,  "1CAP2", "2412343221.", "1CAP2 arts bois option C : marqueteur"),
( 241158, 0, 0, 241, 241,  "2CAP2", "2412343222.", "2CAP2 arts bois option C : marqueteur"),
( 241159, 0, 0, 241, 241,  "1CAP2", "2412343321.", "1CAP2 encadreur"),
( 241160, 0, 0, 241, 241,  "2CAP2", "2412343322.", "2CAP2 encadreur"),
( 241161, 0, 0, 241, 241,  "1CAP2", "2412343521.", "1CAP2 tonnellerie"),
( 241162, 0, 0, 241, 241,  "2CAP2", "2412343522.", "2CAP2 tonnellerie"),
( 241163, 0, 0, 241, 241,  "1CAP2", "2412344521.", "1CAP2 ébéniste"),
( 241164, 0, 0, 241, 241,  "2CAP2", "2412344522.", "2CAP2 ébéniste"),
( 241165, 0, 0, 241, 241,  "1CAP2", "2412343821.", "1CAP2 vannerie"),
( 241166, 0, 0, 241, 241,  "2CAP2", "2412343822.", "2CAP2 vannerie"),
( 241167, 0, 0, 241, 241,  "1CAP2", "2412344621.", "1CAP2 charpentier bois"), -- MEF modifiée en 2020
( 241168, 0, 0, 241, 241,  "2CAP2", "2412344622.", "2CAP2 charpentier bois"), -- MEF modifiée en 2020
( 241169, 0, 0, 241, 241,  "1CAP2", "2412344021.", "1CAP2 constructeur bois"),
( 241170, 0, 0, 241, 241,  "2CAP2", "2412344022.", "2CAP2 constructeur bois"),
( 241171, 0, 0, 241, 241,  "1CAP2", "2412344721.", "1CAP2 menuisier fabricant"), -- MEF modifiée en 2021
( 241172, 0, 0, 241, 241,  "2CAP2", "2412344722.", "2CAP2 menuisier fabricant"), -- MEF modifiée en 2021
( 241173, 0, 0, 241, 241,  "1CAP2", "2412344921.", "1CAP2 menuisier installateur"), -- MEF modifiée en 2021
( 241174, 0, 0, 241, 241,  "2CAP2", "2412344922.", "2CAP2 menuisier installateur"), -- MEF modifiée en 2021
( 241175, 0, 0, 241, 241,  "1CAP2", "2412344321.", "1CAP2 conducteur-opérateur de scierie"),
( 241176, 0, 0, 241, 241,  "2CAP2", "2412344322.", "2CAP2 conducteur-opérateur de scierie"),
( 241177, 0, 0, 241, 241,  "1CAP2", "2412344421.", "1CAP2 charpentier de marine"),
( 241178, 0, 0, 241, 241,  "2CAP2", "2412344422.", "2CAP2 charpentier de marine"),
( 241179, 0, 0, 241, 241,  "1CAP2", "2412400821.", "1CAP2 métier du pressing"), -- MEF modifiée en 2021
( 241180, 0, 0, 241, 241,  "2CAP2", "2412400822.", "2CAP2 métier du pressing"), -- MEF modifiée en 2021
( 241181, 0, 0, 241, 241,  "1CAP2", "2412400721.", "1CAP2 métiers de la blanchisserie"), -- MEF modifiée en 2021
( 241182, 0, 0, 241, 241,  "2CAP2", "2412400722.", "2CAP2 métiers de la blanchisserie"), -- MEF modifiée en 2021
( 241183, 0, 0, 241, 241,  "1CAP2", "2412412521.", "1CAP2 rentrayeur option A : tapis"),
( 241184, 0, 0, 241, 241,  "2CAP2", "2412412522.", "2CAP2 rentrayeur option A : tapis"),
( 241185, 0, 0, 241, 241,  "1CAP2", "2412412621.", "1CAP2 rentrayeur option B : tapisseries"),
( 241186, 0, 0, 241, 241,  "2CAP2", "2412412622.", "2CAP2 rentrayeur option B : tapisseries"),
( 241187, 0, 0, 241, 241,  "1CAP2", "2412412721.", "1CAP2 arts de la broderie"),
( 241188, 0, 0, 241, 241,  "2CAP2", "2412412722.", "2CAP2 arts de la broderie"),
( 241189, 0, 0, 241, 241,  "1CAP2", "2412413021.", "1CAP2 arts de la dentelle opt.fuseaux"),
( 241190, 0, 0, 241, 241,  "2CAP2", "2412413022.", "2CAP2 arts de la dentelle opt.fuseaux"),
( 241191, 0, 0, 241, 241,  "1CAP2", "2412413121.", "1CAP2 arts de la dentelle opt.aiguille"),
( 241192, 0, 0, 241, 241,  "2CAP2", "2412413122.", "2CAP2 arts de la dentelle opt.aiguille"),
( 241193, 0, 0, 241, 241,  "1CAP2", "2412423121.", "1CAP2 arts du tapis et tapiss.de lisse"),
( 241194, 0, 0, 241, 241,  "2CAP2", "2412423122.", "2CAP2 arts du tapis et tapiss.de lisse"),
( 241195, 0, 0, 241, 241,  "1CAP2", "2412423821.", "1CAP2 tapissier-e ameublement en siège"),
( 241196, 0, 0, 241, 241,  "2CAP2", "2412423822.", "2CAP2 tapissier-e ameublement en siège"),
( 241197, 0, 0, 241, 241,  "1CAP2", "2412423921.", "1CAP2 tapissier-e ameublement en décor"),
( 241198, 0, 0, 241, 241,  "2CAP2", "2412423922.", "2CAP2 tapissier-e ameublement en décor"),
( 241199, 0, 0, 241, 241,  "1CAP2", "2412424021.", "1CAP2 métiers de la mode-vêtement flou"),
( 241200, 0, 0, 241, 241,  "2CAP2", "2412424022.", "2CAP2 métiers de la mode-vêtement flou"),
( 241201, 0, 0, 241, 241,  "1CAP2", "2412424121.", "1CAP2 métiers de la mode- vêt. tailleur"),
( 241202, 0, 0, 241, 241,  "2CAP2", "2412424122.", "2CAP2 métiers de la mode- vêt. tailleur"),
( 241203, 0, 0, 241, 241,  "1CAP2", "2412424221.", "1CAP2 métiers mode chapelier-modiste"),
( 241204, 0, 0, 241, 241,  "2CAP2", "2412424222.", "2CAP2 métiers mode chapelier-modiste"),
( 241205, 0, 0, 241, 241,  "1CAP2", "2412431321.", "1CAP2 cordonnier bottier"),
( 241206, 0, 0, 241, 241,  "2CAP2", "2412431322.", "2CAP2 cordonnier bottier"),
( 241207, 0, 0, 241, 241,  "1CAP2", "2412431521.", "1CAP2 fourrure"),
( 241208, 0, 0, 241, 241,  "2CAP2", "2412431522.", "2CAP2 fourrure"),
( 241209, 0, 0, 241, 241,  "1CAP2", "2412431621.", "1CAP2 chaussure"),
( 241210, 0, 0, 241, 241,  "2CAP2", "2412431622.", "2CAP2 chaussure"),
( 241211, 0, 0, 241, 241,  "1CAP2", "2412432221.", "1CAP2 maroquinerie"),
( 241212, 0, 0, 241, 241,  "2CAP2", "2412432222.", "2CAP2 maroquinerie"),
( 241213, 0, 0, 241, 241,  "1CAP2", "2412431821.", "1CAP2 sellerie générale"),
( 241214, 0, 0, 241, 241,  "2CAP2", "2412431822.", "2CAP2 sellerie générale"),
( 241215, 0, 0, 241, 241,  "1CAP2", "2412431921.", "1CAP2 vêtement de peau"),
( 241216, 0, 0, 241, 241,  "2CAP2", "2412431922.", "2CAP2 vêtement de peau"),
( 241217, 0, 0, 241, 241,  "1CAP2", "2412432021.", "1CAP2 cordonnerie multiservice"),
( 241218, 0, 0, 241, 241,  "2CAP2", "2412432022.", "2CAP2 cordonnerie multiservice"),
( 241219, 0, 0, 241, 241,  "1CAP2", "2412432121.", "1CAP2 sellier harnacheur"),
( 241220, 0, 0, 241, 241,  "2CAP2", "2412432122.", "2CAP2 sellier harnacheur"),
( 241221, 0, 0, 241, 241,  "1CAP2", "2412500121.", "1CAP2 certif.aptitude au developt -tom"),
( 241222, 0, 0, 241, 241,  "2CAP2", "2412500122.", "2CAP2 certif.aptitude au developt -tom"),
( 241223, 0, 0, 241, 241,  "1CAP2", "2412510821.", "1CAP2 instrumts coupants & de chirurgie"),
( 241224, 0, 0, 241, 241,  "2CAP2", "2412510822.", "2CAP2 instrumts coupants & de chirurgie"),
( 241225, 0, 0, 241, 241,  "1CAP2", "2412512321.", "1CAP2 décolletage : opérateur regleur"),
( 241226, 0, 0, 241, 241,  "2CAP2", "2412512322.", "2CAP2 décolletage : opérateur regleur"),
( 241227, 0, 0, 241, 241,  "1CAP2", "2412512621.", "1CAP2 outillages a decouper et emboutir"),
( 241228, 0, 0, 241, 241,  "2CAP2", "2412512622.", "2CAP2 outillages a decouper et emboutir"),
( 241229, 0, 0, 241, 241,  "1CAP2", "2412513021.", "1CAP2 développement : fabric.entret.meca. -tom"),
( 241230, 0, 0, 241, 241,  "2CAP2", "2412513022.", "2CAP2 développement : fabric.entret.meca. -tom"),
( 241231, 0, 0, 241, 241,  "1CAP2", "2412513321.", "1CAP2 agent verif. appar. extincteurs"),
( 241232, 0, 0, 241, 241,  "2CAP2", "2412513322.", "2CAP2 agent verif. appar. extincteurs"),
( 241233, 0, 0, 241, 241,  "1CAP2", "2412513421.", "1CAP2 transport câbles-remontées meca."),
( 241234, 0, 0, 241, 241,  "2CAP2", "2412513422.", "2CAP2 transport câbles-remontées meca."),
( 241235, 0, 0, 241, 241,  "1CAP2", "2412513621.", "1CAP2 armurerie (fabricat. et réparat.)"),
( 241236, 0, 0, 241, 241,  "2CAP2", "2412513622.", "2CAP2 armurerie (fabricat. et réparat.)"),
( 241237, 0, 0, 241, 241,  "1CAP2", "2412513721.", "1CAP2 horloger"),
( 241238, 0, 0, 241, 241,  "2CAP2", "2412513722.", "2CAP2 horloger"),
( 241239, 0, 0, 241, 241,  "1CAP2", "2412522121.", "1CAP2 maintenance des matériels option A matériels agricoles"),
( 241240, 0, 0, 241, 241,  "2CAP2", "2412522122.", "2CAP2 maintenance des matériels option A matériels agricoles"),
( 241241, 0, 0, 241, 241,  "1CAP2", "2412522221.", "1CAP2 maintenance des matériels option B matériels TP et manutention"),
( 241242, 0, 0, 241, 241,  "2CAP2", "2412522222.", "2CAP2 maintenance des matériels option B matériels TP et manutention"),
( 241243, 0, 0, 241, 241,  "1CAP2", "2412522321.", "1CAP2 maintenance des matériels option C matériels d’espaces verts"),
( 241244, 0, 0, 241, 241,  "2CAP2", "2412522322.", "2CAP2 maintenance des matériels option C matériels d’espaces verts"),
( 241245, 0, 0, 241, 241,  "1CAP2", "2412521721.", "1CAP2 repar.entr.embarcations plaisance"),
( 241246, 0, 0, 241, 241,  "2CAP2", "2412521722.", "2CAP2 repar.entr.embarcations plaisance"),
( 241247, 0, 0, 241, 241,  "1CAP2", "2412521821.", "1CAP2 maintenance des véhicules option A voitures particulières"),
( 241248, 0, 0, 241, 241,  "2CAP2", "2412521822.", "2CAP2 maintenance des véhicules option A voitures particulières"),
( 241249, 0, 0, 241, 241,  "1CAP2", "2412521921.", "1CAP2 maintenance des véhicules option B véhicules de transport routier"),
( 241250, 0, 0, 241, 241,  "2CAP2", "2412521922.", "2CAP2 maintenance des véhicules option B véhicules de transport routier"),
( 241251, 0, 0, 241, 241,  "1CAP2", "2412522021.", "1CAP2 maintenance des véhicules option C motocycles"),
( 241252, 0, 0, 241, 241,  "2CAP2", "2412522022.", "2CAP2 maintenance des véhicules option C motocycles"),
( 241253, 0, 0, 241, 241,  "1CAP2", "2412530521.", "1CAP2 aéronautique option avionique"),
( 241254, 0, 0, 241, 241,  "2CAP2", "2412530522.", "2CAP2 aéronautique option avionique"),
( 241255, 0, 0, 241, 241,  "1CAP2", "2412530621.", "1CAP2 aéronautique option systèmes"),
( 241256, 0, 0, 241, 241,  "2CAP2", "2412530622.", "2CAP2 aéronautique option systèmes"),
( 241257, 0, 0, 241, 241,  "1CAP2", "2412530721.", "1CAP2 aéronautique option structures"),
( 241258, 0, 0, 241, 241,  "2CAP2", "2412530722.", "2CAP2 aéronautique option structures"),
( 241259, 0, 0, 241, 241,  "1CAP2", "2412542121.", "1CAP2 mise en forme des matériaux"),
( 241260, 0, 0, 241, 241,  "2CAP2", "2412542122.", "2CAP2 mise en forme des matériaux"),
( 241261, 0, 0, 241, 241,  "2CAP2", "2412542322.", "2CAP2 ferronnier"),
( 241262, 0, 0, 241, 241,  "1CAP2", "2412542521.", "1CAP2 outillage en moules métallique"),
( 241263, 0, 0, 241, 241,  "2CAP2", "2412542522.", "2CAP2 outillage en moules métallique"),
( 241264, 0, 0, 241, 241,  "2CAP2", "2412542722.", "2CAP2 tuyautier en orgues"),
( 241265, 0, 0, 241, 241,  "1CAP2", "2412543921.", "1CAP2 métallier"), -- MEF modifiée en 2021
( 241266, 0, 0, 241, 241,  "2CAP2", "2412543922.", "2CAP2 métallier"), -- MEF modifiée en 2021
( 241267, 0, 0, 241, 241,  "1CAP2", "2412543221.", "1CAP2 construction des carrosseries"),
( 241268, 0, 0, 241, 241,  "2CAP2", "2412543222.", "2CAP2 construction des carrosseries"),
( 241269, 0, 0, 241, 241,  "1CAP2", "2412543321.", "1CAP2 peinture en carrosserie"),
( 241270, 0, 0, 241, 241,  "2CAP2", "2412543322.", "2CAP2 peinture en carrosserie"),
( 241271, 0, 0, 241, 241,  "1CAP2", "2412543421.", "1CAP2 réparation des carrosseries"),
( 241272, 0, 0, 241, 241,  "2CAP2", "2412543422.", "2CAP2 réparation des carrosseries"),
( 241273, 0, 0, 241, 241,  "1CAP2", "2412543721.", "1CAP2 réalisations industrielles option A chaudronnerie."),
( 241274, 0, 0, 241, 241,  "2CAP2", "2412543722.", "2CAP2 réalisations industrielles option A chaudronnerie"),
( 241275, 0, 0, 241, 241,  "1CAP2", "2412543621.", "1CAP2 ferronnier d’art"),
( 241276, 0, 0, 241, 241,  "2CAP2", "2412543622.", "2CAP2 ferronnier d’art"),
( 241277, 0, 0, 241, 241,  "1CAP2", "2412552121.", "1CAP2 métiers enseigne signalétique"),
( 241278, 0, 0, 241, 241,  "2CAP2", "2412552122.", "2CAP2 métiers enseigne signalétique"),
( 241279, 0, 0, 241, 241,  "1CAP2", "2412552321.", "1CAP2 prep.& real. ouvrages électriques"), -- fermé
( 241280, 0, 0, 241, 241,  "2CAP2", "2412552322.", "2CAP2 prep.& real. ouvrages électriques"), -- fermé
( 241281, 0, 0, 241, 241,  "1CAP2", "2413110721.", "1CAP2 emballeur professionnel"),
( 241282, 0, 0, 241, 241,  "2CAP2", "2413110722.", "2CAP2 emballeur professionnel"),
( 241283, 0, 0, 241, 241,  "1CAP2", "2413111421.", "1CAP2 agt accueil condte rout.tr.voyag."),
( 241284, 0, 0, 241, 241,  "2CAP2", "2413111422.", "2CAP2 agt accueil condte rout.tr.voyag."),
( 241285, 0, 0, 241, 241,  "2CAP2", "2413111622.", "2CAP2 agent entreposage et messagerie"),
( 241286, 0, 0, 241, 241,  "1CAP2", "2413111721.", "1CAP2 conducteur routier marchandises"),
( 241287, 0, 0, 241, 241,  "2CAP2", "2413111722.", "2CAP2 conducteur routier marchandises"),
( 241288, 0, 0, 241, 241,  "1CAP2", "2413111821.", "1CAP2 conducteur livreur marchandises"),
( 241289, 0, 0, 241, 241,  "2CAP2", "2413111822.", "2CAP2 conducteur livreur marchandises"),
( 241290, 0, 0, 241, 241,  "1CAP2", "2413111921.", "1CAP2 déménageur véhic.utilitaire léger"),
( 241291, 0, 0, 241, 241,  "2CAP2", "2413111922.", "2CAP2 déménageur véhic.utilitaire léger"),
( 241292, 0, 0, 241, 241,  "1CAP2", "2413112321.", "1CAP2 opérateur/opératrice de service-relation client et livraison"),
( 241293, 0, 0, 241, 241,  "2CAP2", "2413112322.", "2CAP2 opérateur/opératrice de service-relation client et livraison"),
( 241294, 0, 0, 241, 241,  "1CAP2", "2413112121.", "1CAP2 transport fluvial"),
( 241295, 0, 0, 241, 241,  "2CAP2", "2413112122.", "2CAP2 transport fluvial"),
( 241296, 0, 0, 241, 241,  "1CAP2", "2413112221.", "1CAP2 opérateur/opératrice logistique"),
( 241297, 0, 0, 241, 241,  "2CAP2", "2413112222.", "2CAP2 opérateur/opératrice logistique"),
( 241298, 0, 0, 241, 241,  "1CAP2", "2413121421.", "1CAP2 employé de commerce multispécialités"), -- fermé 2020-08-31
( 241299, 0, 0, 241, 241,  "2CAP2", "2413121422.", "2CAP2 employé de commerce multispécialités"), -- fermé 2021-08-31
( 241300, 0, 0, 241, 241,  "1CAP2", "2413121521.", "1CAP2 employé de vente spécialisé option A produits alimentaires"), -- fermé 2020-08-31
( 241301, 0, 0, 241, 241,  "2CAP2", "2413121522.", "2CAP2 employé de vente spécialisé option A produits alimentaires"), -- fermé 2021-08-31
( 241302, 0, 0, 241, 241,  "1CAP2", "2413121621.", "1CAP2 employé de vente spécialisé option B produits d’équipement courant"), -- fermé 2020-08-31
( 241303, 0, 0, 241, 241,  "2CAP2", "2413121622.", "2CAP2 employé de vente spécialisé option B produits d’équipement courant"), -- fermé 2021-08-31
( 241304, 0, 0, 241, 241,  "1CAP2", "2413121721.", "1CAP2 vendeur-magasinier pièces de rechange & équipements automobiles"), -- fermé 2020-08-31
( 241305, 0, 0, 241, 241,  "2CAP2", "2413121722.", "2CAP2 vendeur-magasinier pièces de rechange & équipements automobiles"), -- fermé 2021-08-31
( 241306, 0, 0, 241, 241,  "1CAP2", "2413122321.", "1CAP2 fleuriste"), -- MEF modifiée en 2018
( 241307, 0, 0, 241, 241,  "2CAP2", "2413122322.", "2CAP2 fleuriste"), -- MEF modifiée en 2019
( 241308, 0, 0, 241, 241,  "1CAP2", "2413121921.", "1CAP2 employé de vente spécialisé option C service à la clientèle"), -- fermé 2020-08-31
( 241309, 0, 0, 241, 241,  "2CAP2", "2413121922.", "2CAP2 employé de vente spécialisé option C service à la clientèle"), -- fermé 2021-08-31
( 241310, 0, 0, 241, 241,  "1CAP2", "2413122021.", "1CAP2 employé de vente spécialisé option D librairie-papeterie-presse"), -- fermé 2020-08-31
( 241311, 0, 0, 241, 241,  "2CAP2", "2413122022.", "2CAP2 employé de vente spécialisé option D librairie-papeterie-presse"), -- fermé 2021-08-31
( 241312, 0, 0, 241, 241,  "1CAP2", "2413122121.", "1CAP2 poissonnier"),
( 241313, 0, 0, 241, 241,  "2CAP2", "2413122122.", "2CAP2 poissonnier"),
( 241314, 0, 0, 241, 241,  "1CAP2", "2413222821.", "1CAP2 arts de la reliure"),
( 241315, 0, 0, 241, 241,  "2CAP2", "2413222822.", "2CAP2 arts de la reliure"),
( 241316, 0, 0, 241, 241,  "1CAP2", "2413222021.", "1CAP2 métiers de la gravure : ornement."),
( 241317, 0, 0, 241, 241,  "2CAP2", "2413222022.", "2CAP2 métiers de la gravure : ornement."),
( 241318, 0, 0, 241, 241,  "1CAP2", "2413222121.", "1CAP2 métiers de la gravure : impress."),
( 241319, 0, 0, 241, 241,  "2CAP2", "2413222122.", "2CAP2 métiers de la gravure : impress."),
( 241320, 0, 0, 241, 241,  "1CAP2", "2413222221.", "1CAP2 métiers de la gravure : modèle"),
( 241321, 0, 0, 241, 241,  "2CAP2", "2413222222.", "2CAP2 métiers de la gravure : modèle"),
( 241322, 0, 0, 241, 241,  "1CAP2", "2413222321.", "1CAP2 met.gravure : marquage poinçonnage"),
( 241323, 0, 0, 241, 241,  "2CAP2", "2413222322.", "2CAP2 met.gravure : marquage poinçonnage"),
( 241324, 0, 0, 241, 241,  "1CAP2", "2413222521.", "1CAP2 sérigraphie industrielle"),
( 241325, 0, 0, 241, 241,  "2CAP2", "2413222522.", "2CAP2 sérigraphie industrielle"),
( 241326, 0, 0, 241, 241,  "1CAP2", "2413222721.", "1CAP2 signalétique et décors graphiques"),
( 241327, 0, 0, 241, 241,  "2CAP2", "2413222722.", "2CAP2 signalétique et décors graphiques"),
( 241328, 0, 0, 241, 241,  "1CAP2", "2413230621.", "1CAP2 monteur en chapiteaux"),
( 241329, 0, 0, 241, 241,  "2CAP2", "2413230622.", "2CAP2 monteur en chapiteaux"),
( 241330, 0, 0, 241, 241,  "1CAP2", "2413230721.", "1CAP2 accessoiriste réalisateur"),
( 241331, 0, 0, 241, 241,  "2CAP2", "2413230722.", "2CAP2 accessoiriste réalisateur"),
( 241332, 0, 0, 241, 241,  "1CAP2", "2413230821.", "1CAP2 accordeur de pianos"),
( 241333, 0, 0, 241, 241,  "2CAP2", "2413230822.", "2CAP2 accordeur de pianos"),
( 241334, 0, 0, 241, 241,  "1CAP2", "2413231021.", "1CAP2 assistant technique en instruments de musique option accordéon"),
( 241335, 0, 0, 241, 241,  "2CAP2", "2413231022.", "2CAP2 assistant technique en instruments de musique option accordéon"),
( 241336, 0, 0, 241, 241,  "1CAP2", "2413231121.", "1CAP2 ass.tech.instr.musiq. instr.vent"),
( 241337, 0, 0, 241, 241,  "2CAP2", "2413231122.", "2CAP2 ass.tech.instr.musiq. instr.vent"),
( 241338, 0, 0, 241, 241,  "1CAP2", "2413231221.", "1CAP2 assistant technique en instruments de musique option piano"),
( 241339, 0, 0, 241, 241,  "2CAP2", "2413231222.", "2CAP2 assistant technique en instruments de musique option piano"),
( 241340, 0, 0, 241, 241,  "1CAP2", "2413231321.", "1CAP2 assistant technique en instruments de musique option guitare"),
( 241341, 0, 0, 241, 241,  "2CAP2", "2413231322.", "2CAP2 assistant technique en instruments de musique option guitare"),
( 241342, 0, 0, 241, 241,  "1CAP2", "2413231421.", "1CAP2 opérateur projectionniste cinéma"),
( 241343, 0, 0, 241, 241,  "2CAP2", "2413231422.", "2CAP2 opérateur projectionniste cinéma"),
( 241344, 0, 0, 241, 241,  "1CAP2", "2413310621.", "1CAP2 ortho-prothésiste"),
( 241345, 0, 0, 241, 241,  "2CAP2", "2413310622.", "2CAP2 ortho-prothésiste"),
( 241346, 0, 0, 241, 241,  "1CAP2", "2413310721.", "1CAP2 podo-orthésiste"),
( 241347, 0, 0, 241, 241,  "2CAP2", "2413310722.", "2CAP2 podo-orthésiste"),
( 241348, 0, 0, 241, 241,  "1CAP2", "2413320121.", "1CAP2 développement : act.famil.artis.tour. -tom"),
( 241349, 0, 0, 241, 241,  "2CAP2", "2413320122.", "2CAP2 développement : act.famil.artis.tour. -tom"),
( 241350, 0, 0, 241, 241,  "1CAP2", "2413320521.", "1CAP2 accompagnant éducatif petite enfance"), -- MEF modifiée en 2021
( 241351, 0, 0, 241, 241,  "2CAP2", "2413320522.", "2CAP2 accompagnant éducatif petite enfance"), -- MEF modifiée en 2021
( 241352, 0, 0, 241, 241,  "1CAP2", "2413341221.", "1CAP2 commercialisation et services en hôtel-café-restaurant"),
( 241353, 0, 0, 241, 241,  "2CAP2", "2413341222.", "2CAP2 commercialisation et services en hôtel-café-restaurant"),
( 241354, 0, 0, 241, 241,  "1CAP2", "2413340921.", "1CAP2 restaurant"), -- fermé
( 241355, 0, 0, 241, 241,  "2CAP2", "2413340922.", "2CAP2 restaurant"), -- fermé
( 241356, 0, 0, 241, 241,  "1CAP2", "2413341021.", "1CAP2 services en brasserie-café"), -- fermé
( 241357, 0, 0, 241, 241,  "2CAP2", "2413341022.", "2CAP2 services en brasserie-café"), -- fermé
( 241358, 0, 0, 241, 241,  "1CAP2", "2413341121.", "1CAP2 assistant(e) technique en milieux familial et collectif"),
( 241359, 0, 0, 241, 241,  "2CAP2", "2413341122.", "2CAP2 assistant(e) technique en milieux familial et collectif"),
( 241360, 0, 0, 241, 241,  "1CAP2", "2413350121.", "1CAP2 métiers du football"),
( 241361, 0, 0, 241, 241,  "2CAP2", "2413350122.", "2CAP2 métiers du football"),
( 241362, 0, 0, 241, 241,  "1CAP2", "2413361721.", "1CAP2 taxidermiste"), -- MEF modifiée en 2019
( 241363, 0, 0, 241, 241,  "2CAP2", "2413361722.", "2CAP2 taxidermiste"), -- MEF modifiée en 2020
( 241364, 0, 0, 241, 241,  "1CAP2", "2413361621.", "1CAP2 coiffure"), -- MEF modifiée en 2019
( 241365, 0, 0, 241, 241,  "2CAP2", "2413361622.", "2CAP2 coiffure"), -- MEF modifiée en 2020
( 241366, 0, 0, 241, 241,  "1CAP2", "2413361521.", "1CAP2 esthétique cosmétique parfumerie"), -- MEF modifiée en 2018
( 241367, 0, 0, 241, 241,  "2CAP2", "2413361522.", "2CAP2 esthétique cosmétique parfumerie"), -- MEF modifiée en 2019
( 241368, 0, 0, 241, 241,  "1CAP2", "2413400221.", "1CAP2 agent de prévention et de médiation"),
( 241369, 0, 0, 241, 241,  "2CAP2", "2413400222.", "2CAP2 agent de prévention et de médiation"),
( 241370, 0, 0, 241, 241,  "1CAP2", "2413430121.", "1CAP2 agent de la qualité de l’eau"),
( 241371, 0, 0, 241, 241,  "2CAP2", "2413430122.", "2CAP2 agent de la qualité de l’eau"),
( 241372, 0, 0, 241, 241,  "1CAP2", "2413430321.", "1CAP2 assainismt collect.déchets liquid"),
( 241373, 0, 0, 241, 241,  "2CAP2", "2413430322.", "2CAP2 assainismt collect.déchets liquid"),
( 241374, 0, 0, 241, 241,  "2CAP2", "2413430522.", "2CAP2 gestion déchets propreté urbaine"),
( 241375, 0, 0, 241, 241,  "1CAP2", "2413430721.", "1CAP2 agent de propreté et d’hygiène"),
( 241376, 0, 0, 241, 241,  "2CAP2", "2413430722.", "2CAP2 agent de propreté et d’hygiène"),
( 241377, 0, 0, 241, 241,  "1CAP2", "2413440421.", "1CAP2 gardien d’immeuble"),
( 241378, 0, 0, 241, 241,  "2CAP2", "2413440422.", "2CAP2 gardien d’immeuble"),
( 241379, 0, 0, 241, 241,  "1CAP2", "2413440521.", "1CAP2 agent de sécurité"),
( 241380, 0, 0, 241, 241,  "2CAP2", "2413440522.", "2CAP2 agent de sécurité"),
( 241381, 0, 0, 241, 241,  "1CAP2", "2412120321.", "1CAP2 conchyliculture"),
( 241382, 0, 0, 241, 241,  "2CAP2", "2412120322.", "2CAP2 conchyliculture"),
( 241383, 0, 0, 241, 241,  "1CAP2", "2412214021.", "1CAP2 crémier-fromager"),
( 241384, 0, 0, 241, 241,  "2CAP2", "2412214022.", "2CAP2 crémier-fromager"),
( 241385, 0, 0, 241, 241,  "1CAP2", "2412271521.", "1CAP2 monteur en installations thermiques"),
( 241386, 0, 0, 241, 241,  "1CAP2", "2412271522.", "1CAP2 monteur en installations thermiques"),
( 241387, 0, 0, 241, 241,  "2CAP2", "2412332421.", "2CAP2 monteur en installations sanitaires"),
( 241388, 0, 0, 241, 241,  "2CAP2", "2412332422.", "2CAP2 monteur en installations sanitaires"),
( 241389, 0, 0, 241, 241,  "1CAP2", "2412543821.", "1CAP2 réalisations industrielles option B soudage"),
( 241390, 0, 0, 241, 241,  "2CAP2", "2412543822.", "2CAP2 réalisations industrielles option B soudage"),
( 241391, 0, 0, 241, 241,  "1CAP2", "2412552421.", "1CAP2 électricien"), -- MEF modifiée en 2018
( 241392, 0, 0, 241, 241,  "2CAP2", "2412552422.", "2CAP2 électricien"), -- MEF modifiée en 2019
( 241393, 0, 0, 241, 241,  "1CAP2", "2413122221.", "1CAP2 primeur"),
( 241394, 0, 0, 241, 241,  "2CAP2", "2413122222.", "2CAP2 primeur"),
( 241395, 0, 0, 241, 241,  "1CAP2", "2413000121.", "1CAP2 1ère année de CAP services"),
( 241396, 0, 0, 241, 241,  "1CAP2", "2412000621.", "1CAP2 1ère année de CAP production"),
( 241397, 0, 0, 241, 241,  "1CAP2", "2412332621.", "1CAP2 métiers du plâtre et de l’isolation"),
( 241398, 0, 0, 241, 241,  "2CAP2", "2412332622.", "2CAP2 métiers du plâtre et de l’isolation"),
( 241399, 0, 0, 241, 241,  "1CAP2", "2412332721.", "1CAP2 peintre applicateur de revêtements"),
( 241400, 0, 0, 241, 241,  "2CAP2", "2412332722.", "2CAP2 peintre applicateur de revêtements"),
( 241401, 0, 0, 241, 241,  "1CAP2", "2412214221.", "1CAP2 production/service en restauration rapide, collective, cafétéria"),
( 241402, 0, 0, 241, 241,  "2CAP2", "2412214222.", "2CAP2 production/service en restauration rapide, collective, cafétéria"),
( 241403, 0, 0, 241, 241,  "1CAP2", "2413122421.", "1CAP2 équipier polyvalent du commerce"),
( 241404, 0, 0, 241, 241,  "2CAP2", "2413122422.", "2CAP2 équipier polyvalent du commerce"),
( 241405, 0, 0, 241, 241,  "1CAP2", "2412345021.", "1CAP2 assistant luthier du quatuor"),
( 241406, 0, 0, 241, 241,  "2CAP2", "2412345022.", "2CAP2 assistant luthier du quatuor"),

-- 242 CAP (Certificat d’Aptitude Professionnelle) en 3 ans -- dispositif de formation 242

( 242001, 0, 1, 242, 242,  "1CAP3", "242.....31.", "CAP 3 ans, 1e année"),
( 242002, 0, 1, 242, 242,  "2CAP3", "242.....32.", "CAP 3 ans, 2e année"),
( 242003, 0, 1, 242, 242,  "3CAP3", "242.....33.", "CAP 3 ans, 3e année"),
( 242004, 0, 0, 242, 242,  "1CAP3", "2422100231.", "1CAP3 développement : gest.pte expl.rural -tom"), -- fermé 2020-08-31
( 242005, 0, 0, 242, 242,  "2CAP3", "2422100232.", "2CAP3 développement : gest.pte expl.rural -tom"),
( 242006, 0, 0, 242, 242,  "3CAP3", "2422100233.", "3CAP3 développement : gest.pte expl.rural -tom"),
( 242007, 0, 0, 242, 242,  "1CAP3", "2422130331.", "1CAP3 développement : gest. milieu marin -tom"), -- fermé 2020-08-31
( 242008, 0, 0, 242, 242,  "2CAP3", "2422130332.", "2CAP3 développement : gest. milieu marin -tom"),
( 242009, 0, 0, 242, 242,  "3CAP3", "2422130333.", "3CAP3 développement : gest. milieu marin -tom"),
( 242010, 0, 0, 242, 242,  "1CAP3", "2422321231.", "1CAP3 développement : const.entret.batimt -tom"), -- fermé 2020-08-31
( 242011, 0, 0, 242, 242,  "2CAP3", "2422321232.", "2CAP3 développement : const.entret.batimt -tom"),
( 242012, 0, 0, 242, 242,  "3CAP3", "2422321233.", "3CAP3 développement : const.entret.batimt -tom"),
( 242013, 0, 0, 242, 242,  "1CAP3", "2422500131.", "1CAP3 certif.aptitude au developt -tom"), -- fermé 2020-08-31
( 242014, 0, 0, 242, 242,  "2CAP3", "2422500132.", "2CAP3 certif.aptitude au developt -tom"),
( 242015, 0, 0, 242, 242,  "3CAP3", "2422500133.", "3CAP3 certif.aptitude au developt -tom"),
( 242016, 0, 0, 242, 242,  "1CAP3", "2422513031.", "1CAP3 développement : fabric.entret.meca. -tom"), -- fermé 2020-08-31
( 242017, 0, 0, 242, 242,  "2CAP3", "2422513032.", "2CAP3 développement : fabric.entret.meca. -tom"),
( 242018, 0, 0, 242, 242,  "3CAP3", "2422513033.", "3CAP3 développement : fabric.entret.meca. -tom"),
( 242019, 0, 0, 242, 242,  "1CAP3", "2423320131.", "1CAP3 développement : act.famil.artis.tour. -tom"), -- fermé 2020-08-31
( 242020, 0, 0, 242, 242,  "2CAP3", "2423320132.", "2CAP3 développement : act.famil.artis.tour. -tom"),
( 242021, 0, 0, 242, 242,  "3CAP3", "2423320133.", "3CAP3 développement : act.famil.artis.tour. -tom"),
( 242022, 0, 0, 242, 242,  "1CAP3", "2422010131.", "1CAP3 conducteur d’installations de production"), -- fermé 2020-08-31
( 242023, 0, 0, 242, 242,  "2CAP3", "2422010132.", "2CAP3 conducteur d’installations de production"),
( 242024, 0, 0, 242, 242,  "3CAP3", "2422010133.", "3CAP3 conducteur d’installations de production"),
( 242025, 0, 0, 242, 242,  "1CAP3", "2422200531.", "1CAP3 propreté de l’environnement urbain - collecte et recyclage"), -- fermé 2020-08-31
( 242026, 0, 0, 242, 242,  "2CAP3", "2422200532.", "2CAP3 propreté de l’environnement urbain - collecte et recyclage"),
( 242027, 0, 0, 242, 242,  "3CAP3", "2422200533.", "3CAP3 propreté de l’environnement urbain - collecte et recyclage"),
( 242028, 0, 0, 242, 242,  "1CAP3", "2422130531.", "1CAP3 mareyage"), -- fermé 2020-03-26
( 242029, 0, 0, 242, 242,  "2CAP3", "2422130532.", "2CAP3 mareyage"),
( 242030, 0, 0, 242, 242,  "3CAP3", "2422130533.", "3CAP3 mareyage"),
( 242031, 0, 0, 242, 242,  "1CAP3", "2422200231.", "1CAP3 employé technique de laboratoire"), -- fermé 2020-08-31
( 242032, 0, 0, 242, 242,  "2CAP3", "2422200232.", "2CAP3 employé technique de laboratoire"),
( 242033, 0, 0, 242, 242,  "3CAP3", "2429999993.", "3CAP3 employé technique de laboratoire"),
( 242034, 0, 0, 242, 242,  "1CAP3", "2422212931.", "1CAP3 agent polyvalent de restauration"), -- fermé 2020-08-31
( 242035, 0, 0, 242, 242,  "2CAP3", "2422212932.", "2CAP3 agent polyvalent de restauration"), -- fermé 2021-08-31
( 242036, 0, 0, 242, 242,  "3CAP3", "2422212933.", "3CAP3 agent polyvalent de restauration"),
( 242037, 0, 0, 242, 242,  "1CAP3", "2422213031.", "1CAP3 charcutier-traiteur"), -- fermé 2020-08-31
( 242038, 0, 0, 242, 242,  "2CAP3", "2422213032.", "2CAP3 charcutier-traiteur"),
( 242039, 0, 0, 242, 242,  "3CAP3", "2422213033.", "3CAP3 charcutier-traiteur"),
( 242040, 0, 0, 242, 242,  "1CAP3", "2422213331.", "1CAP3 chocolatier confiseur"), -- fermé 2020-08-31
( 242041, 0, 0, 242, 242,  "2CAP3", "2422213332.", "2CAP3 chocolatier confiseur"),
( 242042, 0, 0, 242, 242,  "3CAP3", "2422213333.", "3CAP3 chocolatier confiseur"),
( 242043, 0, 0, 242, 242,  "1CAP3", "2422213531.", "1CAP3 boucher"), -- fermé 2020-08-31
( 242044, 0, 0, 242, 242,  "2CAP3", "2422213532.", "2CAP3 boucher"),
( 242045, 0, 0, 242, 242,  "3CAP3", "2422213533.", "3CAP3 boucher"),
( 242046, 0, 0, 242, 242,  "1CAP3", "2422213731.", "1CAP3 boulanger"), -- fermé 2020-08-31
( 242047, 0, 0, 242, 242,  "2CAP3", "2422213732.", "2CAP3 boulanger"),
( 242048, 0, 0, 242, 242,  "3CAP3", "2422213733.", "3CAP3 boulanger"),
( 242049, 0, 0, 242, 242,  "1CAP3", "2422213831.", "1CAP3 glacier-fabricant"), -- fermé 2020-08-31
( 242050, 0, 0, 242, 242,  "2CAP3", "2422213832.", "2CAP3 glacier-fabricant"),
( 242051, 0, 0, 242, 242,  "3CAP3", "2422213833.", "3CAP3 glacier-fabricant"),
( 242052, 0, 0, 242, 242,  "1CAP3", "2422213931.", "1CAP3 cuisine"), -- fermé 2020-08-31
( 242053, 0, 0, 242, 242,  "2CAP3", "2422213932.", "2CAP3 cuisine"),
( 242054, 0, 0, 242, 242,  "3CAP3", "2422213933.", "3CAP3 cuisine"),
( 242055, 0, 0, 242, 242,  "1CAP3", "2422214031.", "1CAP3 crémier-fromager"), -- fermé 2020-08-31
( 242056, 0, 0, 242, 242,  "2CAP3", "2422214032.", "2CAP3 crémier-fromager"),
( 242057, 0, 0, 242, 242,  "3CAP3", "2422214033.", "3CAP3 crémier-fromager"),
( 242058, 0, 0, 242, 242,  "1CAP3", "2422214131.", "1CAP3 pâtissier"), -- fermé 2020-08-31
( 242059, 0, 0, 242, 242,  "2CAP3", "2422214132.", "2CAP3 pâtissier"),
( 242060, 0, 0, 242, 242,  "3CAP3", "2422214133.", "3CAP3 pâtissier"),
( 242061, 0, 0, 242, 242,  "1CAP3", "2422220131.", "1CAP3 industries chimiques"), -- fermé 2020-08-31
( 242062, 0, 0, 242, 242,  "2CAP3", "2422220132.", "2CAP3 industries chimiques"),
( 242063, 0, 0, 242, 242,  "3CAP3", "2422220133.", "3CAP3 industries chimiques"),
( 242064, 0, 0, 242, 242,  "1CAP3", "2422232431.", "1CAP3 mouleur noyauteur : cuivre bronze"), -- fermé 2020-08-31
( 242065, 0, 0, 242, 242,  "2CAP3", "2422232432.", "2CAP3 mouleur noyauteur : cuivre bronze"),
( 242066, 0, 0, 242, 242,  "3CAP3", "2422232433.", "3CAP3 mouleur noyauteur : cuivre bronze"),
( 242067, 0, 0, 242, 242,  "1CAP3", "2422233231.", "1CAP3 doreur à la feuille ornemaniste"), -- fermé 2020-08-31
( 242068, 0, 0, 242, 242,  "2CAP3", "2422233232.", "2CAP3 doreur à la feuille ornemaniste"),
( 242069, 0, 0, 242, 242,  "3CAP3", "2422233233.", "3CAP3 doreur à la feuille ornemaniste"),
( 242070, 0, 0, 242, 242,  "1CAP3", "2422233931.", "1CAP3 lapidaire option A : diamant"), -- fermé 2020-08-31
( 242071, 0, 0, 242, 242,  "2CAP3", "2422233932.", "2CAP3 lapidaire option A : diamant"),
( 242072, 0, 0, 242, 242,  "3CAP3", "2422233933.", "3CAP3 lapidaire option A : diamant"),
( 242073, 0, 0, 242, 242,  "1CAP3", "2422234031.", "1CAP3 orfèvre option A : monteur orfèvrerie"), -- fermé 2020-08-31
( 242074, 0, 0, 242, 242,  "2CAP3", "2422234032.", "2CAP3 orfèvre option A : monteur orfèvrerie"),
( 242075, 0, 0, 242, 242,  "3CAP3", "2422234033.", "3CAP3 orfèvre option A : monteur orfèvrerie"),
( 242076, 0, 0, 242, 242,  "1CAP3", "2422234131.", "1CAP3 orfèvre option B : tourneur repousseur"), -- fermé 2020-08-31
( 242077, 0, 0, 242, 242,  "2CAP3", "2422234132.", "2CAP3 orfèvre option B : tourneur repousseur"),
( 242078, 0, 0, 242, 242,  "3CAP3", "2422234133.", "3CAP3 orfèvre option B : tourneur repousseur"),
( 242079, 0, 0, 242, 242,  "1CAP3", "2422234231.", "1CAP3 orfèvre option C : polisseur aviveur"), -- fermé 2020-08-31
( 242080, 0, 0, 242, 242,  "2CAP3", "2422234232.", "2CAP3 orfèvre option C : polisseur aviveur"),
( 242081, 0, 0, 242, 242,  "3CAP3", "2422234233.", "3CAP3 orfèvre option C : polisseur aviveur"),
( 242082, 0, 0, 242, 242,  "1CAP3", "2422234331.", "1CAP3 lapidaire option B : pierres de couleur"), -- fermé 2020-08-31
( 242083, 0, 0, 242, 242,  "2CAP3", "2422234332.", "2CAP3 lapidaire option B : pierres de couleur"),
( 242084, 0, 0, 242, 242,  "3CAP3", "2422234333.", "3CAP3 lapidaire option B : pierres de couleur"),
( 242085, 0, 0, 242, 242,  "1CAP3", "2422234931.", "1CAP3 bronzier option A : monteur en bronze"), -- fermé 2020-08-31
( 242086, 0, 0, 242, 242,  "2CAP3", "2422234932.", "2CAP3 bronzier option A : monteur en bronze"),
( 242087, 0, 0, 242, 242,  "3CAP3", "2422234933.", "3CAP3 bronzier option A : monteur en bronze"),
( 242088, 0, 0, 242, 242,  "1CAP3", "2422235031.", "1CAP3 bronzier option B : ciseleur sur bronze"), -- fermé 2020-08-31
( 242089, 0, 0, 242, 242,  "2CAP3", "2422235032.", "2CAP3 bronzier option B : ciseleur sur bronze"),
( 242090, 0, 0, 242, 242,  "3CAP3", "2422235033.", "3CAP3 bronzier option B : ciseleur sur bronze"),
( 242091, 0, 0, 242, 242,  "1CAP3", "2422235131.", "1CAP3 bronzier option C : tourneur sur bronze"), -- fermé 2020-08-31
( 242092, 0, 0, 242, 242,  "2CAP3", "2422235132.", "2CAP3 bronzier option C : tourneur sur bronze"),
( 242093, 0, 0, 242, 242,  "3CAP3", "2422235133.", "3CAP3 bronzier option C : tourneur sur bronze"),
( 242094, 0, 0, 242, 242,  "1CAP3", "2422235331.", "1CAP3 emailleur d’art sur métaux"), -- fermé 2020-08-31
( 242095, 0, 0, 242, 242,  "2CAP3", "2422235332.", "2CAP3 emailleur d’art sur métaux"),
( 242096, 0, 0, 242, 242,  "3CAP3", "2422235333.", "3CAP3 emailleur d’art sur métaux"),
( 242097, 0, 0, 242, 242,  "1CAP3", "2422235431.", "1CAP3 orfèvre option D : planeur en orfèvrerie"), -- fermé 2020-08-31
( 242098, 0, 0, 242, 242,  "2CAP3", "2422235432.", "2CAP3 orfèvre option D : planeur en orfèvrerie"),
( 242099, 0, 0, 242, 242,  "3CAP3", "2422235433.", "3CAP3 orfèvre option D : planeur en orfèvrerie"),
( 242100, 0, 0, 242, 242,  "1CAP3", "2422236031.", "1CAP3 métiers de la fonderie"), -- fermé 2020-08-31
( 242101, 0, 0, 242, 242,  "2CAP3", "2422236032.", "2CAP3 métiers de la fonderie"),
( 242102, 0, 0, 242, 242,  "3CAP3", "2422236033.", "3CAP3 métiers de la fonderie"),
( 242103, 0, 0, 242, 242,  "1CAP3", "2422236131.", "1CAP3 art tec. bij. joaill. option bijouterie joaillerie"), -- fermé 2020-08-31
( 242104, 0, 0, 242, 242,  "2CAP3", "2422236132.", "2CAP3 art tec. bij. joaill option bijouterie joaillerie"),
( 242105, 0, 0, 242, 242,  "3CAP3", "2422236133.", "3CAP3 art tec. bij. joaill option bijouterie joaillerie"),
( 242106, 0, 0, 242, 242,  "1CAP3", "2422236231.", "1CAP3 art tec. bij. joaill option bijouterie sertissage"), -- fermé 2020-08-31
( 242107, 0, 0, 242, 242,  "2CAP3", "2422236232.", "2CAP3 art tec. bij. joaill option bijouterie sertissage"),
( 242108, 0, 0, 242, 242,  "3CAP3", "2422236233.", "3CAP3 art tec. bij. joaill option bijouterie sertissage"),
( 242109, 0, 0, 242, 242,  "1CAP3", "2422236331.", "1CAP3 art tec. bij. joaill option polisseur finisseur"), -- fermé 2020-08-31
( 242110, 0, 0, 242, 242,  "2CAP3", "2422236332.", "2CAP3 art tec. bij. joaill option polisseur finisseur"),
( 242111, 0, 0, 242, 242,  "3CAP3", "2422236333.", "3CAP3 art tec. bij. joaill option polisseur finisseur"),
( 242112, 0, 0, 242, 242,  "1CAP3", "2422240631.", "1CAP3 maintenance industrielle matériaux de construction"), -- fermé 2020-08-31
( 242113, 0, 0, 242, 242,  "2CAP3", "2422240632.", "2CAP3 maintenance industrielle matériaux de construction"),
( 242114, 0, 0, 242, 242,  "3CAP3", "2422240633.", "3CAP3 maintenance industrielle matériaux de construction"),
( 242115, 0, 0, 242, 242,  "1CAP3", "2422241931.", "1CAP3 fabrication industrielle des céramiques"), -- fermé 2020-08-31
( 242116, 0, 0, 242, 242,  "2CAP3", "2422241932.", "2CAP3 fabrication industrielle des céramiques"),
( 242117, 0, 0, 242, 242,  "3CAP3", "2422241933.", "3CAP3 fabrication industrielle des céramiques"),
( 242118, 0, 0, 242, 242,  "1CAP3", "2422242031.", "1CAP3 modèles et moules céramiques"), -- fermé 2020-08-31
( 242119, 0, 0, 242, 242,  "2CAP3", "2422242032.", "2CAP3 modèles et moules céramiques"),
( 242120, 0, 0, 242, 242,  "3CAP3", "2422242033.", "3CAP3 modèles et moules céramiques"),
( 242121, 0, 0, 242, 242,  "1CAP3", "2422242131.", "1CAP3 tournage en céramique"), -- fermé 2020-08-31
( 242122, 0, 0, 242, 242,  "2CAP3", "2422242132.", "2CAP3 tournage en céramique"),
( 242123, 0, 0, 242, 242,  "3CAP3", "2422242133.", "3CAP3 tournage en céramique"),
( 242124, 0, 0, 242, 242,  "1CAP3", "2422242231.", "1CAP3 décoration en céramique"), -- fermé 2020-08-31
( 242125, 0, 0, 242, 242,  "2CAP3", "2422242232.", "2CAP3 décoration en céramique"),
( 242126, 0, 0, 242, 242,  "3CAP3", "2422242233.", "3CAP3 décoration en céramique"),
( 242127, 0, 0, 242, 242,  "1CAP3", "2422242531.", "1CAP3 arts & tech.verre : vitrailliste"), -- fermé 2020-08-31
( 242128, 0, 0, 242, 242,  "2CAP3", "2422242532.", "2CAP3 arts & tech.verre : vitrailliste"),
( 242129, 0, 0, 242, 242,  "3CAP3", "2422242533.", "3CAP3 arts & tech.verre : vitrailliste"),
( 242130, 0, 0, 242, 242,  "1CAP3", "2422242731.", "1CAP3 arts & tech.verre : décorateur"),
( 242131, 0, 0, 242, 242,  "2CAP3", "2422242732.", "2CAP3 arts & tech.verre : décorateur"), -- fermé 2020-08-31
( 242132, 0, 0, 242, 242,  "3CAP3", "2422242733.", "3CAP3 arts & tech.verre : décorateur"),
( 242133, 0, 0, 242, 242,  "1CAP3", "2422242831.", "1CAP3 arts du verre et du cristal"), -- fermé 2020-08-31
( 242134, 0, 0, 242, 242,  "2CAP3", "2422242832.", "2CAP3 arts du verre et du cristal"),
( 242135, 0, 0, 242, 242,  "3CAP3", "2422242833.", "3CAP3 arts du verre et du cristal"),
( 242136, 0, 0, 242, 242,  "1CAP3", "2422242931.", "1CAP3 souffleur verre : ens.lumineuse"), -- fermé 2020-08-31
( 242137, 0, 0, 242, 242,  "2CAP3", "2422242932.", "2CAP3 souffleur verre : ens.lumineuse"),
( 242138, 0, 0, 242, 242,  "3CAP3", "2422242933.", "3CAP3 souffleur verre : ens.lumineuse"),
( 242139, 0, 0, 242, 242,  "1CAP3", "2422243031.", "1CAP3 souffleur verre : verrerie scient."), -- fermé 2020-08-31
( 242140, 0, 0, 242, 242,  "2CAP3", "2422243032.", "2CAP3 souffleur verre : verrerie scient."),
( 242141, 0, 0, 242, 242,  "3CAP3", "2422243033.", "3CAP3 souffleur verre : verrerie scient."),
( 242142, 0, 0, 242, 242,  "1CAP3", "2422250531.", "1CAP3 mise en oeuvre caoutchoucs & elasto"), -- fermé 2020-08-31
( 242143, 0, 0, 242, 242,  "2CAP3", "2422250532.", "2CAP3 mise en oeuvre caoutchoucs & elasto"),
( 242144, 0, 0, 242, 242,  "3CAP3", "2422250533.", "3CAP3 mise en oeuvre caoutchoucs & elasto"),
( 242145, 0, 0, 242, 242,  "1CAP3", "2422250931.", "1CAP3 plasturgie"), -- fermé 2020-08-31
( 242146, 0, 0, 242, 242,  "2CAP3", "2422250932.", "2CAP3 plasturgie"),
( 242147, 0, 0, 242, 242,  "3CAP3", "2422250933.", "3CAP3 plasturgie"),
( 242148, 0, 0, 242, 242,  "1CAP3", "2422251031.", "1CAP3 composit.plastiq.chaudronnes"), -- fermé 2020-08-31
( 242149, 0, 0, 242, 242,  "2CAP3", "2422251032.", "2CAP3 composit.plastiq.chaudronnes"),
( 242150, 0, 0, 242, 242,  "3CAP3", "2422251033.", "3CAP3 composit.plastiq.chaudronnes"),
( 242151, 0, 0, 242, 242,  "1CAP3", "2422260931.", "1CAP3 scieries option B : affuteur sciage"), -- fermé 2020-08-31
( 242152, 0, 0, 242, 242,  "2CAP3", "2422260932.", "2CAP3 scieries option B : affuteur sciage"),
( 242153, 0, 0, 242, 242,  "3CAP3", "2422260933.", "3CAP3 scieries option B : affuteur sciage"),
( 242154, 0, 0, 242, 242,  "1CAP3", "2422271431.", "1CAP3 installateur froid conditionnement d’air"), -- fermé 2020-08-31
( 242155, 0, 0, 242, 242,  "2CAP3", "2422271432.", "2CAP3 installateur froid conditionnement d’air"),
( 242156, 0, 0, 242, 242,  "3CAP3", "2422271433.", "3CAP3 installateur froid conditionnement d’air"),
( 242157, 0, 0, 242, 242,  "1CAP3", "2422271531.", "1CAP3 monteur installations thermiques"), -- fermé 2020-08-31
( 242158, 0, 0, 242, 242,  "2CAP3", "2422271532.", "2CAP3 monteur installations thermiques"),
( 242159, 0, 0, 242, 242,  "3CAP3", "2422271533.", "3CAP3 monteur installations thermiques"),
( 242160, 0, 0, 242, 242,  "1CAP3", "2422300231.", "1CAP3 maintenance bâtiments de collectivités"), -- fermé 2020-08-31
( 242161, 0, 0, 242, 242,  "2CAP3", "2422300632.", "2CAP3 intervention en maintenance technique des bâtiments"), -- MEF modifiée en 2022
( 242162, 0, 0, 242, 242,  "3CAP3", "2422300633.", "3CAP3 intervention en maintenance technique des bâtiments"), -- MEF modifiée en 2022
( 242163, 0, 0, 242, 242,  "1CAP3", "2422300531.", "1CAP3 monteur structures mobiles"), -- fermé 2020-08-31
( 242164, 0, 0, 242, 242,  "2CAP3", "2422300532.", "2CAP3 monteur structures mobiles"),
( 242165, 0, 0, 242, 242,  "3CAP3", "2422300533.", "3CAP3 monteur structures mobiles"),
( 242166, 0, 0, 242, 242,  "1CAP3", "2422311531.", "1CAP3 constructeur de routes"), -- fermé 2020-08-31
( 242167, 0, 0, 242, 242,  "2CAP3", "2422312032.", "2CAP3 constructeur de routes et d’aménagements urbains"), -- MEF modifiée en 2022
( 242168, 0, 0, 242, 242,  "3CAP3", "2422312033.", "3CAP3 constructeur de routes et d’aménagements urbains"), -- MEF modifiée en 2022
( 242169, 0, 0, 242, 242,  "1CAP3", "2422311831.", "1CAP3 conducteur engins travaux publics carrières"), -- fermé 2020-08-31
( 242170, 0, 0, 242, 242,  "2CAP3", "2422311832.", "2CAP3 conducteur engins travaux publics carrières"),
( 242171, 0, 0, 242, 242,  "3CAP3", "2422311833.", "3CAP3 conducteur engins travaux publics carrières"),
( 242172, 0, 0, 242, 242,  "1CAP3", "2422311931.", "1CAP3 constructeur de réseaux de canalisations de travaux publics"), -- fermé 2020-08-31
( 242173, 0, 0, 242, 242,  "2CAP3", "2422311932.", "2CAP3 constructeur de réseaux de canalisations de travaux publics"),
( 242174, 0, 0, 242, 242,  "3CAP3", "2422311933.", "3CAP3 constructeur de réseaux de canalisations de travaux publics"),
( 242175, 0, 0, 242, 242,  "1CAP3", "2422321731.", "1CAP3 maçon"), -- fermé 2020-08-31
( 242176, 0, 0, 242, 242,  "2CAP3", "2422322432.", "2CAP3 maçon"), -- MEF modifiée en 2022
( 242177, 0, 0, 242, 242,  "3CAP3", "2422322433.", "3CAP3 maçon"), -- MEF modifiée en 2022
( 242178, 0, 0, 242, 242,  "1CAP3", "2422321831.", "1CAP3 couvreur"), -- fermé 2020-08-31
( 242179, 0, 0, 242, 242,  "2CAP3", "2422322332.", "2CAP3 couvreur"), -- MEF modifiée en 2022
( 242180, 0, 0, 242, 242,  "3CAP3", "2422322333.", "3CAP3 couvreur"), -- MEF modifiée en 2022
( 242181, 0, 0, 242, 242,  "1CAP3", "2422322031.", "1CAP3 tailleur de pierre"), -- fermé 2020-08-31
( 242182, 0, 0, 242, 242,  "2CAP3", "2422322032.", "2CAP3 tailleur de pierre"),
( 242183, 0, 0, 242, 242,  "3CAP3", "2422322033.", "3CAP3 tailleur de pierre"),
( 242184, 0, 0, 242, 242,  "1CAP3", "2422322131.", "1CAP3 marbrier bâtiment et décoration"), -- fermé 2020-08-31
( 242185, 0, 0, 242, 242,  "2CAP3", "2422322132.", "2CAP3 marbrier bâtiment et décoration"),
( 242186, 0, 0, 242, 242,  "3CAP3", "2422322133.", "3CAP3 marbrier bâtiment et décoration"),
( 242187, 0, 0, 242, 242,  "1CAP3", "2422322231.", "1CAP3 constructeur d’ouvrages en béton armé"), -- fermé 2020-08-31
( 242188, 0, 0, 242, 242,  "2CAP3", "2422322232.", "2CAP3 constructeur d’ouvrages en béton armé"),
( 242189, 0, 0, 242, 242,  "3CAP3", "2422322233.", "3CAP3 constructeur d’ouvrages en béton armé"),
( 242190, 0, 0, 242, 242,  "1CAP3", "2422330231.", "1CAP3 monteur en isolation thermique & acoustique"), -- fermé 2020-08-31
( 242191, 0, 0, 242, 242,  "2CAP3", "2422330232.", "2CAP3 monteur en isolation thermique & acoustique"),
( 242192, 0, 0, 242, 242,  "3CAP3", "2422330233.", "3CAP3 monteur en isolation thermique & acoustique"),
( 242193, 0, 0, 242, 242,  "1CAP3", "2422331131.", "1CAP3 staffeur ornemaniste"), -- fermé 2020-08-31
( 242194, 0, 0, 242, 242,  "2CAP3", "2422331132.", "2CAP3 staffeur ornemaniste"),
( 242195, 0, 0, 242, 242,  "3CAP3", "2422331133.", "3CAP3 staffeur ornemaniste"),
( 242196, 0, 0, 242, 242,  "1CAP3", "2422332131.", "1CAP3 étancheur bâtiment travaux public"), -- fermé 2020-08-31
( 242197, 0, 0, 242, 242,  "2CAP3", "2422332132.", "2CAP3 étancheur bâtiment travaux public"),
( 242198, 0, 0, 242, 242,  "3CAP3", "2422332133.", "3CAP3 étancheur bâtiment travaux public"),
( 242199, 0, 0, 242, 242,  "1CAP3", "2422332231.", "1CAP3 menuisier aluminium-verre"), -- fermé 2020-08-31
( 242200, 0, 0, 242, 242,  "2CAP3", "2422332232.", "2CAP3 menuisier aluminium-verre"),
( 242201, 0, 0, 242, 242,  "3CAP3", "2422332233.", "3CAP3 menuisier aluminium-verre"),
( 242202, 0, 0, 242, 242,  "1CAP3", "2422332431.", "1CAP3 monteur installations sanitaires"), -- fermé 2020-08-31
( 242203, 0, 0, 242, 242,  "2CAP3", "2422332432.", "2CAP3 monteur installations sanitaires"),
( 242204, 0, 0, 242, 242,  "3CAP3", "2422332433.", "3CAP3 monteur installations sanitaires"),
( 242205, 0, 0, 242, 242,  "1CAP3", "2422332531.", "1CAP3 carreleur mosaïste"), -- fermé 2020-08-31
( 242206, 0, 0, 242, 242,  "2CAP3", "2422332532.", "2CAP3 carreleur mosaïste"),
( 242207, 0, 0, 242, 242,  "3CAP3", "2422332533.", "3CAP3 carreleur mosaïste"),
( 242208, 0, 0, 242, 242,  "1CAP3", "2422332631.", "1CAP3 métiers du plâtre et de l’isolation"), -- fermé 2020-08-31
( 242209, 0, 0, 242, 242,  "2CAP3", "2422332632.", "2CAP3 métiers du plâtre et de l’isolation"),
( 242210, 0, 0, 242, 242,  "3CAP3", "2422332633.", "3CAP3 métiers du plâtre et de l’isolation"),
( 242211, 0, 0, 242, 242,  "1CAP3", "2422332731.", "1CAP3 peintre applicateur de revêtements"), -- fermé 2020-08-31
( 242212, 0, 0, 242, 242,  "2CAP3", "2422332732.", "2CAP3 peintre applicateur de revêtements"),
( 242213, 0, 0, 242, 242,  "3CAP3", "2422332733.", "3CAP3 peintre applicateur de revêtements"),
( 242214, 0, 0, 242, 242,  "1CAP3", "2422341131.", "1CAP3 menuisier en sièges"), -- fermé 2020-08-31
( 242215, 0, 0, 242, 242,  "2CAP3", "2422341132.", "2CAP3 menuisier en sièges"),
( 242216, 0, 0, 242, 242,  "3CAP3", "2422341133.", "3CAP3 menuisier en sièges"),
( 242217, 0, 0, 242, 242,  "1CAP3", "2422342031.", "1CAP3 cannage et paillage ameublement"), -- fermé 2020-08-31
( 242218, 0, 0, 242, 242,  "2CAP3", "2422342032.", "2CAP3 cannage et paillage ameublement"),
( 242219, 0, 0, 242, 242,  "3CAP3", "2422342033.", "3CAP3 cannage et paillage ameublement"),
( 242220, 0, 0, 242, 242,  "1CAP3", "2422342731.", "1CAP3 ouvrier archetier"), -- fermé 2020-08-31
( 242221, 0, 0, 242, 242,  "2CAP3", "2422342732.", "2CAP3 ouvrier archetier"),
( 242222, 0, 0, 242, 242,  "3CAP3", "2422342733.", "3CAP3 ouvrier archetier"),
( 242223, 0, 0, 242, 242,  "1CAP3", "2422342831.", "1CAP3 lutherie"), -- fermé 2020-08-31
( 242224, 0, 0, 242, 242,  "2CAP3", "2422342832.", "2CAP3 lutherie"),
( 242225, 0, 0, 242, 242,  "3CAP3", "2422342833.", "3CAP3 lutherie"),
( 242226, 0, 0, 242, 242,  "1CAP3", "2422343031.", "1CAP3 arts bois option A : sculpteur ornemant"), -- fermé 2020-08-31
( 242227, 0, 0, 242, 242,  "2CAP3", "2422343032.", "2CAP3 arts bois option A : sculpteur ornemant"),
( 242228, 0, 0, 242, 242,  "3CAP3", "2422343033.", "3CAP3 arts bois option A : sculpteur ornemant"),
( 242229, 0, 0, 242, 242,  "1CAP3", "2422343131.", "1CAP3 arts bois option B : tourneur"), -- fermé 2020-08-31
( 242230, 0, 0, 242, 242,  "2CAP3", "2422343132.", "2CAP3 arts bois option B : tourneur"),
( 242231, 0, 0, 242, 242,  "3CAP3", "2422343133.", "3CAP3 arts bois option B : tourneur"),
( 242232, 0, 0, 242, 242,  "1CAP3", "2422343231.", "1CAP3 arts bois option C : marqueteur"), -- fermé 2020-08-31
( 242233, 0, 0, 242, 242,  "2CAP3", "2422343232.", "2CAP3 arts bois option C : marqueteur"),
( 242234, 0, 0, 242, 242,  "3CAP3", "2422343233.", "3CAP3 arts bois option C : marqueteur"),
( 242235, 0, 0, 242, 242,  "1CAP3", "2422343331.", "1CAP3 encadreur"), -- fermé 2020-08-31
( 242236, 0, 0, 242, 242,  "2CAP3", "2422343332.", "2CAP3 encadreur"),
( 242237, 0, 0, 242, 242,  "3CAP3", "2422343333.", "3CAP3 encadreur"),
( 242238, 0, 0, 242, 242,  "1CAP3", "2422343531.", "1CAP3 tonnellerie"), -- fermé 2020-08-31
( 242239, 0, 0, 242, 242,  "2CAP3", "2422343532.", "2CAP3 tonnellerie"),
( 242240, 0, 0, 242, 242,  "3CAP3", "2422343533.", "3CAP3 tonnellerie"),
( 242241, 0, 0, 242, 242,  "1CAP3", "2422343831.", "1CAP3 vannerie"), -- fermé 2020-08-31
( 242242, 0, 0, 242, 242,  "2CAP3", "2422343832.", "2CAP3 vannerie"),
( 242243, 0, 0, 242, 242,  "3CAP3", "2422343833.", "3CAP3 vannerie"),
( 242244, 0, 0, 242, 242,  "1CAP3", "2422344631.", "1CAP3 charpentier bois"), -- fermé 2020-08-31
( 242245, 0, 0, 242, 242,  "2CAP3", "2422344632.", "2CAP3 charpentier bois"),
( 242246, 0, 0, 242, 242,  "3CAP3", "2422344633.", "3CAP3 charpentier bois"),
( 242247, 0, 0, 242, 242,  "1CAP3", "2422344031.", "1CAP3 constructeur bois"), -- fermé 2020-08-31
( 242248, 0, 0, 242, 242,  "2CAP3", "2422344032.", "2CAP3 constructeur bois"),
( 242249, 0, 0, 242, 242,  "3CAP3", "2422344033.", "3CAP3 constructeur bois"),
( 242250, 0, 0, 242, 242,  "1CAP3", "2422344131.", "1CAP3 menuisier fabricant men mob agencement"), -- fermé 2020-08-31
( 242251, 0, 0, 242, 242,  "2CAP3", "2422344732.", "2CAP3 menuisier fabricant"), -- MEF modifiée en 2022
( 242252, 0, 0, 242, 242,  "3CAP3", "2422344733.", "3CAP3 menuisier fabricant"), -- MEF modifiée en 2022
( 242253, 0, 0, 242, 242,  "1CAP3", "2422344231.", "1CAP3 menuisier installateur"), -- fermé 2020-08-31
( 242254, 0, 0, 242, 242,  "2CAP3", "2422344932.", "2CAP3 menuisier installateur"), -- MEF modifiée en 2022
( 242255, 0, 0, 242, 242,  "3CAP3", "2422344933.", "3CAP3 menuisier installateur"), -- MEF modifiée en 2022
( 242256, 0, 0, 242, 242,  "1CAP3", "2422344331.", "1CAP3 conducteur-opérateur de scierie"), -- fermé 2020-08-31
( 242257, 0, 0, 242, 242,  "2CAP3", "2422344332.", "2CAP3 conducteur-opérateur de scierie"),
( 242258, 0, 0, 242, 242,  "3CAP3", "2422344333.", "3CAP3 conducteur-opérateur de scierie"),
( 242259, 0, 0, 242, 242,  "1CAP3", "2422344431.", "1CAP3 charpentier de marine"), -- fermé 2020-08-31
( 242260, 0, 0, 242, 242,  "2CAP3", "2422344432.", "2CAP3 charpentier de marine"),
( 242261, 0, 0, 242, 242,  "3CAP3", "2422344433.", "3CAP3 charpentier de marine"),
( 242262, 0, 0, 242, 242,  "1CAP3", "2422344531.", "1CAP3 ébéniste"), -- fermé 2020-08-31
( 242263, 0, 0, 242, 242,  "2CAP3", "2422344532.", "2CAP3 ébéniste"),
( 242264, 0, 0, 242, 242,  "3CAP3", "2422344533.", "3CAP3 ébéniste"),
( 242265, 0, 0, 242, 242,  "1CAP3", "2422400531.", "1CAP3 métier du pressing"), -- fermé 2020-08-31
( 242266, 0, 0, 242, 242,  "2CAP3", "2422400832.", "2CAP3 métiers de l’entretien des textiles option B : pressing"), -- MEF modifiée en 2022
( 242267, 0, 0, 242, 242,  "3CAP3", "2422400833.", "3CAP3 métiers de l’entretien des textiles option B : pressing"), -- MEF modifiée en 2022
( 242268, 0, 0, 242, 242,  "1CAP3", "2422400631.", "1CAP3 métiers de la blanchisserie"), -- fermé 2020-08-31
( 242269, 0, 0, 242, 242,  "2CAP3", "2422400732.", "2CAP3 métiers de l’entretien des textiles option A : blanchisserie"), -- MEF modifiée en 2022
( 242270, 0, 0, 242, 242,  "3CAP3", "2422400733.", "3CAP3 métiers de l’entretien des textiles option A : blanchisserie"), -- MEF modifiée en 2022
( 242271, 0, 0, 242, 242,  "1CAP3", "2422412531.", "1CAP3 rentrayeur option A : tapis"), -- fermé 2020-08-31
( 242272, 0, 0, 242, 242,  "2CAP3", "2422412532.", "2CAP3 rentrayeur option A : tapis"),
( 242273, 0, 0, 242, 242,  "3CAP3", "2422412533.", "3CAP3 rentrayeur option A : tapis"),
( 242274, 0, 0, 242, 242,  "1CAP3", "2422412631.", "1CAP3 rentrayeur option B : tapisseries"), -- fermé 2020-08-31
( 242275, 0, 0, 242, 242,  "2CAP3", "2422412632.", "2CAP3 rentrayeur option B : tapisseries"),
( 242276, 0, 0, 242, 242,  "3CAP3", "2422412633.", "3CAP3 rentrayeur option B : tapisseries"),
( 242277, 0, 0, 242, 242,  "1CAP3", "2422412731.", "1CAP3 arts de la broderie"), -- fermé 2020-08-31
( 242278, 0, 0, 242, 242,  "2CAP3", "2422412732.", "2CAP3 arts de la broderie"),
( 242279, 0, 0, 242, 242,  "3CAP3", "2422412733.", "3CAP3 arts de la broderie"),
( 242280, 0, 0, 242, 242,  "1CAP3", "2422413031.", "1CAP3 arts de la dentelle option fuseaux"), -- fermé 2020-08-31
( 242281, 0, 0, 242, 242,  "2CAP3", "2422413032.", "2CAP3 arts de la dentelle option fuseaux"),
( 242282, 0, 0, 242, 242,  "3CAP3", "2422413033.", "3CAP3 arts de la dentelle option fuseaux"),
( 242283, 0, 0, 242, 242,  "1CAP3", "2422413131.", "1CAP3 arts de la dentelle option aiguille"), -- fermé 2020-08-31
( 242284, 0, 0, 242, 242,  "2CAP3", "2422413132.", "2CAP3 arts de la dentelle option aiguille"),
( 242285, 0, 0, 242, 242,  "3CAP3", "2422413133.", "3CAP3 arts de la dentelle option aiguille"),
( 242286, 0, 0, 242, 242,  "1CAP3", "2422423131.", "1CAP3 arts du tapis et tapisserie de lisse"), -- fermé 2020-08-31
( 242287, 0, 0, 242, 242,  "2CAP3", "2422423132.", "2CAP3 arts du tapis et tapisserie de lisse"),
( 242288, 0, 0, 242, 242,  "3CAP3", "2422423133.", "3CAP3 arts du tapis et tapisserie de lisse"),
( 242289, 0, 0, 242, 242,  "1CAP3", "2422423831.", "1CAP3 tapissier-e ameublement en siège"), -- fermé 2020-08-31
( 242290, 0, 0, 242, 242,  "2CAP3", "2422423832.", "2CAP3 tapissier-e ameublement en siège"),
( 242291, 0, 0, 242, 242,  "3CAP3", "2422423833.", "3CAP3 tapissier-e ameublement en siège"),
( 242292, 0, 0, 242, 242,  "1CAP3", "2422423931.", "1CAP3 tapissier-e ameublement en decor"), -- fermé 2020-08-31
( 242293, 0, 0, 242, 242,  "2CAP3", "2422423932.", "2CAP3 tapissier-e ameublement en decor"),
( 242294, 0, 0, 242, 242,  "3CAP3", "2422423933.", "3CAP3 tapissier-e ameublement en decor"),
( 242295, 0, 0, 242, 242,  "1CAP3", "2422424031.", "1CAP3 métiers de la mode-vêtement flou"), -- fermé 2020-08-31
( 242296, 0, 0, 242, 242,  "2CAP3", "2422424032.", "2CAP3 métiers de la mode-vêtement flou"),
( 242297, 0, 0, 242, 242,  "3CAP3", "2422424033.", "3CAP3 métiers de la mode-vêtement flou"),
( 242298, 0, 0, 242, 242,  "1CAP3", "2422424131.", "1CAP3 métiers de la mode-vêtement tailleur"), -- fermé 2020-08-31
( 242299, 0, 0, 242, 242,  "2CAP3", "2422424132.", "2CAP3 métiers de la mode-vêtement tailleur"),
( 242300, 0, 0, 242, 242,  "3CAP3", "2422424133.", "3CAP3 métiers de la mode-vêtement tailleur"),
( 242301, 0, 0, 242, 242,  "1CAP3", "2422424231.", "1CAP3 métiers mode chapelier-modiste"), -- fermé 2020-08-31
( 242302, 0, 0, 242, 242,  "2CAP3", "2422424232.", "2CAP3 métiers mode chapelier-modiste"),
( 242303, 0, 0, 242, 242,  "3CAP3", "2422424233.", "3CAP3 métiers mode chapelier-modiste"),
( 242304, 0, 0, 242, 242,  "1CAP3", "2422431331.", "1CAP3 cordonnier bottier"), -- fermé 2020-08-31
( 242305, 0, 0, 242, 242,  "2CAP3", "2422431332.", "2CAP3 cordonnier bottier"),
( 242306, 0, 0, 242, 242,  "3CAP3", "2422431333.", "3CAP3 cordonnier bottier"),
( 242307, 0, 0, 242, 242,  "1CAP3", "2422431531.", "1CAP3 fourrure"), -- fermé 2020-08-31
( 242308, 0, 0, 242, 242,  "2CAP3", "2422431532.", "2CAP3 fourrure"),
( 242309, 0, 0, 242, 242,  "3CAP3", "2422431533.", "3CAP3 fourrure"),
( 242310, 0, 0, 242, 242,  "1CAP3", "2422431631.", "1CAP3 chaussure"), -- fermé 2020-08-31
( 242311, 0, 0, 242, 242,  "2CAP3", "2422431632.", "2CAP3 chaussure"),
( 242312, 0, 0, 242, 242,  "3CAP3", "2422431633.", "3CAP3 chaussure"),
( 242313, 0, 0, 242, 242,  "1CAP3", "2422431831.", "1CAP3 sellerie générale"), -- fermé 2020-08-31
( 242314, 0, 0, 242, 242,  "2CAP3", "2422431832.", "2CAP3 sellerie générale"),
( 242315, 0, 0, 242, 242,  "3CAP3", "2422431833.", "3CAP3 sellerie générale"),
( 242316, 0, 0, 242, 242,  "1CAP3", "2422431931.", "1CAP3 vêtement de peau"), -- fermé 2020-08-31
( 242317, 0, 0, 242, 242,  "2CAP3", "2422431932.", "2CAP3 vêtement de peau"),
( 242318, 0, 0, 242, 242,  "3CAP3", "2422431933.", "3CAP3 vêtement de peau"),
( 242319, 0, 0, 242, 242,  "1CAP3", "2422432031.", "1CAP3 cordonnerie multiservice"), -- fermé 2020-08-31
( 242320, 0, 0, 242, 242,  "2CAP3", "2422432032.", "2CAP3 cordonnerie multiservice"),
( 242321, 0, 0, 242, 242,  "3CAP3", "2422432033.", "3CAP3 cordonnerie multiservice"),
( 242322, 0, 0, 242, 242,  "1CAP3", "2422432131.", "1CAP3 sellier harnacheur"), -- fermé 2020-08-31
( 242323, 0, 0, 242, 242,  "2CAP3", "2422432132.", "2CAP3 sellier harnacheur"),
( 242324, 0, 0, 242, 242,  "3CAP3", "2422432133.", "3CAP3 sellier harnacheur"),
( 242325, 0, 0, 242, 242,  "1CAP3", "2422432231.", "1CAP3 maroquinerie"), -- fermé 2020-08-31
( 242326, 0, 0, 242, 242,  "2CAP3", "2422432232.", "2CAP3 maroquinerie"),
( 242327, 0, 0, 242, 242,  "3CAP3", "2422432233.", "3CAP3 maroquinerie"),
( 242328, 0, 0, 242, 242,  "1CAP3", "2422510831.", "1CAP3 instruments coupants & de chirurgie"), -- fermé 2020-08-31
( 242329, 0, 0, 242, 242,  "2CAP3", "2422510832.", "2CAP3 instruments coupants & de chirurgie"),
( 242330, 0, 0, 242, 242,  "3CAP3", "2422510833.", "3CAP3 instruments coupants & de chirurgie"),
( 242331, 0, 0, 242, 242,  "1CAP3", "2422512331.", "1CAP3 décolletage : opérateur regleur"), -- fermé 2020-08-31
( 242332, 0, 0, 242, 242,  "2CAP3", "2422512332.", "2CAP3 décolletage : opérateur regleur"),
( 242333, 0, 0, 242, 242,  "3CAP3", "2422512333.", "3CAP3 décolletage : opérateur regleur"),
( 242334, 0, 0, 242, 242,  "1CAP3", "2422512631.", "1CAP3 outillages à découper et emboutir"), -- fermé 2020-08-31
( 242335, 0, 0, 242, 242,  "2CAP3", "2422512632.", "2CAP3 outillages à découper et emboutir"),
( 242336, 0, 0, 242, 242,  "3CAP3", "2422512633.", "3CAP3 outillages à découper et emboutir"),
( 242337, 0, 0, 242, 242,  "1CAP3", "2422513331.", "1CAP3 agent verif. appar. extincteurs"), -- fermé 2020-08-31
( 242338, 0, 0, 242, 242,  "2CAP3", "2422513332.", "2CAP3 agent verif. appar. extincteurs"),
( 242339, 0, 0, 242, 242,  "3CAP3", "2422513333.", "3CAP3 agent verif. appar. extincteurs"),
( 242340, 0, 0, 242, 242,  "1CAP3", "2422513431.", "1CAP3 transport câbles-remontées mécaniques"), -- fermé 2020-08-31
( 242341, 0, 0, 242, 242,  "2CAP3", "2422513432.", "2CAP3 transport câbles-remontées mécaniques"),
( 242342, 0, 0, 242, 242,  "3CAP3", "2422513433.", "3CAP3 transport câbles-remontées mécaniques"),
( 242343, 0, 0, 242, 242,  "1CAP3", "2422513631.", "1CAP3 armurerie (fabrication et réparation)"), -- fermé 2020-08-31
( 242344, 0, 0, 242, 242,  "2CAP3", "2422513632.", "2CAP3 armurerie (fabrication et réparation)"),
( 242345, 0, 0, 242, 242,  "3CAP3", "2422513633.", "3CAP3 armurerie (fabrication et réparation)"),
( 242346, 0, 0, 242, 242,  "1CAP3", "2422513731.", "1CAP3 horlogerie"), -- fermé 2020-08-31
( 242347, 0, 0, 242, 242,  "2CAP3", "2422513732.", "2CAP3 horlogerie"),
( 242348, 0, 0, 242, 242,  "3CAP3", "2422513733.", "3CAP3 horlogerie"),
( 242349, 0, 0, 242, 242,  "1CAP3", "2422521731.", "1CAP3 réparation entretien embarcations plaisance"), -- fermé 2020-08-31
( 242350, 0, 0, 242, 242,  "2CAP3", "2422521732.", "2CAP3 réparation entretien embarcations plaisance"),
( 242351, 0, 0, 242, 242,  "3CAP3", "2422521733.", "3CAP3 réparation entretien embarcations plaisance"),
( 242352, 0, 0, 242, 242,  "1CAP3", "2422521831.", "1CAP3 maintenance des véhicules option A voitures particulières"), -- fermé 2020-08-31
( 242353, 0, 0, 242, 242,  "2CAP3", "2422521832.", "2CAP3 maintenance des véhicules option A voitures particulières"),
( 242354, 0, 0, 242, 242,  "3CAP3", "2422521833.", "3CAP3 maintenance des véhicules option A voitures particulières"),
( 242355, 0, 0, 242, 242,  "1CAP3", "2422521931.", "1CAP3 maintenance des véhicules option B vehicules transport routier"), -- fermé 2020-08-31
( 242356, 0, 0, 242, 242,  "2CAP3", "2422521932.", "2CAP3 maintenance des véhicules option B vehicules transport routier"),
( 242357, 0, 0, 242, 242,  "3CAP3", "2422521933.", "3CAP3 maintenance des véhicules option B vehicules transport routier"),
( 242358, 0, 0, 242, 242,  "1CAP3", "2422522031.", "1CAP3 maintenance des véhicules option C motocycles"), -- fermé 2020-08-31
( 242359, 0, 0, 242, 242,  "2CAP3", "2422522032.", "2CAP3 maintenance des véhicules option C motocycles"),
( 242360, 0, 0, 242, 242,  "3CAP3", "2422522033.", "3CAP3 maintenance des véhicules option C motocycles"),
( 242361, 0, 0, 242, 242,  "1CAP3", "2422522131.", "1CAP3 maintenance des matériels option A agricoles"), -- fermé 2020-08-31
( 242362, 0, 0, 242, 242,  "2CAP3", "2422522132.", "2CAP3 maintenance des matériels option A agricoles"),
( 242363, 0, 0, 242, 242,  "3CAP3", "2422522133.", "3CAP3 maintenance des matériels option A agricoles"),
( 242364, 0, 0, 242, 242,  "1CAP3", "2422522231.", "1CAP3 maintenance des matériels option B cons.manut."), -- fermé 2020-08-31
( 242365, 0, 0, 242, 242,  "2CAP3", "2422522232.", "2CAP3 maintenance des matériels option B cons.manut."),
( 242366, 0, 0, 242, 242,  "3CAP3", "2422522233.", "3CAP3 maintenance des matériels option B cons.manut."),
( 242367, 0, 0, 242, 242,  "1CAP3", "2422522331.", "1CAP3 maintenance des matériels option C espaces verts"), -- fermé 2020-08-31
( 242368, 0, 0, 242, 242,  "2CAP3", "2422522332.", "2CAP3 maintenance des matériels option C espaces verts"),
( 242369, 0, 0, 242, 242,  "3CAP3", "2422522333.", "3CAP3 maintenance des matériels option C espaces verts"),
( 242370, 0, 0, 242, 242,  "1CAP3", "2422530531.", "1CAP3 aéronautique option avionique"), -- fermé 2020-08-31
( 242371, 0, 0, 242, 242,  "2CAP3", "2422530532.", "2CAP3 aéronautique option avionique"),
( 242372, 0, 0, 242, 242,  "3CAP3", "2422530533.", "3CAP3 aéronautique option avionique"),
( 242373, 0, 0, 242, 242,  "1CAP3", "2422530631.", "1CAP3 aéronautique option systèmes"), -- fermé 2020-08-31
( 242374, 0, 0, 242, 242,  "2CAP3", "2422530632.", "2CAP3 aéronautique option systèmes"),
( 242375, 0, 0, 242, 242,  "3CAP3", "2422530633.", "3CAP3 aéronautique option systèmes"),
( 242376, 0, 0, 242, 242,  "1CAP3", "2422530731.", "1CAP3 aéronautique option structures"), -- fermé 2020-08-31
( 242377, 0, 0, 242, 242,  "2CAP3", "2422530732.", "2CAP3 aéronautique option structures"),
( 242378, 0, 0, 242, 242,  "3CAP3", "2422530733.", "3CAP3 aéronautique option structures"),
( 242379, 0, 0, 242, 242,  "1CAP3", "2422542131.", "1CAP3 mise en forme des matériaux"), -- fermé 2020-08-31
( 242380, 0, 0, 242, 242,  "2CAP3", "2422542132.", "2CAP3 mise en forme des matériaux"),
( 242381, 0, 0, 242, 242,  "3CAP3", "2422542133.", "3CAP3 mise en forme des matériaux"),
( 242382, 0, 0, 242, 242,  "1CAP3", "2422542531.", "1CAP3 outillage en moules métalliques"), -- fermé 2020-08-31
( 242383, 0, 0, 242, 242,  "2CAP3", "2422542532.", "2CAP3 outillage en moules métalliques"),
( 242384, 0, 0, 242, 242,  "3CAP3", "2422542533.", "3CAP3 outillage en moules métalliques"),
( 242385, 0, 0, 242, 242,  "1CAP3", "2422543131.", "1CAP3 serrurier métallier"), -- fermé 2020-08-31
( 242386, 0, 0, 242, 242,  "2CAP3", "2422543932.", "2CAP3 métallier"),-- MEF modifiée en 2022
( 242387, 0, 0, 242, 242,  "3CAP3", "2422543933.", "3CAP3 métallier"),-- MEF modifiée en 2022
( 242388, 0, 0, 242, 242,  "1CAP3", "2422543231.", "1CAP3 construction des carrosseries"), -- fermé 2020-08-31
( 242389, 0, 0, 242, 242,  "2CAP3", "2422543232.", "2CAP3 construction des carrosseries"),
( 242390, 0, 0, 242, 242,  "3CAP3", "2422543233.", "3CAP3 construction des carrosseries"),
( 242391, 0, 0, 242, 242,  "1CAP3", "2422543331.", "1CAP3 peinture en carrosserie"), -- fermé 2020-08-31
( 242392, 0, 0, 242, 242,  "2CAP3", "2422543332.", "2CAP3 peinture en carrosserie"),
( 242393, 0, 0, 242, 242,  "3CAP3", "2422543333.", "3CAP3 peinture en carrosserie"),
( 242394, 0, 0, 242, 242,  "1CAP3", "2422543431.", "1CAP3 réparation des carrosseries"), -- fermé 2020-08-31
( 242395, 0, 0, 242, 242,  "2CAP3", "2422543432.", "2CAP3 réparation des carrosseries"),
( 242396, 0, 0, 242, 242,  "3CAP3", "2422543433.", "3CAP3 réparation des carrosseries"),
( 242397, 0, 0, 242, 242,  "1CAP3", "2422543631.", "1CAP3 ferronnier d’art"), -- fermé 2020-08-31
( 242398, 0, 0, 242, 242,  "2CAP3", "2422543632.", "2CAP3 ferronnier d’art"),
( 242399, 0, 0, 242, 242,  "3CAP3", "2422543633.", "3CAP3 ferronnier d’art"),
( 242400, 0, 0, 242, 242,  "1CAP3", "2422543731.", "1CAP3 réalisations industrielles option A chaudronnerie"), -- fermé 2020-08-31
( 242401, 0, 0, 242, 242,  "2CAP3", "2422543732.", "2CAP3 réalisations industrielles option A chaudronnerie"),
( 242402, 0, 0, 242, 242,  "3CAP3", "2422543733.", "3CAP3 réalisations industrielles option A chaudronnerie"),
( 242403, 0, 0, 242, 242,  "1CAP3", "2422543831.", "1CAP3 réalisations industrielles option B soudage"), -- fermé 2020-08-31
( 242404, 0, 0, 242, 242,  "2CAP3", "2422543832.", "2CAP3 réalisations industrielles option B soudage"),
( 242405, 0, 0, 242, 242,  "3CAP3", "2422543833.", "3CAP3 réalisations industrielles option B soudage"),
( 242406, 0, 0, 242, 242,  "1CAP3", "2422552131.", "1CAP3 métiers enseigne signalétique"), -- fermé 2020-08-31
( 242407, 0, 0, 242, 242,  "2CAP3", "2422552132.", "2CAP3 métiers enseigne signalétique"),
( 242408, 0, 0, 242, 242,  "3CAP3", "2422552133.", "3CAP3 métiers enseigne signalétique"),
( 242409, 0, 0, 242, 242,  "1CAP3", "2422552431.", "1CAP3 électricien"), -- fermé 2020-08-31
( 242410, 0, 0, 242, 242,  "2CAP3", "2422552432.", "2CAP3 électricien"),
( 242411, 0, 0, 242, 242,  "3CAP3", "2422552433.", "3CAP3 électricien"),
( 242412, 0, 0, 242, 242,  "1CAP3", "2423110731.", "1CAP3 emballeur professionnel"), -- fermé 2020-08-31
( 242413, 0, 0, 242, 242,  "2CAP3", "2423110732.", "2CAP3 emballeur professionnel"),
( 242414, 0, 0, 242, 242,  "3CAP3", "2423110733.", "3CAP3 emballeur professionnel"),
( 242415, 0, 0, 242, 242,  "1CAP3", "2423111431.", "1CAP3 agent accueil condte rout.tr.voyag."), -- fermé 2020-08-31
( 242416, 0, 0, 242, 242,  "2CAP3", "2423111432.", "2CAP3 agent accueil condte rout.tr.voyag."),
( 242417, 0, 0, 242, 242,  "3CAP3", "2423111433.", "3CAP3 agent accueil condte rout.tr.voyag."),
( 242418, 0, 0, 242, 242,  "1CAP3", "2423111731.", "1CAP3 conducteur routier marchandises"), -- fermé 2020-08-31
( 242419, 0, 0, 242, 242,  "2CAP3", "2423111732.", "2CAP3 conducteur routier marchandises"),
( 242420, 0, 0, 242, 242,  "3CAP3", "2423111733.", "3CAP3 conducteur routier marchandises"),
( 242421, 0, 0, 242, 242,  "1CAP3", "2423111831.", "1CAP3 conducteur livreur marchandises"), -- fermé 2020-08-31
( 242422, 0, 0, 242, 242,  "2CAP3", "2423111832.", "2CAP3 conducteur livreur marchandises"),
( 242423, 0, 0, 242, 242,  "3CAP3", "2423111833.", "3CAP3 conducteur livreur marchandises"),
( 242424, 0, 0, 242, 242,  "1CAP3", "2423111931.", "1CAP3 déménageur véhicules utilitaire léger"), -- fermé 2020-08-31
( 242425, 0, 0, 242, 242,  "2CAP3", "2423111932.", "2CAP3 déménageur véhicules utilitaire léger"),
( 242426, 0, 0, 242, 242,  "3CAP3", "2423111933.", "3CAP3 déménageur véhicules utilitaire léger"),
( 242427, 0, 0, 242, 242,  "1CAP3", "2423112131.", "1CAP3 transport fluvial"), -- fermé 2020-08-31
( 242428, 0, 0, 242, 242,  "2CAP3", "2423112132.", "2CAP3 transport fluvial"),
( 242429, 0, 0, 242, 242,  "3CAP3", "2423112133.", "3CAP3 transport fluvial"),
( 242430, 0, 0, 242, 242,  "1CAP3", "2423112231.", "1CAP3 opérateur/opératrice logistique"), -- fermé 2020-08-31
( 242431, 0, 0, 242, 242,  "2CAP3", "2423112232.", "2CAP3 opérateur/opératrice logistique"),
( 242432, 0, 0, 242, 242,  "3CAP3", "2423112233.", "3CAP3 opérateur/opératrice logistique"),
( 242433, 0, 0, 242, 242,  "1CAP3", "2423112331.", "1CAP3 opérateur service relation client livraison"), -- fermé 2020-08-31
( 242434, 0, 0, 242, 242,  "2CAP3", "2423112332.", "2CAP3 opérateur service. relation client livraison"),
( 242435, 0, 0, 242, 242,  "3CAP3", "2423112333.", "3CAP3 opérateur service. relation client livraison"),
( 242436, 0, 0, 242, 242,  "1CAP3", "2423121431.", "1CAP3 employé commerce multispécialités"), -- fermé 2020-08-31
( 242437, 0, 0, 242, 242,  "2CAP3", "2423121432.", "2CAP3 employé commerce multispécialités"),
( 242438, 0, 0, 242, 242,  "3CAP3", "2423121433.", "3CAP3 employé commerce multispécialités"),
( 242439, 0, 0, 242, 242,  "1CAP3", "2423121531.", "1CAP3 employé vente : produits alimentaires"), -- fermé 2020-08-31
( 242440, 0, 0, 242, 242,  "2CAP3", "2423121532.", "2CAP3 employé vente : produits alimentaires"),
( 242441, 0, 0, 242, 242,  "3CAP3", "2423121533.", "3CAP3 employé vente : produits alimentaires"),
( 242442, 0, 0, 242, 242,  "1CAP3", "2423121631.", "1CAP3 employé vente : produits équipements courants"), -- fermé 2020-08-31
( 242443, 0, 0, 242, 242,  "2CAP3", "2423121632.", "2CAP3 employé vente : produits équipements courants"),
( 242444, 0, 0, 242, 242,  "3CAP3", "2423121633.", "3CAP3 employé vente : produits équipements courants"),
( 242445, 0, 0, 242, 242,  "1CAP3", "2423121731.", "1CAP3 vendeur-magasinier pièces auto"), -- fermé 2020-08-31
( 242446, 0, 0, 242, 242,  "2CAP3", "2423121732.", "2CAP3 vendeur-magasinier pièces auto"),
( 242447, 0, 0, 242, 242,  "3CAP3", "2423121733.", "3CAP3 vendeur-magasinier pièces auto"),
( 242448, 0, 0, 242, 242,  "1CAP3", "2423121931.", "1CAP3 employé vente spécialisé : service clientèle"), -- fermé 2020-08-31
( 242449, 0, 0, 242, 242,  "2CAP3", "2423121932.", "2CAP3 employé vente spécialisé : service clientèle"),
( 242450, 0, 0, 242, 242,  "3CAP3", "2423121933.", "3CAP3 employé vente spécialisé : service clientèle"),
( 242451, 0, 0, 242, 242,  "1CAP3", "2423122031.", "1CAP3 employé vente spécialisé : librairie papèterie presse"), -- fermé 2020-08-31
( 242452, 0, 0, 242, 242,  "2CAP3", "2423122032.", "2CAP3 employé vente spécialisé : librairie papèterie presse"),
( 242453, 0, 0, 242, 242,  "3CAP3", "2423122033.", "3CAP3 employé vente spécialisé : librairie papèterie presse"),
( 242454, 0, 0, 242, 242,  "1CAP3", "2423122131.", "1CAP3 poissonnier écailler"), -- fermé 2020-08-31
( 242455, 0, 0, 242, 242,  "2CAP3", "2423122132.", "2CAP3 poissonnier écailler"),
( 242456, 0, 0, 242, 242,  "3CAP3", "2423122133.", "3CAP3 poissonnier écailler"),
( 242457, 0, 0, 242, 242,  "1CAP3", "2423122231.", "1CAP3 primeur"), -- fermé 2020-08-31
( 242458, 0, 0, 242, 242,  "2CAP3", "2423122232.", "2CAP3 primeur"),
( 242459, 0, 0, 242, 242,  "3CAP3", "2423122233.", "3CAP3 primeur"),
( 242460, 0, 0, 242, 242,  "1CAP3", "2423122331.", "1CAP3 fleuriste"), -- fermé 2020-08-31
( 242461, 0, 0, 242, 242,  "2CAP3", "2423122332.", "2CAP3 fleuriste"),
( 242462, 0, 0, 242, 242,  "3CAP3", "2423122333.", "3CAP3 fleuriste"),
( 242463, 0, 0, 242, 242,  "1CAP3", "2423222031.", "1CAP3 métiers de la gravure : ornement"), -- fermé 2020-08-31
( 242464, 0, 0, 242, 242,  "2CAP3", "2423222032.", "2CAP3 métiers de la gravure : ornement"),
( 242465, 0, 0, 242, 242,  "3CAP3", "2423222033.", "3CAP3 métiers de la gravure : ornement"),
( 242466, 0, 0, 242, 242,  "1CAP3", "2423222131.", "1CAP3 métiers de la gravure : impression"), -- fermé 2020-08-31
( 242467, 0, 0, 242, 242,  "2CAP3", "2423222132.", "2CAP3 métiers de la gravure : impression"),
( 242468, 0, 0, 242, 242,  "3CAP3", "2423222133.", "3CAP3 métiers de la gravure : impression"),
( 242469, 0, 0, 242, 242,  "1CAP3", "2423222231.", "1CAP3 métiers de la gravure : modèle"), -- fermé 2020-08-31
( 242470, 0, 0, 242, 242,  "2CAP3", "2423222232.", "2CAP3 métiers de la gravure : modèle"),
( 242471, 0, 0, 242, 242,  "3CAP3", "2423222233.", "3CAP3 métiers de la gravure : modèle"),
( 242472, 0, 0, 242, 242,  "1CAP3", "2423222331.", "1CAP3 métiers de la gravure : marquage poinçonnage"), -- fermé 2020-08-31
( 242473, 0, 0, 242, 242,  "2CAP3", "2423222332.", "2CAP3 métiers de la gravure : marquage poinçonnage"),
( 242474, 0, 0, 242, 242,  "3CAP3", "2423222333.", "3CAP3 métiers de la gravure : marquage poinçonnage"),
( 242475, 0, 0, 242, 242,  "1CAP3", "2423222531.", "1CAP3 sérigraphie industrielle"), -- fermé 2020-08-31
( 242476, 0, 0, 242, 242,  "2CAP3", "2423222532.", "2CAP3 sérigraphie industrielle"),
( 242477, 0, 0, 242, 242,  "3CAP3", "2423222533.", "3CAP3 sérigraphie industrielle"),
( 242478, 0, 0, 242, 242,  "1CAP3", "2423222731.", "1CAP3 signalétique et décors graphiques"), -- fermé 2020-08-31
( 242479, 0, 0, 242, 242,  "2CAP3", "2423222732.", "2CAP3 signalétique et décors graphiques"),
( 242480, 0, 0, 242, 242,  "3CAP3", "2423222733.", "3CAP3 signalétique et décors graphiques"),
( 242481, 0, 0, 242, 242,  "1CAP3", "2423222831.", "1CAP3 arts de la reliure"), -- fermé 2020-08-31
( 242482, 0, 0, 242, 242,  "2CAP3", "2423222832.", "2CAP3 arts de la reliure"),
( 242483, 0, 0, 242, 242,  "3CAP3", "2423222833.", "3CAP3 arts de la reliure"),
( 242484, 0, 0, 242, 242,  "1CAP3", "2423230631.", "1CAP3 monteur en chapiteaux"), -- fermé 2020-08-31
( 242485, 0, 0, 242, 242,  "2CAP3", "2423230632.", "2CAP3 monteur en chapiteaux"),
( 242486, 0, 0, 242, 242,  "3CAP3", "2423230633.", "3CAP3 monteur en chapiteaux"),
( 242487, 0, 0, 242, 242,  "1CAP3", "2423230731.", "1CAP3 accessoiriste réalisateur"), -- fermé 2020-08-31
( 242488, 0, 0, 242, 242,  "2CAP3", "2423230732.", "2CAP3 accessoiriste réalisateur"),
( 242489, 0, 0, 242, 242,  "3CAP3", "2423230733.", "3CAP3 accessoiriste réalisateur"),
( 242490, 0, 0, 242, 242,  "1CAP3", "2423230831.", "1CAP3 accordeur de pianos"), -- fermé 2020-08-31
( 242491, 0, 0, 242, 242,  "2CAP3", "2423230832.", "2CAP3 accordeur de pianos"),
( 242492, 0, 0, 242, 242,  "3CAP3", "2423230833.", "3CAP3 accordeur de pianos"),
( 242493, 0, 0, 242, 242,  "1CAP3", "2423231031.", "1CAP3 assistant technique en instruments de musique option accordéon"), -- fermé 2020-08-31
( 242494, 0, 0, 242, 242,  "2CAP3", "2423231032.", "2CAP3 assistant technique en instruments de musique option accordéon"),
( 242495, 0, 0, 242, 242,  "3CAP3", "2423231033.", "3CAP3 assistant technique en instruments de musique option accordéon"),
( 242496, 0, 0, 242, 242,  "1CAP3", "2423231131.", "1CAP3 assistant technique instruments musique option instruments à vent"), -- fermé 2020-08-31
( 242497, 0, 0, 242, 242,  "2CAP3", "2423231132.", "2CAP3 assistant technique instruments musique option instruments à vent"),
( 242498, 0, 0, 242, 242,  "3CAP3", "2423231133.", "3CAP3 assistant technique instruments musique option instruments à vent"),
( 242499, 0, 0, 242, 242,  "1CAP3", "2423231231.", "1CAP3 assistant technique en instruments de musique option piano"), -- fermé 2020-08-31
( 242500, 0, 0, 242, 242,  "2CAP3", "2423231232.", "2CAP3 assistant technique en instruments de musique option piano"),
( 242501, 0, 0, 242, 242,  "3CAP3", "2423231233.", "3CAP3 assistant technique en instruments de musique option piano"),
( 242502, 0, 0, 242, 242,  "1CAP3", "2423231331.", "1CAP3 assistant technique en instruments de musique option guitare"), -- fermé 2020-08-31
( 242503, 0, 0, 242, 242,  "2CAP3", "2423231332.", "2CAP3 assistant technique en instruments de musique option guitare"),
( 242504, 0, 0, 242, 242,  "3CAP3", "2423231333.", "3CAP3 assistant technique en instruments de musique option guitare"),
( 242505, 0, 0, 242, 242,  "1CAP3", "2423310631.", "1CAP3 ortho-prothésiste"), -- fermé 2020-08-31
( 242506, 0, 0, 242, 242,  "2CAP3", "2423310632.", "2CAP3 ortho-prothésiste"),
( 242507, 0, 0, 242, 242,  "3CAP3", "2423310633.", "3CAP3 ortho-prothésiste"),
( 242508, 0, 0, 242, 242,  "1CAP3", "2423310731.", "1CAP3 podo-orthésiste"), -- fermé 2020-08-31
( 242509, 0, 0, 242, 242,  "2CAP3", "2423310732.", "2CAP3 podo-orthésiste"),
( 242510, 0, 0, 242, 242,  "3CAP3", "2423310733.", "3CAP3 podo-orthésiste"),
( 242511, 0, 0, 242, 242,  "1CAP3", "2423320431.", "1CAP3 accompagnant éducatif petite enfance"), -- fermé 2020-08-31
( 242512, 0, 0, 242, 242,  "2CAP3", "2423320532.", "2CAP3 accompagnant éducatif petite enfance"), -- MEF modifiée en 2022
( 242513, 0, 0, 242, 242,  "3CAP3", "2423320533.", "3CAP3 accompagnant éducatif petite enfance"), -- MEF modifiée en 2022
( 242514, 0, 0, 242, 242,  "1CAP3", "2423341131.", "1CAP3 assistant(e) technique en milieux familial et collectif"), -- fermé 2020-08-31
( 242515, 0, 0, 242, 242,  "2CAP3", "2423341132.", "2CAP3 assistant(e) technique en milieux familial et collectif"),
( 242516, 0, 0, 242, 242,  "3CAP3", "2423341133.", "3CAP3 assistant(e) technique en milieux familial et collectif"),
( 242517, 0, 0, 242, 242,  "1CAP3", "2423341231.", "1CAP3 commercialisation et services en hôtel-café-restaurant"), -- fermé 2020-08-31
( 242518, 0, 0, 242, 242,  "2CAP3", "2423341232.", "2CAP3 commercialisation et services en hôtel-café-restaurant"),
( 242519, 0, 0, 242, 242,  "3CAP3", "2423341233.", "3CAP3 commercialisation et services en hôtel-café-restaurant"),
( 242520, 0, 0, 242, 242,  "1CAP3", "2423350131.", "1CAP3 métiers du football"), -- fermé 2020-08-31
( 242521, 0, 0, 242, 242,  "2CAP3", "2423350132.", "2CAP3 métiers du football"),
( 242522, 0, 0, 242, 242,  "3CAP3", "2423350133.", "3CAP3 métiers du football"),
( 242523, 0, 0, 242, 242,  "1CAP3", "2423361531.", "1CAP3 esthétique cosmétique parfumerie"), -- fermé 2020-08-31
( 242524, 0, 0, 242, 242,  "2CAP3", "2423361532.", "2CAP3 esthétique cosmétique parfumerie"),
( 242525, 0, 0, 242, 242,  "3CAP3", "2423361533.", "3CAP3 esthétique cosmétique parfumerie"),
( 242526, 0, 0, 242, 242,  "1CAP3", "2423361631.", "1CAP3 métiers de la coiffure"), -- fermé 2020-08-31
( 242527, 0, 0, 242, 242,  "2CAP3", "2423361632.", "2CAP3 métiers de la coiffure"),
( 242528, 0, 0, 242, 242,  "3CAP3", "2423361633.", "3CAP3 métiers de la coiffure"),
( 242529, 0, 0, 242, 242,  "1CAP3", "2423361731.", "1CAP3 taxidermiste"), -- fermé 2020-08-31
( 242530, 0, 0, 242, 242,  "2CAP3", "2423361732.", "2CAP3 taxidermiste"),
( 242531, 0, 0, 242, 242,  "3CAP3", "2423361733.", "3CAP3 taxidermiste"),
( 242532, 0, 0, 242, 242,  "1CAP3", "2423430131.", "1CAP3 agent de la qualité de l’eau"), -- fermé 2020-08-31
( 242533, 0, 0, 242, 242,  "2CAP3", "2423430132.", "2CAP3 agent de la qualité de l’eau"),
( 242534, 0, 0, 242, 242,  "3CAP3", "2423430133.", "3CAP3 agent de la qualité de l’eau"),
( 242535, 0, 0, 242, 242,  "1CAP3", "2423430331.", "1CAP3 assainissement collectif déchets liquides"), -- fermé 2020-08-31
( 242536, 0, 0, 242, 242,  "2CAP3", "2423430332.", "2CAP3 assainissement collectif déchets liquides"),
( 242537, 0, 0, 242, 242,  "3CAP3", "2423430333.", "3CAP3 assainissement collectif déchets liquides"),
( 242538, 0, 0, 242, 242,  "1CAP3", "2423430731.", "1CAP3 agent de propreté et d’hygiène"), -- fermé 2020-08-31
( 242539, 0, 0, 242, 242,  "2CAP3", "2423430732.", "2CAP3 agent de propreté et d’hygiène"),
( 242540, 0, 0, 242, 242,  "3CAP3", "2423430733.", "3CAP3 agent de propreté et d’hygiène"),
( 242541, 0, 0, 242, 242,  "1CAP3", "2423440431.", "1CAP3 gardien d’immeuble"), -- fermé 2020-08-31
( 242542, 0, 0, 242, 242,  "2CAP3", "2423440432.", "2CAP3 gardien d’immeuble"),
( 242543, 0, 0, 242, 242,  "3CAP3", "2423440433.", "3CAP3 gardien d’immeuble"),
( 242544, 0, 0, 242, 242,  "1CAP3", "2423440531.", "1CAP3 agent de sécurité"), -- fermé 2020-08-31
( 242545, 0, 0, 242, 242,  "2CAP3", "2423440532.", "2CAP3 agent de sécurité"),
( 242546, 0, 0, 242, 242,  "3CAP3", "2423440533.", "3CAP3 agent de sécurité"),
( 242547, 0, 0, 242, 242,  "1CAP3", "2422214231.", "1CAP3 production/service en restauration rapide, collective, cafétéria"), -- fermé 2020-08-31
( 242548, 0, 0, 242, 242,  "2CAP3", "2422214232.", "2CAP3 production/service en restauration rapide, collective, cafétéria"),
( 242549, 0, 0, 242, 242,  "3CAP3", "2422214233.", "3CAP3 production/service en restauration rapide, collective, cafétéria"),
( 242550, 0, 0, 242, 242,  "1CAP3", "2423440531.", "1CAP3 équipier polyvalent du commerce"), -- fermé 2020-08-31
( 242551, 0, 0, 242, 242,  "2CAP3", "2423122432.", "2CAP3 équipier polyvalent du commerce"),
( 242552, 0, 0, 242, 242,  "3CAP3", "2423122433.", "3CAP3 équipier polyvalent du commerce"),
( 242553, 0, 0, 242, 242,  "1CAP3", "2423400231.", "1CAP3 agent de prévention et de médiation"), -- fermé 2020-08-31
( 242554, 0, 0, 242, 242,  "2CAP3", "2423400232.", "2CAP3 agent de prévention et de médiation"),
( 242555, 0, 0, 242, 242,  "3CAP3", "2423400233.", "3CAP3 agent de prévention et de médiation"),
( 242556, 0, 0, 242, 242,  "2CAP3", "2422345032.", "2CAP3 assistant luthier du quatuor"),
( 242557, 0, 0, 242, 242,  "3CAP3", "2422345033.", "3CAP3 assistant luthier du quatuor"),

-- 243 BEP (Brevet d’Etudes Professionnel) -- dispositif de formation 243~244

( 243000, 0, 0, 243, 243,   "BEP1", "243.....11.", "BEP 1 an"), -- fermé 31/08/2011

( 244001, 0, 0, 243, 244,   "2BEP", "244.....21.", "BEP 2 ans, 1e année (seconde)"), -- fermé 31/08/2012
( 244002, 0, 0, 243, 244,   "TBEP", "244.....22.", "BEP 2 ans, 2e année (terminale)"); -- fermé 31/08/2013

INSERT INTO sacoche_niveau VALUES 

-- 247 Bac Pro (Baccalauréat Professionnel) -- dispositif de formation 245~247

( 245000, 0, 0, 247, 245,  "1PRO1", "245.....11.", "Bac Pro 1 an"), -- fermé 31/08/2011
( 246001, 0, 0, 247, 246,  "1PRO2", "246.....21.", "Bac Pro 2 ans, 1e année"), -- fermé 31/08/2009
( 246002, 0, 0, 247, 246,  "2PRO2", "246.....22.", "Bac Pro 2 ans, 2e année (terminale)"), -- fermé 31/08/2011

( 247001, 0, 1, 247, 247,  "1PRO3", "247.....31.", "Bac Pro 3 ans, 1e année (seconde pro)"),
( 247002, 0, 1, 247, 247,  "2PRO3", "247.....32.", "Bac Pro 3 ans, 2e année (première pro)"),
( 247003, 0, 1, 247, 247,  "3PRO3", "247.....33.", "Bac Pro 3 ans, 3e année (terminale pro)"),
( 247004, 0, 0, 247, 247, "2NDPRO", "2472000231.", "2ndPro etud.definition prdts industriels"),
( 247005, 0, 0, 247, 247, "1ERPRO", "2472000232.", "1erPro etud.definition prdts industriels"),
( 247006, 0, 0, 247, 247, "TLEPRO", "2472000233.", "TlePro etud.definition prdts industriels"),
( 247007, 0, 0, 247, 247, "2NDPRO", "2472010231.", "2ndPro pilote de ligne de production"),
( 247008, 0, 0, 247, 247, "1ERPRO", "2472010232.", "1erPro pilote de ligne de production"),
( 247009, 0, 0, 247, 247, "TLEPRO", "2472010233.", "TlePro pilote de ligne de production"),
( 247010, 0, 0, 247, 247, "TLEPRO", "2472120133.", "TlePro cultures marines"),
( 247011, 0, 0, 247, 247, "2NDPRO", "2472120431.", "2ndPro cultures marines"),
( 247012, 0, 0, 247, 247, "1ERPRO", "2472120432.", "1erPro cultures marines"),
( 247013, 0, 0, 247, 247, "TLEPRO", "2472120433.", "TlePro cultures marines"),
( 247014, 0, 0, 247, 247, "2NDPRO", "2472130431.", "2ndPro conduite et gestion des entreprises maritimes-pêche"), -- fermé 31/08/2019
( 247015, 0, 0, 247, 247, "1ERPRO", "2472130432.", "1erPro conduite et gestion des entreprises maritimes-pêche"),
( 247016, 0, 0, 247, 247, "TLEPRO", "2472130433.", "TlePro conduite et gestion des entreprises maritimes-pêche"),
( 247017, 0, 0, 247, 247, "2NDPRO", "2472200331.", "2ndPro bio-industries de transformation"),
( 247018, 0, 0, 247, 247, "1ERPRO", "2472200332.", "1erPro bio-industries de transformation"),
( 247019, 0, 0, 247, 247, "TLEPRO", "2472200333.", "TlePro bio-industries de transformation"),
( 247020, 0, 0, 247, 247, "2NDPRO", "2472200431.", "2ndPro proc. chimie eau papiers-cartons"),
( 247021, 0, 0, 247, 247, "1ERPRO", "2472200432.", "1erPro proc. chimie eau papiers-cartons"),
( 247022, 0, 0, 247, 247, "TLEPRO", "2472200433.", "TlePro proc. chimie eau papiers-cartons"),
( 247023, 0, 0, 247, 247, "2NDPRO", "2472210431.", "2ndPro boucher-charcutier-traiteur"),
( 247024, 0, 0, 247, 247, "1ERPRO", "2472210432.", "1erPro boucher-charcutier-traiteur"),
( 247025, 0, 0, 247, 247, "TLEPRO", "2472210433.", "TlePro boucher-charcutier-traiteur"),
( 247026, 0, 0, 247, 247, "2NDPRO", "2472210531.", "2ndPro boulanger-pâtissier"),
( 247027, 0, 0, 247, 247, "1ERPRO", "2472210532.", "1erPro boulanger-pâtissier"),
( 247028, 0, 0, 247, 247, "TLEPRO", "2472210533.", "TlePro boulanger-pâtissier"),
( 247029, 0, 0, 247, 247, "2NDPRO", "2472210631.", "2ndPro cuisine"),
( 247030, 0, 0, 247, 247, "1ERPRO", "2472210632.", "1erPro cuisine"),
( 247031, 0, 0, 247, 247, "TLEPRO", "2472210633.", "TlePro cuisine"),
( 247032, 0, 0, 247, 247, "2NDPRO", "2472230231.", "2ndPro traitements des matériaux"),
( 247033, 0, 0, 247, 247, "1ERPRO", "2472230232.", "1erPro traitements des matériaux"),
( 247034, 0, 0, 247, 247, "TLEPRO", "2472230233.", "TlePro traitements des matériaux"),
( 247035, 0, 0, 247, 247, "2NDPRO", "2472230331.", "2ndPro fonderie"),
( 247036, 0, 0, 247, 247, "1ERPRO", "2472230332.", "1erPro fonderie"),
( 247037, 0, 0, 247, 247, "TLEPRO", "2472230333.", "TlePro fonderie"),
( 247038, 0, 0, 247, 247, "2NDPRO", "2472230831.", "2ndPro artisanat et métiers d’art : facteur d’orgues option organier"), -- MEF modifiée en 2021
( 247039, 0, 0, 247, 247, "1ERPRO", "2472230832.", "1erPro artisanat et métiers d’art : facteur d’orgues option organier"), -- MEF modifiée en 2021
( 247040, 0, 0, 247, 247, "TLEPRO", "2472230833.", "TlePro artisanat et métiers d’art : facteur d’orgues option organier"), -- MEF modifiée en 2021
( 247041, 0, 0, 247, 247, "2NDPRO", "2472230931.", "2ndPro artisanat et métiers d’art : facteur d’orgues option tuyautier"), -- MEF modifiée en 2021
( 247042, 0, 0, 247, 247, "1ERPRO", "2472230932.", "1erPro artisanat et métiers d’art : facteur d’orgues option tuyautier"), -- MEF modifiée en 2021
( 247043, 0, 0, 247, 247, "TLEPRO", "2472230933.", "TlePro artisanat et métiers d’art : facteur d’orgues option tuyautier"), -- MEF modifiée en 2021
( 247044, 0, 0, 247, 247, "2NDPRO", "2472240231.", "2ndPro artisanat et métiers d’art : option verrerie scientifique technique"),
( 247045, 0, 0, 247, 247, "1ERPRO", "2472240232.", "1erPro artisanat et métiers d’art : option verrerie scientifique technique"),
( 247046, 0, 0, 247, 247, "TLEPRO", "2472240233.", "TlePro artisanat et métiers d’art : option verrerie scientifique technique"),
( 247047, 0, 0, 247, 247, "2NDPRO", "2472240331.", "2ndPro artisanat et métiers d’art : option métiers enseigne et signalétique"),
( 247048, 0, 0, 247, 247, "1ERPRO", "2472240332.", "1erPro artisanat et métiers d’art : option métiers enseigne et signalétique"),
( 247049, 0, 0, 247, 247, "TLEPRO", "2472240333.", "TlePro artisanat et métiers d’art : option métiers enseigne et signalétique"),
( 247050, 0, 0, 247, 247, "2NDPRO", "2472250331.", "2ndPro plastiques et composites"),
( 247051, 0, 0, 247, 247, "1ERPRO", "2472250332.", "1erPro plastiques et composites"),
( 247052, 0, 0, 247, 247, "TLEPRO", "2472250333.", "TlePro plastiques et composites"),
( 247053, 0, 0, 247, 247, "2NDPRO", "2472270331.", "2ndPro techn.install.syst.energ.climatiq"),
( 247054, 0, 0, 247, 247, "1ERPRO", "2472270332.", "1erPro techn.install.syst.energ.climatiq"),
( 247055, 0, 0, 247, 247, "TLEPRO", "2472270333.", "TlePro techn.install.syst.energ.climatiq"),
( 247056, 0, 0, 247, 247, "2NDPRO", "2472270431.", "2ndPro techn. maint. syst.energ.climatiq"),
( 247057, 0, 0, 247, 247, "1ERPRO", "2472270432.", "1erPro techn. maint. syst.energ.climatiq"),
( 247058, 0, 0, 247, 247, "TLEPRO", "2472270433.", "TlePro techn. maint. syst.energ.climatiq"),
( 247059, 0, 0, 247, 247, "2NDPRO", "2472300331.", "2ndPro artisanat et métiers d’art : marchandisage vis."),
( 247060, 0, 0, 247, 247, "1ERPRO", "2472300332.", "1erPro artisanat et métiers d’art : marchandisage vis."),
( 247061, 0, 0, 247, 247, "TLEPRO", "2472300333.", "TlePro artisanat et métiers d’art : marchandisage vis."),
( 247062, 0, 0, 247, 247, "2NDPRO", "2472300431.", "2ndPro technicen d’études du bâtiment option A études et économie"),
( 247063, 0, 0, 247, 247, "1ERPRO", "2472300432.", "1erPro technicen d’études du bâtiment option A études et économie"),
( 247064, 0, 0, 247, 247, "TLEPRO", "2472300433.", "TlePro technicen d’études du bâtiment option A études et économie"),
( 247065, 0, 0, 247, 247, "2NDPRO", "2472300531.", "2ndPro technicien d’études du bâtiment opt. B assistant en architecture"),
( 247066, 0, 0, 247, 247, "1ERPRO", "2472300532.", "1erPro technicien d’études du bâtiment opt. B assistant en architecture"),
( 247067, 0, 0, 247, 247, "TLEPRO", "2472300533.", "TlePro technicien d’études du bâtiment opt. B assistant en architecture"),
( 247068, 0, 0, 247, 247, "2NDPRO", "2472310231.", "2ndPro travaux publics"),
( 247069, 0, 0, 247, 247, "1ERPRO", "2472310232.", "1erPro travaux publics"),
( 247070, 0, 0, 247, 247, "TLEPRO", "2472310233.", "TlePro travaux publics"),
( 247071, 0, 0, 247, 247, "2NDPRO", "2472310331.", "2ndPro technicien géomètre-topographe"),
( 247072, 0, 0, 247, 247, "1ERPRO", "2472310332.", "1erPro technicien géomètre-topographe"),
( 247073, 0, 0, 247, 247, "TLEPRO", "2472310333.", "TlePro technicien géomètre-topographe"),
( 247074, 0, 0, 247, 247, "1ERPRO", "2472320232.", "1erPro artisanat et métiers d’art : arts pierre"),
( 247075, 0, 0, 247, 247, "TLEPRO", "2472320233.", "TlePro artisanat et métiers d’art : arts pierre"),
( 247076, 0, 0, 247, 247, "2NDPRO", "2472320331.", "2ndPro technicien du bâtiment : organisation & réalisation gros oeuvre"),
( 247077, 0, 0, 247, 247, "1ERPRO", "2472320332.", "1erPro technicien du bâtiment : organisation & réalisation gros oeuvre"),
( 247078, 0, 0, 247, 247, "TLEPRO", "2472320333.", "TlePro technicien du bâtiment : organisation & réalisation gros oeuvre"),
( 247079, 0, 0, 247, 247, "2NDPRO", "2472320531.", "2ndPro interventions sur le patrimoine bâti, option A maçonnerie"),
( 247080, 0, 0, 247, 247, "1ERPRO", "2472320532.", "1erPro interventions sur le patrimoine bâti, option A maçonnerie"),
( 247081, 0, 0, 247, 247, "TLEPRO", "2472320533.", "TlePro interventions sur le patrimoine bâti, option A maçonnerie"),
( 247082, 0, 0, 247, 247, "2NDPRO", "2472320631.", "2ndPro interventions sur le patrimoine bâti, option B charpente"),
( 247083, 0, 0, 247, 247, "1ERPRO", "2472320632.", "1erPro interventions sur le patrimoine bâti, option B charpente"),
( 247084, 0, 0, 247, 247, "TLEPRO", "2472320633.", "TlePro interventions sur le patrimoine bâti, option B charpente"),
( 247085, 0, 0, 247, 247, "2NDPRO", "2472320731.", "2ndPro interventions sur le patrimoine bâti, option C couverture"),
( 247086, 0, 0, 247, 247, "1ERPRO", "2472320732.", "1erPro interventions sur le patrimoine bâti, option C couverture"),
( 247087, 0, 0, 247, 247, "TLEPRO", "2472320733.", "TlePro interventions sur le patrimoine bâti, option C couverture"),
( 247088, 0, 0, 247, 247, "2NDPRO", "2472320831.", "2ndPro métiers et arts de la pierre"),
( 247089, 0, 0, 247, 247, "1ERPRO", "2472320832.", "1erPro métiers et arts de la pierre"),
( 247090, 0, 0, 247, 247, "TLEPRO", "2472320833.", "TlePro métiers et arts de la pierre"),
( 247091, 0, 0, 247, 247, "2NDPRO", "2472330331.", "2ndPro menuiserie aluminium-verre"),
( 247092, 0, 0, 247, 247, "1ERPRO", "2472330332.", "1erPro menuiserie aluminium-verre"),
( 247093, 0, 0, 247, 247, "TLEPRO", "2472330333.", "TlePro menuiserie aluminium-verre"),
( 247094, 0, 0, 247, 247, "2NDPRO", "2472330431.", "2ndPro aménagement et finitions du bâtiment"),
( 247095, 0, 0, 247, 247, "1ERPRO", "2472330432.", "1erPro aménagement et finitions du bâtiment"),
( 247096, 0, 0, 247, 247, "TLEPRO", "2472330433.", "TlePro aménagement et finitions du bâtiment"),
( 247097, 0, 0, 247, 247, "TLEPRO", "2472340333.", "TlePro artisanat et métiers d’art : : ébéniste"),
( 247098, 0, 0, 247, 247, "2NDPRO", "2472340431.", "2ndPro technicien constructeur bois"),
( 247099, 0, 0, 247, 247, "1ERPRO", "2472340432.", "1erPro technicien constructeur bois"),
( 247100, 0, 0, 247, 247, "TLEPRO", "2472340433.", "TlePro technicien constructeur bois"),
( 247101, 0, 0, 247, 247, "2NDPRO", "2472340531.", "2ndPro technicien menuisier agenceur"),
( 247102, 0, 0, 247, 247, "1ERPRO", "2472340532.", "1erPro technicien menuisier agenceur"),
( 247103, 0, 0, 247, 247, "TLEPRO", "2472340533.", "TlePro technicien menuisier agenceur"),
( 247104, 0, 0, 247, 247, "2NDPRO", "2472340631.", "2ndPro technicien de scierie"),
( 247105, 0, 0, 247, 247, "1ERPRO", "2472340632.", "1erPro technicien de scierie"),
( 247106, 0, 0, 247, 247, "TLEPRO", "2472340633.", "TlePro technicien de scierie"),
( 247107, 0, 0, 247, 247, "2NDPRO", "2472340731.", "2ndPro technic.fab.bois et materx assoc."),
( 247108, 0, 0, 247, 247, "1ERPRO", "2472340732.", "1erPro technic.fab.bois et materx assoc."),
( 247109, 0, 0, 247, 247, "TLEPRO", "2472340733.", "TlePro technic.fab.bois et materx assoc."),
( 247110, 0, 0, 247, 247, "2NDPRO", "2472340831.", "2ndPro étude et réalisation d’agencement"),
( 247111, 0, 0, 247, 247, "1ERPRO", "2472340832.", "1erPro étude et réalisation d’agencement"),
( 247112, 0, 0, 247, 247, "TLEPRO", "2472340833.", "TlePro étude et réalisation d’agencement"),
( 247113, 0, 0, 247, 247, "2NDPRO", "2472400531.", "2ndPro métiers de l’entretien des textiles option A : blanchisserie"), -- MEF modifiée en 2021
( 247114, 0, 0, 247, 247, "1ERPRO", "2472400532.", "1erPro métiers de l’entretien des textiles option A : blanchisserie"), -- MEF modifiée en 2021
( 247115, 0, 0, 247, 247, "TLEPRO", "2472400533.", "TlePro métiers de l’entretien des textiles option A : blanchisserie"), -- MEF modifiée en 2021
( 247116, 0, 0, 247, 247, "2NDPRO", "2472420231.", "2ndPro artisanat et métiers d’art : tapis d’ameublement"),
( 247117, 0, 0, 247, 247, "1ERPRO", "2472420232.", "1erPro artisanat et métiers d’art : tapis d’ameublement"),
( 247118, 0, 0, 247, 247, "TLEPRO", "2472420233.", "TlePro artisanat et métiers d’art : tapis d’ameublement"),
( 247119, 0, 0, 247, 247, "2NDPRO", "2472420331.", "2ndPro métiers de la mode - vêtement"),
( 247120, 0, 0, 247, 247, "1ERPRO", "2472420332.", "1erPro métiers de la mode - vêtement"),
( 247121, 0, 0, 247, 247, "TLEPRO", "2472420333.", "TlePro métiers de la mode - vêtement"),
( 247122, 0, 0, 247, 247, "2NDPRO", "2472430131.", "2ndPro métiers du cuir : option chaussures"),
( 247123, 0, 0, 247, 247, "1ERPRO", "2472430132.", "1erPro métiers du cuir : option chaussures"),
( 247124, 0, 0, 247, 247, "TLEPRO", "2472430133.", "TlePro métiers du cuir : option chaussures"),
( 247125, 0, 0, 247, 247, "2NDPRO", "2472430231.", "2ndPro métiers du cuir : option maroquinerie"),
( 247126, 0, 0, 247, 247, "1ERPRO", "2472430232.", "1erPro métiers du cuir : option maroquinerie"),
( 247127, 0, 0, 247, 247, "TLEPRO", "2472430233.", "TlePro métiers du cuir : option maroquinerie"),
( 247128, 0, 0, 247, 247, "2NDPRO", "2472500531.", "2ndPro productique meca. option décolletage"),
( 247129, 0, 0, 247, 247, "1ERPRO", "2472500532.", "1erPro productique meca. option décolletage"),
( 247130, 0, 0, 247, 247, "TLEPRO", "2472500533.", "TlePro productiq.meca. option décolletage"),
( 247131, 0, 0, 247, 247, "2NDPRO", "2472500631.", "2ndPro microtechniques"),
( 247132, 0, 0, 247, 247, "1ERPRO", "2472500632.", "1erPro microtechniques"),
( 247133, 0, 0, 247, 247, "TLEPRO", "2472500633.", "TlePro microtechniques"),
( 247134, 0, 0, 247, 247, "2NDPRO", "2472500731.", "2ndPro maintenance des équipements industriels"), -- fermé 2021-08-31
( 247135, 0, 0, 247, 247, "1ERPRO", "2472500732.", "1erPro maintenance des équipements industriels"), -- fermé 2022-08-31
( 247136, 0, 0, 247, 247, "TLEPRO", "2472500733.", "TlePro maintenance des équipements industriels"), -- fermé 2023-08-31
( 247137, 0, 0, 247, 247, "2NDPRO", "2472500831.", "2ndPro électromecanicien marine"), -- fermé 31/08/2019
( 247138, 0, 0, 247, 247, "1ERPRO", "2472500832.", "1erPro électromecanicien marine"), -- fermé 31/08/2020
( 247139, 0, 0, 247, 247, "TLEPRO", "2472500833.", "TlePro électromecanicien marine"), -- fermé 31/08/2021
( 247140, 0, 0, 247, 247, "2NDPRO", "2472500931.", "2ndPro maintenance nautique"),
( 247141, 0, 0, 247, 247, "1ERPRO", "2472500932.", "1erPro maintenance nautique"),
( 247142, 0, 0, 247, 247, "TLEPRO", "2472500933.", "TlePro maintenance nautique"),
( 247143, 0, 0, 247, 247, "2NDPRO", "2472510631.", "2ndPro technicien d’usinage"),
( 247144, 0, 0, 247, 247, "1ERPRO", "2472510632.", "1erPro technicien d’usinage"),
( 247145, 0, 0, 247, 247, "TLEPRO", "2472510633.", "TlePro technicien d’usinage"),
( 247146, 0, 0, 247, 247, "2NDPRO", "2472510731.", "2ndPro technicien outilleur"),
( 247147, 0, 0, 247, 247, "1ERPRO", "2472510732.", "1erPro technicien outilleur"),
( 247148, 0, 0, 247, 247, "TLEPRO", "2472510733.", "TlePro technicien outilleur"),
( 247149, 0, 0, 247, 247, "2NDPRO", "2472510831.", "2ndPro technicien modeleur"),
( 247150, 0, 0, 247, 247, "1ERPRO", "2472510832.", "1erPro technicien modeleur"),
( 247151, 0, 0, 247, 247, "TLEPRO", "2472510833.", "TlePro technicien modeleur"),
( 247152, 0, 0, 247, 247, "TLEPRO", "2472520733.", "TlePro maint.véhic.auto : voit.particul."),
( 247153, 0, 0, 247, 247, "TLEPRO", "2472520833.", "TlePro maint.véhic.auto. : véhic.indust."),
( 247154, 0, 0, 247, 247, "TLEPRO", "2472521033.", "TlePro maint.véhic.auto. : motocycles"),
( 247155, 0, 0, 247, 247, "2NDPRO", "2472521931.", "2ndPro maintenance des matériels option A matériels agricoles"), -- MEF modifiée en 2019
( 247156, 0, 0, 247, 247, "1ERPRO", "2472521932.", "1erPro maintenance des matériels option A matériels agricoles"),
( 247157, 0, 0, 247, 247, "TLEPRO", "2472521933.", "TlePro maintenance des matériels option A matériels agricoles"),
( 247158, 0, 0, 247, 247, "2NDPRO", "2472522031.", "2ndPro maintenance des matériels option B mat. construction et manutention"), -- MEF modifiée en 2019
( 247159, 0, 0, 247, 247, "1ERPRO", "2472522032.", "1erPro maintenance des matériels option B mat. construction et manutention"),
( 247160, 0, 0, 247, 247, "TLEPRO", "2472522033.", "TlePro maintenance des matériels option B mat. construction et manutention"),
( 247161, 0, 0, 247, 247, "2NDPRO", "2472522131.", "2ndPro maintenance des matériels option C matériels d’espaces verts"), -- MEF modifiée en 2019
( 247162, 0, 0, 247, 247, "1ERPRO", "2472522132.", "1erPro maintenance des matériels option C matériels d’espaces verts"),
( 247163, 0, 0, 247, 247, "TLEPRO", "2472522133.", "TlePro maintenance des matériels option C matériels d’espaces verts"),
( 247164, 0, 0, 247, 247, "2NDPRO", "2472521431.", "2ndPro maintenance des véhicules option A voitures particulières"),
( 247165, 0, 0, 247, 247, "1ERPRO", "2472521432.", "1erPro maintenance des véhicules option A voitures particulières"),
( 247166, 0, 0, 247, 247, "TLEPRO", "2472521433.", "TlePro maintenance des véhicules option A voitures particulières"),
( 247167, 0, 0, 247, 247, "2NDPRO", "2472521531.", "2ndPro maintenance des véhicules option B véhicules de transport routier"),
( 247168, 0, 0, 247, 247, "1ERPRO", "2472521532.", "1erPro maintenance des véhicules option B véhicules de transport routier"),
( 247169, 0, 0, 247, 247, "TLEPRO", "2472521533.", "TlePro maintenance des véhicules option B véhicules de transport routier"),
( 247170, 0, 0, 247, 247, "2NDPRO", "2472521631.", "2ndPro maintenance des véhicules option C motocycles"),
( 247171, 0, 0, 247, 247, "1ERPRO", "2472521632.", "1erPro maintenance des véhicules option C motocycles"),
( 247172, 0, 0, 247, 247, "TLEPRO", "2472521633.", "TlePro maintenance des véhicules option C motocycles"),
( 247173, 0, 0, 247, 247, "2NDPRO", "2472530231.", "2ndPro aéronautique option avionique"),
( 247174, 0, 0, 247, 247, "1ERPRO", "2472530232.", "1erPro aéronautique option avionique"),
( 247175, 0, 0, 247, 247, "TLEPRO", "2472530233.", "TlePro aéronautique option avionique"),
( 247176, 0, 0, 247, 247, "2NDPRO", "2472530331.", "2ndPro aéronautique option systèmes"),
( 247177, 0, 0, 247, 247, "1ERPRO", "2472530332.", "1erPro aéronautique option systèmes"),
( 247178, 0, 0, 247, 247, "TLEPRO", "2472530333.", "TlePro aéronautique option systèmes"),
( 247179, 0, 0, 247, 247, "2NDPRO", "2472530431.", "2ndPro aéronautique option structure"),
( 247180, 0, 0, 247, 247, "1ERPRO", "2472530432.", "1erPro aéronautique option structure"),
( 247181, 0, 0, 247, 247, "TLEPRO", "2472530433.", "TlePro aéronautique option structure"),
( 247182, 0, 0, 247, 247, "2NDPRO", "2472530531.", "2ndPro aviation générale"),
( 247183, 0, 0, 247, 247, "1ERPRO", "2472530532.", "1erPro aviation générale"),
( 247184, 0, 0, 247, 247, "TLEPRO", "2472530533.", "TlePro aviation générale"),
( 247185, 0, 0, 247, 247, "2NDPRO", "2472540631.", "2ndPro ouvrages du bâtiment métallerie"),
( 247186, 0, 0, 247, 247, "1ERPRO", "2472540632.", "1erPro ouvrages du bâtiment métallerie"),
( 247187, 0, 0, 247, 247, "TLEPRO", "2472540633.", "TlePro ouvrages du bâtiment métallerie"),
( 247188, 0, 0, 247, 247, "2NDPRO", "2472540831.", "2ndPro réparation des carrosseries"),
( 247189, 0, 0, 247, 247, "1ERPRO", "2472540832.", "1erPro réparation des carrosseries"),
( 247190, 0, 0, 247, 247, "TLEPRO", "2472540833.", "TlePro réparation des carrosseries"),
( 247191, 0, 0, 247, 247, "2NDPRO", "2472541131.", "2ndPro tech.chaudronnerie industrielle"),
( 247192, 0, 0, 247, 247, "1ERPRO", "2472541132.", "1erPro tech.chaudronnerie industrielle"),
( 247193, 0, 0, 247, 247, "TLEPRO", "2472541133.", "TlePro tech.chaudronnerie industrielle"),
( 247194, 0, 0, 247, 247, "2NDPRO", "2472541031.", "2ndPro construction des carrosseries"),
( 247195, 0, 0, 247, 247, "1ERPRO", "2472541032.", "1erPro construction des carrosseries"),
( 247196, 0, 0, 247, 247, "TLEPRO", "2472541033.", "TlePro construction des carrosseries"),
( 247197, 0, 0, 247, 247, "2NDPRO", "2472550631.", "2ndPro électrotec. energ. équip.communic"),
( 247198, 0, 0, 247, 247, "1ERPRO", "2472550632.", "1erPro électrotec. energ. équip.communic"),
( 247199, 0, 0, 247, 247, "TLEPRO", "2472550633.", "TlePro électrotec. energ. équip.communic"),
( 247200, 0, 0, 247, 247, "2NDPRO", "2472550731.", "2ndPro systèmes électroniques numériques"),
( 247201, 0, 0, 247, 247, "1ERPRO", "2472550732.", "1erPro systèmes électroniques numériques"),
( 247202, 0, 0, 247, 247, "TLEPRO", "2472550733.", "TlePro systèmes électroniques numériques"),
( 247203, 0, 0, 247, 247, "2NDPRO", "2472550931.", "2ndPro technic. froid conditionnement air"),
( 247204, 0, 0, 247, 247, "1ERPRO", "2472550932.", "1erPro technic. froid conditionnement air"),
( 247205, 0, 0, 247, 247, "TLEPRO", "2472550933.", "TlePro technic. froid conditionnement air"),
( 247206, 0, 0, 247, 247, "2NDPRO", "2473000131.", "2ndPro gestion-administration"),
( 247207, 0, 0, 247, 247, "1ERPRO", "2473000132.", "1erPro gestion-administration"),
( 247208, 0, 0, 247, 247, "TLEPRO", "2473000133.", "TlePro gestion-administration"),
( 247209, 0, 0, 247, 247, "2NDPRO", "2473111031.", "2ndPro CGEM conduite et gestion des entreprises maritimes option commerce"), -- fermé 31/08/2019
( 247210, 0, 0, 247, 247, "1ERPRO", "2473111032.", "1erPro CGEM conduite et gestion des entreprises maritimes option commerce"), -- fermé 31/08/2020
( 247211, 0, 0, 247, 247, "TLEPRO", "2473111033.", "TlePro CGEM conduite et gestion des entreprises maritimes option commerce"), -- fermé 31/08/2021
( 247212, 0, 0, 247, 247, "2NDPRO", "2473110631.", "2ndPro logistique"),
( 247213, 0, 0, 247, 247, "1ERPRO", "2473110632.", "1erPro logistique"),
( 247214, 0, 0, 247, 247, "TLEPRO", "2473110633.", "TlePro logistique"),
( 247215, 0, 0, 247, 247, "2NDPRO", "2473110731.", "2ndPro conducteur transport routier de marchandises"),
( 247216, 0, 0, 247, 247, "1ERPRO", "2473110732.", "1erPro conducteur transport routier de marchandises"),
( 247217, 0, 0, 247, 247, "TLEPRO", "2473110733.", "TlePro conducteur transport routier de marchandises"),
( 247218, 0, 0, 247, 247, "2NDPRO", "2473110831.", "2ndPro transport"), -- fermé 2020-08-31
( 247219, 0, 0, 247, 247, "1ERPRO", "2473110832.", "1erPro transport"), -- fermé 2021-08-31
( 247220, 0, 0, 247, 247, "TLEPRO", "2473110833.", "TlePro transport"), -- fermé 2022-08-31
( 247221, 0, 0, 247, 247, "2NDPRO", "2473110931.", "2ndPro transport fluvial"),
( 247222, 0, 0, 247, 247, "1ERPRO", "2473110932.", "1erPro transport fluvial"),
( 247223, 0, 0, 247, 247, "TLEPRO", "2473110933.", "TlePro transport fluvial"),
( 247224, 0, 0, 247, 247, "2NDPRO", "2473120231.", "2ndPro commerce"), -- fermé 31/08/2019
( 247225, 0, 0, 247, 247, "1ERPRO", "2473120232.", "1erPro commerce"), -- fermé 31/08/2020
( 247226, 0, 0, 247, 247, "TLEPRO", "2473120233.", "TlePro commerce"), -- fermé 31/08/2021
( 247227, 0, 0, 247, 247, "2NDPRO", "2473120631.", "2ndPro vente (prospect.nego.suiv.client)"),
( 247228, 0, 0, 247, 247, "1ERPRO", "2473120632.", "1erPro vente (prospect.nego.suiv.client)"),
( 247229, 0, 0, 247, 247, "TLEPRO", "2473120633.", "TlePro vente (prospect.nego.suiv.client)"),
( 247230, 0, 0, 247, 247, "2NDPRO", "2473120931.", "2ndPro poissonnier écailler traiteur"),
( 247231, 0, 0, 247, 247, "1ERPRO", "2473120932.", "1erPro poissonnier écailler traiteur"),
( 247232, 0, 0, 247, 247, "TLEPRO", "2473120933.", "TlePro poissonnier écailler traiteur"),
( 247233, 0, 0, 247, 247, "2NDPRO", "2473121031.", "2ndPro accueil -relation clients usagers"), -- fermé 31/08/2019
( 247234, 0, 0, 247, 247, "1ERPRO", "2473121032.", "1erPro accueil -relation clients usagers"), -- fermé 31/08/2020
( 247235, 0, 0, 247, 247, "TLEPRO", "2473121033.", "TlePro accueil -relation clients usagers"), -- fermé 31/08/2021
( 247236, 0, 0, 247, 247, "TLEPRO", "2473220433.", "TlePro production imprimée"),
( 247237, 0, 0, 247, 247, "TLEPRO", "2473220533.", "TlePro production graphique"),
( 247238, 0, 0, 247, 247, "2NDPRO", "2473220631.", "2ndPro façonnage de produits imprimés, routage"),
( 247239, 0, 0, 247, 247, "1ERPRO", "2473220632.", "1erPro façonnage de produits imprimés, routage"),
( 247240, 0, 0, 247, 247, "TLEPRO", "2473220633.", "TlePro façonnage de produits imprimés, routage"),
( 247241, 0, 0, 247, 247, "2NDPRO", "2473220731.", "2ndPro réalis. prod. imprimés & plurimédia option A productions graphiques"),
( 247242, 0, 0, 247, 247, "1ERPRO", "2473220732.", "1erPro réalis. prod. imprimés & plurimédia option A productions graphiques"),
( 247243, 0, 0, 247, 247, "TLEPRO", "2473220733.", "TlePro réalis. prod. imprimés & plurimédia option A productions graphiques"),
( 247244, 0, 0, 247, 247, "2NDPRO", "2473220831.", "2ndPro réalis. prod. imprimés & plurimédia option B productions imprimées"),
( 247245, 0, 0, 247, 247, "1ERPRO", "2473220832.", "1erPro réalis. prod. imprimés & plurimédia option B productions imprimées"),
( 247246, 0, 0, 247, 247, "TLEPRO", "2473220833.", "TlePro réalis. prod. imprimés & plurimédia option B productions imprimées"),
( 247247, 0, 0, 247, 247, "2NDPRO", "2473230331.", "2ndPro artisanat et métiers d’art : com.vis.vis.pluri-m"),
( 247248, 0, 0, 247, 247, "1ERPRO", "2473230332.", "1erPro artisanat et métiers d’art : com.vis.vis.pluri-m"),
( 247249, 0, 0, 247, 247, "TLEPRO", "2473230333.", "TlePro artisanat et métiers d’art : com.vis.vis.pluri-m"),
( 247250, 0, 0, 247, 247, "2NDPRO", "2473230431.", "2ndPro photographie"),
( 247251, 0, 0, 247, 247, "1ERPRO", "2473230432.", "1erPro photographie"),
( 247252, 0, 0, 247, 247, "TLEPRO", "2473230433.", "TlePro photographie"),
( 247253, 0, 0, 247, 247, "2NDPRO", "2473300231.", "2ndPro services de proximité vie locale"),
( 247254, 0, 0, 247, 247, "1ERPRO", "2473300232.", "1erPro services de proximité vie locale"),
( 247255, 0, 0, 247, 247, "TLEPRO", "2473300233.", "TlePro services de proximité vie locale"),
( 247256, 0, 0, 247, 247, "2NDPRO", "2473300731.", "2ndPro accompagnement, soins et services à la personne"), -- MEF modifiée en 2022
( 247257, 0, 0, 247, 247, "1ERPRO", "2473300732.", "1erPro accompagnement, soins et services à la personne"), -- MEF modifiée en 2023
( 247258, 0, 0, 247, 247, "TLEPRO", "2473300733.", "TlePro accompagnement, soins et services à la personne"), -- MEF modifiée en 2024
( 247259, 0, 0, 247, 247, "2NDPRO", "2473300431.", "2ndPro accompagnement soins & services à la personne opt.en structure"), -- fermé 2022-08-31
( 247260, 0, 0, 247, 247, "1ERPRO", "2473300432.", "1erPro accompagnement soins & services à la personne opt.en structure"), -- fermé 2023-08-31
( 247261, 0, 0, 247, 247, "TLEPRO", "2473300433.", "TlePro accompagnement soins & services à la personne opt.en structure"), -- fermé 2024-08-31
( 247262, 0, 0, 247, 247, "2NDPRO", "2473310131.", "2ndPro optique lunetterie"),
( 247263, 0, 0, 247, 247, "1ERPRO", "2473310132.", "1erPro optique lunetterie"),
( 247264, 0, 0, 247, 247, "TLEPRO", "2473310133.", "TlePro optique lunetterie"),
( 247265, 0, 0, 247, 247, "2NDPRO", "2473310431.", "2ndPro technicien en prothèse dentaire"), -- MEF modifiée en 2020
( 247266, 0, 0, 247, 247, "1ERPRO", "2473310432.", "1erPro technicien en prothèse dentaire"), -- MEF modifiée en 2021
( 247267, 0, 0, 247, 247, "TLEPRO", "2473310433.", "TlePro technicien en prothèse dentaire"), -- MEF modifiée en 2022
( 247268, 0, 0, 247, 247, "2NDPRO", "2473340331.", "2ndPro commercialisation et services en restauration"),
( 247269, 0, 0, 247, 247, "1ERPRO", "2473340332.", "1erPro commercialisation et services en restauration"),
( 247270, 0, 0, 247, 247, "TLEPRO", "2473340333.", "TlePro commercialisation et services en restauration"),
( 247271, 0, 0, 247, 247, "2NDPRO", "2473360631.", "2ndPro esthétique cosmétique parfumerie"), -- MEF modifiée en 2022
( 247272, 0, 0, 247, 247, "1ERPRO", "2473360632.", "1erPro esthétique cosmétique parfumerie"), -- MEF modifiée en 2023
( 247273, 0, 0, 247, 247, "TLEPRO", "2473360633.", "TlePro esthétique cosmétique parfumerie"), -- MEF modifiée en 2024
( 247274, 0, 0, 247, 247, "2NDPRO", "2473360231.", "2ndPro perruquier posticheur"),
( 247275, 0, 0, 247, 247, "1ERPRO", "2473360232.", "1erPro perruquier posticheur"),
( 247276, 0, 0, 247, 247, "TLEPRO", "2473360233.", "TlePro perruquier posticheur"),
( 247277, 0, 0, 247, 247, "TLEPRO", "2473430233.", "TlePro environnement nucléaire"),
( 247278, 0, 0, 247, 247, "2NDPRO", "2473430331.", "2ndPro gestion des pollutions et protection de l’environnement"),
( 247279, 0, 0, 247, 247, "1ERPRO", "2473430332.", "1erPro gestion des pollutions et protection de l’environnement"),
( 247280, 0, 0, 247, 247, "TLEPRO", "2473430333.", "TlePro gestion des pollutions et protection de l’environnement"),
( 247281, 0, 0, 247, 247, "2NDPRO", "2473430431.", "2ndPro hygiène, propreté, stérilisation"),
( 247282, 0, 0, 247, 247, "1ERPRO", "2473430432.", "1erPro hygiène, propreté, stérilisation"),
( 247283, 0, 0, 247, 247, "TLEPRO", "2473430433.", "TlePro hygiène, propreté, sterilisation"),
( 247284, 0, 0, 247, 247, "2NDPRO", "2473430531.", "2ndPro techniques d’interventions sur installations nucléaires"),
( 247285, 0, 0, 247, 247, "1ERPRO", "2473430532.", "1erPro techniques d’interventions sur installations nucléaires"),
( 247286, 0, 0, 247, 247, "TLEPRO", "2473430533.", "TlePro techniques d’interventions sur installations nucléaires"),
( 247287, 0, 0, 247, 247, "TLEPRO", "2473440233.", "TlePro sécurité-prévention"),
( 247288, 0, 0, 247, 247, "2NDPRO", "2473440331.", "2ndPro métiers de la sécurité"),
( 247289, 0, 0, 247, 247, "1ERPRO", "2473440332.", "1erPro métiers de la sécurité"),
( 247290, 0, 0, 247, 247, "TLEPRO", "2473440333.", "TlePro métiers de la sécurité"),
( 247291, 0, 0, 247, 247, "2NDPRO", "2472230731.", "2ndPro artisanat et métiers d’art facteur d’orgues, 2nde commune"), -- MEF modifiée en 2021
( 247292, 0, 0, 247, 247, "2NDPRO", "2472240431.", "2ndPro artisanat et métiers d’art, 2nde commune"),
( 247293, 0, 0, 247, 247, "2NDPRO", "2472300631.", "2ndPro technicien d’études du bâtiment, 2nde commune"), -- fermé 2020-08-31
( 247294, 0, 0, 247, 247, "2NDPRO", "2472320931.", "2ndPro interventions sur le patrimoine bâti, 2nde commune"),
( 247295, 0, 0, 247, 247, "1ERPRO", "2472430332.", "1erPro métiers du cuir : option sellerie garnissage"),
( 247296, 0, 0, 247, 247, "TLEPRO", "2472430333.", "TlePro métiers du cuir : option sellerie garnissage"),
( 247297, 0, 0, 247, 247, "2NDPRO", "2472430431.", "2ndPro métiers du cuir, 2nde commune"),
( 247298, 0, 0, 247, 247, "2NDPRO", "2472522231.", "2ndPro métiers de la maintenance des matériels et véhicules"), -- MEF modifiée en 2021
( 247299, 0, 0, 247, 247, "2NDPRO", "2472521831.", "2ndPro maintenance des véhicules, 2nde commune"), -- fermé 2021-08-31
( 247300, 0, 0, 247, 247, "2NDPRO", "2472530731.", "métiers de l’aéronautique, 2nde commune"), -- MEF modifiée en 2020
( 247301, 0, 0, 247, 247, "2NDPRO", "2472551031.", "2ndPro métiers de l’électricité et de ses environnements connectés"),
( 247302, 0, 0, 247, 247, "1ERPRO", "2472551032.", "1erPro métiers de l’électricité et de ses environnements connectés"),
( 247303, 0, 0, 247, 247, "TLEPRO", "2472551033.", "TlePro métiers de l’électricité et de ses environnements connectés"),
( 247304, 0, 0, 247, 247, "1ERPRO", "2472551332.", "1erPro systèmes numériques option A sûreté sécurité infras. hab. tertiaire"),
( 247305, 0, 0, 247, 247, "TLEPRO", "2472551333.", "TlePro systèmes numériques option A sûreté sécurité infras. hab. tertiaire"),
( 247306, 0, 0, 247, 247, "1ERPRO", "2472551432.", "1erPro systèmes numériques option B audiovisuels réseau équip. domestiques"),
( 247307, 0, 0, 247, 247, "TLEPRO", "2472551433.", "TlePro systèmes numériques option B audiovisuels réseau équip. domestiques"),
( 247308, 0, 0, 247, 247, "1ERPRO", "2472551532.", "1erPro systèmes numériques option C réseaux inform. syst. communicants"),
( 247309, 0, 0, 247, 247, "TLEPRO", "2472551533.", "TlePro systèmes numériques option C réseaux inform. syst. communicants"),
( 247310, 0, 0, 247, 247, "2NDPRO", "2472551631.", "2ndPro systèmes numériques, 2nde commune"),
( 247311, 0, 0, 247, 247, "2NDPRO", "2473220931.", "2ndPro réalisation de produits imprimés et plurimédia, 2nde commune"),
( 247312, 0, 0, 247, 247, "2NDPRO", "2473300531.", "2ndPro accompagnement soins et services à la personne, 2nde commune"),
( 247313, 0, 0, 247, 247, "2NDPRO", "2473310331.", "2ndPro technicien en appareillage orthopédique"),
( 247314, 0, 0, 247, 247, "1ERPRO", "2473310332.", "1erPro technicien en appareillage orthopédique"),
( 247315, 0, 0, 247, 247, "TLEPRO", "2473310333.", "TlePro technicien en appareillage orthopédique"),
( 247316, 0, 0, 247, 247, "2NDPRO", "2472270531.", "2ndPro technicien gaz"),
( 247317, 0, 0, 247, 247, "1ERPRO", "2472270532.", "1erPro technicien gaz"),
( 247318, 0, 0, 247, 247, "TLEPRO", "2472270533.", "TlePro technicien gaz"),
( 247319, 0, 0, 247, 247, "TLEPRO", "2473111133.", "Tlepro conduite et gestion des entr. maritimes option plaisance profess."), -- fermé 31/08/2021
( 247320, 0, 0, 247, 247, "2NDPRO", "2473000231.", "2ndPro 2nde professionnelle services"),
( 247321, 0, 0, 247, 247, "2NDPRO", "2472000531.", "2ndPro 2nde professionnelle production"),
( 247322, 0, 0, 247, 247, "2NDPRO", "2472000631.", "2ndPro métiers constr. durable bâtiment & travaux publics 2nde commune"),
( 247323, 0, 0, 247, 247, "2NDPRO", "2472000731.", "2ndPro métiers de la mer 2nde commune"),
( 247324, 0, 0, 247, 247, "2NDPRO", "2472430331.", "2ndPro métiers du cuir : option sellerie garnissage"),
( 247325, 0, 0, 247, 247, "2NDPRO", "2472551331.", "2ndPro systèmes numériques option A sûreté sécurité infras. hab. tertiaire"),
( 247326, 0, 0, 247, 247, "2NDPRO", "2472551431.", "2ndPro systèmes numériques option B audiovisuels réseau équip. domestiques"),
( 247327, 0, 0, 247, 247, "2NDPRO", "2472551531.", "2ndPro systèmes numériques option C réseaux inform. syst. communicants"),
( 247328, 0, 0, 247, 247, "2NDPRO", "2473000331.", "2ndPro métiers gestion administrive, transport & logistique, 2nde commune"),
( 247329, 0, 0, 247, 247, "2NDPRO", "2473121131.", "2ndPro métiers de la relation client, 2nde commune"),
( 247330, 0, 0, 247, 247, "2NDPRO", "2473121231.", "2ndPro métiers de l’accueil"),
( 247331, 0, 0, 247, 247, "1ERPRO", "2473121232.", "1erPro métiers de l’accueil"),
( 247332, 0, 0, 247, 247, "TLEPRO", "2473121233.", "TlePro métiers de l’accueil"),
( 247333, 0, 0, 247, 247, "2NDPRO", "2473121331.", "2ndPro métier commerce et vente option A animation gestion esp. commercial"),
( 247334, 0, 0, 247, 247, "1ERPRO", "2473121332.", "1erPro métier commerce et vente option A animation gestion esp. commercial"),
( 247335, 0, 0, 247, 247, "TLEPRO", "2473121333.", "TlePro métier commerce et vente option A animation gestion esp. commercial"),
( 247336, 0, 0, 247, 247, "2NDPRO", "2473121431.", "2ndPro métier commerce et vente option B pros.client. valor.offre commerc."),
( 247337, 0, 0, 247, 247, "1ERPRO", "2473121432.", "1erPro métier commerce et vente option B pros.client. valor.offre commerc."),
( 247338, 0, 0, 247, 247, "TLEPRO", "2473121433.", "TlePro métier commerce et vente option B pros.client. valor.offre commerc."),
( 247339, 0, 0, 247, 247, "2NDPRO", "2472210731.", "2ndPro métiers de l’alimentation, 2nde commune"),
( 247340, 0, 0, 247, 247, "2NDPRO", "2472300731.", "2ndPro métiers études et modélisation numérique, 2nde commune"),
( 247341, 0, 0, 247, 247, "2NDPRO", "2473221031.", "2ndPro métiers industries graphiques et communication, 2nde commune"),
( 247342, 0, 0, 247, 247, "2NDPRO", "2473340431.", "2ndPro métiers de l’hôtellerie-restauration, 2nde commune"),
( 247343, 0, 0, 247, 247, "2NDPRO", "2473360531.", "2ndPro métiers de la beauté et du bien-être, 2nde commune"),
( 247344, 0, 0, 247, 247, "2NDPRO", "2472501231.", "2ndPro maintenance des systèmes de production connectés"),
( 247345, 0, 0, 247, 247, "1ERPRO", "2472501232.", "1erPro maintenance des systèmes de production connectés"),
( 247346, 0, 0, 247, 247, "TLEPRO", "2472501233.", "TlePro maintenance des systèmes de production connectés"),
( 247347, 0, 0, 247, 247, "2NDPRO", "2473000431.", "2ndPro assistance à la gestion des organisations et de leurs activités"),
( 247348, 0, 0, 247, 247, "1ERPRO", "2473000432.", "1erPro assistance à la gestion des organisations et de leurs activités"),
( 247349, 0, 0, 247, 247, "TLEPRO", "2473000433.", "TlePro assistance à la gestion des organisations et de leurs activités"),
( 247350, 0, 0, 247, 247, "2NDPRO", "2473111431.", "2ndPro organisation de transport de marchandises"),
( 247351, 0, 0, 247, 247, "1ERPRO", "2473111432.", "1erPro organisation de transport de marchandises"),
( 247352, 0, 0, 247, 247, "TLEPRO", "2473111433.", "TlePro organisation de transport de marchandises"),
( 247353, 0, 0, 247, 247, "2NDPRO", "2473300631.", "2ndPro animation - enfance et personnes âgées"),
( 247354, 0, 0, 247, 247, "1ERPRO", "2473300632.", "1erPro animation - enfance et personnes âgées"),
( 247355, 0, 0, 247, 247, "TLEPRO", "2473300633.", "TlePro animation - enfance et personnes âgées"),
( 247356, 0, 0, 247, 247, "2NDPRO", "2473360431.", "2ndPro métiers de la coiffure"),
( 247357, 0, 0, 247, 247, "1ERPRO", "2473360432.", "1erPro métiers de la coiffure"),
( 247358, 0, 0, 247, 247, "TLEPRO", "2473360433.", "TlePro métiers de la coiffure"),
( 247359, 0, 0, 247, 247, "2NDPRO", "2472000831.", "2ndPro métiers des transitions numérique et énergétique"),
( 247360, 0, 0, 247, 247, "2NDPRO", "2472000931.", "2ndPro modélisation et prototypage 3D"),
( 247361, 0, 0, 247, 247, "1ERPRO", "2472000932.", "1erPro modélisation et prototypage 3D"),
( 247362, 0, 0, 247, 247, "TLEPRO", "2472000933.", "TlePro modélisation et prototypage 3D"),
( 247363, 0, 0, 247, 247, "2NDPRO", "2472010331.", "2ndPro métiers de la réalisation d’ensembles mécaniques et industriels"),
( 247364, 0, 0, 247, 247, "2NDPRO", "2472010431.", "2ndPro métiers du pilotage et de la maintenance d’instal. automatisées"),
( 247365, 0, 0, 247, 247, "2NDPRO", "2472270631.", "2ndPro maintenance et efficacité énergétique"),
( 247366, 0, 0, 247, 247, "1ERPRO", "2472270632.", "1erPro maintenance et efficacité énergétique"),
( 247367, 0, 0, 247, 247, "TLEPRO", "2472270633.", "TlePro maintenance et efficacité énergétique"),
( 247368, 0, 0, 247, 247, "2NDPRO", "2472270731.", "2ndPro installateur en chauffage, climatis. et énergies renouvelables"),
( 247369, 0, 0, 247, 247, "1ERPRO", "2472270732.", "1erPro installateur en chauffage, climatis. et énergies renouvelables"),
( 247370, 0, 0, 247, 247, "TLEPRO", "2472270733.", "TlePro installateur en chauffage, climatis. et énergies renouvelables"),
( 247371, 0, 0, 247, 247, "2NDPRO", "2472340931.", "2ndPro métiers de l’agencement, de la menuiserie et de l’ameublement"),
( 247372, 0, 0, 247, 247, "2NDPRO", "2472400431.", "2ndPro métiers de l’entretien des textiles? 2nde commune"),
( 247373, 0, 0, 247, 247, "2NDPRO", "2472400631.", "2ndPro métiers de l’entretien des textiles option B : pressing"),
( 247374, 0, 0, 247, 247, "1ERPRO", "2472400632.", "1erPro métiers de l’entretien des textiles option B : pressing"),
( 247375, 0, 0, 247, 247, "TLEPRO", "2472400633.", "TlePro métiers de l’entretien des textiles option B : pressing"),
( 247376, 0, 0, 247, 247, "TLEPRO", "2472501033.", "TlePro électromécanicien marine"),
( 247377, 0, 0, 247, 247, "TLEPRO", "2472501133.", "TlePro polyvalent navigant pont/machine"),
( 247378, 0, 0, 247, 247, "2NDPRO", "2472501331.", "2ndPro technicien en réalisation de produits mécaniques option RMO"), -- réalisation et maintenance de outillages
( 247379, 0, 0, 247, 247, "1ERPRO", "2472501332.", "1erPro technicien en réalisation de produits mécaniques option RMO"),
( 247380, 0, 0, 247, 247, "TLEPRO", "2472501333.", "TlePro technicien en réalisation de produits mécaniques option RMO"),
( 247381, 0, 0, 247, 247, "2NDPRO", "2472501431.", "2ndPro technicien en réalisation de produits mécaniques option RSP"), -- réalisation et suivi de production
( 247382, 0, 0, 247, 247, "1ERPRO", "2472501432.", "1erPro technicien en réalisation de produits mécaniques option RSP"),
( 247383, 0, 0, 247, 247, "TLEPRO", "2472501433.", "TlePro technicien en réalisation de produits mécaniques option RSP"),
( 247384, 0, 0, 247, 247, "2NDPRO", "2472551731.", "2ndPro métiers du froid et des énergies renouvelables"),
( 247385, 0, 0, 247, 247, "1ERPRO", "2472551732.", "1erPro métiers du froid et des énergies renouvelables"),
( 247386, 0, 0, 247, 247, "TLEPRO", "2472551733.", "TlePro métiers du froid et des énergies renouvelables"),
( 247387, 0, 0, 247, 247, "TLEPRO", "2473111233.", "TlePro cgem-commerce/plaisance professionnelle option voile"),
( 247388, 0, 0, 247, 247, "TLEPRO", "2473111333.", "TlePro cgem-commerce/plaisance professionnelle option yacht"),

-- 250 BMA (Brevet des Métiers d’Art) en 1 an -- dispositif de formation 250

( 250000, 0, 1, 250, 250,  "1BMA1", "250.....11.", "BMA 1 an"),
( 250001, 0, 0, 250, 250,  "1BMA1", "2502231311.", "1BMA1 armurerie"),
( 250002, 0, 0, 250, 250,  "1BMA1", "2502231611.", "1BMA1 art bijou opt.bijout.joaillerie"),
( 250003, 0, 0, 250, 250,  "1BMA1", "2502231711.", "1BMA1 art bijou opt.bijout.sertissage"),
( 250004, 0, 0, 250, 250,  "1BMA1", "2502231811.", "1BMA1 art bijou opt.bijout.poliss.finit"),
( 250005, 0, 0, 250, 250,  "1BMA1", "2502231911.", "1BMA1 orfèvrerie option monture-tournure"),
( 250006, 0, 0, 250, 250,  "1BMA1", "2502240811.", "1BMA1 céramique"),
( 250007, 0, 0, 250, 250,  "1BMA1", "2502240911.", "1BMA1 verrier décorateur"),
( 250008, 0, 0, 250, 250,  "1BMA1", "2502241011.", "1BMA1 souffleur de verre"),
( 250009, 0, 0, 250, 250,  "1BMA1", "2502320111.", "1BMA1 gravure sur pierre"),
( 250010, 0, 0, 250, 250,  "1BMA1", "2502330111.", "1BMA1 volumes : staff matériaux associés"),
( 250011, 0, 0, 250, 250,  "1BMA1", "2502330211.", "1BMA1 graph.décor option A lettres décor"), -- fermé 31/08/2020
( 250012, 0, 0, 250, 250,  "1BMA1", "2502330311.", "1BMA1 graph.décor option B deco.surf.volum"), -- fermé 31/08/2020
( 250013, 0, 0, 250, 250,  "1BMA1", "2502340911.", "1BMA1 technc.factur.instr.option accordéon"),
( 250014, 0, 0, 250, 250,  "1BMA1", "2502341011.", "1BMA1 technc.factur.instr.op.guitare"),
( 250015, 0, 0, 250, 250,  "1BMA1", "2502341111.", "1BMA1 technc.factur.instr.op.piano"),
( 250016, 0, 0, 250, 250,  "1BMA1", "2502341211.", "1BMA1 ébéniste"),
( 250017, 0, 0, 250, 250,  "1BMA1", "2502410611.", "1BMA1 arts tech.tapis tapisserie lisse"),
( 250018, 0, 0, 250, 250,  "1BMA1", "2502410711.", "1BMA1 arts de la dentelle opt.fuseaux"),
( 250019, 0, 0, 250, 250,  "1BMA1", "2502410811.", "1BMA1 arts de la dentelle opt.aiguilles"),
( 250020, 0, 0, 250, 250,  "1BMA1", "2502421011.", "1BMA1 broderie"),
( 250021, 0, 0, 250, 250,  "1BMA1", "2502510111.", "1BMA1 horlogerie"),
( 250022, 0, 0, 250, 250,  "1BMA1", "2502540111.", "1BMA1 technc.factur.instr.op.inst.vents"),
( 250023, 0, 0, 250, 250,  "1BMA1", "2502540211.", "1BMA1 ferronnier d’art (bma)"),
( 250024, 0, 0, 250, 250,  "1BMA1", "2503220611.", "1BMA1 arts de la reliure et dorure"), -- fermé 31/08/2020
( 250025, 0, 0, 250, 250,  "1BMA1", "2503220711.", "1BMA1 orfèvrerie option gravures-ciselure"),

-- 251 BMA (Brevet des Métiers d’Art) en 2 ans -- dispositif de formation 251

( 251001, 0, 1, 251, 251,  "1BMA2", "251.....21.", "BMA 2 ans, 1e année"),
( 251002, 0, 1, 251, 251,  "2BMA2", "251.....22.", "BMA 2 ans, 2e année"),
( 251003, 0, 0, 251, 251,  "1BMA2", "2512231321.", "1BMA2 armurerie"),
( 251004, 0, 0, 251, 251,  "2BMA2", "2512231322.", "2BMA2 armurerie"),
( 251005, 0, 0, 251, 251,  "1BMA2", "2512231621.", "1BMA2 art bijou opt.bijout.joaillerie"),
( 251006, 0, 0, 251, 251,  "2BMA2", "2512231622.", "2BMA2 art bijou opt.bijout.joaillerie"),
( 251007, 0, 0, 251, 251,  "1BMA2", "2512231721.", "1BMA2 art bijou opt.bijout.sertissage"),
( 251008, 0, 0, 251, 251,  "2BMA2", "2512231722.", "2BMA2 art bijou opt.bijout.sertissage"),
( 251009, 0, 0, 251, 251,  "1BMA2", "2512231821.", "1BMA2 art bijou opt.bijout.poliss.finit"),
( 251010, 0, 0, 251, 251,  "2BMA2", "2512231822.", "2BMA2 art bijou opt.bijout.poliss.finit"),
( 251011, 0, 0, 251, 251,  "1BMA2", "2512231921.", "1BMA2 orfèvrerie option monture-tournure"),
( 251012, 0, 0, 251, 251,  "2BMA2", "2512231922.", "2BMA2 orfèvrerie option monture-tournure"),
( 251013, 0, 0, 251, 251,  "1BMA2", "2512240821.", "1BMA2 céramique"),
( 251014, 0, 0, 251, 251,  "2BMA2", "2512240822.", "2BMA2 céramique"),
( 251015, 0, 0, 251, 251,  "1BMA2", "2512240921.", "1BMA2 verrier décorateur"),
( 251016, 0, 0, 251, 251,  "2BMA2", "2512240922.", "2BMA2 verrier décorateur"),
( 251017, 0, 0, 251, 251,  "1BMA2", "2512241021.", "1BMA2 souffleur de verre"),
( 251018, 0, 0, 251, 251,  "2BMA2", "2512241022.", "2BMA2 souffleur de verre"),
( 251019, 0, 0, 251, 251,  "1BMA2", "2512320121.", "1BMA2 gravure sur pierre"),
( 251020, 0, 0, 251, 251,  "2BMA2", "2512320122.", "2BMA2 gravure sur pierre"),
( 251021, 0, 0, 251, 251,  "1BMA2", "2512330121.", "1BMA2 volumes : staff matériaux associés"),
( 251022, 0, 0, 251, 251,  "2BMA2", "2512330122.", "2BMA2 volumes : staff matériaux associés"),
( 251023, 0, 0, 251, 251,  "1BMA2", "2512330221.", "1BMA2 graph.décor option A lettres décor"), -- fermé 31/08/2019
( 251024, 0, 0, 251, 251,  "2BMA2", "2512330222.", "2BMA2 graph.décor option A lettres décor"), -- fermé 31/08/2020
( 251025, 0, 0, 251, 251,  "1BMA2", "2512330321.", "1BMA2 graph.décor option B deco.surf.volum"), -- fermé 31/08/2019
( 251026, 0, 0, 251, 251,  "2BMA2", "2512330322.", "2BMA2 graph.décor option B deco.surf.volum"), -- fermé 31/08/2020
( 251027, 0, 0, 251, 251,  "1BMA2", "2512340921.", "1BMA2 technc.factur.instr.option accordéon"),
( 251028, 0, 0, 251, 251,  "2BMA2", "2512340922.", "2BMA2 technc.factur.instr.option accordéon"),
( 251029, 0, 0, 251, 251,  "1BMA2", "2512341021.", "1BMA2 technc.factur.instr.op.guitare"),
( 251030, 0, 0, 251, 251,  "2BMA2", "2512341022.", "2BMA2 technc.factur.instr.op.guitare"),
( 251031, 0, 0, 251, 251,  "1BMA2", "2512341121.", "1BMA2 technc.factur.instr.op.piano"),
( 251032, 0, 0, 251, 251,  "2BMA2", "2512341122.", "2BMA2 technc.factur.instr.op.piano"),
( 251033, 0, 0, 251, 251,  "1BMA2", "2512341221.", "1BMA2 ébéniste"),
( 251034, 0, 0, 251, 251,  "2BMA2", "2512341222.", "2BMA2 ébéniste"),
( 251035, 0, 0, 251, 251,  "1BMA2", "2512410621.", "1BMA2 arts tech.tapis tapisserie lisse"),
( 251036, 0, 0, 251, 251,  "2BMA2", "2512410622.", "2BMA2 arts tech.tapis tapisserie lisse"),
( 251037, 0, 0, 251, 251,  "1BMA2", "2512410721.", "1BMA2 arts de la dentelle opt.fuseaux"),
( 251038, 0, 0, 251, 251,  "2BMA2", "2512410722.", "2BMA2 arts de la dentelle opt.fuseaux"),
( 251039, 0, 0, 251, 251,  "1BMA2", "2512410821.", "1BMA2 arts de la dentelle opt.aiguilles"),
( 251040, 0, 0, 251, 251,  "2BMA2", "2512410822.", "2BMA2 arts de la dentelle opt.aiguilles"),
( 251041, 0, 0, 251, 251,  "1BMA2", "2512421021.", "1BMA2 broderie"),
( 251042, 0, 0, 251, 251,  "2BMA2", "2512421022.", "2BMA2 broderie"),
( 251043, 0, 0, 251, 251,  "1BMA2", "2512510121.", "1BMA2 horlogerie"),
( 251044, 0, 0, 251, 251,  "2BMA2", "2512510122.", "2BMA2 horlogerie"),
( 251045, 0, 0, 251, 251,  "1BMA2", "2512540121.", "1BMA2 technc.factur.instr.op.inst.vents"),
( 251046, 0, 0, 251, 251,  "2BMA2", "2512540122.", "2BMA2 technc.factur.instr.op.inst.vents"),
( 251047, 0, 0, 251, 251,  "1BMA2", "2512540221.", "1BMA2 ferronnier d’art (bma)"),
( 251048, 0, 0, 251, 251,  "2BMA2", "2512540222.", "2BMA2 ferronnier d’art (bma)"),
( 251049, 0, 0, 251, 251,  "1BMA2", "2513220821.", "1BMA2 arts de la reliure et de la dorure"), -- MEF modifiée en 2019
( 251050, 0, 0, 251, 251,  "2BMA2", "2513220822.", "2BMA2 arts de la reliure et de la dorure"), -- MEF modifiée en 2020
( 251051, 0, 0, 251, 251,  "1BMA2", "2513220721.", "1BMA2 orfèvrerie option gravures-ciselure"),
( 251052, 0, 0, 251, 251,  "2BMA2", "2513220722.", "2BMA2 orfèvrerie option gravures-ciselure"),
( 251053, 0, 0, 251, 251,  "1BMA2", "2512330421.", "1BMA2 arts graphiques option A : signalétique"),
( 251054, 0, 0, 251, 251,  "2BMA2", "2512330422.", "2BMA2 arts graphiques option A : signalétique"),
( 251055, 0, 0, 251, 251,  "1BMA2", "2512330521.", "1BMA2 arts graphiques option B : décor peint"),
( 251056, 0, 0, 251, 251,  "2BMA2", "2512330522.", "2BMA2 arts graphiques option B : décor peint"),

-- 253 MC (Mention Complémentaire) -- dispositif de formation 253

( 253000, 0, 1, 253, 253,     "MC", "253.....11.", "Mention complémentaire"),
( 253001, 0, 0, 253, 253,     "MC", "2532200111.", "MC agt control. non destructif (mc4)"),
( 253002, 0, 0, 253, 253,     "MC", "2532210311.", "MC employé traiteur (mc5)"),
( 253003, 0, 0, 253, 253,     "MC", "2532211011.", "MC pâtisserie, glacerie, chocolaterie, confiserie spécialisées"), -- MEF modifiée en 2019
( 253004, 0, 0, 253, 253,     "MC", "2532210511.", "MC cuisinier desserts restau. (mc5)"),
( 253005, 0, 0, 253, 253,     "MC", "2532210611.", "MC vendeur spec. alimentat. (mc5)"),
( 253006, 0, 0, 253, 253,     "MC", "2532210711.", "MC boulangerie spécialisée (mc5)"),
( 253007, 0, 0, 253, 253,     "MC", "2532210811.", "MC patisserie boulangère (mc5)"),
( 253008, 0, 0, 253, 253,     "MC", "2532210911.", "MC art de la cuisine allégée"),
( 253009, 0, 0, 253, 253,     "MC", "2532230411.", "MC joaillerie (mc5)"),
( 253010, 0, 0, 253, 253,     "MC", "2532240211.", "MC graveur sur pierre (mc5)"),
( 253011, 0, 0, 253, 253,     "MC", "2532240511.", "MC conducteur machines verrerie (mc5)"),
( 253012, 0, 0, 253, 253,     "MC", "2532270111.", "MC maint.équipt thermiq.indiv. (mc5)"),
( 253013, 0, 0, 253, 253,     "MC", "2532270311.", "MC techn.des services énergie (mc4)"),
( 253014, 0, 0, 253, 253,     "MC", "2532270411.", "MC tech.En. Renouv. A én.électrique"),
( 253015, 0, 0, 253, 253,     "MC", "2532270511.", "MC tech.En. Renouv. A én.thermique"),
( 253016, 0, 0, 253, 253,     "MC", "2532320211.", "MC zinguerie (mc5)"),
( 253017, 0, 0, 253, 253,     "MC", "2532330211.", "MC plaquiste (mc5)"),
( 253018, 0, 0, 253, 253,     "MC", "2532330411.", "MC peinture décoration (mc4)"),
( 253019, 0, 0, 253, 253,     "MC", "2532330611.", "MC vendeur-conseil en produits techniques pour l’habitat (mc4)"),
( 253020, 0, 0, 253, 253,     "MC", "2532340311.", "MC parqueteur (mc5)"),
( 253021, 0, 0, 253, 253,     "MC", "2532420111.", "MC essayage-retouche-vente (mc5)"),
( 253022, 0, 0, 253, 253,     "MC", "2532420211.", "MC décors text. perm. ephem. (mc5)"),
( 253023, 0, 0, 253, 253,     "MC", "2532430111.", "MC piquage articles chaussants (mc5)"),
( 253024, 0, 0, 253, 253,     "MC", "2532500211.", "MC technic.ascensorist. serv.-modern"),
( 253025, 0, 0, 253, 253,     "MC", "2532511811.", "MC real.circuit oleohydr.pneum. (mc5)"),
( 253026, 0, 0, 253, 253,     "MC", "2532512211.", "MC maquettes et prototypes (mc4)"),
( 253027, 0, 0, 253, 253,     "MC", "2532512311.", "MC maint.inst.oleohydrau.pneum. (mc4)"),
( 253028, 0, 0, 253, 253,     "MC", "2532520111.", "MC techn.maint.vehicul.indust. (mc4)"),
( 253029, 0, 0, 253, 253,     "MC", "2532520711.", "MC maint.mot.diesel équipmts (mc5)"),
( 253030, 0, 0, 253, 253,     "MC", "2532520811.", "MC maintenance contrôle matériels"),
( 253031, 0, 0, 253, 253,     "MC", "2532530811.", "MC aéronautique option avions à moteurs à turbines (mc4)"),
( 253032, 0, 0, 253, 253,     "MC", "2532530911.", "MC aéronautique option avions à moteurs à pistons (mc4)"),
( 253033, 0, 0, 253, 253,     "MC", "2532531011.", "MC aéronautique option hélicoptères à moteurs à turbines (mc4)"),
( 253034, 0, 0, 253, 253,     "MC", "2532531111.", "MC aéronautique option hélicoptères à moteurs à pistons (mc4)"),
( 253035, 0, 0, 253, 253,     "MC", "2532531211.", "MC aéronautique option avionique (mc4)"),
( 253036, 0, 0, 253, 253,     "MC", "2532540511.", "MC soudage (mc5)"),
( 253037, 0, 0, 253, 253,     "MC", "2532550711.", "MC maint.syst.embarqués automobile (mc5)"),
( 253038, 0, 0, 253, 253,     "MC", "2532550811.", "MC technicien(ne) en réseaux électriques (mc4)"),
( 253039, 0, 0, 253, 253,     "MC", "2533110111.", "MC accueil dans transports (mc4)"),
( 253040, 0, 0, 253, 253,     "MC", "2533110211.", "MC agent transport expl.ferrov. (mc4)"),
( 253041, 0, 0, 253, 253,     "MC", "2533110311.", "MC transporteur fluvial (mc5)"),
( 253042, 0, 0, 253, 253,     "MC", "2533120111.", "MC assist.conseil vent.distanc. (mc4)"),
( 253043, 0, 0, 253, 253,     "MC", "2533130111.", "MC services financiers (mc4)"),
( 253044, 0, 0, 253, 253,     "MC", "2533300111.", "MC aide a domicile (mc5)"),
( 253045, 0, 0, 253, 253,     "MC", "2533341011.", "MC sommellerie (mc5)"),
( 253046, 0, 0, 253, 253,     "MC", "2533341111.", "MC employé barman (mc5)"),
( 253047, 0, 0, 253, 253,     "MC", "2533341211.", "MC organisateur de réception"),
( 253048, 0, 0, 253, 253,     "MC", "2533341311.", "MC accueil-réception (mc4)"),
( 253049, 0, 0, 253, 253,     "MC", "2533360511.", "MC coiffure coupe couleur (mc5)"),
( 253050, 0, 0, 253, 253,     "MC", "2533420111.", "MC entret.collect. patrimoine (mc5)"),
( 253051, 0, 0, 253, 253,     "MC", "2533440111.", "MC sûreté espaces ouverts public (mc5)"),
( 253052, 0, 0, 253, 253,     "MC", "2533440211.", "MC securité civile et d’entreprise (mc5)"),
( 253053, 0, 0, 253, 253,     "MC", "2532500311.", "MC mécatronique navale (mc4)"),
( 253054, 0, 0, 253, 253,     "MC", "2532540611.", "MC technicien(ne) en soudage"),
( 253055, 0, 0, 253, 253,     "MC", "2533350111.", "MC animation-gestion de projets dans le secteur sportif (mc4)"),
( 253056, 0, 0, 253, 253,     "MC", "2532540811.", "MC technicien(ne) en chaudronnerie aéronautique et spatiale"),
( 253057, 0, 0, 253, 253,     "MC", "2532540711.", "MC technicien(ne) en tuyauterie"),
( 253058, 0, 0, 253, 253,     "MC", "2532540911.", "MC technicien en peinture aéronautique"),
( 253059, 0, 0, 253, 253,     "MC", "2532230611.", "MC art de la dorure à chaud"),
( 253060, 0, 0, 253, 253,     "MC", "2532500411.", "MC opérateur polyvalent en interventions subaquatiques"),
( 253061, 0, 0, 253, 253,     "MC", "2533260111.", "MC services numeriques aux organisations (mc4)"),
( 253062, 0, 0, 253, 253,     "MC", "2533350211.", "MC encadrement secteur sportif option activités physiques pour tous"),
( 253063, 0, 0, 253, 253,     "MC", "2533350311.", "MC encadrement secteur sportif option activités aquatiques et natation"),
( 253064, 0, 0, 253, 253,     "MC", "2533350411.", "MC encadrement secteur sportif option activités forme-cours collectifs"),
( 253065, 0, 0, 253, 253,     "MC", "2533350511.", "MC encadr. secteur sportif opt. activ. forme-haltérophilie,musculation"),

-- 254 BP (Brevet Professionnel) -- dispositif de formation 254

( 254001, 0, 1, 254, 254,   "1BP2", "254.....21.", "Brevet Professionnel 2 ans, 1e année"),
( 254002, 0, 1, 254, 254,   "2BP2", "254.....22.", "Brevet Professionnel 2 ans, 2e année"),
( 254003, 0, 0, 254, 254,   "1BP2", "2542010121.", "1BP2 pilote d’instal.prod.par procédés"),
( 254004, 0, 0, 254, 254,   "2BP2", "2542010122.", "2BP2 pilote d’instal.prod.par procédés"),
( 254005, 0, 0, 254, 254,   "1BP2", "2542200221.", "1BP2 tlr option A biologie"),
( 254006, 0, 0, 254, 254,   "2BP2", "2542200222.", "2BP2 tlr option A biologie"),
( 254007, 0, 0, 254, 254,   "1BP2", "2542200321.", "1BP2 tlr option B physicochimie"),
( 254008, 0, 0, 254, 254,   "2BP2", "2542200322.", "2BP2 tlr option B physicochimie"),
( 254009, 0, 0, 254, 254,   "2BP2", "2542210422.", "2BP2 cuisinier"),
( 254010, 0, 0, 254, 254,   "1BP2", "2542211021.", "1BP2 boucher"),
( 254011, 0, 0, 254, 254,   "2BP2", "2542211022.", "2BP2 boucher"),
( 254012, 0, 0, 254, 254,   "1BP2", "2542210721.", "1BP2 charcutier traiteur"),
( 254013, 0, 0, 254, 254,   "2BP2", "2542210722.", "2BP2 charcutier traiteur"),
( 254014, 0, 0, 254, 254,   "1BP2", "2542210821.", "1BP2 boulanger"),
( 254015, 0, 0, 254, 254,   "2BP2", "2542210822.", "2BP2 boulanger"),
( 254016, 0, 0, 254, 254,   "1BP2", "2542210921.", "1BP2 arts de la cuisine"),
( 254017, 0, 0, 254, 254,   "2BP2", "2542210922.", "2BP2 arts de la cuisine"),
( 254018, 0, 0, 254, 254,   "1BP2", "2542220221.", "1BP2 conducteur appareils ind.chimique"),
( 254019, 0, 0, 254, 254,   "2BP2", "2542220222.", "2BP2 conducteur appareils ind.chimique"),
( 254020, 0, 0, 254, 254,   "1BP2", "2542230621.", "1BP2 gemmologue"),
( 254021, 0, 0, 254, 254,   "2BP2", "2542230622.", "2BP2 gemmologue"),
( 254022, 0, 0, 254, 254,   "1BP2", "2542250121.", "1BP2 mise en œuvre caoutch.& elastom."),
( 254023, 0, 0, 254, 254,   "2BP2", "2542250122.", "2BP2 mise en œuvre caoutch.& elastom."),
( 254024, 0, 0, 254, 254,   "1BP2", "2542250221.", "1BP2 plastiques et composites"),
( 254025, 0, 0, 254, 254,   "2BP2", "2542250222.", "2BP2 plastiques et composites"),
( 254026, 0, 0, 254, 254,   "1BP2", "2542270421.", "1BP2 gaz option A : transport du gaz"), -- fermé 31/08/2018
( 254027, 0, 0, 254, 254,   "2BP2", "2542270422.", "2BP2 gaz option A : transport du gaz"), -- fermé 31/08/2019
( 254028, 0, 0, 254, 254,   "1BP2", "2542270521.", "1BP2 gaz option B : distribution gaz"), -- fermé 31/08/2018
( 254029, 0, 0, 254, 254,   "2BP2", "2542270522.", "2BP2 gaz option B : distribution gaz"), -- fermé 31/08/2019
( 254030, 0, 0, 254, 254,   "1BP2", "2542270921.", "1BP2 monteur en installations du génie climatique et sanitaire"),
( 254031, 0, 0, 254, 254,   "2BP2", "2542270922.", "2BP2 monteur en installations du génie climatique et sanitaire"),
( 254032, 0, 0, 254, 254,   "1BP2", "2542271021.", "1BP2 installateur, dépanneur en froid et conditionnement d’air"),
( 254033, 0, 0, 254, 254,   "2BP2", "2542271022.", "2BP2 installateur, dépanneur en froid et conditionnement d’air"),
( 254034, 0, 0, 254, 254,   "2BP2", "2542310322.", "2BP2 conducteur engins chantier t.p."),
( 254035, 0, 0, 254, 254,   "1BP2", "2542310521.", "1BP2 conducteur d’engins : travaux publics et carrières"),
( 254036, 0, 0, 254, 254,   "2BP2", "2542310522.", "2BP2 conducteur d’engins : travaux publics et carrières"),
( 254037, 0, 0, 254, 254,   "1BP2", "2542321521.", "1BP2 métiers de la piscine"),
( 254038, 0, 0, 254, 254,   "2BP2", "2542321522.", "2BP2 métiers de la piscine"),
( 254039, 0, 0, 254, 254,   "2BP2", "2542321022.", "2BP2 métiers de la pierre"),
( 254040, 0, 0, 254, 254,   "2BP2", "2542321122.", "2BP2 couvreur"),
( 254041, 0, 0, 254, 254,   "1BP2", "2542321621.", "1BP2 maçon"), -- MEF modifiée en 2018
( 254042, 0, 0, 254, 254,   "2BP2", "2542321622.", "2BP2 maçon"), -- MEF modifiée en 2019
( 254043, 0, 0, 254, 254,   "1BP2", "2542321321.", "1BP2 couvreur"),
( 254044, 0, 0, 254, 254,   "2BP2", "2542321322.", "2BP2 couvreur"),
( 254045, 0, 0, 254, 254,   "1BP2", "2542321421.", "1BP2 métiers de la pierre"),
( 254046, 0, 0, 254, 254,   "2BP2", "2542321422.", "2BP2 métiers de la pierre"),
( 254047, 0, 0, 254, 254,   "1BP2", "2542331721.", "1BP2 étanchéité du bâtiment et des travaux publics"), -- MEF modifiée en 2018
( 254048, 0, 0, 254, 254,   "2BP2", "2542331722.", "2BP2 étanchéité du bâtiment et des travaux publics"), -- MEF modifiée en 2019
( 254049, 0, 0, 254, 254,   "1BP2", "2542331621.", "1BP2 carreleur mosaïste"), -- MEF modifiée en 2018
( 254050, 0, 0, 254, 254,   "2BP2", "2542331622.", "2BP2 carreleur mosaïste"), -- MEF modifiée en 2019
( 254051, 0, 0, 254, 254,   "1BP2", "2542331421.", "1BP2 métiers du plâtre et de l’isolation"),
( 254052, 0, 0, 254, 254,   "2BP2", "2542331422.", "2BP2 métiers du plâtre et de l’isolation"),
( 254053, 0, 0, 254, 254,   "1BP2", "2542331521.", "1BP2 peintre applicateur de revêtements"),
( 254054, 0, 0, 254, 254,   "2BP2", "2542331522.", "2BP2 peintre applicateur de revêtements"),
( 254055, 0, 0, 254, 254,   "1BP2", "2542331321.", "1BP2 menuisier aluminium-verre"),
( 254056, 0, 0, 254, 254,   "2BP2", "2542331322.", "2BP2 menuisier aluminium-verre"),
( 254057, 0, 0, 254, 254,   "1BP2", "2542340721.", "1BP2 charpentier de marine"),
( 254058, 0, 0, 254, 254,   "2BP2", "2542340722.", "2BP2 charpentier de marine"),
( 254059, 0, 0, 254, 254,   "1BP2", "2542340821.", "1BP2 menuisier"),
( 254060, 0, 0, 254, 254,   "2BP2", "2542340822.", "2BP2 menuisier"),
( 254061, 0, 0, 254, 254,   "1BP2", "2542340921.", "1BP2 charpentier bois"),
( 254062, 0, 0, 254, 254,   "2BP2", "2542340922.", "2BP2 charpentier bois"),
( 254063, 0, 0, 254, 254,   "1BP2", "2542400421.", "1BP2 blanchisseur (se)"),
( 254064, 0, 0, 254, 254,   "2BP2", "2542400422.", "2BP2 blanchisseur (se)"),
( 254065, 0, 0, 254, 254,   "1BP2", "2542400521.", "1BP2 maint.articl.textil. : pressing"),
( 254066, 0, 0, 254, 254,   "2BP2", "2542400522.", "2BP2 maint.articl.textil. : pressing"),
( 254067, 0, 0, 254, 254,   "1BP2", "2542410621.", "1BP2 ameublement : tapisserie décoration"),
( 254068, 0, 0, 254, 254,   "2BP2", "2542410622.", "2BP2 ameublement : tapisserie décoration"),
( 254069, 0, 0, 254, 254,   "1BP2", "2542421421.", "1BP2 vêtement sur mesure : couture flou"),
( 254070, 0, 0, 254, 254,   "2BP2", "2542421422.", "2BP2 vêtement sur mesure : couture flou"),
( 254071, 0, 0, 254, 254,   "1BP2", "2542421521.", "1BP2 vêtement sur mesure : tailleur dame"),
( 254072, 0, 0, 254, 254,   "2BP2", "2542421522.", "2BP2 vêtement sur mesure : tailleur dame"),
( 254073, 0, 0, 254, 254,   "1BP2", "2542421621.", "1BP2 vêtement sur mesure : tailleur homm"),
( 254074, 0, 0, 254, 254,   "2BP2", "2542421622.", "2BP2 vêtement sur mesure : tailleur homm"),
( 254075, 0, 0, 254, 254,   "1BP2", "2542541121.", "1BP2 métallier"),
( 254076, 0, 0, 254, 254,   "2BP2", "2542541122.", "2BP2 métallier"),
( 254077, 0, 0, 254, 254,   "1BP2", "2542551321.", "1BP2 installat. et équipmts électriq."),
( 254078, 0, 0, 254, 254,   "2BP2", "2542551322.", "2BP2 installat. et équipmts électriq."),
( 254079, 0, 0, 254, 254,   "1BP2", "2543100121.", "1BP2 administr.des fonctions publiques"),
( 254080, 0, 0, 254, 254,   "2BP2", "2543100122.", "2BP2 administr.des fonctions publiques"),
( 254081, 0, 0, 254, 254,   "1BP2", "2543120721.", "1BP2 fleuriste"), -- MEF modifiée en 2022
( 254082, 0, 0, 254, 254,   "2BP2", "2543120722.", "2BP2 fleuriste"), -- MEF modifiée en 2022
( 254083, 0, 0, 254, 254,   "1BP2", "2543130121.", "1BP2 banque"), -- fermé 31/08/2019
( 254084, 0, 0, 254, 254,   "2BP2", "2543130122.", "2BP2 banque"), -- fermé 31/08/2020
( 254085, 0, 0, 254, 254,   "1BP2", "2543130621.", "1BP2 assurances"),
( 254086, 0, 0, 254, 254,   "2BP2", "2543130622.", "2BP2 assurances"),
( 254087, 0, 0, 254, 254,   "1BP2", "2543130921.", "1BP2 professions immobilières"),
( 254088, 0, 0, 254, 254,   "2BP2", "2543130922.", "2BP2 professions immobilières"),
( 254089, 0, 0, 254, 254,   "1BP2", "2543220321.", "1BP2 libraire"),
( 254090, 0, 0, 254, 254,   "2BP2", "2543220322.", "2BP2 libraire"),
( 254091, 0, 0, 254, 254,   "1BP2", "2543240121.", "1BP2 bureautique"),
( 254092, 0, 0, 254, 254,   "2BP2", "2543240122.", "2BP2 bureautique"),
( 254093, 0, 0, 254, 254,   "1BP2", "2543310421.", "1BP2 préparateur en pharmacie"),
( 254094, 0, 0, 254, 254,   "2BP2", "2543310422.", "2BP2 préparateur en pharmacie"),
( 254095, 0, 0, 254, 254,   "1BP2", "2543340321.", "1BP2 barman"),
( 254096, 0, 0, 254, 254,   "2BP2", "2543340322.", "2BP2 barman"),
( 254097, 0, 0, 254, 254,   "1BP2", "2543340421.", "1BP2 gouvernante"),
( 254098, 0, 0, 254, 254,   "2BP2", "2543340422.", "2BP2 gouvernante"),
( 254099, 0, 0, 254, 254,   "1BP2", "2543340521.", "1BP2 sommelier"),
( 254100, 0, 0, 254, 254,   "2BP2", "2543340522.", "2BP2 sommelier"),
( 254101, 0, 0, 254, 254,   "2BP2", "2543340622.", "2BP2 restaurant"),
( 254102, 0, 0, 254, 254,   "1BP2", "2543340721.", "1BP2 arts du service et commercialisation en restauration"),
( 254103, 0, 0, 254, 254,   "2BP2", "2543340722.", "2BP2 arts du service et commercialisation en restauration"),
( 254104, 0, 0, 254, 254,   "1BP2", "2543360921.", "1BP2 esthétique cosmétique parfumerie"), -- MEF modifiée en 2019
( 254105, 0, 0, 254, 254,   "2BP2", "2543360922.", "2BP2 esthétique cosmétique parfumerie"), -- MEF modifiée en 2020
( 254106, 0, 0, 254, 254,   "1BP2", "2543360821.", "1BP2 coiffure"),
( 254107, 0, 0, 254, 254,   "2BP2", "2543360822.", "2BP2 coiffure"),
( 254108, 0, 0, 254, 254,   "1BP2", "2543440121.", "1BP2 agent techniq prévention & secur."),
( 254109, 0, 0, 254, 254,   "2BP2", "2543440122.", "2BP2 agent techniq prévention & secur."),
( 254110, 0, 0, 254, 254,   "1BP2", "2543440221.", "1BP2 agent techniq.sécurité transports"),
( 254111, 0, 0, 254, 254,   "2BP2", "2543440222.", "2BP2 agent techniq.sécurité transports"),
( 254112, 0, 0, 254, 254,   "1BP2", "2543360821.", "1BP2 coiffure"),
( 254113, 0, 0, 254, 254,   "2BP2", "2543360822.", "2BP2 coiffure"),
( 254114, 0, 0, 254, 254,   "1BP2", "2542551621.", "1BP2 électricien(ne)"),
( 254115, 0, 0, 254, 254,   "2BP2", "2542551622.", "2BP2 électricien(ne)"),

-- 255 Titre professionnel Niveau IV -- dispositif de formation 255

( 255001, 0, 1, 255, 255,    "TP4", "255..99999.", "Titre professionnel Niveau IV"),
( 255002, 0, 0, 255, 255,    "TP4", "2551099999.", "TP4 formations générales"),
( 255003, 0, 0, 255, 255,    "TP4", "2551199999.", "TP4 mathématiques et sciences"),
( 255004, 0, 0, 255, 255,    "TP4", "2551299999.", "TP4 sciences humaines et droit"),
( 255005, 0, 0, 255, 255,    "TP4", "2551399999.", "TP4 lettres et arts"),
( 255006, 0, 0, 255, 255,    "TP4", "2552099999.", "TP4 spé.pluri-techno de production"),
( 255007, 0, 0, 255, 255,    "TP4", "2552199999.", "TP4 agriculture, pêche, forêt"),
( 255008, 0, 0, 255, 255,    "TP4", "2552299999.", "TP4 transformations"),
( 255009, 0, 0, 255, 255,    "TP4", "2552399999.", "TP4 génie civil construction et bois"),
( 255010, 0, 0, 255, 255,    "TP4", "2552499999.", "TP4 matériaux souples"),
( 255011, 0, 0, 255, 255,    "TP4", "2552599999.", "TP4 mécanique électricité électronique"),
( 255012, 0, 0, 255, 255,    "TP4", "2553099999.", "TP4 spec.pluriv. des services"),
( 255013, 0, 0, 255, 255,    "TP4", "2553199999.", "TP4 échanges et gestion"),
( 255014, 0, 0, 255, 255,    "TP4", "2553299999.", "TP4 communication et information"),
( 255015, 0, 0, 255, 255,    "TP4", "2553399999.", "TP4 services aux personnes"),
( 255016, 0, 0, 255, 255,    "TP4", "2553499999.", "TP4 services à la collectivité"),

-- 256 Titre professionnel Niveau V -- dispositif de formation 256

( 256001, 0, 1, 256, 256,    "TP5", "256..99999.", "Titre professionnel Niveau V"),
( 256002, 0, 0, 256, 256,    "TP5", "2561099999.", "TP5 formations générales"),
( 256003, 0, 0, 256, 256,    "TP5", "2561199999.", "TP5 mathématiques et sciences"),
( 256004, 0, 0, 256, 256,    "TP5", "2561299999.", "TP5 sciences humaines et droit"),
( 256005, 0, 0, 256, 256,    "TP5", "2561399999.", "TP5 lettres et arts"),
( 256006, 0, 0, 256, 256,    "TP5", "2562099999.", "TP5 spé.pluri-techno de production"),
( 256007, 0, 0, 256, 256,    "TP5", "2562199999.", "TP5 agriculture, pêche, forêt"),
( 256008, 0, 0, 256, 256,    "TP5", "2562299999.", "TP5 transformations"),
( 256009, 0, 0, 256, 256,    "TP5", "2562399999.", "TP5 génie civil construction et bois"),
( 256010, 0, 0, 256, 256,    "TP5", "2562499999.", "TP5 matériaux souples"),
( 256011, 0, 0, 256, 256,    "TP5", "2562599999.", "TP5 mécanique électricité électronique"),
( 256012, 0, 0, 256, 256,    "TP5", "2563099999.", "TP5 spec.pluriv. des services"),
( 256013, 0, 0, 256, 256,    "TP5", "2563199999.", "TP5 échanges et gestion"),
( 256014, 0, 0, 256, 256,    "TP5", "2563299999.", "TP5 communication et information"),
( 256015, 0, 0, 256, 256,    "TP5", "2563399999.", "TP5 services aux personnes"),
( 256016, 0, 0, 256, 256,    "TP5", "2563499999.", "TP5 services à la collectivité"),

-- 271 CAPa (Certificat d’Aptitude Professionnelle Agricole) -- dispositif de formation 271

( 271001, 0, 1, 271, 271, "2CAP2A", "271.....22.", "CAP Agricole 2 ans, 1e année"),
( 271002, 0, 1, 271, 271, "1CAP2A", "271.....21.", "CAP Agricole 2 ans, 2e année"),
( 271003, 0, 0, 271, 271, "2CAP2A", "2712100922.", "2CAP2a agriculture des régions chaudes"),
( 271004, 0, 0, 271, 271, "1CAP2A", "2712101021.", "1CAP2a agriculture des régions chaudes"),
( 271005, 0, 0, 271, 271, "2CAP2A", "2712101022.", "2CAP2a agriculture des régions chaudes"),
( 271006, 0, 0, 271, 271, "2CAP2A", "2712111522.", "2CAP2a vigne et vin"),
( 271007, 0, 0, 271, 271, "2CAP2A", "2712111722.", "2CAP2a prod-horticole florale légumière"),
( 271008, 0, 0, 271, 271, "2CAP2A", "2712112022.", "2CAP2a prod-horticoles pépinière"),
( 271009, 0, 0, 271, 271, "2CAP2A", "2712112222.", "2CAP2a prod-horticoles fruitières"),
( 271010, 0, 0, 271, 271, "2CAP2A", "2712112922.", "2CAP2a prod agri matériels prod végétale"),
( 271011, 0, 0, 271, 271, "1CAP2A", "2712113021.", "1CAP2a métiers de l’agriculture"),
( 271012, 0, 0, 271, 271, "2CAP2A", "2712113022.", "2CAP2a métiers de l’agriculture"),
( 271013, 0, 0, 271, 271, "1CAP2A", "2712121321.", "1CAP2a productions aquacoles bpam"),
( 271014, 0, 0, 271, 271, "2CAP2A", "2712121322.", "2CAP2a productions aquacoles bpam"),
( 271015, 0, 0, 271, 271, "2CAP2A", "2712121922.", "2CAP2a maréchalerie"),
( 271016, 0, 0, 271, 271, "1CAP2A", "2712122921.", "1CAP2a maritime et conchylicole"),
( 271017, 0, 0, 271, 271, "2CAP2A", "2712122922.", "2CAP2a maritime et conchylicole"),
( 271018, 0, 0, 271, 271, "2CAP2A", "2712123022.", "2CAP2a prod agri matériels prod animale"),
( 271019, 0, 0, 271, 271, "1CAP2A", "2712123121.", "1CAP2a soigneur d’équidés"),
( 271020, 0, 0, 271, 271, "2CAP2A", "2712123122.", "2CAP2a soigneur d’équidés"),
( 271021, 0, 0, 271, 271, "2CAP2A", "2712123422.", "2CAP2a lad cavalier d’entraînement"),
( 271022, 0, 0, 271, 271, "1CAP2A", "2712123521.", "1CAP2a lad cavalier d’entraînement"),
( 271023, 0, 0, 271, 271, "2CAP2A", "2712123522.", "2CAP2a lad cavalier d’entraînement"),
( 271024, 0, 0, 271, 271, "1CAP2A", "2712123621.", "1CAP2a maréchal ferrant"),
( 271025, 0, 0, 271, 271, "2CAP2A", "2712123622.", "2CAP2a maréchal ferrant"),
( 271026, 0, 0, 271, 271, "1CAP2A", "2712131021.", "1CAP2a entretien espace rural"),
( 271027, 0, 0, 271, 271, "2CAP2A", "2712131022.", "2CAP2a entretien espace rural"),
( 271028, 0, 0, 271, 271, "2CAP2A", "2712131222.", "2CAP2a travaux forestiers bûcheronnage"),
( 271029, 0, 0, 271, 271, "2CAP2A", "2712131322.", "2CAP2a travaux forestiers sylviculture"),
( 271030, 0, 0, 271, 271, "1CAP2A", "2712131421.", "1CAP2a travaux forestiers"),
( 271031, 0, 0, 271, 271, "2CAP2A", "2712131422.", "2CAP2a travaux forestiers"),
( 271032, 0, 0, 271, 271, "2CAP2A", "2712140322.", "2CAP2a travaux paysagers"),
( 271033, 0, 0, 271, 271, "1CAP2A", "2712140521.", "1CAP2a travaux paysagers"),
( 271034, 0, 0, 271, 271, "2CAP2A", "2712140522.", "2CAP2a travaux paysagers"),
( 271035, 0, 0, 271, 271, "2CAP2A", "2713300222.", "2CAP2a services en milieu rural"),
( 271036, 0, 0, 271, 271, "1CAP2A", "2713300321.", "1CAP2a services aux personnes et vente en espace rural"),
( 271037, 0, 0, 271, 271, "2CAP2A", "2713300322.", "2CAP2a services aux personnes et vente en espace rural"),
( 271038, 0, 0, 271, 271, "1CAP2A", "2712123721.", "1CAP2a palefrenier soigneur"),
( 271039, 0, 0, 271, 271, "2CAP2A", "2712123722.", "2CAP2a palefrenier soigneur"),

-- 275 Brevet Professionnel Agricole niveau 4 -- dispositif de formation 275

( 275001, 0, 1, 275, 275,   "BPA4", "275.....99.", "Brevet Professionnel Agricole niveau 4"),
( 275002, 0, 1, 275, 275,   "BPA4", "2752100399.", "BPA4 technicien recherche développement"),
( 275003, 0, 1, 275, 275,   "BPA4", "2752100699.", "BPA4 agroéquipement conducteur machines matériels"),
( 275004, 0, 1, 275, 275,   "BPA4", "2752100799.", "BPA4 responsable entreprise agricole"),
( 275005, 0, 1, 275, 275,   "BPA4", "2752110399.", "BPA4 responsable atelier production horticole"),
( 275006, 0, 1, 275, 275,   "BPA4", "2752110499.", "BPA4 responsable productions legumières fruitières florales pepinières"),
( 275007, 0, 1, 275, 275,   "BPA4", "2752120199.", "BPA4 responsable exploitation aquacole maritime"),
( 275008, 0, 1, 275, 275,   "BPA4", "2752120399.", "BPA4 educateur canin"),
( 275009, 0, 1, 275, 275,   "BPA4", "2752120499.", "BPA4 responsable entreprise hippique"),
( 275010, 0, 1, 275, 275,   "BPA4", "2752120599.", "BPA4 technicien animalier en unité d’expérimentation"),
( 275011, 0, 1, 275, 275,   "BPA4", "2752130399.", "BPA4 responsable chantiers forestiers"),
( 275012, 0, 1, 275, 275,   "BPA4", "2752140399.", "BPA4 aménagements paysagers"),
( 275013, 0, 1, 275, 275,   "BPA4", "2752210299.", "BPA4 industries alimentaires"),

-- 276 Bac Pro A (Baccalauréat Professionnel Agricole) -- dispositif de formation 276

( 276001, 0, 1, 276, 276, "2DPROA", "276.....31.", "Bac Pro Agricole, seconde"),
( 276002, 0, 1, 276, 276, "1EPROA", "276.....32.", "Bac Pro Agricole, première"),
( 276003, 0, 1, 276, 276, "TLPROA", "276.....33.", "Bac Pro Agricole, terminale"),
( 276004, 0, 0, 276, 276, "1EPROA", "2762100132.", "1eProA agro-équipement"),
( 276005, 0, 0, 276, 276, "TLPROA", "2762100133.", "TlProA agro-équipement"),
( 276006, 0, 0, 276, 276, "1EPROA", "2762100432.", "1eProA cgea"),
( 276007, 0, 0, 276, 276, "TLPROA", "2762100433.", "TlProA cgea"),
( 276008, 0, 0, 276, 276, "1EPROA", "2762111332.", "1eProA cge vitivinicole"),
( 276009, 0, 0, 276, 276, "TLPROA", "2762111333.", "TlProA cge vitivinicole"),
( 276010, 0, 0, 276, 276, "2DPROA", "2762111031.", "2dProA productions végétales agroéquip"),
( 276011, 0, 0, 276, 276, "1EPROA", "2762111432.", "1eProA conduite de productions horticoles"), -- MEF modifiée en 2019
( 276012, 0, 0, 276, 276, "TLPROA", "2762111433.", "TlproA conduite de productions horticoles"), -- MEF modifiée en 2020
( 276013, 0, 0, 276, 276, "1EPROA", "2762120332.", "1eProA tech conseil vente en animalerie"),
( 276014, 0, 0, 276, 276, "TLPROA", "2762120333.", "TlproA tech conseil vente en animalerie"),
( 276015, 0, 0, 276, 276, "1EPROA", "2762121332.", "1eProA conduite de productions aquacoles"), -- MEF modifiée en 2023
( 276016, 0, 0, 276, 276, "TLPROA", "2762121333.", "TlProA conduite de productions aquacoles"), -- MEF modifiée en 2024
( 276017, 0, 0, 276, 276, "1EPROA", "2762120732.", "1eProA cgea syst dominante élevage"),
( 276018, 0, 0, 276, 276, "TLPROA", "2762120733.", "TlProA cgea syst dominante élevage"),
( 276019, 0, 0, 276, 276, "2DPROA", "2762120831.", "2dProA productions animales"),
( 276020, 0, 0, 276, 276, "2DPROA", "2762120931.", "2dProA technicien en expérimentation animale"),
( 276021, 0, 0, 276, 276, "1EPROA", "2762121032.", "1eProA conduite-gestion d’une entreprise du secteur canin et félin"),
( 276022, 0, 0, 276, 276, "TLPROA", "2762121033.", "TlproA conduite-gestion d’une entreprise du secteur canin et félin"),
( 276023, 0, 0, 276, 276, "1EPROA", "2762121132.", "1eProA conduite-gestion entreprise hippique"),
( 276024, 0, 0, 276, 276, "TLPROA", "2762121133.", "TlProA conduite-gestion entreprise hippique"),
( 276025, 0, 0, 276, 276, "1EPROA", "2762121232.", "1eProA technicien en expérimentation animale"),
( 276026, 0, 0, 276, 276, "TLPROA", "2762121233.", "TlProA technicien en expérimentation animale"),
( 276027, 0, 0, 276, 276, "1EPROA", "2762130232.", "1eProA gestion des milieux naturels et de la faune"),
( 276028, 0, 0, 276, 276, "TLPROA", "2762130233.", "TlProA gestion des milieux naturels et de la faune"),
( 276029, 0, 0, 276, 276, "1EPROA", "2762130332.", "1eProA forêt"),
( 276030, 0, 0, 276, 276, "TLPROA", "2762130333.", "TlProA forêt"),
( 276031, 0, 0, 276, 276, "2DPROA", "2762140431.", "2dProA nature jardin paysage forêt"),
( 276032, 0, 0, 276, 276, "1EPROA", "2762140532.", "1eProA aménagements paysagers"),
( 276033, 0, 0, 276, 276, "TLPROA", "2762140533.", "TlProA aménagements paysagers"),
( 276034, 0, 0, 276, 276, "1EPROA", "2762140732.", "1eProA technicien conseil vente univers jardinerie"), -- MEF modifiée en 2020
( 276035, 0, 0, 276, 276, "TLPROA", "2762140733.", "TlproA technicien conseil vente univers jardinerie"), -- MEF modifiée en 2021
( 276036, 0, 0, 276, 276, "2DPROA", "2762210331.", "2dProA conseil vente"),
( 276037, 0, 0, 276, 276, "2DPROA", "2762210431.", "2dProA alimentation bio-industries labo"),
( 276038, 0, 0, 276, 276, "1EPROA", "2762210532.", "1eProA laboratoire contrôle qualité"),
( 276039, 0, 0, 276, 276, "TLPROA", "2762210533.", "TlProA laboratoire contrôle qualité"),
( 276040, 0, 0, 276, 276, "1EPROA", "2762210632.", "1eProA technicien conseil vente en alimentation vins spiritueux"), -- fermé 31/08/2021
( 276041, 0, 0, 276, 276, "TLPROA", "2762210633.", "TlproA technicien conseil vente en alimentation vins spiritueux"), -- fermé 31/08/2022
( 276042, 0, 0, 276, 276, "1EPROA", "2762210832.", "1eProA technicien conseil vente en alimentation produits alim et boissons"), -- MEF modifiée en 2021
( 276043, 0, 0, 276, 276, "TLPROA", "2762210833.", "TlproA technicien conseil vente en alimentation produits alim et boissons"), -- MEF modifiée en 2022
( 276044, 0, 0, 276, 276, "2DPROA", "2763300331.", "2dProA service aux personnes et animation dans territoires pour apprentis"), -- MEF modifiée en 2023
( 276045, 0, 0, 276, 276, "1EPROA", "2763300332.", "1eProA service aux personnes et animation dans territoires"), -- MEF modifiée en 2023
( 276046, 0, 0, 276, 276, "TLPROA", "2763300333.", "TlProA service aux personnes et animation dans territoires"), -- MEF modifiée en 2024
( 276047, 0, 0, 276, 276, "2DPROA", "2769999931.", "2dProA sans spécialité définie"),
( 276048, 0, 0, 276, 276, "2DPROA", "2762111231.", "2dProA agricole productions"),
( 276049, 0, 0, 276, 276, "2DPROA", "2762100131.", "2dProA agro-équipement pour apprentis"),
( 276050, 0, 0, 276, 276, "2DPROA", "2762100431.", "2dProA cgea pour apprentis"),
( 276051, 0, 0, 276, 276, "2DPROA", "2762111331.", "2dProA cge vitivinicole pour apprentis"),
( 276052, 0, 0, 276, 276, "2DPROA", "2762111431.", "2dProA conduite de productions horticoles pour apprentis"),
( 276053, 0, 0, 276, 276, "2DPROA", "2762120331.", "2dProA tech conseil vente en animalerie pour apprentis"),
( 276054, 0, 0, 276, 276, "2DPROA", "2762120431.", "2dProA productions aquacoles pour apprentis"), -- fermé 31/08/2023
( 276055, 0, 0, 276, 276, "2DPROA", "2762121031.", "2dProA conduite-gestion entreprise du secteur canin félin pour apprentis"),
( 276056, 0, 0, 276, 276, "2DPROA", "2762121131.", "2dProA conduite-gestion entreprise hippique pour apprentis"),
( 276057, 0, 0, 276, 276, "2DPROA", "2762121231.", "2dProA technicien en expérimentation animale pour apprentis"),
( 276058, 0, 0, 276, 276, "2DPROA", "2762121331.", "2dProA conduite de productions aquacoles pour apprentis"),
( 276059, 0, 0, 276, 276, "2DPROA", "2762130231.", "2dProA gestion des milieux naturels et de la faune pour apprentis"),
( 276060, 0, 0, 276, 276, "2DPROA", "2762130331.", "2dProA forêt pour apprentis"),
( 276061, 0, 0, 276, 276, "2DPROA", "2762140531.", "2dProA aménagements paysagers pour apprentis"),
( 276062, 0, 0, 276, 276, "2DPROA", "2762140731.", "2dProA technicien conseil vente univers jardinerie pour apprentis"),
( 276063, 0, 0, 276, 276, "2DPROA", "2762210531.", "2dProA laboratoire contrôle qualité pour apprentis"),
( 276064, 0, 0, 276, 276, "2DPROA", "2762210831.", "2dProA technicien conseil vente en alimentation boisson pour apprentis"),

-- 277 Brevet Professionnel agricole niveau 5 -- dispositif de formation 277

( 277001, 0, 1, 277, 277,   "BPA5", "277.....99.", "Brevet Professionnel Agricole niveau 5"),
( 277002, 0, 1, 277, 277,   "BPA5", "2772100199.", "BPA5 travaux conduite & entretien engins agricoles"),
( 277003, 0, 1, 277, 277,   "BPA5", "2772110199.", "BPA5 travaux vigne / vin cave"),
( 277004, 0, 1, 277, 277,   "BPA5", "2772110299.", "BPA5 travaux vigne / vigne"),
( 277005, 0, 1, 277, 277,   "BPA5", "2772110399.", "BPA5 travaux des productions horticoles / arboriculture fruitière"),
( 277006, 0, 1, 277, 277,   "BPA5", "2772110499.", "BPA5 travaux des productions horticoles / hortic ornementale légumière"),
( 277007, 0, 1, 277, 277,   "BPA5", "2772120199.", "BPA5 travaux des productions animales / porc volaille"),
( 277008, 0, 1, 277, 277,   "BPA5", "2772120299.", "BPA5 travaux des productions animales / ruminants"),
( 277009, 0, 1, 277, 277,   "BPA5", "2772120399.", "BPA5 travaux des productions animales / polyculture élevage"),
( 277010, 0, 1, 277, 277,   "BPA5", "2772120499.", "BPA5 travaux élevage canin félin"),
( 277011, 0, 1, 277, 277,   "BPA5", "2772130199.", "BPA5 travaux forestiers conduite machines"),
( 277012, 0, 1, 277, 277,   "BPA5", "2772130299.", "BPA5 travaux forestiers bucheronnage"),
( 277013, 0, 1, 277, 277,   "BPA5", "2772130399.", "BPA5 travaux forestiers sylviculture"),
( 277014, 0, 1, 277, 277,   "BPA5", "2772140199.", "BPA5 travaux d’aménagements paysagers"),
( 277015, 0, 1, 277, 277,   "BPA5", "2772210199.", "BPA5 transformations alimentaires / viandes"),
( 277016, 0, 1, 277, 277,   "BPA5", "2772210299.", "BPA5 transformations alimentaires / lait"),
( 277017, 0, 1, 277, 277,   "BPA5", "2772210399.", "BPA5 transformations alimentaires / produits alimentaires"),

-- 278 Certificat spécialisation agricole niveau 4 -- dispositif de formation 278

( 278001, 0, 1, 278, 278,   "CSA4", "278.....99.", "Certificat spécialisation agricole niveau 4"),
( 278002, 0, 1, 278, 278,   "CSA4", "2782100399.", "CSA4 agent collecte approvisionnement"),
( 278003, 0, 1, 278, 278,   "CSA4", "2782100499.", "CSA4 arrosage intégré"),
( 278004, 0, 1, 278, 278,   "CSA4", "2782100599.", "CSA4 production agricole biologique commercialisation"),
( 278005, 0, 1, 278, 278,   "CSA4", "2782100699.", "CSA4 pilote machines bucheronnage"),
( 278006, 0, 1, 278, 278,   "CSA4", "2782110299.", "CSA4 commercialisation des vins"),
( 278007, 0, 1, 278, 278,   "CSA4", "2782110399.", "CSA4 technicien de cave"),
( 278008, 0, 1, 278, 278,   "CSA4", "2782110699.", "CSA4 production cidricole"),
( 278009, 0, 1, 278, 278,   "CSA4", "2782110799.", "CSA4 conduite de la production des plantes à parfum aromatique"),
( 278010, 0, 1, 278, 278,   "CSA4", "2782110899.", "CSA4 conduite de productions maraichères"),
( 278011, 0, 1, 278, 278,   "CSA4", "2782120799.", "CSA4 commerce bétail acheteur estimateur"),
( 278012, 0, 1, 278, 278,   "CSA4", "2782121299.", "CSA4 palmipèdes foie gras commercialisation"),
( 278013, 0, 1, 278, 278,   "CSA4", "2782121399.", "CSA4 production commerciale produits fermiers"),
( 278014, 0, 1, 278, 278,   "CSA4", "2782121499.", "CSA4 élevage avicole commercialisation"),
( 278015, 0, 1, 278, 278,   "CSA4", "2782121599.", "CSA4 éducation et travail des jeunes équidés"),
( 278016, 0, 1, 278, 278,   "CSA4", "2782121899.", "CSA4 apiculture"),
( 278017, 0, 1, 278, 278,   "CSA4", "2782121999.", "CSA4 responsable unité méthanisation"),
( 278018, 0, 1, 278, 278,   "CSA4", "2782122099.", "CSA4 conduite d’un élevage bovin lait"),
( 278019, 0, 1, 278, 278,   "CSA4", "2782122199.", "CSA4 conduite d’un élevage caprin"),
( 278020, 0, 1, 278, 278,   "CSA4", "2782122299.", "CSA4 conduite d’un élevage bovin viande"),
( 278021, 0, 1, 278, 278,   "CSA4", "2782122399.", "CSA4 conduite d’un élevage ovin viande"),
( 278022, 0, 1, 278, 278,   "CSA4", "2782122499.", "CSA4 conduite d’un élevage porcin"),
( 278023, 0, 1, 278, 278,   "CSA4", "2782130199.", "CSA4 débardage à traction animale"),
( 278024, 0, 1, 278, 278,   "CSA4", "2782130299.", "CSA4 technicien cynégétique"),
( 278025, 0, 1, 278, 278,   "CSA4", "2782140199.", "CSA4 création terrain de sport loisir"),
( 278026, 0, 1, 278, 278,   "CSA4", "2782140699.", "CSA4 arboriste élagueur"),
( 278027, 0, 1, 278, 278,   "CSA4", "2782140799.", "CSA4 constructions paysagères"),
( 278028, 0, 1, 278, 278,   "CSA4", "2782140899.", "CSA4 sols sportifs engazonnés"),
( 278029, 0, 1, 278, 278,   "CSA4", "2782140999.", "CSA4 arrosage automatique espaces verts sols sportifs"),
( 278030, 0, 1, 278, 278,   "CSA4", "2782210399.", "CSA4 transformation produits carnés"),
( 278031, 0, 1, 278, 278,   "CSA4", "2782210499.", "CSA4 technicien spécialisé transformation laitière"),
( 278032, 0, 1, 278, 278,   "CSA4", "2783340299.", "CSA4 tourisme vert animation rural"),

-- 279 Certificat spécialisation agricole niveau 5 -- dispositif de formation 279

( 279001, 0, 1, 279, 279,   "CSA5", "279.....99.", "Certificat spécialisation agricole niveau 5"),
( 279002, 0, 1, 279, 279,   "CSA5", "2792100199.", "CSA5 tracteur machine agricole util maintenance"),
( 279003, 0, 1, 279, 279,   "CSA5", "2792110699.", "CSA5 conduite exploitation production agro-biologique"),
( 279004, 0, 1, 279, 279,   "CSA5", "2792120299.", "CSA5 conduite élevage palmipède foie gras"),
( 279005, 0, 1, 279, 279,   "CSA5", "2792120399.", "CSA5 conduite de l’elevage ovin"),
( 279006, 0, 1, 279, 279,   "CSA5", "2792120699.", "CSA5 aide-soignant vétérinaire"),
( 279007, 0, 1, 279, 279,   "CSA5", "2792130199.", "CSA5 techniques cynégétiques"),
( 279008, 0, 1, 279, 279,   "CSA5", "2792140399.", "CSA5 jardinier de golf entretien sols"),
( 279009, 0, 1, 279, 279,   "CSA5", "2792210399.", "CSA5 fabrication commercialisation fromage de chèvre"),
( 279010, 0, 1, 279, 279,   "CSA5", "2793340299.", "CSA5 restauration collective"),
( 279011, 0, 1, 279, 279,   "CSA5", "2793350299.", "CSA5 utilisation conduite attelage chevaux"),

-- 280 CAPA - Apprenti -- dispositif de formation 280

( 280001, 0, 1, 280, 280,   "CAPA", "280.....99.", "CAPA - Apprenti"),
( 280002, 0, 1, 280, 280,   "CAPA", "2802210799.", "CAPA opérateur en industries agroalimentaires / conduite de machines"),
( 280003, 0, 1, 280, 280,   "CAPA", "2802210899.", "CAPA opérateur en industries agroalimentaires / transf prod alimentaires"),

-- 290 Formations spécialisées -- dispositif de formation 290~293 & 318~330

( 290001, 0, 0, 290, 290,   "1PD4", "2902009911.", "1PD4-1 techno.industriell.fondamentales"),
( 290002, 0, 0, 290, 290,   "1PD4", "2902109911.", "1PD4-1 spec.pluriv.de l’agronomie agric."),
( 290003, 0, 0, 290, 290,   "1PD4", "2902209911.", "1PD4-1 spec.pluritechno des transformat."),
( 290004, 0, 0, 290, 290,   "1PD4", "2902309911.", "1PD4-1 spec.pluritechno, génie civil, .."),
( 290005, 0, 0, 290, 290,   "1PD4", "2902409911.", "1PD4-1 spec.pluritechno matériaux soupl."),
( 290006, 0, 0, 290, 290,   "1PD4", "2902509911.", "1PD4-1 spec.pluritechno mecan.-électric."),
( 290007, 0, 0, 290, 290,   "1PD4", "2903009911.", "1PD4-1 spec.plurivalentes des services"),
( 290008, 0, 0, 290, 290,   "1PD4", "2903109911.", "1PD4-1 spec.plurival.echanges & gestion"),
( 290009, 0, 0, 290, 290,   "1PD4", "2903209911.", "1PD4-1 spec.plurival.de la communication"),
( 290010, 0, 0, 290, 290,   "1PD4", "2903309911.", "1PD4-1 spec.pluriv.sanitaires & sociales"),
( 290011, 0, 0, 290, 290,   "1PD4", "2903409911.", "1PD4-1 spec.pluriv.services à la collect"),
( 290012, 0, 0, 290, 290,   "1PD4", "2903310411.", "1PD4-1 aide-soignant"),
( 290013, 0, 0, 290, 290,   "1PD4", "2903310511.", "1PD4-1 auxiliaire de puériculture"),

( 291001, 0, 0, 290, 291,  "MONI1", "2913320221.", "Moniteur éducateur : 1ere année"),
( 291002, 0, 0, 290, 291,  "MONI2", "2913320222.", "Moniteur éducateur : 2eme année"),
( 291003, 0, 0, 290, 291,   "1PD4", "2913320321.", "1PD4-2 tec interv soc et fam"),
( 291004, 0, 0, 290, 291,   "2PD4", "2913320322.", "2PD4-2 tec interv soc et fam"),

( 293001, 0, 0, 290, 293,  "SOIGN", "2933310211.", "Aide soignant"),
( 293002, 0, 0, 290, 293, "AUXPUE", "2933310311.", "Auxiliaire de puériculture"),
( 293003, 0, 0, 290, 293,   "1PD5", "2933320111.", "1PD5-1 auxiliaire vie sociale"),
( 293004, 0, 0, 290, 293, "AIDEMP", "2933320211.", "Aide médico psychologique (dip d’état)"),
( 293005, 0, 0, 290, 293,   "1PD5", "2933320411.", "1PD5-1 Accompagnant éducatif et social accompagnement de la vie à domicile"), -- fermé 31/08/2023
( 293006, 0, 0, 290, 293,   "1PD5", "2933320511.", "1PD5-1 Accompagnant éducatif et social Accomp. vie en structure collective"), -- fermé 31/08/2023
( 293007, 0, 0, 290, 293,   "1PD5", "2933320611.", "1PD5-1 Accompagnant éducatif et social éducation inclusive & vie ordinaire"), -- fermé 31/08/2023
( 293008, 0, 0, 290, 293,   "1PD5", "2933320711.", "1PD5-1 Accompagnant éducatif et social"),

( 318001, 0, 0, 290, 318,  "DCESF", "3183320511.", "DCESF conseiller eco.sociale & familial"), -- fermé 31/08/2020

( 320001, 0, 0, 290, 320, "CM-NIV", "3202000111.", "Mise à niveau BTS DMA arts appliqués"), -- fermé 31/08/2019
( 320002, 0, 0, 290, 320, "CM-NIV", "3202551711.", "Mise à niveau BTS maintenance des systèmes électro-navals"),
( 320003, 0, 0, 320, 320, "CM-NIV", "3203342111.", "Mise à niveau en hôtellerie-restauration"),

( 330001, 0, 0, 290, 330,   "DNTS", "3302270211.", "DNTS maintenance nucléaire"),

-- 301 CPGE (Classes Préparatoires aux Grandes Écoles) -- dispositif de formation 300~301

( 300001, 0, 0, 301, 300, "CLPES1", "3001000511.", "CLPES1 classe préparatoire aux études supérieures"), -- MEF modifiée en 2022
( 300002, 0, 0, 301, 300, "MAR-OM", "3001104411.", "CPGE préparation concours d’officier chef de quart marine-chef"),
( 300003, 0, 0, 301, 300, "MAR-NI", "3001104511.", "CPGE préparation concours navigant ingénieur"),
( 300004, 0, 0, 301, 300, "1CPEC1", "3001201811.", "1CPGE1 classe prépa. économique et commerciale en 1 an bacheliers pro"),

( 301001, 0, 0, 301, 301,  "1MPSI", "3011101821.", "CPGE1 MPSI (mathématique, physique et sciences de l’ingénieur)"),
( 301002, 0, 0, 301, 301,  "1PCSI", "3011101921.", "CPGE1 PCSI (physique, chimie, sciences de l’ingénieur)"),
( 301003, 0, 0, 301, 301,  "1PTSI", "3011102021.", "CPGE1 PTSI (physique, technologie et sciences de l’ingénieur)"),
( 301004, 0, 0, 301, 301, "1BCPST", "3011102121.", "CPGE1 BCPST (biologie, chimie, physique et sciences de la terre)"),
( 301005, 0, 0, 301, 301, "2BCPST", "3011102122.", "CPGE2 BCPST (biologie, chimie, physique et sciences de la terre)"),
( 301006, 0, 0, 301, 301,   "1TSI", "3011102221.", "CPGE1 TSI (technologie et sciences industrielles)"),
( 301007, 0, 0, 301, 301,   "2TSI", "3011102222.", "CPGE2 TSI (technologie et sciences industrielles)"),
( 301008, 0, 0, 301, 301,   "1TPC", "3011102321.", "CPGE1 TPC (technologie, physique et chimie)"),
( 301009, 0, 0, 301, 301,   "2TPC", "3011102322.", "CPGE2 TPC (technologie, physique et chimie)"),
( 301010, 0, 0, 301, 301,    "1TB", "3011102521.", "CPGE1 TB (technologie et biologie)"),
( 301011, 0, 0, 301, 301,    "2TB", "3011102522.", "CPGE2 TB (technologie et biologie)"),
( 301012, 0, 0, 301, 301,    "2MP", "3011102722.", "CPGE2 MP (mathématique et physique)"),
( 301013, 0, 0, 301, 301,    "2PC", "3011102822.", "CPGE2 PC (physique et chimie)"),
( 301014, 0, 0, 301, 301,   "2PSI", "3011102922.", "CPGE2 PSI (physique et sciences de l’ingénieur)"),
( 301015, 0, 0, 301, 301,    "2PT", "3011103022.", "CPGE2 PT (physique et technologie)"),
( 301016, 0, 0, 301, 301,   "2MP*", "3011103122.", "CPGE2 MP * (mathématique et physique)"),
( 301017, 0, 0, 301, 301,   "2PC*", "3011103222.", "CPGE2 PC * (physique et chimie)"),
( 301018, 0, 0, 301, 301,  "2PSI*", "3011103322.", "CPGE2 PSI * (physique et sciences de l’ingénieur)"),
( 301019, 0, 0, 301, 301,   "2PT*", "3011103422.", "CPGE2 PT * (physique et technologie)"),
( 301020, 0, 0, 301, 301, "CPGE-A", "3011103522.", "CPGE2 agri post bts btsa dut"), -- fermé 31/08/2019
( 301021, 0, 0, 301, 301, "1CAC-C", "3011103921.", "CPGE1 ENS Paris-Saclay arts et design"),
( 301022, 0, 0, 301, 301, "2CAC-C", "3011103922.", "CPGE2 ENS Paris-Saclay arts et design"),
( 301023, 0, 0, 301, 301, "ATSINI", "3011104022.", "CPGE ATS ingénierie industrielle"),
( 301024, 0, 0, 301, 301, "ATSGEC", "3011104122.", "CPGE ATS génie civil"),
( 301025, 0, 0, 301, 301, "ATSMCH", "3011104222.", "CPGE ATS métiers de la chimie"),
( 301026, 0, 0, 301, 301, "ATSBIO", "3011104322.", "CPGE ATS biologie"),
( 301027, 0, 0, 301, 301, "1CA-D1", "3011200121.", "CPGE1 ENS Rennes (département droit, économie, management)"),
( 301028, 0, 0, 301, 301, "2CA-D1", "3011200122.", "CPGE2 ENS Rennes (département droit, économie, management)"),
( 301029, 0, 0, 301, 301, "1CA-D2", "3011200221.", "CPGE1 ENS cachan section_d2"),
( 301030, 0, 0, 301, 301, "2CA-D2", "3011200222.", "CPGE2 ENS cachan section_d2"),
( 301031, 0, 0, 301, 301, "1HEC-T", "3011202021.", "CPGE1 classe préparatoire économique et commerciale technologique (ECT)"), -- MEF modifiée en 2021
( 301032, 0, 0, 301, 301, "2HEC-T", "3011202022.", "CPGE2 classe préparatoire économique et commerciale technologique (ECT)"), -- MEF modifiée en 2021
( 301033, 0, 0, 301, 301,  "CA-D2", "3011201022.", "CPGE ENS cachan section_d2 en 1 an"), -- fermé 31/08/2018
( 301034, 0, 0, 301, 301,  "CA-D1", "3011201122.", "CPGE ENS cachan section_d1 en 1 an"), -- fermé 31/08/2018
( 301035, 0, 0, 301, 301, "1HEC-S", "3011201221.", "CPGE1 classe économique et commerciale, option scientifique"), -- fermé 31/08/2021
( 301036, 0, 0, 301, 301, "2HEC-S", "3011201222.", "CPGE2 classe économique et commerciale, option scientifique"), -- fermé 31/08/2021
( 301037, 0, 0, 301, 301, "1HEC-E", "3011201321.", "CPGE1 classe économique et commerciale, option économique"), -- fermé 31/08/2021
( 301038, 0, 0, 301, 301, "2HEC-E", "3011201322.", "CPGE2 classe économique et commerciale, option économique"), -- fermé 31/08/2021
( 301039, 0, 0, 301, 301, "ATSECG", "3011201622.", "CPGE ATS économie-gestion"),
( 301040, 0, 0, 301, 301, "1LETSS", "3011300121.", "CPGE1 lettres et sciences sociales"),
( 301041, 0, 0, 301, 301, "2LSUPB", "3011300122.", "CPGE2 lettres et sciences sociales"),
( 301042, 0, 0, 301, 301, "1CHART", "3011300221.", "CPGE1 classe préparatoire à l’école nationale des Chartes"),
( 301043, 0, 0, 301, 301, "2CHART", "3011300222.", "CPGE2 classe préparatoire à l’école nationale des Chartes"),
( 301044, 0, 0, 301, 301, "1CYRES", "3011201721.", "CPGE1 école spéciale militaire St-Cyr sciences économiques et sociales"),
( 301045, 0, 0, 301, 301, "2CYRES", "3011201722.", "CPGE2 école spéciale militaire St-Cyr sciences économiques et sociales"),
( 301046, 0, 0, 301, 301, "1LETTR", "3011300421.", "CPGE1 lettres 1ere année"),
( 301047, 0, 0, 301, 301, "2LETTR", "3011300522.", "CPGE2 lettres ENS ULM 2e année"),
( 301048, 0, 0, 301, 301, "2FONCL", "3011300622.", "CPGE2 lettres ENS Lyon sciences humaines 2ème année"),
( 301049, 0, 0, 301, 301, "1CYRLE", "3011300821.", "CPGE1 école spéciale militaire St-Cyr option lettres"),
( 301050, 0, 0, 301, 301, "2CYRLE", "3011300822.", "CPGE2 école spéciale militaire St-Cyr option lettres"),
( 301051, 0, 0, 301, 301,  "1MP2I", "3011104621.", "CPGE1 MP2I 1ère période (math., physique, ingénierie et informatique)"),
( 301052, 0, 0, 301, 301,  "1MP2I", "3011104721.", "CPGE1 MP2I 2ème période option sc. industrielles (mat.phy.ing.info.)"),
( 301053, 0, 0, 301, 301,  "1MP2I", "3011104821.", "CPGE1 MP2I 2ème période option sc. informatiques (mat.phy.ing.info.)"),
( 301054, 0, 0, 301, 301,   "2MPI", "3011104922.", "2CPGE2 MPI (mathématiques, physique, informatique)"),
( 301055, 0, 0, 301, 301,  "2MPI*", "3011105022.", "2CPGE2 MPI * (mathématiques, physique, informatique)"),
( 301056, 0, 0, 301, 301, "1CEC-G", "3011201921.", "CPGE1 classe préparatoire économique et commerciale générale (ECG)"),
( 301057, 0, 0, 301, 301, "2CEC-G", "3011201922.", "CPGE2 classe préparatoire économique et commerciale générale (ECG)");

INSERT INTO sacoche_niveau VALUES 

-- 310 BTS (Brevet de Technicien Supérieur) en 1 an -- dispositif de formation 310

( 310000, 0, 1, 310, 310,  "1BTS1", "310.....11.", "BTS 1 an"),
( 310001, 0, 0, 310, 310,  "1BTS1", "3102000911.", "1BTS1 conception des produits industriels"),
( 310002, 0, 0, 310, 310,  "1BTS1", "3102000711.", "1BTS1 design de produits"), -- fermé 2021-08-31
( 310003, 0, 0, 310, 310,  "1BTS1", "3102000811.", "1BTS1 technico-commercial"),
( 310004, 0, 0, 310, 310,  "1BTS1", "3102011311.", "1BTS1 contrôle industriel et régulation automatique"),
( 310005, 0, 0, 310, 310,  "1BTS1", "3102011111.", "1BTS1 conception et réalisation de systèmes automatiques"),
( 310006, 0, 0, 310, 310,  "1BTS1", "3102011211.", "1BTS1 systèmes numériques option A informatique et réseaux"),
( 310007, 0, 0, 310, 310,  "1BTS1", "3102200211.", "1BTS1 techniq.physiq.pour indust.& labo"),
( 310008, 0, 0, 310, 310,  "1BTS1", "3102210611.", "1BTS1 bioqualité"), -- MEF modifiée en 2021
( 310009, 0, 0, 310, 310,  "1BTS1", "3102210411.", "1BTS1 biotechnologies"),
( 310010, 0, 0, 310, 310,  "1BTS1", "3102220411.", "1BTS1 peintures encres et adhésifs"),
( 310011, 0, 0, 310, 310,  "1BTS1", "3102220611.", "1BTS1 chimiste"),
( 310012, 0, 0, 310, 310,  "1BTS1", "3102220711.", "1BTS1 bioanalyses et contrôle (bts)"),
( 310013, 0, 0, 310, 310,  "1BTS1", "3102230711.", "1BTS1 etud. real.outillag.m.e.f. materx"),
( 310014, 0, 0, 310, 310,  "1BTS1", "3102231111.", "1BTS1 traitement des materiaux option A : traitement thermique"),
( 310015, 0, 0, 310, 310,  "1BTS1", "3102231211.", "1BTS1 traitement des materiaux option B : traitement surface"),
( 310016, 0, 0, 310, 310,  "1BTS1", "3102231411.", "1BTS1 fonderie"),
( 310017, 0, 0, 310, 310,  "1BTS1", "3102240411.", "1BTS1 industries céramiques"),
( 310018, 0, 0, 310, 310,  "1BTS1", "3102240511.", "1BTS1 concepteur art & industries céramiques"), -- fermé 2021-08-31
( 310019, 0, 0, 310, 310,  "1BTS1", "3102250411.", "1BTS1 industries plastiques europlastic"),
( 310020, 0, 0, 310, 310,  "1BTS1", "3102260211.", "1BTS1 ind.pap. : prodct.pates pap.cartons"),
( 310021, 0, 0, 310, 310,  "1BTS1", "3102260311.", "1BTS1 ind.pap. : transform.papier-carton"),
( 310022, 0, 0, 310, 310,  "1BTS1", "3102270411.", "1BTS1 ctrl.rayonmt.ionisants protection"),
( 310023, 0, 0, 310, 310,  "1BTS1", "3102270911.", "1BTS1 fluides-énergies-domotique option A génie climatiq. et fluidiq."),
( 310024, 0, 0, 310, 310,  "1BTS1", "3102271011.", "1BTS1 fluides-énergies-domotique option B froid et conditionn. d’air"),
( 310025, 0, 0, 310, 310,  "1BTS1", "3102271111.", "1BTS1 fluides-énergies-domotique option C domotique et bat. communic."),
( 310026, 0, 0, 310, 310,  "1BTS1", "3102301011.", "1BTS1 études et économie construction"),
( 310027, 0, 0, 310, 310,  "1BTS1", "3102301111.", "1BTS1 design d’espace"),
( 310028, 0, 0, 310, 310,  "1BTS1", "3102301211.", "1BTS1 bâtiment"),
( 310029, 0, 0, 310, 310,  "1BTS1", "3102310911.", "1BTS1 géologie appliquée"), -- MEF modifiée en 2020
( 310030, 0, 0, 310, 310,  "1BTS1", "3102310811.", "1BTS1 métiers du géomètre-topographe et de la modélisation numérique"),
( 310031, 0, 0, 310, 310,  "1BTS1", "3102310711.", "1BTS1 travaux publics"),
( 310032, 0, 0, 310, 310,  "1BTS1", "3102320511.", "1BTS1 enveloppe des bâtiments : conception et réalisation"),
( 310033, 0, 0, 310, 310,  "1BTS1", "3102330511.", "1BTS1 étude et réalisation d’agencement"),
( 310034, 0, 0, 310, 310,  "1BTS1", "3102330611.", "1BTS1 finitions, aménagement des bâtiments : conception et réalisation"), -- MEF modifiée en 2022
( 310035, 0, 0, 310, 310,  "1BTS1", "3102341111.", "1BTS1 développement et réalisation bois"),
( 310036, 0, 0, 310, 310,  "1BTS1", "3102341211.", "1BTS1 systèmes constructifs bois et habitat"),
( 310037, 0, 0, 310, 310,  "1BTS1", "3102411011.", "1BTS1 des.mod.text.env. option B text.m.sur"),
( 310038, 0, 0, 310, 310,  "1BTS1", "3102411111.", "1BTS1 innovation textile option A structures"),
( 310039, 0, 0, 310, 310,  "1BTS1", "3102411211.", "1BTS1 innovation textile option B traitements"),
( 310040, 0, 0, 310, 310,  "1BTS1", "3102420611.", "1BTS1 des.mod.text.env. option A mode"),
( 310041, 0, 0, 310, 310,  "1BTS1", "3102420711.", "1BTS1 métiers de la mode-vêtements"),
( 310042, 0, 0, 310, 310,  "1BTS1", "3102430211.", "1BTS1 ind.cuir op_1 : tannerie mégisserie"),
( 310043, 0, 0, 310, 310,  "1BTS1", "3102430511.", "1BTS1 métiers mode-chaussure maroquiner"),
( 310044, 0, 0, 310, 310,  "1BTS1", "3102500111.", "1BTS1 assistance technique d’ingénieur"),
( 310045, 0, 0, 310, 310,  "1BTS1", "3102500511.", "1BTS1 concept. indust. microtechniques"),
( 310046, 0, 0, 310, 310,  "1BTS1", "3102500611.", "1BTS1 industrialis. produits mécaniques"),
( 310047, 0, 0, 310, 310,  "1BTS1", "3102501411.", "1BTS1 maintenance des systèmes option A systèmes de production"), -- MEF modifiée en 2023
( 310048, 0, 0, 310, 310,  "1BTS1", "3102501511.", "1BTS1 maintenance des systèmes option B syst. énergétiques & fluidiques"), -- MEF modifiée en 2023
( 310049, 0, 0, 310, 310,  "1BTS1", "3102501611.", "1BTS1 maintenance des systèmes option C systèmes éoliens"), -- MEF modifiée en 2023
( 310050, 0, 0, 310, 310,  "1BTS1", "3102521411.", "1BTS1 moteurs à combustion interne"),
( 310051, 0, 0, 310, 310,  "1BTS1", "3102521811.", "1BTS1 maintenance des matériels de construction et de manutention"),
( 310052, 0, 0, 310, 310,  "1BTS1", "3102521011.", "1BTS1 av auto. opt.véhic. particuliers"),
( 310053, 0, 0, 310, 310,  "1BTS1", "3102521111.", "1BTS1 av auto. opt.véhic. industriels"),
( 310054, 0, 0, 310, 310,  "1BTS1", "3102521211.", "1BTS1 av auto. opt.motocycles"),
( 310055, 0, 0, 310, 310,  "1BTS1", "3102521311.", "1BTS1 techniques et services en matériels agricoles"),
( 310056, 0, 0, 310, 310,  "1BTS1", "3102530211.", "1BTS1 aéronautique"),
( 310057, 0, 0, 310, 310,  "1BTS1", "3102541311.", "1BTS1 conception et industrialisation en construction navale"),
( 310058, 0, 0, 310, 310,  "1BTS1", "3102541611.", "1BTS1 architectures en métal : conception et réalisation"), -- MEF modifiée en 2019
( 310059, 0, 0, 310, 310,  "1BTS1", "3102540911.", "1BTS1 mise en forme materx par forgeage"),
( 310060, 0, 0, 310, 310,  "1BTS1", "3102541511.", "1BTS1 conception et réalisation en chaudronnerie industrielle"), -- MEF modifiée en 2019
( 310061, 0, 0, 310, 310,  "1BTS1", "3102541111.", "1BTS1 conception et réalisation de carrosserie"),
( 310062, 0, 0, 310, 310,  "1BTS1", "3102551111.", "1BTS1 génie optique : photonique"),
( 310063, 0, 0, 310, 310,  "1BTS1", "3102551211.", "1BTS1 génie optique : optiq.instrumentale"),
( 310064, 0, 0, 310, 310,  "1BTS1", "3102552011.", "1BTS1 électrotechnique"), -- MEF modifiée en 2021
( 310065, 0, 0, 310, 310,  "1BTS1", "3102551611.", "1BTS1 systèmes numériques option B électronique et communications"),
( 310066, 0, 0, 310, 310,  "1BTS1", "3102551811.", "1BTS1 maintenance des systèmes électro-navals"),
( 310067, 0, 0, 310, 310,  "1BTS1", "3102551911.", "1BTS1 systèmes photoniques"),
( 310068, 0, 0, 310, 310,  "1BTS1", "3103110311.", "1BTS1 gestion des transports et logistique associée"), -- MEF modifiée en 2020
( 310069, 0, 0, 310, 310,  "1BTS1", "3103121311.", "1BTS1 management commercial opérationnel"), -- MEF modifiée en 2020
( 310070, 0, 0, 310, 310,  "1BTS1", "3103121211.", "1BTS1 négociation et digitalisation de la relation client"), -- MEF modifiée en 2019
( 310071, 0, 0, 310, 310,  "1BTS1", "3103121511.", "1BTS1 commerce international"), -- MEF modifiée en 2022
( 310072, 0, 0, 310, 310,  "1BTS1", "3103131111.", "1BTS1 assurance"),
( 310073, 0, 0, 310, 310,  "1BTS1", "3103130911.", "1BTS1 professions immobilières"),
( 310074, 0, 0, 310, 310,  "1BTS1", "3103131011.", "1BTS1 banque, conseiller de clientèle (particuliers)"),
( 310075, 0, 0, 310, 310,  "1BTS1", "3103140611.", "1BTS1 compta. gestion des organisations"),
( 310076, 0, 0, 310, 310,  "1BTS1", "3103140911.", "1BTS1 gestion de la PME"), -- MEF modifiée en 2019
( 310077, 0, 0, 310, 310,  "1BTS1", "3103140811.", "1BTS1 comptabilité et gestion"),
( 310078, 0, 0, 310, 310,  "1BTS1", "3103200211.", "1BTS1 communication"),
( 310079, 0, 0, 310, 310,  "1BTS1", "3103210511.", "1BTS1 design communic. : espace & volume"), -- fermé 2021-08-31
( 310080, 0, 0, 310, 310,  "1BTS1", "3103220611.", "1BTS1 com.i.graph.o.et.real.prdts graph"),
( 310081, 0, 0, 310, 310,  "1BTS1", "3103220711.", "1BTS1 com.i.graph.o.et.real.prdts impr."),
( 310082, 0, 0, 310, 310,  "1BTS1", "3103220811.", "1BTS1 édition"),
( 310083, 0, 0, 310, 310,  "1BTS1", "3103232511.", "1BTS1 photographie"),
( 310084, 0, 0, 310, 310,  "1BTS1", "3103232611.", "1BTS1 design graph. op.com.med imprimés"),
( 310085, 0, 0, 310, 310,  "1BTS1", "3103232711.", "1BTS1 design graph. op.com.med. numeriq"),
( 310086, 0, 0, 310, 310,  "1BTS1", "3103232811.", "1BTS1 métiers audiovisuel option gestion de la production"),
( 310087, 0, 0, 310, 310,  "1BTS1", "3103232911.", "1BTS1 métiers audiovisuel option métiers de l’image"),
( 310088, 0, 0, 310, 310,  "1BTS1", "3103233011.", "1BTS1 métiers audiovisuel option métiers du son"),
( 310089, 0, 0, 310, 310,  "1BTS1", "3103233111.", "1BTS1 métiers audiovisuel option techn. ingénierie et expl. équipements"),
( 310090, 0, 0, 310, 310,  "1BTS1", "3103233211.", "1BTS1 métiers audiovisuel option métiers montage et postproduction"),
( 310091, 0, 0, 310, 310,  "1BTS1", "3103240911.", "1BTS1 support à l’action managériale"), -- MEF modifiée en 2019
( 310092, 0, 0, 310, 310,  "1BTS1", "3103261311.", "1BTS1 services informatiques aux organisations option A SISR"), -- MEF modifiée en 2020
( 310093, 0, 0, 310, 310,  "1BTS1", "3103261411.", "1BTS1 services informatiques aux organisations option B SLAM"), -- MEF modifiée en 2021
( 310094, 0, 0, 310, 310,  "1BTS1", "3103300111.", "1BTS1 service & prest. s.sanit.& social"),
( 310095, 0, 0, 310, 310,  "1BTS1", "3103310311.", "1BTS1 diététique"),
( 310096, 0, 0, 310, 310,  "1BTS1", "3103310711.", "1BTS1 opticien lunetier"),
( 310097, 0, 0, 310, 310,  "1BTS1", "3103310911.", "1BTS1 analyses de biologie médicale"),
( 310098, 0, 0, 310, 310,  "1BTS1", "3103311011.", "1BTS1 prothésiste dentaire"),
( 310099, 0, 0, 310, 310,  "1BTS1", "3103320611.", "1BTS1 économie sociale et familiale"), -- MEF modifiée en 2023
( 310100, 0, 0, 310, 310,  "1BTS1", "3103341411.", "1BTS1 hôtel.rest.option A : mercatq gest.hot."), -- fermé 31/08/2019
( 310101, 0, 0, 310, 310,  "1BTS1", "3103341511.", "1BTS1 hôtel.rest.option B : art cul.tabl.serv"), -- fermé 31/08/2019
( 310102, 0, 0, 310, 310,  "1BTS1", "3103341911.", "1BTS1 respons.heberg. ref.label europ."), -- fermé 31/08/2019
( 310103, 0, 0, 310, 310,  "1BTS1", "3103342611.", "1BTS1 tourisme"), -- MEF modifiée en 2020
( 310104, 0, 0, 310, 310,  "1BTS1", "3103360311.", "1BTS1 métiers esthétique-cosmétique-parfumerie option A management"),
( 310105, 0, 0, 310, 310,  "1BTS1", "3103360411.", "1BTS1 métiers esthétique-cosmétique-parfumerie option B formation-marque"),
( 310106, 0, 0, 310, 310,  "1BTS1", "3103360511.", "1BTS1 métiers esthétique-cosmétique-parfumerie option C cosmétologie"),
( 310107, 0, 0, 310, 310,  "1BTS1", "3103360711.", "1BTS1 métiers de la coiffure"),
( 310108, 0, 0, 310, 310,  "1BTS1", "3102011411.", "1BTS1 métiers de l’eau"), -- MEF modifiée en 2019
( 310109, 0, 0, 310, 310,  "1BTS1", "3103430311.", "1BTS1 environnement nucléaire"),
( 310110, 0, 0, 310, 310,  "1BTS1", "3103430411.", "1BTS1 métiers des services à l’environnement"),
( 310111, 0, 0, 310, 310,  "1BTS1", "3103450111.", "1BTS1 notariat"),
( 310112, 0, 0, 310, 310,  "1BTS1", "3102200311.", "1BTS1 pilotage de procédés"),
( 310113, 0, 0, 310, 310,  "1BTS1", "3102220811.", "1BTS1 métiers de la chimie"),
( 310114, 0, 0, 310, 310,  "1BTS1", "3102231711.", "1BTS1 concept. des proces. de réal. de prod. option B produc. sérielle"),
( 310115, 0, 0, 310, 310,  "1BTS1", "3102231811.", "1BTS1 concept. des proces. de réal. de prod. option A produc. unitaire"),
( 310116, 0, 0, 310, 310,  "1BTS1", "3102250511.", "1BTS1 EuroPlastics et composites option CO conception outillage"),
( 310117, 0, 0, 310, 310,  "1BTS1", "3102250611.", "1BTS1 EuroPlastics et composites option POP pilotage optimisation product."),
( 310118, 0, 0, 310, 310,  "1BTS1", "3102521511.", "1BTS1 maintenance des véhicules option A voitures particulières"),
( 310119, 0, 0, 310, 310,  "1BTS1", "3102521611.", "1BTS1 maintenance des véhicules option B véhicules transport routier"),
( 310120, 0, 0, 310, 310,  "1BTS1", "3102521711.", "1BTS1 maintenance des véhicules option C motocycles"),
( 310121, 0, 0, 310, 310,  "1BTS1", "3102541211.", "1BTS1 forge"),
( 310122, 0, 0, 310, 310,  "1BTS1", "3102541411.", "1BTS1 conception des processus de découpe et d’emboutissage"),
( 310123, 0, 0, 310, 310,  "1BTS1", "3103221011.", "1BTS1 études réal. projet communic. option B réal. produits plurimedia"),
( 310124, 0, 0, 310, 310,  "1BTS1", "3103221111.", "1BTS1 études réal. projet communic. option B réal. produits imprimés"),
( 310125, 0, 0, 310, 310,  "1BTS1", "3103342311.", "1BTS1 management en hôtellerie-restauration option A restauration"),
( 310126, 0, 0, 310, 310,  "1BTS1", "3103342411.", "1BTS1 management en hôtellerie-restauration option B production culinaire"),
( 310127, 0, 0, 310, 310,  "1BTS1", "3103342511.", "1BTS1 management en hôtellerie-restauration option C hébergement"),
( 310128, 0, 0, 310, 310,  "1BTS1", "3103440111.", "1BTS1 management opérationnel de la sécurité"),
( 310129, 0, 0, 310, 310,  "1BTS1", "3102011511.", "1BTS1 métiers de la mesure"),
( 310130, 0, 0, 310, 310,  "1BTS1", "3102301311.", "1BTS1 management économique de la construction"),
( 310131, 0, 0, 310, 310,  "1BTS1", "3102501311.", "1BTS1 mécatronique navale"),
( 310132, 0, 0, 310, 310,  "1BTS1", "3102501711.", "1BTS1 maintenance des systèmes option D syst. ascenseurs et élévateurs"),
( 310133, 0, 0, 310, 310,  "1BTS1", "3102521911.", "1BTS1 motorisations toutes énergies"),
( 310134, 0, 0, 310, 310,  "1BTS1", "3103121611.", "1BTS1 conseil et commercialisation de solutions techniques"),
( 310135, 0, 0, 310, 310,  "1BTS1", "3103300211.", "1BTS1 services & prestations des secteurs sanitaire & social"),
( 310136, 0, 0, 310, 310,  "1BTS1", "3103450211.", "1BTS1 collaborateur juriste notarial"),

-- 311 BTS (Brevet de Technicien Supérieur) en 2 ans -- dispositif de formation 311

( 311001, 0, 1, 311, 311,  "1BTS2", "311.....21.", "BTS 2 ans, 1e année"),
( 311002, 0, 1, 311, 311,  "2BTS2", "311.....22.", "BTS 2 ans, 2e année"),
( 311003, 0, 0, 311, 311,  "1BTS2", "3112000921.", "1BTS2 conception des produits industriel"),
( 311004, 0, 0, 311, 311,  "2BTS2", "3112000922.", "2BTS2 conception des produits industriel"),
( 311005, 0, 0, 311, 311,  "1BTS2", "3112000721.", "1BTS2 design de produits"),
( 311006, 0, 0, 311, 311,  "2BTS2", "3112000722.", "2BTS2 design de produits"),
( 311007, 0, 0, 311, 311,  "1BTS2", "3112000821.", "1BTS2 technico-commercial"),
( 311008, 0, 0, 311, 311,  "2BTS2", "3112000822.", "2BTS2 technico-commercial"),
( 311009, 0, 0, 311, 311,  "1BTS2", "3112011321.", "1BTS2 contrôle industriel et régulation automatique"),
( 311010, 0, 0, 311, 311,  "2BTS2", "3112011322.", "2BTS2 contrôle industriel et régulation automatique"),
( 311011, 0, 0, 311, 311,  "1BTS2", "3112011121.", "1BTS2 concept.et real.syst.automatiques"),
( 311012, 0, 0, 311, 311,  "2BTS2", "3112011122.", "2BTS2 concept.et real.syst.automatiques"),
( 311013, 0, 0, 311, 311,  "1BTS2", "3112011221.", "1BTS2 systèmes numériques option A informatique et réseaux"),
( 311014, 0, 0, 311, 311,  "2BTS2", "3112011222.", "2BTS2 systèmes numériques option A informatique et réseaux"),
( 311015, 0, 0, 311, 311,  "1BTS2", "3112200221.", "1BTS2 techniq.physiq.pour indust.& labo"),
( 311016, 0, 0, 311, 311,  "2BTS2", "3112200222.", "2BTS2 techniq.physiq.pour indust.& labo"),
( 311017, 0, 0, 311, 311,  "1BTS2", "3112210621.", "1BTS2 bioqualité"), -- MEF modifiée en 2020
( 311018, 0, 0, 311, 311,  "2BTS2", "3112210622.", "2BTS2 bioqualité"), -- MEF modifiée en 2021
( 311019, 0, 0, 311, 311,  "1BTS2", "3112210421.", "1BTS2 biotechnologies"),
( 311020, 0, 0, 311, 311,  "2BTS2", "3112210422.", "2BTS2 biotechnologies"),
( 311021, 0, 0, 311, 311,  "1BTS2", "3112220421.", "1BTS2 peintures encres et adhésifs"),
( 311022, 0, 0, 311, 311,  "2BTS2", "3112220422.", "2BTS2 peintures encres et adhésifs"),
( 311023, 0, 0, 311, 311,  "1BTS2", "3112220521.", "1BTS2 biophysicien de laboratoire"),
( 311024, 0, 0, 311, 311,  "2BTS2", "3112220522.", "2BTS2 biophysicien de laboratoire"),
( 311025, 0, 0, 311, 311,  "1BTS2", "3112220621.", "1BTS2 chimiste"),
( 311026, 0, 0, 311, 311,  "2BTS2", "3112220622.", "2BTS2 chimiste"),
( 311027, 0, 0, 311, 311,  "1BTS2", "3112220721.", "1BTS2 bioanalyses et contrôle (bts)"),
( 311028, 0, 0, 311, 311,  "2BTS2", "3112220722.", "2BTS2 bioanalyses et contrôle (bts)"),
( 311029, 0, 0, 311, 311,  "1BTS2", "3112230721.", "1BTS2 etud. real.outillag.m.e.f. materx"),
( 311030, 0, 0, 311, 311,  "2BTS2", "3112230722.", "2BTS2 etud. real.outillag.m.e.f. materx"),
( 311031, 0, 0, 311, 311,  "1BTS2", "3112230821.", "1BTS2 physico-métallographe de labo."),
( 311032, 0, 0, 311, 311,  "2BTS2", "3112230822.", "2BTS2 physico-métallographe de labo."),
( 311033, 0, 0, 311, 311,  "1BTS2", "3112231021.", "1BTS2 traitmt materx : 1ere année comm."),
( 311034, 0, 0, 311, 311,  "2BTS2", "3112231122.", "2BTS2 traitm.materx option A : trait.thermiq"),
( 311035, 0, 0, 311, 311,  "2BTS2", "3112231222.", "2BTS2 traitm.materx option B : trait.surface"),
( 311036, 0, 0, 311, 311,  "1BTS2", "3112231421.", "1BTS2 fonderie"),
( 311037, 0, 0, 311, 311,  "2BTS2", "3112231422.", "2BTS2 fonderie"),
( 311038, 0, 0, 311, 311,  "1BTS2", "3112240421.", "1BTS2 industries céramiques"),
( 311039, 0, 0, 311, 311,  "2BTS2", "3112240422.", "2BTS2 industries céramiques"),
( 311040, 0, 0, 311, 311,  "1BTS2", "3112240521.", "1BTS2 concepteur art & indus.céramiq."),
( 311041, 0, 0, 311, 311,  "2BTS2", "3112240522.", "2BTS2 concepteur art & indus.céramiq."),
( 311042, 0, 0, 311, 311,  "1BTS2", "3112250421.", "1BTS2 industries plastiques europlastic"),
( 311043, 0, 0, 311, 311,  "2BTS2", "3112250422.", "2BTS2 industries plastiques europlastic"),
( 311044, 0, 0, 311, 311,  "1BTS2", "3112260221.", "1BTS2 ind.pap. : prodct.pates pap.cartons"),
( 311045, 0, 0, 311, 311,  "2BTS2", "3112260222.", "2BTS2 ind.pap. : prodct.pates pap.cartons"),
( 311046, 0, 0, 311, 311,  "1BTS2", "3112260321.", "1BTS2 ind.pap. : transform.papier-carton"),
( 311047, 0, 0, 311, 311,  "2BTS2", "3112260322.", "2BTS2 ind.pap. : transform.papier-carton"),
( 311048, 0, 0, 311, 311,  "1BTS2", "3112270421.", "1BTS2 ctrl.rayonmt.ionisants protection"),
( 311049, 0, 0, 311, 311,  "2BTS2", "3112270422.", "2BTS2 ctrl.rayonmt.ionisants protection"),
( 311050, 0, 0, 311, 311,  "1BTS2", "3112270921.", "1BTS2 fluides-énergies-domotique option A génie climatiq. et fluidiq."),
( 311051, 0, 0, 311, 311,  "2BTS2", "3112270922.", "2BTS2 fluides-énergies-domotique option A génie climatiq. et fluidiq."),
( 311052, 0, 0, 311, 311,  "1BTS2", "3112271021.", "1BTS2 fluides-énergies-domotique option B froid et conditionn. d’air"),
( 311053, 0, 0, 311, 311,  "2BTS2", "3112271022.", "2BTS2 fluides-énergies-domotique option B froid et conditionn. d’air"),
( 311054, 0, 0, 311, 311,  "1BTS2", "3112271121.", "1BTS2 fluides-énergies-domotique option C domotique et bat. communic."),
( 311055, 0, 0, 311, 311,  "2BTS2", "3112271122.", "2BTS2 fluides-énergies-domotique option C domotique et bat. communic."),
( 311056, 0, 0, 311, 311,  "1BTS2", "3112301021.", "1BTS2 études et économie construction"),
( 311057, 0, 0, 311, 311,  "2BTS2", "3112301022.", "2BTS2 études et économie construction"),
( 311058, 0, 0, 311, 311,  "1BTS2", "3112301121.", "1BTS2 design d’espace"),
( 311059, 0, 0, 311, 311,  "2BTS2", "3112301122.", "2BTS2 design d’espace"),
( 311060, 0, 0, 311, 311,  "1BTS2", "3112301221.", "1BTS2 bâtiment"),
( 311061, 0, 0, 311, 311,  "2BTS2", "3112301222.", "2BTS2 bâtiment"),
( 311062, 0, 0, 311, 311,  "1BTS2", "3112310921.", "1BTS2 géologie appliquée"), -- MEF modifiée en 2019
( 311063, 0, 0, 311, 311,  "2BTS2", "3112310922.", "2BTS2 géologie appliquée"), -- MEF modifiée en 2020
( 311064, 0, 0, 311, 311,  "1BTS2", "3112310821.", "1BTS2 métiers du géomètre-topographe et de la modélisation numérique"),
( 311065, 0, 0, 311, 311,  "2BTS2", "3112310822.", "2BTS2 métiers du géomètre-topographe et de la modélisation numérique"),
( 311066, 0, 0, 311, 311,  "1BTS2", "3112310721.", "1BTS2 travaux publics"),
( 311067, 0, 0, 311, 311,  "2BTS2", "3112310722.", "2BTS2 travaux publics"),
( 311068, 0, 0, 311, 311,  "1BTS2", "3112320521.", "1BTS2 enveloppe des bâtiments : conception et réalisation"),
( 311069, 0, 0, 311, 311,  "2BTS2", "3112320522.", "2BTS2 enveloppe des bâtiments : conception et réalisation"),
( 311070, 0, 0, 311, 311,  "1BTS2", "3112330521.", "1BTS2 étude et réalisation d’agencement"),
( 311071, 0, 0, 311, 311,  "2BTS2", "3112330522.", "2BTS2 étude et réalisation d’agencement"),
( 311072, 0, 0, 311, 311,  "1BTS2", "3112330421.", "1BTS2 finitions, aménagement des bâtiments : conception et réalisation"), -- MEF modifiée en 2021
( 311073, 0, 0, 311, 311,  "2BTS2", "3112330422.", "2BTS2 finitions, aménagement des bâtiments : conception et réalisation"), -- MEF modifiée en 2022
( 311074, 0, 0, 311, 311,  "1BTS2", "3112341121.", "1BTS2 développement et réalisation bois"),
( 311075, 0, 0, 311, 311,  "2BTS2", "3112341122.", "2BTS2 développement et réalisation bois"),
( 311076, 0, 0, 311, 311,  "1BTS2", "3112341221.", "1BTS2 systèmes constructifs bois et habitat"),
( 311077, 0, 0, 311, 311,  "2BTS2", "3112341222.", "2BTS2 systèmes constructifs bois et habitat"),
( 311078, 0, 0, 311, 311,  "1BTS2", "3112411021.", "1BTS2 des.mod.text.env. option B text.m.sur"),
( 311079, 0, 0, 311, 311,  "2BTS2", "3112411022.", "2BTS2 des.mod.text.env. option B text.m.sur"),
( 311080, 0, 0, 311, 311,  "1BTS2", "3112411121.", "1BTS2 innovation textile option A structures"),
( 311081, 0, 0, 311, 311,  "2BTS2", "3112411122.", "2BTS2 innovation textile option A structures"),
( 311082, 0, 0, 311, 311,  "1BTS2", "3112411221.", "1BTS2 innovation textile option B traitements"),
( 311083, 0, 0, 311, 311,  "2BTS2", "3112411222.", "2BTS2 innovation textile option B traitements"),
( 311084, 0, 0, 311, 311,  "1BTS2", "3112420621.", "1BTS2 des.mod.text.env. option A mode"),
( 311085, 0, 0, 311, 311,  "2BTS2", "3112420622.", "2BTS2 des.mod.text.env. option A mode"),
( 311086, 0, 0, 311, 311,  "1BTS2", "3112420721.", "1BTS2 métiers de la mode-vêtements"),
( 311087, 0, 0, 311, 311,  "2BTS2", "3112420722.", "2BTS2 métiers de la mode-vêtements"),
( 311088, 0, 0, 311, 311,  "1BTS2", "3112430121.", "1BTS2 industrie du cuir 1e an.commune"),
( 311089, 0, 0, 311, 311,  "2BTS2", "3112430222.", "2BTS2 ind.cuir op_1 : tannerie mégisserie"),
( 311090, 0, 0, 311, 311,  "1BTS2", "3112430521.", "1BTS2 métiers mode-chaussure maroquiner"),
( 311091, 0, 0, 311, 311,  "2BTS2", "3112430522.", "2BTS2 métiers mode-chaussure maroquiner"),
( 311092, 0, 0, 311, 311,  "1BTS2", "3112500121.", "1BTS2 assistance technique d’ingénieur"),
( 311093, 0, 0, 311, 311,  "2BTS2", "3112500122.", "2BTS2 assistance technique d’ingénieur"),
( 311094, 0, 0, 311, 311,  "1BTS2", "3112500521.", "1BTS2 concept. indust. microtechniques"),
( 311095, 0, 0, 311, 311,  "2BTS2", "3112500522.", "2BTS2 concept. indust. microtechniques"),
( 311096, 0, 0, 311, 311,  "1BTS2", "3112500621.", "1BTS2 industrialis. produits mécaniques"),
( 311097, 0, 0, 311, 311,  "2BTS2", "3112500622.", "2BTS2 industrialis. produits mécaniques"),
( 311098, 0, 0, 311, 311,  "1BTS2", "3112501421.", "1BTS2 maintenance des systèmes option A systèmes de production"), -- MEF modifiée en 2022
( 311099, 0, 0, 311, 311,  "2BTS2", "3112501422.", "2BTS2 maintenance des systèmes option A systèmes de production"), -- MEF modifiée en 2022
( 311100, 0, 0, 311, 311,  "1BTS2", "3112501521.", "1BTS2 maintenance des systèmes option B syst. énergétiques et fluidiques"), -- MEF modifiée en 2022
( 311101, 0, 0, 311, 311,  "2BTS2", "3112501522.", "2BTS2 maintenance des systèmes option B syst. énergétiques et fluidiques"), -- MEF modifiée en 2022
( 311102, 0, 0, 311, 311,  "1BTS2", "3112501621.", "1BTS2 maintenance des systèmes option C systèmes éoliens"), -- MEF modifiée en 2022
( 311103, 0, 0, 311, 311,  "2BTS2", "3112501622.", "2BTS2 maintenance des systèmes option C systèmes éoliens"), -- MEF modifiée en 2022
( 311104, 0, 0, 311, 311,  "1BTS2", "3112521421.", "1BTS2 moteurs à combustion interne"),
( 311105, 0, 0, 311, 311,  "2BTS2", "3112521422.", "2BTS2 moteurs à combustion interne"),
( 311106, 0, 0, 311, 311,  "1BTS2", "3112521821.", "1BTS2 maintenance des matériels de construction et de manutention"),
( 311107, 0, 0, 311, 311,  "2BTS2", "3112521822.", "2BTS2 maintenance des matériels de construction et de manutention"),
( 311108, 0, 0, 311, 311,  "1BTS2", "3112521021.", "1BTS2 av auto. opt.véhic. particuliers"),
( 311109, 0, 0, 311, 311,  "2BTS2", "3112521022.", "2BTS2 av auto. opt.véhic. particuliers"),
( 311110, 0, 0, 311, 311,  "1BTS2", "3112521121.", "1BTS2 av auto. opt.véhic. industriels"),
( 311111, 0, 0, 311, 311,  "2BTS2", "3112521122.", "2BTS2 av auto. opt.véhic. industriels"),
( 311112, 0, 0, 311, 311,  "1BTS2", "3112521221.", "1BTS2 av auto. opt.motocycles"),
( 311113, 0, 0, 311, 311,  "2BTS2", "3112521222.", "2BTS2 av auto. opt.motocycles"),
( 311114, 0, 0, 311, 311,  "1BTS2", "3112521321.", "1BTS2 techniques et services en matériels agricoles"),
( 311115, 0, 0, 311, 311,  "2BTS2", "3112521322.", "2BTS2 techniques et services en matériels agricoles"),
( 311116, 0, 0, 311, 311,  "1BTS2", "3112530221.", "1BTS2 aéronautique"),
( 311117, 0, 0, 311, 311,  "2BTS2", "3112530222.", "2BTS2 aéronautique"),
( 311118, 0, 0, 311, 311,  "1BTS2", "3112541321.", "1BTS2 conception et industrialisation en construction navale"),
( 311119, 0, 0, 311, 311,  "2BTS2", "3112541322.", "2BTS2 conception et industrialisation en construction navale"),
( 311120, 0, 0, 311, 311,  "1BTS2", "3112541621.", "1BTS2 architectures en métal : conception et réalisation"),
( 311121, 0, 0, 311, 311,  "2BTS2", "3112541622.", "2BTS2 architectures en métal : conception et réalisation"),
( 311122, 0, 0, 311, 311,  "1BTS2", "3112540921.", "1BTS2 mise en forme materx par forgeage"),
( 311123, 0, 0, 311, 311,  "2BTS2", "3112540922.", "2BTS2 mise en forme materx par forgeage"),
( 311124, 0, 0, 311, 311,  "1BTS2", "3112541521.", "1BTS2 conception et réalisation en chaudronnerie industrielle"),
( 311125, 0, 0, 311, 311,  "2BTS2", "3112541522.", "2BTS2 conception et réalisation en chaudronnerie industrielle"),
( 311126, 0, 0, 311, 311,  "1BTS2", "3112541121.", "1BTS2 conception et réalisation de carrosserie"),
( 311127, 0, 0, 311, 311,  "2BTS2", "3112541122.", "2BTS2 conception et réalisation de carrosserie"),
( 311128, 0, 0, 311, 311,  "2BTS2", "3112551122.", "2BTS2 génie optique : photonique"),
( 311129, 0, 0, 311, 311,  "2BTS2", "3112551222.", "2BTS2 génie optique : optiq.instrumentale"),
( 311130, 0, 0, 311, 311,  "1BTS2", "3112552021.", "1BTS2 électrotechnique"), -- MEF modifiée en 2020
( 311131, 0, 0, 311, 311,  "2BTS2", "3112552022.", "2BTS2 électrotechnique"), -- MEF modifiée en 2021
( 311132, 0, 0, 311, 311,  "1BTS2", "3112551621.", "1BTS2 systèmes numériques option B électronique et communications"),
( 311133, 0, 0, 311, 311,  "2BTS2", "3112551622.", "2BTS2 systèmes numériques option B électronique et communications"),
( 311134, 0, 0, 311, 311,  "1BTS2", "3112551821.", "1BTS2 maintenance des systèmes électro-navals"),
( 311135, 0, 0, 311, 311,  "2BTS2", "3112551822.", "2BTS2 maintenance des systèmes électro-navals"),
( 311136, 0, 0, 311, 311,  "1BTS2", "3112551921.", "1BTS2 systèmes photoniques"),
( 311137, 0, 0, 311, 311,  "2BTS2", "3112551922.", "2BTS2 systèmes photoniques"),
( 311138, 0, 0, 311, 311,  "1BTS2", "3113110321.", "1BTS2 gestion des transports et logistique associée"), -- MEF modifiée en 2019
( 311139, 0, 0, 311, 311,  "2BTS2", "3113110322.", "2BTS2 gestion des transports et logistique associée"), -- MEF modifiée en 2020
( 311140, 0, 0, 311, 311,  "1BTS2", "3113121321.", "1BTS2 management commercial opérationnel"), -- MEF modifiée en 2019
( 311141, 0, 0, 311, 311,  "2BTS2", "3113121322.", "2BTS2 management commercial opérationnel"), -- MEF modifiée en 2020
( 311142, 0, 0, 311, 311,  "1BTS2", "3113121221.", "1BTS2 négociation et digitalisation de la relation client"),
( 311143, 0, 0, 311, 311,  "2BTS2", "3113121222.", "2BTS2 négociation et digitalisation de la relation client"),
( 311144, 0, 0, 311, 311,  "1BTS2", "3113121521.", "1BTS2 commerce international"), -- MEF modifiée en 2021
( 311145, 0, 0, 311, 311,  "2BTS2", "3113121522.", "2BTS2 commerce international"), -- MEF modifiée en 2022
( 311146, 0, 0, 311, 311,  "1BTS2", "3113131121.", "1BTS2 assurance"),
( 311147, 0, 0, 311, 311,  "2BTS2", "3113131122.", "2BTS2 assurance"),
( 311148, 0, 0, 311, 311,  "1BTS2", "3113130921.", "1BTS2 professions immobilières"),
( 311149, 0, 0, 311, 311,  "2BTS2", "3113130922.", "2BTS2 professions immobilières"),
( 311150, 0, 0, 311, 311,  "1BTS2", "3113131021.", "1BTS2 banque, conseiller de clientèle (particuliers)"),
( 311151, 0, 0, 311, 311,  "2BTS2", "3113131022.", "2BTS2 banque, conseiller de clientèle (particuliers)"),
( 311152, 0, 0, 311, 311,  "2BTS2", "3113140622.", "2BTS2 compta. gestion des organisations"),
( 311153, 0, 0, 311, 311,  "1BTS2", "3113140921.", "1BTS2 gestion de la PME"),
( 311154, 0, 0, 311, 311,  "2BTS2", "3113140922.", "2BTS2 gestion de la PME"),
( 311155, 0, 0, 311, 311,  "1BTS2", "3113140821.", "1BTS2 comptabilité et gestion"),
( 311156, 0, 0, 311, 311,  "2BTS2", "3113140822.", "2BTS2 comptabilité et gestion"),
( 311157, 0, 0, 311, 311,  "1BTS2", "3113200221.", "1BTS2 communication"),
( 311158, 0, 0, 311, 311,  "2BTS2", "3113200222.", "2BTS2 communication"),
( 311159, 0, 0, 311, 311,  "1BTS2", "3113210521.", "1BTS2 design communic. : espace & volume"),
( 311160, 0, 0, 311, 311,  "2BTS2", "3113210522.", "2BTS2 design communic. : espace & volume"),
( 311161, 0, 0, 311, 311,  "1BTS2", "3113220621.", "1BTS2 com.i.graph.o.et.real.prdts graph"),
( 311162, 0, 0, 311, 311,  "2BTS2", "3113220622.", "2BTS2 com.i.graph.o.et.real.prdts graph"),
( 311163, 0, 0, 311, 311,  "1BTS2", "3113220721.", "1BTS2 com.i.graph.o.et.real.prdts impr."),
( 311164, 0, 0, 311, 311,  "2BTS2", "3113220722.", "2BTS2 com.i.graph.o.et.real.prdts impr."),
( 311165, 0, 0, 311, 311,  "1BTS2", "3113220821.", "1BTS2 édition"),
( 311166, 0, 0, 311, 311,  "2BTS2", "3113220822.", "2BTS2 édition"),
( 311167, 0, 0, 311, 311,  "1BTS2", "3113232521.", "1BTS2 photographie"),
( 311168, 0, 0, 311, 311,  "2BTS2", "3113232522.", "2BTS2 photographie"),
( 311169, 0, 0, 311, 311,  "1BTS2", "3113232621.", "1BTS2 design graph. op.com.med imprimés"),
( 311170, 0, 0, 311, 311,  "2BTS2", "3113232622.", "2BTS2 design graph. op.com.med imprimés"),
( 311171, 0, 0, 311, 311,  "1BTS2", "3113232721.", "1BTS2 design graph. op.com.med. numeriq"),
( 311172, 0, 0, 311, 311,  "2BTS2", "3113232722.", "2BTS2 design graph. op.com.med. numeriq"),
( 311173, 0, 0, 311, 311,  "1BTS2", "3113232821.", "1BTS2 métiers audiovisuel option gestion de la production"),
( 311174, 0, 0, 311, 311,  "2BTS2", "3113232822.", "2BTS2 métiers audiovisuel option gestion de la production"),
( 311175, 0, 0, 311, 311,  "1BTS2", "3113232921.", "1BTS2 métiers audiovisuel option métiers de l’image"),
( 311176, 0, 0, 311, 311,  "2BTS2", "3113232922.", "2BTS2 métiers audiovisuel option métiers de l’image"),
( 311177, 0, 0, 311, 311,  "1BTS2", "3113233021.", "1BTS2 métiers audiovisuel option métiers du son"),
( 311178, 0, 0, 311, 311,  "2BTS2", "3113233022.", "2BTS2 métiers audiovisuel option métiers du son"),
( 311179, 0, 0, 311, 311,  "1BTS2", "3113233121.", "1BTS2 métiers audiovisuel option techn. ingénierie et expl. équipements"),
( 311180, 0, 0, 311, 311,  "2BTS2", "3113233122.", "2BTS2 métiers audiovisuel option techn. ingénierie et expl. équipements"),
( 311181, 0, 0, 311, 311,  "1BTS2", "3113233221.", "1BTS2 métiers audiovisuel option métiers montage et postproduction"),
( 311182, 0, 0, 311, 311,  "2BTS2", "3113233222.", "2BTS2 métiers audiovisuel option métiers montage et postproduction"),
( 311183, 0, 0, 311, 311,  "1BTS2", "3113240921.", "1BTS2 support à l’action managériale"),
( 311184, 0, 0, 311, 311,  "2BTS2", "3113240922.", "2BTS2 support à l’action managériale"),
( 311185, 0, 0, 311, 311,  "1BTS2", "3113261221.", "1BTS2 services informatiques aux organisations 1ère année"), -- MEF modifiée en 2020
( 311186, 0, 0, 311, 311,  "2BTS2", "3113261322.", "2BTS2 services informatiques aux organisations option A SISR"), -- MEF modifiée en 2021
( 311187, 0, 0, 311, 311,  "2BTS2", "3113261422.", "2BTS2 services informatiques aux organisations option B SLAM"), -- MEF modifiée en 2021
( 311188, 0, 0, 311, 311,  "1BTS2", "3113300121.", "1BTS2 service & prest. s.sanit.& social"),
( 311189, 0, 0, 311, 311,  "2BTS2", "3113300122.", "2BTS2 service & prest. s.sanit.& social"),
( 311190, 0, 0, 311, 311,  "1BTS2", "3113310321.", "1BTS2 diététique"),
( 311191, 0, 0, 311, 311,  "2BTS2", "3113310322.", "2BTS2 diététique"),
( 311192, 0, 0, 311, 311,  "1BTS2", "3113310721.", "1BTS2 opticien lunetier"),
( 311193, 0, 0, 311, 311,  "2BTS2", "3113310722.", "2BTS2 opticien lunetier"),
( 311194, 0, 0, 311, 311,  "1BTS2", "3113310921.", "1BTS2 analyses de biologie médicale"),
( 311195, 0, 0, 311, 311,  "2BTS2", "3113310922.", "2BTS2 analyses de biologie médicale"),
( 311196, 0, 0, 311, 311,  "1BTS2", "3113311021.", "1BTS2 prothésiste dentaire"),
( 311197, 0, 0, 311, 311,  "2BTS2", "3113311022.", "2BTS2 prothésiste dentaire"),
( 311198, 0, 0, 311, 311,  "1BTS2", "3113320621.", "1BTS2 économie sociale et familiale"), -- MEF modifiée en 2022
( 311199, 0, 0, 311, 311,  "2BTS2", "3113320622.", "2BTS2 économie sociale et familiale"), -- MEF modifiée en 2023
( 311200, 0, 0, 311, 311,  "1BTS2", "3113342221.", "1BTS2 management en hôtellerie-restauration 1ère année commune"),
( 311201, 0, 0, 311, 311,  "2BTS2", "3113341422.", "2BTS2 hôtel.rest.option A : mercatq gest.hot."), -- fermé 31/08/2019
( 311202, 0, 0, 311, 311,  "2BTS2", "3113341522.", "2BTS2 hôtel.rest.option B : art cul.tabl.serv"), -- fermé 31/08/2019
( 311203, 0, 0, 311, 311,  "1BTS2", "3113341921.", "1BTS2 respons.heberg. ref.label europ."), -- fermé 31/08/2018
( 311204, 0, 0, 311, 311,  "2BTS2", "3113341922.", "2BTS2 respons.heberg. ref.label europ."), -- fermé 31/08/2019
( 311205, 0, 0, 311, 311,  "1BTS2", "3113342621.", "1BTS2 tourisme"), -- MEF modifiée en 2019
( 311206, 0, 0, 311, 311,  "2BTS2", "3113342622.", "2BTS2 tourisme"), -- MEF modifiée en 2020
( 311207, 0, 0, 311, 311,  "2BTS2", "3113360322.", "2BTS2 met. esth.cosm.parf. option A manag."),
( 311208, 0, 0, 311, 311,  "2BTS2", "3113360422.", "2BTS2 met. esth.cosm.parf. option B marq."),
( 311209, 0, 0, 311, 311,  "2BTS2", "3113360522.", "2BTS2 met. esth.cosm.parf. option C cosm."),
( 311210, 0, 0, 311, 311,  "1BTS2", "3113360621.", "1BTS2 met. esth.cosm.parf. 1e ann. comm."),
( 311211, 0, 0, 311, 311,  "1BTS2", "3113360721.", "1BTS2 métiers de la coiffure"),
( 311212, 0, 0, 311, 311,  "2BTS2", "3113360722.", "2BTS2 métiers de la coiffure"),
( 311213, 0, 0, 311, 311,  "1BTS2", "3112011421.", "1BTS2 métiers de l’eau"),
( 311214, 0, 0, 311, 311,  "2BTS2", "3112011422.", "2BTS2 métiers de l’eau"),
( 311215, 0, 0, 311, 311,  "1BTS2", "3113430321.", "1BTS2 environnement nucléaire"),
( 311216, 0, 0, 311, 311,  "2BTS2", "3113430322.", "2BTS2 environnement nucléaire"),
( 311217, 0, 0, 311, 311,  "1BTS2", "3113430421.", "1BTS2 métiers des services à l’environnement"),
( 311218, 0, 0, 311, 311,  "2BTS2", "3113430422.", "2BTS2 métiers des services à l’environnement"),
( 311219, 0, 0, 311, 311,  "1BTS2", "3113450121.", "1BTS2 notariat"),
( 311220, 0, 0, 311, 311,  "2BTS2", "3113450122.", "2BTS2 notariat"),
( 311221, 0, 0, 311, 311,  "1BTS2", "3112200321.", "1BTS2 pilotage de procédés"),
( 311222, 0, 0, 311, 311,  "2BTS2", "3112200322.", "2BTS2 pilotage de procédés"),
( 311223, 0, 0, 311, 311,  "1BTS2", "3112220821.", "1BTS2 métiers de la chimie"),
( 311224, 0, 0, 311, 311,  "2BTS2", "3112220822.", "2BTS2 métiers de la chimie"),
( 311225, 0, 0, 311, 311,  "1BTS2", "3112231621.", "1BTS2 conception des processus réalisation produits 1ère année commune"),
( 311226, 0, 0, 311, 311,  "2BTS2", "3112231722.", "2BTS2 concept. des proces. de réal. de prod. option B produc. sérielle"),
( 311227, 0, 0, 311, 311,  "2BTS2", "3112231822.", "2BTS2 concept. des proces. de réal. de prod. option A produc. unitaire"),
( 311228, 0, 0, 311, 311,  "1BTS2", "3112250521.", "1BTS2 EuroPlastics et composites option CO conception outillage"),
( 311229, 0, 0, 311, 311,  "2BTS2", "3112250522.", "2BTS2 EuroPlastics et composites option CO conception outillage"),
( 311230, 0, 0, 311, 311,  "1BTS2", "3112250621.", "1BTS2 EuroPlastics et composites option POP pilotage optimisation product."),
( 311231, 0, 0, 311, 311,  "2BTS2", "3112250622.", "2BTS2 EuroPlastics et composites option POP pilotage optimisation product."),
( 311232, 0, 0, 311, 311,  "1BTS2", "3112521521.", "1BTS2 maintenance des véhicules option A voitures particulières"),
( 311233, 0, 0, 311, 311,  "2BTS2", "3112521522.", "2BTS2 maintenance des véhicules option A voitures particulières"),
( 311234, 0, 0, 311, 311,  "1BTS2", "3112521621.", "1BTS2 maintenance des véhicules option B véhicules transport routier"),
( 311235, 0, 0, 311, 311,  "2BTS2", "3112521622.", "2BTS2 maintenance des véhicules option B véhicules transport routier"),
( 311236, 0, 0, 311, 311,  "1BTS2", "3112521721.", "1BTS2 maintenance des véhicules option C motocycles"),
( 311237, 0, 0, 311, 311,  "2BTS2", "3112521722.", "2BTS2 maintenance des véhicules option C motocycles"),
( 311238, 0, 0, 311, 311,  "1BTS2", "3112541221.", "1BTS2 forge"),
( 311239, 0, 0, 311, 311,  "2BTS2", "3112541222.", "2BTS2 forge"),
( 311240, 0, 0, 311, 311,  "1BTS2", "3112541421.", "1BTS2 conception des processus de découpe et d’emboutissage"),
( 311241, 0, 0, 311, 311,  "2BTS2", "3112541422.", "2BTS2 conception des processus de découpe et d’emboutissage"),
( 311242, 0, 0, 311, 311,  "1BTS2", "3113220921.", "1BTS2 études de réal. d’un projet de communication 1ère année commune"),
( 311243, 0, 0, 311, 311,  "2BTS2", "3113221022.", "2BTS2 études réal. projet communic. option B réal. produits plurimedia"),
( 311244, 0, 0, 311, 311,  "2BTS2", "3113221122.", "2BTS2 études réal. projet communic. option B réal. produits imprimés"),
( 311245, 0, 0, 311, 311,  "2BTS2", "3113342522.", "2BTS2 management en hôtellerie-restauration option C hébergement"),
( 311246, 0, 0, 311, 311,  "2BTS2", "3113342322.", "2BTS2 management en hôtellerie-restauration option A restauration"),
( 311247, 0, 0, 311, 311,  "2BTS2", "3113342422.", "2BTS2 management en hôtellerie-restauration option B production culinaire"),
( 311248, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 management opérationnel de la sécurité"),
( 311249, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 management opérationnel de la sécurité"),
( 311250, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 métiers de la mesure"),
( 311251, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 métiers de la mesure"),
( 311252, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 management économique de la construction"),
( 311253, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 management économique de la construction"),
( 311254, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 mécatronique navale"),
( 311255, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 mécatronique navale"),
( 311256, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 maintenance des systèmes option D syst. ascenseurs et élévateurs"),
( 311257, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 maintenance des systèmes option D syst. ascenseurs et élévateurs"),
( 311258, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 motorisations toutes énergies"),
( 311259, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 motorisations toutes énergies"),
( 311260, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 conseil et commercialisation de solutions techniques"),
( 311261, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 conseil et commercialisation de solutions techniques"),
( 311262, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 services & prestations des secteurs sanitaire & social"),
( 311263, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 services & prestations des secteurs sanitaire & social"),
( 311264, 0, 0, 311, 311,  "1BTS2", "3113440121.", "1BTS2 collaborateur juriste notarial"),
( 311265, 0, 0, 311, 311,  "2BTS2", "3113440122.", "2BTS2 collaborateur juriste notarial"),

-- 312 BTS (Brevet de Technicien Supérieur) en 3 ans -- dispositif de formation 312

( 312001, 0, 1, 312, 312,  "1BTS3", "312.....31.", "BTS 3 ans, 1e année"),
( 312002, 0, 1, 312, 312,  "2BTS3", "312.....32.", "BTS 3 ans, 2e année"),
( 312003, 0, 1, 312, 312,  "3BTS3", "312.....33.", "BTS 3 ans, 3e année"),
( 312004, 0, 0, 312, 312,  "1BTS3", "3123310431.", "1BTS3 prothésiste orthésiste"),
( 312005, 0, 0, 312, 312,  "2BTS3", "3123310432.", "2BTS3 prothésiste orthésiste"),
( 312006, 0, 0, 312, 312,  "3BTS3", "3123310433.", "3BTS3 prothésiste orthésiste"),
( 312007, 0, 0, 312, 312,  "1BTS3", "3123310531.", "1BTS3 podo-orthésiste"),
( 312008, 0, 0, 312, 312,  "2BTS3", "3123310532.", "2BTS3 podo-orthésiste"),
( 312009, 0, 0, 312, 312,  "3BTS3", "3123310533.", "3BTS3 podo-orthésiste"),

-- 313 DTS (Diplôme de Technicien Supérieur) -- dispositif de formation 313

( 313001, 0, 0, 313, 313,  "1DTS3", "3133310931.", "1DTS3 imagerie médicale radiolo.thérap."),
( 313002, 0, 0, 313, 313,  "2DTS3", "3133310932.", "2DTS3 imagerie médicale radiolo.thérap."),
( 313003, 0, 0, 313, 313,  "3DTS3", "3133310933.", "3DTS3 imagerie médicale radiolo.thérap."),

-- 315 DMA (Diplôme des Métiers d’Art) en 1 an -- dispositif de formation 315

( 315000, 0, 1, 315, 315,  "1DMA1", "315.....11.", "DMA 1 an"),
( 315001, 0, 0, 315, 315,  "1DMA1", "3151330111.", "1DMA1 cinéma d’animation"),
( 315002, 0, 0, 315, 315,  "1DMA1", "3152230111.", "1DMA1 art du bijou et du joyau"),
( 315003, 0, 0, 315, 315,  "1DMA1", "3152240111.", "1DMA1 textil.céramiq : céram.artisanale"),
( 315004, 0, 0, 315, 315,  "1DMA1", "3152240211.", "1DMA1 décor architect.opt.verre cristal"),
( 315005, 0, 0, 315, 315,  "1DMA1", "3152300111.", "1DMA1 habitat : décors et mobiliers"),
( 315006, 0, 0, 315, 315,  "1DMA1", "3152300211.", "1DMA1 habitat : ornements et objets"),
( 315007, 0, 0, 315, 315,  "1DMA1", "3152300311.", "1DMA1 décor archi_a : traitement plastique"),
( 315008, 0, 0, 315, 315,  "1DMA1", "3152300411.", "1DMA1 décor archi_b : décor du mur"),
( 315009, 0, 0, 315, 315,  "1DMA1", "3152300511.", "1DMA1 décor archi_c : métal"),
( 315010, 0, 0, 315, 315,  "1DMA1", "3152300611.", "1DMA1 décor archi_d : matériaux synthèse"),
( 315011, 0, 0, 315, 315,  "1DMA1", "3152340911.", "1DMA1 lutherie"),
( 315012, 0, 0, 315, 315,  "1DMA1", "3152341011.", "1DMA1 habitat opt.restauration mobilier"),
( 315013, 0, 0, 315, 315,  "1DMA1", "3152341111.", "1DMA1 facture instrumentale : accordéon"),
( 315014, 0, 0, 315, 315,  "1DMA1", "3152341211.", "1DMA1 facture instrumentale : guitare"),
( 315015, 0, 0, 315, 315,  "1DMA1", "3152341311.", "1DMA1 facture instrumentale : piano"),
( 315016, 0, 0, 315, 315,  "1DMA1", "3152410111.", "1DMA1 textil.céramique : arts textiles"),
( 315017, 0, 0, 315, 315,  "1DMA1", "3152420411.", "1DMA1 op. costumier réalisateur"),
( 315018, 0, 0, 315, 315,  "1DMA1", "3152510111.", "1DMA1 horlogerie"),
( 315019, 0, 0, 315, 315,  "1DMA1", "3152540111.", "1DMA1 facture instrumentale : instruments a vents"),
( 315020, 0, 0, 315, 315,  "1DMA1", "3153220511.", "1DMA1 arts graphiques opt.gravure"),
( 315021, 0, 0, 315, 315,  "1DMA1", "3153220611.", "1DMA1 arts graphiques opt.reliure dorure"),
( 315022, 0, 0, 315, 315,  "1DMA1", "3153220711.", "1DMA1 arts graphiques opt.illustration"),
( 315023, 0, 0, 315, 315,  "1DMA1", "3153220811.", "1DMA1 arts graphiques opt.typographie"),
( 315024, 0, 0, 315, 315,  "1DMA1", "3153230111.", "1DMA1 cirque"),
( 315025, 0, 0, 315, 315,  "1DMA1", "3153231311.", "1DMA1 marionnette"),
( 315026, 0, 0, 315, 315,  "1DMA1", "3153231611.", "1DMA1 régie du spectacle option lumière"),
( 315027, 0, 0, 315, 315,  "1DMA1", "3153231711.", "1DMA1 régie du spectacle option son"),

-- 316 DMA (Diplôme des Métiers d’Art) en 2 ans -- dispositif de formation 316

( 316001, 0, 1, 316, 316,  "1DMA2", "316.....21.", "DMA 2 ans, 1e année"),
( 316002, 0, 1, 316, 316,  "2DMA2", "316.....22.", "DMA 2 ans, 2e année"),
( 316003, 0, 0, 316, 316,  "1DMA2", "3161330121.", "1DMA2 cinéma d’animation"),
( 316004, 0, 0, 316, 316,  "2DMA2", "3161330122.", "2DMA2 cinéma d’animation"),
( 316005, 0, 0, 316, 316,  "1DMA2", "3162230121.", "1DMA2 art du bijou et du joyau"),
( 316006, 0, 0, 316, 316,  "2DMA2", "3162230122.", "2DMA2 art du bijou et du joyau"),
( 316007, 0, 0, 316, 316,  "1DMA2", "3162240121.", "1DMA2 textil.céramiq : céram.artisanale"),
( 316008, 0, 0, 316, 316,  "2DMA2", "3162240122.", "2DMA2 textil.céramiq : céram.artisanale"),
( 316009, 0, 0, 316, 316,  "1DMA2", "3162240221.", "1DMA2 décor architect.opt.verre cristal"),
( 316010, 0, 0, 316, 316,  "2DMA2", "3162240222.", "2DMA2 décor architect.opt.verre cristal"),
( 316011, 0, 0, 316, 316,  "1DMA2", "3162300121.", "1DMA2 habitat : décors et mobiliers"),
( 316012, 0, 0, 316, 316,  "2DMA2", "3162300122.", "2DMA2 habitat : décors et mobiliers"),
( 316013, 0, 0, 316, 316,  "1DMA2", "3162300221.", "1DMA2 habitat : ornements et objets"),
( 316014, 0, 0, 316, 316,  "2DMA2", "3162300222.", "2DMA2 habitat : ornements et objets"),
( 316015, 0, 0, 316, 316,  "1DMA2", "3162300321.", "1DMA2 décor archi_a : traitement plastique"),
( 316016, 0, 0, 316, 316,  "2DMA2", "3162300322.", "2DMA2 décor archi_a : traitement plastique"),
( 316017, 0, 0, 316, 316,  "1DMA2", "3162300421.", "1DMA2 décor archi_b : décor du mur"),
( 316018, 0, 0, 316, 316,  "2DMA2", "3162300422.", "2DMA2 décor archi_b : décor du mur"),
( 316019, 0, 0, 316, 316,  "1DMA2", "3162300521.", "1DMA2 décor archi_c : métal"),
( 316020, 0, 0, 316, 316,  "2DMA2", "3162300522.", "2DMA2 décor archi_c : métal"),
( 316021, 0, 0, 316, 316,  "1DMA2", "3162300621.", "1DMA2 décor archi_d : matériaux synthèse"),
( 316022, 0, 0, 316, 316,  "2DMA2", "3162300622.", "2DMA2 décor archi_d : matériaux synthèse"),
( 316023, 0, 0, 316, 316,  "1DMA2", "3162340921.", "1DMA2 lutherie"),
( 316024, 0, 0, 316, 316,  "2DMA2", "3162340922.", "2DMA2 lutherie"),
( 316025, 0, 0, 316, 316,  "1DMA2", "3162341021.", "1DMA2 habitat opt.restauration mobilier"),
( 316026, 0, 0, 316, 316,  "2DMA2", "3162341022.", "2DMA2 habitat opt.restauration mobilier"),
( 316027, 0, 0, 316, 316,  "1DMA2", "3162341121.", "1DMA2 facture instrumentale : accordéon"),
( 316028, 0, 0, 316, 316,  "2DMA2", "3162341122.", "2DMA2 facture instrumentale : accordéon"),
( 316029, 0, 0, 316, 316,  "1DMA2", "3162341221.", "1DMA2 facture instrumentale : guitare"),
( 316030, 0, 0, 316, 316,  "2DMA2", "3162341222.", "2DMA2 facture instrumentale : guitare"),
( 316031, 0, 0, 316, 316,  "1DMA2", "3162341321.", "1DMA2 facture instrumentale : piano"),
( 316032, 0, 0, 316, 316,  "2DMA2", "3162341322.", "2DMA2 facture instrumentale : piano"),
( 316033, 0, 0, 316, 316,  "1DMA2", "3162410121.", "1DMA2 textil.céramique : arts textiles"),
( 316034, 0, 0, 316, 316,  "2DMA2", "3162410122.", "2DMA2 textil.céramique : arts textiles"),
( 316035, 0, 0, 316, 316,  "1DMA2", "3162420421.", "1DMA2 op. costumier réalisateur"),
( 316036, 0, 0, 316, 316,  "2DMA2", "3162420422.", "2DMA2 op. costumier réalisateur"),
( 316037, 0, 0, 316, 316,  "1DMA2", "3162510121.", "1DMA2 horlogerie"),
( 316038, 0, 0, 316, 316,  "2DMA2", "3162510122.", "2DMA2 horlogerie"),
( 316039, 0, 0, 316, 316,  "1DMA2", "3162540121.", "1DMA2 facture instrumentale : instruments a vents"),
( 316040, 0, 0, 316, 316,  "2DMA2", "3162540122.", "2DMA2 facture instrumentale : instruments a vents"),
( 316041, 0, 0, 316, 316,  "1DMA2", "3163220521.", "1DMA2 arts graphiques opt.gravure"),
( 316042, 0, 0, 316, 316,  "2DMA2", "3163220522.", "2DMA2 arts graphiques opt.gravure"),
( 316043, 0, 0, 316, 316,  "1DMA2", "3163220621.", "1DMA2 arts graphiques opt.reliure dorure"),
( 316044, 0, 0, 316, 316,  "2DMA2", "3163220622.", "2DMA2 arts graphiques opt.reliure dorure"),
( 316045, 0, 0, 316, 316,  "1DMA2", "3163220721.", "1DMA2 arts graphiques opt.illustration"),
( 316046, 0, 0, 316, 316,  "2DMA2", "3163220722.", "2DMA2 arts graphiques opt.illustration"),
( 316047, 0, 0, 316, 316,  "1DMA2", "3163220821.", "1DMA2 arts graphiques opt.typographie"),
( 316048, 0, 0, 316, 316,  "2DMA2", "3163220822.", "2DMA2 arts graphiques opt.typographie"),
( 316049, 0, 0, 316, 316,  "1DMA2", "3163230121.", "1DMA2 cirque"),
( 316050, 0, 0, 316, 316,  "2DMA2", "3163230122.", "2DMA2 cirque"),
( 316051, 0, 0, 316, 316,  "1DMA2", "3163231321.", "1DMA2 marionnette"),
( 316052, 0, 0, 316, 316,  "2DMA2", "3163231322.", "2DMA2 marionnette"),
( 316053, 0, 0, 316, 316,  "1DMA2", "3163231621.", "1DMA2 régie du spectacle option lumière"),
( 316054, 0, 0, 316, 316,  "2DMA2", "3163231622.", "2DMA2 régie du spectacle option lumière"),
( 316055, 0, 0, 316, 316,  "1DMA2", "3163231721.", "1DMA2 régie du spectacle option son"),
( 316056, 0, 0, 316, 316,  "2DMA2", "3163231722.", "2DMA2 régie du spectacle option son"),

-- 317 DMA (Diplôme des Métiers d’Art) en 3 ans -- dispositif de formation 317

( 317001, 0, 0, 317, 317,  "1DMA3", "3171340131.", "1DMA3 diplôme national des métiers d’art et du design"), -- fermé 31/08/2019
( 317002, 0, 0, 317, 317,  "2DMA3", "3171340132.", "2DMA3 diplôme national des métiers d’art et du design"),
( 317003, 0, 0, 317, 317,  "3DMA3", "3171340133.", "3DMA3 diplôme national des métiers d’art et du design"),

-- 350 DUT (Diplôme Universitaire de Technologie) -- dispositif de formation 350

( 350001, 0, 1, 350, 350,  "2DUT1", "350.....21.", "DUT 2 ans, 1e année"),
( 350002, 0, 1, 350, 350,  "2DUT2", "350.....22.", "DUT 2 ans, 2e année"),
( 350003, 0, 0, 350, 350,  "1DUT2", "3501110221.", "1DUT2 sciences et génie des matériaux"),
( 350004, 0, 0, 350, 350,  "2DUT2", "3501110222.", "2DUT2 sciences et génie des matériaux"),
( 350005, 0, 0, 350, 350,  "1DUT2", "3501110321.", "1DUT2 mesures phys mat (année commune)"),
( 350006, 0, 0, 350, 350,  "1DUT2", "3501160321.", "1DUT2 chimie (année commune)"),
( 350007, 0, 0, 350, 350,  "1DUT2", "3501180521.", "1DUT2 génie bio (année commune)"),
( 350008, 0, 0, 350, 350,  "2DUT2", "3501180622.", "2DUT2 génie bio - analys bio et biochim"),
( 350009, 0, 0, 350, 350,  "2DUT2", "3501180722.", "2DUT2 génie bio - ind alim et biologiq"),
( 350010, 0, 0, 350, 350,  "2DUT2", "3501180822.", "2DUT2 génie bio - agronomie"),
( 350011, 0, 0, 350, 350,  "2DUT2", "3501180922.", "2DUT2 génie bio - génie de environnemt"),
( 350012, 0, 0, 350, 350,  "1DUT2", "3502000621.", "1DUT2 génie industriel et maintenance"),
( 350013, 0, 0, 350, 350,  "2DUT2", "3502000622.", "2DUT2 génie industriel et maintenance"),
( 350014, 0, 0, 350, 350,  "1DUT2", "3502221121.", "1DUT2 génie chimiq procédé (an. commu)"),
( 350015, 0, 0, 350, 350,  "2DUT2", "3502221222.", "2DUT2 génie chimiq proc - procédés"),
( 350016, 0, 0, 350, 350,  "2DUT2", "3502221322.", "2DUT2 génie chimiq proc- bio-procédés"),
( 350017, 0, 0, 350, 350,  "1DUT2", "3502270121.", "1DUT2 génie thermique et énergie"),
( 350018, 0, 0, 350, 350,  "2DUT2", "3502270122.", "2DUT2 génie thermique et énergie"),
( 350019, 0, 0, 350, 350,  "1DUT2", "3502300121.", "1DUT2 génie civil (année commune)"),
( 350020, 0, 0, 350, 350,  "1DUT2", "3502510121.", "1DUT2 génie mécanique et productique"),
( 350021, 0, 0, 350, 350,  "2DUT2", "3502510122.", "2DUT2 génie mécanique et productique"),
( 350022, 0, 0, 350, 350,  "1DUT2", "3502550621.", "1DUT2 génie elec info ind (an. commune)"),
( 350023, 0, 0, 350, 350,  "1DUT2", "3503100421.", "1DUT2 gestion adminis et commerciale"),
( 350024, 0, 0, 350, 350,  "2DUT2", "3503100422.", "2DUT2 gestion adminis et commerciale"),
( 350025, 0, 0, 350, 350,  "1DUT2", "3503100521.", "1DUT2 gea (année commune)"),
( 350026, 0, 0, 350, 350,  "1DUT2", "3503110221.", "1DUT2 gestion logistique transport"),
( 350027, 0, 0, 350, 350,  "2DUT2", "3503110222.", "2DUT2 gestion logistique transport"),
( 350028, 0, 0, 350, 350,  "1DUT2", "3503110321.", "1DUT2 génie conditionnement emballage"),
( 350029, 0, 0, 350, 350,  "2DUT2", "3503110322.", "2DUT2 génie conditionnement emballage"),
( 350030, 0, 0, 350, 350,  "1DUT2", "3503120121.", "1DUT2 techniques de commercialisation"),
( 350031, 0, 0, 350, 350,  "2DUT2", "3503120122.", "2DUT2 techniques de commercialisation"),
( 350032, 0, 0, 350, 350,  "1DUT2", "3503120221.", "1DUT2 techniques commercialisation - syst, solutions & serv indus"),
( 350033, 0, 0, 350, 350,  "2DUT2", "3503120222.", "2DUT2 techniques commercialisation - syst, solutions & serv indus"),
( 350034, 0, 0, 350, 350,  "1DUT2", "3503200221.", "1DUT2 services et réseaux de communication"),
( 350035, 0, 0, 350, 350,  "2DUT2", "3503200222.", "2DUT2 services et réseaux de communication"),
( 350036, 0, 0, 350, 350,  "1DUT2", "3503210121.", "1DUT2 info com journalisme"),
( 350037, 0, 0, 350, 350,  "2DUT2", "3503210122.", "2DUT2 info com journalisme"),
( 350038, 0, 0, 350, 350,  "1DUT2", "3503210221.", "1DUT2 info com publicité"),
( 350039, 0, 0, 350, 350,  "2DUT2", "3503210222.", "2DUT2 info com publicité"),
( 350040, 0, 0, 350, 350,  "1DUT2", "3503260521.", "1DUT2 informatique (année commune)"),
( 350041, 0, 0, 350, 350,  "1DUT2", "3503300321.", "1DUT2 carr soc services à la personne"),
( 350042, 0, 0, 350, 350,  "2DUT2", "3503300322.", "2DUT2 carr soc services à la personne"),
( 350043, 0, 0, 350, 350,  "1DUT2", "3503310321.", "1DUT2 aide & assistance pour monitoring & maintien domicile"),
( 350044, 0, 0, 350, 350,  "2DUT2", "3503310322.", "2DUT2 aide & assistance pour monitoring & maintien domicile"),
( 350045, 0, 0, 350, 350,  "1DUT2", "3503320121.", "1DUT2 carr soc éducation spécialisée"),
( 350046, 0, 0, 350, 350,  "2DUT2", "3503320122.", "2DUT2 carr soc éducation spécialisée"),
( 350047, 0, 0, 350, 350,  "1DUT2", "3503320221.", "1DUT2 carr soc assistance sociale"),
( 350048, 0, 0, 350, 350,  "2DUT2", "3503320222.", "2DUT2 carr soc assistance sociale"),
( 350049, 0, 0, 350, 350,  "1DUT2", "3503320321.", "1DUT2 carr soc gestion urbaine"),
( 350050, 0, 0, 350, 350,  "2DUT2", "3503320322.", "2DUT2 carr soc gestion urbaine"),
( 350051, 0, 0, 350, 350,  "1DUT2", "3503350521.", "1DUT2 carr soc animation soc socio-cult"),
( 350052, 0, 0, 350, 350,  "2DUT2", "3503350522.", "2DUT2 carr soc animation soc socio-cult"),
( 350053, 0, 0, 350, 350,  "1DUT2", "3503440321.", "1DUT2 hygiène sécurité environnement"),
( 350054, 0, 0, 350, 350,  "2DUT2", "3503440322.", "2DUT2 hygiène sécurité environnement"),
( 350055, 0, 0, 350, 350,  "1DUT2", "3503450121.", "1DUT2 carrières juridiques"),
( 350056, 0, 0, 350, 350,  "2DUT2", "3503450122.", "2DUT2 carrières juridiques"),

-- 351 Titre professionnel Niveau III -- dispositif de formation 351

( 351001, 0, 1, 351, 351,    "TP3", "351..99999.", "Titre professionnel Niveau III"),
( 351002, 0, 0, 351, 351,    "TP3", "3511099999.", "TP3 formations générales"),
( 351003, 0, 0, 351, 351,    "TP3", "3511199999.", "TP3 mathématiques et sciences"),
( 351004, 0, 0, 351, 351,    "TP3", "3511299999.", "TP3 sciences humaines et droit"),
( 351005, 0, 0, 351, 351,    "TP3", "3511399999.", "TP3 lettres et arts"),
( 351006, 0, 0, 351, 351,    "TP3", "3512099999.", "TP3 spé.pluri-techno de production"),
( 351007, 0, 0, 351, 351,    "TP3", "3512199999.", "TP3 agriculture, pêche, forêt"),
( 351008, 0, 0, 351, 351,    "TP3", "3512299999.", "TP3 transformations"),
( 351009, 0, 0, 351, 351,    "TP3", "3512399999.", "TP3 génie civil construction et bois"),
( 351010, 0, 0, 351, 351,    "TP3", "3512499999.", "TP3 matériaux souples"),
( 351011, 0, 0, 351, 351,    "TP3", "3512599999.", "TP3 mécanique électricité électronique"),
( 351012, 0, 0, 351, 351,    "TP3", "3513099999.", "TP3 spec.pluriv. des services"),
( 351013, 0, 0, 351, 351,    "TP3", "3513199999.", "TP3 échanges et gestion"),
( 351014, 0, 0, 351, 351,    "TP3", "3513299999.", "TP3 communication et information"),
( 351015, 0, 0, 351, 351,    "TP3", "3513399999.", "TP3 services aux personnes"),
( 351016, 0, 0, 351, 351,    "TP3", "3513499999.", "TP3 services à la collectivité"),

-- 370 BTSa (Brevet de Technicien Supérieur Agricole) en 1 an -- dispositif de formation 370

( 370000, 0, 1, 370, 370, "1BTS1A", "370.....11.", "BTS Agricole 1 an"),
( 370001, 0, 0, 370, 370, "1BTS1A", "3702100711.", "1BTS1a génie des équipements agricoles"),
( 370002, 0, 0, 370, 370, "1BTS1A", "3702101111.", "1BTS1a dev agriculture régions chaudes"),
( 370003, 0, 0, 370, 370, "1BTS1A", "3702101311.", "1BTS1a technico-commercial"),
( 370004, 0, 0, 370, 370, "1BTS1A", "3702101411.", "1BTS1a analyse, cdte et stratégie de l’entreprise agricole acse"),
( 370005, 0, 0, 370, 370, "1BTS1A", "3702111111.", "1BTS1a production horticole"),
( 370006, 0, 0, 370, 370, "1BTS1A", "3702120511.", "1BTS1a productions animales"),
( 370007, 0, 0, 370, 370, "1BTS1A", "3702130411.", "1BTS1a gestion forestière"),
( 370008, 0, 0, 370, 370, "1BTS1A", "3702130811.", "1BTS1a gestion-protec nature"),
( 370009, 0, 0, 370, 370, "1BTS1A", "3702140111.", "1BTS1a aménagements paysagers"),
( 370010, 0, 0, 370, 370, "1BTS1A", "3703430211.", "1BTS1a gestion-maîtrise-eau"),

-- 371 BTSa (Brevet de Technicien Supérieur Agricole) en 2 ans -- dispositif de formation 371

( 371001, 0, 1, 371, 371, "1BTS2A", "371.....21.", "BTS Agricole 2 ans, 1e année"),
( 371002, 0, 1, 371, 371, "2BTS2A", "371.....22.", "BTS Agricole 2 ans, 2e année"),
( 371003, 0, 0, 371, 371, "1BTS2A", "3712100721.", "1BTS2a génie des équipements agricoles"),
( 371004, 0, 0, 371, 371, "2BTS2A", "3712100722.", "2BTS2a génie des équipements agricoles"),
( 371005, 0, 0, 371, 371, "1BTS2A", "3712101121.", "1BTS2a dev agriculture régions chaudes"),
( 371006, 0, 0, 371, 371, "2BTS2A", "3712101122.", "2BTS2a dev agriculture régions chaudes"),
( 371007, 0, 0, 371, 371, "1BTS2A", "3712101221.", "1BTS2a sta produits céréaliers"),
( 371008, 0, 0, 371, 371, "2BTS2A", "3712101222.", "2BTS2a sta produits céréaliers"),
( 371009, 0, 0, 371, 371, "1BTS2A", "3712101321.", "1BTS2a technico-commercial"),
( 371010, 0, 0, 371, 371, "2BTS2A", "3712101322.", "2BTS2a technico-commercial"),
( 371011, 0, 0, 371, 371, "1BTS2A", "3712101421.", "1BTS2a analyse, cdte et stratégie de l’entreprise agricole acse"),
( 371012, 0, 0, 371, 371, "2BTS2A", "3712101422.", "2BTS2a analyse, cdte et stratégie de l’entreprise agricole acse"),
( 371013, 0, 0, 371, 371, "1BTS2A", "3712111121.", "1BTS2a production horticole"),
( 371014, 0, 0, 371, 371, "2BTS2A", "3712111122.", "2BTS2a production horticole"),
( 371015, 0, 0, 371, 371, "1BTS2A", "3712111221.", "1BTS2a viticulture œnologie"),
( 371016, 0, 0, 371, 371, "2BTS2A", "3712111222.", "2BTS2a viticulture œnologie"),
( 371017, 0, 0, 371, 371, "1BTS2A", "3712111321.", "1BTS2a agronomie productions végétales"),
( 371018, 0, 0, 371, 371, "2BTS2A", "3712111322.", "2BTS2a agronomie productions végétales"),
( 371019, 0, 0, 371, 371, "1BTS2A", "3712120521.", "1BTS2a productions animales"),
( 371020, 0, 0, 371, 371, "2BTS2A", "3712120522.", "2BTS2a productions animales"),
( 371021, 0, 0, 371, 371, "1BTS2A", "3712120621.", "1BTS2a aquaculture"),
( 371022, 0, 0, 371, 371, "2BTS2A", "3712120622.", "2BTS2a aquaculture"),
( 371023, 0, 0, 371, 371, "1BTS2A", "3712130421.", "1BTS2a gestion forestière"),
( 371024, 0, 0, 371, 371, "2BTS2A", "3712130422.", "2BTS2a gestion forestière"),
( 371025, 0, 0, 371, 371, "1BTS2A", "3712130821.", "1BTS2a gestion-protec-nature"),
( 371026, 0, 0, 371, 371, "2BTS2A", "3712130822.", "2BTS2a gestion-protec-nature"),
( 371027, 0, 0, 371, 371, "1BTS2A", "3712140121.", "1BTS2a aménagements paysagers"),
( 371028, 0, 0, 371, 371, "2BTS2A", "3712140122.", "2BTS2a aménagements paysagers"),
( 371029, 0, 0, 371, 371, "1BTS2A", "3712211121.", "1BTS2a analyses agri biolog et biotechno"),
( 371030, 0, 0, 371, 371, "2BTS2A", "3712211122.", "2BTS2a analyses agri biolog et biotechno"),
( 371031, 0, 0, 371, 371, "1BTS2A", "3712211221.", "1BTS2a sta produits laitiers"),
( 371032, 0, 0, 371, 371, "2BTS2A", "3712211222.", "2BTS2a sta produits laitiers"),
( 371033, 0, 0, 371, 371, "1BTS2A", "3712211321.", "1BTS2a sta aliments et processus technologiques"),
( 371034, 0, 0, 371, 371, "2BTS2A", "3712211322.", "2BTS2a sta aliments et processus technologiques"),
( 371035, 0, 0, 371, 371, "1BTS2A", "3712211421.", "1BTS2a sta viandes et produits de la pêche"),
( 371036, 0, 0, 371, 371, "2BTS2A", "3712211422.", "2BTS2a sta viandes et produits de la pêche"),
( 371037, 0, 0, 371, 371, "1BTS2A", "3713000121.", "1BTS2a développement, animation des territoires ruraux"),
( 371038, 0, 0, 371, 371, "2BTS2A", "3713000122.", "2BTS2a développement, animation des territoires ruraux"),
( 371039, 0, 0, 371, 371, "1BTS2A", "3713430221.", "1BTS2a gestion maîtrise eau"),
( 371040, 0, 0, 371, 371, "2BTS2A", "3713430222.", "2BTS2a gestion maîtrise eau"),
( 371041, 0, 0, 371, 371, "1BTS2A", "3713430221.", "1BTS2a technico-commercial Spé Produit de la filière bois"),
( 371042, 0, 0, 371, 371, "2BTS2A", "3713430222.", "2BTS2a technico-commercial Spé Produit de la filière bois"),
( 371043, 0, 0, 371, 371, "1BTS2A", "3713430221.", "1BTS2a technico-commercial Spé Vins, bières et spiritueux"),
( 371044, 0, 0, 371, 371, "2BTS2A", "3713430222.", "2BTS2a technico-commercial Spé Vins, bières et spiritueux"),
( 371045, 0, 0, 371, 371, "1BTS2A", "3713430221.", "1BTS2a technico-commercial Spé Univers jardins et animaux de compagnie"),
( 371046, 0, 0, 371, 371, "2BTS2A", "3713430222.", "2BTS2a technico-commercial Spé Univers jardins et animaux de compagnie"),
( 371047, 0, 0, 371, 371, "1BTS2A", "3713430221.", "1BTS2a technico-commercial Spé Biens et services pour l’agriculture"),
( 371048, 0, 0, 371, 371, "2BTS2A", "3713430222.", "2BTS2a technico-commercial Spé Biens et services pour l’agriculture"),
( 371049, 0, 0, 371, 371, "1BTS2A", "3713430221.", "1BTS2a technico-commercial Spé Alimentation et boisson"),
( 371050, 0, 0, 371, 371, "2BTS2A", "3713430222.", "2BTS2a technico-commercial Spé Alimentation et boisson"),

-- 372 Certificat spécialisation agricole niveau 3 -- dispositif de formation 372

( 372001, 0, 1, 372, 372,   "CSA3", "372.....99.", "Certificat spécialisation agricole niveau 3"),
( 372002, 0, 1, 372, 372,   "CSA3", "3722100299.", "CSA3 hydraulique agricole"),
( 372003, 0, 1, 372, 372,   "CSA3", "3722100499.", "CSA3 responsable technico-commercial en agroéquipement"),
( 372004, 0, 1, 372, 372,   "CSA3", "3722100599.", "CSA3 technicien conseil système informatique appliqué"),
( 372005, 0, 1, 372, 372,   "CSA3", "3722100699.", "CSA3 vente export prod agricole agroalimentaire"),
( 372006, 0, 1, 372, 372,   "CSA3", "3722100799.", "CSA3 responsable technico-commercial en agrofournitures"),
( 372007, 0, 1, 372, 372,   "CSA3", "3722110299.", "CSA3 techniques d’agriculture biologique"),
( 372008, 0, 1, 372, 372,   "CSA3", "3722110399.", "CSA3 technicien conseil agriculture biologique"),
( 372009, 0, 1, 372, 372,   "CSA3", "3722110499.", "CSA3 responsable technico-commercial horticulture ornementale"),
( 372010, 0, 1, 372, 372,   "CSA3", "3722110599.", "CSA3 responsable technico-commercial fruits et légumes"),
( 372011, 0, 1, 372, 372,   "CSA3", "3722110699.", "CSA3 gestion des arbres d’ornement"),
( 372012, 0, 1, 372, 372,   "CSA3", "3722120499.", "CSA3 automatisation gestion informatisée élevage laitier"),
( 372013, 0, 1, 372, 372,   "CSA3", "3722120599.", "CSA3 technicien conseil production laitière"),
( 372014, 0, 1, 372, 372,   "CSA3", "3722120899.", "CSA3 technicien conseil production laitière ovine"),
( 372015, 0, 1, 372, 372,   "CSA3", "3722121199.", "CSA3 responsable technico-commercial produits carnés"),
( 372016, 0, 1, 372, 372,   "CSA3", "3722130299.", "CSA3 agri aménagement rural zones montagne"),
( 372017, 0, 1, 372, 372,   "CSA3", "3722140199.", "CSA3 collaborateur du concepteur paysagiste"),
( 372018, 0, 1, 372, 372,   "CSA3", "3722210199.", "CSA3 gestion promotion qualité production filière lait"),
( 372019, 0, 1, 372, 372,   "CSA3", "3722210499.", "CSA3 technico-commercial entr agr para agro-alimentaire"),
( 372020, 0, 1, 372, 372,   "CSA3", "3722211199.", "CSA3 responsable technico-commercial ind agro-alimentaire laitière"),
( 372021, 0, 1, 372, 372,   "CSA3", "3723120199.", "CSA3 technico-commercial vins dérivés : commerce"),
( 372022, 0, 1, 372, 372,   "CSA3", "3723120299.", "CSA3 technico-commercial vins dérivés : produit"),

-- 390 Préparations diverses post-BAC -- dispositif de formation 390~441

( 390001, 0, 0, 390, 390,   "1PD3", "3902009911.", "1PD3 techno.industriell.fondamentales"),
( 390002, 0, 0, 390, 390,   "1PD3", "3902109911.", "1PD3 spec.pluriv.de l’agronomie agric."),
( 390003, 0, 0, 390, 390,   "1PD3", "3902209911.", "1PD3 spec.pluritechno des transformat."),
( 390004, 0, 0, 390, 390,   "1PD3", "3902309911.", "1PD3 spec.pluritechno, génie civil, .."),
( 390005, 0, 0, 390, 390,   "1PD3", "3902409911.", "1PD3 spec.pluritechno matériaux soupl."),
( 390006, 0, 0, 390, 390,   "1PD3", "3902509911.", "1PD3 spec.pluritechno mecan.-électric."),
( 390007, 0, 0, 390, 390,   "1PD3", "3903009911.", "1PD3 spec.plurivalentes des services"),
( 390008, 0, 0, 390, 390,   "1PD3", "3903109911.", "1PD3 spec.plurival.echanges & gestion"),
( 390009, 0, 0, 390, 390,   "1PD3", "3903209911.", "1PD3 spec.plurival.de la communication"),
( 390010, 0, 0, 390, 390,   "1PD3", "3903309911.", "1PD3 spec.pluriv.sanitaires & sociales"),
( 390011, 0, 0, 390, 390,   "PMED", "3903310111.", "Pmed prépa écoles paramédicales"),
( 390012, 0, 0, 390, 390, "PUERIC", "3903310511.", "Diplôme de puériculture"),
( 390013, 0, 0, 390, 390,   "PSOC", "3903320411.", "Psoc prépa écoles carrières sociales"),
( 390014, 0, 0, 390, 390,   "1PD3", "3903340111.", "1PD3 hôtesse d’accueil"),
( 390015, 0, 0, 390, 390,   "1PD3", "3903409911.", "1PD3 spec.pluriv.services à la collect"),

( 391001, 0, 0, 390, 391, "1PD3-2", "3912220121.", "1PD3-2 biophysicien de laboratoire"),
( 391002, 0, 0, 390, 391, "2PD3-2", "3912220122.", "2PD3-2 biophysicien de laboratoire"),
( 391003, 0, 0, 390, 391, "1PD3-2", "3912230121.", "1PD3-2 physico-métallographe de laboratoire"),
( 391004, 0, 0, 390, 391, "2PD3-2", "3912230122.", "2PD3-2 physico-métallographe de laboratoire"),

( 392001, 0, 0, 390, 392, "ASSOC1", "3923320531.", "Diplôme d’état assistant social, 1ère année"), -- fermé 31/08/2018
( 392002, 0, 0, 390, 392, "ASSOC2", "3923320532.", "Diplôme d’état assistant social, 2ème année"), -- fermé 31/08/2019
( 392003, 0, 0, 390, 392, "ASSOC3", "3923320533.", "Diplôme d’état assistant social, 3ème année"), -- fermé 31/08/2020

( 412001, 0, 0, 390, 412,   "DCG1", "4123140331.", "Diplôme comptabilité et gestion, 1ère année"),
( 412002, 0, 0, 390, 412,   "DCG2", "4123140332.", "Diplôme comptabilité et gestion, 2ème année"),
( 412003, 0, 0, 390, 412,   "DCG3", "4123140333.", "Diplôme comptabilité et gestion, 3ème année"),

( 414001, 0, 0, 390, 414,  "DSCG1", "4143140121.", "Diplôme supérieur comptabilité et gestion, 1ère année"),
( 414002, 0, 0, 390, 414,  "DSCG2", "4143140122.", "Diplôme supérieur comptabilité et gestion, 2ème année"),

( 416001, 0, 0, 390, 416,  "1DSAA", "4162200121.", "Diplôme supérieur des arts appliqués : design, 1ère année"),
( 416002, 0, 0, 390, 416,  "2DSAA", "4162200122.", "Diplôme supérieur des arts appliqués : design, 2ème année"),

( 417001, 0, 0, 390, 417, "DECESF", "4173320611.", "DECESF conseiller en économie sociale familiale"),

( 419001, 0, 0, 390, 419, "1MADE1", "4191340111.", "Diplôme national des métiers d’art et du design"),

( 421001, 0, 0, 390, 421,  "CPES1", "4211000131.", "CPES1 Cycle pluridisciplinaire d’études supérieures"),
( 421002, 0, 0, 390, 421,  "CPES2", "4211000132.", "CPES2 Cycle pluridisciplinaire d’études supérieures"),
( 421003, 0, 0, 390, 421,  "CPES3", "4211000133.", "CPES3 Cycle pluridisciplinaire d’études supérieures"),

( 418001, 0, 0, 390, 418, "1MADE3", "4181340131.", "1MADE3 Diplôme national des métiers d’art et du design"),
( 418002, 0, 0, 390, 418, "2MADE3", "4181340132.", "2MADE3 Diplôme national des métiers d’art et du design"),
( 418003, 0, 0, 390, 418, "3MADE3", "4181340133.", "3MADE3 Diplôme national des métiers d’art et du design"),
( 418004, 0, 0, 390, 418, "1MADE1", "4191340111.", "1MADE1 Diplôme national des métiers d’art et du design"),

( 441001, 0, 0, 390, 441,   "1PD2", "4413310131.", "Infirmier ou infirmière niveau II, 1ère année"),
( 441002, 0, 0, 390, 441,   "2PD2", "4413310132.", "Infirmier ou infirmière niveau II, 2ème année"),
( 441003, 0, 0, 390, 441,   "3PD2", "4413310133.", "Infirmier ou infirmière niveau II, 3ème année"),
( 441004, 0, 0, 390, 441,   "1PD2", "4413320331.", "DE éducateur de jeunes enfants niveau II, 1ère année"),
( 441005, 0, 0, 390, 441,   "2PD2", "4413320332.", "DE éducateur de jeunes enfants niveau II, 2ème année"),
( 441006, 0, 0, 390, 441,   "3PD2", "4413320333.", "DE éducateur de jeunes enfants niveau II, 3ème année"),
( 441007, 0, 0, 390, 441,   "1PD2", "4413320431.", "DE éducateur technique spécialisé niveau II, 1ère année"),
( 441008, 0, 0, 390, 441,   "2PD2", "4413320432.", "DE éducateur technique spécialisé niveau II, 2ème année"),
( 441009, 0, 0, 390, 441,   "3PD2", "4413320433.", "DE éducateur technique spécialisé niveau II, 3ème année"),
( 441010, 0, 0, 390, 441,   "1PD2", "4413320231.", "DE éducateur spécialisé niveau II, 1ère année"),
( 441011, 0, 0, 390, 441,   "2PD2", "4413320232.", "DE éducateur spécialisé niveau II, 2ème année"),
( 441012, 0, 0, 390, 441,   "3PD2", "4413320233.", "DE éducateur spécialisé niveau II, 3ème année"),
( 441013, 0, 0, 390, 441,   "1PD2", "4413320531.", "DE assistant social niveau II, 1ère année"),
( 441014, 0, 0, 390, 441,   "2PD2", "4413320532.", "DE assistant social niveau II, 2ème année"),
( 441015, 0, 0, 390, 441,   "3PD2", "4413320533.", "DE assistant social niveau II, 3ème année"),
( 441016, 0, 0, 390, 441,   "1PD2", "4413310731.", "Imagerie médicale et radiologie thérapeutique, 1ère année"),
( 441017, 0, 0, 390, 441,   "2PD2", "4413310732.", "Imagerie médicale et radiologie thérapeutique, 2ème année"),
( 441018, 0, 0, 390, 441,   "3PD2", "4413310733.", "Imagerie médicale et radiologie thérapeutique, 3ème année"),

-- 415 Licence professionnelle -- dispositif de formation 415

( 415001, 0, 1, 415, 415, "LICPRO", "4159999911.", "Licence professionnelle"),
( 415002, 0, 0, 415, 351, "LICPRO", "4151099999.", "LICPRO formations générales"),
( 415003, 0, 0, 415, 351, "LICPRO", "4151199999.", "LICPRO mathématiques et sciences"),
( 415004, 0, 0, 415, 351, "LICPRO", "4151299999.", "LICPRO sciences humaines et droit"),
( 415005, 0, 0, 415, 351, "LICPRO", "4151399999.", "LICPRO lettres et arts"),
( 415006, 0, 0, 415, 351, "LICPRO", "4152099999.", "LICPRO spé.pluri-techno de production"),
( 415007, 0, 0, 415, 351, "LICPRO", "4152199999.", "LICPRO agriculture, pêche, forêt"),
( 415008, 0, 0, 415, 351, "LICPRO", "4152299999.", "LICPRO transformations"),
( 415009, 0, 0, 415, 351, "LICPRO", "4152399999.", "LICPRO génie civil construction et bois"),
( 415010, 0, 0, 415, 351, "LICPRO", "4152499999.", "LICPRO matériaux souples"),
( 415011, 0, 0, 415, 351, "LICPRO", "4152599999.", "LICPRO mécanique électricité électronique"),
( 415012, 0, 0, 415, 351, "LICPRO", "4153099999.", "LICPRO spec.pluriv. des services"),
( 415013, 0, 0, 415, 351, "LICPRO", "4153199999.", "LICPRO échanges et gestion"),
( 415014, 0, 0, 415, 351, "LICPRO", "4153299999.", "LICPRO communication et information"),
( 415015, 0, 0, 415, 351, "LICPRO", "4153399999.", "LICPRO services aux personnes"),
( 415016, 0, 0, 415, 351, "LICPRO", "4153499999.", "LICPRO services à la collectivité"),

-- 450 Licence LMD -- dispositif de formation 450

( 450001, 0, 1, 450, 450, "LICLMD", "450..99999.", "Licence LMD"),
( 450002, 0, 0, 450, 450, "LICLMD", "4501099999.", "LICLMD formations générales"),
( 450003, 0, 0, 450, 450, "LICLMD", "4501199999.", "LICLMD mathématiques et sciences"),
( 450004, 0, 0, 450, 450, "LICLMD", "4501299999.", "LICLMD sciences humaines et droit"),
( 450005, 0, 0, 450, 450, "LICLMD", "4501399999.", "LICLMD lettres et arts"),
( 450006, 0, 0, 450, 450, "LICLMD", "4502099999.", "LICLMD spé.pluri-techno de production"),
( 450007, 0, 0, 450, 450, "LICLMD", "4502199999.", "LICLMD agriculture, pêche, forêt"),
( 450008, 0, 0, 450, 450, "LICLMD", "4502299999.", "LICLMD transformations"),
( 450009, 0, 0, 450, 450, "LICLMD", "4502399999.", "LICLMD génie civil construction et bois"),
( 450010, 0, 0, 450, 450, "LICLMD", "4502499999.", "LICLMD matériaux souples"),
( 450011, 0, 0, 450, 450, "LICLMD", "4502599999.", "LICLMD mécanique électricité électronique"),
( 450012, 0, 0, 450, 450, "LICLMD", "4503099999.", "LICLMD spec.pluriv. des services"),
( 450013, 0, 0, 450, 450, "LICLMD", "4503199999.", "LICLMD échanges et gestion"),
( 450014, 0, 0, 450, 450, "LICLMD", "4503299999.", "LICLMD communication et information"),
( 450015, 0, 0, 450, 450, "LICLMD", "4503399999.", "LICLMD services aux personnes"),
( 450016, 0, 0, 450, 450, "LICLMD", "4503499999.", "LICLMD services à la collectivité"),

-- 451 Titre professionnel Niveau II -- dispositif de formation 451

( 451001, 0, 1, 451, 451,    "TP2", "451..99999.", "Titre professionnel Niveau II"),
( 451002, 0, 0, 451, 451,    "TP2", "4511099999.", "TP2 formations générales"),
( 451003, 0, 0, 451, 451,    "TP2", "4511199999.", "TP2 mathématiques et sciences"),
( 451004, 0, 0, 451, 451,    "TP2", "4511299999.", "TP2 sciences humaines et droit"),
( 451005, 0, 0, 451, 451,    "TP2", "4511399999.", "TP2 lettres et arts"),
( 451006, 0, 0, 451, 451,    "TP2", "4512099999.", "TP2 spé.pluri-techno de production"),
( 451007, 0, 0, 451, 451,    "TP2", "4512199999.", "TP2 agriculture, pêche, forêt"),
( 451008, 0, 0, 451, 451,    "TP2", "4512299999.", "TP2 transformations"),
( 451009, 0, 0, 451, 451,    "TP2", "4512399999.", "TP2 génie civil construction et bois"),
( 451010, 0, 0, 451, 451,    "TP2", "4512499999.", "TP2 matériaux souples"),
( 451011, 0, 0, 451, 451,    "TP2", "4512599999.", "TP2 mécanique électricité électronique"),
( 451012, 0, 0, 451, 451,    "TP2", "4513099999.", "TP2 spec.pluriv. des services"),
( 451013, 0, 0, 451, 451,    "TP2", "4513199999.", "TP2 échanges et gestion"),
( 451014, 0, 0, 451, 451,    "TP2", "4513299999.", "TP2 communication et information"),
( 451015, 0, 0, 451, 451,    "TP2", "4513399999.", "TP2 services aux personnes"),
( 451016, 0, 0, 451, 451,    "TP2", "4513499999.", "TP2 services à la collectivité"),

-- 550 MASTER -- dispositif de formation 550

( 550001, 0, 1, 550, 550, "MASTER", "550..99999.", "MASTER"),
( 550002, 0, 0, 550, 550, "MASTER", "5501099999.", "MASTER formations générales"),
( 550003, 0, 0, 550, 550, "MASTER", "5501199999.", "MASTER mathématiques et sciences"),
( 550004, 0, 0, 550, 550, "MASTER", "5501299999.", "MASTER sciences humaines et droit"),
( 550005, 0, 0, 550, 550, "MASTER", "5501399999.", "MASTER lettres et arts"),
( 550006, 0, 0, 550, 550, "MASTER", "5502099999.", "MASTER spé.pluri-techno de production"),
( 550007, 0, 0, 550, 550, "MASTER", "5502199999.", "MASTER agriculture, pêche, forêt"),
( 550008, 0, 0, 550, 550, "MASTER", "5502299999.", "MASTER transformations"),
( 550009, 0, 0, 550, 550, "MASTER", "5502399999.", "MASTER génie civil construction et bois"),
( 550010, 0, 0, 550, 550, "MASTER", "5502499999.", "MASTER matériaux souples"),
( 550011, 0, 0, 550, 550, "MASTER", "5502599999.", "MASTER mécanique électricité électronique"),
( 550012, 0, 0, 550, 550, "MASTER", "5503099999.", "MASTER spec.pluriv. des services"),
( 550013, 0, 0, 550, 550, "MASTER", "5503199999.", "MASTER échanges et gestion"),
( 550014, 0, 0, 550, 550, "MASTER", "5503299999.", "MASTER communication et information"),
( 550015, 0, 0, 550, 550, "MASTER", "5503399999.", "MASTER services aux personnes"),
( 550016, 0, 0, 550, 550, "MASTER", "5503499999.", "MASTER services à la collectivité"),

-- 551 Titre professionnel Niveau I -- dispositif de formation 551

( 551001, 0, 1, 551, 551,    "TP1", "551..99999.", "Titre professionnel Niveau I"),
( 551002, 0, 0, 551, 551,    "TP1", "5511099999.", "TP1 formations générales"),
( 551003, 0, 0, 551, 551,    "TP1", "5511199999.", "TP1 mathématiques et sciences"),
( 551004, 0, 0, 551, 551,    "TP1", "5511299999.", "TP1 sciences humaines et droit"),
( 551005, 0, 0, 551, 551,    "TP1", "5511399999.", "TP1 lettres et arts"),
( 551006, 0, 0, 551, 551,    "TP1", "5512099999.", "TP1 spé.pluri-techno de production"),
( 551007, 0, 0, 551, 551,    "TP1", "5512199999.", "TP1 agriculture, pêche, forêt"),
( 551008, 0, 0, 551, 551,    "TP1", "5512299999.", "TP1 transformations"),
( 551009, 0, 0, 551, 551,    "TP1", "5512399999.", "TP1 génie civil construction et bois"),
( 551010, 0, 0, 551, 551,    "TP1", "5512499999.", "TP1 matériaux souples"),
( 551011, 0, 0, 551, 551,    "TP1", "5512599999.", "TP1 mécanique électricité électronique"),
( 551012, 0, 0, 551, 551,    "TP1", "5513099999.", "TP1 spec.pluriv. des services"),
( 551013, 0, 0, 551, 551,    "TP1", "5513199999.", "TP1 échanges et gestion"),
( 551014, 0, 0, 551, 551,    "TP1", "5513299999.", "TP1 communication et information"),
( 551015, 0, 0, 551, 551,    "TP1", "5513399999.", "TP1 services aux personnes"),
( 551016, 0, 0, 551, 551,    "TP1", "5513499999.", "TP1 services à la collectivité"),

-- 552 Ingénieur -- dispositif de formation 552

( 552001, 0, 1, 552, 552,    "ING", "552..99999.", "Ingénieur"),
( 552002, 0, 0, 552, 552,    "ING", "5521099999.", "ING formations générales"),
( 552003, 0, 0, 552, 552,    "ING", "5521199999.", "ING mathématiques et sciences"),
( 552004, 0, 0, 552, 552,    "ING", "5521299999.", "ING sciences humaines et droit"),
( 552005, 0, 0, 552, 552,    "ING", "5521399999.", "ING lettres et arts"),
( 552006, 0, 0, 552, 552,    "ING", "5522099999.", "ING spé.pluri-techno de production"),
( 552007, 0, 0, 552, 552,    "ING", "5522199999.", "ING agriculture, pêche, forêt"),
( 552008, 0, 0, 552, 552,    "ING", "5522299999.", "ING transformations"),
( 552009, 0, 0, 552, 552,    "ING", "5522399999.", "ING génie civil construction et bois"),
( 552010, 0, 0, 552, 552,    "ING", "5522499999.", "ING matériaux souples"),
( 552011, 0, 0, 552, 552,    "ING", "5522599999.", "ING mécanique électricité électronique"),
( 552012, 0, 0, 552, 552,    "ING", "5523099999.", "ING spec.pluriv. des services"),
( 552013, 0, 0, 552, 552,    "ING", "5523199999.", "ING échanges et gestion"),
( 552014, 0, 0, 552, 552,    "ING", "5523299999.", "ING communication et information"),
( 552015, 0, 0, 552, 552,    "ING", "5523399999.", "ING services aux personnes"),
( 552016, 0, 0, 552, 552,    "ING", "5523499999.", "ING services à la collectivité"),

-- 740 Formation complémentaire / Décrochage scolaire -- dispositif de formation 740~742 & 753

( 740001, 0, 0, 740, 740,  "FCND5", "7409990111.", "Formation complémentaire non diplômante hors MLDS de niveau 5"),
( 740002, 0, 0, 740, 740,  "FCIL5", "7409990511.", "Formation complémentaire maritime d’initiative locale niveau 3"),

( 741001, 0, 0, 740, 741,  "FCND4", "7419990111.", "Formation complémentaire non diplômante hors MLDS de niveau 4"),
( 741002, 0, 0, 740, 741, "CL-PAS", "7412000111.", "Classe passerelle BTS production"),
( 741003, 0, 0, 740, 741, "CL-PAS", "7413000111.", "Classe passerelle BTS services"),
( 741004, 0, 0, 740, 741,  "FCIL4", "7419990211.", "Formation complémentaire maritime d’initiative locale niveau 5"),

( 742001, 0, 0, 740, 742,  "FCND3", "7429990111.", "Formation complémentaire non diplômante hors MLDS de niveau 3"),
( 742002, 0, 0, 740, 742,  "FCIL3", "7429990211.", "Formation complémentaire maritime d’initiative locale niveau 4"),

( 753001, 0, 0, 740, 753,    "APF", "7534100111.", "Accompagnement parcours de formation MLDS"),
( 753002, 0, 0, 740, 753,    "APF", "7534100211.", "Parcours aménagé de la formation initiale"),

-- 900 LMD (Diplôme d’enseignement supérieur) -- dispositif de formation 900 (non référencé)

( 900001, 0, 0, 900, 900,     "L1",            "", "Licence, 1ère année"),
( 900002, 0, 0, 900, 900,     "L2",            "", "Licence, 2ème année"),
( 900003, 0, 0, 900, 900,     "L3",            "", "Licence, 3ème année"),
( 900011, 0, 0, 900, 900,     "M1",            "", "Master, 1ère année"),
( 900012, 0, 0, 900, 900,     "M2",            "", "Master, 2ème année"),
( 900021, 0, 0, 900, 900,     "D1",            "", "Doctorat, 1ère année"),
( 900022, 0, 0, 900, 900,     "D2",            "", "Doctorat, 2ème année"),
( 900023, 0, 0, 900, 900,     "D3",            "", "Doctorat, 3ème année"),

-- Niveau (à l’origine vierge et avec un niveau_famille_id = 0 pour ne pas être visible) permettant de s’assurer que les niveaux spécifiques créés auront un id supérieur
-- ALTER TABLE sacoche_niveau auto_increment = 1000000 poserait pb en cas de restauration de base ou de niveaux spécifiques déjà existants

( 999999, 0, 1,   1,   0,    "SCO",            "", "Cycle scolaire (tous niveaux)");

ALTER TABLE sacoche_niveau ENABLE KEYS;
