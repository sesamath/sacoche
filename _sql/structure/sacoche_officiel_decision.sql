DROP TABLE IF EXISTS sacoche_officiel_decision;

CREATE TABLE sacoche_officiel_decision (
  decision_id        SMALLINT    UNSIGNED                NOT NULL AUTO_INCREMENT,
  decision_categorie VARCHAR(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "mention | engagement | orientation",
  decision_ordre     TINYINT     UNSIGNED                NOT NULL DEFAULT 1,
  decision_synthese  VARCHAR(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  decision_contenu   VARCHAR(80) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  PRIMARY KEY (decision_id),
  UNIQUE KEY (decision_synthese),
  KEY decision_categorie (decision_categorie)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE sacoche_officiel_decision DISABLE KEYS;

INSERT INTO sacoche_officiel_decision VALUES
(1, "mention"    , 1, "encouragements"    , "Encouragements du conseil de classe."),
(2, "mention"    , 2, "compliments"       , "Compliments du conseil de classe."),
(3, "mention"    , 3, "félicitations"     , "Félicitations du conseil de classe."),
(4, "orientation", 1, "passages"          , "Passage dans la classe supérieure."),
(5, "orientation", 2, "redoublements"     , "Proposition de redoublement."),
(6, "engagement" , 1, "délégué de classe" , "Délégué de classe."),
(7, "engagement" , 2, "délégué CA"        , "Délégué au conseil d’administration.");

ALTER TABLE sacoche_officiel_decision ENABLE KEYS;
