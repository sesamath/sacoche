DROP TABLE IF EXISTS sacoche_livret_jointure_modaccomp_eleve;

-- Attention : pas de valeur par défaut possible pour les champs TEXT et BLOB... sauf NULL !

CREATE TABLE sacoche_livret_jointure_modaccomp_eleve (
  livret_modaccomp_id   SMALLINT   UNSIGNED                NOT NULL AUTO_INCREMENT,
  eleve_id              MEDIUMINT  UNSIGNED                NOT NULL DEFAULT 0,
  livret_modaccomp_code VARCHAR(5) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  info_complement       TEXT       COLLATE utf8_unicode_ci NOT NULL COMMENT "Dans le cas où la modalité d’accompagnement est PPRE ou CTR.",
  PRIMARY KEY (livret_modaccomp_id),
  UNIQUE KEY modaccomp_key (eleve_id,livret_modaccomp_code),
  KEY livret_modaccomp_code (livret_modaccomp_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
