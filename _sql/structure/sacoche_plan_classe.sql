DROP TABLE IF EXISTS sacoche_plan_classe;

CREATE TABLE sacoche_plan_classe (
  plan_id          SMALLINT    UNSIGNED                NOT NULL AUTO_INCREMENT,
  prof_id          MEDIUMINT   UNSIGNED                NOT NULL DEFAULT 0,
  groupe_id        MEDIUMINT   UNSIGNED                NOT NULL DEFAULT 0,
  plan_nom         VARCHAR(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  plan_nb_rangees  TINYINT     UNSIGNED                NOT NULL DEFAULT 5,
  plan_nb_colonnes TINYINT     UNSIGNED                NOT NULL DEFAULT 6,
  PRIMARY KEY (plan_id),
  KEY prof_id (prof_id),
  KEY groupe_id (groupe_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
