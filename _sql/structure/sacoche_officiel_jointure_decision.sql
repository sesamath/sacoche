DROP TABLE IF EXISTS sacoche_officiel_jointure_decision;

CREATE TABLE sacoche_officiel_jointure_decision (
  periode_id           MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  user_id              MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  decision_mention     SMALLINT  UNSIGNED NOT NULL DEFAULT 0 COMMENT "sacoche_officiel_decision.decision_id pour sacoche_officiel_decision.decision_categorie = mention",
  decision_engagement  SMALLINT  UNSIGNED NOT NULL DEFAULT 0 COMMENT "sacoche_officiel_decision.decision_id pour sacoche_officiel_decision.decision_categorie = engagement",
  decision_orientation SMALLINT  UNSIGNED NOT NULL DEFAULT 0 COMMENT "sacoche_officiel_decision.decision_id pour sacoche_officiel_decision.decision_categorie = orientation",
  PRIMARY KEY ( user_id , periode_id ),
  KEY periode_id (periode_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
