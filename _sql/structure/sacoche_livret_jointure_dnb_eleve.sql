DROP TABLE IF EXISTS sacoche_livret_jointure_dnb_eleve;

CREATE TABLE sacoche_livret_jointure_dnb_eleve (
  eleve_id    MEDIUMINT   UNSIGNED                NOT NULL DEFAULT 0,
  dnb_epreuve VARCHAR(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  dnb_note    TINYINT     UNSIGNED                         DEFAULT NULL,
  UNIQUE KEY dnb_key (eleve_id,dnb_epreuve),
  KEY dnb_epreuve (dnb_epreuve)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
