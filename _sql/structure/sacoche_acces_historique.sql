DROP TABLE IF EXISTS sacoche_acces_historique;

-- Attention : pour un champ DATE ou DATETIME, DEFAULT NOW() ne fonctionne qu’à partir de MySQL 5.6.5
-- Attention : pour un champ DATE ou DATETIME, la configuration NO_ZERO_DATE (incluse dans le mode strict de MySQL 5.7.4 à 5.7.7), interdit les valeurs en dehors de 1000-01-01 00:00:00 à 9999-12-31 23:59:59

CREATE TABLE sacoche_acces_historique (
  user_id    MEDIUMINT   UNSIGNED                NOT NULL DEFAULT 0,
  acces_date DATETIME                                     DEFAULT NULL COMMENT "Ne vaut normalement jamais NULL.",
  acces_mode VARCHAR(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  acces_info VARCHAR(75) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  UNIQUE KEY historique_key (user_id,acces_date), -- Attention, les valeurs NULL ne sont pas prise en compte dans le test d’unicité d’une clef
  KEY acces_date (acces_date)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
