DROP TABLE IF EXISTS sacoche_jointure_plan_eleve;

CREATE TABLE sacoche_jointure_plan_eleve (
  plan_id          SMALLINT    UNSIGNED                NOT NULL AUTO_INCREMENT,
  eleve_id         MEDIUMINT   UNSIGNED                NOT NULL DEFAULT 0,
  jointure_rangee  TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  jointure_colonne TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  jointure_ordre   TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  jointure_equipe  CHAR(1)     COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Permet par la suite d’assigner une note à tous les membres de l’équipe.",
  jointure_role    VARCHAR(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT "" COMMENT "Rôle éventuel dans l’équipe (info pour le prof uniquement).",
  PRIMARY KEY ( plan_id , eleve_id ),
  KEY eleve_id (eleve_id),
  KEY ordre (jointure_ordre)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
