DROP TABLE IF EXISTS sacoche_livret_jointure_devoirsfaits_eleve;

CREATE TABLE sacoche_livret_jointure_devoirsfaits_eleve (
  livret_devoirsfaits_id SMALLINT  UNSIGNED NOT NULL AUTO_INCREMENT,
  eleve_id               MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (livret_devoirsfaits_id),
  UNIQUE KEY eleve_id (eleve_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
