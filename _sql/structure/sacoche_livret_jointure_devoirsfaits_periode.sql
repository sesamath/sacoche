DROP TABLE IF EXISTS sacoche_livret_jointure_devoirsfaits_periode;

CREATE TABLE sacoche_livret_jointure_devoirsfaits_periode (
  livret_devoirsfaits_id SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  periode_livret         TINYINT  UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (livret_devoirsfaits_id,periode_livret),
  KEY periode_livret (periode_livret)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
