DROP TABLE IF EXISTS sacoche_livret_jointure_langcultregion_eleve;

CREATE TABLE sacoche_livret_jointure_langcultregion_eleve (
  livret_langcultregion_code VARCHAR(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  eleve_id                   MEDIUMINT  UNSIGNED                NOT NULL DEFAULT 0,
  PRIMARY KEY ( eleve_id ),
  KEY livret_langcultregion_code (livret_langcultregion_code)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
