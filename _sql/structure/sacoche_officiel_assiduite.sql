DROP TABLE IF EXISTS sacoche_officiel_assiduite;

CREATE TABLE sacoche_officiel_assiduite (
  periode_id           MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  user_id              MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  assiduite_absence    SMALLINT  UNSIGNED          DEFAULT NULL COMMENT "nombre total d’absences",
  assiduite_absence_nj SMALLINT  UNSIGNED          DEFAULT NULL COMMENT "nombre d’absences non justifiées",
  assiduite_retard     SMALLINT  UNSIGNED          DEFAULT NULL COMMENT "nombre total de retards",
  assiduite_retard_nj  SMALLINT  UNSIGNED          DEFAULT NULL COMMENT "nombre de retards non justifiés",
  PRIMARY KEY ( user_id , periode_id ),
  KEY periode_id (periode_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
