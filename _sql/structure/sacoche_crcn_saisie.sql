DROP TABLE IF EXISTS sacoche_crcn_saisie;

-- Attention : pour un champ DATE ou DATETIME, DEFAULT NOW() ne fonctionne qu’à partir de MySQL 5.6.5
-- Attention : pour un champ DATE ou DATETIME, la configuration NO_ZERO_DATE (incluse dans le mode strict de MySQL 5.7.4 à 5.7.7), interdit les valeurs en dehors de 1000-01-01 00:00:00 à 9999-12-31 23:59:59

CREATE TABLE sacoche_crcn_saisie (
  eleve_id           MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  crcn_competence_id TINYINT   UNSIGNED NOT NULL DEFAULT 0,
  crcn_niveau_numero TINYINT   UNSIGNED          DEFAULT NULL COMMENT "Vaut NULL si mémorisation d’une valeur supprimée.",
  prof_id            MEDIUMINT UNSIGNED NOT NULL DEFAULT 0,
  crcn_saisie_date   DATE                        DEFAULT NULL COMMENT "Ne vaut normalement jamais NULL.",
  PRIMARY KEY ( eleve_id , crcn_competence_id ),
  KEY eleve_id (eleve_id),
  KEY prof_id (prof_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT="Uniquement pour les saisies des niveaux de maîtrise (les commentaires sont dans sacoche_livret_saisie).";
