DROP TABLE IF EXISTS sacoche_livret_jointure_modaccomp_periode;

CREATE TABLE sacoche_livret_jointure_modaccomp_periode (
  livret_modaccomp_id SMALLINT UNSIGNED NOT NULL DEFAULT 0,
  periode_livret      TINYINT  UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (livret_modaccomp_id,periode_livret),
  KEY periode_livret (periode_livret)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
