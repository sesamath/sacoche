DROP TABLE IF EXISTS sacoche_catalogue_categorie;

CREATE TABLE sacoche_catalogue_categorie (
  categorie_id    SMALLINT    UNSIGNED                NOT NULL AUTO_INCREMENT,
  user_id         MEDIUMINT   UNSIGNED                NOT NULL DEFAULT 0,
  categorie_ordre TINYINT     UNSIGNED                NOT NULL DEFAULT 0,
  categorie_titre VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT "",
  PRIMARY KEY (categorie_id),
  KEY user_id (user_id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
