<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Reprise du code evaluation_voir.ajax.php

if(!defined('SACoche')) {Json::api_end( 400 /* Bad Request */ , 'L’adresse de l’API est '.URL_DIR_SACOCHE.'api.php' );}

// Paramètres éventuels et vérifications
$eleve_id   = $_SESSION['USER_ID'];
$prof_id    = 0;
$all_dates  = 0;
$date_debut = !empty($_POST['date_debut']) ? Clean::post('date_debut', 'date_fr') : To::jour_debut_annee_scolaire('fr') ;
$date_fin   = !empty($_POST['date_fin'])   ? Clean::post('date_fin'  , 'date_fr') : To::jour_fin_annee_scolaire('fr') ;
if( !$date_debut || !$date_fin )
{
  Json::api_end( 400 /* Bad Request */ , 'Paramètre(s) de date invalide.' );
}

// Formater les dates
$date_debut_sql = To::date_french_to_sql($date_debut);
$date_fin_sql   = To::date_french_to_sql($date_fin);
// Vérifier que la date de début est antérieure à la date de fin
if($date_debut_sql>$date_fin_sql)
{
  Json::api_end( 400 /* Bad Request */ , 'La date de début est postérieure à la date de fin.' );
}

// Lister les évaluations
$DB_TAB = DB_STRUCTURE_ELEVE::DB_lister_devoirs_eleve( $eleve_id , $prof_id , $date_debut_sql , $date_fin_sql , $all_dates , $_SESSION['USER_PROFIL_TYPE'] );
if(empty($DB_TAB))
{
  Json::api_end( 400 /* Bad Request */ , 'Aucune évaluation trouvée sur la période '.$date_debut.' ~ '.$date_fin.'.' );
}
// Récupérer le nb de saisies déjà effectuées par évaluation (ça posait trop de problème dans la requête précédente : saisies comptées plusieurs fois, évaluations sans saisies non retournées...)
$tab_devoir_id = array();
foreach($DB_TAB as $DB_ROW)
{
  $tab_devoir_id[$DB_ROW['devoir_id']] = $DB_ROW['devoir_id'];
}
$tab_nb_saisies_effectuees = array_fill_keys($tab_devoir_id,0);
$DB_TAB2 = DB_STRUCTURE_ELEVE::DB_lister_nb_saisies_par_evaluation( $eleve_id , implode(',',$tab_devoir_id) , $_SESSION['USER_PROFIL_TYPE'] );
foreach($DB_TAB2 as $DB_ROW)
{
  $tab_nb_saisies_effectuees[$DB_ROW['devoir_id']] = $DB_ROW['saisies_nombre'];
}
// On alimente la réponse retournée.
foreach($DB_TAB as $DB_ROW)
{
  $nb_saisies_possibles = $DB_ROW['items_nombre'];
  $date_affich = To::date_sql_to_french($DB_ROW['devoir_date']);
  $remplissage_info = ($tab_nb_saisies_effectuees[$DB_ROW['devoir_id']]>=$nb_saisies_possibles) ? 'oui' : ( ($tab_nb_saisies_effectuees[$DB_ROW['devoir_id']]) ? 'partiel' : 'non' ) ;
  $doc_sujet   = ($DB_ROW['jointure_doc_sujet'])   ? $DB_ROW['jointure_doc_sujet']   : ( ($DB_ROW['devoir_doc_sujet'])   ? $DB_ROW['devoir_doc_sujet']   : '' ) ;
  $doc_corrige = ($DB_ROW['jointure_doc_corrige']) ? $DB_ROW['jointure_doc_corrige'] : ( ($DB_ROW['devoir_doc_corrige']) ? $DB_ROW['devoir_doc_corrige'] : '' ) ;
  // Afficher une ligne du tableau
  Json::add_row( NULL , array(
    'id'          => $DB_ROW['devoir_id'],
    'date'        => $date_affich,
    'professeur'  => To::texte_genre_identite($DB_ROW['prof_nom'],FALSE,$DB_ROW['prof_prenom'],TRUE,$DB_ROW['prof_genre']),
    'description' => $DB_ROW['devoir_info'],
    'rempli'      => $remplissage_info,
    'enonce'      => $doc_sujet,
    'corrige'     => $doc_corrige,
  ) );
}
$nb_evals = count($DB_TAB);
$s = ($nb_evals>1) ? 's' : '' ;

Json::api_end( 200 /* OK */ , $nb_evals.' évaluation'.$s.' trouvée'.$s.'.' );

?>
