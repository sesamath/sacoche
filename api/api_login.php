<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {Json::api_end( 400 /* Bad Request */ , 'L’adresse de l’API est '.URL_DIR_SACOCHE.'api.php' );}

if($_SESSION['USER_PROFIL_TYPE']!='public')
{
  Json::api_end( 100 /* Continue */ , 'Connexion déjà établie ; se déconnecter ou appeler le service souhaité.' );
}

// Vérification du jeton de la forme id_base + 'i' + token
list($BASE,$token) = explode('i',$JETON) + array_fill(0,2,NULL); // Evite des NOTICE en initialisant les valeurs manquantes
if( is_null($BASE) || is_null($token) )
{
  Json::api_end( 400 /* Bad Request */ , 'Jeton de connexion invalide.' );
}
if( ( (HEBERGEUR_INSTALLATION=='multi-structures') && !$BASE ) || ( (HEBERGEUR_INSTALLATION=='mono-structure') && $BASE ) )
{
  Json::api_end( 400 /* Bad Request */ , 'Jeton de connexion incohérent.' );
}

// On tente une connexion
list( $auth_SUCCESS , $auth_DATA ) = SessionUser::tester_authentification_utilisateur( $BASE , $JETON /*login*/ , FALSE /*password*/ , 'api' /*mode_connection*/ );
if($auth_SUCCESS===FALSE)
{
  Json::api_end( 400 /* Bad Request */ , $auth_DATA );
}
else
{
  SessionUser::initialiser_utilisateur( $BASE , $auth_DATA );
  $tab_data = array(
    'nom'       => $_SESSION['USER_NOM'],
    'prenom'    => $_SESSION['USER_PRENOM'],
    'profil'    => $_SESSION['USER_PROFIL_NOM_COURT'],
    'structure' => $_SESSION['ETABLISSEMENT']['DENOMINATION'],
  );
  Json::api_end( 200 /* OK */ , 'Connexion réussie.' , $tab_data );
}
?>
