<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Reprise du code evaluation_voir.ajax.php

if(!defined('SACoche')) {Json::api_end( 400 /* Bad Request */ , 'L’adresse de l’API est '.URL_DIR_SACOCHE.'api.php' );}

// Paramètres éventuels et vérifications
$eleve_id  = $_SESSION['USER_ID'];
$devoir_id = Clean::post('devoir_id', 'entier');
if(!$devoir_id)
{
  Json::api_end( 400 /* Bad Request */ , 'Identifiant de devoir invalide ou manquant.' );
}

// Infos sur le devoir
$DB_ROW = DB_STRUCTURE_ELEVE::DB_recuperer_devoir_infos( $devoir_id );
if(empty($DB_ROW))
{
  Json::api_end( 400 /* Bad Request */ , 'Devoir inexistant.' );
}
$devoir_info = 'Devoir du '.To::date_sql_to_french($DB_ROW['devoir_date']).' par '.To::texte_genre_identite($DB_ROW['user_nom'],FALSE,$DB_ROW['user_prenom'],TRUE,$DB_ROW['user_genre']).' [ '.$DB_ROW['devoir_info'].' ]';

// liste des items
$DB_TAB_COMP = DB_STRUCTURE_ELEVE::DB_lister_items_devoir_avec_infos_pour_eleves( $devoir_id , $_SESSION['USER_PROFIL_TYPE'] );
// Normalement, un devoir est toujours lié à au moins un item... sauf si l’item a été supprimé dans le référentiel !
if(empty($DB_TAB_COMP))
{
  Json::api_end( 400 /* Bad Request */ , 'Devoir associé à aucun item.' );
}
$tab_liste_item = array_keys($DB_TAB_COMP);
$liste_item_id = implode(',',$tab_liste_item);

// préparer les lignes
$tab_data = array(
  'item' => array(),
  'commentaire_ecrit' => NULL,
  'commentaire_audio' => NULL,
);
foreach($tab_liste_item as $item_id)
{
  $DB_ROW = $DB_TAB_COMP[$item_id][0];
  $item_reference   = ($DB_ROW['ref_perso']) ? $DB_ROW['matiere_ref'].'.'.$DB_ROW['ref_perso'] : $DB_ROW['matiere_ref'].'.'.$DB_ROW['ref_auto'] ;
  $item_information = ($DB_ROW['item_comm']) ? $DB_ROW['item_comm'] : '' ;
  $item_lien        = ($DB_ROW['item_lien']) ? $DB_ROW['item_lien'] : '' ;
  $tab_data['item'][$item_id] = array(
    'note'        => NULL,
    'reference'   => $item_reference,
    'nom'         => $DB_ROW['item_nom'],
    'information' => $item_information,
    'lien'        => $item_lien,
  );
}

// récupérer les saisies et les ajouter
$DB_TAB = DB_STRUCTURE_ELEVE::DB_lister_saisies_devoir_eleve( $devoir_id , $eleve_id , $_SESSION['USER_PROFIL_TYPE'] , FALSE /*with_marqueurs*/ );
foreach($DB_TAB as $DB_ROW)
{
  $tab_data['item'][$DB_ROW['item_id']]['note'] = $DB_ROW['saisie_note'];
  if(isset(API::$tab_substitution_notes_speciales[$DB_ROW['saisie_note']]))
  { // AB DI NE NF NN NR
    API::$tab_legende_notes_speciales_nombre[$DB_ROW['saisie_note']]++;
  }
}

// Les commentaires texte ou audio
$DB_ROW = DB_STRUCTURE_DEVOIR_JOINTURE::DB_recuperer_commentaires($devoir_id,$eleve_id);
if(!empty($DB_ROW))
{
  if($DB_ROW['jointure_texte'])
  {
    $msg_url = $DB_ROW['jointure_texte'];
    if(strpos($msg_url,URL_DIR_SACOCHE)===0)
    {
      $fichier_chemin = url_to_chemin($msg_url);
      $tab_data['commentaire_ecrit'] = is_file($fichier_chemin) ? file_get_contents($fichier_chemin) : 'Erreur : fichier avec le contenu du commentaire non trouvé.' ;
    }
    else
    {
      $tab_data['commentaire_ecrit'] = cURL::get_contents($msg_url);
    }
  }
  if($DB_ROW['jointure_audio'])
  {
    $tab_data['commentaire_audio'] = $DB_ROW['jointure_audio'];
  }
}

// la légende, qui peut être personnalisée (codes AB, NN, etc.)
$tab_data['legende'] = API::legende( array( 'codes_notation'=>TRUE ) );

// Retour
Json::api_end( 200 /* OK */ , $devoir_info , $tab_data );

?>
