<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend PDF

// Ces méthodes ne concernent que la mise en page d’un plan de classe

class PDF_plan_classe extends PDF
{

  private $titre_hauteur = 0;
  private $is_nom    = TRUE;
  private $is_prenom = TRUE;
  private $is_photo  = TRUE;
  private $is_ordre  = FALSE;
  private $is_equipe = FALSE;
  private $is_role   = FALSE;

  public function initialiser( $plan_nom , $nb_rangees , $nb_colonnes , $is_nom , $is_prenom , $is_photo , $is_ordre , $is_equipe , $is_role )
  {
    $this->taille_police = 10;
    $this->SetMargins($this->marge_gauche , $this->marge_haut , $this->marge_droite);
    $this->AddPage($this->orientation , $this->page_size);
    $this->SetAutoPageBreak(TRUE);
    // Valeurs à retenir
     $this->is_nom    = $is_nom;
     $this->is_prenom = $is_prenom;
     $this->is_photo  = $is_photo;
     $this->is_ordre  = $is_ordre;
     $this->is_equipe = $is_equipe;
     $this->is_role   = $is_role;
   // Titre
    $this->SetFont(FONT_FAMILY , 'B' , $this->taille_police*1.2);
    $this->CellFit( $this->page_largeur_moins_marges , 7 , To::pdf($plan_nom)  , 0 /*bordure*/ , 1 /*br*/ , 'C' /*alignement*/ , FALSE /*remplissage*/ );
    $this->titre_hauteur = $this->GetY() - $this->marge_haut;
    // Calcul des dimensions
    $this->cases_largeur = $this->page_largeur_moins_marges / $nb_colonnes;
    $this->cases_hauteur = ( $this->page_hauteur_moins_marges - $this->titre_hauteur ) / $nb_rangees;
    // Prêt pour la suite
    $this->SetFont(FONT_FAMILY , '' , $this->taille_police);
  }

  public function place_eleve( $jointure_rangee , $jointure_colonne , $tab)
  {
    $pos_x = $this->marge_gauche + ( $jointure_colonne * $this->cases_largeur );
    $pos_y = $this->marge_haut   + ( $jointure_rangee  * $this->cases_hauteur ) + $this->titre_hauteur;
    $this->Rect( $pos_x , $pos_y , $this->cases_largeur , $this->cases_hauteur , 'D' /* DrawFill */ );
    if(is_null($tab))
    {
      return;
    }
    $this->SetXY( $pos_x , $pos_y );
    // On récupère les infos
    extract($tab); // $nom $prenom $ordre $equipe $role $img_width $img_height $img_src
    if($this->is_nom)
    {
      $this->CellFit( $this->cases_largeur , 4 , To::pdf($nom) , 0 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , FALSE /*remplissage*/ );
    }
    if($this->is_prenom)
    {
      $this->CellFit( $this->cases_largeur , 4 , To::pdf($prenom) , 0 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , FALSE /*remplissage*/ );
    }
    if( $this->is_equipe && $equipe )
    {
      $this->choisir_couleur_fond('e'.$equipe);
      $txt_equipe = ( $this->is_role && $role ) ? $equipe.' : '.$role : 'Équipe '.$equipe ;
      $this->CellFit( $this->cases_largeur , 4 , To::pdf($txt_equipe) , 0 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , TRUE /*remplissage*/ );
      $this->choisir_couleur_fond('blanc');
    }
    if( $this->is_ordre && $ordre )
    {
      $this->CellFit( $this->cases_largeur , 4 , To::pdf('#'.$ordre) , 0 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , FALSE /*remplissage*/ );
    }
    if($this->is_photo)
    {
      $img_width  *= $this->coef_conv_pixel_to_mm;
      $img_height *= $this->coef_conv_pixel_to_mm;
      $this->SetXY( $pos_x + $this->cases_largeur - $img_width - 1 , $pos_y + $this->cases_hauteur - $img_height - 1 );
      // image
      if($img_src)
      {
        $this->MemImage(base64_decode($img_src),$this->GetX(),$this->GetY(),$img_width,$img_height,'JPEG');
      }
      else
      {
        $this->Image('./_img/trombinoscope_vide.png',$this->GetX(),$this->GetY(),$img_width,$img_height,'PNG');
      }
    }
  }

}
?>