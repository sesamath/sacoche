<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

class HtmlMail
{

  /**
   * Pour empêcher les robots d’aspirer les adresses de courriel des liens mailto.
   * 
   * Au début, je convertissais tous les caractères en Unicode.
   * C’était une méthode reconnue comme efficace, tout en étant simple et compatible avec les règles d’accessibilité.
   * 
   * Mais maintenant l’usage de javascript devient quasiment incoutournable pour une parade efficace.
   * D'où le code ci-dessous, couplé à du javascript ensuite.
   * 
   * Il s’agit d’utiliser une transformation ROT13 comme souvent conseillé par la littérature à ce sujet,
   * couplé avec un peu d’Unicode et un préfixe aléatoire histoire de brouiller encore davantage.
   * 
   * Désormais, au chargement la page contient par exemple le html
   * <span id="meltoo1"></span>
   * et le javascript
   * meltoo[1]=["qyu3762apbagnpg-fnpbpur%40frfnzngu%2Rarg?fhowrpg=FNPbpur%20-%20Égnoyvffrzragf%20hgvyvfngrhef%20ibvfvaf&obql=Crafrm%20à%20vaqvdhre%20dhv%20ibhf%20êgrf%20rg%20bù%20ibhf%20êgrf%2R","nous contacter",!1];
   * 
   * Aucun spambot ne devrait y trouver son bonheur.
   * 
   * Javascript se charge d’une première conversion du html en
   * <a href="" class="lien_mail" data-numero="1">nous contacter</a>
   * pour avoir un lien cliquable visuellement normal.
   * 
   * Et il faut cliquer sur le lien pour enfin avoir le href complété :
   * <a href="mailto:contact-sacoche%40sesamath%2Enet?subject=SACoche%20-%20Établissements%20utilisateurs%20voisins&amp;body=Pensez%20à%20indiquer%20qui%20vous%20êtes%20et%20où%20vous%20êtes%2E" class="lien_mail" data-numero="1">nous contacter</a>
   * 
   * Un robot générique même bien programmé n’en viendra pas à bout sans une intervention humaine. ;-)
   */

  private static $mail_counter = 0;

  private static $tab_origine = array( '<br>'   , ' '   , '@'   , '.'   );
  private static $tab_unicode = array( '%0D%0A' , '%20' , '%40' , '%2E' );

  // //////////////////////////////////////////////////
  // Méthodes privées (internes)
  // //////////////////////////////////////////////////

  /**
   * Conversion unicode de quelques caractères voyants ou spéciaux.
   * ? & = ne peuvent y être ajoutés (ou alors il faut les remettre en javascript)
   *
   * @param string
   * @return string
   */
  private static function to_unicode($text)
  {
    return str_replace( HtmlMail::$tab_origine , HtmlMail::$tab_unicode , $text );
  }

  /**
   * Méthode d’encodage avec la méthode rot13.
   * On insère 8 caractères aléatoires devant.
   *
   * @param string
   * @return string
   */
  private static function encoder($text)
  {
    return Outil::fabriquer_mdp(NULL).str_replace( '"' , '\"' , str_rot13($text) );
  }

  // //////////////////////////////////////////////////
  // Méthode publique
  // //////////////////////////////////////////////////

  /**
   * Afficher un lien mailto en masquant l’adresse de courriel pour les robots.
   * Sauf pour les appels AJAX car dans ce cas il n'y a pas récupération des valeurs javascript ncessaires.
   *
   * @param string $mail_adresse
   * @param string $mail_sujet
   * @param string $texte_lien
   * @param string $mail_contenu
   * @param string $mail_copy
   * @return string
   */
  public static function to( $mail_adresse , $mail_sujet , $texte_lien , $mail_contenu='' , $mail_copy='' )
  {
    $mailto = $mail_adresse.'?subject='.$mail_sujet;
    $mailto.= ($mail_copy)    ? '&cc='.$mail_copy      : '' ;
    $mailto.= ($mail_contenu) ? '&body='.$mail_contenu : '' ;
    // Lien présent au chargement de la page
    if( SACoche == 'index' )
    {
      $href = HtmlMail::encoder( HtmlMail::to_unicode( $mailto ) );
      if(strpos($texte_lien,'@'))
      {
        $is_texte_crypte = 'true';
        $texte_lien = HtmlMail::encoder( $texte_lien );
      }
      else
      {
        $is_texte_crypte = 'false';
      }
      if(!HtmlMail::$mail_counter)
      {
        Layout::add( 'js_inline_before' , 'window.meltoo = {};' );
      }
      HtmlMail::$mail_counter++;
      Layout::add( 'js_inline_before' , 'window.meltoo['.HtmlMail::$mail_counter.'] = ["'.$href.'","'.$texte_lien.'",'.$is_texte_crypte.'];' );
      return '<span id="meltoo'.HtmlMail::$mail_counter.'"></span>';
    }
    // Lien ajouté en ajax (dont appel au serveur communauraire)
    else
    {
      $href = HtmlMail::to_unicode( $mailto );
      return '<a href="mailto:'.$href.'" class="lien_mail"">'.html($texte_lien).'</a>';
    }
  }

}
