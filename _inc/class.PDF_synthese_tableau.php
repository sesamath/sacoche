<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend PDF

// Ces méthodes ne concernent que la mise en page d’un tableau de synthèse d’une synthèse d’items

class PDF_synthese_tableau extends PDF
{

  public function initialiser( $eleve_nb , $matiere_nb , $rubrique_nb )
  {
    $hauteur_entete = 10;
    $intitule_synthese_facteur = 8;
    $intitule_eleve_facteur    = 5;
    $this->cases_largeur     = $this->page_largeur_moins_marges / ( $eleve_nb + $intitule_synthese_facteur );
    $this->intitule_largeur  = $intitule_synthese_facteur  * $this->cases_largeur;
    $this->taille_police     = $this->cases_largeur;
    $this->taille_police     = min($this->taille_police,10); // pas plus de 10
    $this->taille_police     = max($this->taille_police,5);  // pas moins de 5
    $this->cases_hauteur     = ( $this->page_hauteur_moins_marges - $hauteur_entete ) / ( $rubrique_nb + ($matiere_nb*1.5) + $intitule_eleve_facteur + $this->legende );
    $this->etiquette_hauteur = $intitule_eleve_facteur * $this->cases_hauteur;
    $this->cases_hauteur     = min($this->cases_hauteur,10); // pas plus de 10
    $this->cases_hauteur     = max($this->cases_hauteur,3);  // pas moins de 3
    $this->SetMargins($this->marge_gauche , $this->marge_haut , $this->marge_droite);
    $this->AddPage($this->orientation , $this->page_size);
    $this->SetAutoPageBreak(TRUE);
  }

  public function entete( $titre_nom , $indicateur , $texte_periode )
  {
    $hauteur_entete = 10;
    // Intitulé + Période
    $this->SetFont(FONT_FAMILY , 'B' , 10);
    $this->SetXY($this->marge_gauche , $this->marge_haut);
    $this->Cell( $this->page_largeur_moins_marges , 4 , To::pdf($titre_nom)     , 0 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , FALSE /*fond*/ );
    $this->SetFont(FONT_FAMILY , '' , 8);
    $this->Cell( $this->page_largeur_moins_marges , 4 , To::pdf($texte_periode) , 0 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , FALSE /*fond*/ );
    // Synthèse + Indicateur
    $this->SetFont(FONT_FAMILY , 'B' , 10);
    $this->SetXY($this->marge_gauche , $this->marge_haut);
    $this->Cell( $this->page_largeur_moins_marges , 4 , To::pdf('Tableau de synthèse') , 0 /*bordure*/ , 2 /*br*/ , 'R' /*alignement*/ , FALSE /*fond*/ );
    $this->Cell( $this->page_largeur_moins_marges , 4 , To::pdf($indicateur)           , 0 /*bordure*/ , 2 /*br*/ , 'R' /*alignement*/ , FALSE /*fond*/ );
    // On se positionne sous l’en-tête
    $this->SetXY($this->marge_gauche , $this->marge_haut+$hauteur_entete);
    $this->SetFont(FONT_FAMILY , '' , $this->taille_police);
  }

  public function saut_de_page_si_besoin( $hauteur_necessaire )
  {
    $hauteur_dispo_restante = $this->page_hauteur - $this->GetY() - $this->marge_bas ;
    if( $hauteur_dispo_restante < $hauteur_necessaire )
    {
      $this->AddPage($this->orientation , $this->page_size);
    }
  }

  public function ligne_tete_cellule_debut( $contenu )
  {
    $this->SetFont(FONT_FAMILY , 'B' , 10);
    $this->CellFit( $this->intitule_largeur , $this->etiquette_hauteur , To::pdf($contenu) , 0 , 0 , 'C' , FALSE /*fond*/ , '' );
    $this->SetFont(FONT_FAMILY , '' , $this->taille_police);
    $this->choisir_couleur_fond('gris_clair');
  }

  public function ligne_tete_cellule_corps( $contenu )
  {
      $this->VertCellFit( $this->cases_largeur, $this->etiquette_hauteur, To::pdf($contenu), 1 /*border*/ , 0 /*br*/ , $this->fond );
  }

  public function ligne_tete_fin()
  {
    $this->SetXY( $this->marge_gauche , $this->GetY() + $this->etiquette_hauteur );
  }

  public function ligne_corps_cellule_debut_matiere( $contenu , $lignes_nb )
  {
    $interligne_hauteur = $this->cases_hauteur / 4;
    $this->saut_de_page_si_besoin( $this->cases_hauteur * $lignes_nb );
    $this->SetXY( $this->marge_gauche , $this->GetY() + $interligne_hauteur );
    $this->choisir_couleur_fond('gris_moyen');
    $this->SetFont(FONT_FAMILY , 'B' , $this->taille_police);
    $this->CellFit( $this->intitule_largeur , $this->cases_hauteur , To::pdf($contenu) , 1 , 0 , 'L' , $this->fond , '' );
  }

  public function ligne_corps_fin_matiere()
  {
    $interligne_hauteur = $this->cases_hauteur / 4;
    $this->SetFont(FONT_FAMILY , '' , $this->taille_police);
    $this->SetXY( $this->marge_gauche , $this->GetY() + $this->cases_hauteur + $interligne_hauteur );
  }

  public function ligne_corps_cellule_debut_rubrique( $contenu )
  {
    $this->choisir_couleur_fond('gris_clair');
    $this->CellFit( $this->intitule_largeur , $this->cases_hauteur , To::pdf($contenu) , 1 , 0 , 'L' , $this->fond , '' );
  }

  public function ligne_corps_fin_rubrique()
  {
    $this->SetXY( $this->marge_gauche , $this->GetY() + $this->cases_hauteur );
  }

  public function legende()
  {
    $this->lignes_hauteur = $this->cases_hauteur;
    $ordonnee = $this->page_hauteur - $this->marge_bas - $this->lignes_hauteur*0.75;
    $this->afficher_legende( 'score_bilan' /*type_legende*/ , $ordonnee /*ordonnée*/ );
  }

}
?>