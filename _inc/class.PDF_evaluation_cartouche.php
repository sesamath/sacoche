<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend PDF

// Ces méthodes ne concernent que la mise en page d’un cartouche

class PDF_evaluation_cartouche extends PDF
{

  public function initialiser( $detail , $longueur_ref_max , $item_nb , $cases_nb , $cart_autoeval )
  {
    $this->items_nb              = $item_nb;
    $this->colonne_bilan_largeur = ( ($cart_autoeval==1) && ($cases_nb==1) ) ? 20 : 0 ; // Cellule / colonne avec le texte "auto-éval." | "éval. prof"
    $this->item_largeur          = ( ($detail=='minimal') && ($cases_nb!=1) ) ? 15 : 0 ; // Première colonne avec les codes de notation
    $minimal_colonnes_nb         = ( ($cart_autoeval==1) && ($cases_nb!=1) ) ? $item_nb*2 : $item_nb ;
    $complet_colonne_largeur     = ( ($cart_autoeval==1) && ($cases_nb==1) ) ? $this->colonne_bilan_largeur : 10 ;
    $this->cases_largeur         = ($detail=='minimal') ? ($this->page_largeur_moins_marges - $this->item_largeur - $this->colonne_bilan_largeur) / $minimal_colonnes_nb : $complet_colonne_largeur ;
    $this->cases_hauteur         = 5 ;
    $this->lignes_hauteur        = $this->cases_hauteur ; // pour la légende
    $this->cases_nb              = $cases_nb ;
    $complet_colonnes_nb         = ($cart_autoeval==0) ? $cases_nb : $cases_nb*2 ;
    $this->reference_largeur     = ($longueur_ref_max) ? ceil($longueur_ref_max*1.7) : 0 ;
    $this->intitule_largeur      = ($detail=='minimal') ? 0 : $this->page_largeur_moins_marges - $this->reference_largeur - ($this->cases_largeur*$complet_colonnes_nb) ;
    $this->SetMargins($this->marge_gauche , $this->marge_haut , $this->marge_droite);
    $this->AddPage($this->orientation , $this->page_size);
    $this->SetAutoPageBreak(FALSE);
    $largeur = ( ($detail=='complet') || ($cases_nb==1) ) ? $this->cases_largeur : $this->item_largeur ;
    $this->calculer_dimensions_images( $largeur , $this->cases_hauteur );
  }

  public function entete( $texte_entete , $lignes_nb , $detail , $cart_autoeval )
  {
    // On prend une nouvelle page PDF si besoin
    $hauteur_requise  = ( $this->cases_hauteur * $lignes_nb ) + ( $this->legende * 1.5 );
    $hauteur_restante = $this->page_hauteur - $this->GetY() - $this->marge_bas;
    if($hauteur_requise > $hauteur_restante)
    {
      $this->AddPage($this->orientation , $this->page_size);
    }
    $this->SetFont(FONT_FAMILY , '' , 10);
    $this->choisir_couleur_fond('gris_clair');
    $tab_codes = array_fill_keys($this->SESSION['NOTE_ACTIF'],TRUE) + array('X'=>FALSE);
    $largeur_reference_texte = $this->reference_largeur + $this->intitule_largeur;
    if( ($this->cases_nb==1) && ($detail=='complet') )
    {
      $this->SetX($this->marge_gauche);
      if($cart_autoeval==0)
      {
        $this->CellFit( $this->page_largeur_moins_marges , $this->cases_hauteur , To::pdf($texte_entete) , 1 /*bordure*/ , 1 /*br*/ , 'L' /*alignement*/ , $this->fond );
      }
      else
      {
        
        $this->CellFit( $largeur_reference_texte     , $this->cases_hauteur , To::pdf($texte_entete) , 1 /*bordure*/ , 0 /*br*/ , 'L' /*alignement*/ , $this->fond );
        $this->CellFit( $this->colonne_bilan_largeur , $this->cases_hauteur , To::pdf('auto-éval.')  , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , $this->fond );
        $this->CellFit( $this->colonne_bilan_largeur , $this->cases_hauteur , To::pdf('éval. prof')  , 1 /*bordure*/ , 1 /*br*/ , 'C' /*alignement*/ , $this->fond );
      }
    }
    else if( ($this->cases_nb==1) && ($detail=='minimal') )
    {
      $this->SetX($this->marge_gauche);
      if($cart_autoeval==0)
      {
        $this->CellFit( $this->page_largeur_moins_marges , $this->cases_hauteur , To::pdf($texte_entete) , 1 /*bordure*/ , 1 /*br*/ , 'L' /*alignement*/ , $this->fond );
      }
      else
      {
        $memo_y = $this->GetY();
        $this->SetXY( $this->marge_gauche , $memo_y + $this->cases_hauteur*2 );
        $this->CellFit( $this->colonne_bilan_largeur , $this->cases_hauteur , To::pdf('auto-éval.')  , 1 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , $this->fond );
        $this->CellFit( $this->colonne_bilan_largeur , $this->cases_hauteur , To::pdf('éval. prof')  , 1 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , $this->fond );
        $this->SetXY( $this->marge_gauche + $this->colonne_bilan_largeur , $memo_y );
        $this->CellFit( $this->intitule_largeur      , $this->cases_hauteur , To::pdf($texte_entete) , 1 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , $this->fond );
      }
    }
    else if( ($this->cases_nb!=1) && ($detail=='complet') )
    {
      if($cart_autoeval==0)
      {
        $this->SetX($this->marge_gauche);
      }
      else
      {
        $this->SetX($this->marge_gauche + $largeur_reference_texte);
        $largeur = $this->cases_nb * $this->cases_largeur;
        $this->CellFit( $largeur , $this->cases_hauteur , To::pdf('auto-évaluation')  , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , $this->fond );
        $this->CellFit( $largeur , $this->cases_hauteur , To::pdf('évaluation prof')  , 1 /*bordure*/ , 1 /*br*/ , 'C' /*alignement*/ , $this->fond );
      }
      $this->CellFit( $largeur_reference_texte , $this->cases_hauteur , To::pdf($texte_entete) , 1 /*bordure*/ , 0 /*br*/ , 'L' /*alignement*/ , $this->fond );
      // pour faire 2 passages en cas d'autoévaluation
      for( $i = 1-$cart_autoeval ; $i <= 1 ; $i++ )
      {
        foreach($tab_codes as $note_code => $is_note )
        {
          if($is_note)
          {
            $this->afficher_note_lomer( $note_code , 1 /*border*/ , 0 /*br*/ );
          }
          else
          {
            $this->CellFit( $this->cases_largeur , $this->cases_hauteur , To::pdf('Autre') , 1 /*bordure*/ ,  $i /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
          }
        }
      }
    }
    else if( ($this->cases_nb!=1) && ($detail=='minimal') )
    {
      $this->SetX( $this->marge_gauche + $this->item_largeur );
      $memo_x = $this->GetX();
      $this->CellFit( $this->page_largeur_moins_marges - $this->item_largeur , $this->cases_hauteur , To::pdf($texte_entete) , 1 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , $this->fond );
      if($cart_autoeval==1)
      {
        $largeur = $this->items_nb * $this->cases_largeur;
        $this->CellFit( $largeur , $this->cases_hauteur , To::pdf('auto-évaluation') , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , $this->fond );
        $this->CellFit( $largeur , $this->cases_hauteur , To::pdf('évaluation prof') , 1 /*bordure*/ , 1 /*br*/ , 'C' /*alignement*/ , $this->fond );
        $this->SetX( $memo_x );
      }
      $memo_y = $this->GetY();
      $this->SetXY( $this->marge_gauche , $memo_y + $this->cases_hauteur );
      krsort($tab_codes,SORT_NUMERIC); // Pour avoir les notes de valeurs croissantes de bas en haut
      foreach($tab_codes as $note_code => $is_note )
      {
        if($is_note)
        {
          $this->afficher_note_lomer( $note_code , 1 /*border*/ , 2 /*br*/ );
        }
        else
        {
          $this->CellFit( $this->item_largeur , $this->cases_hauteur , To::pdf('Autre') , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
        }
      }
      $this->SetXY($memo_x , $memo_y);
    }
    $this->choisir_couleur_fond('gris_moyen');
    $this->SetFont(FONT_FAMILY , '' , 8);
  }

  public function competence_detail_minimal_colonne( $item_ref , $note , $cart_autoeval )
  {
    $memo_x = $this->GetX();
    $memo_y = $this->GetY();
    // pour faire 2 passages en cas d'autoévaluation et plusieurs cases à cacher
    for( $i = 1-$cart_autoeval*min(1,$this->cases_nb-1) ; $i <= 1 ; $i++ )
    {
      if($this->cases_largeur>30)
      {
        $this->CellFit( $this->cases_largeur , $this->cases_hauteur , To::pdf($item_ref) , 1 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
      }
      else
      {
        list($ref_matiere,$ref_suite) = explode('.',$item_ref,2);
        $this->SetFont(FONT_FAMILY , '' , 7);
        $this->CellFit( $this->cases_largeur , $this->cases_hauteur/2 , To::pdf($ref_matiere) , 0 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
        $this->CellFit( $this->cases_largeur , $this->cases_hauteur/2 , To::pdf($ref_suite)   , 0 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
        $this->SetFont(FONT_FAMILY , '' , 8);
        $this->SetY($memo_y,FALSE);
        $this->Cell( $this->cases_largeur , $this->cases_hauteur , '' , 1 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
      }
      if($this->cases_nb==1)
      {
        // Avec une case à remplir
        if($cart_autoeval==1)
        {
          $this->Cell( $this->cases_largeur , $this->cases_hauteur , '' , 1 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
        }
        $this->afficher_note_lomer( $note , 1 /*border*/ , 0 /*br*/ );
      }
      else
      {
        // Avec ( $this->SESSION['NOMBRE_CODES_NOTATION'] + 1 ) cases dont une à cocher
        $tab_codes = array_fill_keys($this->SESSION['NOTE_ACTIF'],TRUE) + array('X'=>FALSE);
        krsort($tab_codes,SORT_NUMERIC); // Pour avoir les notes de valeurs croissantes de bas en haut
        if($i==0)
        {
          foreach($tab_codes as $note_code => $is_note )
          {
            $this->Cell( $this->cases_largeur , $this->cases_hauteur , '' , 1 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
          }
          $this->SetXY( $memo_x + $this->cases_largeur * $this->items_nb , $memo_y );
        }
        else
        {
          foreach($tab_codes as $note_code => $is_note )
          {
            if($is_note)
            {
              $coche = ($note_code==$note) ? $note : '' ;
              $fill  = ($note_code==$note) ? TRUE  : FALSE ;
            }
            else
            {
              $coche = ( $note && !isset($tab_codes[$note]) ) ? 'X'  : '' ;
              $fill  = ( $note && !isset($tab_codes[$note]) ) ? TRUE : FALSE ;
            }
            $this->CellFit( $this->cases_largeur , $this->cases_hauteur , To::pdf($coche) , 1 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , $fill /*fond*/ );
          }
        }
      }
    }
    $this->SetXY( $memo_x + $this->cases_largeur , $memo_y );
  }

  public function competence_detail_complet_ligne( $item_ref , $item_intitule , $note , $cart_autoeval )
  {
    if($this->reference_largeur)
    {
      $memo_x = $this->GetX();
      $memo_y = $this->GetY();
      list($ref_matiere,$ref_suite) = explode('.',$item_ref,2);
      $this->SetFont(FONT_FAMILY , '' , 7);
      $this->CellFit( $this->reference_largeur , $this->cases_hauteur/2 , To::pdf($ref_matiere) , 0 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
      $this->CellFit( $this->reference_largeur , $this->cases_hauteur/2 , To::pdf($ref_suite)   , 0 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
      $this->SetFont(FONT_FAMILY , '' , 8);
      $this->SetXY($memo_x , $memo_y);
      $this->Cell( $this->reference_largeur , $this->cases_hauteur , ''                         , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
    }
    $this->CellFit( $this->intitule_largeur  , $this->cases_hauteur , To::pdf($item_intitule) , 1 /*bordure*/ , 0 /*br*/ , 'L' /*alignement*/ , FALSE /*fond*/ );
    if($this->cases_nb==1)
    {
      // Avec une case à remplir
      if($cart_autoeval==0)
      {
        $this->afficher_note_lomer( $note , 1 /*border*/ , 1 /*br*/ );
      }
      else
      {
        $this->Cell( $this->colonne_bilan_largeur , $this->cases_hauteur , '' , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
        $this->Cell( $this->colonne_bilan_largeur , $this->cases_hauteur , '' , 1 /*bordure*/ , 1 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
      }
      
    }
    else
    {
      // Avec ( $this->SESSION['NOMBRE_CODES_NOTATION'] + 1 ) cases dont une à cocher
      $tab_codes = array_fill_keys($this->SESSION['NOTE_ACTIF'],TRUE) + array('X'=>FALSE);
      if($cart_autoeval==1)
      {
        foreach($tab_codes as $note_code => $is_note )
        {
          $this->Cell( $this->cases_largeur , $this->cases_hauteur , '' , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
        }
      }
      foreach($tab_codes as $note_code => $is_note )
      {
        if($is_note)
        {
          $coche = ($note_code==$note) ? 'X'  : '' ;
          $fill  = ($note_code==$note) ? TRUE : FALSE ;
          $br = 0;
        }
        else
        {
          $coche = ( $note && !isset($tab_codes[$note]) ) ? $note : '' ;
          $fill  = ( $note && !isset($tab_codes[$note]) ) ? TRUE  : FALSE ;
          $br = 1;
        }
        $this->CellFit( $this->cases_largeur , $this->cases_hauteur , To::pdf($coche) , 1 /*bordure*/ , $br /*br*/ , 'C' /*alignement*/ , $fill /*fond*/ );
      }
    }
  }

  public function commentaire_interligne( $decalage_nb_lignes , $commentaire , $commentaire_nb_lignes , $lignes_vide_nb , $cart_hauteur )
  {
    if($decalage_nb_lignes)
    {
      $this->SetXY($this->marge_gauche , $this->GetY() + $decalage_nb_lignes*$this->cases_hauteur);
    }
    if($commentaire)
    {
      // cadre
      $memo_x = $this->GetX();
      $memo_y = $this->GetY();
      $this->choisir_couleur_fond('gris_clair');
      $this->Cell( $this->page_largeur_moins_marges , $commentaire_nb_lignes*$this->cases_hauteur , '' , 1 /*bordure*/ , 0 /*br*/ , 'L' /*alignement*/ , $this->fond );
      $this->SetXY($memo_x , $memo_y);
      $this->SetFont(FONT_FAMILY , '' , 9);
      $this->afficher_appreciation( $this->page_largeur_moins_marges , $commentaire_nb_lignes*$this->cases_hauteur , 8 /*taille_police*/ , 4 /*taille_interligne*/ , $commentaire );
    }
    // légende
    if($this->legende)
    {
      $this->SetXY($this->marge_gauche , $this->GetY() + 0.5*$this->cases_hauteur);
      $this->afficher_legende( 'codes_notation' /*type_legende*/ , $this->GetY() /*ordonnée*/ );
    }
    if($cart_hauteur=='page')
    {
      $this->AddPage($this->orientation , $this->page_size);
    }
    else
    {
      // positionnement, dont marge
      $this->SetXY($this->marge_gauche , $this->GetY() + (2+$lignes_vide_nb)*$this->cases_hauteur);
      // trait de séparation
      $this->choisir_couleur_trait('gris_moyen');
      $this->SetLineWidth(0.1);
      $this->SetDash(1,2); // active le pointillé
      $this->Line( 4 , $this->GetY()-$this->cases_hauteur , $this->page_largeur - 4 , $this->GetY()-$this->cases_hauteur );
      $this->SetDash(); //enlève le pointillé
      $this->choisir_couleur_trait('noir');
    }
  }

}
?>