<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend PDF

// Ces méthodes ne concernent que la mise en page d’un tableau de répartition statistique de la maîtrise du socle 2016

class PDF_socle2016_repartition extends PDF
{

  public function initialiser( $composante_nb , $degres_nb )
  {
    $hauteur_entete = 15;
    $this->intitule_largeur  = 60;
    $this->etiquette_hauteur = 10;
    $this->cases_largeur     = ( $this->page_largeur_moins_marges - $this->intitule_largeur ) / ( $degres_nb + 1 );
    $this->cases_hauteur     = ( $this->page_hauteur_moins_marges - $hauteur_entete - $this->etiquette_hauteur ) / ( $composante_nb );
    $this->cases_largeur     = min( $this->cases_largeur , 40 );
    $this->cases_hauteur     = min( $this->cases_hauteur , 15 );
    $this->taille_police     = 8;
    $this->SetMargins( $this->marge_gauche , $this->marge_haut , $this->marge_droite );
    $this->AddPage($this->orientation , $this->page_size);
    $this->SetAutoPageBreak(TRUE);
  }

  public function entete( $titre , $groupe_nom , $objet )
  {
    $hauteur_entete = 15;
    // Intitulé
    $this->SetFont(FONT_FAMILY , 'B' , 10);
    $this->SetXY($this->marge_gauche , $this->marge_haut);
    $this->Cell( $this->page_largeur_moins_marges , 5 , To::pdf($titre                  ) , 0 /*bordure*/ , 2 /*br*/ , 'L' /*alignement*/ , FALSE /*fond*/ );
    $this->Cell( $this->page_largeur_moins_marges , 5 , To::pdf($groupe_nom.' - '.$objet) , 0 /*bordure*/ , 1 /*br*/ , 'L' /*alignement*/ , FALSE /*fond*/ );
    // On se positionne sous l’en-tête
    $this->SetXY($this->marge_gauche , $this->marge_haut+$hauteur_entete);
    $this->SetFont(FONT_FAMILY , 'B' , $this->taille_police);
  }

  public function ligne_tete_cellule_debut()
  {
    $this->Cell( $this->intitule_largeur , $this->etiquette_hauteur , '' , 0 , 0 , 'L' , FALSE /*fond*/ );
    $this->choisir_couleur_fond('gris_clair');
  }

  public function ligne_tete_cellule_corps( $contenu )
  {
    $tab_contenu = explode(' ',str_replace('Très bonne','Très&nbsp;bonne',$contenu));
    // fond & contour
    $drawfill = ($this->fond) ? 'DF' : 'D' ;
    $this->Rect( $this->GetX() , $this->GetY() , $this->cases_largeur , $this->etiquette_hauteur , $drawfill );
    // contenu
    foreach($tab_contenu as $contenu_partiel)
    {
      $contenu_partiel = str_replace('Très&nbsp;bonne','Très bonne',$contenu_partiel);
      $this->CellFit( $this->cases_largeur , $this->etiquette_hauteur/2 , To::pdf($contenu_partiel) , 0 /*bordure*/ , 2 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
    }
    $this->SetXY( $this->GetX()+$this->cases_largeur , $this->GetY() - $this->etiquette_hauteur );
    // $this->CellFit( $this->cases_largeur , $this->etiquette_hauteur , To::pdf($contenu) , 1 /*border*/ , 0 /*br*/ , 'C' /*alignement*/ , $this->fond );
  }

  public function ligne_retour( $id )
  {
    // $id vaut 0 pour la première ligne
    $hauteur = ($id) ? $this->cases_hauteur : $this->etiquette_hauteur ;
    $this->SetXY( $this->marge_gauche , $this->GetY() + $hauteur );
  }

  public function ligne_corps_cellule_debut( $contenu1 , $contenu2 = NULL )
  {
    $this->choisir_couleur_fond('gris_clair');
    if(!$contenu2)
    {
      $this->SetFont(FONT_FAMILY , 'B' , $this->taille_police);
      $this->CellFit( $this->intitule_largeur , $this->cases_hauteur , To::pdf($contenu1) , 1 , 0 , 'L' , $this->fond , '' );
    }
    else
    {
      $hauteur = $this->cases_hauteur / 2 ;
      $this->SetFont(FONT_FAMILY , 'B' , $this->taille_police);
      $this->CellFit( $this->intitule_largeur , $hauteur , To::pdf($contenu1) , 0 , 2 , 'L' , $this->fond , '' );
      $this->SetFont(FONT_FAMILY , '' , $this->taille_police);
      $this->CellFit( $this->intitule_largeur , $hauteur , To::pdf($contenu2) , 0 , 2 , 'L' , $this->fond , '' );
      $this->SetY( $this->GetY() - $this->cases_hauteur );
      $this->CellFit( $this->intitule_largeur , $this->cases_hauteur , ''     , 1 , 0 , 'L' , FALSE       , '' );
    }
  }

  public function ligne_corps_cellule_corps( $pourcentage )
  {
    $coefficient = $pourcentage/100 ;
    // Tracer un rectangle coloré d’aire et d’intensité de niveau de gris proportionnels
    $teinte_gris = 255-128*$coefficient ;
    $this->SetFillColor($teinte_gris,$teinte_gris,$teinte_gris);
    $memo_X = $this->GetX();
    $memo_Y = $this->GetY();
    $rect_largeur = $this->cases_largeur * sqrt( $coefficient ) ;
    $rect_hauteur = $this->cases_hauteur * sqrt( $coefficient ) ;
    $pos_X = $memo_X + ($this->cases_largeur - $rect_largeur) / 2 ;
    $pos_Y = $memo_Y + ($this->cases_hauteur - $rect_hauteur) / 2 ;
    $this->SetXY($pos_X , $pos_Y);
    $this->Cell( $rect_largeur , $rect_hauteur , '' , 0 , 0 , 'C' , TRUE /*fond*/ , '' );
    // Écrire le %
    $this->SetXY( $memo_X , $memo_Y );
    $this->SetFont(FONT_FAMILY , '' , $this->taille_police*(1+$coefficient));
    $this->Cell( $this->cases_largeur , $this->cases_hauteur , To::pdf($pourcentage.' %') , 1 , 0 , 'C' , FALSE , '' );
  }

}
?>