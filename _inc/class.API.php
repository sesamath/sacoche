<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

class API
{

  // //////////////////////////////////////////////////
  // Tableaux prédéfinis
  // //////////////////////////////////////////////////

  // sert pour indiquer la classe css d’un état d’acquisition
  public static $tab_couleur = array( '1'=>'A1' , '2'=>'A2' , '3'=>'A3' , '4'=>'A4' , '5'=>'A5' , '6'=>'A6' );

  // sert pour indiquer la légende des notes spéciales
  private static $tab_legende_notes_speciales_texte  = array('AB'=>'Absent','DI'=>'Dispensé','NE'=>'Non évalué','NF'=>'Non fait','NN'=>'Non noté','NR'=>'Non rendu');
  public  static $tab_legende_notes_speciales_nombre = array('AB'=>0       ,'DI'=>0         ,'NE'=>0           ,'NF'=>0         ,'NN'=>0         ,'NR'=>0          );

  // sert pour l’affichage des notes
  public  static $tab_substitution_notes_speciales   = array('AB'=>'Abs.'  ,'DI'=>'Disp'    ,'NE'=>'N.E.'      ,'NF'=>'N.F.'    ,'NN'=>'N.N.'    ,'NR'=>'N.R.'     );
  private static $tab_notes_image_valide = array('1','2','3','4','5','6','PA','AB','DI','NE','NF','NN','NR');

  // A renseigner une fois au début mais pas à chaque appel
  public static $afficher_score = NULL;
  public static $afficher_degre = NULL;
  public static $couleur = 'oui';

  // //////////////////////////////////////////////////
  // Méthodes privées (internes)
  // //////////////////////////////////////////////////

  /**
   * Valeur de l’attribut "src" pour un symbole de notation commun (AB, etc.)
   * Chemin relatif à la racine ; pas besoin de rajouter '.' pour une feuille de style car contenu de session personnalisé dans le fichier PHP et non dans le fichier CSS.
   *
   * @param string $symbole_nom
   * @return string
   */
  private static function note_src_commun( $symbole_nom )
  {
    return URL_DIR_IMG.'note/commun/h/'.$symbole_nom.'.gif';
  }

  /**
   * Renvoyer le chemin d’une note (code couleur) pour une sortie HTML.
   * Le daltonisme a déjà été pris en compte pour forger $_SESSION['NOTE'][i]['FICHIER']
   *
   * @param string $note
   * @return string
   */
  private static function note_src( $note )
  {
    return isset($_SESSION['NOTE'][$note]) ? str_replace('./_img/',URL_DIR_IMG,$_SESSION['NOTE'][$note]['FICHIER']) : API::note_src_commun($note) ;
  }

  // //////////////////////////////////////////////////
  // Méthodes publiques
  // //////////////////////////////////////////////////

  /**
   * Retourner la légende pour un appel à l’API.
   *
   * "aff_prop_sans_score" pour "etat_acquisition" seulement
   * "force_nb" pour "etat_acquisition" seulement // force_nb semble inutilisé
   * "force_nb" si un item a été surligné         // force_nb semble inutilisé
   *
   * @param array $tab_legende   tableau de clefs parmi "codes_notation" ; "score_bilan" ; "etat_acquisition" ; "degre_maitrise" ; "socle_points" ; "aff_prop_sans_score" ; "force_nb"
   * @return string
   */
  public static function legende( $tab_legende )
  {
    // initialisation variables
    $tab_retour = array();
    $key_couleur = $_SESSION['USER_DALTONISME'] ? 'GRIS' : 'COULEUR' ;
    // légende codes_notation
    if(!empty($tab_legende['codes_notation']))
    {
      $tab_retour['notes'] = array();
      foreach( $_SESSION['NOTE_ACTIF'] as $note_id )
      {
        $tab_retour['notes'][$note_id] = array(
          'sigle' => $_SESSION['NOTE'][$note_id]['SIGLE'],
          'image' => API::note_src($note_id),
          'texte' => $_SESSION['NOTE'][$note_id]['LEGENDE'],
        );
      }
      foreach(API::$tab_legende_notes_speciales_nombre as $note => $nombre)
      {
        if($nombre)
        {
          $tab_retour['notes'][$note] = array(
            'sigle' => API::$tab_substitution_notes_speciales[$note],
            'image' => API::note_src($note),
            'texte' => API::$tab_legende_notes_speciales_texte[$note],
          );
        }
      }
    }
    // légende scores bilan
    if(!empty($tab_legende['score_bilan']))
    {
      if( API::$afficher_score === NULL )
      {
        // En cas de bilan officiel, doit être déterminé avant
        API::$afficher_score = Outil::test_user_droit_specifique($_SESSION['DROIT_VOIR_SCORE_BILAN']);
      }
      $tab_retour['etats'] = array();
      foreach( $_SESSION['ACQUIS'] as $acquis_id => $tab_acquis_info )
      {
        $tab_retour['etats'][$acquis_id] = array(
          'sigle'     => $tab_acquis_info['SIGLE'],
          'texte'     => $tab_acquis_info['LEGENDE'],
          'couleur'   => $tab_acquis_info[$key_couleur],
          'seuil_min' => (API::$afficher_score) ? $tab_acquis_info['SEUIL_MIN'] : NULL ,
          'seuil_max' => (API::$afficher_score) ? $tab_acquis_info['SEUIL_MAX'] : NULL ,
        );
      }
      $retour .= '</div>'.NL;
    }
    // légende etat_acquisition
    if(!empty($tab_legende['etat_acquisition']))
    {
      $tab_retour['etats'] = array();
      foreach( $_SESSION['ACQUIS'] as $acquis_id => $tab_acquis_info )
      {
        $tab_retour['etats'][$acquis_id] = array(
          'sigle'     => $tab_acquis_info['SIGLE'],
          'texte'     => $tab_acquis_info['LEGENDE'],
          'couleur'   => $tab_acquis_info[$key_couleur],
        );
      }
      if(!empty($tab_legende['aff_prop_sans_score']))
      {
        $tab_retour['etats'][0] = array(
          'sigle'     => '',
          'texte'     => $_SESSION['ETAT_ACQUISITION_NEUTRE_LEGENDE'],
          'couleur'   => '#FFFFFF',
        );
      }
    }
    // légende degrés de maîtrise du socle
    if(!empty($tab_legende['degre_maitrise']))
    {
      if( API::$afficher_degre === NULL )
      {
        // En cas de bilan officiel, doit être déterminé avant
        API::$afficher_degre = Outil::test_user_droit_specifique($_SESSION['DROIT_VOIR_SCORE_MAITRISE']);
      }
      $tab_retour['socle'] = array();
      foreach( $_SESSION['LIVRET'] as $maitrise_id => $tab_maitrise_info )
      {
        $tab_retour['socle'][$maitrise_id] = array(
          'texte'     => empty($tab_legende['socle_points']) ? $tab_maitrise_info['LEGENDE'] : $tab_maitrise_info['LEGENDE'].' ('.$tab_maitrise_info['POINTS'].' pts)',
          'couleur'   => $tab_maitrise_info[$key_couleur],
          'seuil_min' => (API::$afficher_degre) ? $tab_maitrise_info['SEUIL_MIN'] : NULL ,
          'seuil_max' => (API::$afficher_degre) ? $tab_maitrise_info['SEUIL_MAX'] : NULL ,
        );
      }
    }
    // retour
    return $tab_retour;
  }

}
?>