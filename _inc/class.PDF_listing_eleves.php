<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend PDF

// Ces méthodes ne concernent que la mise en page d’un export d’une liste d’élèves

class PDF_listing_eleves extends PDF
{

  public function initialiser( $eleve_nb , $groupe_nom , $affichage_classe_et_groupe )
  {
    $hauteur_entete          = 10;
    $hauteur_minimale        = 6.5;
    $maximum_eleves_par_page = ( $this->page_hauteur_moins_marges - $hauteur_entete ) / $hauteur_minimale;
    $nombre_pages            = ceil( $eleve_nb / $maximum_eleves_par_page );
    $nombre_eleves_par_page  = $eleve_nb / $nombre_pages;
    $this->intitule_largeur  = 70;
    $this->cases_nb          = 10;
    $this->cases_largeur     = ( $this->page_largeur_moins_marges - $this->intitule_largeur ) / $this->cases_nb;
    $this->taille_police     = $this->cases_largeur*0.8;
    $this->cases_hauteur     = ( $this->page_hauteur_moins_marges - $hauteur_entete ) / $nombre_eleves_par_page;
    $this->SetMargins($this->marge_gauche , $this->marge_haut , $this->marge_droite);
    $this->AddPage($this->orientation , $this->page_size);
    $this->SetAutoPageBreak(TRUE);
    $this->choisir_couleur_fond('gris_clair');
    // Intitulé
    $this->SetFont(FONT_FAMILY , 'B' , 10);
    $this->SetXY($this->marge_gauche , $this->marge_haut);
    $this->CellFit( $this->intitule_largeur , $hauteur_entete , To::pdf($groupe_nom)  , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , TRUE /*fond*/ );
    $this->SetFont(FONT_FAMILY , '' , $this->taille_police);
    if($affichage_classe_et_groupe)
    {
      $this->CellFit( 2*$this->cases_largeur , $hauteur_entete , To::pdf('Classe')  , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , TRUE /*fond*/ );
      $this->cases_nb -= 2;
    }
    $this->cases_vides( $hauteur_entete , TRUE );
  }

  public function cases_vides( $hauteur , $fond )
  {
    for( $num_case=1 ; $num_case<=$this->cases_nb ; $num_case++ )
    {
      $br = ($num_case<$this->cases_nb) ? 0 : 1 ;
      $this->Cell( $this->cases_largeur , $hauteur , '' , 1 /*bordure*/ , $br , 'L' /*alignement*/ , $fond );
    }
  }

  public function ligne_eleve( $user_nom , $user_prenom , $indication_classe )
  {
    $this->Cell( $this->intitule_largeur , $this->cases_hauteur , To::pdf($user_nom.' '.$user_prenom) , 1 /*bordure*/ , 0 /*br*/ , 'L' /*alignement*/ , FALSE /*fond*/ );
    if($indication_classe)
    {
      $this->CellFit( 2*$this->cases_largeur , $this->cases_hauteur , To::pdf($indication_classe)  , 1 /*bordure*/ , 0 /*br*/ , 'C' /*alignement*/ , FALSE /*fond*/ );
    }
    $this->cases_vides( $this->cases_hauteur , FALSE );
  }

}
?>