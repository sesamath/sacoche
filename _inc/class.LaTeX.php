<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

class LaTeX
{

  // ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Attributs de la classe (équivalents des "variables")
  // ////////////////////////////////////////////////////////////////////////////////////////////////////

  private $contenu = '';
  private $eol = "\r\n";

  // //////////////////////////////////////////////////
  // Méthodes publiques
  // //////////////////////////////////////////////////

  /**
  * Méthode Magique - Constructeur
  */
  public function __construct()
  {
  }

  /**
   * Échapement des caractères réservés LaTeX.
   * 
   * @param string $text
   * @return string
   */
  public static function escape( $text )
  {
    // Caractères réservés LaTeX nécessitant d’être échapés.
    $tab_bad = array(  '$' ,  '&' ,  '%' ,  '#' ,  '_' ,  '{' ,  '}' ,  '^',  '~' , '\\' );
    $tab_bon = array( '\$' , '\&' , '\%' , '\#' , '\_' , '\{' , '\}' , '\^', '\~' , '\textbackslash{}' );
    return str_replace( $tab_bad , $tab_bon , $text );
  }

  /**
   * Ajout de commandes ou de données ne nécessitant pas d’être échapées.
   * Précision éventuelle de fin de ligne.
   * 
   * @param string $text
   * @param int    $nb_eol
   * @return objet
   */
  public function add( $text , $nb_eol=0 )
  {
    // on complète avec les données
    if(is_null($text))
    {
      // Par exemple pour seulement un retour à la ligne
    }
    else
    {
      $this->contenu .= $text;
    }
    // validation d’une ligne
    if($nb_eol)
    {
      $this->contenu .= str_repeat($this->eol,$nb_eol);
    }
    // Retourne l’objet afin de permettre le chaînage de méthodes
    return $this;
  }

  /**
   * Ajout de données nécessitant d’être échapées.
   * Précision éventuelle de fin de ligne.
   * 
   * @param string $text
   * @param int    $nb_eol
   * @return objet
   */
  public function add_texte( $text , $nb_eol=0 )
  {
    $this->add( LaTeX::escape( $text ) , $nb_eol );
    return $this;
  }

  /**
   * Envoi des données LaTeX.
   * 
   * @param void
   * @return string
   */
  public function get()
  {
    return $this->contenu;
  }

}
?>