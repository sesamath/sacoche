<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 *
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 *
 * Ce fichier est une partie de SACoche.
 *
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 *
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 *
 */
 
// Extension de classe qui étend PDF

// Ces méthodes ne concernent que la mise en page le tableau d’un relevé d’items par évaluations sélectionnées

class PDF_item_tableau_eval extends PDF
{

  public function initialiser( $eleve_nb , $devoir_nb , $item_nb )
  {
    $hauteur_entete = 10;
    $colonnes_nb = 8 + 4 + $eleve_nb ; // 8 colonnes pour l’intitulé du devoir ; 4 colonnes pour la référence de l’item
    $lignes_nb   = 5 + ($devoir_nb/4) + $item_nb ; // 5 lignes pour la ligne d’entête
    $this->cases_largeur     = $this->page_largeur_moins_marges / $colonnes_nb;
    $this->cases_largeur     = min($this->cases_largeur,20); // pas plus de 20
    $this->cases_hauteur     = ( $this->page_hauteur_moins_marges - $hauteur_entete - 2 ) / ( $lignes_nb + $this->legende ); // - en-tête -2 pour une petite marge
    $this->cases_hauteur     = min($this->cases_hauteur,10); // pas plus de 10
    $this->cases_hauteur     = max($this->cases_hauteur,3);  // pas moins de 3
    $this->intitule_largeur  = $this->cases_largeur*4;
    $this->etiquette_hauteur = $this->cases_hauteur*5;
    $this->taille_police     = $this->cases_largeur*0.8;
    $this->taille_police     = min($this->taille_police,10); // pas plus de 10
    $this->taille_police     = max($this->taille_police,5);  // pas moins de 5
    $this->SetMargins($this->marge_gauche , $this->marge_haut , $this->marge_droite);
    $this->AddPage($this->orientation , $this->page_size);
    $this->SetAutoPageBreak(TRUE);
    $this->calculer_dimensions_images( $this->cases_largeur , $this->cases_hauteur );
  }

  public function entete( $titre_nom , $matiere_et_groupe )
  {
    $hauteur_entete = 10;
    $this->SetFont(FONT_FAMILY , 'B' , 10);
    $this->SetXY($this->marge_gauche , $this->marge_haut);
    $this->Cell( $this->page_largeur_moins_marges*2/3 , 5 , To::pdf($titre_nom)         , 0 /*bordure*/ , 0 /*br*/ , 'L' /*alignement*/ , FALSE /*fond*/ );
    $this->Cell( $this->page_largeur_moins_marges*1/3 , 5 , To::pdf($matiere_et_groupe) , 0 /*bordure*/ , 1 /*br*/ , 'R' /*alignement*/ , FALSE /*fond*/ );
    $this->SetXY($this->marge_gauche , $this->marge_haut+$hauteur_entete);
    $this->SetFont(FONT_FAMILY , '' , $this->taille_police);
  }

  public function saut_de_page_si_besoin( $hauteur_necessaire )
  {
    $hauteur_dispo_restante = $this->page_hauteur - $this->GetY() - $this->marge_bas ;
    if( $hauteur_dispo_restante < $hauteur_necessaire )
    {
      $this->AddPage($this->orientation , $this->page_size);
    }
  }

  public function ligne_tete( $objet , $contenu='' )
  {
    if( $objet == 'devoir' )
    {
      $this->choisir_couleur_fond('gris_moyen');
      $this->CellFit( $this->intitule_largeur*2 , $this->etiquette_hauteur , To::pdf($contenu) , 1 /*bordure*/ , 0 /*br*/ , 'C' , $this->fond , '' );
    }
    else if( $objet == 'item' )
    {
      $this->CellFit( $this->intitule_largeur , $this->etiquette_hauteur , To::pdf($contenu) , 1 /*bordure*/ , 0 /*br*/ , 'C' , $this->fond , '' );
      $this->choisir_couleur_fond('gris_clair');
    }
    else if( $objet == 'eleve' )
    {
      $this->VertCellFit( $this->cases_largeur, $this->etiquette_hauteur, To::pdf($contenu), 1 /*bordure*/ , 0 /*br*/ , $this->fond );
    }
    else if( $objet == 'fin' )
    {
      $this->SetXY($this->marge_gauche , $this->GetY() + $this->etiquette_hauteur );
    }
  }

  public function ligne_corps( $objet , $contenu='' , $nb_lignes=0 )
  {
    if( $objet == 'devoir' )
    {
      $interligne_hauteur = $this->cases_hauteur / 4;
      $devoir_hauteur = $this->cases_hauteur * $nb_lignes;
      $this->saut_de_page_si_besoin( $interligne_hauteur + $devoir_hauteur );
      $this->SetXY($this->marge_gauche , $this->GetY() + $interligne_hauteur );
      $memo_x = $this->GetX();
      $memo_y = $this->GetY();
      $this->choisir_couleur_fond('gris_clair');
      $this->Cell( $this->intitule_largeur*2 , $devoir_hauteur , '' , 1 /*bordure*/ , 0 /*br*/ , 'L' /*alignement*/ , $this->fond );
      $this->SetXY($memo_x , $memo_y);
      $this->afficher_appreciation( $this->intitule_largeur*2 , $devoir_hauteur , $this->taille_police /*taille_police*/ , $this->taille_police/2 /*taille_interligne*/ , $contenu );
      $this->SetXY($memo_x + $this->intitule_largeur*2 , $memo_y);
    }
    else if( $objet == 'item' )
    {
      $this->choisir_couleur_fond('gris_clair');
      $this->CellFit( $this->intitule_largeur , $this->cases_hauteur , To::pdf($contenu) , 1 /*bordure*/ , 0 /*br*/ , 'L' , $this->fond , '' );
    }
    else if( $objet == 'note' )
    {
      if($contenu)
      {
        $this->afficher_note_lomer( $contenu , 1 /*bordure*/ , 0 /*br*/ );
      }
      else
      {
        $this->Cell( $this->cases_largeur , $this->cases_hauteur , '' , 1 /*bordure*/ , 0 /*br*/ , 'C' , FALSE /*fond*/ , '' );
      }
    }
    else if( $objet == 'fin' )
    {
      $this->SetXY($this->marge_gauche + $this->intitule_largeur*2 , $this->GetY() + $this->cases_hauteur );
    }
  }

  public function legende()
  {
    $this->lignes_hauteur = $this->cases_hauteur;
    $ordonnee = $this->page_hauteur - $this->marge_bas - $this->lignes_hauteur*0.75;
    $this->afficher_legende( 'codes_notation' /*type_legende*/ , $ordonnee /*ordonnée*/ );
  }

}
?>