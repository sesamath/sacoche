<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

// Niveaux de cycle ou du CECRL à récupérer en complément de la famille de niveaux voulue

function sql_niveau_generique($niveau_famille_id)
{
  $tab_sql_niveau_generique = array(
     60 => 'niveau_id IN(1,2,3,201)',
    100 => 'niveau_id IN(3,4,10,202,203)',
    160 => 'niveau_id IN(16,202,203)',
    200 => 'niveau_id IN(20,204,205,206)',
    210 => 'niveau_id IN(20,204,205,206)',
    220 => 'niveau_id = 23',
    240 => 'niveau_id = 24',
    241 => 'niveau_id = 24',
    242 => 'niveau_id = 24',
    243 => 'niveau_id = 25',
    247 => 'niveau_id = 26',
    250 => 'niveau_id = 27',
    251 => 'niveau_id = 27',
    254 => 'niveau_id = 28',
    271 => 'niveau_id = 29',
    275 => 'niveau_id = 36',
    276 => 'niveau_id = 30',
    277 => 'niveau_id = 36',
    278 => 'niveau_id = 34',
    279 => 'niveau_id = 34',
    301 => 'niveau_id = 31',
    310 => 'niveau_id = 32',
    311 => 'niveau_id = 32',
    312 => 'niveau_id = 32',
    315 => 'niveau_id = 33',
    316 => 'niveau_id = 33',
    350 => 'niveau_id = 35',
    370 => 'niveau_id = 37',
    371 => 'niveau_id = 37',
  );
  return isset($tab_sql_niveau_generique[$niveau_famille_id]) ? 'OR '.$tab_sql_niveau_generique[$niveau_famille_id].' ' : '' ;
}
?>