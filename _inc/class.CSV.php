<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

class CSV
{

  // ////////////////////////////////////////////////////////////////////////////////////////////////////
  // Attributs de la classe (équivalents des "variables")
  // ////////////////////////////////////////////////////////////////////////////////////////////////////

  private $ligne = array();
  private $contenu = '';
  private $separateur = '';
  private $eol = "\r\n";

  // //////////////////////////////////////////////////
  // Méthodes publiques
  // //////////////////////////////////////////////////

  /**
  * Méthode Magique - Constructeur
  */
  public function __construct( $separateur=';' )
  {
    $this->separateur = $separateur;
  }

  /**
   * Ajout de données (chaine ou tableau).
   * Précision éventuelle de fin de ligne.
   * 
   * @param array|string $datas
   * @param int          $nb_eol
   * @return objet
   */
  public function add( $datas , $nb_eol=0 )
  {
    // on complète la ligne avec les données
    if(is_array($datas))
    {
      $this->ligne = array_merge( $this->ligne , $datas );
    }
    else if(is_null($datas))
    {
      // Par exemple pour seulement un retour à la ligne
    }
    else
    {
      $this->ligne[] = $datas;
    }
    // validation d’une ligne
    if($nb_eol)
    {
      // Formater une ligne pour un fichier CSV
      // @see https://fr.wikipedia.org/wiki/Comma-separated_values
      $this->contenu .= '"' . implode( '"'.$this->separateur.'"' , str_replace('"','""',$this->ligne) ) . '"'.str_repeat($this->eol,$nb_eol);
      $this->ligne = array();
    }
    // Retourne l’objet afin de permettre le chaînage de méthodes
    return $this;
  }

  /**
   * Envoi des données du CSV.
   * 
   * @param void
   * @return string
   */
  public function get()
  {
    return $this->contenu;
  }

}
?>