<?php
/**
 * @version $Id$
 * @author Thomas Crespin <thomas.crespin@sesamath.net>
 * @copyright Thomas Crespin 2009-2022
 * 
 * ****************************************************************************************************
 * SACoche <https://sacoche.sesamath.net> - Suivi d’Acquisitions de Compétences
 * © Thomas Crespin pour Sésamath <https://www.sesamath.net> - Tous droits réservés.
 * Logiciel placé sous la licence libre Affero GPL 3 <https://www.gnu.org/licenses/agpl-3.0.html>.
 * ****************************************************************************************************
 * 
 * Ce fichier est une partie de SACoche.
 * 
 * SACoche est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes 
 * de la “GNU Affero General Public License” telle que publiée par la Free Software Foundation :
 * soit la version 3 de cette licence, soit (à votre gré) toute version ultérieure.
 * 
 * SACoche est distribué dans l’espoir qu’il vous sera utile, mais SANS AUCUNE GARANTIE :
 * sans même la garantie implicite de COMMERCIALISABILITÉ ni d’ADÉQUATION À UN OBJECTIF PARTICULIER.
 * Consultez la Licence Publique Générale GNU Affero pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU Affero avec SACoche ;
 * si ce n’est pas le cas, consultez : <http://www.gnu.org/licenses/>.
 * 
 */

if(!defined('SACoche')) {exit('Ce fichier ne peut être appelé directement !');}

/**
 * Code inclus commun aux pages
 * [./pages/releve_socle2016.ajax.php]
 * [./pages/export_fichier.ajax.php]
 */

/* TODO -> À RETIRER SI INUTILE : $make_officiel + $make_livret + $make_action + $make_html + $make_pdf + $tab_destinataires */

Erreur500::prevention_et_gestion_erreurs_fatales( TRUE /*memory*/ , FALSE /*time*/ );

/*
$type_individuel | $type_synthese | $type_repartition
*/

// Chemins d’enregistrement

// $fichier_nom = ($make_action!='imprimer') ? 'releve_socle2016_'.$releve_modele.'_'.Clean::fichier($groupe_nom).'_<REPLACE>_'.FileSystem::generer_fin_nom_fichier__date_et_alea() : 'officiel_'.$BILAN_TYPE.'_'.Clean::fichier($groupe_nom).'_'.FileSystem::generer_fin_nom_fichier__date_et_alea() ;
$fichier_nom = 'releve_socle2016_'.Clean::fichier($groupe_nom).'_<REPLACE>_'.FileSystem::generer_fin_nom_fichier__date_et_alea();

// Si positionnement demandé ou besoin pour synthèse
$calcul_positionnement = ( $type_synthese || $type_repartition || $aff_socle_position || $aff_socle_points_DNB ) ? TRUE : FALSE ;

// Initialisation de tableaux

$tab_socle_domaine    = array();  // [socle_domaine_id] => domaine_nom;
$tab_socle_composante = array();  // [socle_domaine_id][socle_composante_id] => composante_nom;
$tab_join_item_socle  = array();  // [item_id] => socle_composante_id;
$tab_eleve_infos      = array();  // [eleve_id] => array(eleve_INE,eleve_nom,eleve_prenom,date_naissance)
$tab_item_infos       = array();  // [item_id] => array(item_ref,item_nom,item_cart,item_lien,matiere_id,calcul_methode,calcul_limite);
$tab_eval             = array();  // [eleve_id][item_id][]['note'] => note
$tab_remove_strong    = array('<strong>','</strong>');

// Initialisation de variables

$liste_eleve_id = implode(',',$tab_eleve);

if( ($make_html) || ($make_pdf) )
{
  $texte_coef  = ''; // sans objet
  $texte_socle = ''; // sans objet
  if(!$aff_lien)  { $texte_lien_avant = ''; }
  if(!$aff_lien)  { $texte_lien_apres = ''; }
  $toggle_class = ($aff_start) ? 'toggle_moins' : 'toggle_plus' ;
  $toggle_etat  = ($aff_start) ? '' : ' class="hide"' ;
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Récupération de la liste des domaines et composantes du socle (indépendant du cycle sélectionné)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$DB_TAB = DB_STRUCTURE_COMMUN::DB_recuperer_socle2016_arborescence();
$socle_domaine_id    = 0;
$socle_composante_id = 0;
foreach($DB_TAB as $DB_ROW)
{
  if( $DB_ROW['socle_domaine_id'] != $socle_domaine_id )
  {
    $socle_domaine_id  = $DB_ROW['socle_domaine_id'];
    $tab_socle_domaine[$socle_domaine_id] = $DB_ROW['socle_domaine_nom_simple'];
  }
  $DB_ROW['socle_composante_id']         = ( ($socle_detail=='detail') || ($socle_domaine_id==1) ) ? $DB_ROW['socle_composante_id']         : $socle_domaine_id*10 ;
  $DB_ROW['socle_composante_nom_simple'] = ( ($socle_detail=='detail') || ($socle_domaine_id==1) ) ? $DB_ROW['socle_composante_nom_simple'] : 'Toutes composantes confondues' ;
  $socle_composante_id = $DB_ROW['socle_composante_id'];
  $tab_socle_composante[$socle_domaine_id][$socle_composante_id] = $DB_ROW['socle_composante_nom_simple'];
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Récupération de la liste des élèves
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($_SESSION['USER_PROFIL_TYPE']=='eleve')
{
  $tab_eleve_infos[$_SESSION['USER_ID']] = array(
    'eleve_nom'      => $_SESSION['USER_NOM'],
    'eleve_prenom'   => $_SESSION['USER_PRENOM'],
    'eleve_genre'    => $_SESSION['USER_GENRE'],
    'date_naissance' => $_SESSION['USER_NAISSANCE_DATE'],
    'eleve_INE'      => NULL,
    'eleve_ID_BE'    => NULL,
  );
}
else
{
  $tab_eleve_infos = DB_STRUCTURE_BILAN::DB_lister_eleves_cibles( $liste_eleve_id , $groupe_type , $eleves_ordre );
  if(!is_array($tab_eleve_infos))
  {
    Json::end( FALSE , 'Aucun élève trouvé correspondant aux identifiants transmis !' );
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Récupération de la liste des items et des liaisons items / composantes
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$DB_TAB = DB_STRUCTURE_BILAN::DB_recuperer_associations_items_composantes($cycle_id);
foreach($DB_TAB as $DB_ROW)
{
  $socle_composante_id = ( ($socle_detail=='detail') || ($DB_ROW['socle_domaine_id']==1) ) ? $DB_ROW['socle_composante_id'] : $DB_ROW['socle_domaine_id']*10 ;
  $tab_join_item_socle[$DB_ROW['item_id']][$socle_composante_id] = $socle_composante_id;
}
$liste_item_id = implode(',',array_keys($tab_join_item_socle));

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Récupération de la liste des résultats
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($liste_item_id)
{
  // On fait plusieurs passages pour limiter le risque d’un dépassement de mémoire s’il y a bcp de données remontées
  $nb_eleves = substr_count($liste_eleve_id,',') + 1;
  $nb_items  = substr_count($liste_item_id ,',') + 1;
  $nb_passages = ceil( $nb_eleves * $nb_items / 25000 );
  if($nb_passages==1)
  {
    $tab_passage_eleves = array($liste_eleve_id);
  }
  else
  {
    $nb_eleves_par_passage = ceil( $nb_eleves / $nb_passages );
    $tab_passage_eleves = array_chunk( explode(',',$liste_eleve_id) , $nb_eleves_par_passage );
    foreach($tab_passage_eleves as $i => $tab)
    {
      $tab_passage_eleves[$i] = implode(',',$tab);
    }
  }
  $date_sql_debut = ($only_annee) ? To::jour_debut_annee_scolaire('sql') : FALSE ;
  foreach($tab_passage_eleves as $sous_liste_eleve_id)
  {
    $DB_TAB = DB_STRUCTURE_BILAN::DB_lister_result_eleves_items( $sous_liste_eleve_id , $liste_item_id , -1 /*matiere_id*/ , 'non' /*only_diagnostic*/ , $date_sql_debut , FALSE /*date_sql_fin*/ , $_SESSION['USER_PROFIL_TYPE'] , FALSE /*only_prof_id*/ , FALSE /*only_valeur*/ , TRUE /*onlynote*/ , FALSE /*first_order_by_date*/ );
    foreach($DB_TAB as $DB_ROW)
    {
      if( ($mode=='auto') || in_array($DB_ROW['matiere_id'],$tab_matiere) )
      {
        $tab_eval[$DB_ROW['eleve_id']][$DB_ROW['item_id']][]['note'] = $DB_ROW['note'];
        $tab_item_infos[$DB_ROW['item_id']] = TRUE;
      }
    }
  }
  if(count($tab_item_infos))
  {
    $liste_item_id = implode(',',array_keys($tab_item_infos));
    $detail = ($type_individuel) ? TRUE : FALSE ;
    $DB_TAB = DB_STRUCTURE_BILAN::DB_lister_infos_items( $liste_item_id , $detail );
    foreach($DB_TAB as $DB_ROW)
    {
      $tab_item_infos[$DB_ROW['item_id']] = array(
        'calcul_methode'      => $DB_ROW['calcul_methode'],
        'calcul_limite'       => $DB_ROW['calcul_limite'],
      );
      if($type_individuel)
      {
        $item_ref = ($DB_ROW['ref_perso']) ? $DB_ROW['ref_perso'] : $DB_ROW['ref_auto'] ;
        $tab_item_infos[$DB_ROW['item_id']] += array(
          'item_ref'            => $DB_ROW['matiere_ref'].'.'.$item_ref,
          'item_nom'            => $DB_ROW['item_nom'],
          'item_cart'           => $DB_ROW['item_cart'],
          'item_lien'           => $DB_ROW['item_lien'],
          'matiere_id'          => $DB_ROW['matiere_id'],
          'matiere_nb_demandes' => $DB_ROW['matiere_nb_demandes'],
        );
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Récupérer et mettre en session les seuils pour les degrés de maîtrise du livret
// ////////////////////////////////////////////////////////////////////////////////////////////////////

Outil::recuperer_seuils_livret( 'cycle'.$cycle_id );

// Liste des matières d’un prof
$listing_prof_matieres_id = ( !$make_officiel && $type_individuel && ($_SESSION['USER_PROFIL_TYPE']=='professeur') ) ? DB_STRUCTURE_COMMUN::DB_recuperer_matieres_professeur($_SESSION['USER_ID']) : '' ;
$tab_prof_matieres_id = !empty($listing_prof_matieres_id) ? explode(',',$listing_prof_matieres_id) : array() ;

// Fermeture de session (mais pas destruction, juste écriture et libération des données pour éviter un verrouillage en écriture)
Session::write_close();

// ////////////////////////////////////////////////////////////////////////////////////////////////////
/* 
 * Libérer de la place mémoire car les scripts de bilans sont assez gourmands.
 * Supprimer $DB_TAB ne fonctionne pas si on ne force pas auparavant la fermeture de la connexion.
 * SebR devrait peut-être envisager d’ajouter une méthode qui libère cette mémoire, si c’est possible...
 */
// ////////////////////////////////////////////////////////////////////////////////////////////////////

DB::close(SACOCHE_STRUCTURE_BD_NAME);
unset($DB_TAB);

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialiser les tableaux pour retenir les données
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$tab_init_score = array_fill_keys( array_keys($_SESSION['ACQUIS']) , 0 );
$tab_init_bilan = array( 'nb'=>0 , '%'=>FALSE , 'indice'=>FALSE , 'points'=>0 );
$tab_score_eleve_composante = array();  // [eleve_id][composante_id] => array([etats]) // Retenir le nb d’items acquis ou pas / élève / composante
$tab_bilan_eleve_composante = array();  // [eleve_id][composante_id] => array(nb,%,indice) // Retenir les infos sur les valeurs bilan / élève / composante du socle
$tab_infos_eleve_composante = array();  // [eleve_id][composante_id] => array() // Retenir les infos sur les items travaillés et leurs scores / élève / composante du socle
$tab_contenu_presence       = array( 'eleve' => array() , 'composante' => array() , 'detail' => array() );

$tab_points_valeur = array( 0=>0 , 1=>10 , 2=>25 , 3=>40 , 4=>50 );
$tab_points_texte  = array( 0=>'' , 10=>'10 points' , 25=>'25 points' , 40=>'40 points' , 50=>'50 points' );

// Pour chaque élève...
foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
{
  $tab_contenu_presence['eleve'][$eleve_id] = 0;
  $tab_bilan_eleve_composante[$eleve_id]['total_dnb'] = 0;
  // Pour chaque domaine...
  foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
  {
    // Pour chaque composante...
    foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
    {
      $tab_score_eleve_composante[$eleve_id][$socle_composante_id] = $tab_init_score;
      $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id] = $tab_init_bilan;
      $tab_infos_eleve_composante[$eleve_id][$socle_composante_id] = array();
      $tab_contenu_presence['composante'][$socle_composante_id] = 0;
      $tab_contenu_presence['detail'][$eleve_id][$socle_composante_id] = 0;
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Elaboration du bilan relatif au socle, en HTML et PDF => Tableaux et variables pour mémoriser les infos ; dans cette partie on ne fait que les calculs (aucun affichage)
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$afficher_score = Outil::test_user_droit_specifique( $_SESSION['DROIT_VOIR_SCORE_BILAN'] );

// Pour chaque élève évalué...
foreach($tab_eval as $eleve_id => $tab_eval_eleve)
{
  // Pour chaque item évalué...
  foreach($tab_eval_eleve as $item_id => $tab_devoirs)
  {
    extract($tab_item_infos[$item_id]);  // $item_ref $item_nom $item_cart $item_lien $matiere_id $matiere_nb_demandes $calcul_methode $calcul_limite
    // calcul du bilan de l’item
    $score = OutilBilan::calculer_score( $tab_devoirs , $calcul_methode , $calcul_limite , NULL /*date_sql_debut*/ );
    if($score!==FALSE)
    {
      // on détermine si il est acquis ou pas
      $indice = OutilBilan::determiner_etat_acquisition( $score );
      // le détail HTML
      if($type_individuel)
      {
        if($aff_lien)
        {
          $texte_lien_avant = ($item_lien) ? '<a target="_blank" rel="noopener noreferrer" href="'.html($item_lien).'">' : '';
          $texte_lien_apres = ($item_lien) ? '</a>' : '';
        }
        $icones_action = ( ($_SESSION['USER_PROFIL_TYPE']=='professeur') && in_array($matiere_id,$tab_prof_matieres_id) )
                       ? '<q class="ajouter_note"'.infobulle('Ajouter une évaluation à la volée.').'></q>'
                       . '<q class="modifier_note"'.infobulle('Modifier à la volée une saisie d’évaluation.').'></q>'
                       : '' ;
        if($aff_panier)
        {
          $debut_date = $date_sql_debut;
          if(!$matiere_nb_demandes) { $icones_action .= '<q class="demander_non"'.infobulle('Pas de demande autorisée pour les items de cette matière.').'></q>'; }
          elseif(!$item_cart)       { $icones_action .= '<q class="demander_non"'.infobulle('Pas de demande autorisée pour cet item précis.').'></q>'; }
          else                      { $icones_action .= '<q class="demander_add" data-score="'.( $score ? $score : -1 ).'" data-date="'.$debut_date.'"'.infobulle('Ajouter aux demandes d’évaluations.').'></q>'; }
        }
      }
      // on enregistre les infos
      foreach($tab_join_item_socle[$item_id] as $socle_composante_id)
      {
        if( $make_html && $type_individuel && $aff_socle_items_acquis )
        {
          $pourcentage = ($afficher_score) ? $score.'%' : '&nbsp;' ;
          $tab_infos_eleve_composante[$eleve_id][$socle_composante_id][] = '<div data-matiere="'.$matiere_id.'" data-item="'.$item_id.'" data-eleve="'.$eleve_id.'"><span class="pourcentage A'.$indice.'">'.$pourcentage.'</span> '.$texte_coef.$texte_socle.$texte_lien_avant.html($item_ref.' - '.$item_nom).$texte_lien_apres.$icones_action.'</div>';
        }
        $tab_score_eleve_composante[$eleve_id][$socle_composante_id][$indice]++;
        $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['nb']++;
      }
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On calcule les pourcentages d’acquisition à partir du nombre d’items de chaque état
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($calcul_positionnement)
{
  foreach( $tab_score_eleve_composante as $eleve_id => $tab_score_composante )
  {
    $total_dnb = 0;
    foreach( $tab_score_composante as $socle_composante_id => $tab_score )
    {
      $nb_items = $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['nb'];
      $pourcentage = ($nb_items) ? OutilBilan::calculer_pourcentage_acquisition_items( $tab_score , $nb_items ) : FALSE ;
      $indice = OutilBilan::determiner_degre_maitrise($pourcentage);
      $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['%'] = $pourcentage;
      $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['indice'] = $indice;
      $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['points'] = ($indice!==FALSE) ? $tab_points_valeur[$indice] : FALSE ;
      $total_dnb += $tab_points_valeur[$indice];
    }
    $tab_bilan_eleve_composante[$eleve_id]['total_dnb'] = $total_dnb;
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Restriction de l’affichage aux seuls éléments ayant fait l’objet d’une évaluation
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$is_resultat = FALSE;
// Pour chaque élève...
foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
{
  // Pour chaque domaine...
  foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
  {
    // Pour chaque composante...
    foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
    {
      if( $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['nb'] || !$only_presence )
      {
        $tab_contenu_presence['eleve'][$eleve_id]++;
        $tab_contenu_presence['composante'][$socle_composante_id]++;
        $tab_contenu_presence['detail'][$eleve_id][$socle_composante_id]++;
        $is_resultat = TRUE;
      }
    }
  }
}

if(!$is_resultat)
{
  Json::end( FALSE , 'Aucun élève trouvé avec un item évalué relié au cycle '.$cycle_id.' du socle !' );
}

$tab = array_filter( $tab_contenu_presence['eleve'] , 'non_zero' );
$eleve_nb = count($tab);
$composante_nb_moyen = array_sum($tab) / $eleve_nb;

$tab = array_filter( $tab_contenu_presence['composante'] , 'non_zero' );
$composante_nb = count($tab);
$eleve_nb_moyen = array_sum($tab) / $composante_nb;

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Répartition statistique
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($type_repartition)
{
  $tab_repartition_count = array(); // [socle_composante_id][indice] => nb;
  $tab_degres_init = array();
  foreach($_SESSION['LIVRET'] as $id => $tab)
  {
    if($tab['USED'])
    {
      $tab_degres_init[$id] = 0;
    }
  }
  $tab_degres_init['sans'] = 0;
  // Pour chaque domaine...
  foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
  {
    // Pour chaque composante...
    foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
    {
      if($tab_contenu_presence['composante'][$socle_composante_id])
      {
        $tab_repartition_count[$socle_composante_id] = $tab_degres_init;
        // Pour chaque élève...
        foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
        {
          if($tab_contenu_presence['eleve'][$eleve_id])
          {
            $key = !empty($tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['indice']) ? $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id]['indice'] : 'sans' ;
            $tab_repartition_count[$socle_composante_id][$key] += 1;
          }
        }
      }
    }
  }
  // On passe maintenant des effectifs aux pourcentages
  foreach($tab_repartition_count as $socle_composante_id => $tab_indice)
  {
    $somme = array_sum($tab_indice);
    foreach($tab_indice as $key => $nombre)
    {
      $tab_repartition_count[$socle_composante_id][$key] = ($somme) ? round($nombre/$somme*100,0) : 0;
    }
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Nombre de boucles par élève (entre 1 et 3 pour les bilans officiels, dans ce cas $tab_destinataires[] est déjà complété ; une seule dans les autres cas).
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if(!isset($tab_destinataires))
{
  foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
  {
    $tab_destinataires[$eleve_id][0] = TRUE ;
  }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// On va passer à la production des documents
// ////////////////////////////////////////////////////////////////////////////////////////////////////

$affichage_direct   = ( ( ( in_array($_SESSION['USER_PROFIL_TYPE'],array('eleve','parent')) ) && (SACoche!='webservices') ) || ($make_officiel) ) ? TRUE : FALSE ;
$affichage_checkbox = ( $type_synthese && ($_SESSION['USER_PROFIL_TYPE']=='professeur') && (SACoche!='webservices') )                             ? TRUE : FALSE ;

$nb_matieres = count($tab_matiere);
$titre_detail  = ($socle_detail=='detail') ? '(toutes composantes)' : '(rubriques du livret)' ;
$titre_matiere = ($mode=='auto') ? 'Toutes matières' : ( ($nb_matieres==1) ? $tab_remove_strong[0].$matiere_nom.$tab_remove_strong[1] : $tab_remove_strong[0].$nb_matieres.' matières'.$tab_remove_strong[1] ) ;
$titre_periode = ($only_annee) ? $tab_remove_strong[0].'Année scolaire en cours'.$tab_remove_strong[1] : 'Toute la scolarité' ;
$titre_html    = 'Estimation de maîtrise du socle commun '.$titre_detail.' - '.$cycle_nom.' - '.$titre_matiere.' - '.$titre_periode ;
$titre_pdf_csv = str_replace($tab_remove_strong,'',$titre_html);

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Elaboration du relevé individuel, en HTML et PDF
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($type_individuel)
{
  if($make_html)
  {
    $html  = $affichage_direct ? '' : '<style>'.$_SESSION['CSS'].'</style>'.NL;
    $html .= $make_officiel    ? '' : '<h1>'.$titre_html.'</h1>'.NL;
    $separation = (count($tab_eleve_infos)>1) ? '<hr class="breakafter">'.NL : '' ;
    $html_javascript = '';
    $tab_legende = array(
      'etat_acquisition' => $aff_socle_items_acquis ,
      'degre_maitrise'   => $aff_socle_position ,
      'socle_points'     => $aff_socle_points_DNB ,
    );
  }
  if($make_pdf)
  {
    // Appel de la classe et définition de qqs variables supplémentaires pour la mise en page PDF
    $pdf = new PDF_socle2016_releve( $make_officiel , 'A4' /*page_size*/ , 'portrait' /*orientation*/ , $marge_gauche , $marge_droite , $marge_haut , $marge_bas , $couleur , $fond , $legende );
  }
  /*
   * ********************************
   * Cas d’une présentation par élève
   * ********************************
   */
  if($socle_individuel_format=='eleve')
  {
    if($make_pdf)
    {
      $pdf->initialiser( $socle_individuel_format , $eleve_nb , $composante_nb , $eleve_nb_moyen , $composante_nb_moyen , $pages_nb , $aff_socle_items_acquis , $aff_socle_position , $aff_socle_points_DNB );
    }
    // Pour chaque élève...
    foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
    {
      if($tab_contenu_presence['eleve'][$eleve_id])
      {
        extract($tab_eleve); // $eleve_INE $eleve_ID_BE $eleve_nom $eleve_prenom $eleve_genre $date_naissance
        $eleve_nom_prenom = To::texte_eleve_identite($eleve_nom,$eleve_prenom,$eleves_ordre);
        foreach($tab_destinataires[$eleve_id] as $numero_tirage => $tab_adresse)
        {
          // Si cet élève a été évalué...
          if(isset($tab_eval[$eleve_id]))
          {
            $sous_titre = $groupe_nom.' - '.$eleve_nom_prenom;
            $sous_titre.= ($aff_socle_points_DNB) ? ' - '.$tab_bilan_eleve_composante[$eleve_id]['total_dnb'].' points sur 400' : '' ;
            // Intitulé
            if($make_html)
            {
              $html .=  $separation.'<h2>'.html($sous_titre).'</h2>'.NL;
              $html .=  '<table class="livret"><tbody>'.NL;
            }
            if($make_pdf)
            {
              $nb_lignes  = $tab_contenu_presence['eleve'][$eleve_id];
              $pdf->entete( $titre_pdf_csv , $sous_titre , $nb_lignes , $pages_nb );
            }
            // Pour chaque domaine / composante...
            foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
            {
              foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
              {
                if($tab_contenu_presence['detail'][$eleve_id][$socle_composante_id])
                {
                  $tab_score = $tab_score_eleve_composante[$eleve_id][$socle_composante_id];
                  $tab_bilan = $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id];
                  if($make_html) { $html .= '<tr><td><b>'.html($socle_domaine_id.' '.$socle_domaine_nom).'</b><br>'.html($socle_composante_nom).'</td>'; }
                  if($make_pdf)  { $pdf->ligne_debut( $socle_domaine_id.' '.$socle_domaine_nom , $socle_composante_nom ); }
                  if($aff_socle_items_acquis)
                  {
                    if($make_html)
                    {
                      if(!empty($tab_infos_eleve_composante[$eleve_id][$socle_composante_id]))
                      {
                        $detail_acquisition = OutilBilan::afficher_nombre_acquisitions_par_etat( $tab_score , TRUE /*detail_couleur*/ );
                        $html .= '<td><a href="#toggle" class="'.$toggle_class.'"'.infobulle('Voir / masquer le détail des items associés.').' id="to_'.$eleve_id.'_'.$socle_composante_id.'"></a> '.$detail_acquisition;
                        $html .= '<div id="'.$eleve_id.'_'.$socle_composante_id.'"'.$toggle_etat.'>'.implode('',$tab_infos_eleve_composante[$eleve_id][$socle_composante_id]).'</div></td>';
                      }
                      else
                      {
                        $html .= '<td><span class="notnow">aucun item évalué</span></td>' ;
                      }
                    }
                    if($make_pdf)
                    {
                      $tab_score = array_filter($tab_score,'non_zero');
                      $pdf->afficher_proportion_acquis( $pdf->synthese_largeur , $pdf->cases_hauteur , $tab_score , $tab_bilan['nb'] /*total*/ , TRUE /*avec_texte_nombre*/ , TRUE /*avec_texte_code*/ );
                    }
                  }
                  if($aff_socle_position)
                  {
                    if($make_html) { $html .= Html::td_maitrise( $tab_bilan['indice'] , $tab_bilan['%'] , $tableau_tri_maitrise_mode , '%' /*pourcent*/ , TRUE /*all_columns*/ ); }
                    if($make_pdf)  { $pdf->afficher_degre_maitrise( $tab_bilan['indice'] , $tab_bilan['%'] , '%' /*pourcent*/ , TRUE /*all_columns*/ ); }
                  }
                  if($aff_socle_points_DNB)
                  {
                    if($make_html) { $html .= '<td><b>'.$tab_points_texte[$tab_bilan['points']].'</b></td>'; }
                    if($make_pdf)  { $pdf->cellule_nombre_points( $tab_points_texte[$tab_bilan['points']] ); }
                  }
                  if($make_html)
                  {
                    $html .= '</tr>'.NL;
                  }
                  if($make_pdf)
                  {
                    $pdf->ligne_retour();
                  }
                }
              }
            }
            if($make_html) { $html .= '</tbody></table>'.NL; }
            // Légende
            if( ( ($make_html) || ($make_pdf) ) && ($legende=='oui') )
            {
              if($make_html) { $html .= Html::legende($tab_legende); }
              if($make_pdf)  { $pdf->legende( $aff_socle_items_acquis , $aff_socle_position , $aff_socle_points_DNB ); }
            }
          }
        }
      }
    }
  }
  /*
   * *******************************
   * Cas d’une présentation par composante
   * *******************************
   */
  elseif($socle_individuel_format=='composante')
  {
    if($make_pdf)
    {
      $pdf->initialiser( $socle_individuel_format , $eleve_nb , $composante_nb , $eleve_nb_moyen , $composante_nb_moyen , $pages_nb , $aff_socle_items_acquis , $aff_socle_position , $aff_socle_points_DNB );
    }
    // Pour chaque domaine / composante...
    foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
    {
      foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
      {
        if($tab_contenu_presence['composante'][$socle_composante_id])
        {
          $sous_titre = $socle_domaine_id.' '.$socle_domaine_nom;
          // Intitulé
          if($make_html)
          {
            $html .=  $separation.'<h2>'.html($sous_titre).'</h2>'.NL;
            $html .=  '<h3>'.html($socle_composante_nom).'</h3>'.NL;
            $html .=  '<table class="livret"><tbody>'.NL;
          }
          if($make_pdf)
          {
            $nb_lignes  = $tab_contenu_presence['composante'][$socle_composante_id];
            $pdf->entete( $titre_pdf_csv , $sous_titre.' - '.$socle_composante_nom , $nb_lignes , $pages_nb );
          }
          // Pour chaque élève...
          foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
          {
            if($tab_contenu_presence['detail'][$eleve_id][$socle_composante_id])
            {
              extract($tab_eleve); // $eleve_INE $eleve_ID_BE $eleve_nom $eleve_prenom $eleve_genre $date_naissance
              $eleve_nom_prenom = To::texte_eleve_identite($eleve_nom,$eleve_prenom,$eleves_ordre);
              foreach($tab_destinataires[$eleve_id] as $numero_tirage => $tab_adresse)
              {
                // Si cet élève a été évalué...
                if(isset($tab_eval[$eleve_id]))
                {
                  $classe_eleve = $groupe_nom.' - '.$eleve_nom_prenom;
                  $classe_eleve.= ($aff_socle_points_DNB) ? ' - '.$tab_bilan_eleve_composante[$eleve_id]['total_dnb'].' points sur 400' : '' ;
                  $tab_score = $tab_score_eleve_composante[$eleve_id][$socle_composante_id];
                  $tab_bilan = $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id];
                  if($make_html) { $html .= '<tr><td>'.html($classe_eleve).'</td>'; }
                  if($make_pdf)  { $pdf->ligne_debut( $classe_eleve ); }
                  if($aff_socle_items_acquis)
                  {
                    if($make_html)
                    {
                      if(!empty($tab_infos_eleve_composante[$eleve_id][$socle_composante_id]))
                      {
                        $detail_acquisition = OutilBilan::afficher_nombre_acquisitions_par_etat( $tab_score , TRUE /*detail_couleur*/ );
                        $html .= '<td><a href="#toggle" class="'.$toggle_class.'"'.infobulle('Voir / masquer le détail des items associés.').' id="to_'.$eleve_id.'_'.$socle_composante_id.'"></a> '.$detail_acquisition;
                        $html .= '<div id="'.$eleve_id.'_'.$socle_composante_id.'"'.$toggle_etat.'>'.implode('',$tab_infos_eleve_composante[$eleve_id][$socle_composante_id]).'</div></td>';
                      }
                      else
                      {
                        $html .= '<td><span class="notnow">aucun item évalué</span></td>' ;
                      }
                    }
                    if($make_pdf)
                    {
                      $tab_score = array_filter($tab_score,'non_zero');
                      $pdf->afficher_proportion_acquis( $pdf->synthese_largeur , $pdf->cases_hauteur , $tab_score , $tab_bilan['nb'] /*total*/ , TRUE /*avec_texte_nombre*/ , TRUE /*avec_texte_code*/ );
                    }
                  }
                  if($aff_socle_position)
                  {
                    if($make_html) { $html .= Html::td_maitrise( $tab_bilan['indice'] , $tab_bilan['%'] , $tableau_tri_maitrise_mode , '%' /*pourcent*/ , TRUE /*all_columns*/ ); }
                    if($make_pdf)  { $pdf->afficher_degre_maitrise( $tab_bilan['indice'] , $tab_bilan['%'] , '%' /*pourcent*/ , TRUE /*all_columns*/ ); }
                  }
                  if($aff_socle_points_DNB)
                  {
                    if($make_html) { $html .= '<td><b>'.$tab_points_texte[$tab_bilan['points']].'</b></td>'; }
                    if($make_pdf)  { $pdf->cellule_nombre_points( $tab_points_texte[$tab_bilan['points']] ); }
                  }
                  if($make_html)
                  {
                    $html .= '</tr>'.NL;
                  }
                  if($make_pdf)
                  {
                    $pdf->ligne_retour();
                  }
                }
              }
            }
          }
          if($make_html) { $html .= '</tbody></table>'.NL; }
          // Légende
          if( ( ($make_html) || ($make_pdf) ) && ($legende=='oui') )
          {
            if($make_html) { $html .= Html::legende($tab_legende); }
            if($make_pdf)  { $pdf->legende( $aff_socle_items_acquis , $aff_socle_position , $aff_socle_points_DNB ); }
          }
        }
      }
    }
  }
  if( !$make_officiel && $make_html && ($_SESSION['USER_PROFIL_TYPE']=='professeur') )
  {
    $script = 'var CSRF = "'.$CSRF_eval_eclair.'";'; // Pour les évaluations à la volée.
    $html .= '<script>'.$script.'</script>'.NL;
  }
  // On enregistre les sorties HTML et PDF et CSV
  if($make_html) { FileSystem::ecrire_fichier(   CHEMIN_DOSSIER_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.html' , $html ); }
  if($make_pdf)  { FileSystem::ecrire_objet_pdf( CHEMIN_DOSSIER_EXPORT.str_replace('<REPLACE>','individuel',$fichier_nom).'.pdf'  , $pdf  ); }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Elaboration de la synthèse collective en HTML et PDF
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($type_synthese)
{
  if($socle_synthese_affichage=='pourcentage')
  {
    $objet = 'Synthèse du pourcentage d’items acquis';
    $clef = '%';
    $unit = '%';
  }
  elseif($socle_synthese_affichage=='position')
  {
    $objet = 'Synthèse des pré-positionnements';
    $clef = 'indice';
    $unit = ' / 4';
  }
  else
  {
    $objet = 'Synthèse de la prévision du nombre de points pour le brevet';
    $clef = 'points';
    $unit = ' pts';
  }
  if($make_html)
  {
    $html  = $affichage_direct ? '' : '<style>'.$_SESSION['CSS'].'</style>'.NL;
    $html .= $make_officiel    ? '' : '<h1>'.$titre_html.'</h1>'.NL;
    $html .= '<hr>'.NL.'<h2>'.html($groupe_nom).' - '.$objet.' (selon l’objet et le mode de tri choisis)</h2>'.NL;
    $th     = ($socle_synthese_format=='eleve') ? 'Élève' : 'Socle' ;
    $sorter = ($socle_synthese_format=='eleve') ? ' data-sorter="text"' : ' data-sorter="FromData"' ;
    $html_table_head = '<thead><tr><th'.$sorter.'>'.$th.'</th>';
    $html_table_body = '';
    $html_table_foot = '';
  }
  if($make_pdf)
  {
    // Appel de la classe et redéfinition de qqs variables supplémentaires pour la mise en page PDF
    // On définit l’orientation la plus adaptée
    $orientation_auto = ( ( ($eleve_nb>$composante_nb) && ($socle_synthese_format=='eleve') ) || ( ($composante_nb>$eleve_nb) && ($socle_synthese_format=='composante') ) ) ? 'portrait' : 'landscape' ;
    $pdf = new PDF_socle2016_synthese( $make_officiel , 'A4' /*page_size*/ , $orientation_auto , $marge_gauche , $marge_droite , $marge_haut , $marge_bas , $couleur , $fond , $legende );
    $pdf->initialiser( $socle_synthese_format , $eleve_nb , $composante_nb , $socle_synthese_affichage );
    $pdf->entete( $titre_pdf_csv , $groupe_nom , $objet , $socle_synthese_format );
    $pdf->ligne_tete_cellule_debut();
  }
  if($make_csv)
  {
    $csv = new CSV();
    $csv->add( $objet , 1 )->add( 'Exploitation tableur' , 1 )->add( $groupe_nom , 1 );
    $csv->add( 'Nom / Prénom' );
  }
  if($socle_synthese_format=='eleve')
  {
    // Pour chaque domaine / composante...
    foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
    {
      foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
      {
        if($tab_contenu_presence['composante'][$socle_composante_id])
        {
          $txt_abrev_domaine    = 'Domaine '.$socle_domaine_id;
          $txt_abrev_composante = ($socle_composante_id%10) ? ' - Composante '.($socle_composante_id%10) : '' ;
          $txt_abrev = $txt_abrev_domaine.$txt_abrev_composante;
          if($make_html) { $html_table_head .= '<th data-sorter="FromData" data-empty="bottom"'.infobulle($socle_domaine_id.' '.$socle_domaine_nom.BRJS.$socle_composante_nom).'><dfn>'.html($txt_abrev).'</dfn></th>'; }
          if($make_pdf)  { $pdf->ligne_tete_cellule_corps( $txt_abrev ); }
          if($make_csv)  { $csv->add( ($socle_composante_id%10) ? $socle_domaine_id.'.'.($socle_composante_id%10) : $socle_domaine_id ); }

        }
      }
    }
    if($socle_synthese_affichage=='points')
    {
      $txt_full  = 'Nombre de points pour le brevet (sur 400)';
      $txt_abrev = 'Nombre de points (sur 400)';
      if($make_html) { $html_table_head .= '<th'.infobulle($txt_full).'><dfn>'.html($txt_abrev).'</dfn></th>'; }
      if($make_pdf)  { $pdf->ligne_tete_cellule_corps( $txt_abrev , TRUE /*is_bold*/ ); }
      if($make_csv)  { $csv->add( 'Points (sur 400)' ); }
    }
    if($make_csv)
    {
      $csv->add( NULL , 1 );
    }
    if($make_html)
    {
      $checkbox_vide = ($affichage_checkbox) ? '<th data-sorter="false" class="nu">&nbsp;</th>' : '' ;
      $html_table_head .= $checkbox_vide;
    }
  }
  else
  {
    // Pour chaque élève...
    foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
    {
      if($tab_contenu_presence['eleve'][$eleve_id])
      {
        extract($tab_eleve); // $eleve_INE $eleve_ID_BE $eleve_nom $eleve_prenom $eleve_genre $date_naissance
        $eleve_nom_prenom = To::texte_eleve_identite($eleve_nom,$eleve_prenom,$eleves_ordre);
        if($make_html) { $html_table_head .= '<th data-sorter="FromData" data-empty="bottom"><dfn>'.html($eleve_nom_prenom).'</dfn></th>'; }
        if($make_pdf)  { $pdf->ligne_tete_cellule_corps( $eleve_nom_prenom ); }
      }
    }
  }
  if($make_html) { $html_table_head .= '</tr></thead>'.NL; }
  if($make_pdf)  { $pdf->ligne_retour(0); }
  // lignes suivantes
  if($socle_synthese_format=='eleve')
  {
    // Pour chaque élève...
    foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
    {
      if($tab_contenu_presence['eleve'][$eleve_id])
      {
        extract($tab_eleve); // $eleve_INE $eleve_ID_BE $eleve_nom $eleve_prenom $eleve_genre $date_naissance
        $eleve_nom_prenom = To::texte_eleve_identite($eleve_nom,$eleve_prenom,$eleves_ordre);
        if($make_html) { $html_table_body .= '<tr><td>'.html($eleve_nom_prenom).'</td>'; }
        if($make_pdf)  { $pdf->ligne_corps_cellule_debut( $eleve_nom_prenom ); }
        if($make_csv)  { $csv->add( $eleve_nom_prenom ); }
        // Pour chaque domaine / composante...
        foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
        {
          foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
          {
            if($tab_contenu_presence['composante'][$socle_composante_id])
            {
              $tab_bilan = $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id];
              if($make_html) { $html_table_body .= Html::td_maitrise( $tab_bilan['indice'] , $tab_bilan[$clef] , $tableau_tri_maitrise_mode , $unit /*pourcent*/ , FALSE /*all_columns*/ ); }
              if($make_pdf)  { $pdf->afficher_degre_maitrise( $tab_bilan['indice'] , $tab_bilan[$clef] , $unit /*pourcent*/ , FALSE /*all_columns*/ ); }
              if($make_csv)  { $csv->add( $tab_bilan[$clef] ); }
            }
          }
        }
        if($socle_synthese_affichage=='points')
        {
          $points = $tab_bilan_eleve_composante[$eleve_id]['total_dnb'];
          if($make_html) { $html_table_body .= '<th class="hc">'.$points.'</th>'; }
          if($make_pdf)  { $pdf->cellule_total_points( $points ); }
          if($make_csv)  { $csv->add( $points ); }
        }
        if($make_csv)
        {
          $csv->add( NULL , 1 );
        }
        if($make_html)
        {
          $col_checkbox = ($affichage_checkbox) ? '<td class="nu"><input type="checkbox" name="id_user[]" value="'.$eleve_id.'"></td>' : '' ;
          $html_table_body .= $col_checkbox.'</tr>'.NL;
        }
        if($make_pdf) { $pdf->ligne_retour($eleve_id); }
      }
    }
  }
  else
  {
    // Pour chaque domaine / composante...
    foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
    {
      foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
      {
        if($tab_contenu_presence['composante'][$socle_composante_id])
        {
          if($make_html) { $html_table_body .= '<tr><td data-sort="'.$socle_composante_id.$socle_domaine_id.'"><b>'.html($socle_domaine_id.' '.$socle_domaine_nom).'</b><br>'.html($socle_composante_nom).'</td>'; }
          if($make_pdf)  { $pdf->ligne_corps_cellule_debut( $socle_domaine_id.' '.$socle_domaine_nom , $socle_composante_nom ); }
          // Pour chaque élève...
          foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
          {
            if($tab_contenu_presence['eleve'][$eleve_id])
            {
              $tab_bilan = $tab_bilan_eleve_composante[$eleve_id][$socle_composante_id];
              if($make_html) { $html_table_body .= Html::td_maitrise( $tab_bilan['indice'] , $tab_bilan[$clef] , $tableau_tri_maitrise_mode , $unit /*pourcent*/ , FALSE /*all_columns*/ ); }
              if($make_pdf)  { $pdf->afficher_degre_maitrise( $tab_bilan['indice'] , $tab_bilan[$clef] , $unit /*pourcent*/ , FALSE /*all_columns*/ ); }
            }
          }
          if($make_html) { $html_table_body .= '</tr>'.NL; }
          if($make_pdf) { $pdf->ligne_retour($socle_composante_id); }
        }
      }
    }
    if($socle_synthese_affichage=='points')
    {
      $txt_full  = 'Nombre de points pour le brevet (sur 400)';
      if($make_html) { $html_table_body .= '<tr><th>'.$txt_full.'</th>'; }
      if($make_pdf)  { $pdf->ligne_corps_cellule_debut( $txt_full , NULL /*contenu2*/ , TRUE /*is_bold*/ ); }
      foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
      {
        if($tab_contenu_presence['eleve'][$eleve_id])
        {
          $points = $tab_bilan_eleve_composante[$eleve_id]['total_dnb'];
          if($make_html) { $html_table_body .= '<th class="hc">'.$points.'</th>'; }
          if($make_pdf)  { $pdf->cellule_total_points( $points ); }
        }
      }
      if($make_html) { $html_table_body .= '</tr>'.NL; }
      if($make_pdf) { $pdf->ligne_retour(1); }
    }
  }
  if($make_html) { $html_table_body = '<tbody>'.NL.$html_table_body.'</tbody>'.NL; }
  // dernière ligne
  if( ($socle_synthese_format=='composante') && $affichage_checkbox )
  {
    if($make_html) { $html_table_foot .= '<tfoot>'.NL.'<tr><th class="nu">&nbsp;</th>'; }
    foreach($tab_eleve_infos as $eleve_id => $tab_eleve)
    {
      if($tab_contenu_presence['eleve'][$eleve_id])
      {
        if($make_html) { $html_table_foot .= '<td class="nu"><input type="checkbox" name="id_user[]" value="'.$eleve_id.'"></td>'; }
      }
    }
    if($make_html) { $html_table_foot .= '</tr>'.'</tfoot>'.NL; }
  }
  // assemblage pour la sortie HTML
  if($make_html)
  {
    $html .= ($affichage_checkbox) ? '<form id="form_synthese" action="#" method="post">'.NL : '' ;
    $html .= '<table id="table_s" class="bilan_synthese vsort">'.NL.$html_table_head.$html_table_foot.$html_table_body.'</table>'.NL;
  }
  // Légende
  if( ( ($make_html) || ($make_pdf) ) && ($legende=='oui') )
  {
    $tab_legende = array(
      'degre_maitrise' => TRUE ,
      'socle_points'   => ($socle_synthese_affichage=='points') ,
    );
    if($make_html) { $html .= Html::legende($tab_legende); }
    if($make_pdf)  { $pdf->legende( $socle_synthese_affichage ); }
  }
  $script = $affichage_direct ? '$("#table_s").tablesorter();' : 'function tri(){$("#table_s").tablesorter();}' ;
  if($make_html)
  {
    $html .= ($affichage_checkbox) ? HtmlForm::afficher_synthese_exploitation('eleves').'</form>'.NL : '';
    $html .= '<script>'.$script.'</script>'.NL;
  }
  // On enregistre les sorties HTML / PDF / CSV
  if($make_html) { FileSystem::ecrire_fichier(   CHEMIN_DOSSIER_EXPORT.str_replace('<REPLACE>','synthese',$fichier_nom).'.html' , $html ); }
  if($make_pdf)  { FileSystem::ecrire_objet_pdf( CHEMIN_DOSSIER_EXPORT.str_replace('<REPLACE>','synthese',$fichier_nom).'.pdf'  , $pdf  ); }
  if($make_csv)  { FileSystem::ecrire_objet_csv( CHEMIN_DOSSIER_EXPORT.str_replace('<REPLACE>','synthese',$fichier_nom).'.csv'  , $csv  ); }
}

// ////////////////////////////////////////////////////////////////////////////////////////////////////
// Elaboration de la répartition statistique en HTML et PDF
// ////////////////////////////////////////////////////////////////////////////////////////////////////

if($type_repartition)
{
  $objet = 'Répartition statistique des positionnements';
  if($make_html)
  {
    $html  = $affichage_direct ? '' : '<style>'.$_SESSION['CSS'].'</style>'.NL;
    $html .= $make_officiel    ? '' : '<h1>'.$titre_html.'</h1>'.NL;
    $html .= '<hr>'.NL.'<h2>'.html($groupe_nom).' - '.$objet.'</h2>'.NL;
    $html_table_head = '<thead><tr><th class="nu"></th>';
    $html_table_body = '';
  }
  if($make_pdf)
  {
    // Appel de la classe et redéfinition de qqs variables supplémentaires pour la mise en page PDF
    // On définit l’orientation la plus adaptée
    $pdf = new PDF_socle2016_repartition( $make_officiel , 'A4' /*page_size*/ , 'portrait' , $marge_gauche , $marge_droite , $marge_haut , $marge_bas , $couleur , $fond );
    $pdf->initialiser( $composante_nb , count($tab_degres_init) );
    $pdf->entete( $titre_pdf_csv , $groupe_nom , $objet );
    $pdf->ligne_tete_cellule_debut();
  }
  // première ligne
  foreach($tab_degres_init as $id => $zero)
  {
    $titre_degre = ($id!='sans') ? $_SESSION['LIVRET'][$id]['LEGENDE'] : 'Sans positionnement' ;
    if($make_html) { $html_table_head .= '<th class="hc">'.str_replace(' ','<br>',str_replace('Très bonne','Très&nbsp;bonne',html($titre_degre))).'</th>'; }
    if($make_pdf)  { $pdf->ligne_tete_cellule_corps( $titre_degre ); }
  }
  if($make_html) { $html_table_head .= '</tr></thead>'.NL; }
  if($make_pdf)  { $pdf->ligne_retour(0); }
  // lignes suivantes
  // Pour chaque domaine / composante...
  foreach($tab_socle_domaine as $socle_domaine_id => $socle_domaine_nom)
  {
    foreach($tab_socle_composante[$socle_domaine_id] as $socle_composante_id => $socle_composante_nom)
    {
      if($tab_contenu_presence['composante'][$socle_composante_id])
      {
        if($make_html) { $html_table_body .= '<tr><td><b>'.html($socle_domaine_id.' '.$socle_domaine_nom).'</b><br>'.html($socle_composante_nom).'</td>'; }
        if($make_pdf)  { $pdf->ligne_corps_cellule_debut( $socle_domaine_id.' '.$socle_domaine_nom , $socle_composante_nom ); }
        // Pour chaque degré de maitrise...
        foreach($tab_repartition_count[$socle_composante_id] as $key => $pourcentage)
        {
          if($make_html) { $html_table_body .= '<td class="hc" style="font-size:'.(75+$pourcentage).'%">'.$pourcentage.'&nbsp;%'.'</td>'; }
          if($make_pdf)  { $pdf->ligne_corps_cellule_corps( $pourcentage ); }
        }
        if($make_html) { $html_table_body .= '</tr>'.NL; }
        if($make_pdf) { $pdf->ligne_retour($socle_composante_id); }
      }
    }
  }
  // assemblage pour la sortie HTML
  if($make_html)
  {
    $html .= '<table class="bilan_repartition">'.NL.$html_table_head.$html_table_body.'</table>'.NL;
  }
  // On enregistre les sorties HTML / PDF / CSV
  if($make_html) { FileSystem::ecrire_fichier(   CHEMIN_DOSSIER_EXPORT.str_replace('<REPLACE>','repartition',$fichier_nom).'.html' , $html ); }
  if($make_pdf)  { FileSystem::ecrire_objet_pdf( CHEMIN_DOSSIER_EXPORT.str_replace('<REPLACE>','repartition',$fichier_nom).'.pdf'  , $pdf  ); }
}
?>