================================================================================
Librairie : phpCAS                      Versions : 1.3.8 / 1.6.1
================================================================================

phpCAS est une classe PHP utilisée comme client CAS, pour communiquer avec un serveur CAS.

Seule la classe est présente dans SACoche.
Pour infos et documentations :
https://apereo.atlassian.net/wiki/spaces/CASC/pages/103252517/phpCAS
https://github.com/apereo/phpCAS

La version 1.3.8 est la dernière compatible avec PHP 5.
Pour ne pénaliser personne, deux versions sont dans les sources, et utilisées en fonction de la version de PHP :
- la version 1.3.8 dans le dossier phpCAS_old
- la version 1.6.1 dans le dossier phpCAS_new

========================================
Traitement des XML invalides
========================================

phpCAS renvoie des erreurs moches et pas explicites.
Voir le fichier
./pages/public_login_SSO.php
où on tente de mettre en place une surcouche plus explicite et aux couleurs de l’appli.

Reste le problème de nombreuses erreurs type
PHP Warning:  DOMDocument::loadXML(): AttValue: &quot; or ' expected in Entity
PHP Warning:  DOMDocument::loadXML(): Couldn't find end of Start Tag font in Entity
PHP Warning:  DOMDocument::loadXML(): Entity 'eacute' not defined in Entity
PHP Warning:  DOMDocument::loadXML(): Entity 'egrave' not defined in Entity
PHP Warning:  DOMDocument::loadXML(): Entity 'nbsp' not defined in Entity
PHP Warning:  DOMDocument::loadXML(): EntityRef: expecting ';' in Entity
PHP Warning:  DOMDocument::loadXML(): Input is not proper UTF-8, indicate encoding !
PHP Warning:  DOMDocument::loadXML(): Opening and ending tag mismatch
PHP Warning:  DOMDocument::loadXML(): Premature end of data in tag html line 1 in Entity
PHP Warning:  DOMDocument::loadXML(): attributes construct error in Entity
en particulier quand le serveur d’authentification renvoie un XML invalide du style
<cas:serviceResponse xmlns:cas='http://www.yale.edu/tp/cas'><cas:authenticationFailure code="INVALID_SERVICE">Service https://sacoche.sesamath.net/sacoche/?sso=6317&cookie invalid for the ticket found</cas:authenticationFailure></cas:serviceResponse>
(qui contient une éperluette non convertie dans l’adresse),
ce qui remplit les logs PHP en plus de l’affichage à l’utilisateur.
Pour y remédier, dans le fichier
./phpCAS/CAS/Client.php
on a remplacé 4 fois
!($dom->loadXML(...)
par
!(@$dom->loadXML(...)
(l’erreur demeure interceptée mais elle ne remplit plus les logs PHP)
Y penser lors de futures mises à jour de la librairie...

========================================
Ajout de la prise en compte de l’atttribut user éventuel
========================================

Pour un ENT, le <cas:user> ne contient pas l’identifiant de jointure (il contient le mail de l’utilisateur...).
Ce prestataire ENT demande de récupérer la valeur dans <cas:attributes>.
Sauf que visiblement phpCAS exclu les attributs donc le nom est parmi { user ; proxies ; proxyGrantingTicket }.
Je suppose car dans ce cas la valeur est censée être identique au <cas:user> à la racine...
Pour y remédier, dans le fichier
./phpCAS/CAS/Client.php
- version OLD vers la ligne 3418 j’ai commenté
  case 'user':
- version NEW vers la ligne 3688 j’ai retiré "user" de 
  if (in_array($child_nodeName, array("user", "proxies", "proxyGrantingTicket"))) {

========================================
Fichier de certificats
========================================

Requis pour l’utilisation de phpCAS::setCasServerCACert().

A/ [certificats/cacert.pem] est issu de http://curl.haxx.se/docs/caextract.html
B/ [certificats/ca-bundle.crt] a été généré par Daniel
 @see https://com-interne.sesamath.net/doku.php?id=serveur:memos:ssl_debug#generer_une_liste_de_certif_racine
 (cf ssl:/sso/_docs_dev/readme_certificat_CAS.php)

Très curieusement, seul A fonctionne sur mon PC pour la vérification de certains domaines, alors que B est plus complet.
Du coup, j’ai généré [certificats/certificats.txt] qui est la concaténation des deux !

En cas de souci avec un ENT, on peut vérifier manuellement la validité de son certificat
 avec l’un de ces services efficaces et explicatifs :
https://www.ssllabs.com/ssltest/analyze.html
https://www.sslshopper.com/ssl-checker.html
https://ssltools.websecurity.symantec.com/checker/views/certCheck.jsp
https://cryptoreport.websecurity.symantec.com/checker/views/certCheck.jsp

Exemple pour tester avec le fichier de la machine :
curl --cacert certificats.txt https://enc.hauts-de-seine.fr
