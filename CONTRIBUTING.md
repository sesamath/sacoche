
**En premier lieu, consulter la page [Support / Contact](https://sacoche.sesamath.net/?page=support) qui présente les différentes situations.**

Pour échanger vos réflexions avec d'autres utilisateurs, poser des questions, ou suggérer des évolutions, vous pouvez utiliser [cette liste de discussions](https://sacoche.sesamath.net?page=echanger).

Pour une demande d'assistance personnelle, utiliser [le formulaire dédié depuis votre espace **SACoche**](https://sacoche.sesamath.net/?page=support#toggle_anomalie_personnel), procédure conforme au RGPD qui nous donnera accès aux informations nécessaires afin d'analyser la situation.

Pour un échange plus général en privé, vous pouvez utiliser l'adresse de contact *sacoche* + @ + *sesamath.net*

Enfin, vous pouvez aussi éventuellement [ouvrir un ticket](https://forge.apps.education.fr/sesamath/sacoche/-/issues) sur le dépôt GIT.
